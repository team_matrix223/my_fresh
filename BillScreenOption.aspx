﻿  <%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="BillScreenOption.aspx.cs" Inherits="BillScreenOption" EnableEventValidation="false" %>
<%--<%@ Register src="~/Templates/CashCustomers.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

    <form id="form1" runat="server">

        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  <asp:HiddenField ID="hdnScreen" runat="server"/>
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
        <link href="css/customcss/billform.css" rel="stylesheet" />
<%--    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-glyphicons.css" /><a href="BreakageExpiry.aspx">BreakageExpiry.aspx</a>
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/customcss/nbscreen1.css" rel="stylesheet" />
		<link href="css/customcss/billscreeoption.css" rel="stylesheet" />
     <script src="js/jquery-ui.js"></script>
   
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
          <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
             <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <style type="text/css">
        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }


        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }
    </style>
    <script language="javscript" type="text/javascript">
        var etrType = "";
		function BindBanks() {
			$.ajax({
				type: "POST",
				data: '{}',
				url: "screen.aspx/BindBanks",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);
					$("#ddlBank_pm").html(obj.BankOptions);
					$("#ddlBank_pm option[value='" + DefaultBank + "']").prop("selected", true);
				}


			});
		}


		function ChangePaymode() {
			var Mode = "";
			var FinalAmount = 0;
			//var BNF = $("#dvShow_pm").attr('dir'); 
			var BNF = $("#txtBillNumber_pm").val();
			if (BNF == "") {
				alert("Please Enter Bill Number");
				//$("#txtBillNumber").focus();
				return;
			}
			var BillType = $("#ddlbillttype_pm").val();
			var CashAmount = $("#txtCashReceived_pm").val();
			var CreditCustId = $("#ddlChosseCredit_pm").val();


			if (BillType == "Cash" || BillType == "CreditCard" || BillType == "OnlinePayment") {

				if (Number($("#txtBalanceReturn_pm").val()) < 0) {

					$.uiUnlock();
					alert("Total amount is not equal to Bill Amount....Please first tally amount.");
					$("#btnBillWindowOk_pm").removeAttr('disabled');
					return;
				}
				else {



					CashAmount = CashAmount - Number($("#txtBalanceReturn_pm").val());
				}

			}


			if (BillType == "CreditCard") {
				if ($("#ddlBank_pm").val() == "") {
					alert("Choose Bank");
					$("#ddlBank_pm").focus();
					return;
				}
			}

			if (BillType == "Credit") {

				if (CreditCustId == 0) {

					alert("Please select Credit Customer");
					$("#txtddlCreditCustomers_pm").focus();

					return;
				}


			}


			if (Number(CashAmount) < 0) {
				$.uiUnlock();
				alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
				$("#btnBillWindowOk_pm").removeAttr('disabled');
				return;
			}





			var CrCardAmount = $("#Text13_pm").val();
			var Bank = $("#ddlBank_pm").val();
			var BankName = $("#ddlBank_pm option:selected").text();
			var OnlinePayment = 0;
			var CreditAmount = $("#txtCashReceived_pm").val();

			var CreditCustName = $("#ddlChosseCredit_pm option:selected").text();
			var amt_cod = parseFloat($("#txtCODAmount_pm").val());
			var amt_online = parseFloat($("#txtpaymentAmount_pm").val());
			var total_amt = parseFloat(amt_cod) + parseFloat(amt_online)
			if (amt_cod <= 0 || amt_cod == "") {
				OnlinePayment = parseFloat(total_amt);
			}

			else if (amt_cod > 0 && amt_online <= 0 || amt_cod > 0 && amt_online == "") {

				CashAmount = amt_cod;

			}
			var OtherPaymentID = $("#<%=ddlOtherPayment_pm.ClientID %>").val();
			 if ($("#rdoCOD_pm").prop('checked') == true) {
				 Mode = "COD"
				 CashAmount = parseFloat($("#txtCODAmount_pm").val()).toFixed(2);
			 }
			 else if ($("#rdoOnline_pm").prop('checked') == true) {
				 Mode = "OnlinePayment";
				 CashAmount = parseFloat($("#txtpaymentAmount_pm").val()).toFixed(2);
			 }
			 if ($("#txtFinalBillAmount_pm").val() != "") {
				 FinalAmount = parseFloat($("#txtFinalBillAmount_pm").val()).toFixed(2);
			 }

			 $.ajax({
				 type: "POST",
				 data: '{ "BillNowPrefix": "' + BNF + '","BillType": "' + BillType + '","CashAmount": "' + CashAmount + '","CrCardAmount": "' + CrCardAmount + '","Bank": "' + Bank + '","BankName": "' + BankName + '","CreditAmount": "' + CreditAmount + '","CreditCustId": "' + CreditCustId + '","CreditCustName": "' + CreditCustName + '","OnlinePayment":"' + OnlinePayment + '","OtherPaymentID":"' + OtherPaymentID + '","Mode":"' + Mode + '","FinalAmount":"' + FinalAmount + '"}',
				 url: "changePaymode.aspx/ChangePaymode",
				 contentType: "application/json",
				 dataType: "json",
				 success: function (msg) {

					 var obj = jQuery.parseJSON(msg.d);
					 $("#<%=ddlOtherPayment_pm.ClientID %>").val("0");
						$("#rdoCOD_pm").prop('checked', false);
						$("#rdoOnline_pm").prop('checked', false);
						$("#txtCODAmount_pm").val("");
						$("#txtpaymentAmount_pm").val("");

						//InsertOnlineOtherPayment_pm(BNF);
						alert("PayMode Changed Successfully");

						$("#dvpaymode").hide();

						ResetBillCntrols();
						Reset();
						// return;

					},
					error: function (xhr, ajaxOptions, thrownError) {

						var obj = jQuery.parseJSON(xhr.responseText);
						alert(obj.Message);
					},
					complete: function () {
						$("#btnBillWindowOk_pm").attr("disabled", "disabled");
						Reset();
						$("#ddlbillttype_pm").val("Cash");
						$("#txtFinalBillAmount_pm").val("0.00");
						$("#txtBillNumber_pm").val("");

						$.uiUnlock();

					}

				});



        }



		function DEVBalanceCalculation_pm() {


			//var txtCashReceived = $("#txtCashReceived_pm");
			//var txtCreditCard = $("#Text13_pm");
			//var txtCheque = $("#Text15_pm");
			//var txtFinalBillAmount = $("#txtFinalBillAmount_pm");


			var txtCashReceived = $("#txtCashReceived_pm");
			var txtCreditCard = $("#Text13_pm");
			var txtCheque = $("#Text15_pm");
			var txtFinalBillAmount = $("#txtFinalBillAmount_pm");
			var txtOnlineAmount = $("#txtpaymentAmount_pm");
			var txtCoupanAmount = $("#Coupans_pm");
			var txtCODAmount = $("#txtCODAmount_pm");


			if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
				txtCreditCard.val(0);
				txtCheque.val(0);
			}
			else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
				txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
				txtCheque.val(0);

			}
			else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
				txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

			}
			var balReturn = Number((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) + Number(txtCoupanAmount.val()) + Number(txtCODAmount.val()) - Number(txtFinalBillAmount.val())));

			$("#txtBalanceReturn_pm").val(balReturn.toFixed(2));


		}



		function Reset() {
			$("#txtCashReceived_pm").val("0.00");
			$("#Text13_pm").val("0.00");
			$("#ddlBank_pm").val("");
			$("#dvOuter_ddlCreditCustomers_pm").hide();

			$("#ddlChosseCredit_pm").hide();
			$("#ddlChosseCredit_pm").val(0);
		}


        $(document).ready(function () {

            var Isdata = "";
            $("body").attr('autocomplete', 'off');
                          $('#<%=dd_customername.ClientID %>').change(function () {

<%--            PaymentModeID = $('#<%=dd_customername.ClientID %> option:selected').val();

            cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();

          

            if (cst_id != 0) {

                $("#dvDeliveryChrg").val(0);
               // $("#dvDeliveryChrg").attr("disabled", "disabled");
                $("#chkDelivery").hide();
                $("#lbldelcharges").hide();
                //$("#DelCharges").hide(); 
                $("#dvnetAmount").text(0);
                $("#txtorderno").show();

            }
            else {
                $("#dvDeliveryChrg").val(DeliveryCharges);
               // $("#dvDeliveryChrg").attr("disabled", "disabled");
                $("#chkDelivery").hide();
                $("#lbldelcharges").hide();
                $("#DelCharges").show();
                $("#txtorderno").val("");
                $("#txtorderno").hide();

            }--%>

            $("#txxtItemCode").focus();

        });


        });


        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }
        var PriceEnable = false;
        var MRPEnable = false;

                var cst_id = 0;
                var clickmode = "";
                var TakeAwayDine = "";
                var BillBasicType = "";
                var AllowServicetax = false;
                var AllowServicetaxontake = false;
                var EnableCashCustomer = false;
                var DefaultPaymode = "";
        var DefaultBank = "";
		var DefaultBankName = "";
        var RoundBillAmount = false;
        var BillOption = "";
        var PrintRoute = false;
        var PrintShortName = false;
		var OpenDiv = false;
                var FocAffect = false;
                var NEgativeStock = false;
                var AllowDiscountOnBilling = false;
                var EnableDiscountAmount = false;
                var EnableCustomerDiscount = false;
                var DiscountOnBillValue = false;
                var BackEndDiscount = false;
                var BEndDiscountAmt = 0;
                var m_BillMode = "";
                var RoleForEditRate = false;
                var HomeDelCharges = false;
                var minbillvalue = 0;
        var DeliveryCharges = 0;
        var newbill = 0;
                var getdel_cust_id = 0;
                var DiscountValues = [];
                function clsDiscount() {
                    this.StartValue = 0;
                    this.EndValue = 0;
                    this.DisPer = 0;

                }


  
		       
                // un-comment to display key code
                // $("input").keydown(function (e) {
                //   console.log(e.which);
                // });

                function ResetCashCustmr() {

                    $("#lblCashCustomerName").text("");
                    CshCustSelId = 0;
                    CshCustSelName = "";
                    $("#CashCustomer").css("display", "none");
                    $("#txtddlMobSearchBox").val("");
                    $("#ddlMobSearchBox").html("");
					$("#txtpnno").text("");

                }

                $(document).ready(function (eOuter) {
                    $('input').on('keydown', function (eInner) {
                        if (eInner.which === 40) { //enter key
                            var tabindex = $(this).attr('tabindex');
                            tabindex++; //increment tabindex
                            $('[tabindex=' + tabindex + ']').focus();

                        }
                    });
                });
		
		
                function BindAddOn(counter) {
                    $.ajax({
                        type: "POST",
                        data: '{"counter": "' + counter + '"}',
						url: "BillScreenOption.aspx/LoadUserControl",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            $("#dvAddOn").html(msg.d);

                            $("#dvAddOn").dialog({
                                autoOpen: true,

                                width: 500,
                                resizable: false,
                                modal: false
                            });

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                        }

                    }
                       );



                }

                function processingComplete() {
                    $.uiUnlock();

                }


		        var billinser = 0;
		        var onlineoption = "COD";
                var CshCustSelId = 0;
                var CshCustSelName = "";
                var CrdCustSelId = 0;
                var CrdCustSelName = "";
                var m_BillNowPrefix = "";
                var m_DiscountType = "";
                var Sertax = 0;
                var Takeaway = 0;
                var Takeawaydefault = 0;
                var mode = "";
                var count = 0;
                var modeRet = "";
                var billingmode = ""
                var OrderId = "";
                var Type = "";
                var TotalItems = 0;
                var is_tab = 0;
        var uparrow = 0; 
        var row_index = 0;
                //.....................................

                function BindTables() {

                    $.ajax({
                        type: "POST",
                        data: '{}',
                        url: "screen.aspx/BindTables",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);
                            $("#ddlTable").html(obj.TableOptions);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                        }


                    });


                }



                function OpenBillWindow() {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").hide();
                    $("#dvBillWindow").show();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                }

                function OpenProductWindow() {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                }

                function UnHoldBill(HoldNo) {
                    RestControls();

                    $.ajax({
                        type: "POST",
                        data: '{ "HoldNo": "' + HoldNo + '"}',
                        url: "screen.aspx/GetByHoldNo",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);


                            for (var i = 0; i < obj.productLists.length; i++) {
                                addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], "", obj.productLists[i]["Max_Retail_Price"], obj.productLists[i]["CursorOn"],0);


                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {
                            $("#CustomerSearchWindow").hide();
                            $("#dvProductList").show();
                            $("#dvBillWindow").hide();
                            $("#dvCreditCustomerSearch").hide();
                            $("#dvHoldList").hide();
                            $("#dvOrderList").hide();
                            $.uiUnlock();
                        }

                    });


                }


		function CounterBill(billno) {
            $.uiLock();
            
            BillNowPrefix = billno;
			$.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + billno + '"}',
				url: "BillScreenOption.aspx/GetCounterBillDetailByBillNowPrefix",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

					

					for (var i = 0; i < obj.productLists.length; i++) {
						//addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"],obj.productLists[i]["Edit_SaleRate"]);
						addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["ChallanNo"], obj.productLists[i]["Max_Retail_Price"], obj.productLists[i]["CursorOn"],1);
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
                complete: function () {
                   
					
					$.uiUnlock();
				}

			});
            $.uiUnlock();
           
		}


        function EditBill(BillNowPrefix) {
                    $.uiLock();
                    BillNowPrefix = m_BillNowPrefix;
                    $.ajax({
                        type: "POST",
                        data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
						url: "BillScreenOption.aspx/GetBillDetailByBillNowPrefix",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg)
                        {

                            var obj = jQuery.parseJSON(msg.d);

                            $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);

                            for (var i = 0; i < obj.productLists.length; i++)
                            {
                                //addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"],obj.productLists[i]["Edit_SaleRate"]);
                                addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["ChallanNo"], obj.productLists[i]["Max_Retail_Price"], obj.productLists[i]["CursorOn"],0);
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {
                            $("#CustomerSearchWindow").hide();
                            $("#dvProductList").show();
                            $("#dvBillWindow").hide();
                            $("#dvCreditCustomerSearch").hide();
                            $("#dvHoldList").hide();
                            $("#dvOrderList").hide();
                            $.uiUnlock();
                        }

                    });


                }

                function selected_prdct(itemcode) {

                    GetByItemCode(itemcode);
                    $("#search_item").dialog('close');
					$("#ddlItemSearchBox").html("<option value='0'></option>");
                   
                }
               
                function del_row() {

                    var element_id = "";
                    if (element_id == "txtItemCode") {
                        return false;
                    }
                    var RowIndex = Number($(".delit").closest('tr').index());
                    var tr = $(".delit").closest("tr");
                    tr.remove();

                    ProductCollection.splice(RowIndex, 1);

                    if (ProductCollection.length <= 0) {


                        RestControls();
                    }

                    $('#tbProductInfo').find(".delit").remove();
                    //$('.delit').remove();
                    Bindtr();
                }
        var upfirst = 0;
        $(document).keydown(function (e) {
           
           
            var tr = $('#tbProductInfo tr:has(input:visible)').filter(function () {
                return $('input', this).is(':focus');

            });

            $('#tbProductInfo tr').removeClass('delit');
            tr.addClass('delit');

            if (e.which == 120) {
                if ($("#txtItemCode").is(':focus')) {
                    return false;
                }
                del_row();
            }
           
                
        });


		$("#tbProductInfo").keydown(function (e) {

			

		});



   
                $(document.body).on('keypress', '.delit', function (e) {
                    var IsEdit = PriceEnable;
                    
                    var keyC = e.keyCode;
                    var currentRow = $(this).closest("tr");
                   
                    if (keyC == 13) {
                        

                        if (IsEdit == false) {
                            
							if (currentRow.find("#txtBillQty").is(':focus')) {
								$("#txtItemCode").focus();
								

                            }
							else if (currentRow.find("#txtBillPrice").is(':focus')) {
								currentRow.find("#txtMaxRetailPrice").focus().select();

							}
							else if (currentRow.find("#txtMaxRetailPrice").is(':focus')) {
								$("#txtItemCode").focus();

                            }
							else if (currentRow.find("#txtBillName").is(':focus')) {
								currentRow.find("#txtBillQty").focus().select();


							}
                        }
                        else {
							
                            if (currentRow.find("#txtBillQty").is(':focus')) {
                                //$("#txtItemCode").focus();
                                currentRow.find("#txtBillPrice").focus().select();
                                //currentRow.find("#txtItemCode").focus().select();

                            }
                            else if (currentRow.find("#txtBillPrice").is(':focus')) {
                                currentRow.find("#txtMaxRetailPrice").focus().select();

                            }
                            else if (currentRow.find("#txtMaxRetailPrice").is(':focus')) {
                                $("#txtItemCode").focus();

                            }
                            else if (currentRow.find("#txtBillName").is(':focus')) {
								currentRow.find("#txtBillQty").focus().select();

							}
                        }


                     
                        is_tab = 1;
                    }
                    
                });


            

                function GetAllProducts(keyword) {
                    $.ajax({
                        type: "POST",
                        data: '{ "keyword": "' + keyword + '"}',
                        async: false,
                        url: "BillScreenOption.aspx/GetAllProducts",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {


                            var obj = jQuery.parseJSON(msg.d);
                            var table = $('#tbl_getall_prdct').DataTable();
							
                            
                            //clear datatable
                           table.clear().draw(); 

                            //destroy datatable
                            table.destroy();
                            
                            
                             $("#tbl_body_getall_prdct").html(obj.TableData);
                            $("#tbl_getall_prdct").DataTable();

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                        }

                    });


                }


                function GenerateBill(OrderNo) {
                    RestControls();
                    OrderId = OrderNo;

                    $.ajax({
                        type: "POST",
                        data: '{ "OrderNo": "' + OrderNo + '"}',
                        url: "screen.aspx/GetByOrderNo",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);

                            for (var i = 0; i < obj.productLists.length; i++) {
                                addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["ItemDiscount"], 1, obj.productLists[i]["Edit_SaleRate"], "", ProductCollection[i]["MaxRetailPrice"], ProductCollection[i]["Cursor_On"],0);


                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {
                            $("#CustomerSearchWindow").hide();
                            $("#dvProductList").show();
                            $("#dvBillWindow").hide();
                            $("#dvCreditCustomerSearch").hide();
                            $("#dvHoldList").hide();
                            $("#dvOrderList").hide();
                            $.uiUnlock();
                        }

                    });


                }


                function BackToList() {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();

                }




                function DEVBalanceCalculation() {


                    var txtCashReceived = $("#txtCashReceived");
                    var txtCreditCard = $("#Text13");
                    var txtCheque = $("#Text15");
                    var txtFinalBillAmount = $("#txtFinalBillAmount");


                    if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                        txtCreditCard.val(0);
                        txtCheque.val(0);
                    }
                    else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                        txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                        txtCheque.val(0);

                    }
                    else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                        txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

                    }



                    var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


                    $("#txtBalanceReturn").val(balReturn.toFixed(2));


                }




                //...................................

                var DiscountAmt = 0;

        var Total = 0;
        var ttlQty = 0;
                var DisPer = 0;
                var VatAmt = 0;
                var TaxAmt = 0;


                function bindGrid2() {

                    var searchon = $("input[name='searchon1']:checked").val();
                    var criteria = $("input[name='searchcriteria1']:checked").val();
                    var stext = $("#Txtsrchcredit").val();


                    jQuery("#jQGridDemoCredit").GridUnload();

                    jQuery("#jQGridDemoCredit").jqGrid({
                        url: 'handlers/CreditCustomerSearch.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '&cType=' + "Customer",
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Code', 'Name', 'CSTNO', 'TINNO'],
                        colModel: [
                                    { name: 'CCODE', key: true, index: 'CCODE', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                    { name: 'CNAME', index: 'CNAME', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                                    { name: 'CST_NO', index: 'CST_NO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                    { name: 'TINNO', index: 'TINNO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },




                        ],
                        rowNum: 10,
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPagerCredit',
                        sortname: 'CCODE',
                        viewrecords: true,
                        height: "100%",
                        width: "700px",
                        sortorder: 'desc',
                        caption: "Customers List"


                    });


                    $('#jQGridDemoCredit').jqGrid('navGrid', '#jQGridDemoPagerCredit',
                               {
                                   refresh: false,
                                   edit: false,
                                   add: false,
                                   del: false,
                                   search: true,
                                   searchtext: "Search",
                                   addtext: "Add",
                               },

                               {//SEARCH
                                   closeOnEscape: true

                               }

                                 );

                    var Datad = jQuery('#jQGridDemoCredit');
                    Datad.jqGrid('setGridWidth', '100%');

                    $("#jQGridDemoCredit").jqGrid('setGridParam',
                     {
                         onSelectRow: function (rowid, iRow, iCol, e) {

                             if (billingmode == "direct") {
                                 $("#btnOk").css("display", "block");
                                 $("#btnCancel").css("display", "block");
                             }
                             else {
                                 $("#CustomerSearchWindow").hide();
                                 $("#dvProductList").hide();
                                 $("#dvBillWindow").show();
                                 $("#dvCreditCustomerSearch").hide();
                                 $("#dvHoldList").hide();
                                 $("#dvOrderList").hide();
                                 var customerId = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CCODE');
                                 CrdCustSelId = customerId;
                                 $("#hdnCreditCustomerId").val(customerId);
                                 $("#lblCreditCustomerName").text($('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME'));
                                 CrdCustSelName = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME')
                                 //$('#dvCreditCustomerSearch').dialog('close');
                                 $("#creditCustomer").css("display", "block");
                                 $("#ddlbilltype option[value='Credit']").prop("selected", true);

                             }



                         }
                     });

                }




                function bindGrid(searchon, mobile) {


                    //var searchon=$("input[name='searchon']:checked").val();
                    var criteria = $("input[name='searchcriteria']:checked").val();
                    var stext = $("#txtSearch1").val();
                    if (searchon == "M") {
                        stext = mobile;

                    }

					
                    jQuery("#jQGridDemo").GridUnload();


                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/CustomersList.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ID', 'Name', 'Address1', 'Address2', 'Area', 'City', 'State', 'DateOfBirth', 'AnniversaryDate', 'Discount', 'ContactNo', 'Tag', 'FocBill', 'Group'],
                        colModel: [
                                    { name: 'Customer_ID', key: true, index: 'Customer_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                                    { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: false },
                                     { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                     { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },

                                     { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                     { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                     { name: 'Date_Of_Birth', index: 'Date_Of_Birth', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                     { name: 'Date_Anniversary', index: 'Date_Anniversary', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                     { name: 'Discount', index: 'Discount', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                                     { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },

                                    { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                                    { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                                   { name: 'grpid', index: 'grpid', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                        ],
                        rowNum: 10,
                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Code',
                        viewrecords: true,
                        height: "100%",
                        width: "800px",
                        sortorder: 'desc',
                        caption: "Customers List"


                    });


                    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                               {
                                   refresh: false,
                                   edit: false,
                                   add: false,
                                   del: false,
                                   search: true,
                                   searchtext: "Search",
                                   addtext: "Add",
                               },

                               {//SEARCH
                                   closeOnEscape: true

                               }



                                 );

                    var Datad = jQuery('#jQGridDemo');
                    Datad.jqGrid('setGridWidth', '290');



                    $("#jQGridDemo").jqGrid('setGridParam',
                        {
                            onSelectRow: function (rowid, iRow, iCol, e) {


                                var Discount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Discount');
                                $("#lblCashCustomerName").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));

                                CshCustSelId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_ID');
                                CshCustSelName = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name');
                                //                 $("#lblCashCustomerAddress").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));
                                // $('#dvSearch').dialog('close');
                                $("#CashCustomer").css("display", "block");

                                if (EnableCustomerDiscount == 1) {
                                    $("#dvdisper").val(Discount.toFixed(2));

                                }

                                $("#CustomerSearchWindow").hide();
                                $("#dvProductList").show();
                                $("#dvBillWindow").hide();
                                $("#dvHoldList").hide();
                                $("#dvOrderList").hide();
                                CommonCalculation();
                            }
                        });




                }

        function RestControls() {
            CountrBillNo = "";
            CountrBill = "";


                    $("#lblCreditCustomerName").text("");

                    $("ddlCreditCustomers").hide();
                    $("ddlChosseCredit").hide();
                    $("ddlChosseCredit").val(0);
                    BillNowPrefix1 = "";
                    Type = "";
                    $("#CashCustomer").css("display", "none");
                    ProductCollection = [];

                    $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                    var tr = "<tr style='border-bottom:0px'> <td style='width: 100px'> <input type='text' autocomplete='off'  class='form-control tbItemCode nbscreen_itemcode' placeholder='Enter Code' aria-describedby='basic-addon2' id='txxtItemCode' /></td></tr>";
                    $("#tbProductInfo").prepend(tr);
                    $("#dvdisper").val(0);
                    $("#dvdiscount").val(0);
                    $("#dvnetAmount").html("0");
                    $("#dvRound").html("0");
                    $("#dvTax").html("0");
                    $("#dvVat").html("0");
                    $("#dvDeliveryChrg").val("0");
                    $("#dvsertaxper").html("0");
                    $("#dvsbtotal").html("0");
                    $("#lblNoItems").html(" ");
                       $('#<%=dd_customername.ClientID %>').removeAttr("disabled");
                    m_ItemId = 0;
                    m_ItemCode = "";
                    m_ItemName = "";
                    m_Qty = 0;
                    m_Price = 0;
                    m_Vat = 0;
                    m_Max_Retail_Price = 0;


                    $("#txtFinalBillAmount").val("");
                    $("#ddlbillttype").val("");
                    $("#txtCashReceived").val("0");
                    $("#Text13").val("0");
                    $("#Text15").val("0");
                    $("#Text14").val("");
                    $("#ddlType").val("");
                    $("#ddlBank").val("");
                    $("#ddlTable option").removeAttr("selected");
                    $("#Text16").val("");
                    $("#txtBalanceReturn").val("0");
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvProductList").show();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $("#txxtItemCode").focus();
                    $("#txtCashReceived").val("0").prop("readonly", false);
                    CrdCustSelId = 0;
                    CshCustSelId = 0;
                    CshCustSelName = "";
                    CrdCustSelName = "";
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            $("#btnEdit").css({ "display": "none" });
            $("#btnShowScreen").css({ "display": "none" });

            $("#btnDelete").css({ "display": "none" });

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "9") {

                    $("#btnShowScreen").css({ "display": "block" });
                }

                if (arrRole[i] == "3") {

                    $("#btnEdit").css({ "display": "block" });
                }

                if (arrRole[i] == "2") {

                    $("#btnDelete").css({ "display": "block" });
                }
            }


            //$.ajax({
            //    type: "POST",
            //    data: '{ }',
            //    url: "BillScreenOption.aspx/GetLastBill",
            //    contentType: "application/json",
            //    dataType: "json",
            //    success: function (msg) {

            //        var obj = jQuery.parseJSON(msg.d);
            //        $("#billnumb").html(obj.bill_no);
            //        $("#billamt").html(obj.net_amt);


            //    },
            //    error: function (xhr, ajaxOptions, thrownError) {

            //        var obj = jQuery.parseJSON(xhr.responseText);
            //        alert(obj.Message);
            //    },
            //    complete: function () {



            //    }

            //});


        }


        function InsertHoldBill() {

            if (m_BillMode != "") {
                alert("Sorry, You cannot Hold Bill in Edit Mode");
                return;
            }


            var custcode = 0;
            var custName = "";
            if (CshCustSelId == "0") {
                custcode = 0;
                custName = "CASH";

            }
            else {
                custcode = CshCustSelId;
                custName = CshCustSelName;

            }
           


            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();
            var addtaxamt = $("#dvVat").html();
            var NetAmt = $("#dvnetAmount").html();

            var billmode = "0";

            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var PAmt = [];
            var Tax = [];

            var Ptax = [];
            var ItemRemarks = [];

            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {

                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Price[i] = ProductCollection[i]["Price"];
                Qty[i] = ProductCollection[i]["Qty"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];

                Tax[i] = ProductCollection[i]["TaxCode"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
            }


            $.ajax({
                type: "POST",
                data: '{ "CustomerId": "' + custcode + '","CustomerName": "' + custName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","ItemRemarksArr": "' + ItemRemarks + '"}',
                url: "screen.aspx/InsertHoldBill",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {
                        alert("Bill Holded Successfully");
                        RestControls();
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }



        function AddRemarks(counterId) {

            var DRemarks = $("#txtRemarks" + counterId).val();
            ProductCollection[counterId]["ItemRemarks"] = DRemarks;


        }

		function GeneratePdf(BillNowPrefix) {

			$.ajax({
				type: "POST",
				data: '{"BillNowPrefix": "' + BillNowPrefix + '"}',
				url: "BillScreen.aspx/GeneratePDF",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);




				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


				}

			});


		}



        function InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment) {
            
            if (billinser == 0) {
                var OrderNo = OrderId;
                if (OrderNo == "") {
                    OrderNo = "0";
                }
                if (billmode == "OnlinePayment") {

                    if ($("#ddlOtherPayment").val() != 0) {

                        cst_id = $("#ddlOtherPayment").val();


                    }
                    else {

                        cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();

                    }

                }
                else {
                    cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();
                }

                var BIllValue = $("#dvsbtotal").html();

                var DisPer = $("#dvdisper").val();

                var lessdisamt = $("#dvdiscount").val();

                var addtaxamt = $("#dvVat").html();
                var RoundAmt = 0;
                var hdnNetamt = $("#hdnnetamt").val();
                RoundAmt = Number(NetAmt) - Number(hdnNetamt);

                var DeliveryCharges = $("#dvDeliveryChrg").val();
                var remarks = option;

                if (hdnNetamt <= 0) {
                    alert("Bill Amount Should be Greater than Zero");

                    return;
                }
                var Tableno = $("#ddlTable").val();
                if (Tableno == "") {
                    Tableno = "0";
                }

                if (option == "Dine") {


                    if (Tableno == "0") {
                        alert("Please choose Table Number");
                        $("#ddlTable").focus();
                        return;
                    }
                }

                var setatx = $("#dvTax").html();
                var ServiceTax = $("#dvsertaxper").html();

                var ItemCode = [];
                var ItemName = [];
                var MRP = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];
                var ItemRemarks = [];
                var SurPer = [];
                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");

                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    ItemName[i] = ProductCollection[i]["ItemName"];
                    MRP[i] = ProductCollection[i]["MaxRetailPrice"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    OrgSaleRate[i] = ProductCollection[i]["Price"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                    ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                    SurPer[i] = ProductCollection[i]["SurVal"];

                }

                TaxDen = [];
                VatAmtDen = [];
                VatDen = [];
                SurDen = [];

                $("div[name='tax']").each(
                    function (y) {
                        TaxDen[y] = $(this).html();
                    }
                );
                $("div[name='amt']").each(
                    function (z) {
                        VatAmtDen[z] = $(this).html();
                    }
                );
                $("div[name='vat']").each(
                    function (a) {
                        VatDen[a] = $(this).html();
                    }
                );
                $("div[name='sur']").each(
                    function (a) {
                        SurDen[a] = $(this).html();
                    }
                );
                var BillNowPrefix = "";

                $.uiLock();

                $.ajax({
                    type: "POST",
                    data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","itemnameArr": "' + ItemName + '","mrpArr": "' + MRP + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","Mode": "' + clickmode + '","EmpCode":"' + 0 + '","KKCPer":"' + 0 + '","KKCAmt":"' + 0 + '","SBCPer":"' + 0 + '","SBCAmt":"' + 0 + '","BillRemarks":"' + 0 + '","cst_id": "' + cst_id + '","Order_No":"0","CounterBillNo":"' + CountrBillNo + '"}',
                    url: "BillScreenOption.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        BillNowPrefix = obj.BNF;

                        if (PrintRoute == 1) {

                            if (clickmode == "New") {
                                GeneratePdf(BillNowPrefix);

                            }
                        }

                        if (BillOption == "Direct") {
                            var iframe = document.getElementById('reportout');

                            //                if (iframe != null) {
                            //                    document.body.removeChild(iframe);
                            //                } 

                            iframe = document.createElement("iframe");
                            iframe.setAttribute("id", "reportout");
                            iframe.style.width = 0 + "px";
                            iframe.style.height = 0 + "px";
                            document.body.appendChild(iframe);

                            window.location = 'BillReport.aspx?BillNowPrefix=' + BillNowPrefix;

                        }




                        if (obj.Status == -11) {
                            alert("You don't have permission to perform this action..Consult Admin Department.");
                            return;
                        }


                        if (obj.Status == -5) {
                            alert("Please Login Again and Try Again..");
                            return;
                        }

                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else {

                            $("#lblAmmmt").html(NetAmt);
                            //$("#dvbillSave").toggle();
                            //                             Printt(BillNowPrefix);
                            $.uiUnlock();
                            RestControls();
							$.ajax({
								type: "POST",
								data: '{ }',
								url: "BillScreen.aspx/GetLastBill",
								contentType: "application/json",
								dataType: "json",
								success: function (msg) {

									var obj = jQuery.parseJSON(msg.d);
									$("#billnumb").html(obj.lastrecord.BillNowPrefix);

									$("#billamt").html(obj.lastrecord.Net_Amount);


								},
								error: function (xhr, ajaxOptions, thrownError) {

									var obj = jQuery.parseJSON(xhr.responseText);
									alert(obj.Message);
								},
								complete: function () {

									$.uiUnlock();

								}

							});
                            BindGrid();
                            $(".product_table").removeClass('newprotbl');

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $("#btnBillWindowOk").removeAttr('disabled');


                        $.uiUnlock();


                    }

                });
            }

        }


        var option = "";


        function Printt(celValue) {



            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");

            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";

            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);

            document.getElementById('reportout').contentWindow.location = "Reports/rptRetailBill.aspx?BillNowPrefix=" + celValue;



        }

        var incldy = "";
        var CountrBill = "";
        var CountrBillNo = "";
        var ItmQty = 0;
        function GetByItemCode(ItemCode) {
			
            incldy = "";
            CountrBill = "";
			if (ItemCode.indexOf('y') > -1 || ItemCode.indexOf('Y') > -1) {
				incldy = "y";
			}
			else {
				incldy = "";

            }
            if (ItemCode.indexOf('@@') > -1) {
                
				
                CountrBill = "Counter";


            }
            if (CountrBill == "") {
                if (ItemCode.indexOf('@') > -1) {

                    var arr = ItemCode.split('@');

                    var arr2 = arr[1].substring(0, 4);

                    ItemCode = arr2;

                    ItmQty = (arr[1].substring(4,9) / 1000);
                    
                }
            }
           
			
           
            $.uiLock('');
            if (CountrBill == "Counter") {

                var arr = ItemCode.split('@');
                CountrBillNo = arr[2];
                
				CheckCounterBill(CountrBillNo);
            }
          
            else {
				

                var rows = $('#tbProductInfo tr').length;
                var billtype = $('#<%=dd_customername.ClientID %> option:selected').val();
                $.ajax({
                    type: "POST",
                    data: '{ "ItemCode": "' + ItemCode + '", "billtype": "' + billtype + '"}',
                    url: "BillScreenOption.aspx/GetByItemCode",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        if (obj.productData.ItemID == 0) {

                            CheckItemCode(ItemCode);

                        }
                        else {
                            is_tab = 0;

                            addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, "", obj.productData.Discount, 0, obj.productData.Edit_SaleRate, "", obj.productData.Max_Retail_Price, obj.productData.CursorOn,0)

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        $.uiUnlock();
                    }

                });
                if (rows > 0) {

                    $('#<%=dd_customername.ClientID %>').attr("disabled", "disabled");


                }

            }

        }

        var isopen = true;
		function CheckItemCode(ItemCode) {
			$.ajax({
				type: "POST",
				data: '{"ItemCode": "' + ItemCode + '"}',
				url: "BillScreenOption.aspx/ChkItemCode",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

                    var isexist = obj.rtnval;
                   
                    if (isexist == 1) {

                        BindGridProducts(ItemCode);
                        $("#ItemGrid1").dialog({
                            modal: true,
                            closeOnEscape: false,
                        });

                        $("#ItemGrid1").parent().addClass('Itemgrid-pop');


                    }
                    else {
                        alert("Item Code Does not Exists");
                        $("#txtItemCode").focus();
                        return;
                    }

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


					$.uiUnlock();
				}

			});

        }
        var checkbnkcount = 0;
		function CheckBankCount() {

			$.ajax({

				type: "POST",
				data: '{}',
				url: "BillScreenOption.aspx/ChkBankCount",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

                    checkbnkcount = obj.rtnval;

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


					$.uiUnlock();
				}

			});

		}



        function CheckCounterBill(ItemCode) {
			
            $.ajax({
               
				type: "POST",
				data: '{"billNo": "' + ItemCode + '"}',
				url: "BillScreenOption.aspx/ChkCounterBill",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

					var isexist = obj.rtnval;

                    if (isexist == 0) {
                      
						CounterBill(ItemCode);
                    }
                    else {
                        alert("Bill has been generated against this estimate");
                        RestControls();
                    }



				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


					$.uiUnlock();
				}

			});

		}




        function GetDiscountType() {
            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/GetDiscountType",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    m_DiscountType = obj.Discount.DiscountType;
                    if (m_DiscountType == "DisPerAmount") {
                        $("#dvdiscount").removeAttr("disabled");
                        $("#dvdisper").removeAttr("disabled");
                    }
                    else if (m_DiscountType == "BackEndDiscount") {


                        $("#dvdiscount").prop("disabled", "disabled").val("0");
                        $("#dvdisper").prop("disabled", "disabled").val("0");
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });

        }

        function GetPluginData(Type) {

            if (Type == "CashCustomer") {


                var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
				$("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + "," + $("#ddlMobSearchBox option:selected").attr("phone") + "," + $("#ddlMobSearchBox option:selected").attr("Address"));

                CshCustSelId = $("#ddlMobSearchBox option:selected").val();
                CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

                $("#CashCustomer").css("display", "block");

                if (EnableCustomerDiscount == 1) {
                    $("#dvdisper").val(Discount);

                }

                $("#CustomerSearchWindow").hide();
                $("#dvProductList").show();
                $("#dvBillWindow").hide();
                $("#dvHoldList").hide();
                $("#dvOrderList").hide();
                CommonCalculation();





            }
            else if (Type == "Accounts") {
                var customerId = $("#ddlCreditCustomers").val();
                CrdCustSelId = customerId;
                $("#hdnCreditCustomerId").val(customerId);


                $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
                CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



                $("#creditCustomer").css("display", "block");
            }
            if (Type == "Product") {
                
                var ItemCode = $("#ddlItemSearchBox option:selected").attr("Item_Code");
               
				selected_prdct(ItemCode)
            }
        }
		var m_CustomrId = -1;

        $(document).ready(
         function () {
				$('#cm_DOB').daterangepicker({
					singleDatePicker: true,
					calender_style: "picker_1"
				}, function (start, end, label) {
					console.log(start.toISOString(), end.toISOString(), label);
				});

				$('#cm_DOA').daterangepicker({
					singleDatePicker: true,
					calender_style: "picker_1"
				}, function (start, end, label) {


					console.log(start.toISOString(), end.toISOString(), label);
				});

				$("#cm_ddlPrefix").val();
				$("#cm_Name").val("");
				$("#cm_Address1").val("");
				$("#cm_Address2").val("");
				//                                           
				$("#cm_DOB").val("1900-01-01");
				$("#cm_DOA").val("1900-01-01");
				$("#cm_Discount").val("0");
				$("#cm_Tag").val("0");
				$("#cm_ContactNumber").val("");
				$('#cm_IsActive').prop("checked", true);


             GetLastBill();
                $("#btnrowdelete").click(function () {
                   
				

                });



                $("#btnsearch").click(function () {
                    $("#search_item").dialog({
                     width: 800,
                     modal: true
                    });
                   // BindGrid21();

                   // GetAllProducts('');
                 //$("#search_product").dialog({
                 //    width: 800,
                 //    modal: true
                 //});
                    $("#search_item").parent().addClass('search-pop');
					$("#ddlItemSearchBox").focus();
                 //$("#tbl_getall_prdct_filter input").focus();
             });
             
             $("#btnGSTBill").click(
             function () {

                 $("#GSTDialog").dialog({
                     autoOpen: true,

                     width: 400,
                     resizable: false,
                     modal: false
                 });

                 $("#chkGst").prop("checked", true);
             }
             );

//////////////////////////////////////

				$("#ddlbillttype_pm").change(
					function () {



						if ($(this).val() == "Credit") {
							$("#ddlcustName_pm").show();
							$("#codChk").hide();
							$("#OnlineChk").hide();
							$("#dvOuter_ddlCreditCustomers_pm").show();
							$("#lblCashHeading_pm").text("Receipt Amt:");
							$("#ddlbillttype_pm option[value='Credit']").prop("selected", true);
							$("#txtCashReceived_pm").val("0").prop("readonly", false);
							$("#txtCashReceived_pm").removeAttr("disabled");
							$("#Text13_pm").val("0").prop("readonly", true);
							$("#Text14_pm").val("").prop("readonly", true);
							$("#Text15_pm").val("0").prop("readonly", true);
							$("#Text16_pm").val("").prop("readonly", true);
							$("#ddlType_pm").prop("disabled", true);
							$("#ddlOtherPayment_pm").hide();
							$("#rdoCOD_pm").hide();
							$("#rdoOnline_pm").hide();
							$("#Td1_pm").hide();
							$("#txtCODAmount_pm").hide();
							$("#COD_pm").hide();
							$("#Online_pm").hide();

							$("#txtpaymentAmount_pm").hide();

							$("#No_pm").hide();
							$("#Amt_pm").hide();
							$("#Coupans_pm").hide();
							$("#OP_pm").hide();
							$("#coupan_pm").hide();
							$("#holder_pm  input").hide();
							$("#holder2_pm  input").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();

							$("#ddlBank_pm").prop("disabled", true);
							$("#ddlChosseCredit_pm").show();
							$("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
							$("#Text13_pm").hide();
							$("#ddlType_pm").hide();
							$("#ddlBank_pm").hide();
							$("#crcredit_pm").hide();
							$("#Type_pm").hide();
							$("#Bank_pm").hide();
							$("#lblCashHeading_pm").hide();
							$("#txtCashReceived_pm").hide();
							$("#rdoCOD_pm").hide();
							$("#rdoOnline_pm").hide();
							$("#lblCashHeading_pm").show();
							$("#txtCashReceived_pm").show();
							$("#lblCashHeading_pm").hide();
							$("#txtCashReceived_pm").hide();
							var FinalAmt = $("#txtFinalBillAmount_pm").val();
							$("#txtCashReceived_pm").val(parseFloat(FinalAmt).toFixed(2));
							$("#txtBalanceReturn_pm").val(0);
						}
						else if ($(this).val() == "CreditCard") {
							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId_pm").val(0);
							$("#lblCreditCustomerName_pm").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers_pm").html("");
							$("#txtddlCreditCustomers_pm ").val("");
							$("#Coupans_pm").hide();
							$("#OP_pm").hide();
							$("#coupan_pm").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();
							$("#COD_pm").hide();
							$("#Online_pm").hide();
							$("#rdoOnline_pm").hide();
							$("#lblCashHeading_pm").text("Cash Rec:");
							$("#creditCustomer_pm").css("display", "none");
							$("#ddlType_pm").prop("disabled", true);
							$("#ddlOtherPayment_pm").hide();
							$("#txtpaymentAmount_pm").hide();
							$("#rdoCOD_pm").hide();
							$("#Td1_pm").hide();
							$("#txtCODAmount_pm").hide();
							$("#dvOuter_ddlCreditCustomers_pm ").hide();
							$("#ddlChosseCredit_pm").hide();
							$("#holder_pm  input").hide(); $("#holder2_pm  input").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();

							//                   //$("#ddlOtherPayment").hide();

							$("#txtCashReceived_pm ").val("0").prop("readonly", true);
							$("#Text13_pm").removeAttr("disabled");
							$("#Text13_pm").removeAttr("readonly");
							//$("#Text13_pm").val("0").prop("readonly", false);
							$("#Text14_pm").val("").prop("readonly", false);
							$("#Text15_pm").val("0").prop("readonly", false);
							$("#Text16_pm").val("").prop("readonly", false);
							$("#ddlType_pm").prop("disabled", false);
							$("#ddlBank_pm").prop("disabled", false);
							$("#Text13_pm").val($("#txtFinalBillAmount_pm ").val());
							$("#txtBalanceReturn_pm ").val(" ");
							// $("#codChk").hide();
							// $("#OnlineChk").hide();
							$("#Text13_pm").show();
							$("#ddlType_pm").show();
							$("#ddlBank_pm").show();
							$("#crcredit_pm").show();
							$("#Type_pm").show();
							$("#Bank_pm").show();
							$("#lblCashHeading_pm").hide();
							$("#txtCashReceived_pm").hide();
							$("#rdoCOD_pm").hide();
							$("#rdoOnline_pm").hide();
							$("#lblCashHeading_pm").hide();
							$("#txtCashReceived_pm").hide();
							$("#ddlcustName_pm").hide();
						}

						else if ($(this).val() == "Cash") {

							$("#ddlcustName_pm").hide();
							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId_pm").val(0);
							$("#lblCreditCustomerName_pm").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers_pm").html("");
							$("#txtddlCreditCustomers_pm").val("");
							$("#Coupans_pm").hide();
							$("#OP_pm").hide();
							$("#lblCashHeading_pm").text("Cash Rec:");
							$("#creditCustomer_pm").css("display", "none");
							$("#coupan_pm").hide();
							$("#dvOuter_ddlCreditCustomers_pm").hide();
							$("#ddlChosseCredit_pm").hide();
							$("#ddlType_pm").prop("disabled", true);
							$("#ddlOtherPayment_pm").hide();
							$("#COD_pm").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();
							$("#Online_pm").hide();
							$("#txtpaymentAmount_pm").hide();
							$("#rdoCOD_pm").hide();
							$("#Td1_pm").hide();
							$("#txtCODAmount_pm").hide();
							//                   //$("#ddlOtherPayment").hide();
							$("#holder_pm input").hide();
							$("#holder2_pm input").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();

							$("#rdoOnline_pm").hide();
							//$("#txtCashReceived_pm").val("0").prop("disabled", false);
							$("#txtCashReceived_pm").removeAttr("disabled");
							$("#Text13_pm").val("0").prop("readonly", true);
							$("#Text14_pm").val("").prop("readonly", true);
							$("#Text15_pm").val("0").prop("readonly", true);
							$("#Text16_pm").val("").prop("readonly", true);
							$("#ddlType_pm").prop("disabled", true);
							$("#ddlBank_pm").prop("disabled", true);
							$("#txtCashReceived_pm").val($("#txtFinalBillAmount_pm").val());
							$("#txtBalanceReturn_pm").val(" ");
							//  $("#codChk").hide();
							// $("#OnlineChk").hide();
							$("#Text13_pm").hide();
							$("#ddlType_pm").hide();
							$("#ddlBank_pm").hide();
							$("#crcredit_pm").hide();
							$("#Type_pm").hide();
							$("#Bank_pm").hide();
							$("#txtCashReceived_pm").show();
							$("#lblCashHeading_pm").show();
							$("#rdoCOD_pm").hide();
							$("#rdoOnline_pm").hide();

						}
						else if ($(this).val() == "OnlinePayment") {

							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId_pm").val(0);
							$("#lblCreditCustomerName_pm").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers_pm").html("");
							$("#txtddlCreditCustomers_pm").val("");
							$("#ddlcustName_pm").hide();
							$("#lblCashHeading").text("Cash Rec:");
							$("#creditCustomer").css("display", "none");
							$("#coupan_pm").hide();
							$("#dvOuter_ddlCreditCustomers_pm").hide();
							$("#ddlChosseCredit_pm").hide();
							$("#COD_pm").show();
							$("#Online_pm").show();
							$("#No_pm").show();
							$("#Amt_pm").show();
							$("#ddlOtherPayment_pm").show();
							$("#txtpaymentAmount_pm").show();
							//                            $("#rdoOnline").show();
							//                               $("#rdoCOD").show();
							if ($("#txtCODAmount_pm").val() != 0) {
								onlineoption = "COD"


							}
							//$("#Coupans_pm").val("0").prop("readonly", true);
							$("#Td1_pm").show();
							$("#txtCODAmount_pm").show();
							$("#holder2_pm").hide();
							$("#holder_pm").hide();
							$("#No_pm").hide();
							$("#Amt_pm").hide();


							$("#Coupans_pm").show();
							$("#coupan_pm").show();
							$("#OP_pm").show();
							$("#txtCashReceived_pm").val("0").prop("readonly", true);
							$("#Text13_pm").val("0").prop("readonly", true);
							$("#Text14_pm").val("").prop("readonly", true);
							$("#Text15_pm").val("0").prop("readonly", false);
							$("#Text16_pm").val("").prop("readonly", false);
							$("#ddlType_pm").prop("disabled", true);
							$("#ddlBank_pm").prop("disabled", true);
							$("#txtpaymentAmount_pm").val($("#txtFinalBillAmount_pm").val());
							$("#codChk").show();
							$("#OnlineChk").show();
							// $("#codChk").hide();
							// $("#OnlineChk").hide();
							$("#Text13_pm").hide();
							$("#ddlType_pm").hide();
							$("#ddlBank_pm").hide();
							$("#crcredit_pm").hide();
							$("#Type_pm").hide();
							$("#Bank_pm").hide();
							$("#lblCashHeading_pm").hide();
							$("#txtCashReceived_pm").hide();
							$("#rdoCOD_pm").show();
							$("#rdoOnline_pm").show();
							$("#rdoOnline_pm").prop('checked', true);
							//$("#txtpaymentAmount").val($("#txtFinalBillAmount_pm").val());

						}

					}
				);
                /////////////////////////////////

				function GetLastBill() {

					$.ajax({
						type: "POST",
						data: '{ }',
						url: "BillScreen.aspx/GetLastBill",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);
							$("#billnumb").html(obj.lastrecord.BillNowPrefix);

							$("#billamt").html(obj.lastrecord.Net_Amount);

							
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {

							$.uiUnlock();

						}

					});

				}

      //       function GetLastBill() {

      //           $.ajax({
      //               type: "POST",
      //               data: '{ }',
					 //url: "BillScreenOption.aspx/GetLastBill",
      //               contentType: "application/json",
      //               dataType: "json",
      //               success: function (msg) {

      //                   var obj = jQuery.parseJSON(msg.d);
      //                   $("#billnumb").html(obj.bill_no);
      //                   $("#billamt").html(obj.net_amt);
                  

      //               },
      //               error: function (xhr, ajaxOptions, thrownError) {

      //                   var obj = jQuery.parseJSON(xhr.responseText);
      //                   alert(obj.Message);
      //               },
      //               complete: function () {

                     

      //               }

      //           });

      //       }
             function updateGSTBill() {

                 var LocalOut = "Local"
                 if ($("#rdblocal").prop("checked") == true) {
                     LocalOut = "Local";
                 }
                 else {
                     LocalOut = "Out";
                 }

                 var BillType = false;
                 if ($('#chkGst').prop('checked') == true) {
                     BillType = true;
                 }

                 var CshCustSelId = $("#ddlMobSearchBox1 option:selected").val();
                 var CshCustSelName = $("#ddlMobSearchBox1 option:selected").attr("name");

                 $.ajax({
                     type: "POST",
                     data: '{"LocalOut": "' + LocalOut + '","BillNowPrefix": "' + m_BillNowPrefix + '","BillType": "' + BillType + '",CustomerId : "' + CshCustSelId + '",CustomerName : "' + CshCustSelName + '"}',
					 url: "BillScreenOption.aspx/UpdateGSTBill",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);
                         alert("Data Saved Successfully");


                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {
                         ResetBillCntrols();
                         m_BillNowPrefix = 0;
                         $("#GSTDialog").dialog('close');
                         BindGrid();

                         $.uiUnlock();

                     }

                 });


             }

				


             $("#Txtsrchcredit").keypress(function (e) {


                 if (e.keyCode == 13)
                 {

                     $("#btncreditsrch").click();
                 }

             });

             $("#btnsa").click(
           function () {


               updateGSTBill();


           }
           );


             $("#DelCharges").hide();

             $("#ddlChosseCredit").change(
                 function () {


                     if ($("#ddlChosseCredit").val() != "0") {

                         if ($("#lblCreditCustomerName").text() == "") {

                             CrdCustSelId = $("#ddlChosseCredit").val();
                             $("#hdnCreditCustomerId").val(CrdCustSelId);
                             CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                             $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())

                             $("#creditCustomer").css("display", "block");
                         }
                     }




                 });

                var count = 0;
                $(window).keydown(function (e) {
                    if (e.keyCode == 112) {
                        
                        return false;
                    }
                    if (e.keyCode == 27) {
						
						
						$("#ItemGrid1").dialog("close");
						event.stopPropagation();
						isfirst = 1;
						isupfirst = 1;
						selrowf = true;

                        
                    }

                });
                var billt = "";
                
             $(window).keydown(function (e) {

                 switch (e.keyCode) {


                    // case 112:   // F1 key is left or up
                         //count = count + 1

                         //if (count == 1) {

                         //    CashSave();
                         //    count = 0;
                         //}

                         //else {

                         //    alert("Bill Is Under Process Already...");
                         //    return;

                         //}

                         //return false;

                     case 107:
                         
                         
							 count = count + 1

                             if (count == 1) {
                                 
								 CashSave();
                                
                                 ClearData();
								 count = 0;
                                
							 }

							 else {

								 alert("Bill Is Under Process Already...");
								 return;

							 }

                         

						
						 return false;
                     case 113: //F2 key is left or down
						 count = count + 1;
                         if (count == 1) {
                             
                                 $("#btnOnline").click();
							
                             count = 0;
							
						 }
						 else {

							 alert("Bill Is Under Process Already...");
							 return;

						 }

                         return false;
                       //  return false; //"return false" will avoid further events
                     case 114: //F3 key is left or down
						
                         return false;
                     case 115: //F4 key is left or down
						 count = count + 1;
						 if (count == 1) {
                             CreditCardSave();
							
							 count = 0;
						 }
						 else {

							 alert("Bill Is Under Process Already...");
							 return;

						 }

                         return false;
                     case 117: //F6 key is left or down
                         ReturnItem();

                         return false;
                     case 118: //F7 key is left or down
						

                         return false;
                     case 119: //F8 key is left or down

						

                         return false;

                     case 122: //F11 key is left or down
                         BillUnhold();
                         return false;

                     case 123: //F12 key is left or down
                         InsertHoldBill();
                         return false;

                     case 9://TAB
                         is_tab = 1;

                     case 116: // F5 key
                         ClearData();
						
                         return false;
                     case 13:
                        
                         if (billt == "CreditCard") {
							
                             $("#btnCreditCardBilling").click();
							 ClearData();
                             billt = "";
                         }
                         if (billt == "Online") {
							
                             $("#btnOnlineBilling").click();
							 ClearData();
                             billt = "";
						 }
                         
                    
                 }
                 return; //using "return" other attached events will execute
             });


             $("#chkDelivery").change(function () {

                 if ($('#chkDelivery').prop('checked') == true) {
                     $("#DelCharges").show();
                 }
                 else {

                     $("#DelCharges").hide();
                 }

             });



             $("#btnMsgClose").click(
            function () {
                $("#dvbillSave").css("display", "none");
                RestControls();
            }
            )
             ;

             $("#btn_add_newcst").click(function () {
                 $("#CustomerDialog").dialog({
                     autoOpen: true,

                     width: 800,
                     resizable: false,
                     modal: true
                 });
                 $("#CustomerDialog").parent().addClass('custdia-pop');
             });
             $("#btnReprint").click(
             function () {

                 var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
                 if ($.trim(SelectedRow) == "") {
                     alert("No Bill is selected to Reprint");
                     return;
                 }

                 m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
                     if (BillOption == "Direct") {
                         var iframe = document.getElementById('reportout');

                         //                if (iframe != null) {
                         //                    document.body.removeChild(iframe);
                         //                } 

                         iframe = document.createElement("iframe");
                         iframe.setAttribute("id", "reportout");
                         iframe.style.width = 0 + "px";
                         iframe.style.height = 0 + "px";
                         document.body.appendChild(iframe);

                         window.location = 'BillReport.aspx?BillNowPrefix=' + m_BillNowPrefix;

                     }
                     else {
                         
                                    $.uiLock('');
                                    var PrintType = "Reprint";

                                    $.ajax({
                                        type: "POST",
                                        data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
                         url: "BillScreenOption.aspx/Reprint",
                                        contentType: "application/json",
                                        dataType: "json",
                                        success: function (msg) {

                                            var obj = jQuery.parseJSON(msg.d);


                                        },
                                        error: function (xhr, ajaxOptions, thrownError) {

                                            var obj = jQuery.parseJSON(xhr.responseText);
                                            alert(obj.Message);
                                        },
                                        complete: function () {

                                            $.uiUnlock();

                                        }

                                    });
                     }





             }
             );



             $("#colDvProductList").mouseenter(
             function () {

                 $("#colProducts").slideUp(200);
             }

             );

             $("#ddlCreditCustomers").val("").removeAttr("disabled");
             $("#ddlCreditCustomers").css({ "display": "none" });

             $("#ddlChosseCredit").css({ "display": "none" });
             $("#ddlChosseCredit").val(0);

             $.ajax({
                 type: "POST",
                 data: '{ }',
				 url: "BillScreenOption.aspx/GetAllBillSetting",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     BillOption = obj.setttingData.BillOption;
                    
                     BillBasicType = obj.setttingData.retail_bill;
                     TakeAwayDine = obj.setttingData.TakeAwayDine;
                     AllowServicetax = obj.setttingData.ServiceTax;
                     Takeaway = obj.setttingData.AlloServicetax_TakeAway;
                     HomeDelCharges = obj.setttingData.homedel_charges;
                     minbillvalue = obj.setttingData.min_bill_value;
                     DeliveryCharges = obj.setttingData.del_charges;
                     EnableCashCustomer = obj.setttingData.CashCustomer;
                     DefaultBank = obj.setttingData.defaultBankID;
					 DefaultBankName = obj.setttingData.defaultbankName; 
                     RoundBillAmount = obj.setttingData.roundamt;
					 PrintRoute = obj.setttingData.PrintRoute;
                     PrintShortName = obj.setttingData.shortname;
					 OpenDiv = obj.setttingData.OpenDiv;
                     FocAffect = obj.setttingData.focaffect;
                     NEgativeStock = obj.setttingData.NegtiveStock;

                     DefaultPaymode = obj.setttingData.defaultpaymodeID;
                     PriceEnable = obj.setttingData.EnablePrice;
                     MRPEnable = obj.setttingData.EnableMRP;
                     
                     AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;

                     if (AllowDiscountOnBilling == 1) {
                         EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                         EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                         DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                         BackEndDiscount = obj.setttingData.Back_End_Discount;

                     }


                     if (EnableCashCustomer == 1) {

                         $("#btnCash").removeAttr("disabled")
                         //$("#txtMobSearchBox,#imgSrchCashCust").show();
                         $("#txtddlMobSearchBox").removeAttr("disabled");

                     }
                     else {
                         $("#btnCash").prop("disabled", true).css("background", "gray");
                         //$("#txtMobSearchBox,#imgSrchCashCust").hide();

                         $("#txtddlMobSearchBox").attr("disabled", "disabled");
                     }


                     if (BillBasicType == "I") {

                         $("#vatIncOrExc").hide();
                     }
                     else {
                         $("#vatIncOrExc").show();
                     }

                    // $("#txtddlMobSearchBox").attr("disabled", "disabled");
                     if (HomeDelCharges == 1) {

                         if (minbillvalue == 0 || DeliveryCharges == 0) {
                             $("#dvDeliveryChrg").removeAttr("disabled");
                         }
                         else {
                             $("#dvDeliveryChrg").attr("disabled", "disabled");
                         }

                     }
                     else {

                     }

                     if (DiscountOnBillValue == 1) {

                         DiscountValues = obj.DiscountDetail;



                     }

                     if (EnableDiscountAmount == 1) {

                         $("#dvdisper").removeAttr('disabled');
                         $("#dvdiscount").removeAttr('disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }
                     else {

                         $("#dvdisper").attr('disabled', 'disabled');
                         $("#dvdiscount").attr('disabled', 'disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {



                 }

             });

				

             ValidateRoles();

             function ValidateRoles() {

                 var arrRole = [];
                 arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                 for (var i = 0; i < arrRole.length; i++) {

                     if (arrRole[i] == "18") {
                         // RoleForEditRate = true;


                     }
                     else if (arrRole[i] == "9") {
                         $("#btnShowScreen").click(
                             function () {


                                 if ($("#<%=hdnScreen.ClientID%>").val() == 2) {

                                     window.location = "managekotscreen.aspx";
                                 }
                                 else {
                                     $("#<%=dd_customername.ClientID%>").val(0);
                                     $('#<%=dd_customername.ClientID %>').removeAttr("disabled");

                                     clickmode = "New";
                                     RestControls();
                                     m_BillNowPrefix = "";
                                     m_BillMode = "";
                                     $("#ddlbillttype").removeAttr("disabled");
                                     $("#btnCreditCard").css("background", "#f06671");
                                     $("#ddlCreditCustomers").html("");
                                     $("#txtddlCreditCustomers").val("");

                                     $("#btnCash").css("background", "#f06671");
                                     $("#btnhold").css("background", "#f06671");

                                     $("#screenDialog").dialog({
                                         autoOpen: true,

                                         width: 1250,
                                         resizable: false,
                                         closeOnEscape: false,
                                         modal: false
                                     });

                                     GetLastBill();
                                     //$.ajax({
                                     //    type: "POST",
                                     //    data: '{ }',
                                     //    url: "BillScreenOption.aspx/GetLastBill",
                                     //    contentType: "application/json",
                                     //    dataType: "json",
                                     //    success: function (msg) {

                                     //        var obj = jQuery.parseJSON(msg.d);
                                     //        $("#billnumb").html(obj.bill_no);
                                     //        $("#billamt").html(obj.net_amt);


                                     //    },
                                     //    error: function (xhr, ajaxOptions, thrownError) {

                                     //        var obj = jQuery.parseJSON(xhr.responseText);
                                     //        alert(obj.Message);
                                     //    },
                                     //    complete: function () {



                                     //    }

                                     //});

                                 }

                             });
                     

                     }

                 

                     else if (arrRole[i] == "2") {
                         $("#btnDelete").show();

                         $("#btnDelete").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');

            if ($.trim(SelectedRow) == "") {
                alert("No Bill is selected to Cancel");
                return;
            }

            var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
            if (confirm("Are You sure to delete this record")) {
                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
					url: "BillScreenOption.aspx/Delete",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        BindGrid();
                        alert("Bill is Canceled successfully.");

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $.uiUnlock();
                    }
                });

            }


        }
        );

                     }




                     else if (arrRole[i] == "3") {
                         $("#btnEdit").show();

                         $("#btnEdit").click(function () {
                                 $('#<%=dd_customername.ClientID %>').attr("disabled", "disabled");
           RestControls();
           clickmode = "Edit"
           var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Product is selected to add");
               return;
           }
           CshCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code');
           CshCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Name');
           CrdCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_ID');
           CrdCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name');

           m_BillMode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillMode');


           $("#btnhold").css("background", "gray");




           if (CshCustSelId != 0) {

               $("#ddlMobSearchBox").html("<option value='" + CshCustSelId + "'>" + CshCustSelName + "</option>");
               $("#txtddlMobSearchBox").val(CshCustSelName);
               $("#CashCustomer").show();
               $("#lblCashCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCustAddress'));
           }


           $("#ddlbillttype").prop("disabled", true);


           if (m_BillMode == "Credit") {

               $("#btnCash").css("background", "gray");
               $("#btnCreditCard").css("background", "gray");

               $("#lblCreditCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CreditCustAddress'));

           }
           else if (m_BillMode == "CreditCard") {
               $("#btnCash").css("background", "gray");
               $("#btnCreditCard").css("background", "#f06671");

           }
           else if (m_BillMode == "Cash") {
               $("#btnCash").css("background", "#f06671");
               $("#btnCreditCard").css("background", "gray");
           }

           var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');
           option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');
           getdel_cust_id = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'cst_id');
           $("#<%=dd_customername.ClientID%>").val(getdel_cust_id);



           $("#<%=ddloption.ClientID%>").val(option);
           if (option == "Dine") {
               $("#ddlTable").removeAttr("disabled");

               $("#ddlTable option").removeAttr("selected");

               $('#ddlTable option[value=' + TableNo + ']').prop('selected', 'selected');
           }



           $("#dvdisper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));




           EditBill(m_BillNowPrefix);
           $("#dvsertaxper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'servalue'));

           $("#screenDialog").dialog({
               autoOpen: true,

               width: 800,
               resizable: false,
               modal: false
           });

       }

        );

   }
   else if (arrRole[i] == "10") {
       $("#btnhold").click(
 function () {

     InsertHoldBill();
 });

   }



}

}

             $("#ddlTable").prop("disabled", true);
             BindTables();
             GetDiscountType();

             $("#btnGetByItemCode").click(
             function () {


                 var ItemCode = $("#txtItemCode");

                 if (ItemCode.val().trim() == "") {
                     ItemCode.focus();
                     return;
                 }


                 GetByItemCode(ItemCode.val());

             }

             );




                $("#ddlMobSearchBox").supersearch({
                 Type: "CashCustomer",
                 Caption: "Please enter Customer Name/Mobile ",
                 AccountType: "D",
                 Width: 120,
                 DefaultValue: 0,
                 Godown: 0
             });

		     $("#ddlItemSearchBox").supersearch({
				 Type: "Product",
				 Caption: "Please enter Item Code/Name ",
				 AccountType: "",
				 Width: 214,
				 DefaultValue: 0,
				 Godown: 0
				});


             $("#ddlMobSearchBox1").supersearch({
                 Type: "CashCustomer",
                 Caption: "CustName/Mobile ",
                 AccountType: "D",
                 Width: 50,
                 DefaultValue: 0,
                 Godown: 0
             });


             $("#ddlCreditCustomers").supersearch({
                 Type: "Accounts",
                 Caption: "Please enter Customer Name/Code ",
                 AccountType: "D",
                 Width: 100,
                 DefaultValue: 0,
                 Godown: 0
             });




             $("#dvdisper").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         if (value > 100) {
             $(this).val(100);

         }

         BindtrDiscount($(this).val());

         CommonCalculation();

     }
     );

             $("#dvdiscount").keyup(
           function () {
               var regex = /^[0-9]*$/;


               var value = jQuery.trim($(this).val());

               if (value.length >= 1) {
                   if (!regex.test(value) || value <= 0) {

                       $(this).val(0);


                   }
               }

               var ttlAmt = $("#dvsbtotal").html();

               if (value > Number(ttlAmt)) {
                   $(this).val(ttlAmt);

               }


               $("#dvdisper").val(($("#dvdiscount").val() * 100 / $("#dvsbtotal").html()).toFixed(2));
               CommonCalculation();

           }
           );



             $("#dvDeliveryChrg").keyup(
            function () {
                var regex = /^[0-9]*$/;


                var value = jQuery.trim($(this).val());

                if (value.length >= 1) {
                    if (!regex.test(value) || value <= 0) {

                        $(this).val(0);


                    }
                }

                CommonCalculation();

            }
            );

               

             $("#btnCash").click(
           function () {
               $("#btnCash").attr('disabled', 'disabled');
               CashSave();

           });


                function CashSave() {
                    if (etrType != "Search") {
                        if (m_BillMode == "Credit" || m_BillMode == "CreditCard") {
                            //alert("Bill Mode is "+m_BillMode+" and cannot be changed to Cash.");
                            //return;
                        }

                        var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
                        var CustomerId = "CASH";
                        var CustomerName = "CASH";
                        var billmode = "Cash";
                        var NetAmt = $("#dvnetAmount").html();
                        var CreditBank = "";
                        var CashAmt = $("#dvnetAmount").html();
                        var creditAmt = 0;
                        var CreditCardAmt = 0;
                        var cashcustcode = 0;
                        var cashcustName = "";
                        if (CshCustSelId == "0") {
                            cashcustcode = 0;
                            cashcustName = "CASH"
                        }
                        else {
                            cashcustcode = CshCustSelId;
                            cashcustName = CshCustSelName;
                            CustomerId = CshCustSelId;
                            CustomerName = CshCustSelName;
                        }
                        var OnlinePayment = 0
                        InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
                        $("#btnCash").removeAttr('disabled');
                    }
             }

             $("#btnClear").click(
             function () {

                 ClearData();


             });


             function ClearData() {
                 m_BillNowPrefix = "";
                 m_BillMode = "";
                 $("#ddlbillttype").removeAttr("disabled");
                 $("#btnCreditCard").css("background", "#f06671");
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");
                 $("#ddlChosseCredit").val(0);
                 $("#btnCash").css("background", "#f06671");
                 $("#btnhold").css("background", "#f06671");
                 $("#lblNoItems").html(" ");
                 $("#<%=dd_customername.ClientID%>").val(0);
                 RestControls();
             }



             $("#btnOk").click(
         function () {
             var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
             var CustomerId = 0;
             var CustomerName = "";
             CustomerId = $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CCODE');
             CustomerName = $('#jQGridDemoCredit').jqGrid('getCell', SelectedRow, 'CNAME');
             //if (CrdCustSelId == "0") {
             //    CustomerId = 0;
             //    CustomerName = "";
             //}
       
             //else {
                
             //}
             var billmode = "Credit";
             var NetAmt = $("#dvnetAmount").html();
             var CreditBank = "";
             var CashAmt = 0
             var creditAmt = $("#dvnetAmount").html();
             var CreditCardAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";


             var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "";
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }
             var OnlinePayment = 0;
             InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
             $("#dvCreditCustomerSearch").dialog('close')
         });



             $("#btnCredit").click(
            function () {


                if (ProductCollection.length == 0) {
                    alert("First Choose Products for billing");
                    return;
                }



                billingmode = "direct";
                $("#dvCreditCustomerSearch").dialog({

                    model: true,
                  
                    width: 572

                });
                $("#dvCreditCustomerSearch").parent().addClass('dvcrecustser-pop');
                $("#Txtsrchcredit").focus();
            });

             $("#btnUnHold").click(
            function () {


                BillUnhold();



            });

             function BillUnhold() {
                 $.ajax({
                     type: "POST",
                     data: '{}',
                     url: "screen.aspx/GetHoldList",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);

                         $("#tbHoldList").html(obj.holdList);

                         //$("#CustomerSearchWindow").hide();
                         //$("#dvProductList").hide();
                         //$("#dvBillWindow").hide();

                         //$("#dvCreditCustomerSearch").hide();
                         $("#dvHoldList").dialog({

                             model: true,
                             width:500

                         });
                         $("#dvHoldList").parent().addClass('dvholdlist-pop');
                         //$("#dvOrderList").hide();

                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {

                     }

                 }
              );

             }


             $("#btnOrdersList").click(
               function () {


                   $.ajax({
                       type: "POST",
                       data: '{}',
                       url: "screen.aspx/GetOrdersList",
                       contentType: "application/json",
                       dataType: "json",
                       success: function (msg) {

                           var obj = jQuery.parseJSON(msg.d);

                           $("#tbOrderList").html(obj.OrderList);

                           $("#CustomerSearchWindow").hide();
                           $("#dvProductList").hide();
                           $("#dvBillWindow").hide();

                           $("#dvCreditCustomerSearch").hide();
                           $("#dvHoldList").hide();
                           $("#dvOrderList").show();
                           $("#btnreturn").attr('disabled', true);
                           $("#btnsavebill").attr('disabled', true);



                           $("#btnUnHold").attr('disabled', true);
                           $("#btnCash").attr('disabled', true);
                           $("#btnCredit").attr('disabled', true);


                       },
                       error: function (xhr, ajaxOptions, thrownError) {

                           var obj = jQuery.parseJSON(xhr.responseText);
                           alert(obj.Message);
                       },
                       complete: function () {
                           $("#btnhold").attr('disabled', 'disabled');
                       }

                   }
             );

               });







             $("#btnreturn").click(
                function () {


                    ReturnItem();


                    //count = count+1;
                }
                );



             function ReturnItem() {

                 if (modeRet == "Return") {
                     $("#btnreturn").css("background", "#f06671");

                     modeRet = "";
                 }
                 else {

                     $("#btnreturn").css("background", "red");

                     modeRet = "Return";

                 }
             }

             //      $("#txtMobSearchBox").keyup(
             //            function (event) {

             //             var keycode=(event.keyCode ? event.keyCode: event.which);
             // 
             //      if(keycode=='13')
             //      {
             //            alert("Hello");
             //      }


             //            });


             $("#dvdisper").val("0");
             $("#dvdiscount").val("0");

             $("#btnSearch").click(
           function () {

               var Keyword = $("#txtSearch");
               if (Keyword.val().trim() != "") {
                   $("#colProducts").slideDown(200);
                   Search(0, Keyword.val());
               }
               else {
                   Keyword.focus();
               }
               $("#txtSearch").val("");

           });



             $("#txtSearch").keyup(
             function (event) {

                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {


                     var Keyword = $("#txtSearch");
                     if (Keyword.val().trim() != "") {
                         $("#colProducts").slideDown(200);
                         Search(0, Keyword.val());
                         $("#txtSearch").val("");
                     }
                     else {
                         Keyword.focus();
                     }


                 }


             }

             );




             $(document.body).on('keyup', '.tbItemCode', function (event) {



                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {



                     if ($(this).val().trim() != "") {

                         GetByItemCode($(this).val());
                         $(this).val("").focus();
                     }
                     else {
                         $(this).focus().val("");
                     }

                     if ($("#dvdisper").val() != "0") {

                         BindtrDiscount($("#dvdisper").val());
                     }
                     $("#txtItemCode").val("").focus();
                     $(".product_table").animate({ scrollTop: $(document).height() }, 1000);

                 }
         

             }

             );




             $("#btnBillWindowClose").click(
             function () {

                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 $("#dvBillWindow").hide();

                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();
                 // $("#dvBillWindow").dialog('close');


             }

                );

                $("#btnClose").click(
                    function () {
						$("#screenDialog").dialog('close');


                    });

             $("#dvGetCreditCustomers,#dvGetCreditCustomersMember,#dvGetCreditCustomersPackage").click(
            function () {


                var DataGrid = jQuery('#jQGridDemo1');
                DataGrid.jqGrid('setGridWidth', '680');

                jQuery('#jQGridDemo1').GridUnload();


                $("#CustomerSearchWindow").hide();
                $("#dvProductList").hide();
                $("#dvBillWindow").hide();
                $("#dvCreditCustomerSearch").show();
                $("#dvHoldList").hide();

                $("#dvOrderList").hide();

                //            $('#dvCreditCustomerSearch').dialog(
                //        {
                //            autoOpen: false,

                //            width: 720,
                //            resizable: false,
                //            modal: false

                //        });

                // 
                //            linkObj = $(this);
                //            var dialogDiv = $('#dvCreditCustomerSearch');
                //            dialogDiv.dialog("option", "position", [233, 48]);
                //            dialogDiv.dialog('open');
                //            return false;


            });





             $("#ddlbillttype").change(
         function () {



             if ($(this).val() == "Credit") {


                 $("#dvOuter_ddlCreditCustomers").show();
                 $("#lblCashHeading").text("Receipt Amt:");
                 $("#ddlbillttype option[value='Credit']").prop("selected", true);
                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", true);
                 $("#Text14").val("").prop("readonly", true);
                 $("#Text15").val("0").prop("readonly", true);
                 $("#Text16").val("").prop("readonly", true);
                 $("#ddlType").prop("disabled", true);
                 $("#ddlBank").prop("disabled", true);
                 $("#ddlChosseCredit").show();

             }
             else if ($(this).val() == "CreditCard") {
                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();


                 $("#txtCashReceived").val("0").prop("readonly", true);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);

             }
             else if ($(this).val() == "Cash") {


                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);


             }
             else if ($(this).val() == "OnlinePayment") {

                 var customerId = 0;
                 CrdCustSelId = 0;
                 CrdCustSelName = "";
                 $("#hdnCreditCustomerId").val(0);
                 $("#lblCreditCustomerName").text("");
                 CrdCustSelName = "";
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");

                 $("#lblCashHeading").text("Cash Rec:");
                 $("#creditCustomer").css("display", "none");

                 $("#dvOuter_ddlCreditCustomers").hide();
                 $("#ddlChosseCredit").hide();



                 $("#txtCashReceived").val("0").prop("readonly", false);
                 $("#Text13").val("0").prop("readonly", false);
                 $("#Text14").val("").prop("readonly", false);
                 $("#Text15").val("0").prop("readonly", false);
                 $("#Text16").val("").prop("readonly", false);
                 $("#ddlType").prop("disabled", false);
                 $("#ddlBank").prop("disabled", false);
             }

         }
         );




             option = $("#<%=ddloption.ClientID%>").val();

             $("#<%=ddloption.ClientID%>").change(
                   function () {
                       option = $("#<%=ddloption.ClientID%>").val();
                      if (option == "TakeAway") {

                          $("#ddlTable").prop("disabled", true);
                          $("#ddlTable option").removeAttr("selected");
                      }
                      else {
                          $("#ddlTable").prop("disabled", false).focus();
                          // alert("Choose Table No");
                      }
                      Bindtr();

                  }
       );




             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindCategories",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                     $("#categories").html(obj.categoryData);
                     if (AllowServicetax == 1) {
                         Sertax = obj.setttingData.SerTax;
                     }
                     else {
                         Sertax = 0;
                     }
                     //  Takeaway = obj.setttingData.TakeAway;
                     Takeawaydefault = obj.setttingData.TakeAwayDefault;

                     var CatId = obj.CategoryId;

                     Search(CatId, "");
                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
         );



             $("#btnBillWindowOk").click(
     function () {

         $("#btnBillWindowOk").attr('disabled', 'disabled');

         var billtype = $("#ddlbillttype").val();
         if (billtype == "") {
             alert("Select BillType First");
             $("#ddlbillttype").focus();
             return;

         }


         if (billtype == "Credit")  {

             if (CrdCustSelId == 0) {

                 alert("Please select Credit Customer");
                 $("#txtddlCreditCustomers").focus();
                 $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }


         }


         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');




         DEVBalanceCalculation();



         var txtcreditcardcheck = $("#Text13").val();

         if (txtcreditcardcheck != "0") {
             if ($("#ddlType").val() == "") {
                 $.uiUnlock();
                 alert("Please Select Credit Card Type");
                 $("#ddlType").focus();
                 $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             //if ($("#Text14").val() == "") {
             //    $.uiUnlock();
             //    alert("Please Enter Credit Card No");
             //    $("#Text14").focus();
             //    return;
             //}

         }

         var txtchequecheck = $("#Text15").val();
         if (txtchequecheck != "0") {
             if ($("#ddlBank").val() == "") {
                 $.uiUnlock();
                 alert("Please Select Bank");
                 $("#ddlBank").focus();
                 $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             if ($("#Text16").val() == "") {
                 $.uiUnlock();
                 alert("Please Enter Cheque No");
                 $("#Text16").focus();
                 $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
         }


         var cashamount = $("#txtCashReceived").val();


         if (billtype == "Cash" || billtype == "CreditCard") {
             if (Number($("#txtBalanceReturn").val()) < 0) {
                 $.uiUnlock();
                 alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                 $("#btnBillWindowOk").removeAttr('disabled');
                 return;
             }
             else {
                 cashamount = cashamount - Number($("#txtBalanceReturn").val());
             }

         }

         if (Number(cashamount) < 0) {
             $.uiUnlock();
             alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
             $("#btnBillWindowOk").removeAttr('disabled');
             return;
         }


         if (billtype == "CreditCard") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = $("#ddlBank option:selected").text();
             var CustomerName = $("#ddlBank option:selected").text();
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt = 0
             var creditAmt = 0;
             var OnlinePayment = 0;
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }
         if (billtype == "Cash") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt = $("#txtCashReceived").val();
             var creditAmt = 0;
             var OnlinePayment = 0;
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }


         if (billtype == "OnlinePayment") {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "CASH";
             var CustomerName = "CASH";
             var billmode = billtype;
             var NetAmt = $("#txtFinalBillAmount").val();

             var CashAmt = 0;
             var creditAmt = 0;
             var OnlinePayment = $("#txtFinalBillAmount").val();
             var BIllValue = $("#dvsbtotal").html();


             var CreditBank = $("#ddlBank").val();
             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }
         else if (billtype == "Credit") {

             var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
             var CustomerId = 0;
             var CustomerName = "";
             if (CrdCustSelId == "0") {
                 CustomerId = 0;
                 CustomerName = "";
             }
             else {
                 CustomerId = CrdCustSelId;
                 CustomerName = CrdCustSelName;
             }

             var NetAmt = $("#txtFinalBillAmount").val();

             var billmode = billtype;

             var CreditBank = $("#ddlBank").val();
             var OnlinePayment = 0;
             var CashAmt = $("#txtCashReceived").val();

             var BIllValue = $("#dvsbtotal").html();
             var creditAmt = 0;
             if (billmode == "Credit") {
                 creditAmt = Number(BIllValue) - Number(CashAmt);


             }

             var CreditCardAmt = $("#Text13").val();


             var cashcustcode = 0;
             var cashcustName = "";


             var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "";
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
             }

         }

         InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);

         $('#dvBillWindow').dialog('close');
       
     }
     );


				$("#txtpnno").change(

                    function () {
						var PhnNo = $("#txtpnno").val();
						$.ajax({
							type: "POST",
							data: '{"PhnNo": "' + PhnNo + '"}',
							url: "BillScreenOption.aspx/GetByMobileNo",
							contentType: "application/json",
							dataType: "json",
							success: function (msg) {

								var obj = jQuery.parseJSON(msg.d);
								var Discount = obj.CustomerData.Discount;
								$("#lblCashCustomerName").text(obj.CustomerData.Customer_Name + "," + obj.CustomerData.Contact_No + " , " + obj.CustomerData.Address);

                                CshCustSelId = obj.CustomerData.Customer_ID;
								CshCustSelName = obj.CustomerData.Customer_Name;

								$("#CashCustomer").css("display", "block");

								if (EnableCustomerDiscount == 1) {
									$("#dvdisper").val(Discount);

								}
								



							},
							error: function (xhr, ajaxOptions, thrownError) {

								var obj = jQuery.parseJSON(xhr.responseText);
								alert(obj.Message);
							},
							complete: function () {

							}

						}
						);





						

                    });




             $("#txtCashReceived").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }


         DEVBalanceCalculation();

     }
     );



             $("#Text13").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }


                 DEVBalanceCalculation();

             }
             );
             $("#Text15").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }

                 DEVBalanceCalculation();

             }
             );

             function ResetBillCntrols() {

                 $("#txtFinalBillAmount").val("");
                 $("#ddlbillttype").val("");
                 $("#txtCashReceived").val("0");
                 $("#Text13").val("0");
                 $("#Text15").val("0");
                 $("#Text14").val("");
                 $("#ddlType").val("");
                 $("#ddlBank").val("");

                 $("#Text16").val("");
                 $("#txtBalanceReturn").val("0");
                 modeRet = "";
                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 $("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();



                 $("#hdnCreditCustomerId").val("0");
                 $("#ddlbillttype option[value='" + DefaultPaymode + "']").prop("selected", true);
                 $("#creditCustomer").css("display", "none");


                 if (m_BillNowPrefix != "") {

                     if (m_BillMode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (m_BillMode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (m_BillMode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }
                 }

                 else {


                     if (DefaultPaymode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (DefaultPaymode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();




                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (DefaultPaymode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }



                 }








                 if (m_BillNowPrefix != "") {
                     $("#ddlbillttype option[value='" + m_BillMode + "']").prop("selected", true);
                 }
                 if (m_BillMode == "Credit") {

                     $("#ddlCreditCustomers").html("<option value='" + CrdCustSelId + "'>" + CrdCustSelName + "</option>");
                     $("#txtddlCreditCustomers").val(CrdCustSelName);
                     $("#creditCustomer").show();

                     if ($("#ddlChosseCredit").val() != "0") {
                         CrdCustSelId = $("#ddlChosseCredit").val();
                         CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                     }


                 }


             }




             $("#btnCreditCardBilling").click(
       function () {

					 var CustomerId = "";
					 var CustomerName = "";
                     var CreditBank = 0;
                    

			        CustomerId = $("#ddlCreditCardBank").val();
			        CustomerName = $("#ddlCreditCardBank option:selected").text();
				    CreditBank = $("#ddlCreditCardBank").val();
					

         //  var CustomerId = $("#ddlCreditCardBank").val()
          // var CustomerName = $("#ddlCreditCardBank option:selected").text();
           var billmode = "CreditCard";
           var NetAmt = $("#dvnetAmount").html();
          // var CreditBank = $("#ddlCreditCardBank").val()
           var CashAmt = 0;
           var creditAmt = 0;
           var CreditCardAmt = $("#dvnetAmount").html();
           var cashcustcode = 0;
           var cashcustName = "";
           if (CshCustSelId == "0") {
               cashcustcode = 0;
               cashcustName = "CASH";
           }
           else {
               cashcustcode = CshCustSelId;
               cashcustName = CshCustSelName;
           }
           var OnlinePayment = 0;
           InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
           $("#dvCreditCardWindow").dialog('close');





       });


				$("#btnOnlineBilling").click(
                    function () {
                        
						if ($("#txtCODAmount").val() == 0 && $("#txtpaymentAmount").val() == 0) {
							alert("Choose Atleast One Option Either COD or ONLINE");
							return;
						}
						var CustomerId = " ";
						var CustomerName = " ";
						var billmode = "OnlinePayment";
						var NetAmt = $("#dvnetAmount").html()


                        var CashAmt = 0;
						
						if ($("#txtCODAmount").val() != 0) {
							onlineoption = "COD"

						}
						if (onlineoption == "COD") {
							CashAmt = $("#txtCODAmount").val();
                        }
						
						var creditAmt = 0;
						
						var OnlinePayment = $("#txtpaymentAmount").val();
						var BIllValue = $("#dvsbtotal").html();


						var CreditBank = "";
						var CreditCardAmt = 0;


						var cashcustcode = 0;
						var cashcustName = "";
						if (CshCustSelId == "0") {
							cashcustcode = 0;
							cashcustName = "CASH"
						}
						else {
							cashcustcode = CshCustSelId;
							cashcustName = CshCustSelName;
						}

                        

						
							
							InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);

						
						$("#dvOnlineWindow").dialog('close');

                        ClearData();
                        billt = "";



					});


				$("#btnOnline").click(
                    function () {
                        
						$("#rdoCOD").hide();
                        if (ProductCollection.length == 0) {
                            alert("Please first Select ProductsFor Billing");
                            $.uiUnlock();

                            return false;
                        }

                        $("#btnOnline").attr('disabled', 'disabled');
                        $("#OnlineChk").prop('checked', true);

                        $("#txtpaymentAmount").val($("div[id='dvnetAmount']").html());

                        billt = "Online";
                        var cstid = $("#<%=dd_customername.ClientID %>").val();
						
                        if (cstid != 0) {

                            $("#ddlOtherPayment").val(cstid);
                            $("#ddlOtherPayment").attr("disabled","disabled");
                        }
                       
                            if (OpenDiv == 1) {
                                $("#dvOnlineWindow").dialog({

                                    modal: true



                                });
								$("#dvOnlineWindow").parent().addClass('dvonlinewin-pop');

                            }
                            else {
                                $("#btnOnlineBilling").click();
                            }
                        


                    });



             $("#btnCreditCard").click(
                 function () {
					
                 $("#btnCreditCard").attr('disabled', 'disabled');
                 CreditCardSave();
                     $("#search_item").dialog('close');
                    
                    
             }

             );



             function CreditCardSave() {
                 if (m_BillMode == "Credit" || m_BillMode == "Cash") {
                     $("#btnCreditCard").removeAttr('disabled');
                     // alert("Bill Mode is "+m_BillMode+" and cannot be changed to Credit Card.");
                     // return;
                 }
                 billt = "CreditCard";
                // CheckBankCount();
                 
                     $("#dvCreditCardWindow").dialog({
                         modal: true

                     });
                     $("#dvCreditCardWindow").parent().addClass('dvcrecardwin-pop');

                     $.ajax({
                         type: "POST",
                         data: '{}',
                         url: "screen.aspx/BindBanks",
                         contentType: "application/json",
                         dataType: "json",
                         success: function (msg) {

                             var obj = jQuery.parseJSON(msg.d);
                             $("#ddlCreditCardBank").html(obj.BankOptions);
                             $("#ddlCreditCardBank option[value='" + DefaultBank + "']").prop("selected", true);
                         }


                     });

                
                
                
             }



            



             $("#btncreditsrch").click(
       function () {
           bindGrid2();
       }
       );




             $("#btnCustomer").click(
         function () {

         

         });


             $(document).on("click", "#dvClose", function (event) {

                 var RowIndex = Number($(this).closest('tr').index());
                 var tr = $(this).closest("tr");
                 tr.remove();

                 ProductCollection.splice(RowIndex, 1);

                 if (ProductCollection.length <= 0) {

                     //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");

                         $("#<%=dd_customername.ClientID%>").val(0);
                     RestControls();



                 }
                 Bindtr();

             });


				
;
    

                $(document).on("keydown", "#tbProductInfo", function (e) {
                    var $table = $(this);
                    var $active = $('input:focus,select:focus', $table);
                    var $next = null;
                    var focusableQuery = 'input:visible,select:visible,textarea:visible';

                    var position = parseInt($active.closest('td').index()) + 1;

                    if (position == 1) {


                        position =2;
                    }

                    if (e.keyCode == 38) { // up arrow

						
                        $next = $active.closest('tr').prev().find('td:nth-child(' + position + ')').find(focusableQuery);

                        $next.focus();
                        e.preventDefault();
						//$(".BillQtycls").focus(function () { $(this).select(); });
                      
                 }
                 if (e.keyCode == 40) {
                    
                     $next = $active.closest('tr').next().find('td:nth-child(' + position + ')').find(focusableQuery);

                     $next.focus();
                     e.preventDefault();
					// $(".BillQtycls").focus(function () { $(this).select(); });
				 }
             });


				//function arrow(dir) {
				//	var activeTableRow = $('#jQGridProduct tbody tr.active')[dir]("#jQGridProduct tbody tr");
				//	if (activeTableRow.length) {
				//		$('#jQGridProduct tbody tr.active').removeClass("active");
    //                    activeTableRow.addClass('active');
                       
    //                }
					
				//};

                $(document).on("keydown", "#jQGridProduct", function (e) {


					
                    if (e.keyCode == 40) {

                        currow = 0;

                        //arrow("next");
                        
                        if (selrowf == true) {
                            
							$("#jQGridProduct").setSelection(1);
                            selrowf = false;
                            
						}
						var list = $('#jQGridProduct'),

							$td = $(e.target).closest("tr.jqgrow>td"),
							p = list.jqGrid("getGridParam"),
							//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
							cm = "Item_Code";

						var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


						var selectedRow = list.jqGrid('getGridParam', 'selrow');


                        if (isfirst == 0) {
                           
                            selectedRow = selectedRow -1;
                           
                        }

						
                        if (selectedRow == null) return;
                       
						var ids = list.getDataIDs();
                        var index = list.getInd(selectedRow);
                       
						if (ids.length < 2) return;
                        index++;
                        
						list.setSelection(ids[index - 1], false, e);
                        currow = index;
                        
						var rows = document.querySelectorAll('#jQGridProduct tr');

						var line = document.querySelector(1);



						rows[line].scrollTop({
                            behavior: 'smooth',
                            block: 'nearest'
                        });
						
      //                  var w = $(window);

      //                  var row = $('#jQGridProduct').find('tr').eq(line);
						
      //                  if (row.length) {

      //                      w.scrollTop(row.offset().top - (w.height / 2));
						//}
							e.preventDefault();
						

						
						
                    }

                    if (e.keyCode == 38) {
                       
						currow = 0;
						//arrow("prev");
						var list = $('#jQGridProduct'),

							$td = $(e.target).closest("tr.jqgrow>td"),
							p = list.jqGrid("getGridParam"),
							//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
							cm = "Item_Code";

						var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


						var selectedRow = list.jqGrid('getGridParam', 'selrow');
						
                       
						if (isupfirst == 0) {
                            selectedRow = Number(selectedRow) + Number(1);
                           
                        }
						
						if (selectedRow == null) return;
						var ids = list.getDataIDs();

                        var index = list.getInd(selectedRow);
                       
						if (ids.length < 2) return;
                        index--;
                       
						list.setSelection(ids[index - 1], false, e);
                        currow = index;
                        
                        var rows = document.querySelectorAll('#jQGridProduct tr');

						var line = document.querySelector(1);



						rows[line].scrollTop({
							behavior: 'smooth',
							block: 'nearest'
						});
						
						//var w = $(window);
						//var row = $('#jQGridProduct').find('tr').eq(line);

      //                  if (row.length) {
      //                      list.scrollTop(row.offset().top + (12/ 2));
						//}
						  e.preventDefault();
                        

                    }
                    
						if (e.ctrlKey && e.keyCode == 13)
                        {
                            
                            if (currow == 0) {
                                currow = 1;
                            }
							var rowid = currow;
							var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
							GetByItemCode(serviceId);
                            $("#ItemGrid1").dialog("close");
                            event.stopPropagation();
							isfirst = 1;
							isupfirst = 1;
							selrowf = true;
						}
                   // }

                });



             $("#btnSearch1").click(
         function () {

             bindGrid("N", "");

         }
         );

         });
		var m_cntrbill = 0;
        var m_ItemId = 0;
		
        var m_ItemCode = "";
        var m_ItemName = "";
		
        var m_Qty = 0;
        var m_Price = 0;
        var m_TaxRate = 0;
        var m_Surval = 0;
        var m_ItemDiscount = 0;
        var m_EditSaleRate = 0;
        var m_challanno = "";
        var m_Max_Retail_Price = 0;

        var ProductCollection = [];
        function clsproduct() {
            this.ItemId = 0;
            this.ItemCode = "";
            this.ItemName = "";
            
            this.Qty = 0;
            this.Price = 0;
            this.TaxCode = 0;
            this.SurVal = 0;
            this.ProductAmt = 0;
            this.Producttax = 0;
            this.ProductSurchrg = 0;
            this.TaxId = 0;
            this.ItemRemarks = "";
            this.ItemDiscount = 0;
            this.EditSaleRate = false;
            this.ChallanNo = "";
            this.Max_Retail_Price = 0;
            this.cntrbill = 0;
        }

		

        function Bindtr() {
            var cntrb = "";
            ItmQty = 0;
            var isadd = 0;
            DiscountAmt = 0;
            VatAmt = 0;
            Total = 0;
            isfocus = 0;
            var fPrice = 0;
            TotalItems = 0;
            ttlQty = 0;
            $("div[name='vat']").html("0.00");
            $("div[name='amt']").html("0.00");
            $("div[name='sur']").html("0.00");

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            // $('#tbProductInfo tr').find(".td_itemcode").remove();
            var counterId = 0;
            BEndDiscountAmt = 0;
            var Count = 0;
            var color = "";
            for (var i = 0; i < ProductCollection.length; i++) {

                if (Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]) < 0) {
                    color = "red";
                }


                var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);

                if (ProductCollection[i]["ChallanNo"] == "")
                {

                    if (RoleForEditRate == true)
                    {
						if (ProductCollection[i]["cntrbill"] == 0) {
                            var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' class='BillQtycls' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' id='txtBillQty'/></td><td><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr><tr></tr>";
                        }
                        else {
							cntrb = "Counter";
							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' class='BillQtycls' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' id='txtBillQty'/></td><td><input type='txtBillPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtMaxRetailPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr><tr></tr>";
                        }
                    }
                    else
                    {
                       
                        if (ProductCollection[i]["EditSaleRate"] == false) {
                            if (ProductCollection[i]["cntrbill"] == 0) {
                                var tr = "<tr style='background-color: " + color + ";' class='tr_itemrow'><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' class='inputs BillQtycls' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' id='txtBillQty'/></td><td><input class='inputs' type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input class='inputs' type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                            } else {
                                cntrb = "Counter";
								var tr = "<tr style='background-color: " + color + ";' class='tr_itemrow'><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' class='inputs BillQtycls' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' id='txtBillQty'/></td><td><input class='inputs' type='txtBillPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input class='inputs' type='txtMaxRetailPrice' disabled = 'disabled'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr>";
                            }
                        }
                        else {
                            if (ProductCollection[i]["cntrbill"] == 0) {
                                var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' " + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' class='BillQtycls' id='txtBillQty'/></td><td><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input type='txtMaxRetailPrice' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style ='display:none;'><input type='text'    disabled=disabled   data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                            }
                            else {
								cntrb = "Counter";
								var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' " + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' Name='txtBillQty' class='BillQtycls' id='txtBillQty'/></td><td><input type='txtBillPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input type='txtMaxRetailPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style ='display:none;'><input type='text'    disabled=disabled   data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr>";

							}
                        }

                    }
                }
                else
                {
					
                    if (RoleForEditRate == true)
                    {
                        
                        if (ProductCollection[i]["cntrbill"] == 0) {
                            var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/></td><td ><input type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtMaxRetailPrice' style='width:100px;text-align:center' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td style='width:180px;text-align:center;font-size:17px'>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                        }
                        else {
							cntrb = "Counter";
							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/></td><td ><input type='txtBillPrice' disabled = 'disabled' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtMaxRetailPrice' style='width:100px;text-align:center' disabled = 'disabled' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td style='width:180px;text-align:center;font-size:17px'>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr>";

						}
                    }
                    else
                    {
						
                        if (ProductCollection[i]["EditSaleRate"] == false) {
                            if (ProductCollection[i]["cntrbill"] == 0) {
                                var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/></td><td ><input type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * parseFloat(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                            }
                            else {
								cntrb = "Counter";
								var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/></td><td ><input type='txtBillPrice' disabled = 'disabled'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice' disabled = 'disabled'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * parseFloat(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr>";

                            }

                        }
                        else {
                            if (ProductCollection[i]["cntrbill"] == 0) {
                                var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/><td><input type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                            }
                            else {
								cntrb = "Counter";
								var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' disabled = 'disabled' value='" + Number(ProductCollection[i]["Qty"]).toFixed(3) + "' class='BillQtycls' Name='txtBillQty' id='txtBillQty'/><td><input type='txtBillPrice' disabled = 'disabled'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input type='txtMaxRetailPrice' disabled = 'disabled'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(3) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2) + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer;pointer-events: none;'><img src='images/trash.png'/></i></td></tr>";

							}
                        }

                    }

                }


                counterId = counterId + 1;

                Count = Count + 1;
                $("#tbProductInfo").append(tr);

                fPrice = 0;
                fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

                

                
                if (BillBasicType == "I")
                {
                    if (ProductCollection[i]["SurVal"] == "0")
                    {
                        var TAx = ((Number(fPrice) - ((parseFloat(fPrice) * parseFloat(itemDiscount) / 100))) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                    }
                    else
                    {

                        var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
                        var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));

                    }
                }
                else
                {

                    var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / 100);
                }


				var surchrg = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["SurVal"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                var tottax = Number(TAx);




                var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);
                VatAmt = VatAmt + tottax;
                Total = Total + fPrice;
                ProductCollection[i]["ProductAmt"] = fPrice;
                ProductCollection[i]["Producttax"] = TAx;
                ProductCollection[i]["ProductSurchrg"] = surchrg;
                var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
                var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
                var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
                $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
                $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
                $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));
                BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
                TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
                color = "";
                isfocus = 0;
				var qty = ProductCollection[i]["Qty"];
				ttlQty = ttlQty + Number(qty);

           
                //if (is_tab == 0) {
				
                if (ProductCollection[i]["Cursor_On"] == "QTY") {

                    if (incldy == "y") {

                        $('#tbProductInfo tr:last').find("#txtBillName").focus().select();
                    }
                    else {
                        $('#tbProductInfo tr:last').find("#txtBillQty").focus().select();

                    }
                    if (is_tab == 1) {

                        $('#tbProductInfo tr:last').find("#txtBillPrice").focus().select();
                    }
                    if (cntrb == "Counter") {
                       
						isfocus = 1;
                    }
                   
                }
                else if (ProductCollection[i]["Cursor_On"] == "RATE")
                {

                    $('#tbProductInfo tr:last').find("#txtBillPrice").focus().select();
                    if (is_tab == 1)
                    {
                        $('#tbProductInfo tr:last').find("#txtMaxRetailPrice").focus().select();
                    }
					if (cntrb == "Counter") {

						isfocus = 1;
					}
                }
               
                else
                {
                    isfocus = 1;
                }
                //}
              
            }
            if (isadd == 0) {


                if (($('#tbProductInfo tr').length > 35)) {
					$(".product_table").addClass('newprotbl');
                    isadd = 1;
                }
            }
            if ($('#tbProductInfo tr').length != 0) {
                $("#tbProductInfo").append("<tr class='td_itemcode'><td class='td_itemcode' colspan='8'> <input type='text'   class='form-control tbItemCode nbscreen_itemcode' placeholder='Enter Code' aria-describedby='basic-addon2' id='txtItemCode' /></td></tr>");
            }
            CommonCalculation();

            if (ProductCollection.length == 0) {
                var tr = "<tr style='border-bottom:0px'><td style='width: 100px'> <input type='text' autofocus  class='form-control tbItemCode nbscreen_itemcode' autocomplete='off'  placeholder='Enter Code' aria-describedby='basic-addon2' id='txxtItemCode' /></td></tr>";
                $("#tbProductInfo").prepend(tr);
            }
            if (isfocus == 1) {
				
                    $("#txtItemCode").focus();
   
            }
           
            //$("#txtItemCode").focus();
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            is_tab = 0;
           
            $('input').attr('autocomplete', 'off');
			$("#lblNoQty").html(ttlQty);

        }



		function isNumberKey(evt) {
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
				return false;

			return true;
		}


		function ClearCustomerDialog() {

			m_CustomrId = -1;

			$("#cm_ddlPrefix").val();
			$("#cm_Name").val("");
			$("#cm_Address1").val("");
			$("#cm_Address2").val("");
			// $("#cm_DOB").val("");
			//$("#cm_DOA").val("");
			$("#cm_DOB").val("1/1/1");
			$("#cm_DOA").val("1/1/1");
			$("#cm_Discount").val("0");
			$("#cm_Tag").val("0");
			$("#cm_ContactNumber").val("");
			$("#cm_EmailId").val("");
			$("#CustomerDialog").dialog("close");
			$("#txtGSTNo").val("");
			$('#cm_IsActive').prop("checked", true);




		}

		function InsertUpdateCustomer() {

			if ($("#txtGSTNo").val() != "0") {


				if ($("#txtGSTNo").val().length < 15) {
					alert("InValid GST No.!")
					return;
				}
			}
			var objCustomer = {};
			objCustomer.Customer_ID = m_CustomrId;
			objCustomer.Prefix = $("#cm_ddlPrefix").val();
			objCustomer.Customer_Name = $("#cm_Name").val();
			objCustomer.Address_1 = $("#cm_Address1").val();
			objCustomer.Address_2 = $("#cm_Address2").val();
			objCustomer.Area_ID = $("#cm_ddlArea").val();
			objCustomer.Pincode = $("#cm_Pincode").val();

			objCustomer.City_ID = $("#cm_ddlCities").val();
			objCustomer.State_ID = $("#cm_ddlState").val();

			objCustomer.Date_Of_Birth = $("#cm_DOB").val();



			objCustomer.Date_Anniversary = $("#cm_DOA").val();







			objCustomer.Discount = $("#cm_Discount").val();
			objCustomer.Contact_No = $("#cm_ContactNumber").val();

			objCustomer.EmailId = $("#cm_EmailId").val();
			objCustomer.Tag = $("#cm_Tag").val();
			//objCustomer.GSTNo = $("#txtGSTNo").val();
			var Foc = false;

			if ($("#cm_Tag").val() == "") {
				objCustomer.Tag = '0';
			}
			else {
				objCustomer.Tag = $("#cm_Tag").val();
			}

			if ($("#txtGSTNo").val() == "") {
				objCustomer.GSTNo = '0';
			}
			else {
				objCustomer.GSTNo = $("#txtGSTNo").val();
			}
			if ($('#cm_FOC').is(":checked")) {
				Foc = true;
			}

			objCustomer.FocBill = Foc;
			var IsActive = false;
			if ($('#cm_IsActive').is(":checked")) {
				IsActive = true;
			}
			var IsCredit = 0;
			if ($('#cm_Credit').is(":checked")) {
				IsCredit = 1;
			}
			objCustomer.IsActive = IsActive;
			objCustomer.IsCredit = IsCredit;
			var DTO = { 'objCustomer': objCustomer };

			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "managecashcustomers.aspx/InsertUpdateCustomer",
				data: JSON.stringify(DTO),
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

					if (obj.Status == -1) {

						alert("Sorry. Contact Number Already Registered with our Database");
						$("#cm_ContactNumber").focus();
						return;
					}
					else {
                        alert("CustomerSaved Successfully");
                        var Discount = obj.customer.Discount;
                        $("#lblCashCustomerName").text(obj.customer.Prefix + " " + obj.customer.Customer_Name + "," + obj.customer.Contact_No + "," + obj.customer.Address);

                        CshCustSelId = obj.customer.Customer_ID;
                        CshCustSelName = obj.customer.Customer_Name;

						$("#CashCustomer").css("display", "block");

						if (EnableCustomerDiscount == 1) {
							$("#dvdisper").val(Discount);

						}

						CommonCalculation();

						//BindCreditCst();
						ClearCustomerDialog();


					}
				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


				}

			});


		}



		function GetVMResponse(Id, Title, IsActive, Status, Type) {
			$("#dvVMDialog").dialog("close");

			var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
			if (Type == "Area") {

				$("#cm_ddlArea").append(opt);

			}

			if (Type == "City") {

				$("#cm_ddlCities").append(opt);

			}

			if (Type == "State") {

				$("#cm_ddlState").append(opt);

			}





		}

		function OpenVMDialog(Type) {


			$.ajax({
				type: "POST",
				data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
				url: "managearea.aspx/LoadUserControl",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					$("#dvVMDialog").remove();
					$("body").append("<div id='dvVMDialog'/>");
					$("#dvVMDialog").html(msg.d).dialog({ modal: true });
				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {

				}
			});

		}





        function BindtrDiscount(disc) {
            DiscountAmt = 0;
            VatAmt = 0;
            Total = 0;
            var fPrice = 0;
            TotalItems = 0;
            $("div[name='vat']").html("0.00");
            $("div[name='amt']").html("0.00");
            $("div[name='sur']").html("0.00");

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            var counterId = 0;
            BEndDiscountAmt = 0;
            var Count = 0;

            for (var i = 0; i < ProductCollection.length; i++) {

                ProductCollection[i]["ItemDiscount"] = disc;

                var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);

                if (ProductCollection[i]["ChallanNo"] == "") {

                    if (RoleForEditRate == true) {

						var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td ><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice' value='" + NUmber(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                    }
                    else {

                        if (ProductCollection[i]["EditSaleRate"] == false) {

							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td ><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td>" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                        }
                        else {
							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td><div id='btnMinus' class='btn btn-primary btn-small' >-</div></td><td ><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td ><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td><input type='txtMaxRetailPrice' value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td ><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                        }

                    }
                }
                else {

                    if (RoleForEditRate == true) {

						var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td ><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td ><input type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td >" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled   data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                    }
                    else {

                        if (ProductCollection[i]["EditSaleRate"] == false) {

							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td ><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td><input type='txtBillPrice'  value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled   data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                        }
                        else {
							var tr = "<tr><td>" + ProductCollection[i]["ItemCode"] + "</td><td><input type='txtBillName' class='BillNamecls' value='" + ProductCollection[i]["ItemName"] + "' id='txtBillName'/></td><td ><input type='txtBillQty' value='" + Number(ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillQty'/></td><td ><input type='txtBillPrice' value='" + Number(ProductCollection[i]["Price"]).toFixed(2) + "' id='txtBillPrice'/></td><td ><input type='txtMaxRetailPrice'  value='" + Number(ProductCollection[i]["MaxRetailPrice"]).toFixed(2) + "' id='txtMaxRetailPrice'/></td><td>" + Number(Number(ProductCollection[i]["Qty"]).toFixed(2) * Number(ProductCollection[i]["Price"]).toFixed(2)).toFixed(2)  + "</td><td >" + ProductCollection[i]["TaxCode"] + "</td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled   data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                        }

                    }

                }


                //        if(RoleForEditRate == true)
                //        {
                //         var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                //        }
                //        else
                //        {
                //         if(ProductCollection[i]["EditSaleRate"] == false)
                //           {
                //          
                //            var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                //            }
                //            else{
                //            var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                //            }

                //        }
                counterId = counterId + 1;

                Count = Count + 1;
                $("#tbProductInfo").append(tr);

                fPrice = 0;
                fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

                if (BillBasicType == "I") {


                    if (ProductCollection[i]["SurVal"] == "0") {
                        var TAx = ((Number(fPrice) - ((parseFloat(fPrice) * parseFloat(itemDiscount) / 100))) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                    }
                    else {

                        var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
                        var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"]) + Survalll));

                    }
                }
                else {

                    var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / 100);


                }



                var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
                var tottax = Number(TAx) + Number(surchrg);


                var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);

                VatAmt = VatAmt + tottax;


                Total = Total + fPrice;
                ProductCollection[i]["ProductAmt"] = fPrice;
                ProductCollection[i]["Producttax"] = TAx;
                ProductCollection[i]["ProductSurchrg"] = surchrg;


                var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
                var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
                var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
                $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
                $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
                $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


                BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);

                //TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);


            }


            if ($('#tbProductInfo tr').length != 0) {
                $("#tbProductInfo").append("<tr class='td_itemcode'><td class='td_itemcode' colspan='8'> <input type='text' class='form-control tbItemCode nbscreen_itemcode' placeholder='Enter Code' aria-describedby='basic-addon2' id='txtItemCode' /></td></tr>");
            }
            CommonCalculation();

            if (ProductCollection.length == 0) {
                var tr = "<tr style='border-bottom:0px'><td style='width: 100px'> <input type='text' class='form-control tbItemCode nbscreen_itemcode' autocomplete='off' placeholder='Enter Code' aria-describedby='basic-addon2' id='txxtItemCode' /></td></tr>";
                $("#tbProductInfo").prepend(tr);
            }
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();


        }




        function CommonCalculation() {
			
           var m_Total = Total.toFixed(2);
            if (HomeDelCharges == 1) {
                if (minbillvalue != 0) {
                    if (m_Total > minbillvalue) {
                        $("#dvDeliveryChrg").val(0)

                    }
                    else {
                        $("#dvDeliveryChrg").val(DeliveryCharges)


                    }
                }
            }
            $("div[id='dvsbtotal']").html(m_Total);

            // VatAmt = Number(Number(Total.toFixed(2)) - Number(DiscountAmt.toFixed(2))) * Number();

            $("div[id='dvVat']").html(VatAmt.toFixed(2));


            TaxAmt = GetServiceTax();


            if (BillBasicType == "E") {
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
            else {

                if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

            }
            else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
                if (Takeaway == 1) {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
                else {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }


            }

        }
        var BEndDiscountPer = 0;


        if (BackEndDiscount == 1) {



            BEndDiscountPer = BEndDiscountAmt * 100 / m_Total;

            $("#dvdisper").val(BEndDiscountPer.toFixed(2));
        }


        if (DiscountOnBillValue == 1) {

            for (var i = 0; i < DiscountValues.length; i++) {

                var StartValue = DiscountValues[i]["StartValue"];
                var EndValue = DiscountValues[i]["EndValue"];
                var DisPer = DiscountValues[i]["DisPer"];


                if (parseFloat(m_Total) >= parseFloat(StartValue) && parseFloat(m_Total) <= parseFloat(EndValue)) {

                    $("#dvdisper").val(DisPer.toFixed(2));
                    break;

                }
                else {
                    $("#dvdisper").val(0);

                }



            }
        }



        DiscountAmt = (Number(Total) * Number($("#dvdisper").val())) / 100;


        $("#dvdiscount").val(DiscountAmt.toFixed(2));


        if (RoundBillAmount == 1) {
            if (BillBasicType == "E") {
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));

            }
            else {

                if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }
                else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

                    if (Takeaway == 1) {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                }


            }
        }
        else {
            if (BillBasicType == "E") {
                $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
            else {
                if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                } else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

                    if (Takeaway == 1) {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    } else {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
                }


            }


        }


        if (BillBasicType == "E") {

            $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

        }
        else {

            if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
            else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

                if (Takeaway == 1) {
                    $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                } else {
                    $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
            }

        }


        var netamount = $("div[id='dvnetAmount']").html();
        var hdnNetamt = $("#hdnnetamt").val();
        var Round = Number(netamount) - Number(hdnNetamt)



        $("div[id='dvRound']").html(Round.toFixed(2));

        var no_rows = $("#tbProductInfo tr").length-1;

        if (no_rows <=0) {

            $("#lblNoItems").html(0);
        }
        else {


            $("#lblNoItems").html(no_rows);
        }
   

        //alert(Total);
        //alert(TaxAmt);
        //alert(VatAmt);
        //alert(DiscountAmt);

        //alert($("#hdnnetamt").val());
        //alert($("div[id='dvnetAmount']").html());


    }

		



    function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount, B_Qty, EditSaleRate, ChallanNo, Max_Retail_Price, CursorOn,cntrbill) {

        m_cntrbill = cntrbill;
        m_ItemId = ProductId;
        m_ItemCode = Code;
        m_Price = Price;
        m_ItemName = Name;
        m_EditSaleRate = EditSaleRate;
        m_Max_Retail_Price = Max_Retail_Price

		
        if (B_Qty == 0) {
            if (modeRet == "Return") {
                m_Qty = -1;
            }
            else {
                if (ItmQty == 0) {

                    m_Qty = 1;
                }
                else {
                    m_Qty = ItmQty;

				}
               
            }
        }
        else {
            m_Qty = B_Qty;
        }
        m_TaxRate = TaxCode;
        m_Surval = SurVal;
        m_Tax_Id = Tax_Id;
        m_ItemDiscount = Discount;
        m_challanno = ChallanNo;
        m_Max_Retail_Price = Max_Retail_Price;
        m_CursorOn = CursorOn;

        //         var item = $.grep(ProductCollection, function (item) {
        //             return item.ItemId == m_ItemId;
        //         });

        //         if (item.length) {

        //               for (var i = 0; i < ProductCollection.length; i++)
        //               {
        //                   if(ProductCollection[i]["ItemId"] == m_ItemId)
        //                   {
        //                       var qty = ProductCollection[i]["Qty"];
        //                       qty = qty+1;
        //                       ProductCollection[i]["Qty"] = qty ;
        //                       Bindtr();
        //                       return;
        //                   }
        //               }

        //         }

       
        TO = new clsproduct();

        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;
        TO.ChallanNo = m_challanno;
        TO.MaxRetailPrice = m_Max_Retail_Price;
        TO.Cursor_On = m_CursorOn;
        TO.cntrbill = m_cntrbill;
        ProductCollection.push(TO);
       
        Bindtr();
		


    }


    function GetServiceTax() {


        var TaxAmt = 0;


        $("#dvsertaxper").html(Sertax);


        if (option == "TakeAway") {
            if (Takeaway == "1") {

                TaxAmt = (Number(Total) * Number(Sertax)) / 100;
                $("#dvTax").html(TaxAmt.toFixed(2));


            }
            else {
                $("#dvsertaxper").html("0");
                TaxAmt == "0";
                $("#dvTax").html(TaxAmt.toFixed(2));
            }
        }
        else if (option == "Dine") {
            TaxAmt = (Number(Total) * Number(Sertax)) / 100;
            $("#dvTax").html(TaxAmt.toFixed(2));


        }


        return TaxAmt;

    }


    function Search(CatId, Keyword) {



        $.ajax({
            type: "POST",
            data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
            url: "screen.aspx/AdvancedSearch",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                $("#products").html(obj.productData);



            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }

        }
       );


    }

    //........................................

       
    $(document).ready(function (e) {

        $('#txt_userid').attr('autocomplete', 'off');
    });
    $(document).on("change", "#txtBillQty", function (event) {

      
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
        ttlQty = ttlQty - Qty;
       


        var fQty = $(this).val();
       // ttlQty = ttlQty + fQty;
        if (isNaN(fQty)) {
            fQty = 1;

        }
        ProductCollection[RowIndex]["Qty"] = fQty;

        Bindtr();
      
   
        $("#txtItemCode").focus();

    });


    $(document).on("change", "#txtBillPrice", function (event) {

       
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];



        var fPrice = $(this).val();

        ProductCollection[RowIndex]["Price"] = fPrice;

		var fmrp = ProductCollection[RowIndex]["MaxRetailPrice"];
       
        if (fmrp < fPrice) {
			ProductCollection[RowIndex]["MaxRetailPrice"] = fPrice;
		}
        Bindtr();


    });


		$(document).on("change", "#txtMaxRetailPrice", function (event) {


			var RowIndex = Number($(this).closest('tr').index());
			var PId = ProductCollection[RowIndex]["ItemId"];

			var Mode = "Plus";

			var Qty = ProductCollection[RowIndex]["Qty"];
			var Price = ProductCollection[RowIndex]["Price"];



			var fmrp = $(this).val();

			ProductCollection[RowIndex]["MaxRetailPrice"] = fmrp;

			Bindtr();


        });

		$(document).on("change", "#txtBillName", function (event) {


			var RowIndex = Number($(this).closest('tr').index());
			var PId = ProductCollection[RowIndex]["ItemId"];

			var Mode = "Plus";

			var Qty = ProductCollection[RowIndex]["Qty"];
			var Price = ProductCollection[RowIndex]["Price"];



			var fname = $(this).val();

            ProductCollection[RowIndex]["ItemName"] = fname;

			Bindtr();


		});




    $(document).on("click", "#btnPlus", function (event) {


        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
        var fQty = 0;
        if (Qty < 0) {
            fQty = Number(Qty) + (-1);
        }
        else {
            fQty = Number(Qty) + 1;
        }




        ProductCollection[RowIndex]["Qty"] = fQty;

        Bindtr();

    });


    $(document).on("click", "#btnMinus", function (event) {

        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];
        var Mode = "Minus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];

        var fQty = 0;
        if (Qty < 0) {
            fQty = Number(Qty) - (-1);
        }
        else {
            fQty = Number(Qty) - 1;
        }

        ProductCollection[RowIndex]["Qty"] = fQty;
        if (fQty == "0") {
            ProductCollection.splice(RowIndex, 1);
        }

        Bindtr();


    });



    //............................................





    </script>
    
 <div id="dvAddOn"></div>
<div class="right_col" role="main">
    <div class="billing_screen_right_col" role="main">

        <div id="dvpaymode" class="change_paymode" style="z-index: 100;position:  relative;float: left;display:none">

                   

                    <div class="clearfix"></div>

                   
                    <div class="x_panel">

               <h2>Change Paymode</h2>
                        <div class="x_content">

                             <table width="100%">
             <tbody><tr><td align="center">
             <div style="" class="bill_number">
             
         
             <table width="100%" cellspacing="5" cellpadding="5">
             <tbody><tr>
             <td>Bill Number:</td><td>
             <input type="text" id="txtBillNumber_pm" style="width:200px;height:30px; text-transform:uppercase" class="form-control input-small" />
             </td>
            <td><div id="dvShow" class="btn btn-primary btn-small"  style="margin: 2px;display:none">
                
                Show</div></td>


                 
             </tr>
             </tbody></table>
                 </div>
             </td></tr>
             <tr>
             <td style="border-bottom:dotted 1px silver"></td>
             </tr>
             <tr>
             <td>
         <div class="cate paymode_cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px;">
                         <tr>
                                
                                <td colspan="100%">
                                    <table class="payment_cate_table">
                                        <tr>

                               
                               <%-- <td>
                                    Bill Type:
                                </td>--%>
                                <td>
                                    <select id="ddlbillttype_pm" style="height: 119px; width: 258px; padding-left: 0px;" class="form-control" multiple="multiple">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>

                                <td>
                                    <span id="ddlcustName_pm">Customer Name</span>
<%--                                  <select id="ddlCreditCustomers_pm" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 166px;
">
                                             <option value="0"></option>
                                             </select>--%>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit_pm" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>


                         

                                </td>
                               <td>
                                    
                                </td>
                                
                            </tr>
                            <tr>
                                <td  style="width:130px;">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;" id="txtFinalBillAmount_pm" disabled="disabled" multiple="multiple"/>
                                </td>
                                <td>
                                
                                </td>
                                <td>
                                    
                                </td>
                                <td>

                                

                                </td>
                                          
                                 <td> </td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading_pm" style="font-weight: normal;">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;" value="0"
                                        id="txtCashReceived_pm" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer_pm" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display:none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName_pm" style="font-weight: normal; margin-top: 1px;">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="crcredit_pm" >
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;display:none;" value="0"
                                        id="Text13_pm" />
                                </td>
                                            <td id="Type_pm">Type:</td>
                                                  <td>
                                    <select id="ddlType_pm" style="height: 35px; width: 150px;display:none;" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>
                           
                                <td>
                                   
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                
                                </td>
                                <td colspan="100%">
                                  
                                </td>
                            </tr>
                            <tr>
                                <td id="Bank_pm" style="display:none;">
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank_pm" style="height: 32px; width: 90px;display:none;" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15_pm"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>

                                              <tr><td id="OP_pm" style=" display: none;">Online payment</td>

                            <td> <select id="ddlOtherPayment_pm" class="form-control" clientidmode="Static" runat="server" style=" display: none;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                           
                                             </select></td>
                            
                                        
                                        
                                             </tr>
                            <tr>


                                                 
                                                  <td  style="text-align:left;width:50px">
                                                       <input type="checkbox" id="rdoCOD_pm"  style="width:15px;height:19px;display:none" />
                         <label class="headings" id="COD_pm" for="rdoCOD" style=" display: none;">COD</label>
                                </td>
                                                     <td colspan="3">
                                    <table>
                                        <tbody><tr>
                                                <td>
                                                   
                                   <input class="form-control input-small" style="width: 70px;height:25px;color:Black;display:none" id="txtCODAmount_pm" type="text" value="0"/>
                                </td>
                                <td>
                                      <input type="checkbox" id="rdoOnline_pm"  style="width:15px;height:19px;display:none;" />
                                    <label class="headings" id="Online_pm" style=" display: none;" for="rdoOnline">Online</label>
                                </td>
                                <td>
                                  <input type="text" class="form-control input-small"style="width: 70px;height:25px;color:Black;display:none" value="0"
                                        id="txtpaymentAmount_pm" />
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>
                                        </tr>
                                        <tr> 
                                           <td id="coupan_pm" style=" display: none;">Enter Coupan Qty</td>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 60px;display:none" value="0"
                                        id="Coupans_pm" />
                                        </td>
                                      <td style="width: 50px"><label id="No_pm"style="display: none; margin-left: -150px;" >CoupanNo</label>
                                      <div id="holder_pm"style="display: block; margin-left: -150px;"></div></td>
                                      <td style="width: 50px"><label id="Amt_pm" style=" display: none;">CoupanAmt</label><div id="holder2_pm"></div></td> 
                                             
                  
                                      </tr>
                                    </table>

                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16_pm" />
                                </td>
                            </tr>

                            <tr class="paymode_close">
                                <td>
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px;" id="txtBalanceReturn_pm"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk_pm" class="btn btn-primary btn-small" style="margin: 2px;">
                                                                Change Paymode</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose_pm" class="btn btn-primary btn-small"  style="margin: 2px;">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                 </tbody>
                        </table>
                    </div>   
             
             
             
             </td></tr>
             
             
             </tbody></table>

                        </div>
                    </div>


     

                     
                </div>
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Bills List</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group" margin-bottom:10px">

                                 <div class="form-group">
                                
                                <table>
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>

                <table id="jQGridDemoBill">
                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                </div>


                                   
                                        <div class="form-group">
                                            <td class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table class="btn-tbl-outer">
                                                    <tr><td><div class="btn btn-primary" style="display: block;" id="btnShowScreen">
                                           <i class="fa fa-external-link"></i> New</div></td>
                                           <td><div class="btn btn-success" style="display:none;" id="btnEdit">
                                           <i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                           
                                           <td><div class="btn btn-danger" style="display: block;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> Reprint</div></td>
                                            <td >
                                        <div id="btnDelete" style="display:none;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i> Cancel Bill</div>

                                           

                                    </td>
                                    <td> <div id="btnGSTBill" style="display:block;" class="btn btn-success">
                                           <i class="fa fa-trash m-right-xs"></i> GST Bill</div></td>
                                                        
                                                            <td> 
                                             <asp:LinkButton ID="btnRefreshPrinter" OnClick="btnRefreshPrinter_Click" runat="server" class="btn btn-primary"> <i class="fa fa-refresh"></i> Refresh Printer</asp:LinkButton>
                                                          </td>
                                                         <td>
                          <div id="dvShow_pm" class="btn btn-primary" style="">
                                                                <i class="fa fa-edit m-right-xs"></i> <u>C</u>hange Paymode</div></td>
                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     








                    
                    <script type="text/javascript">


                        $(document).ready(function () {



                            $("#Img2").click(
                                   function () {

                                       $("#CustomerDialog").dialog({
                                           autoOpen: true,

                                           width: 800,
                                           resizable: false,
                                           modal: true
                                       });
                                       $("#CustomerDialog").parent().addClass('custdia-pop');
                                   }
                                   );


                            if ($("#ddlbillttype").val() == "Cash") {
                                var customerId = 0;
                                CrdCustSelId = 0;
                                CrdCustSelName = "";
                                $("#hdnCreditCustomerId").val(0);
                                $("#lblCreditCustomerName").text("");
                                CrdCustSelName = "";
                                $("#ddlCreditCustomers").html("");
                                $("#txtddlCreditCustomers").val("");

                                $("#lblCashHeading").text("Cash Rec:");
                                $("#creditCustomer").css("display", "none");

                                $("#dvOuter_ddlCreditCustomers").hide();

                                $("#ddlChosseCredit").hide();
                                $("#ddlChosseCredit").val(0);

                                $("#txtCashReceived").val("0").prop("readonly", false);
                                $("#Text13").val("0").prop("readonly", false);
                                $("#Text14").val("").prop("readonly", false);
                                $("#Text15").val("0").prop("readonly", false);
                                $("#Text16").val("").prop("readonly", false);
                                $("#ddlType").prop("disabled", false);
                                $("#ddlBank").prop("disabled", false);


                            }


                            $("#txtDateFrom,#txtDateTo").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();



                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                            );

                            BindBanks();

							$("#btnBillWindowOk_pm").click(function () {

								var CheckBillType = $("#ddlbillttype_pm").val();
								if (CheckBillType == 'OnlinePayment') {
									if ($("#<%=ddlOtherPayment_pm.ClientID %>").val() == "0") {
										  alert("Please Select Other Paymentmode");
										  return false;
									  }
									  else {
										  ChangePaymode();
									  }
								  }
								  else {
									  ChangePaymode();
								  }
							  }
							  );


							$("#btnBillWindowClose_pm").click(function () {


								$("#dvpaymode").hide();

								ResetBillCntrols();
								Reset();


							});

							$("#dvShow_pm").click(
								function () {

									var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
									if ($.trim(SelectedRow) == "") {
										alert("No Bill is selected to Change Paymode");
										return;
									}
									else {
										$("#dvpaymode").show();
										$("#txtBillNumber_pm").val(m_BillNowPrefix);
										$("#txtBillNumber_pm").attr("disabled", "disabled");
										$("#ddlbillttype_pm").focus();


										GetBillByBNF();



										// CHangePaymodeDv();
										//                     $("#txtBillNumber_pm").removeAttr("disabled");
										//$("#txtBillNumber_pm").focus();
									}
								}
							);




                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


<div id="dvbillSave" style="float: left; display:none; position:absolute;z-index:1000;left:340px;width:300px;bottom:240px;background-color: #293C52;
                                                                color: white; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                                <table style="width:100%;">
                                                                    <tr style="text-align:center">
                                                                    <td style="text-align:center">BILL SAVED SUCCESSFULLY</td>
                                                                        
                                                                    </tr>
                                                                    <tr><td style="text-align:center;font-size:20px">AMOUNTED Rs.<asp:Label ID ="lblAmmmt" runat="server" ClientIDMode ="Static"></asp:Label></td></tr>
                                                                    <tr><td>
                                                                    <div class="button1" style="background-color:White;color:Black;width:60px;box-shadow: -2px -5px 5px transparent;margin-left:41%;height:40px;"  id="btnMsgClose">
                                    OK</div>
                                                                   </td></tr>
                                                                    
                                                                  
                                                                </table>
                                                            </div>
															
<div id="CustomerDialog" style="display:none">


							 <div class="form-div">
								 <div class="form-lbl">Prefix <span>*</span></div>
								 <div class="form-inp">
									 <div class="prefix-inp">
									  <asp:DropDownList class="form-control" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                       <input type="text" class="txt form-control validate required alphanumeric" placeholder="Customer Name" id="cm_Name">
								</div>
									 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">Address1 <span>*</span></div>
								 <div class="form-inp">
									  <textarea class="txt form-control validate required" id="cm_Address1"></textarea>
								 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">State <span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <asp:DropDownList  class="form-control" ID="cm_ddlState" ClientIDMode="Static"  runat="server" ></asp:DropDownList>
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">
											 City <span class="required">*</span> 
										 </div>
										 <div class="sec-input">
											 <asp:DropDownList  class="form-control " ID="cm_ddlCities" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                                 
                                            <span class="fa fa-plus city-plus" onclick="javascript:OpenVMDialog('City')"></span>
										 </div>
									 </div>
								 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">Area <span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <asp:DropDownList class="form-control" ID="cm_ddlArea" ClientIDMode="Static" runat="server">

                                        </asp:DropDownList>
                                        <span class="fa fa-plus area-plus" onclick="javascript:OpenVMDialog('Area')"></span>
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Pincode <span class="required">*</span></div>
										 <div class="sec-input">
											 <input type="number" class="txt form-control validate required alphanumeric" onkeypress="return isNumberKey(event)" placeholder="Pincode" id="cm_Pincode">
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">Contact<span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <input type="text" class="txt form-control validate required valNumber" placeholder="Mobil no" id="cm_ContactNumber" onkeypress="return isNumberKey(event)" maxlength= "10">
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Email</div>
										 <div class="sec-input">
											<input type="text" class="txt form-control" placeholder="Email" id="cm_EmailId" >
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">Discount</div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <input type="text" class="txt form-control validate  valNumber" onkeypress="return isNumberKey(event)" value="0" id="cm_Discount" />
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Tag</div>
										 <div class="sec-input">
											 <input type="text" class="txt form-control alphanumeric" id="cm_Tag" >
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">DOB</div>
								 <div class="form-inp">
									 <div class="first-inp">
										  <input type="text"   class="form-control"   id="cm_DOB" aria-describedby="inputSuccess2Status" />
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">An. Date</div>
										 <div class="sec-input">
											 <input type="text"   class="form-control"   id="cm_DOA" aria-describedby="inputSuccess2Status" />
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">GST No</div>
								 <div class="form-inp">
									 <input type="text" class="txt form-control" required="required" value="0" id="txtGSTNo">
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">IsActive <span class="required">*</span></div>
								 <div class="form-inp">
									  <div class="first-check">
										 <input type="checkbox" checked="checked" id="cm_IsActive" />
										</div>
									 	 <div class="first-check">
										 
											<label for="first-name" class="control-label ">FOC <span class="required">*</span></label>
                                            <input type="checkbox" id="cm_FOC"  />
										</div>
									 	 <div class="first-check">
											<label for="first-name" class="control-label ">Credit</label>
											<input type="checkbox"  id="cm_Credit" />
                                                            
									 </div>
								 </div>
							 </div>
							
                                <div class="form-div cashcustomer_btns">
                                    <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                    <button class="btn btn-danger"  onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                    <%-- <button class="btn btn-danger"  id="closkey" type="button"><i class="fa fa-mail-reply-all"></i> Close keyboard</button>--%>
                                </div>



<%--<div class="form-horizontal form-label-left input_mask">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>--%>


</div>
															
															

<div id="GSTDialog" style="background-color:#26b99a;display:none">

 <table style="border-collapse: separate; border-spacing: 1;font-size:17px">

                        <tr style="display:none"><td><input type="radio" id="rdblocal" name="local" checked="checked" /> <label class="control-label" for="rbLocal" style="font-weight:normal"> Local</label></td>
                        <td><input type="radio" id="rdbout" name="local" /> <label class="control-label" for="rdbout" style="font-weight:normal"> Outstation</label></td></tr>
                        <tr><td><input type="checkbox" checked="checked" data-index="27" style="font-size:25px" id="chkGst" /></td><td>Mark as GST Bill</td></tr>
                        <tr> <td colspan="100%">
               
                <table><tr>      
                                        <td style="color:black"><b>CUSTOMER </b></td>
                                            <td style="padding-left:5px;">

                                            
                                                <select id="ddlMobSearchBox1">

                                                </select>
                                                

                                                <input type="text" placeholder="Mobile Number" style="display:none; border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox1" class="ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox" />

                                                 <div id="Div1" style="display:none;">
                                                    <img id="img1" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>
                                            <td>
                                              <img id="Img2"  style="cursor:pointer;width:25px"  src="images/adduser-white.png"    />
                                            </td></tr></table></td>               
               
                       <td>
                                                <table width="100%" id="Table1" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2"; margin:0 auto !important;>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="Label1" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <td style="padding-left:50px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                                </td>
                                                   
                </tr>


                        <tr><td><div id="btnsa" style="display:block;" class="btn btn-primary">
                                           <i class="fa fa-trash m-right-xs"></i>Save</div></td></tr>
</table>



</div>


<div id="screenDialog" class="nbscreen_dialog" style="display:none">
     <asp:DropDownList ID="dd_customername" runat="server" style="width:185px;height:30px;margin-left:5px" CssClass="shop-select">
                                               
                                            </asp:DropDownList>
    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
    <div class="container">
        <div class="row">
            <div class="nav1" style="padding-top:0px;margin-top:-5px;display:none"> 
                <ul id="categories">
                </ul>
            </div>
        </div>
        <div class="row">

         <div class="col-xs-12">

                
                <%--<div class="Search" style="padding-left:200px">
                    <div class="input-group">
                        <table width="100%">
                            <tr>
                                <td style="width: 100px">
                                    <input type="text" class="form-control tbItemCode" placeholder="Enter Code" aria-describedby="basic-addon2"
                                        id="txtItemCode" style="padding: 10px 10px; width: 100%; border: 0px; height: 50px" />
                                </td>
                                <td>
                                    <span id="btnGetByItemCode" value="Search" >
                                        <img src="images/plusicon.png" alt="" style="width: 50px; padding-left: 10px" /></span>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Search By Name" aria-describedby="basic-addon2"
                                        id="txtSearch" style="padding: 10px 10px; width: 100%; border: 0px; height: 50px" />
                                </td>
                                <td>
                                    <span   id="btnSearch" value="Search">
                                        <img src="images/search-button.png" alt="" style="width: 50px; padding-left: 10px" /></span>
                                </td>
                                <td style="padding-left:50px">
                                 <input type="checkbox" data-index="27" style="font-size:25px" id="chkDelivery" /><label style="color:White;font-size:18px">Delivery Charges</label>
                                </td>
                                 
                            </tr>
                        </table>
                    </div>
                </div>--%>
                
                </div>
                

                <div class="col-xs-12" style="display:none;margin-bottom:5px" id="colProducts">
                <div id="products" style="overflow-y: scroll; height: 190px; background: #2A1003">
                </div>
                    </div>




            						<div id="ItemGrid1" title="Press CTRL+ENTER To Select Item" style="display:none;">
						
							<table id="jQGridProduct">
												</table>
												<div id="jQGridProductPager">
												</div>
						</div>

            <div class="col-xs-12" style="padding-left: 0px" id="colDvProductList">
                <div id="dvProductList" class="nbscreen_product_list" style="display: block;"">
                    <div class="leftside">
                        <table>
                            <thead>
                                <tr>
                                    <th>Item Code</th>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>MRP</th>
                                    <th>Amount</th>
                                    <th>Tax</th>
                                    <th>Delete</th>
                               </tr>
                           </thead>
                        </table>
                    </div>
                    <div class="cate nbscreen_cate1">
                    
                <div class="table_and_shortkey">
                    <div class="product_table">
                        <table class="child-div" id="tbProductInfo">
                             <tr id="tr_prime_ItemCode">
                                 <td style='width: 100px'> 
                                     <input type='text' class='form-control tbItemCode nbscreen_itemcode' placeholder='Enter Code' aria-describedby='basic-addon2' id='txt_prime_ItemCode' />

                                 </td>
                             </tr>
                        </table>
                    </div>
                    

                        <p class="back_short_key">Press <b style="color:red;">SHIFT+TAB</b> for Back </p>

                                            

                    </div>

                      <table class="buttons">
                        <tr>
                            <td colspan="100%" align="right" style="padding-right:10px;display:none">
                                <table>
                                    <tbody>
                                        <tr>  <td><b>Choose Service</b></td>
                                               <td><b><asp:DropDownList ID="ddloption" runat="server" 
                                                    Style="width: 86px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList></b>  <b><select id="ddlTable" style="height: 25px; width: 86px" class="form-control"></b>
                                            </td>
                                            
                                              </tr>
                                        <tr>
                                        
                                        <td><b>CHOOSE CASH CUSTOMER </b></td>
                                <%--            <td style="padding-left:5px">


                                                <select id="ddlMobSearchBox">

                                                </select>


                                                <input type="text" placeholder="Mobile Number" style="display:none;border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox">

                                                 <div id="btnCustomer" style="display:none;">
                                                    <img id="imgSrchCashCust" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>--%>

                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                            
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button  class="button1" id="btnCash">(+)&nbsp Cash</button>
                               
                            </td>
                            <td>
                      <div class="left-btn-div">
                                <div class="button1" id="btnOnline">
                                    (F2) Online Payment</div>
                         </div>

                            </td>
                            <td>
                                <div class="button1" id="btnCreditCard"  >
                                    (F4)&nbsp Credit Card</div> 


                            </td>
                            <td>
                                <div class="button1" id="btnCredit">()&nbsp;Credit</div>
                            </td>
                            <td rowspan = "2">
                               <div class="button1" id="btnClear">
                                    (F5)&nbsp Refresh</div>
                            </td>
                            <td>
                                <div class="button1" id="btnreturn">
                                    (F6)&nbsp Return</div>
                            </td>
                            <td>
                                <div class="button1" id="btnsavebill" style="display:none">
                                   (F4)&nbsp;Paymode</div>
                            </td>
                            
                                <td>
                                      
                                <%--<div class="button1" id="btnsearch">
                                    (F7)&nbsp Search</div>--%>
                            </td>
                           <td>
                               
                           </td>

                            <td>
                                <div class="button1" id="btnrowdelete">
                                    (F9)&nbsp Row Delete</div>
                            </td>
                           
                            <td>
                                <div class="button1" id="btnUnHold">
                                    (F11)&nbsp UnHold</div>
                            </td>

                            <td>
                                <div class="button1" id="btnhold">
                                    (F12)&nbsp Hold</div>
                            </td>
                             <td>
                                <div class="button1" id="btnClose">
                                    &nbsp Exit</div>
                            </td>
                            

                            

                             


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>


                        </tr>
                        <tr>
                            
                            
                            <td>

                                 <div id="dvOnlineWindow" style="display:none">
                                    
                                         <div class="form-field">
									<div class="form-txt">
										 <input type="radio"   id="rdoCOD" name="abc" />
                         <input type="checkbox" disabled="disabled" id="codChk"  style="width:15px;height:19px" />  
										<label class="headings" id="COD" for="rdoCOD">COD</label> 
									</div>
									<div class="form-input">
										 <input class="form-control input-small cls_mmp" data="OnlinePayment" style="width: 90px;height:25px;color:Black;" disabled="disabled" id="txtCODAmount" type="text" value="0"/>&nbsp
									</div>
								</div>
								<div class="form-field ">
									<div class="form-txt online-tr-op">
										 <input type="checkbox" id="OnlineChk"  style="width:15px;height:19px" />
										<label class="headings" id="Online" for="rdoOnline">Online</label> 
									</div>
									<div class="form-input ">
										 <input type="text" class="form-control input-small cls_mmp" data="OnlinePayment"  style="width: 90px;height:25px;color:Black;" value="0"
                                        id="txtpaymentAmount" />
									
										<select id="ddlOtherPayment" class="form-control" clientidmode="Static" runat="server" style=" display: inline-block;
    float: left;
    font-size: 15px;
    min-width: 0;
    padding: 0;
    width: 145px;
	font-weight:400;
">
                                           
                                             </select>
									</div>
								</div>



                                        
                                    <div  class="btn btn-primary" id="btnOnlineBilling">
                                    Create Bill
                                     </div> 
                                   
                                    
                                    </div>

                             <div id="dvCreditCardWindow" style="display:none">
                                    <table>
                                    <tr><td>Bank:</td><td><select id="ddlCreditCardBank" disabled="disabled"></select></td>
                                    <td>
                                    
                                    <div  class="btn btn-primary" id="btnCreditCardBilling">
                                    Create Bill
                                     </div> 
                                    </td>
                                    
                                    </tr>
                                  

                                    </table>
                                    
                                    </div>

                                 <div id="search_item" title="Products List" style="display:none">
                                      <select style="width:154px" id="ddlItemSearchBox">

                                                </select>
                                     <table style="display:none" id="jQGridDemoItem" >
                </table>
                <div style="display:none" id ="jQGridDemoPagerItem">
                </div>
                                     </div>
                             <div id="search_product" style="display:none">
                                    <table class="table table-bordered table-hover table-striped" id="tbl_getall_prdct">
                                        <thead>
                                            <tr>
                                                <th>Item Code</th>
                                                   <th>Item Name</th>
                                                   <th>MRP</th>
                                                   <th>Sale Rate</th>
                                                   <th>Tax</th>

                                            </tr>

                                        </thead>
                                   <tbody id="tbl_body_getall_prdct">




                                   </tbody>
                                  

                                    </table>
                                    
                                    </div>


                             
                              

                                 <%--<div class="button1" id="btnCredit" style="background:gray;display:none" disabled>
                                    (F8)&nbsp Credit</div> --%>
                            </td>
                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                           <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </div>
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    <div class="cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="txtFinalBillAmount" />
                                </td>
                                <td>
                                    Bill Type:
                                </td>
                                <td>
                                    <select id="ddlbillttype" style="height: 27px; width: 120px; padding-left: 0px" class="form-control">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>
                                <td>

                                  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                             <option value="0"></option>
                                             </select>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="txtCashReceived" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                    Type:
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 25px; width: 90px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" style="height: 25px; width: 90px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 64px">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" checked="checked"  value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan = "100%">
                             <table>
                             <td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </table>
                                              
                                           
                                           
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Table
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <div class="cate2 nbscreen_cate2" style="padding:0px;margin-top:0px">
                    <table id="tbamountinfo">
                        <tr>
                            <td valign="top" class="main_td">
                                <div class="left-tbl-pan">
                                     <div class="phn-div">
                                     <table>
                                         <tr style='border-bottom:0px'>
                                             <td style='width: 100px'><input type='text'   placeholder='Enter Phone No'  id='txtpnno' /></td></tr></table></div>
                                <div class="user_btn_bottom" >
                                    
                                    <div class="user-search-div">
                                                <select id="ddlMobSearchBox">

                                                </select>
                                        </div>
                                      <div class="user-img-div">
                                        <img src="images/adduser-white.png" id="btn_add_newcst"/>                           

                                      </div> 
                                    

                                      
                                </div>
                                          <div class="cash_customer_detail">
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2">
                                                    <tr>
                                                        <td>
                                                               
                                                               <img src="images/close-icon.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' />
                                                     
                                                        </td>

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="lblCashCustomerName" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                            </div>
                                </div>
                                <div class="right-tbl-pan">
                                    <asp:Literal id="ltDateTime" runat="Server"/>

                                  
                                

                                <div class="item_numbers">
                                <table >
                                     <tr>
                                         <td>No Of Items&nbsp&nbsp: </td>
                                         <td><div>
                                                 <asp:Label ID ="lblNoItems"  runat="server" ClientIDMode ="Static"></asp:Label>
                                        </div></td>
                                     </tr>
                                      <tr>
                                         <td>Total Qty:&nbsp&nbsp: </td>
                                         <td><div>
                                                 <asp:Label ID ="lblNoQty"  runat="server" ClientIDMode ="Static"></asp:Label>
                                        </div></td>
                                     </tr>
                                 </table>
                                </div>

                                <div class="both_amount">
                                    <div class="bill_numb">
                                        <ul>
                                            <li><p>Last Bill No. &nbsp&nbsp:</p></li>
                                            <li><p><span id="billnumb"></span></p></li>
                                        </ul>
                                    </div>
                                    <div class="bill_amount">
                                        <ul>
                                            <li><p>Last Bill Amount &nbsp&nbsp:</p></li>
                                            <li><p><span id="billamt"></span></p></li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                                <table>
                                    <tr>
                                        <td></td>
                                    </tr>
                                </table>
                                

							
                                
                               
                                      

                                
                         
                                   
                                  
                            </td>

                            
                            <td valign="top" align="right" class="main_td_right">
                                <table  id="myCustomTable">
                                    <tr>
                                        <td>
                                            Amount:
                                        </td>
                                        <td>
                                            <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Discount:
                                        </td>
                                        <td>
                                        <input type="text" autocomplete="off"  class="form-control input-small " id="dvdisper" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td>
                                           <input type="text" autocomplete="off"  class="form-control input-small" id="dvdiscount" value ="0" />
                                        </td>
                                     </tr>
                                        
                                    <tr>
                                        <td>
                                      
                                        </td>
                                        <td>
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                           %
                                        </td>
                                        <td colspan="50%">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr id ="vatIncOrExc">
                                        <td style="padding-right: 10px;height: 25px;      font-weight: bold; text-align: right">
                                            GST Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges">
                                        <td style="padding-right: 10px;height: 25px;      font-weight: bold; text-align: right">
                                            DeliveryCharges:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%"></td>
                                        <td>   
                                             <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvDeliveryChrg" value ="0" />
                                        </td>
                                    </tr>

                                     <tr>
                                        <td>
                                            Round Off:
                                        </td>
                                        <td>
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr >
                                        <td>
                                            Net Amount:
                                        </td>
                                        <td class="net-amt-val">
                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>
                              </table>
                            </td>
                      </tr>
             </table>

                    <%--<table>
                        <tr>
                            <td colspan="100%" align="right" style="padding-right:10px;display:none">
                                <table>
                                    <tbody>
                                        <tr>  <td><b>Choose Service</b></td>
                                               <td><b>
                                                   <asp:DropDownList ID="ddloption" runat="server" 
                                                    Style="width: 86px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList></b>  <b><select id="ddlTable" style="height: 25px; width: 86px" class="form-control"></b>
                                            </td>
                                            
                                              </tr>
                                        <tr>
                                        
                                        <td><b>CHOOSE CASH CUSTOMER </b></td>
                                            <td style="padding-left:5px">


                                                <select id="ddlMobSearchBox">

                                                </select>


                                                <input type="text" placeholder="Mobile Number" style="display:none;border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox">

                                                 <div id="btnCustomer" style="display:none;">
                                                    <img id="imgSrchCashCust" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                            
                                            </td>
                                       
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="button1" id="btnreturn">
                                    (F3) Return</div>
                            </td>
                            <td>
                                <div class="button1" id="btnsavebill">
                                    Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnhold">
                                    (F12) Hold</div>
                            </td>

                             <td rowspan = "2">
                               <div class="button1" id="btnClear"  style="height:105px;vertical-align:middle">
                                    (F2) Refresh Bill Screen</div>
                            </td>


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>


                        </tr>
                        <tr>
                            <td>
                                <div class="button1" id="btnUnHold">
                                    (F11) UnHold</div>
                            </td>
                            <td>
                                <button  class="button1" id="btnCash">(F1) Cash</button>
                               
                            </td>
                            <td>

                             <div id="dvCreditCardWindow" style="display:none">
                                    <table>
                                    <tr><td>Bank:</td><td><select id="ddlCreditCardBank"></select></td>
                                    <td>
                                    
                                    <div  class="btn btn-primary" id="btnCreditCardBilling">
                                    Create Bill
                                     </div> 
                                    </td>
                                    
                                    </tr>
                                  

                                    </table>
                                    
                                    </div>

                                
                             <div id="search_product" style="display:none">
                                    <table class="table table-bordered table-hover table-striped" id="tbl_getall_prdct">
                                        <thead>
                                            <tr>
                                                <th>Item Code</th>
                                                   <th>Item Name</th>
                                                   <th>MRP</th>
                                                   <th>Sale Rate</th>
                                                   <th>Tax</th>

                                            </tr>

                                        </thead>
                                   <tbody id="tbl_body_getall_prdct">


                                   </tbody>
                                  

                                    </table>
                                    
                                    </div>


                             
                              <div class="button1" id="btnCreditCard"  >
                                    (F8) Credit Card</div> 

                                 <div class="button1" id="btnCredit" style="background:gray;display:none" disabled>
                                    (F8) Credit</div> 
                            </td>
                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>--%>
                </div>
            </div>
            
        </div>
    </div>

</div>

            
    </form>

     
  <script language="javascript">

	 

      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          jQuery("#jQGridDemoBill").GridUnload();

          jQuery("#jQGridDemoBill").jqGrid({
              url: 'handlers/ManageBills.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer', 'CashCustAddr', 'CreditCustAddr', 'cst_id'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },

                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                              { name: 'cst_id', index: 'cst_id', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

              ],

              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'BillNowPrefix',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Bills List",




          });

          var $grid = $("#jQGridDemoBill");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  etrType = "Search";
                  $("#globalSearch").click();
              }
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


              $("#btnEdit").css({ "display": "none" });
              $("#btnDelete").css({ "display": "none" });
              $("#btnShowScreen").css({ "display": "none" });


              for (var i = 0; i < arrRole.length; i++) {

                  if (arrRole[i] == 9) {

                      $("#btnShowScreen").css({ "display": "block" });
                  }
                  if (arrRole[i] == 2) {

                      $("#btnDelete").css({ "display": "block" });
                  }
                  if (arrRole[i] == 3) {
                      $("#btnEdit").css({ "display": "block" });
                      m_BillNowPrefix = 0;
                      m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

                  }
                  m_BillNowPrefix = 0;
                  m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

              }







              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
              //  $("#txtddlMobSearchBox").val(CashCustName);

              //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

          }
      });





      $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
              {
                  refresh: false,
                  edit: false,
                  add: false,
                  del: false,
                  search: false,
                  searchtext: "Search",
                  addtext: "Add",
              },

              {//SEARCH
                  closeOnEscape: true

              }

                );


      var DataGrid = jQuery('#jQGridDemoBill');
      DataGrid.jqGrid('setGridWidth', '700');


      }


	  //function BindGrid21() {
		  
		 // jQuery("#jQGridDemoItem").GridUnload();

		 // jQuery("#jQGridDemoItem").jqGrid({
			//  url: 'handlers/ManageProducts.ashx',
			//  ajaxGridOptions: { contentType: "application/json" },
			//  datatype: "json",

			//  colNames: ['ItemCode', 'ItemName', 'MRP', 'SaleRate', 'Tax'],
			//  colModel: [
				 
			//	  { name: 'Item_Code', key: true, index: 'Item_Code', width: 50, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
				 
			//	  { name: 'Item_Name', index: 'Item_Name', width: 80, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
			//	  { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 30, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
			//	  // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
			//	  { name: 'Sale_Rate', index: 'Sale_Rate', width: 30, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
			//	  { name: 'Tax', index: 'Tax', width: 30, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
				 

				 

			//  ],

			//  rowNum: 10,

			//  mtype: 'GET',
			//  toolbar: [true, "top"],
			//  loadonce: true,
			//  rowList: [10, 20, 30],
			//  pager: '#jQGridDemoPagerItem',
			//  sortname: 'Item_Code',
			//  viewrecords: true,
			//  height: "100%",
			//  width: "1100px",
			//  sortorder: 'desc',
			//  ignoreCase: true,
			//  caption: "Products List",




		 // });

		 // var $grid = $("#jQGridDemoItem");
		 // // fill top toolbar
		 // $('#t_' + $.jgrid.jqID($grid[0].id))
			//  .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText3\" type=\"text\"></input>&nbsp;<button id=\"globalSearch3\" type=\"button\">Search</button></div>"));
		 // $("#globalSearchText3").keypress(function (e) {
			//  var key = e.charCode || e.keyCode || 0;
			//  if (key === $.ui.keyCode.ENTER) { // 13
			//	  $("#globalSearch3").click();
			//  }
		 // });
		 // $("#globalSearch3").button({
			//  icons: { primary: "ui-icon-search" },
			//  text: false
		 // }).click(function () {
			//  var postData = $grid.jqGrid("getGridParam", "postData"),
			//	  colModel = $grid.jqGrid("getGridParam", "colModel"),
			//	  rules = [],
			//	  searchText = $("#globalSearchText3").val(),
			//	  l = colModel.length,
			//	  i,
			//	  cm;
			//  for (i = 0; i < l; i++) {
			//	  cm = colModel[i];
			//	  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
			//		  rules.push({
			//			  field: cm.name,
			//			  op: "cn",
			//			  data: searchText
			//		  });
			//	  }
			//  }
			//  postData.filters = JSON.stringify({
			//	  groupOp: "OR",
			//	  rules: rules
			//  });
			//  $grid.jqGrid("setGridParam", { search: true });
			//  $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
   //           return false;

		 // });

		 


		 // $("#jQGridDemoItem").jqGrid('setGridParam',
			//  {

			//	  onSelectRow: function (rowid, iRow, iCol, e) {


			//		  var itemcode = $('#jQGridDemoItem').jqGrid('getCell', rowid, 'Item_Code');
                      

			//		  selected_prdct(itemcode)

					 

			//	  }
			//  });





		 //  $('#jQGridDemoItem').jqGrid('navGrid', '#jQGridDemoPagerItem',
			//   {
			//	   refresh: false,
			//	   edit: false,
			//	   add: false,
			//	   del: false,
			//	   search: false,
			//	   searchtext: "Search",
			//	   addtext: "Add",
			//   },

			//   {//SEARCH
			//	   closeOnEscape: true

			//   }

		 //  );


		 //  var DataGrid = jQuery('#jQGridDemoItem');
		 //  DataGrid.jqGrid('setGridWidth', '700');


	  // }




      //////////////////////////////item Search with Up and Down Arrow Keys////////

	  function BindGridProducts(stext) {
		  
		  var list = $("#jQGridProduct");
		  jQuery("#jQGridProduct").GridUnload();

		  jQuery("#jQGridProduct").jqGrid({
			  url: 'handlers/CProductsSearch.ashx?stext=' + stext,
			  ajaxGridOptions: { contentType: "application/json" },
			  datatype: "json",

			  colNames: ['Code', 'Name', 'Rate', 'MRP', 'TaxId', 'Tax', 'Hsn', 'Unit'
			  ],
			  colModel: [
				  { name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, hidden: false },
				  { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, hidden: false },
                  { name: 'Sale_Rate', index: 'Sale_Rate', width: 50, stype: 'text', sortable: true, hidden: false },
				  { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 50, stype: 'text', sortable: true, hidden: false },
				  { name: 'Tax_ID', index: 'Tax_ID', width: 50, stype: 'text', sortable: true, hidden: true },
				  { name: 'Tax_Code', index: 'Tax_Code', width: 50, stype: 'text', sortable: true, hidden: false },
				  { name: 'hsncode', index: 'hsncode', width: 50, stype: 'text', sortable: true, hidden: true },
				  { name: 'Sales_In_Unit', index: 'Sales_In_Unit', width: 50, stype: 'text', sortable: true, hidden: true },

			  ],
			  //rowNum: 10,
			  mtype: 'GET',
			  loadonce: true,
			  //toppager: true,
			  //rowList: [10, 20, 30],
			  //pager: '#jQGridProductPager',
			  sortname: 'Item_Code',
			  viewrecords: true,
			  height: "100%",
			  width: "800px",

			  sortorder: 'desc',
			  caption: "",
			  editurl: 'handlers/CProductsSearch.ashx',

			  //toolbar: [true, "top"],
              ignoreCase: true,
              


		  });

		 


		  var $grid = $("#jQGridProduct");
		  




		  $("#jQGridProduct").jqGrid('setGridParam',
			  {
				  ondblClickRow: function (rowid, iRow, iCol, e) {

                     
					  var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
					  GetByItemCode(serviceId);
					  $("#ItemGrid1").dialog("close");
					  event.stopPropagation();
					  isfirst = 1;
					  isupfirst = 1;
					  selrowf = true;

				  }
			  });





		

		  list.jqGrid('gridResize');
		  list.jqGrid('bindKeys');

		  var DataGrid = jQuery('#jQGridProduct');


		  DataGrid.jqGrid('setGridWidth', '400');
		  DataGrid.jqGrid('setSelection', 1, true);
		  //list.setSelection("selectRow", 0);



	  }


	  var selrowf = true;

	  var isfirst = 0;
	  var isupfirst = 0;
	  var currow = 0;
	  $(document).keydown(function (e) {
		  switch (e.which) {
      //        case 40: // down
				  
      //            if (selrowf == true) {
                      
					 // $("#jQGridProduct").setSelection(1);
					 // selrowf = false;
				  //}
				  //var list = $('#jQGridProduct'),

					 // $td = $(e.target).closest("tr.jqgrow>td"),
					 // p = list.jqGrid("getGridParam"),
					 // //cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
					 // cm = "Item_Code";

				  //var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


				  //var selectedRow = list.jqGrid('getGridParam', 'selrow');


				  //if (isfirst == 0) {

					 // selectedRow = selectedRow - 1;

				  //}

				  //if (selectedRow == null) return;
				  //var ids = list.getDataIDs();
				  //var index = list.getInd(selectedRow);

				  //if (ids.length < 2) return;
				  //index++;
				  //list.setSelection(ids[index - 1], false, e);
				  //currow = index;


				  ////alert(currow);
				  ////list.jqGrid('saveRow', selectedRow);
				  //////lastsel2 = selectedRow;
				  ////list.jqGrid('editRow', ids[index-1], {
				  ////	keys: true,
				  ////	focusField: cmName

				  ////});

				  //e.preventDefault();
				  //break;

			  //case 38:

				 // var list = $('#jQGridProduct'),

					//  $td = $(e.target).closest("tr.jqgrow>td"),
					//  p = list.jqGrid("getGridParam"),
					//  //cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
					//  cm = "Item_Code";

				 // var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


				 // var selectedRow = list.jqGrid('getGridParam', 'selrow');

				 // if (isupfirst == 0) {
					//  selectedRow = selectedRow + 1;

				 // }

				 // if (selectedRow == null) return;
				 // var ids = list.getDataIDs();

				 // var index = list.getInd(selectedRow);

				 // if (ids.length < 2) return;
				 // index--;

				 // list.setSelection(ids[index - 1], false, e);
				 // currow = index;

				  
				 // e.preventDefault();
				 // break;

			  //case 13:
				  
				 // if (e.ctrlKey && e.keyCode == 13)
				 // //if (e.keyCode == 13)
				 // {

					//  //var rowid = parseInt($(this).attr("id"));

					//  var rowid = currow;

					  
     //                 var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');

                      
     //                 GetByItemCode(serviceId);
					//  $("#ItemGrid1").dialog("close");
					//  isfirst = 1;
					//  isupfirst = 1;
					//  selrowf = true;
				 // }
			  //});

			  default:
				  return;
		  }
      });

	  function CheckMode(Mode) {
		  if (Mode == "Credit") {

			  // $("#codChk").hide();
			  //  $("#OnlineChk").hide();
			  $("#dvOuter_ddlCreditCustomers_pm").show();
			  $("#lblCashHeading_pm").text("Receipt Amt:");
			  $("#ddlbillttype_pm option[value='Credit']").prop("selected", true);
			  $("#txtCashReceived_pm").val("0").prop("readonly", false);
			  $("#txtCashReceived_pm").removeAttr("disabled");
			  $("#Text13_pm").val("0").prop("readonly", true);
			  $("#Text14_pm").val("").prop("readonly", true);
			  $("#Text15_pm").val("0").prop("readonly", true);
			  $("#Text16_pm").val("").prop("readonly", true);
			  $("#ddlType_pm").prop("disabled", true);
			  $("#ddlOtherPayment_pm").hide();
			  $("#rdoCOD_pm").hide();
			  $("#rdoOnline_pm").hide();
			  $("#Td1_pm").hide();
			  $("#txtCODAmount_pm").hide();
			  $("#COD_pm").hide();
			  $("#Online_pm").hide();

			  $("#txtpaymentAmount_pm").hide();

			  $("#No_pm").hide();
			  $("#Amt_pm").hide();
			  $("#Coupans_pm").hide();
			  $("#OP_pm").hide();
			  $("#coupan_pm").hide();
			  $("#holder_pm  input").hide();
			  $("#holder2_pm  input").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();

			  $("#ddlBank_pm").prop("disabled", true);
			  $("#ddlChosseCredit_pm").show();
			  $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
			  $("#Text13_pm").hide();
			  $("#ddlType_pm").hide();
			  $("#ddlBank_pm").hide();
			  $("#crcredit_pm").hide();
			  $("#Type_pm").hide();
			  $("#Bank_pm").hide();
			  $("#lblCashHeading_pm").hide();
			  $("#txtCashReceived_pm").hide();
			  $("#rdoCOD_pm").hide();
			  $("#rdoOnline_pm").hide();
			  $("#lblCashHeading_pm").show();
			  $("#txtCashReceived_pm").show();

		  }
		  else if (Mode == "CreditCard") {
			  var customerId = 0;
			  CrdCustSelId = 0;
			  CrdCustSelName = "";
			  $("#hdnCreditCustomerId_pm").val(0);
			  $("#lblCreditCustomerName_pm").text("");
			  CrdCustSelName = "";
			  $("#ddlCreditCustomers_pm").html("");
			  $("#txtddlCreditCustomers_pm ").val("");
			  $("#Coupans_pm").hide();
			  $("#OP_pm").hide();
			  $("#coupan_pm").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();
			  $("#COD_pm").hide();
			  $("#Online_pm").hide();
			  $("#rdoOnline_pm").hide();
			  $("#lblCashHeading_pm").text("Cash Rec:");
			  $("#creditCustomer_pm").css("display", "none");
			  $("#ddlType_pm").prop("disabled", true);
			  $("#ddlOtherPayment_pm").hide();
			  $("#txtpaymentAmount_pm").hide();
			  $("#rdoCOD_pm").hide();
			  $("#Td1_pm").hide();
			  $("#txtCODAmount_pm").hide();
			  $("#dvOuter_ddlCreditCustomers_pm ").hide();
			  $("#ddlChosseCredit_pm").hide();
			  $("#holder_pm  input").hide(); $("#holder2_pm  input").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();

			  //                   //$("#ddlOtherPayment").hide();

			  $("#txtCashReceived_pm ").val("0").prop("readonly", true);
			  $("#Text13_pm").removeAttr("disabled");
			  $("#Text13_pm").removeAttr("readonly");
			  //$("#Text13_pm").val("0").prop("readonly", false);
			  $("#Text14_pm").val("").prop("readonly", false);
			  $("#Text15_pm").val("0").prop("readonly", false);
			  $("#Text16_pm").val("").prop("readonly", false);
			  $("#ddlType_pm").prop("disabled", false);
			  $("#ddlBank_pm").prop("disabled", false);
			  $("#Text13_pm").val($("#txtFinalBillAmount_pm ").val());
			  $("#txtBalanceReturn_pm ").val(" ");
			  // $("#codChk").hide();
			  // $("#OnlineChk").hide();
			  $("#Text13_pm").show();
			  $("#ddlType_pm").show();
			  $("#ddlBank_pm").show();
			  $("#crcredit_pm").show();
			  $("#Type_pm").show();
			  $("#Bank_pm").show();
			  $("#lblCashHeading_pm").hide();
			  $("#txtCashReceived_pm").hide();
			  $("#rdoCOD_pm").hide();
			  $("#rdoOnline_pm").hide();
			  $("#lblCashHeading_pm").hide();
			  $("#txtCashReceived_pm").hide();
		  }

		  else if (Mode == "Cash") {


			  var customerId = 0;
			  CrdCustSelId = 0;
			  CrdCustSelName = "";
			  $("#hdnCreditCustomerId_pm").val(0);
			  $("#lblCreditCustomerName_pm").text("");
			  CrdCustSelName = "";
			  $("#ddlCreditCustomers_pm").html("");
			  $("#txtddlCreditCustomers_pm").val("");
			  $("#Coupans_pm").hide();
			  $("#OP_pm").hide();
			  $("#lblCashHeading_pm").text("Cash Rec:");
			  $("#creditCustomer_pm").css("display", "none");
			  $("#coupan_pm").hide();
			  $("#dvOuter_ddlCreditCustomers_pm").hide();
			  $("#ddlChosseCredit_pm").hide();
			  $("#ddlType_pm").prop("disabled", true);
			  $("#ddlOtherPayment_pm").hide();
			  $("#COD_pm").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();
			  $("#Online_pm").hide();
			  $("#txtpaymentAmount_pm").hide();
			  $("#rdoCOD_pm").hide();
			  $("#Td1_pm").hide();
			  $("#txtCODAmount_pm").hide();
			  //                   //$("#ddlOtherPayment").hide();
			  $("#holder_pm input").hide();
			  $("#holder2_pm input").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();

			  $("#rdoOnline_pm").hide();
			  //$("#txtCashReceived_pm").val("0").prop("disabled", false);
			  $("#txtCashReceived_pm").removeAttr("disabled");
			  $("#Text13_pm").val("0").prop("readonly", true);
			  $("#Text14_pm").val("").prop("readonly", true);
			  $("#Text15_pm").val("0").prop("readonly", true);
			  $("#Text16_pm").val("").prop("readonly", true);
			  $("#ddlType_pm").prop("disabled", true);
			  $("#ddlBank_pm").prop("disabled", true);
			  $("#txtCashReceived_pm").val($("#txtFinalBillAmount_pm").val());
			  $("#txtBalanceReturn_pm").val(" ");
			  // $("#codChk").hide();
			  // $("#OnlineChk").hide();
			  $("#Text13_pm").hide();
			  $("#ddlType_pm").hide();
			  $("#ddlBank_pm").hide();
			  $("#crcredit_pm").hide();
			  $("#Type_pm").hide();
			  $("#Bank_pm").hide();
			  $("#txtCashReceived_pm").show();
			  $("#lblCashHeading_pm").show();
			  $("#rdoCOD_pm").hide();
			  $("#rdoOnline_pm").hide();

		  }
		  else if (Mode == "OnlinePayment") {

			  var customerId = 0;
			  CrdCustSelId = 0;
			  CrdCustSelName = "";
			  $("#hdnCreditCustomerId_pm").val(0);
			  $("#lblCreditCustomerName_pm").text("");
			  CrdCustSelName = "";
			  $("#ddlCreditCustomers_pm").html("");
			  $("#txtddlCreditCustomers_pm").val("");

			  $("#lblCashHeading").text("Cash Rec:");
			  $("#creditCustomer").css("display", "none");
			  $("#coupan_pm").hide();
			  $("#dvOuter_ddlCreditCustomers_pm").hide();
			  $("#ddlChosseCredit_pm").hide();
			  $("#COD_pm").show();
			  $("#Online_pm").show();
			  $("#No_pm").show();
			  $("#Amt_pm").show();
			  $("#ddlOtherPayment_pm").show();
			  $("#txtpaymentAmount_pm").show();
			  //                            $("#rdoOnline").show();
			  //                               $("#rdoCOD").show();
			  if ($("#txtCODAmount_pm").val() != 0) {
				  onlineoption = "COD"


			  }
			  //$("#Coupans_pm").val("0").prop("readonly", true);
			  $("#Td1_pm").show();
			  $("#txtCODAmount_pm").show();
			  $("#holder2_pm").hide();
			  $("#holder_pm").hide();
			  $("#No_pm").hide();
			  $("#Amt_pm").hide();


			  $("#Coupans_pm").show();
			  $("#coupan_pm").show();
			  $("#OP_pm").show();
			  $("#txtCashReceived_pm").val("0").prop("readonly", true);
			  $("#Text13_pm").val("0").prop("readonly", true);
			  $("#Text14_pm").val("").prop("readonly", true);
			  $("#Text15_pm").val("0").prop("readonly", false);
			  $("#Text16_pm").val("").prop("readonly", false);
			  $("#ddlType_pm").prop("disabled", true);
			  $("#ddlBank_pm").prop("disabled", true);
			  $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
			  $("#codChk").show();
			  $("#OnlineChk").show();
			  // $("#codChk").hide();
			  // $("#OnlineChk").hide();
			  $("#Text13_pm").hide();
			  $("#ddlType_pm").hide();
			  $("#ddlBank_pm").hide();
			  $("#crcredit_pm").hide();
			  $("#Type_pm").hide();
			  $("#Bank_pm").hide();
			  $("#lblCashHeading_pm").hide();
			  $("#txtCashReceived_pm").hide();
			  $("#rdoCOD_pm").show();
			  $("#rdoOnline_pm").show();
		  }
	  }

	  function GetBillByBNF() {

		  var Mode = "";
		  //var BNF = $("#dvShow_pm").attr('dir');
		  var BNF = $("#txtBillNumber_pm").val();



		  $.ajax({
			  type: "POST",
			  data: '{ "BNF": "' + BNF + '"}',
			  url: "changePaymode.aspx/GetBillByBNF",
			  contentType: "application/json",
			  dataType: "json",
			  success: function (msg) {

				  var obj = jQuery.parseJSON(msg.d);
				  $("#rdoOnline_pm").prop('checked', false);
				  $("#rdoCOD_pm").prop('checked', false);

				  $("#<%=ddlOtherPayment_pm.ClientID %>").val(obj.BillData.cst_id);

						$("#ddlbillttype_pm").val(obj.BillData.BillMode);
						$("#txtFinalBillAmount_pm").val(obj.BillData.Net_Amount);
						if ($("#txtFinalBillAmount_pm").val() != 0) {

							$("#btnBillWindowOk_pm").removeAttr("disabled");

						}
						else {

							alert("BillNo Does Not Exists");
							return;

						}

						$("#Text13_pm").val(obj.BillData.CrCard_Amount);
						$("#ddlBank_pm").val(obj.BillData.CreditBank);
						if (obj.BillData.BillMode == "Credit") {

							$("#ddlChosseCredit_pm").val(obj.BillData.Customer_ID);
							$("#lblCreditCustomerName_pm").html(obj.BillData.Customer_Name);
							$("#creditCustomer_pm").css("display", "block");
							$("#dvOuter_ddlCreditCustomers_pm").show();
							$("#ddlChosseCredit_pm").show();
							$("#ddlcustName_pm").show();
							$("#txtCashReceived_pm").val(obj.BillData.Credit_Amount);
							Mode = "Credit";
							CheckMode(Mode)
						}
						else if (obj.BillData.BillMode == "OnlinePayment") {
							Mode = "OnlinePayment";
							CheckMode(Mode)
							var mode = "COD";

							//$("#txtpaymentAmount").val($("div[id='dvnetAmount']").html());
							if (obj.BillData.OnlinePayment > 0) {

								mode = "OnlinePayment";
							}

							// $("#txtCashReceived_pm").val(obj.BillData.OnlinePayment);
							if (mode == 'OnlinePayment') {

								$("#rdoOnline_pm").prop('checked', true);
								$("#txtCODAmount_pm").prop('checked', false);
								$("#txtpaymentAmount_pm").val(obj.BillData.OnlinePayment);
								$("#txtpaymentAmount_pm").attr('readonly', 'readonly');
								$("#txtCODAmount_pm").attr('readonly', 'readonly');
								$("#txtCODAmount_pm").val(0);
								$("#ddlcustName_pm").hide();
								if (obj.Amount != 0) {
									$("#txtBalanceReturn_pm").val(0);
								}
							}
							else if (mode == 'COD') {
								$("#rdoCOD_pm").prop('checked', true);
								$("#rdoOnline_pm").prop('checked', false);
								$("#txtCODAmount_pm").val(obj.BillData.Cash_Amount);
								$("#txtCODAmount_pm").attr('readonly', 'readonly');
								$("#txtpaymentAmount_pm").attr('readonly', 'readonly');
								$("#txtpaymentAmount_pm").val(0);
								if (obj.Amount != 0) {
									$("#txtBalanceReturn_pm").val(0);
								}
								$("#ddlcustName_pm").hide();
							}
						}
						else if (obj.BillData.BillMode == "Cash") {
							Mode = "Cash";
							CheckMode(Mode)
							$("#ddlcustName_pm").hide();
							$("#txtCashReceived_pm").val(obj.BillData.Cash_Amount);
						}
						else if (obj.BillData.BillMode == "CreditCard") {
							Mode = "CreditCard";
							CheckMode(Mode)
							$("#txtCashReceived_pm").val(obj.BillData.Cash_Amount);
						}


					},
					error: function (xhr, ajaxOptions, thrownError) {

						var obj = jQuery.parseJSON(xhr.responseText);
						alert(obj.Message);
					},
					complete: function () {

						if ($("#ddlbillttype_pm").val() == "Cash" || $("#ddlbillttype_pm").val() == "OnlinePayment") {
							$("#Text13_pm").attr("disabled", "disabled");
							$("#ddlBank_pm").attr("disabled", "disabled");
							$("#txtCashReceived_pm").removeAttr("disabled");
							$("#dvOuter_ddlCreditCustomers_pm").hide();
							$("#ddlChosseCredit_pm").hide();
							$("#ddlChosseCredit_pm").val(0);
						}
						else if ($("#ddlbillttype_pm").val() == "CreditCard") {

							$("#Text13_pm").removeAttr("disabled");
							$("#ddlBank_pm").removeAttr("disabled");
							$("#txtCashReceived_pm").attr("disabled", "disabled");
							$("#dvOuter_ddlCreditCustomers_pm").hide();
							$("#ddlChosseCredit_pm").hide();
							$("#ddlChosseCredit_pm").val(0);
							$("#ddlcustName_pm").hide();
						}
						else if ($("#ddlbillttype_pm").val() == "Credit") {
							$("#Text13_pm").attr("disabled", "disabled");
							$("#ddlBank_pm").attr("disabled", "disabled");
							$("#txtCashReceived_pm").removeAttr("disabled");
							$("#dvOuter_ddlCreditCustomers_pm").show();
							$("#ddlChosseCredit_pm").show();
							$("#ddlcustName_pm").show();
						}


						DEVBalanceCalculation_pm();

						if ($("#txtCODAmount_pm").val() != "0" || $("#txtpaymentAmount_pm").val() != "0") {
							$("#txtBalanceReturn_pm").val(0);
						}
						;
						$.uiUnlock();

					}

				});

	  };
      ///////////////////////////// end up and down arrows///////
  </script>

               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>

	
</asp:Content>

