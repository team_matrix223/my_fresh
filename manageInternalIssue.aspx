﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageInternalIssue.aspx.cs" Inherits="manageInternalIssue" %>

<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">


           <link href="css/customcss/Inventory.css" rel="stylesheet" />

      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
   
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
  
       <%-- <script type="text/javascript" src="js/SearchPlugin.js"></script>--%>
    <script src="js/SearchPluginRaw.js" type="text/javascript"></script>
     <link href="css/css.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">

        var m_RefNo = 0;
        var m_TransferID = 0;
        var ProductCollection = [];
        var mrpeditable = false;
        var saleRateeditable = false;
        $(document).ready(function () {
            // register jQuery extension
            jQuery.extend(jQuery.expr[':'], {
                focusable: function (el, index, selector) {
                    return $(el).is('a, button, :input, [tabindex]');
                }
            });
            var counter_cant = 0;
            $(document).on('keypress', 'input,select', function (e) {
                var focus_on;
                var counter;
                $(":focus").each(function () {
                    focus_on = this.name;
                    try {
                        counter = this.attributes.counter.value;
                    } catch (e) {
                        counter = "";
                    }

                });
				if (focus_on == "txtServiceId") {
					if (e.which == 13) {

						$("#txtQty" + counter).focus();
						$("#txtQty" + counter).select();
					}

				}


				if (focus_on == "txtQty") {
					if (e.which == 13) {

						addTR();
					}

				}
                //if (focus_on != "txtServiceId") {
                //    if (e.which == 13) {
                //        e.preventDefault();
                //        // Get all focusable elements on the page
                //        var $canfocus = $(':focusable');
                //        var index = $canfocus.index(document.activeElement) + 1;
                //        if (index >= $canfocus.length) index = 0;
                //        $canfocus.eq(index).focus();
                //        $canfocus.eq(index).select();
                //        if (focus_on == "txtTaxPer") {
                //            if ($("#tbProducts tr").length == counter) {

                //                addTR();
                //            }
                //            $("#txtServiceId" + counter + 1 + "").focus();

                //        }
                //    }
                //}
            });
            function OnLoadContent() {

               // for (var i = 0; i < 10; i++) {
                    addTR();
               // }

            };
        function ApplyRoles(Roles) {



            $("#<%=hdnRoles.ClientID%>").val(Roles);
       
        }
            ValidateRoles();

        function clsProduct() {

            this.Item_Code = "";
            this.ITEM_NAME = "";
            this.QTYISSUED = 0;
            this.RATE = 0;
            this.AMOUNT = 0;
          

        }

    

        function CommonCalculation() {

            var Amt = Number($("#txtQty").val()) * $("#txtRate").val();

            $("#txtAmount").val(Amt);
        }

                    function addTR() {
          //  var qtyVal = $("#txtQty" + $("#hdnCounter").val()).val();
            var counterId = Number($("#hdnCounter").val()) + 1;
            $("#hdnCounter").val(counterId);
            var tr = "";


            tr = "<tr><td><input type='text'  id='txtServiceId" + counterId + "'  counter='" + counterId + "'   class='form-control input-small cls_itemcode' name='txtServiceId' /></td>" +
              "<td><select id='ddlProducts" + counterId + "' counter='" + counterId + "'   name='ddlProducts' class='cls_ddlproduct' disabed = 'disabled'>" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +

                "<td><input type='text' id='txtQty" + counterId + "'  counter='" + counterId + "'  class='form-control input-small cls_qty' name='txtQty' /></td>" +

                 "<td><input type='text' id='txtRate" + counterId + "'  counter='" + counterId + "' readonly=readonly   class='form-control input-small  validate' name='txtRate'   value = '0.0'/></td>" +


                "<td style='display:none'><input type='text' id='txtMRP" + counterId + "'    counter='" + counterId + "' readonly=readonly  class='form-control input-small  validate float' name='txtMRP1'  value = '0.0'/></td>" +

               "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "' readonly=readonly   class='form-control input-small' name='txtAmount'  value='0.0' /></td>";
              



            tr = tr + "<td stytle='color:white'><div id='btnAddRow" + counterId + "' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

            tr = tr + "<td><div id='btnRemove" + counterId + "'  style='cursor:pointer' class='cls_remove' counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>";

            $("#tbProducts").append(tr);

            $("#txtServiceId" + counterId + "").focus();

   


        }
        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());

            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });

        $(document).on('click', '.fa-plus', function (e) {
            addTR();
        });
        $(document).on('click', '.cls_remove', function (e) {

            var counterId = $(this).attr('counter');
            var len = $("input[name='txtServiceId']").length;
            if (len == 1) {
                alert("Row deletion failed. Package must contain atleast one service");
                return;
            }
            var tr = $("#btnRemove" + counterId).closest("tr");
            tr.remove();
            CommonCalculations(counterId);


        });

        $(document).on('change', '.cls_ddlproduct', function (e) {
            var _counter = $(this).attr('counter');
            global_counterId = _counter;
            var ItemId = $("#ddlProducts" + _counter).val();
            var Rate = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("srate");
            var Item_code = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("pid");
            var mrp = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("mrp");

            $("#txtRate" + _counter).val(Rate);
            $("#txtServiceId" + _counter).val(Item_code);
            $("#txtMRP" + _counter + "").val(mrp);
            //GetfromStock(Item_code);
            //FillData(_counter, ItemId);

        });
        $(document).on('keypress', '.cls_itemcode', function (e) {
            if (e.which == 13) {
                GetfromStock($(this).val());
                var _counter = $(this).attr('counter');

                global_counterId = _counter;
                var ItemId = $('#ddlProducts' + _counter + ' option[pid=' + $(this).val() + ']').attr("value");

                $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').prop('selected', 'selected');
                var Rate = $('#ddlProducts' + _counter + ' option[pid=' + $(this).val() + ']').attr("srate");
                $("#txtRate" + _counter).val(Rate);

                // FillData($(this).attr('counter'), ItemId);
            }

        });
        $(document).on('keyup', '.cls_qty', function (e) {

            CommonCalculations($(this).attr('counter'));

        });
        function CommonCalculations(_counter) {
            var Rate = $("#txtRate" + _counter).val();
            var Qty = $("#txtQty" + _counter).val();
            $("#txtAmount" + _counter).val(Rate * Qty);

        }
        $(document.body).on('click', '.btn_select', function (e) {
            var currentRow = $(this).closest("tr");
            var mrp = currentRow.find(".cls_mrp").text();
            $("#txtMRP" + global_counterId + "").val(mrp);
            $("#txtQty" + global_counterId + "").select();
            global_counterId = "";
            $("#dvlast3Dialog").hide();


        });
        $("#closetast3").click(function () {

            $("#dvlast3Dialog").hide();
            $("#txtQty" + global_counterId + "").focus();


        });
        function GetfromStock(itemcode) {

            $.ajax({
                type: "POST",
				data: '{ "ItemCode": "' + itemcode + '", "billtype": "' + 0 + '"}',
				url: "BillScreenOption.aspx/GetByItemCode",
                contentType: "application/json",

                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
					
					if (obj.productData.Item_Name == "") {

                       

						CheckItemCode(itemcode, global_counterId);


						return;

					}
                    //else {
                    //    ddlServiceVal = itemcode;
                    //    $("#ddlProducts" + global_counterId + " option").removeAttr("selected");
                    //    $('#ddlProducts' + global_counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');

                    //    cost = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("cost");

                    //    amount = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("cost");

                    //    tax = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("tax");

                    //    mrp = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("mrp");

                    //    srate = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("srate");

                    //    unit = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("unit");

                    //    $("#txtAmount" + global_counterId).val(amount);
                    //    $("#txtRate" + global_counterId).val(cost);
                    //    $("#txtQty" + global_counterId).val("1");
                    //    $("#txtMRP" + global_counterId).val(mrp);
                    //    $("#txtSRate" + global_counterId).val(srate);
                    //    $("#txtTaxPer" + global_counterId).val(tax);
                    //    $("#txtFree" + global_counterId).val("0");
                    //    $("#txtDis1Per" + global_counterId).val("0.0");
                    //    $("#txtDis2Per" + global_counterId).val("0.0");
                    //    $("#txtUnit" + global_counterId).val(unit);

                    //    $("#txtServiceId" + global_counterId).val(ddlServiceVal);

                    //    if ($("#ddlProducts" + global_counterId).val() != "") {
                    //        $("#txtQty" + global_counterId).focus();
                    //    }
                    //}



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }




            });

            }

			function CheckItemCode(ItemCode, counterId) {
				$.ajax({
					type: "POST",
					data: '{"ItemCode": "' + ItemCode + '"}',
					url: "BillScreenOption.aspx/ChkItemCode",
					contentType: "application/json",
					dataType: "json",
					success: function (msg) {

						var obj = jQuery.parseJSON(msg.d);

						var isexist = obj.rtnval;

                        if (isexist == 1) {
                           
                            BindGridProducts(ItemCode, counterId);
                           
							$("#ItemGrid1").dialog({
								modal: true,
								width: 450
							});

							$("#ItemGrid1").parent().addClass('Itemgrid-pop');


						}
						else {

                            alert("Item Does not exists");
                            $("#txtServiceId" + counterId).focus();
							return;
						}

					},
					error: function (xhr, ajaxOptions, thrownError) {

						var obj = jQuery.parseJSON(xhr.responseText);
						alert(obj.Message);
					},
					complete: function () {


						$.uiUnlock();
					}

				});

			}

			var curcounter = 0;
			function BindGridProducts(stext, counterId) {
				curcounter = counterId;
				var list = $("#jQGridProduct");
				jQuery("#jQGridProduct").GridUnload();

				jQuery("#jQGridProduct").jqGrid({
					url: 'handlers/CProductsSearch.ashx?stext=' + stext,
					ajaxGridOptions: { contentType: "application/json" },
					datatype: "json",

					colNames: ['Code', 'Name', 'Cost', 'Tax', 'TaxId', 'MRP', 'Hsn', 'Unit'
					],
					colModel: [
						{ name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, hidden: false },
						{ name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, hidden: false },
						{ name: 'Sale_Rate', index: 'Sale_Rate', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'Tax_Code', index: 'Tax_Code', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'Tax_ID', index: 'Tax_ID', width: 50, stype: 'text', sortable: true, hidden: true },
						{ name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'hsncode', index: 'hsncode', width: 50, stype: 'text', sortable: true, hidden: true },
						{ name: 'Sales_In_Unit', index: 'Sales_In_Unit', width: 50, stype: 'text', sortable: true, hidden: true },

					],
					//rowNum: 10,
					mtype: 'GET',
					//loadonce: true,
					//toppager: true,
					//rowList: [10, 20, 30],
					//pager: '#jQGridProductPager',
					sortname: 'Item_Code',
					viewrecords: true,
					height: "100%",
					width: "800px",

					sortorder: 'desc',
					caption: "",
					editurl: 'handlers/CProductsSearch.ashx',

					//toolbar: [true, "top"],
					ignoreCase: true,



				});

				//$("#jQGridProduct tr:nth-child(2)").attr('aria-selected', true);

				//$("#jQGridProduct tr:nth-child(2)").addClass('ui-state-highlight');



				var $grid = $("#jQGridProduct");
				// fill top toolbar
				//$('#t_' + $.jgrid.jqID($grid[0].id))
				//	.append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchTextItem\" type=\"text\"></input>&nbsp;<button id=\"globalSearchItem\" type=\"button\">Search</button></div>"));
				//$("#globalSearchTextItem").keypress(function (e) {
				//	var key = e.charCode || e.keyCode || 0;
				//	if (key === $.ui.keyCode.ENTER) { // 13
				//		$("#globalSearchItem").click();
				//	}
				//});
				//$("#globalSearchItem").button({
				//	icons: { primary: "ui-icon-search" },
				//	text: false
				//}).click(function () {
				//	var postData = $grid.jqGrid("getGridParam", "postData"),
				//		colModel = $grid.jqGrid("getGridParam", "colModel"),
				//		rules = [],
				//		searchText = $("#globalSearchTextItem").val(),
				//		l = colModel.length,
				//		i,
				//		cm;
				//	for (i = 0; i < l; i++) {
				//		cm = colModel[i];
				//		if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
				//			rules.push({
				//				field: cm.name,
				//				op: "cn",
				//				data: searchText
				//			});
				//		}
				//	}
				//	postData.filters = JSON.stringify({
				//		groupOp: "OR",
				//		rules: rules
				//	});
				//	$grid.jqGrid("setGridParam", { search: true });ss
				//	$grid.trigger("reloadGrid", [{ page: 1, current: true }]);
				//	return false;
				//});




				$("#jQGridProduct").jqGrid('setGridParam',
					{
						ondblClickRow: function (rowid, iRow, iCol, e) {

							var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');

                            var ItemId = $('#ddlProducts' + counterId + ' option[pid=' + serviceId + ']').attr("value");

                            $('#ddlProducts' + counterId + '  option[value=' + serviceId + ']').prop('selected', 'selected');
                            var Rate = $('#ddlProducts' + counterId + ' option[pid=' + serviceId + ']').attr("srate");
							$("#txtRate" + counterId).val(Rate);


							$("#txtServiceId" + counterId).val(serviceId)

							if ($("#ddlProducts" + counterId).val() != "") {
								$("#txtQty" + counterId).focus();
							}



							$("#ItemGrid1").dialog("close");



						}
					});





				//$('#jQGridProduct').jqGrid('navGrid', '#jQGridProductPager',
				//	{
				//		refresh: false,
				//		edit: false,
				//		add: false,
				//		del: false,
				//		search: true,
				//		searchtext: "Search",
				//		addtext: "Add",
				//	},

				//	{//SEARCH
				//		//closeOnEscape: true

				//	});

				list.jqGrid('gridResize');
				list.jqGrid('bindKeys');

				var DataGrid = jQuery('#jQGridProduct');


				DataGrid.jqGrid('setGridWidth', '400');
				DataGrid.jqGrid('setSelection', 1, true);
				//list.setSelection("selectRow", 0);



			}

			var selrowf = true;

			var isfirst = 0;
			var isupfirst = 0;
            var currow = 0;


			$(document).on("keydown", "#jQGridProduct", function (e) {



				if (e.keyCode == 40) {



					//arrow("next");

					if (selrowf == true) {

						$("#jQGridProduct").setSelection(1);
						selrowf = false;

					}
					var list = $('#jQGridProduct'),

						$td = $(e.target).closest("tr.jqgrow>td"),
						p = list.jqGrid("getGridParam"),
						//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						cm = "Item_Code";

					var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					var selectedRow = list.jqGrid('getGridParam', 'selrow');


					if (isfirst == 0) {

						selectedRow = selectedRow - 1;

					}


					if (selectedRow == null) return;

					var ids = list.getDataIDs();
					var index = list.getInd(selectedRow);

					if (ids.length < 2) return;
					index++;

					list.setSelection(ids[index - 1], false, e);
					currow = index;

					var rows = document.querySelectorAll('#jQGridProduct tr');

					var line = document.querySelector(1);



					rows[line].scrollTop({
						behavior: 'smooth',
						block: 'nearest'
					});

					//                  var w = $(window);

					//                  var row = $('#jQGridProduct').find('tr').eq(line);

					//                  if (row.length) {

					//                      w.scrollTop(row.offset().top - (w.height / 2));
					//}
					e.preventDefault();




				}

				if (e.keyCode == 38) {


					//arrow("prev");
					var list = $('#jQGridProduct'),

						$td = $(e.target).closest("tr.jqgrow>td"),
						p = list.jqGrid("getGridParam"),
						//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						cm = "Item_Code";

					var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					var selectedRow = list.jqGrid('getGridParam', 'selrow');


					if (isupfirst == 0) {
						selectedRow = Number(selectedRow) + Number(1);

					}

					if (selectedRow == null) return;
					var ids = list.getDataIDs();

					var index = list.getInd(selectedRow);

					if (ids.length < 2) return;
					index--;

					list.setSelection(ids[index - 1], false, e);
					currow = index;

					var rows = document.querySelectorAll('#jQGridProduct tr');

					var line = document.querySelector(1);



					rows[line].scrollTop({
						behavior: 'smooth',
						block: 'nearest'
					});

					//var w = $(window);
					//var row = $('#jQGridProduct').find('tr').eq(line);

					//                  if (row.length) {
					//                      list.scrollTop(row.offset().top + (12/ 2));
					//}
					e.preventDefault();


				}

				if (e.ctrlKey && e.keyCode == 13) {

					var rowid = currow;

					var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');

					var ItemId = $('#ddlProducts' + curcounter + ' option[pid=' + serviceId + ']').attr("value");

					$('#ddlProducts' + curcounter + '  option[value=' + serviceId + ']').prop('selected', 'selected');
					var Rate = $('#ddlProducts' + curcounter + ' option[pid=' + serviceId + ']').attr("srate");
					$("#txtRate" + curcounter).val(Rate);

					$("#txtServiceId" + curcounter).val(serviceId)

					if ($("#ddlProducts" + curcounter).val() != "") {
						$("#txtQty" + curcounter).focus();
					}

					$("#ItemGrid1").dialog("close");


					$("#txtQty" + curcounter).focus();

					event.stopPropagation();
					isfirst = 1;
					isupfirst = 1;
					selrowf = true;
				}


				// }

			});



        function BindGridData(_counter) {


            var ItemId = $("#ddlProducts" + _counter).val();
            if (ItemId != "") {


                TO = new clsProduct();

                TO.Item_ID = $("#ddlProducts" + _counter).val();
                TO.Item_Code = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("pid");
                TO.Item_Name = $('#ddlProducts' + _counter + ":selected").text();
				TO.QTYISSUED = $("#txtQty" + _counter).val();
                TO.Rate = $("#txtRate" + _counter).val();
                //TO.MRP = $("#txtMRP" + _counter).val();
                TO.Amount = $("#txtAmount" + _counter).val();
                TO.Tax_ID = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("taxid");
                TO.Tax_Rate = $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').attr("tax_code");
               
                ProductCollection.push(TO);
            }
        }
        function BindRows() {

            var TotalAmount = 0;
            var html = "";
            for (var i = 0; i < ProductCollection.length; i++) {


                html += "<tr>";
                html += "<td>" + ProductCollection[i]["ITEM_NAME"] + "</td>";
                html += "<td>" + ProductCollection[i]["QTYISSUED"] + "</td>";
                html += "<td>" + ProductCollection[i]["RATE"] + "</td>";
                //html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
                html += "<td>" + ProductCollection[i]["AMOUNT"] + "</td>";

                html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
                html += "</tr>";

                TotalAmount += parseFloat(ProductCollection[i]["Amount"])

            }

            $("#txtTotalAmount").val(TotalAmount);

            $("#tbKitProducts").html(html);

        }


        function ResetControls() {

            m_TransferID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtRefNo").val("Auto");
            $("#txtDate").removeAttr("disabled");
            $("#txtRemarks").val("");
            ProductCollection = [];
            ResetList();



        }

        function ResetList() {

            $("#txtCode").val("");
            $("#txtName").val("");
            $("#txtQty").val("");
            $("#txtRate").val("");
            $("#txtMarketPrice").val("");
            $("#txtAmount").val("");

            $("#ddlProducts").html("<option value='0'></option>");
            $("#txtddlProducts").val("").focus();





        }

        function GetPluginData(Type) {

            if (Type == "Product") {

                $("#txtCode").val($("#ddlProducts option:selected").attr("item_code"));
                $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
                $("#txtQty").val("").focus();
                $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
                $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
                $("#txtAmount").val(0);



            }
        }


            //$("#btnEdit").css({ "display": "none" });
            //$("#btnDelete").css({ "display": "none" });
            //$("#btnNew").css({ "display": "none" });

            $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());

			$("#txtDate").val($("#<%=hdntodaydate.ClientID %>").val());

            $("#btnGo").click(
                function () {


                    BindGrid();
                }
                );


            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });



            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $("#txtDate").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

            BindGodown();
            function BindGodown() {



                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "managedeliverynote.aspx/BindDealers",
                    data: {},
                    dataType: "json",
                    success: function (msg) {


                        var obj = jQuery.parseJSON(msg.d);


                        var html1 = "<option value = 0>--SELECT--</option>";

                        for (var i = 0; i < obj.GodownOptions.length; i++) {

                            html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                        }

                        $("#ddlGodown").html(html1);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                        $.ajax({
                            type: "POST",
                            data: '{ }',
                            url: "managedeliverynote.aspx/GetDefaultGodown",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);



                                $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                              //  $("#ddlGodown").prop("disabled", true);

                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {



                            }

                        });



                    }
                });

            }

			$(document).keydown(function (e) {
				if (e.keyCode == 27) {
					//return false;
				}


			});
			$("#tbProducts").keydown(function (e) {

				var cellindex = $(this).parents('td').index();
				cellindex = cellindex + 1;
				if (e.which == 40) {

					$(e.target).closest('tr').nextAll('tr').find('td').eq(cellindex).find(':text').focus();
				}
				if (e.which == 38) {

					$(e.target).closest('tr').prevAll('tr').first().find('td').eq(cellindex).find(':text').focus();
				}

			});



            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managekits.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    mrpeditable = obj.Mrp;
                    saleRateeditable = obj.Sale;


                    if (saleRateeditable == "0") {

                        $("#txtRate").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtRate").removeAttr('disabled');
                    }

                    if (mrpeditable == "0") {
                        $("#txtMarketPrice").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtMarketPrice").removeAttr('disabled');
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });


            ResetList();

            $("#btnCancelDialog").click(function () {
                window.location = "manageInternalIssue.aspx";

            });



            $("#btnSave").click(function () {
                 var Master_Code = "";

                 $(".cls_ddlproduct").each(function () {
                     counter_id = $(this).attr('counter');
                     BindGridData(counter_id);
                 });
      


                 var m_GodownId = $("#ddlGodown").val();
               

                 var m_Date = $("#txtDate").val();
                 var Department = $("#<%=ddlDept.ClientID %>").val();
                 var Remarks = $("#txtRemarks").val();
                


                 var DTO = { 'objDailyTransfer': ProductCollection, 'RefNo': m_TransferID, 'GodownId': m_GodownId, 'Date': m_Date, 'Department': Department, 'Remarks': Remarks };


                 $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "manageInternalIssue.aspx/InsertUpdate",
                     data: JSON.stringify(DTO),
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);

                         if (obj.status == -11) {
                             alert("You don't have permission to perform this action..Consult Admin Department.");
                             return;
                         }


                         if (m_TransferID == 0) {

                             alert("Internal Issue Added Successfully");

                         }
                         else {

                             alert("Internal Issue Updated Successfully");

                         }

                         BindGrid();
                         $("#TransferDialog").dialog("close");


                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {


                     }
                 });



             });


            $("#btnAddKitItems").click(function () {
                if (!(Number($("#txtQty").val()) > 0)) {
                    $("#txtQty").focus();
                    return;

                }
                


                TO = new clsProduct();

                TO.Item_ID = $("#ddlProducts option:selected").val();
                TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
                TO.ITEM_NAME = $("#ddlProducts option:selected").attr("item_name");
                TO.QTYISSUED = $("#txtQty").val();
                TO.RATE = $("#txtRate").val();
                TO.AMOUNT = $("#txtAmount").val();
                TO.Tax_ID = $("#ddlProducts option:selected").attr("tax_id")
                TO.Tax_Rate = $("#ddlProducts option:selected").attr("tax_code");

                ProductCollection.push(TO);
             


                BindRows();
                ResetList();

            }

            );



            $("#txtQty").keyup(function () {

                CommonCalculation();

            });

            $("#txtRate").keyup(function () {
                CommonCalculation();


            });





            $("#ddlProducts").supersearch({
                Type: "Product",
                Caption: "Please enter Item Name/Code ",
                AccountType: "",
                Width: 214,
                DefaultValue: 0,
                Godown: 0
            });





            BindGrid();


         

            function ValidateRoles() {

                var arrRole = [];
                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
                for (var i = 0; i < arrRole.length; i++) {

                    if (arrRole[i] == "9") {

                        $("#btnNew").show();

                        $("#btnNew").click(function () {


                                             ResetControls();
                                             $("#TransferDialog").dialog({
                                                 autoOpen: true,
                                                 closeOnEscape:false,
                                                 width: 1000,
                                                 resizable: false,
                                                 modal: true
                                             });

                                             OnLoadContent();
                                         });
                    }



                    else if (arrRole[i] == "3") {
                        $("#btnEdit").show();

                        $("#btnEdit").click(function () {


             $("#txtDate").prop("disabled", true);
             $.ajax({
                 type: "POST",
                 contentType: "application/json; charset=utf-8",
                 url: "manageInternalIssue.aspx/GetById",
                 data: '{"RefNo":"' + m_TransferID + '"}',
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

       $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
             for (var i = 0; i < obj.TransferDetail.length; i++) {
                 var tr = "";


                 tr = "<tr><td><input type='text'  id='txtServiceId" + i + "'  counter='" + i + "'   class='form-control input-small cls_itemcode' name='txtServiceId'  value=" + obj.TransferDetail[i].Item_Code + "></td>" +
              "<td><select id='ddlProducts" + i + "' counter='" + i + "'   name='ddlProducts' class='cls_ddlproduct' disabled = 'disabled' >" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +

                     "<td><input type='text' id='txtQty" + i + "'  counter='" + i + "'  class='form-control input-small cls_qty' name='txtQty'   value=" + obj.TransferDetail[i].QTYISSUED + "></td>" +

                 "<td><input type='text' id='txtRate" + i + "'  counter='" + i + "' readonly=readonly   class='form-control input-small  validate' name='txtRate'  value=" + obj.TransferDetail[i].RATE + "></td>" +
                "<td style='display:none'><input type='text' id='txtMRP" + i + "'    counter='" + i + "' readonly=readonly  class='form-control input-small  validate float' name='txtMRP1'   value='0'></td>" +

               "<td><input type='text' id='txtAmount" + i + "'  counter='" + i + "' readonly=readonly   class='form-control input-small' name='txtAmount'  value=" + obj.TransferDetail[i].AMOUNT + "></td>";

            tr = tr + "<td stytle='color:white'><div id='btnAddRow" + i + "' style='cursor:pointer'  counter='" + i + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

            tr = tr + "<td><div id='btnRemove" + i + "'  style='cursor:pointer' class='cls_remove' counter='" + i + "'><i class='fa fa-remove'></i></div> </td></tr>";

            $("#tbProducts").append(tr);

            $("#txtServiceId" + i + "").focus();
            var itemid = $('#ddlProducts' + i + ' option[pid=' + obj.TransferDetail[i].Item_Code + ']').attr("value")
            $("#ddlProducts" + i + "").val(itemid);
   
             }
            // for (var i = 0; i < 10 - obj.TransferDetail.length; i++) {
                 addTR();
            // }
             $("#hdnCounter").val(obj.TransferDetail.length);

                     $("#TransferDialog").dialog({
                         autoOpen: true,

                         width: 1000,
                         resizable: false,
                         modal: true
                     });

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }
             });



         });
}

                    else if (arrRole[i] == "2") {

                        $("#btnDelete").show();

                        $("#btnDelete").click(
         function () {

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             if ($.trim(SelectedRow) == "") {
                 alert("No Production is selected to Delete");
                 return;
             }

             var RefNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ISSUENO')
             if (confirm("Are You sure to delete this record")) {



                 $.ajax({
                     type: "POST",
                     data: '{"RefNo":"' + RefNo + '"}',
                     url: "manageInternalIssue.aspx/Delete",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);

                         if (obj.Status == "0") {

                             alert("An error occured during Transaction. Please try again Later");

                         }
                         else {
                             alert("Internal Issue is Deleted successfully.");
                         }
                         BindGrid();

                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                     complete: function () {

                     }
                 });

             }


         }
         );


                    }
                }




            }




        });

    </script>
    <style>
        button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
            display: none;
        }
        .inventry_adddialog
        {
            min-height:610px !important;
            max-height:610px !important;
            height:610px !important;
            overflow:auto !important;
            background:#fff !important;

        }
        [name="txtServiceId"] {
            width: 130px;
        }
        [name="txtQty"] {
            width: 130px !important;
        }
        [name="txtRate"],[name="txtMRP1"],[name="txtAmount"] {
            width: 130px !important;
        }
        .date-label-manage-interal-issue {
            width: 90px;
        }
        .manage-interal-issue-top-tbl input, .manage-interal-issue-top-tbl select {
            width: 214px;
        }
        .remark-section-internal-issue {
            float: left;
            width: 100%;
            margin-top: 15px;
        }
        .rmrk-internal-issue {
            float: left;
            width: 50%;
        }
        .btn-save-internal-issue {
            float: right;
            width: 50%;
            text-align: right;
        }
        textarea#txtRemarks {
            width: 339px;
        }
        .recive-info-table-div {
            overflow: auto;
            float: left;
            width: 100%;
            min-height: 377px;
            max-height: 377px;
        }
        table#jQGridDemo td {
            text-align: center !important;
        }
        .page-title {
            padding: 0;
        }
        h2.manage_title_top{
            margin: 0px;
            font-size: 15px;
            font-weight:400;
        }
        @media(min-width:768px) and (max-width:991px) {
            [name="txtRate"], [name="txtMRP1"], [name="txtAmount"] {
                width: 90px !important;
            }

            [name="txtServiceId"] {
                width: 70px;
            }

            [name="txtQty"] {
                width: 70px !important;
            }

            .date-label-manage-interal-issue {
                width: 70px;
            }

            .input-td-manage-interal-issue {
                width: 170px;
                float: left;
            }

            .manage-interal-issue-top-tbl input, .manage-interal-issue-top-tbl select {
                width: 100%;
            }

            textarea#txtRemarks {
                width: 315px;
            }

            .recive-info-table-div {
                min-height: 347px;
                max-height: 347px;
            }
            .recive-info-table-div .manage_table_top td {
	            padding: 3px 10px;
	        }
            [name="txtServiceId"], [name="txtQty"], [name="txtRate"], [name="txtAmount"] {
	            height: 25px !important;
            }
        }
        @media(min-width:992px) and (max-width:1024px)
        {
            [name="txtServiceId"] {
	            width: 100px;
            }
            .recive-info-table-div {
                min-height: 347px;
                max-height: 347px;
            }
            .recive-info-table-div .manage_table_top td {
	            padding: 3px 10px;
	        }
        }
        @media(min-width:1025px) and (max-width:1300px)
        {
            .recive-info-table-div {
                min-height: 347px;
                max-height: 347px;
            }
            .recive-info-table-div .manage_table_top td {
	            padding: 3px 10px;
	        }
        }
    </style>
    <form runat="server" id="formID" method="post">

          <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <asp:HiddenField ID="hdnDate" runat="server" />
                   <asp:HiddenField ID="hdnCounter"  value="1" runat="server" ClientIDMode="Static"/>
          <asp:DropDownList ID="hdnProducts" Style="display: none" runat="server">
        </asp:DropDownList>
        <div class="right_col" role="main">
            <div class="">
                <div class="page-title">
                    <div class="title_left">
                        <h3>Internal Issue</h3>
                    </div>
                    <div class="x_panel">
                        <div class="form-group">
                            <div class="youhave">

                                  <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>
                                                    Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>



                                <table id="jQGridDemo">
                                </table>
                                <table cellspacing="0" cellpadding="0" style="margin-top: 5px">
                                    <tr>

                                        <td>
                                            <div id="btnNew" class="btn btn-primary">
                                                <i class="fa fa-external-link"></i>New
                                            </div>
                                        </td>

                                        <td>
                                            <div id="btnEdit" class="btn btn-success" >
                                                <i class="fa fa-edit m-right-xs"></i>Edit
                                            </div>
                                        </td>

                                        <td>
                                            <div id="btnDelete" class="btn btn-danger"  >
                                                <i class="fa fa-trash m-right-xs"></i>Delete
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div id="jQGridDemoPager">
                                </div>
                            </div>

                            <div id="ItemGrid1" title="Press CTRL+ENTER To Select Item" style="display:none">
						
							<table id="jQGridProduct">
												</table>
												<div id="jQGridProductPager">
												</div>
						</div>
                            <div class="inventry_adddialog" id="TransferDialog" style="display: none"  title="<%= Request.Cookies[Constants.BranchName].Value %>">

                         <table class="manage_table_top manage-interal-issue-top-tbl">
                             <tr>
                                <td style="padding:0px;">
                                  <h2 class="manage_title_top">Internal Issue Info</h2>
                                </td>
                              </tr>
                            <tr>
                                <td style="padding:2px 0 0px;" colspan="100%">
                                    <table>
                                        <tr>
                                            <td class="date-label-manage-interal-issue">
                                                <label class="control-label">Ref No: </label>
                                            </td>
                                            <td class="input-td-manage-interal-issue">
                                                <input type="text" id="txtRefNo" value="Auto" disabled="disabled" />
                                            </td>
                                            <td class="date-label-manage-interal-issue">
                                                <label class="control-label">Department: </label>
                                            </td>
                                            <td class="input-td-manage-interal-issue">
                                                <asp:DropDownList ID="ddlDept" runat="server"></asp:DropDownList>
                                            </td>
                                            <td class="date-label-manage-interal-issue">
                                                <label class="control-label">Godown: </label>
                                            </td>
                                            <td class="input-td-manage-interal-issue">
                                                <select id="ddlGodown"></select>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                                    
                            <tr>
                                <td style="padding:0 0px 2px;" colspan="100%">
                                    <table class="date-table-manage-interal-issue">
                                        <tr>
                                            <td class="date-label-manage-interal-issue">
                                                <label class="control-label">Date: </label>
                                            </td>
                                           <td class="input-td-manage-interal-issue">
                                                <input type="text" id="txtDate" />
                                            </td>
                                            <td>
                                            </td>
                                            <td>

                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>                                     
                          </table>    <div>
                                                          
                                                            <div id="dvlast3Dialog" style="position: fixed; display: none; background-color: rgb(76, 73, 77); color: white; border: solid 1px silver; border-radius: 10px; padding: 5px;">

                                                                <table class="table">
                                                                    <tr>
                                                                        <td colspan="8" style="text-align: center; font-weight: bold; text-decoration: underline; background-color: #1479b8;border-bottom: 1px solid #ddd; padding-top:10px; padding-bottom:10px;">Select Item From List</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Sr.no.</th>
                                                                        <th>Item Code</th>
                                                                        <th style='display:none'>MRP</th>
                                                                          <th>Qty</th>
                                                                        <th>Sale Rate</th>
                                                                              <th>Tax</th>
                                                                              <th>Unit</th>
                                                                      
                                                                      
                                                                    </tr>
                                                                    <tbody id="tbllastrec"></tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <button type="button" id="closetast3" class="btn btn-danger">Close</button>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                        </div>
                  
                                          <div class="recive-info-table-div">
                                                <table class="manage_table_top" style="margin: 0px;" id="tbProducts">
                                                        <thead>
                                                            <tr>
                                                                <th>Code</th>
                                                                <th>Product</th>
                                                           
                                                                <th>Qty</th>
                                                     
                                                                <th>Rate</th>
                                                 
                                                                <th style="display:none">MRP</th>
                                                                <th>Amount</th>
                                                        
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                    </table>

                                   </div>
                       <%--                                 <table class="manage_table_top">

                                                                        <thead>
                                                                            <tr>
                                                                                <th>Item/Code</th>
                                                                                <th>Code</th>
                                                                                <th>Name</th>
                                                                                <th>Qty</th>
                                                                                <th>Rate</th>
                                                                                
                                                                                <th>Amount</th>
                                                                                <th></th>
                                                                                <th></th>
                                                                            
                                                                            </tr>

                                                                        </thead>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td>
                                                                                   <select id="ddlProducts" class="form-control"></select>
                                                                                </td>
                                                                                <td>
                                                                                    <input type="text" id="txtCode" readonly="readonly" class="form-control customTextBox" />
                                                                                </td>
                                                                                <td>
                                                                                    <input id="txtName" type="text" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtQty" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtRate" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>
                                                                                <td>
                                                                                    <input type="text" id="txtAmount" class="form-control customTextBox" readonly="readonly" /></td>
                                                                                
                                                                                <td>
                                                                                    <button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td>
                                                                            </tr>
                                                                        </tbody>

                                                        </table>--%>


                            <%--                    <div class="manage_table_top" style="max-height: 200px; overflow-y: scroll; min-height: 200px;">
                                
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <th>Name</th>
                                                                    <th>Qty</th>
                                                                    <th>Rate</th>
                                                                    <th>Amount</th>
                                                                   
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbKitProducts">
                                                            </tbody>
                                                        </table>

                                                </div>--%>
                                <div class="remark-section-internal-issue">

                                    <div class="rmrk-internal-issue"> 
                                        <label class="control-label">Remark: </label>
                                        <textarea id="txtRemarks"></textarea>
                                    </div>
                                    <div class="btn-save-internal-issue">  
                                               <div id="btnSave" class="btn btn-primary inv_sub_canc"><i class="fa fa-save"></i> Save</div>
                                               <button id="btnCancelDialog" class="btn btn-danger inv_sub_canc"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                    </div>


                                 </div>    

                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->



            </div>
    </form>
    <script type="text/javascript">
        function BindGrid() {

            var DateFrom = $("#txtDateFrom").val();
            var DateTo = $("#txtDateTo").val();
            jQuery("#jQGridDemo").GridUnload();
            jQuery("#jQGridDemo").jqGrid({
                url: 'handlers/manageinternalissue.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['IssueNo', 'Date','Department','Passing','Remarks', 'Godown'],



                colModel: [
                            { name: 'ISSUENO', key: true, index: 'ISSUENO', width: 100, stype: 'text', sorttype: 'int' },

                            { name: 'strDate', index: 'strDate', width: 100, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'DepartmentName', index: 'DepartmentName', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                              { name: 'PASSING', index: 'PASSING', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                               { name: 'REMARKS', index: 'REMARKS', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                           { name: 'Godown', index: 'Godown ', width: 100, stype: 'text', sorttype: 'int', hidden: true },


                ],
                rowNum: 10,

                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'Kit_ID',
                viewrecords: true,
                height: "100%",
                width: "400px",
                sortorder: 'asc',
                caption: "Production List",

                editurl: 'handlers/ManageKits.ashx',
                ignoreCase: true,
                toolbar: [true, "top"],


            });

            var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



            $("#jQGridDemo").jqGrid('setGridParam',
    {
        onSelectRow: function (rowid, iRow, iCol, e) {
          

            m_TransferID = 0;


            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                   $("#btnEdit").css({ "display": "none" });
                   $("#btnDelete").css({ "display": "none" });
                   $("#btnNew").css({ "display": "none" });

                   for (var i = 0; i < arrRole.length; i++) {

                       if (arrRole[i] == 9) {

                           $("#btnNew").css({ "display": "block" });
                       }
                       if (arrRole[i] == 2) {

                           $("#btnDelete").css({ "display": "block" });
                       }
                       if (arrRole[i] == 3) {
                           $("#btnEdit").css({ "display": "block" });
                          
                       }



                   }


            m_TransferID = $('#jQGridDemo').jqGrid('getCell', rowid, 'ISSUENO');
            
            $("#txtDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strDate'));
            $("#ddlGodown option[value='" + $('#jQGridDemo').jqGrid('getCell', rowid, 'Godown_ID') + "']").prop("selected", true);
            $("#txtRefNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ISSUENO'));
            $("#<%=ddlDept.ClientID%> option[value='" + $('#jQGridDemo').jqGrid('getCell', rowid, 'DEPARTMENT') + "']").prop("selected", true);
         

            
            $("#txtRemarks").val($('#jQGridDemo').jqGrid('getCell', rowid, 'REMARKS'));
        }
    });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>

