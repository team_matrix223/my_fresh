﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="printpdf.aspx.cs" Inherits="printpdf" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <link href="css/customcss/setup.css" rel="stylesheet" />

 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   <script language="javascript" type="text/javascript">


      
    $(document).ready(
    function () {

            $("#txtBillNo").change(function () {
                var BillNO = $("#txtBillNo").val();
                
                var dbname = $("#<%=hdnDb.ClientID%>").val();
                var link = $("#<%=hdnurl.ClientID%>").val();
                var finallink = (link +'PDFFile/' + dbname + '/' + BillNO + '.pdf');
               

                window.open(finallink);
            });


			$("#btnPrint").click(function () {
				var BillNO = $("#txtBillNo").val();

				var dbname = $("#<%=hdnDb.ClientID%>").val();
				var link = $("#<%=hdnurl.ClientID%>").val();
				var finallink = (link + 'PDFFile/' + dbname + '/' + BillNO + '.pdf');


				window.open(finallink);
			});



    }
    );

</script>
<style>.setup_title
{
    background :#ffffff;
    color:#73879C;
}
select#cntAdmin_ddlAccount {
    border: 1px solid #DDE2E8;
    height: 25px !important;
}
.youhave {
    padding: 15px 0;
}
.manage-bank-x-panel-sec
{
    max-height: 293px;
    overflow: auto;
}
table#frmCity td {
    text-align: left;
}
@media(max-width:480px)
{
    #frmCity input[type="text"], #frmCity select {
	    width: 100% !important;
    }
}
@media(max-width:567px)
{
    .x_content {
		    overflow: auto;
        }
}
@media(min-width:992px) and (max-width:1200px)
{
    .manage-bank-x-panel-sec {
	    max-height: 215px;
	    overflow: auto;
    }
}
</style>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDb" runat="server"/>
      <asp:HiddenField ID="hdnurl" runat="server"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Print Bill</h3>
                        </div>
                       <!-- <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div> 
                            </div>
                        </div>-->
                    </div>
                    <div class="clearfix"></div>
                     
                    <div class="x_title setup_title">
                           
                             
                            <div class="clearfix"></div>
                        </div>
                    <div class="x_panel">
                        
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                     <tr><td class="headings">BillNo:</td><td> 
                         <input type="text"  name="txtBillNo" maxlength="50" class="form-control validate required alphanumeric"   data-index="1" id="txtBillNo" style="width: 213px"/>
                        <%-- <asp:TextBox ID="txtbillno" AutoPostBack="true" runat="server" PlaceHolder="Enter BillNo" OnTextChanged="txtchanged"></asp:TextBox>--%>
                                                          </td>

                         <td>  <button  class="button1" id="btnPrint">&nbsp Print</button></td>
                     </tr>
                      
               
                                        

                     </table>

                        </div>
                    </div>

                  <!--  <div class="x_title setup_title">
                            <h2>Manage Banks</h2>
                             
                            <div class="clearfix"></div>
                        </div> -->
                    

                     
                </div>
                <!-- /page content -->

            </div>

                <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter2" runat="server" />
                </footer>
                <!-- /footer content -->
 
</form>

            <script type="text/javascript">
              





    </script>
     

</asp:Content>

