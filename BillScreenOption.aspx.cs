﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using System.Data;

public partial class BillScreenOption : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    protected void Page_Load(object sender, EventArgs e)
    {
        // Response.Cookies[Constants.posid].Value = "2";

        Int32 Screen = 0;
        //Response.Cookies[Constants.posid].Value = "2";
        Int32 pos = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        Response.Cookies[Constants.posid].Value = pos.ToString();


        try
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies[Constants.ScreenNo].Value))
            {

                Screen = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.ScreenNo].Value);
            }
            else
            {
                Connection con1 = new Connection();
                SqlConnection con = new SqlConnection(con1.sqlDataString);
                SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + pos + "", con);
                DataTable dt = new DataTable();
                dt.Clear();
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());


                    Response.Cookies[Constants.ScreenNo].Value = Screen.ToString();
                }
                else
                {
                    Response.Write("<script>alert('PosId doesn't Exists')</script>");
                    return;
                }



            }
        }
        catch
        {
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + pos + "", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());


                Response.Cookies[Constants.ScreenNo].Value = Screen.ToString();
            }
            else
            {
                Response.Write("<script>alert('PosId doesn't Exists')</script>");
                return;
            }
        }


        if (!IsPostBack)
        {

           // lblpos.Text = pos.ToString();
            hdnScreen.Value = Screen.ToString();
            hdnDate.Value = DateTime.Now.ToShortDateString();
            ltDateTime.Text ="<span style='font-weight:bold;'>Today - "+ String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now)+"</span>" ;
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
			gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
         
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }
            BindCreditCustomers();
            ddcustomertype();
            BindDropDownList();
            BindOtherPayment();
        }

        CheckRole();
    }
    void BindOtherPayment()
    {

        msr.req = "bind_ddcustomer_onlinepay";
        DataTable dt = msr.bind_item_dd();
        ddlOtherPayment.DataSource = dt;
        ddlOtherPayment.DataTextField = "customer_name";
        ddlOtherPayment.DataValueField = "cst_id";
        ddlOtherPayment.DataBind();
        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose Paymode", "0");
        //ddlOtherPayment.Items.Insert(0, listItem1);


        msr.req = "bind_ddcustomer_onlinepay";
        DataTable dt1 = msr.bind_item_dd();
        ddlOtherPayment_pm.DataSource = dt1;
        ddlOtherPayment_pm.DataTextField = "customer_name";
        ddlOtherPayment_pm.DataValueField = "cst_id";
        ddlOtherPayment_pm.DataBind();

    }
    public void BindDropDownList()
    {

        DataSet ds = new CommonMasterDAL().GetAll();

        cm_ddlCities.DataSource = ds.Tables[0];
        cm_ddlCities.DataValueField = "CIty_ID";
        cm_ddlCities.DataTextField = "City_Name";
        cm_ddlCities.DataBind();

        cm_ddlArea.DataSource = ds.Tables[1];
        cm_ddlArea.DataTextField = "Area_Name";
        cm_ddlArea.DataValueField = "Area_ID";
        cm_ddlArea.DataBind();


        cm_ddlState.DataSource = ds.Tables[2];
        cm_ddlState.DataTextField = "State_Name";
        cm_ddlState.DataValueField = "STATE_ID";
        cm_ddlState.DataBind();



        cm_ddlPrefix.DataSource = ds.Tables[3];
        cm_ddlPrefix.DataTextField = "PROP_NAME";
        cm_ddlPrefix.DataValueField = "PROP_NAME";
        cm_ddlPrefix.DataBind();


    }



    [WebMethod]
    public static string InsertUpdate(string CustomerId, string CustomerName, decimal BillValue, decimal DiscountPer, decimal DiscountAmt,
       decimal AddTaxAmt, decimal NetAmt, string BillMode, string CreditBank, decimal CashAmt, decimal CreditAmt, decimal CrCardAmt, decimal RoundAmt,
       int CashCustCode, string CashCustName, int TableNo, decimal SerTax, string Remarks, Int32 OrderNo, string Type, Int32 EmpCode,
       string BillNowPrefix, string itemcodeArr,string itemnameArr, string mrpArr, string qtyArr, string priceArr, string taxArr, string orgsalerateArr, string SurPerArr,
       string AmountArr, string TaxAmountArr, string SurValArr, string ItemRemarksArr, string arrTaxden, string arrVatAmtden, string arrVatden,
       string arrSurden, decimal OnlinePayment, decimal DeliveryCharges, decimal KKCPer, decimal KKCAmt, decimal SBCPer, decimal SBCAmt,
       string BillRemarks, int cst_id, string Order_No,string CounterBillNo)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');



        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -11;
        }






        int Status = 0;
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //User objuser = new User()
        //{
        //    UserNo = UserNo

        //};

        //Int32 status2 = new UserBLL().chkIsLogin(objuser);

        string[] TotalTexAmount = TaxAmountArr.Split(',');
        decimal d;
        decimal[] ArrayTexAmount;
        if (TotalTexAmount.All(number => Decimal.TryParse(number, out d)))
        {
            ArrayTexAmount = Array.ConvertAll<string, decimal>(TotalTexAmount, Convert.ToDecimal);
            AddTaxAmt = ArrayTexAmount.Sum();
        }

        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            Bill_Value = BillValue,
            DiscountPer = DiscountPer,
            Less_Dis_Amount = DiscountAmt,
            Add_Tax_Amount = AddTaxAmt,
            Net_Amount = NetAmt,
            BillMode = BillMode,
            CreditBank = CreditBank,
            UserNO = UserNo,
            Bill_Type = 1,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CrCard_Amount = CrCardAmt,
            Round_Amount = RoundAmt,
            Bill_Printed = false,
            Passing = false,
            CashCust_Code = CashCustCode,
            CashCust_Name = CashCustName,
            Tax_Per = 0,
            R_amount = RoundAmt,
            tableno = TableNo,
            remarks = Remarks,
            servalue = SerTax,
            ReceiviedGRNNo = 0,
            EmpCode = EmpCode,
            OrderNo = OrderNo,
            BranchId = Branch,
            OnlinePayment = OnlinePayment,
            DeliveryCharges = DeliveryCharges,
            KKCPer = KKCPer,
            KKCAmt = KKCAmt,
            SBCPer = SBCPer,
            SBCAmt = SBCAmt,
            BillRemarks = BillRemarks,
            cst_id = cst_id,
            Order_No = Order_No,


        };
        //if (status2 != 0)
        //{
        string[] ItemCode = itemcodeArr.Split(',');
        string[] ItemName = itemnameArr.Split(',');
        string[] MRP = mrpArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] Tax = taxArr.Split(',');
        string[] OrgSaleRate = orgsalerateArr.Split(',');
        string[] SurPer = SurPerArr.Split(',');
        string[] Amount = AmountArr.Split(',');
        string[] TaxAmount = TaxAmountArr.Split(',');
        string[] SurVal = SurValArr.Split(',');
        string[] remarksdata = ItemRemarksArr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("ItemName");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("OrgSaleRate");
        dt.Columns.Add("SurPer");
        dt.Columns.Add("SurVal");
        dt.Columns.Add("Remarks");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");



        for (int i = 0; i < ItemCode.Length; i++)
        {
            // decimal TotalAmt=Convert.ToDecimal()
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = ItemCode[i];
            dr["ItemName"] = ItemName[i];
            dr["MRP"] = Convert.ToDecimal(MRP[i]);
            dr["Rate"] = Convert.ToDecimal(Price[i]);
            dr["Qty"] = Convert.ToDecimal(Qty[i]);
            dr["Amount"] = Convert.ToDecimal(Amount[i]);
            dr["Tax"] = Convert.ToDecimal(Tax[i]);
            dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
            dr["OrgSaleRate"] = Convert.ToDecimal(OrgSaleRate[i]);
            dr["SurPer"] = Convert.ToDecimal(SurPer[i]);
            dr["SurVal"] = Convert.ToDecimal(SurVal[i]);
            dr["Remarks"] = Convert.ToString(remarksdata[i]);
            dt.Rows.Add(dr);
        }


        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }


        if (objBill.BillNowPrefix == "")
        {
            Status = new BillBLL().Insertfornew(objBill, dt, dt1,CounterBillNo);
        }
        else
        {
            Status = new BillBLL().Updatefornew(objBill, dt, dt1);
        }
        //}
        //else
        //{
        //    Status = -5;


        //}
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Bill = objBill,
            BNF = objBill.BillNowPrefix,
            Status = Status
        };
        return ser.Serialize(JsonData);
    }






    [WebMethod]
    public static string GetByItemCode(string ItemCode, int billtype)
    {
        Product objProduct = new Product() { Item_Code = ItemCode };
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        
        
        new ProductBLL().GetByItemCodeforNewScreen(objProduct, Branch, billtype);

        var JsonData = new
        {
            productData = objProduct
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }




    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    public void ddcustomertype()
    {
        mst_customer_rate msr = new mst_customer_rate();
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Shop", "0");
        dd_customername.Items.Insert(0, listItem1);

    }
   
    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);



        ddlChosseCredit_pm.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit_pm.DataValueField = "CCODE";
        ddlChosseCredit_pm.DataTextField = "CNAME";
        ddlChosseCredit_pm.DataBind();
        ListItem li2 = new ListItem();
        li2.Text = "--Choose Customer--";
        li2.Value = "0";
        ddlChosseCredit_pm.Items.Insert(0, li2);


    }

    [WebMethod]
    public static string LoadUserControl(string counter)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
            (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }


    [WebMethod]
    public static string ChkUserLogin()
    {

        string rtnval = "1";
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Select 1 from userinfo where IsLogin='false' and User_ID=@Username", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Username", HttpContext.Current.Request.Cookies[Constants.EmployeeName].Value);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);

            if (dt.Rows.Count == 1)
            {
                rtnval = "0";
            }
            con.Close();
            return rtnval;
        }

    }


    [WebMethod]
    public static string ChkCounterBill(string billNo)
    {

        string rtnval = "1";
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
                
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter("Select * from CounterBill where BillNowPrefix = '"+billNo+"'", con);
            adb.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                string billremarks = dt.Rows[0]["BillRemarks"].ToString();
                if (billremarks == "")
                {
                    rtnval = "0";

                }
                else
                {

                    rtnval = "1";
                }
                
              
            }
           

            con.Close();

        }

        var JsonData = new
        {
            rtnval = rtnval
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }




    [WebMethod]
    public static string ChkBankCount()
    {

        string rtnval = "0";
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();

            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter("Select count(*) as countrow from prop_bank", con);
            adb.Fill(dt);

            if (dt.Rows.Count > 0)
            {

                rtnval = dt.Rows[0]["countrow"].ToString();


            }


            con.Close();

        }

        var JsonData = new
        {
            rtnval = rtnval
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }






    [WebMethod]
    public static string ChkItemCode(string ItemCode)
    {

        string rtnval = "1";
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Select 1 from Packing_Belongs where Item_Name LIKE '%' + @SearchText + '%' order by Item_Name", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@SearchText", ItemCode);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);

            if (dt.Rows.Count >= 1)
            {
                rtnval = "1";
            }
            else
            {

                rtnval = "0";
            }
            
            con.Close();
            
        }

        var JsonData = new
        {
            rtnval = rtnval
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetByMobileNo(string PhnNo)
    
    {
        Customers objCustomer = new Customers() { Contact_No = PhnNo };
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);


        new CustomerBLL().GetByMobileNo(objCustomer);

        var JsonData = new
        {
            CustomerData= objCustomer
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetProducts(string KeyWord)

    {
        
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);


        string servicedata =  new ProductBLL().KeywordSearch(KeyWord, "", Convert.ToString(Branch));


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;

        var JsonData = new
        {
            ProductOptions = servicedata,


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetAllProducts(string keyword)
    {
        StringBuilder strBuilder = new StringBuilder();
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        con.Open();

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        if (keyword == "")
        {
            SqlCommand cmd = new SqlCommand("select Item_code,Item_Name,Max_Retail_Price,Sale_Rate,Tax_Code from packing_belongs order by Item_Name", con);
            cmd.Parameters.AddWithValue("@keyword", keyword);
            cmd.CommandType = CommandType.Text;
           

            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                strBuilder.Append(string.Format("<tr style='cursor: pointer' onclick=selected_prdct('" + rd["Item_code"].ToString() + "')><td>" + rd["Item_code"].ToString() + "</td><td>" + rd["Item_Name"].ToString() + "</td><td>" + rd["Max_Retail_Price"].ToString() + "</td><td>" + rd["Sale_Rate"].ToString() + "</td><td>" + rd["Tax_Code"].ToString() + "</td></tr>"));

            }
        }
        else
        {
            SqlCommand cmd = new SqlCommand("select Item_code,Item_Name,Max_Retail_Price,Sale_Rate,Tax_Code from packing_belongs where item_code+Item_Name like '%" + keyword + "%' order by Item_Name", con);
            cmd.Parameters.AddWithValue("@keyword", keyword);
            cmd.CommandType = CommandType.Text;
           
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                strBuilder.Append(string.Format("<tr style='cursor: pointer' onclick=selected_prdct('" + rd["Item_code"].ToString() + "')><td>" + rd["Item_code"].ToString() + "</td><td>" + rd["Item_Name"].ToString() + "</td><td>" + rd["Max_Retail_Price"].ToString() + "</td><td>" + rd["Sale_Rate"].ToString() + "</td><td>" + rd["Tax_Code"].ToString() + "</td></tr>"));

            }

        }
       
        var JsonData = new {

            TableData = strBuilder.ToString()

        };
        con.Close();
        return ser.Serialize(JsonData);



    }

    [WebMethod]
    public static string GetLastBill()
    {
        Connection conn = new Connection();
        string billno = "";
        string netamt = "";
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("SELECT top 1 BillNoWPrefix,Net_Amount FROM Bill_Master where  userno=@userno ORDER BY Bill_No DESC", con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@userno", HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        SqlDataReader rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            billno= rd["BillNoWPrefix"].ToString();
            netamt= rd["Net_Amount"].ToString();

        }
        var JsonData = new
        {

            bill_no = billno,
            net_amt = netamt

        };
        con.Close();
        return ser.Serialize(JsonData);



    }
    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetCounterBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByCounterBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string Reprint(string PrintType, string BillNowPrefix)
    {        
        
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new CommonMasterDAL().Reprint(UserNo,PrintType,Branch,BillNowPrefix);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
             

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string UpdateGSTBill(string LocalOut, string BillNowPrefix, Boolean BillType, string CustomerId, string CustomerName)
    {

        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new BillDAL().UpdateGSTBill(LocalOut, BillNowPrefix, BillType, CustomerId, CustomerName, UserNo);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {


        };
        return ser.Serialize(JsonData);

    }
    
    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    protected void btnRefreshPrinter_Click(object sender, EventArgs e)
    {
        Connection con = new Connection();

        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("truncate table printer", conn);
            cmd.ExecuteNonQuery();

        }
    }

    [WebMethod]
    public static string Delete(string BillNowPrefix)
    {
        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');
        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new BillBLL().DeleteBill(objBill);
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


   
}