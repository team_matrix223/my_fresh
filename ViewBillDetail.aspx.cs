﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class BillScreen : System.Web.UI.Page
{
    public string m_getdate { get; set; }
    mst_customer_rate msr = new mst_customer_rate();
    Connection con = new Connection();
    Int32 finalBranch;
    protected void Page_Load(object sender, EventArgs e)
    {
       
       

        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
           // ltDateTime.Text = "<span style='font-weight:bold;'>Today - " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now) + "</span>";
            m_getdate = DateTime.Now.ToString("ddddd,MMMM-dd-yyyy");
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            Int32 pos = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
            Response.Cookies[Constants.posid].Value = pos.ToString();

            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

            //else {
            //    Process.Start("osk");
            //}
            BindUsers(Branch);
           // BindBranches();
            ddcustomertype();

            GetTodayData(Branch);

        }

        
    }

    protected void btnDetail_Click(object sender, EventArgs e)
    {
        
            Response.Redirect("BillScreen.aspx");
        
    }
    public void GetTodayData(Int32 Branch)
    {
       
            LblTodayCash.Visible = true;
            LblTodayTotalSale.Visible = true;
            LblTodayOnlineSale.Visible = true;
            LblTodayCrCardSale.Visible = true;
            lbltotl.Visible = true;
            lblcash.Visible = true;
            lblcrcrd.Visible = true;
            lblonline.Visible = true;
            Connection con = new Connection();
            using (SqlConnection conn = new SqlConnection(con.sqlDataString))
            {
               
                conn.Open();
            Int32 Branch12 = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            Int32 posid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.posid].Value);
            SqlCommand cmd = new SqlCommand("strp_Billtiles", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BranchId", Branch12);
                cmd.Parameters.AddWithValue("@pos_id", posid);    
            SqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    LblTodayCash.Text = rd["TodayCashSale"].ToString();
                    LblTodayTotalSale.Text = rd["TodayTotalSale"].ToString();
                    // LblTodayOnlineSaleOrder.Text = rd["TodayOnlineOrder"].ToString();
                    LblTodayOnlineSale.Text = rd["TodayOnlineSale"].ToString();
                    //LblTodayPendingSettelment.Text = rd["TodayPendingSettelment"].ToString();
                    //LblTodayOpenTables.Text = rd["TodayOpenTables"].ToString();
                    //LblTodayOpenKot.Text = rd["TodayOpenTables"].ToString();
                    LblTodayCrCardSale.Text = rd["TodayCrCardSale"].ToString();

                }



            }
        
       
    }

    //protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    int BranchId = Convert.ToInt32(ddlBranch.SelectedValue.ToString());
    //    if (BranchId == 0)
    //    {
    //     finalBranch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    //    }
    //    else
    //    {
    //        finalBranch = BranchId;

    //    }

    //    BindUsers(finalBranch);
    //    GetTodayData(finalBranch);

    //}

    //    void BindBranches()
    //{
    //    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    //    ddlBranch.DataSource = new BranchBLL().GetAllBranch();
    //    ddlBranch.DataValueField = "BranchId";
    //    ddlBranch.DataTextField = "BranchName";
    //    ddlBranch.DataBind();

    //}

    void BindUsers(Int32 Branch)
    {
        
        ddlUser.DataSource = new UserBLL().GetAll(Branch);
        ddlUser.DataValueField = "UserNo";
        ddlUser.DataTextField = "User_ID";
        ddlUser.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose User--";
        li1.Value = "0";
        ddlUser.Items.Insert(0, li1);

    }

    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        ddPos.DataSource = dt;
        ddPos.DataTextField = "title";
        ddPos.DataValueField = "posid";
        ddPos.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose POS--";
        li1.Value = "0";
        ddPos.Items.Insert(0, li1);
    }
    [WebMethod]
    public static string GetBillDetailByBillNowPrefixforView(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefixforview(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string ChkUserLogin()
    {
        string rtnval = "1";
        try
        {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("Select 1 from userinfo where IsLogin='false' and User_ID=@Username",con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@Username",HttpContext.Current.Request.Cookies[Constants.EmployeeName].Value);
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        if (dt.Rows.Count == 1)
        {

            rtnval = "0";
        }
        con.Close();

     
        }
        catch
        {

           
        }
        return rtnval;
    }

   

    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }



}