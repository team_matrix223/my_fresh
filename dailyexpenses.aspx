﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="dailyexpenses.aspx.cs" Inherits="dailyexpenses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#172D44;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }
        .right_col.daily-expenses-right-col
        {
            min-height:unset;
        }
        body
        {
            background: lavender !important;
        }
        .page-title .title_left {
                width: 100%;
		        padding-left:15px;
	            background :#1479B8;
	            color:#ffffff;
        }
        @media (max-width:480px)
        {
            .page-title .title_left
            {
                width:100%;
            }
            .daily-expenses-right-col #txtDescription {
                width: 100% !important;
            }
        }
        @media (min-width:481px) and (max-width:567px)
        {
            .daily-expenses-right-col #txtDescription {
                width: 100% !important;
            }
        }
        @media (min-width:568px) and (max-width:667px)
        {
            .daily-expenses-right-col #txtDescription {
                width: 300px !important;
            }
        }
        @media (min-width:668px) and (max-width:767px)
        {
            .daily-expenses-right-col #txtDescription {
                width: 350px !important;
            }
        }
    </style>
    
     <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col daily-expenses-right-col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Daily Expense</h3>
                        </div>
              <!--          <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel">
                      
                        <div class="x_content">

                         <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   

                             <tr>
                                 <td colspan="100%">
                                     <table class="table">
                                         <tr>
                                             <th> </th>
                                             <th>Description</th>
                                             <th>Amount</th>
                                          
                                            <th></th>
                                         </tr>
                                         <tr>
                                             <td></td>
                                             <td><input type="text" id="txtDescription" style="width:500px" class="form-control"/></td>
                                             <td><input type="text" id="txtAmount" class="form-control"/></td>
                                            


                                             <td><input type="button" class="btn btn-primary" id="btnAddExpense" style="padding:1px 10px 1px 10px" value="Add"/></td>

                                         </tr>

                                         <tbody id="tbItems">



                                         </tbody>
                                     </table>

                                 </td>


                             </tr>

                                   <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"   class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>Add Expense</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                                               

                              
                     

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>


 
</form>

     <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">

       var ProductCollection = [];
       function clsProduct() {
           this.Description = 0;
           this.Amount = 0;
         

       }





       $(document).ready(
    function () {







        Bind();


        function Bind() {

            $.ajax({
                type: "POST",
                data: '{ }',
                url: "dailyexpenses.aspx/GetById",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    ProductCollection = obj.ExpenseDetail;


                    BindRows();






                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });

        }

        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());

            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });

        function InsertUpdate() {

            if (ProductCollection.length == 0) {

                alert("Please add Expense Detail");
                $("#txtDescription").focus();
                return;
            }



            var objSettings = {};



            var DTO = { 'objExpenseDetail': ProductCollection };

            $.uiLock('');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "dailyexpenses.aspx/InsertUpdate",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {
                  
                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 1) {

                        $("#tbItems").html("");
                        ProductCollection = [];
                        alert("Expenses Saved Successfully.");
                        return;
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    Bind();
                    $.uiUnlock();
                }
            });

        }

        function BindRows() {

            var html = "";
            for (var i = 0; i < ProductCollection.length; i++) {




                html += "<tr><td>" + (i + 1) + "</td>";
                html += "<td>" + ProductCollection[i]["Description"] + "</td>";
                html += "<td>" + ProductCollection[i]["Amount"] + "</td>";
                html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
                html += "</tr>";



            }


            $("#tbItems").html(html);

        }

        function ResetList() {

            $("#txtDescription").val("").focus();
            $("#txtAmount").val("");


        }

        $("#btnAddExpense").click(
           function () {



               if ($("#txtDescription").val().trim() == "") {

                   $("#txtDescription").focus();
                   return;

               }

               if (isNaN($("#txtAmount").val()) || $("#txtAmount").val() == "") {
                   $("#txtAmount").focus();
                   return;
               }


               TO = new clsProduct();

               TO.Description = $("#txtDescription").val();
               TO.Amount = $("#txtAmount").val();


               ProductCollection.push(TO);

               BindRows();
               ResetList();

           }

           );



        $("#btnAdd").click(
        function () {

            InsertUpdate();
        }
        );


    });

       </script>





</asp:Content>

