﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccountSettingDAL
/// </summary>
public class AccountSettingDAL:Connection
{
    public SqlDataReader GetAccountSettings(NewAccounts objAccountSettings)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objAccountSettings.BranchId);



        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "setting_sp_GetAccountSettings", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int16 UpdateAccountSettings(NewAccounts objAccountSettings, DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[60];

        objParam[0] = new SqlParameter("@CN_Acc2DebitCode", objAccountSettings.CN_Acc2DebitCode);
        objParam[1] = new SqlParameter("@CN_Acc2DebitName", objAccountSettings.CN_Acc2DebitName);
        objParam[2] = new SqlParameter("@OrderSaleAcnt", objAccountSettings.OrderSaleAcnt);
        objParam[3] = new SqlParameter("@OrderSaleAcntName", objAccountSettings.OrderSaleAcntName);
        objParam[4] = new SqlParameter("@OrderAdvAcnt", objAccountSettings.OrderAdvAcnt);
        objParam[5] = new SqlParameter("@OrderAdvAcntName", objAccountSettings.OrderAdvAcntName);
        objParam[6] = new SqlParameter("@DebitNoteAcnt", objAccountSettings.DebitNoteAcnt);
        objParam[7] = new SqlParameter("@DebitNoteAcntName", objAccountSettings.DebitNoteAcntName);
        objParam[8] = new SqlParameter("@CompAcnt", objAccountSettings.CompAcnt);
        objParam[9] = new SqlParameter("@CompAcntName", objAccountSettings.CompAcntName);
        objParam[10] = new SqlParameter("@DisplayAcnt", objAccountSettings.DisplayAcnt);
        objParam[11] = new SqlParameter("@DisplayAcntName", objAccountSettings.DisplayAcntName);
        objParam[12] = new SqlParameter("@AdjAcnt", objAccountSettings.AdjAcnt);
        objParam[13] = new SqlParameter("@AdjAcntName", objAccountSettings.AdjAcntName);
        objParam[14] = new SqlParameter("@DiscntAcnt", objAccountSettings.DiscntAcnt);
        objParam[15] = new SqlParameter("@DiscntAcntName", objAccountSettings.DiscntAcntName);
        objParam[16] = new SqlParameter("@TcsAcnt", objAccountSettings.TcsAcnt);
        objParam[17] = new SqlParameter("@TcsAcntName", objAccountSettings.TcsAcntName);
        objParam[18] = new SqlParameter("@BillRoundAcnt", objAccountSettings.BillRoundAcnt);
        objParam[19] = new SqlParameter("@BillRoundAcntName", objAccountSettings.BillRoundAcntName);
        objParam[20] = new SqlParameter("@ServiceChrgAcnt", objAccountSettings.ServiceChrgAcnt);
        objParam[21] = new SqlParameter("@ServiceChrgAcntName", objAccountSettings.ServiceChrgAcntName);
        objParam[22] = new SqlParameter("@ServiceTaxAcnt", objAccountSettings.ServiceTaxAcnt);
        objParam[23] = new SqlParameter("@ServiceTaxAcntName", objAccountSettings.ServiceTaxAcntName);
        objParam[24] = new SqlParameter("@IncomeTaxAcnt", objAccountSettings.IncomeTaxAcnt);
        objParam[25] = new SqlParameter("@IncomeTaxAcntName", objAccountSettings.IncomeTaxAcntName);
        objParam[26] = new SqlParameter("@LabourAcnt", objAccountSettings.LabourAcnt);
        objParam[27] = new SqlParameter("@LabourAcntName", objAccountSettings.LabourAcntName);
        objParam[28] = new SqlParameter("@GstdedAcnt", objAccountSettings.GstdedAcnt);
        objParam[29] = new SqlParameter("@GstdedAcntName", objAccountSettings.GstdedAcntName);
        objParam[30] = new SqlParameter("@OtherdedAcnt", objAccountSettings.OtherdedAcnt);
        objParam[31] = new SqlParameter("@OtherdedAcntName", objAccountSettings.OtherdedAcntName);
        objParam[32] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[32].Direction = ParameterDirection.ReturnValue;
        objParam[33] = new SqlParameter("@UserId", objAccountSettings.UserId);
        objParam[34] = new SqlParameter("@SecurityAcnt", objAccountSettings.SecurityAcnt);
        objParam[35] = new SqlParameter("@SecurityAcntName", objAccountSettings.SecurityAcntName);
        objParam[36] = new SqlParameter("@ResrveAcnt", objAccountSettings.ResrveAcnt);
        objParam[37] = new SqlParameter("@ResrveAcntName", objAccountSettings.ResrveAcntName);

        objParam[38] = new SqlParameter("@JobwrkAcnt", objAccountSettings.JobwrkAcnt);
        objParam[39] = new SqlParameter("@JobwrkAcntName", objAccountSettings.JobwrkAcntName);
        objParam[40] = new SqlParameter("@RarAcnt", objAccountSettings.RarAcnt);
        objParam[41] = new SqlParameter("@RarAcntName", objAccountSettings.RarAcntName);
        objParam[42] = new SqlParameter("@SaleAcnt", objAccountSettings.SaleAcnt);
        objParam[43] = new SqlParameter("@SaleAcntName", objAccountSettings.SaleAcntName);
        objParam[44] = new SqlParameter("@PurchaseAcnt", objAccountSettings.PurchaseAcnt);
        objParam[45] = new SqlParameter("@PurchaseAcntName", objAccountSettings.PurchaseAcntName);

        objParam[46] = new SqlParameter("@JournalAcnt", objAccountSettings.JournalAcnt);
        objParam[47] = new SqlParameter("@JournalAcntName", objAccountSettings.JournalAcntName);
        objParam[48] = new SqlParameter("@MSEAcnt", objAccountSettings.MSEAcnt);
        objParam[49] = new SqlParameter("@MSEAcntName", objAccountSettings.MSEAcntName);
        objParam[50] = new SqlParameter("@OutpurAcnt", objAccountSettings.OutpurAcnt);
        objParam[51] = new SqlParameter("@OutpurAcntName", objAccountSettings.OutpurAcntName);
        objParam[52] = new SqlParameter("@OutTaxAcnt", objAccountSettings.OutTaxAcnt);
        objParam[53] = new SqlParameter("@OutTaxAcntName", objAccountSettings.OutTaxAcntName);
        objParam[54] = new SqlParameter("@CstSaleAcnt", objAccountSettings.CstSaleAcnt);
        objParam[55] = new SqlParameter("@CstSaleAcntName", objAccountSettings.CstSaleAcntName);
        objParam[56] = new SqlParameter("@CstsaleTaxAcnt", objAccountSettings.CstsaleTaxAcnt);
        objParam[57] = new SqlParameter("@CstsaleTaxAcntName", objAccountSettings.CstsaleTaxAcntName);
        objParam[58] = new SqlParameter("@BranchId", objAccountSettings.BranchId);
        objParam[59] = new SqlParameter("@dt", dt);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "settings_sp_NewInsertAccountSettings", objParam);
            retValue = Convert.ToInt16(objParam[32].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}