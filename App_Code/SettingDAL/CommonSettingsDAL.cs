﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for CommonSettingsDAL
/// </summary>
public class CommonSettingsDAL:Connection
{
    public SqlDataReader GetPuchaseGridOptionsByType(int BranchId,string Type)
    {
      
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Type", Type);
        objParam[1] = new SqlParameter("@BranchId", BranchId);


        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "settings_sp_GetPurchaseGridOptionsByType", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetDeliveryNoteGridSettings()
    {
        SqlParameter[] objParam = new SqlParameter[0];



        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "GetDeliveryNoteGridSettings", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }




     public Int16 UpdateOptions(string Query)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Query ", Query);
        
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
       
         try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "setting_sp_PurchaseGridOptionsUpdate", objParam);
            retValue = Convert.ToInt16(objParam[1].Value);
       
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



     public SqlDataReader GetKitSettings()
     {
         SqlParameter[] objParam = new SqlParameter[0];



         SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "GetKitSettings", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }

     public SqlDataReader GetPurchaseReceiptSettings(int Userid)
     {
         SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@userid ", Userid);

        SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "GetPurchaseReceiptGridSettings", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }


     public SqlDataReader GetPurchaseReturnSettings()
     {
         SqlParameter[] objParam = new SqlParameter[0];



         SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "GetPurchaseReturnGridSettings", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }

   

     public SqlDataReader GetAllBillSettings(CommonSettings objSetting)
     {
         SqlParameter[] objParam = new SqlParameter[2];

         objParam[0] = new SqlParameter("@Type", objSetting.Type);
         objParam[1] = new SqlParameter("@BranchId", objSetting.BranchId);

         SqlDataReader dr = null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "GetallBillSettings", objParam);


         }

         finally
         {
             objParam = null;
         }
         return dr;

     }
}