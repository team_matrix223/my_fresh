﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for AccountSettingBLL
/// </summary>
public class AccountSettingBLL
{
    public Int16 UpdateAccountSettings(NewAccounts objAccountSettings, DataTable dt)
    {

        return new AccountSettingDAL().UpdateAccountSettings(objAccountSettings,dt);
    }



    public void GetSettings(NewAccounts objAccountSettings)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new AccountSettingDAL().GetAccountSettings(objAccountSettings);
            if (dr.HasRows)
            {
                dr.Read();

                objAccountSettings.CN_Acc2DebitCode = Convert.ToString(dr["CN_Acc2DebitCode"]);
                objAccountSettings.CN_Acc2DebitName = Convert.ToString(dr["CN_Acc2DebitName"]);
                objAccountSettings.OrderSaleAcnt = Convert.ToString(dr["OrderSaleAccCode"]);
                objAccountSettings.OrderSaleAcntName = Convert.ToString(dr["OrderSaleAccName"]);
                objAccountSettings.OrderAdvAcnt = Convert.ToString(dr["OrderAdvAccCode"]);
                objAccountSettings.OrderAdvAcntName = Convert.ToString(dr["OrderAdvAccName"]);
                objAccountSettings.DebitNoteAcnt = Convert.ToString(dr["DebitNote_AccCode"]);
                objAccountSettings.DebitNoteAcntName = Convert.ToString(dr["DebitNote_AccName"]);
                objAccountSettings.CompAcnt = Convert.ToString(dr["Comp_ACCCode"]);
                objAccountSettings.CompAcntName = Convert.ToString(dr["Comp_ACCName"]);
                objAccountSettings.DisplayAcnt = Convert.ToString(dr["Disp_AccCode"]);
                objAccountSettings.DisplayAcntName = Convert.ToString(dr["Disp_AccName"]);
                objAccountSettings.AdjAcnt = Convert.ToString(dr["Adj_AccCode"]);
                objAccountSettings.AdjAcntName = Convert.ToString(dr["Adj_AccName"]);
                objAccountSettings.DiscntAcnt = Convert.ToString(dr["Disc_AccCode"]);
                objAccountSettings.DiscntAcntName = Convert.ToString(dr["Disc_AccName"]);
                objAccountSettings.TcsAcnt = Convert.ToString(dr["tcs_accCode"]);
                objAccountSettings.TcsAcntName = Convert.ToString(dr["tcs_accName"]);
                objAccountSettings.BillRoundAcnt = Convert.ToString(dr["Round_accCode"]);

                objAccountSettings.BillRoundAcntName = Convert.ToString(dr["Round_accname"]);
                objAccountSettings.ServiceChrgAcnt = Convert.ToString(dr["ServiceChgAccCode"]);
                objAccountSettings.ServiceChrgAcntName = Convert.ToString(dr["ServiceChgAccName"]);
                objAccountSettings.ServiceTaxAcnt = Convert.ToString(dr["ServiceTaxAccCode"]);
                objAccountSettings.ServiceTaxAcntName = Convert.ToString(dr["ServiceTaxAccName"]);
                objAccountSettings.IncomeTaxAcnt = Convert.ToString(dr["Incometax_Code"]);
                objAccountSettings.IncomeTaxAcntName = Convert.ToString(dr["Incometax_Name"]);
                objAccountSettings.LabourAcnt = Convert.ToString(dr["Labour_Code"]);
                objAccountSettings.LabourAcntName = Convert.ToString(dr["Labour_Name"]);
                objAccountSettings.GstdedAcnt = Convert.ToString(dr["Gst_Code"]);
                objAccountSettings.GstdedAcntName = Convert.ToString(dr["Gst_Name"]);
                objAccountSettings.OtherdedAcnt = Convert.ToString(dr["Other_Code"]);
                objAccountSettings.OtherdedAcntName = Convert.ToString(dr["Other_Name"]);

                objAccountSettings.SecurityAcnt = Convert.ToString(dr["Security_Code"]);
                objAccountSettings.SecurityAcntName = Convert.ToString(dr["Security_Name"]);
                objAccountSettings.ResrveAcnt = Convert.ToString(dr["Reserve_Code"]);
                objAccountSettings.ResrveAcntName = Convert.ToString(dr["Reserve_Name"]);
                objAccountSettings.JobwrkAcnt = Convert.ToString(dr["jobwrk_code"]);
                objAccountSettings.JobwrkAcntName = Convert.ToString(dr["jobwrk_Name"]);
                objAccountSettings.RarAcnt = Convert.ToString(dr["RAR_Code"]);
                objAccountSettings.RarAcntName = Convert.ToString(dr["RAR_Name"]);

                objAccountSettings.SaleAcnt = Convert.ToString(dr["Sale_Code"]);
                objAccountSettings.SaleAcntName = Convert.ToString(dr["Sale_Name"]);
                objAccountSettings.PurchaseAcnt = Convert.ToString(dr["PUR_Code"]);
                objAccountSettings.PurchaseAcntName = Convert.ToString(dr["PUR_Name"]);
                objAccountSettings.JournalAcnt = Convert.ToString(dr["JRNL_Code"]);
                objAccountSettings.JournalAcntName = Convert.ToString(dr["JRNL_Name"]);
                objAccountSettings.MSEAcnt = Convert.ToString(dr["MSE_Code"]);
                objAccountSettings.MSEAcntName = Convert.ToString(dr["MSE_Name"]);

                objAccountSettings.OutpurAcnt = Convert.ToString(dr["OSPurc_AccCode"]);
                objAccountSettings.OutpurAcntName = Convert.ToString(dr["OSPurc_AccName"]);
                objAccountSettings.OutTaxAcnt = Convert.ToString(dr["OSPurc_TaxCode"]);
                objAccountSettings.OutTaxAcntName = Convert.ToString(dr["OSPurc_TaxName"]);
                objAccountSettings.CstSaleAcnt = Convert.ToString(dr["CST_ACCCode"]);
                objAccountSettings.CstSaleAcntName = Convert.ToString(dr["CST_ACCName"]);
                objAccountSettings.CstsaleTaxAcnt = Convert.ToString(dr["CST_TaxCode"]);
                objAccountSettings.CstsaleTaxAcntName = Convert.ToString(dr["CST_TaxName"]);
                objAccountSettings.BankAccounts = Convert.ToString(dr["bnklist"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}