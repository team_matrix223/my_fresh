﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for CommonSettingsBLL
/// </summary>
public class CommonSettingsBLL
{
    public string GetPurchaseGridOptionsByType(int BranchId,string Type)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new CommonSettingsDAL().GetPuchaseGridOptionsByType(BranchId,Type);
            if (dr.HasRows)
            {

                while (dr.Read())
                {

                    string chkchecked = Convert.ToBoolean(dr["CEdit"]) == true ? "checked=checked" : "";

                    strBuilder.Append(string.Format("<tr ><td style='width:200px'><b>{0} :</b></td><td><input type='checkbox' name='settings' title='{0}' value='{1}' " + chkchecked + "/></td></tr>", dr["Title"].ToString(), dr["ColumnName"].ToString()));
                }
            }
            

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public void GetDeliveryNoteGridSettings(out string DRate, out string DSRate, out string DMRP)
    {
        DRate = "";
        DSRate = "";
        DMRP = "";

        SqlDataReader dr = null;
        try
        {
            dr = new CommonSettingsDAL().GetDeliveryNoteGridSettings();
            if (dr.HasRows)
            {
                dr.Read();
                DRate = Convert.ToString(dr["DRate"]);
                DSRate = Convert.ToString(dr["DSRate"]);
                DMRP = Convert.ToString(dr["DMRP"]);



            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public Int16 UpdateOptions(string Query)
    {
        return new CommonSettingsDAL().UpdateOptions(Query);
    }



    public void GetKitSettings(out string MrpEditable,out string SaleRateEditable)
    {
        MrpEditable = "";
        SaleRateEditable = "";
        SqlDataReader dr = null;
        try
        {
            dr =new CommonSettingsDAL().GetKitSettings();
            if (dr.HasRows)
            {
                dr.Read();
              
                MrpEditable = Convert.ToString(dr["KMRP"]);
                SaleRateEditable = Convert.ToString(dr["KSRate"]);
              

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

    public void GetPurchaseReturnSettings(out string RPRate, out string RSRate, out string RMRP, out string RAmt, out string RDis1, out string RDis2, out string RTax1, out string RTax2)
    {
        RPRate = "";
        RSRate = "";
        RMRP = "";
        RAmt = "";
        RDis1 = "";
        RDis2 = "";
        RTax1 = "";
        RTax2 = "";
        SqlDataReader dr = null;
        try
        {
            dr = new CommonSettingsDAL().GetPurchaseReturnSettings();
            if (dr.HasRows)
            {
                dr.Read();

                RPRate = Convert.ToString(dr["RPRate"]);
                RSRate = Convert.ToString(dr["RSRate"]);
                RMRP = Convert.ToString(dr["RMRP"]);
                RAmt = Convert.ToString(dr["RAmt"]);
                RDis1 = Convert.ToString(dr["RDis1"]);
                RDis2 = Convert.ToString(dr["RDis2"]);
                RTax1 = Convert.ToString(dr["RTax1"]);
                RTax2 = Convert.ToString(dr["RTax2"]);


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

    public void GetPurchaseReceiptSettings(out string PRate, out string PSRate, out string PMRP, out string PAmt, out string PDis1, out string PDis2, out string PTax1, out string PTax2,int AdminId)
    {
        PRate = "";
        PSRate = "";
        PMRP = "";
        PAmt = "";
        PDis1 = "";
        PDis2 = "";
        PTax1 = "";
        PTax2 = "";
        SqlDataReader dr = null;
        try
        {
            dr = new CommonSettingsDAL().GetPurchaseReceiptSettings(AdminId);
            if (dr.HasRows)
            {
                dr.Read();
                PRate = Convert.ToString(dr["PRate"]);
                PSRate = Convert.ToString(dr["PSRate"]);
                PMRP = Convert.ToString(dr["PMRP"]);
                PAmt = Convert.ToString(dr["PAmt"]);
                PDis1 = Convert.ToString(dr["PDis1"]);
                PDis2 = Convert.ToString(dr["PDis2"]);
                PTax1 = Convert.ToString(dr["PTax1"]);
                PTax2 = Convert.ToString(dr["PTax2"]);


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public List<DiscountDetail> GetAllBillSettings(CommonSettings objSettings)
    {

        SqlDataReader dr = null;
        try
        {
           
            dr = new CommonSettingsDAL().GetAllBillSettings(objSettings);

            if (dr.HasRows)
            {
                dr.Read();

                objSettings.retail_bill = Convert.ToString(dr["retail_bill"].ToString());
                objSettings.vat_bill = Convert.ToString(dr["vat_bill"].ToString());
                objSettings.ServiceTax = Convert.ToBoolean(dr["service_tax"].ToString());
                objSettings.SerTax_Per = Convert.ToDecimal(dr["Tax_Per"].ToString());
                objSettings.AlloServicetax_TakeAway = Convert.ToBoolean(dr["AlloServicetax_TakeAway"].ToString());
                objSettings.homedel_charges = Convert.ToBoolean(dr["homedel_charges"].ToString());
                objSettings.min_bill_value = Convert.ToDecimal(dr["min_bill_value"].ToString());
                objSettings.del_charges = Convert.ToDecimal(dr["del_charges"].ToString());
                objSettings.AllowKKC = Convert.ToBoolean(dr["AllowKKC"].ToString());
                objSettings.KKC = Convert.ToDecimal(dr["KKC"].ToString());
                objSettings.AllowSBC = Convert.ToBoolean(dr["AllowSBC"].ToString());
                objSettings.SBC = Convert.ToDecimal(dr["SBC"].ToString());
                objSettings.DefaultService = Convert.ToString(dr["DefaultService"].ToString());
                objSettings.PrintTime = Convert.ToBoolean(dr["PrintTime"].ToString());
                objSettings.NoOfCopy = Convert.ToInt32(dr["NoOfCopy"].ToString());
                objSettings.PrintKot_Reprint = Convert.ToBoolean(dr["PrintKot_Reprint"].ToString());
                objSettings.OpenDiv = Convert.ToBoolean(dr["OpenDiv"].ToString());
                objSettings.itemspos = Convert.ToBoolean(dr["itemspos"].ToString());
                objSettings.BillOption = Convert.ToString(dr["BillOption"].ToString());
                objSettings.PrintRoute= Convert.ToBoolean(dr["PrintRoute"].ToString());
                objSettings.PrintCoupon = Convert.ToBoolean(dr["CouponPrinting"].ToString());
            }
            
            dr.NextResult();
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.CashCustomer = Convert.ToBoolean(dr["CashCustomer"]);
                objSettings.StartCustomerPoint = Convert.ToBoolean(dr["StartCustomerPoint"]);
                objSettings.defaultpaymodeID = Convert.ToString(dr["defaultpaymodeID"]);
                objSettings.defaultpaymode = Convert.ToString(dr["defaultpaymode"]);
                objSettings.defaultBankID = Convert.ToString(dr["defaultBankID"]);
                objSettings.defaultbankName = Convert.ToString(dr["defaultbankName"]);
                objSettings.roundamt = Convert.ToBoolean(dr["roundamt"]);
                objSettings.showrate = Convert.ToBoolean(dr["showrate"]);
                objSettings.tracinguser = Convert.ToBoolean(dr["tracinguser"]);
                objSettings.itemname = Convert.ToBoolean(dr["itemname"]);
                objSettings.shortname = Convert.ToBoolean(dr["shortname"]);
                objSettings.headerduplicate = Convert.ToBoolean(dr["headerduplicate"]);
                objSettings.focaffect = Convert.ToBoolean(dr["focaffect"]);

                objSettings.stock = Convert.ToString(dr["stock"]);
                objSettings.KitStock = Convert.ToString(dr["KitStock"]);
                objSettings.barcode = Convert.ToBoolean(dr["barcode"]);
                objSettings.holdsys = Convert.ToBoolean(dr["holdsys"]);
                objSettings.Tenderwindow = Convert.ToBoolean(dr["Tenderwindow"]);
                objSettings.Tax2onRetail = Convert.ToBoolean(dr["Tax2onRetail"]);
                objSettings.Tax2onVAT = Convert.ToBoolean(dr["Tax2onVAT"]);
                objSettings.Tax2onCST = Convert.ToBoolean(dr["Tax2onCST"]);
                objSettings.KotSystem = Convert.ToBoolean(dr["KotSystem"]);
                objSettings.BoxSystem = Convert.ToBoolean(dr["BoxSystem"]);
                objSettings.NegtiveStock = Convert.ToBoolean(dr["NegtiveStock"]);
                objSettings.StartCursor = Convert.ToString(dr["StartCursor"]);


            }

            dr.NextResult();
            if (dr.HasRows)
            {
                dr.Read();

                objSettings.EnableMRP = Convert.ToBoolean(dr["MRP_Enable"]);
                objSettings.EnablePrice = Convert.ToBoolean(dr["SRate_Enable"]);
               

            }



            dr.NextResult();
             if (dr.HasRows)
             {
                 dr.Read();

                 objSettings.Allow_Dis_on_Billing = Convert.ToBoolean(dr["Allow_Dis_on_Billing"]);
                 objSettings.Enable_Dis_Col = Convert.ToBoolean(dr["Enable_Dis_Col"]);
                 objSettings.Enable_Dis_Amt = Convert.ToBoolean(dr["Enable_Dis_Amt"]);
                 objSettings.Enable_Cust_Dis = Convert.ToBoolean(dr["Enable_Cust_Dis"]);
                 objSettings.Dis_Bill_Value = Convert.ToBoolean(dr["Dis_Bill_Value"]);
                 objSettings.Back_End_Discount = Convert.ToBoolean(dr["Back_End_Discount"]);

             }


             List<DiscountDetail> lst = new List<DiscountDetail>();
             dr.NextResult();
             if (dr.HasRows)
             {
                 dr.Read();
                 while (dr.Read())
                 {
                     DiscountDetail obj = new DiscountDetail();
                     obj.StartValue = Convert.ToDecimal(dr["Start_Value"]);
                     obj.EndValue = Convert.ToDecimal(dr["End_Value"]);
                     obj.DisPer = Convert.ToDecimal(dr["Disper"]);
                     lst.Add(obj);
                 }
                

             }

             return lst;
        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


}