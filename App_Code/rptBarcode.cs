﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

/// <summary>
/// Summary description for rptBarcode
/// </summary>
public class rptBarcode : DevExpress.XtraReports.UI.XtraReport
{
	private DevExpress.XtraReports.UI.DetailBand Detail;
	private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
    private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
    private XRBarCode xrBarCode1;
    private XRLabel xrLabel2;
    private XRLabel xrLabel1;
    private XRLabel xrLabel6;
    private XRLabel xrLabel3;
    private XRLabel xrLabel8;
    private XRLabel xrLabel7;
    private dsSuperStore dsSuperStore1;
    private dsSuperStoreTableAdapters.master_sp_ProductsGetByIDByQtyTableAdapter master_sp_ProductsGetByIDByQtyTableAdapter1;
    private dsSuperStore dsSuperStore2;
    private dsSuperStore dsSuperStore3;
    private dsSuperStore dsSuperStore4;
    private dsSuperStore dsSuperStore5;
    private XRLabel xrLabel9;
    private XRLabel xrLabel12;
    private XRLabel xrLabel11;
    private XRLabel xrLabel14;
    private XRLabel xrLabel13;
    private XRLabel xrLabel15;
    private XRLabel xrLabel4;
	/// <summary>
	/// Required designer variable.
	/// </summary>
	private System.ComponentModel.IContainer components = null;

	public rptBarcode(int ItemId,int Qty,DateTime Date,bool BestExp,String Day,string wt)
	{
       
        
        Connection con = new Connection();
        InitializeComponent();
        master_sp_ProductsGetByIDByQtyTableAdapter1.Connection = new System.Data.SqlClient.SqlConnection(con.sqlDataString);
        //master_sp_ProductsGetByIDByQtyTableAdapter1.Fill(dsSuperStore5.master_sp_ProductsGetByIDByQty, ItemId,Qty, Convert.ToDecimal(wt = string.IsNullOrEmpty(wt) ? "0" : wt));
        master_sp_ProductsGetByIDByQtyTableAdapter1.Fill(dsSuperStore5.master_sp_ProductsGetByIDByQty, ItemId, Qty);
        dsSuperStore5.EnforceConstraints = false;
        if (BestExp == true)
        {
            xrLabel9.Text = "Best Before :";
            xrLabel4.Text = Day + " Days";
          
        }
        else
        {
            xrLabel9.Text = "Expiry:";
            double noofdays = Convert.ToDouble(Day); 
            DateTime newdate = Date.AddDays(noofdays);
            xrLabel4.Text = newdate.ToString("dd-MMM-yy");
            
        }
        xrLabel14.Text = wt;
	}
	
	/// <summary> 
	/// Clean up any resources being used.
	/// </summary>
	/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
	protected override void Dispose(bool disposing) {
		if (disposing && (components != null)) {
			components.Dispose();
		}
		base.Dispose(disposing);
	}

	#region Designer generated code

	/// <summary>
	/// Required method for Designer support - do not modify
	/// the contents of this method with the code editor.
	/// </summary>
	private void InitializeComponent() {
        string resourceFileName = "rptBarcode.resx";
        DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator1 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
        this.Detail = new DevExpress.XtraReports.UI.DetailBand();
        this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
        this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
        this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
        this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
        this.dsSuperStore1 = new dsSuperStore();
        this.master_sp_ProductsGetByIDByQtyTableAdapter1 = new dsSuperStoreTableAdapters.master_sp_ProductsGetByIDByQtyTableAdapter();
        this.dsSuperStore2 = new dsSuperStore();
        this.dsSuperStore3 = new dsSuperStore();
        this.dsSuperStore4 = new dsSuperStore();
        this.dsSuperStore5 = new dsSuperStore();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
        // 
        // Detail
        // 
        this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel4,
            this.xrLabel15,
            this.xrLabel11,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLabel12,
            this.xrLabel9,
            this.xrLabel8,
            this.xrLabel7,
            this.xrLabel3,
            this.xrLabel6,
            this.xrLabel2,
            this.xrLabel1,
            this.xrBarCode1});
        this.Detail.HeightF = 185.2083F;
        this.Detail.Name = "Detail";
        this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel4
        // 
        this.xrLabel4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(242.9456F, 21.00002F);
        this.xrLabel4.Name = "xrLabel4";
        this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel4.SizeF = new System.Drawing.SizeF(55.45116F, 20.99999F);
        this.xrLabel4.StylePriority.UseFont = false;
        this.xrLabel4.StylePriority.UseTextAlignment = false;
        this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel15
        // 
        this.xrLabel15.Font = new System.Drawing.Font("Verdana", 8F);
        this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 118.3335F);
        this.xrLabel15.Multiline = true;
        this.xrLabel15.Name = "xrLabel15";
        this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel15.SizeF = new System.Drawing.SizeF(277.0417F, 33.33334F);
        this.xrLabel15.StylePriority.UseFont = false;
        this.xrLabel15.StylePriority.UseTextAlignment = false;
        this.xrLabel15.Text = "Marketed By: MY FRESH\r\nBooth No: 2,3&6, Sec-9 D,Inner Market,CHD";
        this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel11
        // 
        this.xrLabel11.Font = new System.Drawing.Font("Verdana", 8F);
        this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(9.999959F, 78.91674F);
        this.xrLabel11.Multiline = true;
        this.xrLabel11.Name = "xrLabel11";
        this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel11.SizeF = new System.Drawing.SizeF(277.0416F, 49.83343F);
        this.xrLabel11.StylePriority.UseFont = false;
        this.xrLabel11.StylePriority.UseTextAlignment = false;
        this.xrLabel11.Text = "PKD By:ORGANO FRESH SOLUTIONS PVT LTD.\r\nPLOT NO:-1,TIMBER MARKET,SECTOR 26,\r\nCHAN" +
            "DIGARH";
        this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrLabel14
        // 
        this.xrLabel14.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(242.9456F, 63.00004F);
        this.xrLabel14.Name = "xrLabel14";
        this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel14.SizeF = new System.Drawing.SizeF(55.45116F, 11.91672F);
        this.xrLabel14.StylePriority.UseFont = false;
        this.xrLabel14.StylePriority.UseTextAlignment = false;
        this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel13
        // 
        this.xrLabel13.Font = new System.Drawing.Font("Arial", 8F);
        this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(186.451F, 63.00004F);
        this.xrLabel13.Name = "xrLabel13";
        this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel13.SizeF = new System.Drawing.SizeF(46.00728F, 15.9167F);
        this.xrLabel13.StylePriority.UseFont = false;
        this.xrLabel13.StylePriority.UseTextAlignment = false;
        this.xrLabel13.Text = "Weight";
        this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        this.xrLabel13.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.xrLabel13_BeforePrint);
        // 
        // xrLabel12
        // 
        this.xrLabel12.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold);
        this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(28.09056F, 151.6668F);
        this.xrLabel12.Name = "xrLabel12";
        this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel12.SizeF = new System.Drawing.SizeF(222.4583F, 19.58324F);
        this.xrLabel12.StylePriority.UseFont = false;
        this.xrLabel12.StylePriority.UseTextAlignment = false;
        this.xrLabel12.Text = "Lic No:- 13018001000509";
        this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel9
        // 
        this.xrLabel9.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold);
        this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(186.451F, 21F);
        this.xrLabel9.Multiline = true;
        this.xrLabel9.Name = "xrLabel9";
        this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel9.ProcessNullValues = DevExpress.XtraReports.UI.ValueSuppressType.SuppressAndShrink;
        this.xrLabel9.SizeF = new System.Drawing.SizeF(46.00728F, 21.00001F);
        this.xrLabel9.StylePriority.UseFont = false;
        this.xrLabel9.StylePriority.UseTextAlignment = false;
        this.xrLabel9.Text = "Expiry\r\n";
        this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel8
        // 
        this.xrLabel8.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "master_sp_ProductsGetByIDByQty.PkdOn", "{0:dd-MM-yy}")});
        this.xrLabel8.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(242.9456F, 42.00002F);
        this.xrLabel8.Name = "xrLabel8";
        this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel8.SizeF = new System.Drawing.SizeF(63.05442F, 21.00002F);
        this.xrLabel8.StylePriority.UseFont = false;
        this.xrLabel8.StylePriority.UseTextAlignment = false;
        this.xrLabel8.Text = "xrLabel8";
        this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel7
        // 
        this.xrLabel7.Font = new System.Drawing.Font("Arial", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(186.451F, 42.00002F);
        this.xrLabel7.Name = "xrLabel7";
        this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel7.SizeF = new System.Drawing.SizeF(42.25723F, 21.00002F);
        this.xrLabel7.StylePriority.UseFont = false;
        this.xrLabel7.StylePriority.UseTextAlignment = false;
        this.xrLabel7.Text = "Pkd On:";
        this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel3
        // 
        this.xrLabel3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(186.451F, 0F);
        this.xrLabel3.Name = "xrLabel3";
        this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel3.SizeF = new System.Drawing.SizeF(55F, 21.00002F);
        this.xrLabel3.StylePriority.UseFont = false;
        this.xrLabel3.StylePriority.UseTextAlignment = false;
        this.xrLabel3.Text = "Price";
        this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
        // 
        // xrLabel6
        // 
        this.xrLabel6.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "master_sp_ProductsGetByIDByQty.Sale_Rate")});
        this.xrLabel6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(250.5488F, 0F);
        this.xrLabel6.Name = "xrLabel6";
        this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel6.SizeF = new System.Drawing.SizeF(55.45116F, 21.00002F);
        this.xrLabel6.StylePriority.UseFont = false;
        this.xrLabel6.StylePriority.UseTextAlignment = false;
        this.xrLabel6.Text = "xrLabel6";
        this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
        // 
        // xrLabel2
        // 
        this.xrLabel2.Angle = 90F;
        this.xrLabel2.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "master_sp_ProductsGetByIDByQty.Item_Code")});
        this.xrLabel2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(287.0416F, 78.91677F);
        this.xrLabel2.Name = "xrLabel2";
        this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel2.SizeF = new System.Drawing.SizeF(18.95836F, 72.75007F);
        this.xrLabel2.StylePriority.UseFont = false;
        this.xrLabel2.StylePriority.UseTextAlignment = false;
        this.xrLabel2.Text = "xrLabel2";
        this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
        // 
        // xrLabel1
        // 
        this.xrLabel1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "master_sp_ProductsGetByIDByQty.Item_Name")});
        this.xrLabel1.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 10.00001F);
        this.xrLabel1.Name = "xrLabel1";
        this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
        this.xrLabel1.SizeF = new System.Drawing.SizeF(176.451F, 22.91673F);
        this.xrLabel1.StylePriority.UseFont = false;
        this.xrLabel1.StylePriority.UseTextAlignment = false;
        this.xrLabel1.Text = "xrLabel1";
        this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // xrBarCode1
        // 
        this.xrBarCode1.AutoModule = true;
        this.xrBarCode1.DataBindings.AddRange(new DevExpress.XtraReports.UI.XRBinding[] {
            new DevExpress.XtraReports.UI.XRBinding("Text", null, "master_sp_ProductsGetByIDByQty.Item_Code")});
        this.xrBarCode1.LocationFloat = new DevExpress.Utils.PointFloat(28.09056F, 38.00004F);
        this.xrBarCode1.Name = "xrBarCode1";
        this.xrBarCode1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
        this.xrBarCode1.ShowText = false;
        this.xrBarCode1.SizeF = new System.Drawing.SizeF(127.4238F, 25F);
        code128Generator1.CharacterSet = DevExpress.XtraPrinting.BarCode.Code128Charset.CharsetAuto;
        this.xrBarCode1.Symbology = code128Generator1;
        this.xrBarCode1.Text = "xrBarCode1";
        // 
        // TopMargin
        // 
        this.TopMargin.HeightF = 0F;
        this.TopMargin.Name = "TopMargin";
        this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // BottomMargin
        // 
        this.BottomMargin.HeightF = 0F;
        this.BottomMargin.Name = "BottomMargin";
        this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
        this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
        // 
        // dsSuperStore1
        // 
        this.dsSuperStore1.DataSetName = "dsSuperStore";
        this.dsSuperStore1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // master_sp_ProductsGetByIDByQtyTableAdapter1
        // 
        this.master_sp_ProductsGetByIDByQtyTableAdapter1.ClearBeforeFill = true;
        // 
        // dsSuperStore2
        // 
        this.dsSuperStore2.DataSetName = "dsSuperStore";
        this.dsSuperStore2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // dsSuperStore3
        // 
        this.dsSuperStore3.DataSetName = "dsSuperStore";
        this.dsSuperStore3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // dsSuperStore4
        // 
        this.dsSuperStore4.DataSetName = "dsSuperStore";
        this.dsSuperStore4.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // dsSuperStore5
        // 
        this.dsSuperStore5.DataSetName = "dsSuperStore";
        this.dsSuperStore5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
        // 
        // rptBarcode
        // 
        this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
        this.DataAdapter = this.master_sp_ProductsGetByIDByQtyTableAdapter1;
        this.DataMember = "master_sp_ProductsGetByIDByQty";
        this.DataSource = this.dsSuperStore1;
        this.Margins = new System.Drawing.Printing.Margins(0, 4, 0, 0);
        this.PageHeight = 200;
        this.PageWidth = 310;
        this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
        this.Version = "11.2";
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dsSuperStore5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

	}

	#endregion

    private void xrLabel13_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
    {

    }
}
