﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TaxStructure
/// </summary>
public class TaxStructure
{
    public int Tax_ID { get; set; }
    public decimal Tax_Rate { get; set; }
    public string Description { get; set; }
    public string Dr_AccCode { get; set; }
    public bool VatCode_Dr { get; set; }
    public string Dr_VatCode { get; set; }
    public bool VatCode_Cr { get; set; }
    public string Cr_AccCode { get; set; }
    public string Cr_VatCode { get; set; }
    public bool ChkSur { get; set; }
    public decimal SurValue { get; set; }
    public string Dr_Sur { get; set; }
    public string Cr_Sur { get; set; }


    public string sDr_Sur { get; set; }
    public string sCr_Sur { get; set; }
    public string sCr_AccCode { get; set; }
    public string sDr_AccCode { get; set; }
    public string sCr_VatCode { get; set; }
    public string sDr_VatCode { get; set; }
   

    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public int BranchId { get; set; }
    public decimal CESS { get; set; }
	public TaxStructure()
	{
        Tax_ID = 0;
        Tax_Rate = 0;
        Description = string.Empty;
        Dr_AccCode = string.Empty;
        VatCode_Dr = false;
        Dr_VatCode = string.Empty;
        VatCode_Cr = false;
        Cr_AccCode = string.Empty;
        Cr_VatCode = string.Empty;
        ChkSur = false;
        SurValue = 0;
        Dr_Sur = string.Empty;
        Cr_Sur = string.Empty;
        UserId = 0;
        IsActive = false;
        BranchId = 0;
        CESS = 0;
	}
}