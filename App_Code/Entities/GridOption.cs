﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for GridOption
/// </summary>
public class GridOption
{

	public int Id { get; set; }
	public string Caption { get; set; }
	public string GridColumn { get; set; }
	public string HideShow { get; set; }

	public string Width { get; set; }

	public string OPtionsList { get; set; }
	public GridOption()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}