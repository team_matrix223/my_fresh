﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Purchase
/// </summary>
public class BreakageExpiry
{
  
    public int BreakageId { get; set; }
    public DateTime DateOfBreakage { get; set; }
    public string strBreakageDate { get { return DateOfBreakage.ToString("d"); } }
    
    public int GodownId { get; set; }
    public string Godown { get; set; }
    public int BranchId { get; set; }
    public BreakageExpiry()
    {
        
        BreakageId = 0;
        DateOfBreakage = DateTime.Now;
        GodownId = 0;
        Godown = string.Empty;
        BranchId = 0;
         
    }
}