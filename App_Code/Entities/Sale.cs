﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Sale
/// </summary>
public class Sale
{
    public string RRItem_Name { get; set; }
    public decimal RRMRP { get; set; }
    public decimal RRRate { get; set; }
    public decimal RRTax_Code { get; set; }

    public string RRItem_Code { get; set; }
    public string Item_Code { get; set; }
    public string  Item_Name { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Tax_Code { get; set; }
    public string master_code { get; set; }
    public decimal qty_to_less { get; set; }
    public int godown_id { get; set; }
    public string Item_Type { get; set; }
    public int BranchId { get; set; }

	public Sale()
	{
        Item_Code = string.Empty;
        Item_Name = string.Empty;
        MRP = 0;
        Rate = 0;
        Tax_Code = 0;
        master_code = string.Empty;
        qty_to_less  = 0;
        godown_id = 0;
        Item_Type = string.Empty;
        BranchId = 0;

	}
}