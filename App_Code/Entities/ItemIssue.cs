﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for StockAdj
/// </summary>
public class ItemIssue
{
    public int RefNo { get; set; }
    public string Date { get; set; }
    public string strDate { get { return Date; } }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal Qty { get; set; }
    public decimal MRP { get; set; }
    public int UserNo { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public int Godown_ID { get; set; }
    public int BranchId { get; set; }
    public decimal Stock { get; set; }
    public int GodownTo { get; set; }

    public string FromGodown { get; set; }

    public string ToGodown { get; set; }

    public ItemIssue()
    {
        RefNo = 0;
        Date = string.Empty;
        Item_Code = string.Empty;
        Item_Name = string.Empty;
        MRP = 0;
        Stock = 0;
        UserNo = 0;
        Rate = 0;
        Amount = 0;
        Godown_ID = 0;
        BranchId = 0;
        Qty = 0;
        GodownTo = 0;

    }
}