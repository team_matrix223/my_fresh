﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Paymodes
/// </summary>
public class Paymodes
{
	public int Id { get; set; }
	public string Name { get; set; }
	public Paymodes()
	{
		Id = 0;
		Name = string.Empty;
	}
}