﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for HappyHours
/// </summary>
public class HappyHours
{

    public int Id { get; set; }
    public string CategoryId { get; set; }
    public string TimeFrom { get; set; }
    public string TimeTo { get; set; }

    public bool IsActive { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }

	public HappyHours()
	{
        Id = 0;
        CategoryId = "";
        TimeFrom = "";
        TimeTo = "";
        IsActive = true;
        UserId = 0;
        BranchId = 0;
	}
}