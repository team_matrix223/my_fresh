﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SGroups
/// </summary>
public class PropSGroups
{
    public int SGroup_ID { get; set; }
    public string SGroup_Name { get; set; }
    public bool ShowInMenu { get; set; }
    public int Department_Id { get; set; }
    public int Group_Id { get; set; }
    public bool IsActive { get; set; }
    public int UserId { get; set; }

	public PropSGroups()
	{
        SGroup_ID = -1;
        SGroup_Name = "";
        ShowInMenu = true;
        Department_Id = 0;
        Group_Id = 0;
        IsActive = true;
        UserId = 0;
	}
}