﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OtherPayment
/// </summary>
public class OtherPayment
{

    public int OtherPayment_ID { get; set; }
    public string OtherPayment_Name { get; set; }

    public int UserId { get; set; }
    public bool IsActive { get; set; }
	public OtherPayment()
	{
        OtherPayment_ID = 0;
        OtherPayment_Name = "";
      
        UserId = 0;
        IsActive = false;
	}
}