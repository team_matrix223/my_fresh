﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Acc_Ledger
/// </summary>
public class AccTrans
{
    public decimal Debit { get; set; }
    public decimal Credit { get; set; }
    public string VOUCH_NO { get; set; }
    public string TRDAY_NO { get; set; }
    public string CCODE { get; set; }
    public string CNAME { get; set; }
    public decimal TR_AMOUNT { get; set; }
    public string TR_DRCR { get; set; }
    public string PUR_NO { get; set; }
    public string BILL_NO { get; set; }
    public string VTYPE { get; set; }
    public string NARR { get; set; }
    public int USERNO { get; set; }
    public bool PASS { get; set; }
    public DateTime TR_DATE { get; set; }
    public string strTRDate { get { return TR_DATE.ToString("d"); } }
    public string Chq_No { get; set; }
    public string PartyBank { get; set; }
    public Int32 SrNo { get; set; }
    public string ShortName { get; set; }
    public string RowType { get; set; }
    public string AutoVoucher { get; set; }

    public Int32 BranchId { get; set; }

    public Int32 Code { get; set; }

    public string CashBankCode { get; set; }

    public string Ref_No { get; set; }




    public AccTrans()
	{
        VOUCH_NO = "";
        TRDAY_NO = "";
        CCODE = "";
        CNAME = "";
        TR_DRCR = "";     
        TR_AMOUNT = 0;
        PUR_NO = "";
        BILL_NO = "";
        VTYPE = "";
        USERNO = 0;
        NARR = "";
        TR_DATE = DateTime.Now;
        PASS = false;
        PartyBank = "";
        Chq_No = "";
        RowType = "";
        AutoVoucher = "";
        SrNo = 0;
        ShortName = "";
        BranchId = 0;
        Code = 0;
        CashBankCode = "";
        Ref_No = "";


	}
}