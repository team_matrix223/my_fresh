﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Colors
/// </summary>
public class Coupons
{
    public int CouponNo { get; set; }
    public int CouponAmt { get; set; }
    public int RefNo { get; set; }
    public string BillNo { get; set; }
    public int CouponFrom { get; set; }
    public int CouponTo { get; set; }
    public int Amount { get; set; }
    public int NoOfCoupons { get; set; }
    public DateTime RefDate { get; set; }
    public int BranchId { get; set; }

    public string strBD { get { return RefDate.ToString("d"); } }

    public Coupons()
	{
        RefNo = -1;
        BillNo = "";
        CouponFrom = 0;
        CouponTo = 0;
        NoOfCoupons = 0;
        RefDate = DateTime.Now;
        BranchId = 0;
	}
}