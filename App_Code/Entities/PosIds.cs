﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for PosIds
/// </summary>
public class PosIds
{
	public int PosId { get; set; }
	public string Title { get; set; }
	public PosIds()
	{
		PosId = 0;
		Title = string.Empty;
	}
}