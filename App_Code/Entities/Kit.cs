﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Kit
/// </summary>
/// 


public class KitDetail
{

    
    public int Item_ID { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal Qty { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public int Tax_ID { get; set; }
    public decimal Tax_Rate { get; set; }
}


public class Kit
{

    public int Kit_ID { get; set; }
    public string Master_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal Amount { get; set; }
    public decimal MRP { get; set; }
    public decimal SALE_RATE { get; set; }
    public int UserId { get; set; }
	
    
    public Kit()
	{
        Kit_ID = 0;
        Master_Code = "";
        Item_Name = "";
        Amount = 0;
        MRP = 0;
        SALE_RATE = 0;
        UserId = 0;
	}
}