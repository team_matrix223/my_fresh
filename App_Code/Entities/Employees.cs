﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Employees
/// </summary>
public class Employees
{
    public int Code { get; set; }
    public string Name { get; set; }
    public string Address1 { get; set; }
    public string Address2 { get; set; }
    public string Address3 { get; set; }
    public string ContactNo { get; set; }
    public string ContactPerson { get; set; }
    public string Remarks { get; set; }
    public bool Auth { get; set; }
    public int UserId { get; set; }
    public bool IsActive { get; set; }
    public bool IsDeliveryBoy { get; set; }
    public int BranchId { get; set; }
    public int AreaId { get; set; }
    public int CityId { get; set; }
    public int StateId { get; set; }
    public string KotPswrd { get; set; }
    public decimal ComplimentryValue { get; set; }
    public decimal Discount { get; set; }
    public decimal Balance { get; set; }
    public string Designation { get; set; }

    public Employees()
	{
        Code = 0;
        Name = "";
        Address1 = "";
        Address2 = "";
        Address3 = "";
        ContactNo = "";
        ContactPerson = "";
        Remarks = "";
        Auth = false;
        BranchId = 0;
        IsActive = true;
        IsDeliveryBoy = false;
        UserId = 0;
        AreaId = 0;
        StateId = 0;
        CityId = 0;
        KotPswrd = "";
        ComplimentryValue = 0;
        Discount = 0;
        Balance = 0;
        Designation = "";


    }
}