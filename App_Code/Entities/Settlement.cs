﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Settlement
/// </summary>
public class Settlement
{
    public string BillNowPrefix { get; set; }
    public int BillNo { get; set; }
    public DateTime BillDate { get; set; }
    public string CustomerName { get; set; }
    public string BillMode { get; set; }
    public decimal NetAmount { get; set; }
    public int tableNo { get; set; }
    public int EmpCode { get; set; }
    public string EmpName { get; set; }
    public string strBD { get { return BillDate.ToString("d"); } }
	public Settlement()
	{
        BillNowPrefix = "";
        BillNo = 0;
        BillDate = DateTime.Now;
        CustomerName = "";
        BillMode = "";
        NetAmount = 0;
        tableNo = 0;
        EmpCode = 0;
        EmpName = "";
	}
}