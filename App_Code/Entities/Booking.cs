﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Booking
/// </summary>
/// 

public class BookingDetail
{

    public string Code { get; set; }
    public string Name { get; set; }
    public decimal Weight { get; set; }
    public decimal Qty { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Amount { get; set; }
    public decimal Vat { get; set; }
    public decimal DisptchdQty { get; set; }
    public decimal TaxPer { get; set; }
    public decimal Surcharge { get; set; }

    public decimal DisPer { get; set; }

    public decimal DisAmt { get; set; }

}





public class Booking
{
    public int OrderNo { get; set; }
    public DateTime OrderDate { get; set; }
    public string strOD { get { return OrderDate.ToString("d"); } }
    public string Customer_ID { get; set; }
    public string CustomerName { get; set; }
    public string MobileNo { get; set; }
    public string Address { get; set; }
    public string DeliveryType { get; set; }
    public DateTime DeliveryTime { get; set; }
    public string strDD { get { return DeliveryTime.ToString("d"); } }
    public string DeliveryAddress { get; set; }
    public decimal Advance { get; set; }
    public string PaymentMode { get; set; }
    public decimal LeftPayRecd { get; set; }
    public string Remarks { get; set; }
    public int UserNo { get; set; }
    public bool Passing { get; set; }
    public DateTime DateBalRecd { get; set; }
    public int Ecode { get; set; }
    public decimal DisAmt { get; set; }
    public string Picture { get; set; }
    public decimal VatAmount { get; set; }
    public decimal NetAmount { get; set; }
    public DateTime OrderTime { get; set; }
    public string IndRemarks { get; set; }
    public string Prefix { get; set; }
    public bool ReadTag { get; set; }
    public bool FDispatch { get; set; }
    public Int32 Employee { get; set; }
    public decimal DisPer { get; set; }
    public decimal AdvancePaid { get; set; }
    public int BranchId { get; set; }

    public string CreditCardNumber { get; set; }
    public string DelTime { get; set; }
    public decimal TotalDispatchedAmt { get; set; }
    public string CCODE { get; set; }
    public string CNAME { get; set; }




    public string ManualOrderNo { get; set; }
    public string SerialNo { get; set; }

    public int PosId { get; set; }

    public Booking()
	{

        ManualOrderNo = string.Empty;
        SerialNo = string.Empty;
        CCODE = string.Empty;
        CNAME = string.Empty;
        TotalDispatchedAmt = 0;
        DelTime = string.Empty;
        CreditCardNumber = string.Empty;
        OrderNo = 0;
        OrderDate = DateTime.Now;
        Customer_ID = "";
        CustomerName = "";
        Address = "";
        DeliveryType = "";
        DeliveryTime = DateTime.Now;
        DeliveryAddress = "";
        Advance = 0;
        PaymentMode = "";
        LeftPayRecd = 0;
        Remarks = "";
        UserNo = 0;
        Passing = false;
        DateBalRecd = DateTime.Now;
        Ecode = 0;
        DisAmt = 0;
        Picture = "";
        VatAmount = 0;
        NetAmount = 0;
        OrderTime = DateTime.Now;
        IndRemarks = "";
        Prefix = "";
        ReadTag = false;
        FDispatch = false;
        Employee = 0;
        DisPer = 0;
        BranchId = 0;
        PosId = 0;
	}
}