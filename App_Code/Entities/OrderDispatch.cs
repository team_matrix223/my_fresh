﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for OrderDispatch
/// </summary>
/// 
public class DispatchDetail
{
    public int RefNo { get; set; }
 
    public DateTime RefDate { get; set; }
    public string strRD { get { return RefDate.ToString("d"); } }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal Weight { get; set; }
    public decimal OrdQty { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Tax { get; set; }
    public decimal DptQty { get; set; }
    public decimal DisPer { get; set; }
    public string Master_Code { get; set; }
    public decimal TaxP { get; set; }
    public decimal SurValue { get; set; }

}







public class OrderDispatch
{

    public int RefNo { get; set; }
    public string OrderNo { get; set; }
    public DateTime RefDate { get; set; }
    public string strRD { get { return RefDate.ToString("d"); } }
    public DateTime DspTime { get; set; }
    public string strDT { get { return DspTime.ToString("d"); } }
    public int CustCode { get; set; }
    public decimal DSpValue { get; set; }
    public decimal Discount { get; set; }
    public decimal TaxAmount { get; set; }
  
    public decimal NetAmount { get; set; }
    public string PayMOde { get; set; }
    public decimal CrPay { get; set; }
    public decimal CrLeftPay { get; set; }
    public string TaxType { get; set; }
    public int UserId { get; set; }
    public string PcName { get; set; }
    public string Billno { get; set; }
    public decimal Payment { get; set; }
    public decimal Disper { get; set; }
    public int BranchId { get; set; }

    public string CCODE { get; set; }
    public string CNAME { get; set; }
    public decimal CrdAmount { get; set; }
    public string CardNumber { get; set; }
    public string CardType { get; set; }
    public string Bank { get; set; }

    public int AdvanceMode { get; set; }
    public decimal OnlinePayment { get; set; }
    public OrderDispatch()
	{
        RefNo = 0;
        OrderNo = "";
        RefDate = DateTime.Now;
        DspTime = DateTime.Now;
        CustCode = 0;
        DSpValue = 0;
        Discount = 0;
        TaxAmount = 0;
        NetAmount = 0;
        Payment = 0;
        PayMOde = "";
        CrPay = 0;
        CrLeftPay = 0;
        TaxType = "";
        UserId = 0;
        PcName = "";
        Billno = "";
        BranchId = 0;
        CCODE = "";
        CNAME = "";
        CrdAmount = 0;
        CardNumber = "";
        CardType = "";
        Bank = "";
        AdvanceMode = 0;
        OnlinePayment = 0;
        
	}
}