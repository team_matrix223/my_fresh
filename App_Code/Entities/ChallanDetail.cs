﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ChallanDetail
/// </summary>
public class ChallanDetail
{
    public string Bill_Prefix { get; set; }
    public int Bill_No { get; set; }
    public string BillNoWPrefix { get; set; }
    public DateTime Bill_Date { get; set; }
    public string Item_Code { get; set; }
    public string Item_Name { get; set; }
    public decimal MRP { get; set; }
    public decimal Rate { get; set; }
    public decimal Qty { get; set; }
    public decimal Dis_Per { get; set; }
    public decimal Dis_Amt { get; set; }
    public decimal Amount { get; set; }
    public string BillMode { get; set; }
    public decimal Tax { get; set; }

    public decimal Tax_Amount { get; set; }
    public string Unit { get; set; }
    public decimal Org_SaleRate { get; set; }
    public string MASTER_CODE { get; set; }
    public decimal QTY_TO_LESS { get; set; }
    public int godown_id { get; set; }
    public decimal SurVal { get; set; }
    public decimal FreeQty { get; set; }
    public decimal RowNum { get; set; }
	public ChallanDetail()
	{
		
	}
}