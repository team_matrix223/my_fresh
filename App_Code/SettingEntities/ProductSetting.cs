﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductSetting
/// </summary>
public class ProductSetting
{
    public bool Auto_GenCode { get; set; }
    public string Item_CodeLen { get; set; }
    public string Item_CodeStart { get; set; }
    public bool Alphabet_Code { get; set; }
    public bool Comp_WiseDis { get; set; }
    public bool Batch_No { get; set; }
    public bool ItemDup { get; set; }
    public bool ItemCompDup { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }
    
	public ProductSetting()
	{
        Auto_GenCode = false;
        Item_CodeLen = "";
        Item_CodeStart = "";
        Alphabet_Code = false;
        Comp_WiseDis = false;
        Batch_No = false;
        ItemDup = false;
        ItemCompDup = false; 
        UserId = 0;
        BranchId = 0;
	}
}