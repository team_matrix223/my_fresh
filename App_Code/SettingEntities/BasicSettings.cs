﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BasicSettings
/// </summary>
public class BasicSettings
{

    public bool service_tax { get; set; }
    public decimal tax_per { get; set; }
    public bool homedel_charges { get; set; }
    public decimal min_bill_value { get; set; }
    public decimal del_charges { get; set; }
    public string retail_bill { get; set; }
    public string vat_bill { get; set; }
    public string cst_bill { get; set; }
    public int UserId { get; set; }
    public int BranchId { get; set; }
    public bool AlloServicetax_TakeAway { get; set; }
    public bool AllowKKC { get; set; }
    public decimal KKC { get; set; }
    public bool AllowSBC { get; set; }
    public decimal SBC { get; set; }
    public string DefaultService { get; set; }
    public bool PrintTime { get; set; }
    public int NoOfCopy { get; set; }
    public bool PrintKot_Reprint { get; set; }

	public BasicSettings()
	{
        service_tax = false;
        tax_per = 0;
        homedel_charges = false;
        min_bill_value = 0;
        del_charges = 0;
        retail_bill = "";
        vat_bill = "";
        cst_bill = "";
        UserId = 0;
        BranchId = 0;
        AlloServicetax_TakeAway = false;
        AllowSBC = false;
        AllowKKC = false;
        KKC = 0;
        SBC = 0;
        DefaultService = "";
        PrintTime = false;
        NoOfCopy = 1;
        PrintKot_Reprint = false;
	}
}