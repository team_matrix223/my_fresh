﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NewAccounts
/// </summary>
public class NewAccounts
{
	public int BranchId { get; set; }
	public int UserId { get; set; }
	public string CN_Acc2DebitCode { get; set; }
	public string CN_Acc2DebitName { get; set; }
	public string CompAcnt { get; set; }
	public string CompAcntName { get; set; }
	public string DisplayAcnt { get; set; }
	public string DisplayAcntName { get; set; }
	public string AdjAcnt { get; set; }
	public string AdjAcntName { get; set; }
	public string DiscntAcnt { get; set; }
	public string DiscntAcntName { get; set; }
	public string TcsAcnt { get; set; }
	public string TcsAcntName { get; set; }
	public string BillRoundAcnt { get; set; }
	public string BillRoundAcntName { get; set; }
	public string ServiceTaxAcnt { get; set; }
	public string ServiceTaxAcntName { get; set; }
	public string ServiceChrgAcnt { get; set; }
	public string ServiceChrgAcntName { get; set; }
	public string IncomeTaxAcnt { get; set; }
	public string IncomeTaxAcntName { get; set; }
	public string LabourAcnt { get; set; }
	public string LabourAcntName { get; set; }
	public string GstdedAcnt { get; set; }
	public string GstdedAcntName { get; set; }
	public string OtherdedAcnt { get; set; }
	public string OtherdedAcntName { get; set; }

	public string SecurityAcnt { get; set; }
	public string SecurityAcntName { get; set; }

	public string ResrveAcnt { get; set; }
	public string ResrveAcntName { get; set; }

	public string JobwrkAcnt { get; set; }
	public string JobwrkAcntName { get; set; }
	public string RarAcnt { get; set; }
	public string RarAcntName { get; set; }
	public string SaleAcnt { get; set; }
	public string SaleAcntName { get; set; }

	public string PurchaseAcnt { get; set; }
	public string PurchaseAcntName { get; set; }

	public string JournalAcnt { get; set; }
	public string JournalAcntName { get; set; }
	public string MSEAcnt { get; set; }
	public string MSEAcntName { get; set; }
	public string OutpurAcnt { get; set; }
	public string OutpurAcntName { get; set; }

	public string OutTaxAcnt { get; set; }
	public string OutTaxAcntName { get; set; }

	public string CstSaleAcnt { get; set; }
	public string CstSaleAcntName { get; set; }
	public string CstsaleTaxAcnt { get; set; }
	public string CstsaleTaxAcntName { get; set; }
	public string OrderSaleAcnt { get; set; }
	public string OrderSaleAcntName { get; set; }
	public string OrderAdvAcnt { get; set; }
	public string OrderAdvAcntName { get; set; }
	public string DebitNoteAcnt { get; set; }
	public string DebitNoteAcntName { get; set; }
	public string BankAccounts { get; set; }
	public NewAccounts()
	{
		CN_Acc2DebitCode = "";
		CN_Acc2DebitName = "";
		OrderSaleAcnt = "";
		OrderSaleAcntName = "";
		OrderAdvAcnt = "";
		OrderAdvAcntName = "";
		DebitNoteAcnt = "";
		DebitNoteAcntName = "";
		 CompAcnt = "";
		 CompAcntName = "";
		 DisplayAcnt = "";
		 DisplayAcntName = "";
		 AdjAcnt = "";
		 AdjAcntName = "";
		 DiscntAcnt = "";
		 DiscntAcntName = "";
		 TcsAcnt = "";
		 TcsAcntName = "";
		 BillRoundAcnt = "";
		 BillRoundAcntName = "";
		 ServiceTaxAcnt = "";
		 ServiceTaxAcntName = "";
		 ServiceChrgAcnt = "";
		 ServiceChrgAcntName = "";
		 IncomeTaxAcnt = "";
		 IncomeTaxAcntName = "";
		 LabourAcnt ="";
		 LabourAcntName = "";
	     GstdedAcnt = "";
		 GstdedAcntName = "";
		 OtherdedAcnt = "";
		 OtherdedAcntName = "";
		 SecurityAcnt = "";
		 SecurityAcntName = "";
		 ResrveAcnt = "";
		 ResrveAcntName = "";
		 JobwrkAcnt = "";
		 JobwrkAcntName = "";
		 RarAcnt = "";
		 RarAcntName = "";
		 SaleAcnt = "";
		 SaleAcntName = "";
		 PurchaseAcnt = "";
		 PurchaseAcntName = "";
		 JournalAcnt ="";
		 JournalAcntName = "";
		 MSEAcnt = "";
		 MSEAcntName = "";
		 OutpurAcnt = "";
		 OutpurAcntName = "";
		 OutTaxAcnt = "";
		 OutTaxAcntName = "";
		 CstSaleAcnt = "";
		 CstSaleAcntName = "";
		 CstsaleTaxAcnt = "";
		 CstsaleTaxAcntName = "";
	}
}