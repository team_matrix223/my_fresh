﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AccountSettings
/// </summary>
public class AccountSettings
{
    public int BranchId { get; set; }

    public string CN_Acc2DebitCode { get; set; }

    
    public string CN_Acc2DebitName { get; set; }
public string Comp_AccCode { get; set; }
public string Comp_AccName { get; set; }
public string OSPurc_AccCode { get; set; }
public string OSPurc_AccName { get; set; }
public string OSPurc_TaxCode { get; set; }
public string OSPurc_TaxName { get; set; }
public string CST_ACCCode { get; set; }
public string CST_ACCName { get; set; }
public string CST_TaxCode { get; set; }
public string CST_TaxName { get; set; }
public string Disp_AccCode { get; set; }
public string Disp_AccName { get; set; }
public string Adj_AccCode { get; set; }
public string Adj_AccName { get; set; }
public string DebitNote_AccCode { get; set; }
public string DebitNote_AccName { get; set; }
public string Disc_AccCode { get; set; }
public string Disc_AccName { get; set; }
public string tcs_accName { get; set; }
public string tcs_accCode { get; set; }
public string Round_accCode { get; set; }
public string Round_accname { get; set; }
public string OrderSaleAccCode { get; set; }
public string OrderSaleAccName { get; set; }
public string OrderAdvAccCode { get; set; }
public string OrderAdvAccName { get; set; }
public string ServiceTaxAccCode { get; set; }
public string ServiceTaxAccName { get; set; }
public string ServiceChgAccCode { get; set; }
public string ServiceChgAccName { get; set; }


public string EduCess_AccCode { get; set; }
public string EduCess_AccName { get; set; }
public string HECess_AccCode { get; set; }
public string HECess_AccName { get; set; }
public string ExciseDuty_Code { get; set; }
public string ExciseDuty_AccName { get; set; }
public string BranchTfrSale_Code { get; set; }
public string BranchTfrSale_AccName { get; set; }
public int UserId { get; set; }

    public string IncomeTax_ACCCode { get; set; }
    public string IncomeTax_ACCName { get; set; }

    public string labour_ACCCode { get; set; }
    public string Labour_ACCName { get; set; }

    public string GST_ACCCode { get; set; }
    public string GST_ACCName { get; set; }

    public string Other_ACCCode { get; set; }
    public string Other_ACCName { get; set; }

    public string Security_ACCCode { get; set; }
    public string Security_ACCName { get; set; }

    public string reserve_ACCCode { get; set; }
    public string reserve_ACCName { get; set; }
    public string jobwrk_ACCCode { get; set; }
    public string jobwrk_ACCName { get; set; }
    public string rar_ACCCode { get; set; }
    public string rar_ACCName { get; set; }
    public string sale_ACCCode { get; set; }
    public string sale_ACCName { get; set; }

    public string Purchase_ACCCode { get; set; }
    public string Purchase_ACCName { get; set; }
    public string journal_ACCCode { get; set; }
    public string journal_ACCName { get; set; }

    public string mse_ACCCode { get; set; }
    public string mse_ACCName { get; set; }
    public AccountSettings()
	{
		CN_Acc2DebitName="";
 Comp_AccCode="";
 Comp_AccName ="";
 OSPurc_AccCode ="";
 OSPurc_AccName ="";
 OSPurc_TaxCode ="";
 OSPurc_TaxName ="";
 CST_ACCCode ="";
 CST_ACCName ="";
 CST_TaxCode="";
 CST_TaxName ="";
 Disp_AccCode ="";
 Disp_AccName ="";
 Adj_AccCode="";
 Adj_AccName ="";
 DebitNote_AccCode ="";
 DebitNote_AccName="";
 Disc_AccCode ="";
 Disc_AccName ="";
 tcs_accName ="";
tcs_accCode ="";
 Round_accCode ="";
 Round_accname ="";
 OrderSaleAccCode="";
 OrderSaleAccName ="";
 OrderAdvAccCode ="";
 OrderAdvAccName ="";
 ServiceTaxAccCode ="";
 ServiceTaxAccName ="";
 ServiceChgAccCode="";
 ServiceChgAccName="";
 EduCess_AccCode = "";
 EduCess_AccName = "";
 HECess_AccCode = "";
 HECess_AccName = "";
 ExciseDuty_Code = "";
 ExciseDuty_AccName = "";
 BranchTfrSale_Code = "";
 BranchTfrSale_AccName = "";
	}
}