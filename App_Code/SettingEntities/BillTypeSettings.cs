﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for BillTypeSettings
/// </summary>
public class BillTypeSettings
{

    public bool LocalBillling { get; set; }
    public bool RetailBilling { get; set; }
    public bool VatBilling { get; set; }
    public bool Outstation { get; set; }
    public bool SaleTax { get; set; }
    public string Ret_Def_Series { get; set; }
    public string Vat_Def_Series { get; set; }
    public string CST_Def_Series { get; set; }
    public string Default_Branch { get; set; }
    public int Default_BranchId { get; set; }
    public int UserId { get; set; }

	public BillTypeSettings()
	{
        LocalBillling = false;
        RetailBilling = false;
        VatBilling = false;
        Outstation = false;
        SaleTax = false;
        Ret_Def_Series = "";
        Vat_Def_Series = "";
        CST_Def_Series = "";
        Default_Branch = "";
        Default_BranchId =0;
        UserId = 0;


	}
}