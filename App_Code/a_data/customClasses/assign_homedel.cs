﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
/// <summary>
/// Summary description for assign_homedel
/// </summary>
public class assign_homedel
{
   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public string billnowprefix { get; set; }
    public int emp_id { get; set; }

    public assign_homedel()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable gethomedel()
    {

        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_homedel", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;
    }


    public void  insert_update_homedel()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_homedel", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@billnowprefix", billnowprefix);
        cmd.Parameters.AddWithValue("@emp_id", emp_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
    
    }
}