﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for mst_customer_rate
/// </summary>
public class mst_customer_rate
{

   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);

    public string req { get; set; }
    public string customer_name { get; set; }
    public string status { get; set; }
    public int cst_id { get; set; }
    public int Dept_id { get; set; }
    public int grp_id { get; set; }
    public int sbgrp_id { get; set; }
    public string baseon { get; set; }
    public int cst_rateid { get; set; }
    public string item_Code { get; set; }
    public decimal rate { get; set; }

    public mst_customer_rate()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    
    public DataTable bind_item_dd()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_customer_rate", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }

    public DataTable bind_item_ddnew()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_customer_ratenew", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        cmd.Parameters.AddWithValue("@Dept_id", Dept_id);
        cmd.Parameters.AddWithValue("@grp_id", grp_id);
        cmd.Parameters.AddWithValue("@sbgrp_id", sbgrp_id);
        cmd.Parameters.AddWithValue("@baseon", baseon);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }

    public void getcustomerrate()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_customer_rate", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@cst_id", cst_rateid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {

            item_Code =rd["Item_Code"].ToString();
            cst_id = Convert.ToInt32(rd["cst_id"].ToString());
            customer_name =rd["customer_name"].ToString();
            rate = Convert.ToDecimal(rd["rate"].ToString());
            status = rd["status"].ToString();
        }

        adp.Fill(dt);
        con.Close();

    }

    public void insert_update_customer()
    {

        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_customer_rate", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@customer_name", "");
        cmd.Parameters.AddWithValue("@status", status);
        cmd.Parameters.AddWithValue("@cst_id", cst_id);
        cmd.Parameters.AddWithValue("@item_Code", item_Code);
        cmd.Parameters.AddWithValue("@rate", rate);
        
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }
}