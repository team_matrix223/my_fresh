﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for companycust_rpt_cls
/// </summary>
public class companycust_rpt_cls
{
   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public string StartDate { get; set; }
    public string EndDate { get; set; }
    public string BillMode { get; set; }
    public int cst_id { get; set; }
    public int pos_id { get; set; }
    
    public companycust_rpt_cls()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetReport()
    {

        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_companycust", con);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.AddWithValue("@strt_date", StartDate);
        cmd.Parameters.AddWithValue("@end_date", EndDate);
        cmd.Parameters.AddWithValue("@cst_id",cst_id); 
        cmd.Parameters.AddWithValue("@pos_id", pos_id);
        cmd.Parameters.AddWithValue("@BillMode", BillMode);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;
    }
}