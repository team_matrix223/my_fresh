﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
/// <summary>
/// Summary description for pos
/// </summary>
public class pos
{
   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public string title { get; set; }
    public string status { get; set; }
    public int posid { get; set; }
    public pos()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public DataTable bindgride()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_pos", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }

    public void insert_update_pos()
    {

        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_pos", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@title", title);
        cmd.Parameters.AddWithValue("@status", status);
        cmd.Parameters.AddWithValue("@posid", posid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }

    public void get_pos()
    {

        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_pos", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@posid", posid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {

            title = rd["title"].ToString();
            status = rd["status"].ToString();

        }
        con.Close();


    }


    public void del_pos()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_pos", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@posid", posid);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();


    }
}