﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for MangeTable
/// </summary>
public class MangeTable
{
 // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
  public void Insert_Update_Record(string TableName,int TableID,string MultiPrint)
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        if (TableID != 0)
    {
      var Param = new SqlParameter[4];
      Param[0] = new SqlParameter("@req", "Edit");
      Param[1] = new SqlParameter("@TableName", TableName);
      Param[2] = new SqlParameter("@TableID", TableID);
      Param[3] = new SqlParameter("@MultiPrint", MultiPrint);
      SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "Strp_Tables", Param);
    }
    else
    {
      var Param = new SqlParameter[3];
      Param[0] = new SqlParameter("@req", "insert");
      Param[1] = new SqlParameter("@TableName", TableName);
            Param[1] = new SqlParameter("@MultiPrint", MultiPrint);
            SqlHelper.ExecuteNonQuery(con, CommandType.StoredProcedure, "Strp_Tables", Param);
    }
  }
  public DataSet GetRecordTableByID(int TableID)
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        var Param = new SqlParameter[2];
    Param[0] = new SqlParameter("@req", "GetRecordbyID");
    Param[1] = new SqlParameter("@TableID", TableID);
    DataSet ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "Strp_Tables", Param);
    return ds;
  }
  public DataSet GetRecordTable()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        var Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@req", "GetRecord");
    DataSet ds = SqlHelper.ExecuteDataset(con, CommandType.StoredProcedure, "Strp_Tables", Param);
    return ds;
  }
}