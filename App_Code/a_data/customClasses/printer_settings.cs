﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


public class printer_settings
{
   // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
    public string req { get; set; }
    public string Dep_Name { get; set; }
    public string Print_Name { get; set; }
    public string Dos { get; set; }
    public string Net_Use { get; set; }
    public string Net_Del { get; set; }

    public printer_settings()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable bindgride()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_setting_billprint", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
        return dt;

    }


    public void InsertUpdatePrinterSettings()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_setting_billprint", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@Print_Name", Print_Name);
        cmd.Parameters.AddWithValue("@Dep_Name", Dep_Name);
        cmd.Parameters.AddWithValue("@Dos", Dos);
        cmd.Parameters.AddWithValue("@Net_Use", Net_Use);
        cmd.Parameters.AddWithValue("@Net_Del", Net_Del);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        con.Close();
     

    }

    public void get_printersetting()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);

        con.Open();
        SqlCommand cmd = new SqlCommand("strp_setting_billprint", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@req", req);
        cmd.Parameters.AddWithValue("@Dep_Name", Dep_Name);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        SqlDataReader rd = cmd.ExecuteReader();
        if (rd.Read())
        {

            Dep_Name = rd["Dep_Name"].ToString();
            Print_Name = rd["Print_Com"].ToString();
            Dos= rd["Dos"].ToString();
            Net_Use = rd["Net_Use"].ToString();
            Net_Del = rd["Net_Del"].ToString();
      

        }
        con.Close();


    }
}