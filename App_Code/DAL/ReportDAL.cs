﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ReportDAL
/// </summary>
public class ReportDAL:Connection
{

    public SqlDataReader GetPurchaseReport( string BillMode, string datefrom, string dateto, Int32 BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[4];
       
        objParam[0] = new SqlParameter("@BillMode", BillMode);
        objParam[1] = new SqlParameter("@FromDate", datefrom);
        objParam[2] = new SqlParameter("@ToDate", dateto);
        objParam[3] = new SqlParameter("@BranchId", BranchId);
      
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_PurchaseDetailedReport",objParam);

            
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAllForReport(Int32 BillType, string BillMode, string datefrom, string dateto,Int32 BranchId,string SaleType)
    {

        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@BillType", BillType);
        objParam[1] = new SqlParameter("@BillMode", BillMode);
        objParam[2] = new SqlParameter("@FromDate", datefrom);
        objParam[3] = new SqlParameter("@ToDate", dateto);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        objParam[5] = new SqlParameter("@SaleType", SaleType);
        SqlDataReader dr = null;
        
        try
        {
            
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_SaleDetailedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

      public SqlDataReader GetSaleDatedReport(Int32 BillType, string BillMode, string datefrom, string dateto,Int32 BranchId,string SaleType)
    {

        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@BillType", BillType);
        objParam[1] = new SqlParameter("@BillMode", BillMode);
        objParam[2] = new SqlParameter("@FromDate", datefrom);
        objParam[3] = new SqlParameter("@ToDate", dateto);
        objParam[4] = new SqlParameter("@BranchId", BranchId);
        objParam[5] = new SqlParameter("@SaleType", SaleType);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "report_sp_SaleDatedReport", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


      public SqlDataReader GetAdvanceBooking(string BillMode, Int32 BranchId,DateTime DateFrom,DateTime DateTo)
      {

          SqlParameter[] objParam = new SqlParameter[4];

          objParam[0] = new SqlParameter("@Mode", BillMode);
          objParam[1] = new SqlParameter("@BranchId", BranchId);
          objParam[2] = new SqlParameter("@FromDate", DateFrom);
          objParam[3] = new SqlParameter("@TODate", DateTo);
          SqlDataReader dr = null;
          try
          {
              dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "Report_sp_GetAdvanceBooking", objParam);


          }

          finally
          {
              objParam = null;
          }
          return dr;

      }

    public SqlDataReader GetAllCashMemoOptions()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'CashMemo'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetAllCashMemoDatedOptions()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'CashMemoDated'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllGSTOptions()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'GSTDetail'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetCashMemo()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'CashMemo' and HideShow = 'checked'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetCashMemoDated()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'CashMemoDated' and HideShow = 'checked'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetGSTCheckedOptions()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'GSTDetail' and HideShow = 'checked'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public Int16 InsertCashMemo( DataTable dt)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@dt", dt);
       // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CashMemoOPtions", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 InsertPurchase(DataTable dt)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@dt", dt);
        // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_PurchaseOPtions", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 InsertCashMemoDated(DataTable dt)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@dt", dt);
        // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CashMemoDatedOPtions", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 InsertGSTOptions(DataTable dt)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@dt", dt);
        // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_GSTOPtions", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public SqlDataReader GetPurchaseOptions()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.Text,
            "select * from dbo.Prop_GridSetting_Report where Caption = 'Purchase'", objParam);

            //
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertTempReport(string TypeName ,int Id)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@TypeName", TypeName);
        objParam[1] = new SqlParameter("@Id", Id);
        // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "Report_sp_inserttempreport", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 DeleteTempReport(string TypeName)
    {
        GridOption obgridoption = new GridOption();
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@TypeName", TypeName);
       
        // objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "Report_sp_Deletetempreport", objParam);
            retValue = 1;
            obgridoption.Id = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

}