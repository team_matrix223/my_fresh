﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for AccGroupsDAL
/// </summary>
public class VoucherDAL:Connection
{
    public DataSet GetAllAccounts(int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_GetAllAccounts", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet GetBalanceByCode(string CCODE)
    {



        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@CCODE", CCODE);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "Report_sp_VoucherBal", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet GetAllAccountsByBank(int BranchId,string Bank)
    {



        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@Bank", Bank);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_GetAllAccountsByBank", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public SqlDataReader GetAccountByBnkSeting(Int32 BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAccountBybnkSeting", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }
    public SqlDataReader GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@DateFrom", DateFrom);
        objParam[1] = new SqlParameter("@DateTo", DateTo);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_VoucherGetByDate", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }

    public int InsertUpdate(AccTrans objTrans, DataTable dt,string BankId,string multisingle)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[18];
        objParam[0] = new SqlParameter("@VOUCH_NO", objTrans.VOUCH_NO);
        objParam[1] = new SqlParameter("@TR_DATE", objTrans.TR_DATE);
        objParam[2] = new SqlParameter("@VTYPE", objTrans.VTYPE);
        objParam[3] = new SqlParameter("@PUR_NO", objTrans.PUR_NO);
        objParam[4] = new SqlParameter("@BILL_NO", objTrans.BILL_NO);
        objParam[5] = new SqlParameter("@USERNO", objTrans.USERNO);
        objParam[6] = new SqlParameter("@Ref_No", objTrans.Ref_No);
        objParam[7] = new SqlParameter("@PASS", objTrans.PASS);
        objParam[8] = new SqlParameter("@PartyBank", objTrans.PartyBank);
        objParam[9] = new SqlParameter("@RowType", objTrans.RowType);
        objParam[10] = new SqlParameter("@AutoVoucher", objTrans.AutoVoucher);
        objParam[11] = new SqlParameter("@SrNo", objTrans.SrNo);
        objParam[12] = new SqlParameter("@ShortName", objTrans.ShortName);
        objParam[13] = new SqlParameter("@VoucherDetails", dt);
       
        objParam[14] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[15] = new SqlParameter("@BranchId", objTrans.BranchId);
       
        objParam[14].Direction = ParameterDirection.ReturnValue;
        objParam[16] = new SqlParameter("@BankId", BankId);
        objParam[17] = new SqlParameter("@multisingle", multisingle);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_VoucherInsertUpdate", objParam);
            retValue = Convert.ToInt32(objParam[14].Value);
            objTrans.VOUCH_NO = Convert.ToString(retValue);
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


    public SqlDataReader GetVoucherDetails(int VouchNo,string VType, int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@VouchNo", VouchNo);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@VType", VType);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_VoucherDetailGetByVouchNo", objParam);



        }

        finally
        {
            objParam = null;
        }
        return dr;


    }



}