﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BankDAL
/// </summary>
public class GroupDAL:Connection
{
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllGroups", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetGroupsByTax(Int32  Tax)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];
       
        ObjParam[0] = new SqlParameter("@TaxId", Tax);
       
        try
        {

            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_GroupsGetByTaxId", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }


    public SqlDataReader GetGroupsByTaxToTax(Int32 Tax)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[1];

        ObjParam[0] = new SqlParameter("@TaxId", Tax);

        try
        {

            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "master_sp_GroupsGetByTaxIdtaxto", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
}

