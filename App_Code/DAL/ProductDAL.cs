﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for ProductDAL
/// </summary>
public class ProductDAL:Connection
{


    public SqlDataReader GetById(int ItemId)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemId", ItemId);
    
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PackingBelongsGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetProductsByKeywrod(string Keywrd, int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@SearchText", Keywrd);
        SqlDataReader dr = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            dr  = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAlKeyword", objParam);
        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public DataSet GetAll(int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAll", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet GetAllForTransfer(int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAllForTransfer", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public DataSet GetAdvanceProduct(string Keywrd,int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@SearchText", Keywrd);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAlKeyword", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }



    public DataSet AdvanceSearchForStock(string Keyword, string Type, string AccountType, int GroupId, string ItemType, int GodownId, int branchId)
    {

        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GroupId", GroupId);
        objParam[4] = new SqlParameter("@ItemType", ItemType);
        objParam[5] = new SqlParameter("@GodownId", GodownId);
        objParam[6] = new SqlParameter("@BranchId", branchId);




        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchForStock", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public DataSet GetAdvanceProductForPurchase(string Keywrd, int BranchId)
    {



        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@SearchText", Keywrd);
        DataSet ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAlKeywordForPurchase", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }
    public DataSet KeywordSearch(string Keyword, string Type,string BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@Keyword", Keyword);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@branchId", BranchId);


        DataSet ds = null;
        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PackingBelongsKeywordSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

    public SqlDataReader GetByItemType(string ItemType)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ItemType", ItemType);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetItemsByType", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader AdvancedSearchForMenuCard(int CategoryId, string Keyword)
    {

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearchForMenuCard", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public void UpdateImageUrl(Int64 ProductId,string ImageUrl)
    {

 
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@ItemID", ProductId);
        objParam[1] = new SqlParameter("@ImageUrl", ImageUrl);
       
   
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
          "pos_sp_Packing_BelongsUpdateImageUrl", objParam);
           
        }
        finally
        {
            objParam = null;
        }
       
    }


    

    public SqlDataReader AdvancedSearch(int CategoryId,string Keyword,int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);
        objParam[2] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader AdvancedSearchforCounterBill(int CategoryId, string Keyword, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
       
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearchforBill_CounterBill", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader AdvancedSearchforbill(int CategoryId, string Keyword, int BranchId,int Posid)
    {

        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        objParam[3] = new SqlParameter("@PosId", Posid);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearchforBill", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader AdvancedSearchForGST(int CategoryId, string Keyword, int BranchId)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@CategoryId", CategoryId);
        objParam[1] = new SqlParameter("@Keyword", Keyword);
        objParam[2] = new SqlParameter("@BranchId", BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsAdvancedSearchforGST", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByItemCodeforgst(string Code, int BranchId, int billtype)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@billtype", billtype);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCodeforGst", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetByItemCodeforNewScreen(string Code, int BranchId, int billtype)
    {

        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@billtype", billtype);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCodefornewscreen", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetByItemCodeForStock(Product objProduct, string Type, string AccountType, int GroupId, string ItemType, int GodownId, int branchId)
    {

        SqlParameter[] objParam = new SqlParameter[7];
        objParam[0] = new SqlParameter("@Keyword", objProduct.Item_Code);
        objParam[1] = new SqlParameter("@Type", Type);
        objParam[2] = new SqlParameter("@AccountType", AccountType);
        objParam[3] = new SqlParameter("@GroupId", GroupId);
        objParam[4] = new SqlParameter("@ItemType", ItemType);
        objParam[5] = new SqlParameter("@GodownId", GodownId);
        objParam[6] = new SqlParameter("@BranchId", branchId);




        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_AccountsKeywordSearchForStockByItemCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByItemCodepsm(string Code)
    {
        
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@ItemCode", Code);
       
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCodeforpsm", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public Int32 getitemcodeCheck(string ItemCode)
    {
        Int32 retVal = 0;

        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@ItemCode", ItemCode);
        //objParam[1] = new SqlParameter("@Password", HashSHA1(objReg.Password));
       
        objParam[1] = new SqlParameter("@retval", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
       


        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_GetByItemCodeforcur", objParam);
            retVal = Convert.ToInt32(objParam[1].Value);

        }

        finally
        {
            objParam = null;
        }


        return retVal;
    }

    public SqlDataReader GetByItemCodeCounterBill(string Code, int BranchId, int billtype)
    {
        Int32 pos = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        SqlParameter[] objParam = new SqlParameter[3];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@billtype", billtype);
        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCode_CounterBill", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader NewGetByItemCode(string Code, int BranchId, int billtype,int posid)
    {
        if (posid == 0)
        {
            posid = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        }
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@billtype", billtype);
        objParam[3] = new SqlParameter("@PosId", posid);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetByItemCode(string Code, int BranchId,int billtype)
    {
        Int32 pos = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        SqlParameter[] objParam = new SqlParameter[4];
        objParam[0] = new SqlParameter("@ItemCode", Code);
        objParam[1] = new SqlParameter("@BranchId", BranchId);
        objParam[2] = new SqlParameter("@billtype", billtype);
        objParam[3] = new SqlParameter("@PosId", pos);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_ProductsGetByItemCode", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAllProduct()
    {



        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader ds = null;
        try
        {
            //ds=(DataSet)HttpContext.Current.Cache["data"];
            ds = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
              "master_sp_ItemsGetAll", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }

}