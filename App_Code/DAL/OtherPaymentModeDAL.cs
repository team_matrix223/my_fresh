﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;
/// <summary>
/// Summary description for OtherPaymentModeDAL
/// </summary>
public class OtherPaymentModeDAL:Connection
{
    public SqlDataReader GetById(OtherPaymentMode objOtherPaymentMode)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@OtherPayment_ID", objOtherPaymentMode);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "sp_OtherPaymentModeGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_OtherPaymentModeGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
    public Int16 InsertUpdate(OtherPaymentMode objOtherPaymentMode)
    {
        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];
        objParam[0] = new SqlParameter("@PaymetModeID", objOtherPaymentMode.PaymetMode);
        objParam[1] = new SqlParameter("@OtherPayment_ID", objOtherPaymentMode.OtherPayment_ID);
        objParam[2] = new SqlParameter("@OtherPayment_name", objOtherPaymentMode.OtherPayment_Name);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 3);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@UserId", objOtherPaymentMode.UserId);
        objParam[5] = new SqlParameter("@IsActive", objOtherPaymentMode.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "sp_OtherPaymentModeInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[3].Value);
            objOtherPaymentMode.OtherPayment_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(OtherPaymentMode objOtherPaymentMode)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@OtherPayment_ID", objOtherPaymentMode.OtherPayment_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "sp_OtherPaymentModeDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objOtherPaymentMode.OtherPayment_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}