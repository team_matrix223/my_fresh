﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for DepartmentDAL
/// </summary>
public class DepartmentDAL:Connection
{
    public SqlDataReader GetById(Departments objDepartment)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Prop_ID", objDepartment.Prop_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DepartmentGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetProductByDept(int Dept)
    {

        SqlParameter[] objparam = new SqlParameter[1];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@Dept", Dept);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProductByDept", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }

    public SqlDataReader GetProductByItemWise(int Dept,string ItemWise)
    {

        SqlParameter[] objparam = new SqlParameter[2];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@Dept", Dept);
        objparam[1] = new SqlParameter("@ItemWise", ItemWise);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProductByItemWIse", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }

    public SqlDataReader GetProductByDeptforHSN(int Dept,string Hsncode,string ItemWise)
    {

        SqlParameter[] objparam = new SqlParameter[3];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@Dept", Dept);
        objparam[1] = new SqlParameter("@Hsncode", Hsncode);
        objparam[2] = new SqlParameter("@ItemWise", ItemWise);
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetProductByDeptForHSNforitemwise", objparam);


        }

        finally
        {
            objparam = null;
        }
        return dr;
    }

    public int UpdateByHsnCode(Int32 Dept, string FromHsn, string ToHsn,string ItemWise)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[5];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@HSNCode", FromHsn);
        objparam[1] = new SqlParameter("@ToHsnCode", ToHsn);
        objparam[2] = new SqlParameter("@DepttId", Dept);
        objparam[4] = new SqlParameter("@ItemWise", ItemWise);
        objparam[3] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[3].Direction = ParameterDirection.ReturnValue;

        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "UpdatePackingbelongsDetailByHSncodeItemWIse", objparam);
            retval = Convert.ToInt32(objparam[3].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }


    public int UpdateByGroup(Int32 Dept, string FromGrp, string ToGrp)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[4];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@FromGrp", FromGrp);
        objparam[1] = new SqlParameter("@ToGrp", ToGrp);
        objparam[2] = new SqlParameter("@DepttId", Dept);

        objparam[3] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[3].Direction = ParameterDirection.ReturnValue;

        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "UpdatePackingbelongsDetailByGroup", objparam);
            retval = Convert.ToInt32(objparam[3].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }


    public int UpdatePackingbelongsDetail(Int64 ItemID, decimal Price, decimal Mrp,decimal tax,string HSNCode, int DepttId, int Grpid)
    {
        int retval = 0;
        SqlParameter[] objparam = new SqlParameter[8];
        SqlDataReader dr = null;
        objparam[0] = new SqlParameter("@ItemID", ItemID);
        objparam[1] = new SqlParameter("@Price", Price);
        objparam[2] = new SqlParameter("@Mrp", Mrp);
		objparam[4] = new SqlParameter("@tax", tax);
        objparam[5] = new SqlParameter("@HSNCode", HSNCode);
        objparam[6] = new SqlParameter("@DepttId", DepttId);
        objparam[7] = new SqlParameter("@Grpid", Grpid);
        objparam[3] = new SqlParameter("@RetVal", SqlDbType.Int, 4);
        objparam[3].Direction = ParameterDirection.ReturnValue;
    
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "UpdatePackingbelongsDetail", objparam);
            retval = Convert.ToInt32(objparam[3].Value);

        }

        finally
        {
            objparam = null;
        }
        return retval;
    }



    public SqlDataReader GetAll()
    {
        List<Departments> DepartmentList = new List<Departments>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_DepartmentsGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Departments objDepartment)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@PROP_ID ", objDepartment.Prop_ID);
        objParam[1] = new SqlParameter("@PROP_NAME", objDepartment.Prop_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objDepartment.IsActive);
        objParam[4] = new SqlParameter("@UserId", objDepartment.UserId);
        objParam[5] = new SqlParameter("@posid", objDepartment.posid);
        objParam[6] = new SqlParameter("@showinmenu", objDepartment.showinmenu);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DepartmentInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objDepartment.Prop_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public int Delete(Departments objDepartment)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Prop_ID", objDepartment.Prop_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DepartmentDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objDepartment.Prop_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}