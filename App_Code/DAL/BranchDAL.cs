﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BranchDAL
/// </summary>
public class BranchDAL:Connection
{



    public SqlDataReader GetAll()
    {
        string Designation = "";
        Int32 Branch = 0;
        try
        {
            Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

            Designation = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value);
        }
        catch
        {

        }
       
        SqlDataReader dr = null;
        if ( Designation == "")
        {
            SqlParameter[] objParam = new SqlParameter[0];
 
            try
            {

                dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
                "master_sp_BranchGetAll", objParam);


            }

            finally
            {
                objParam = null;
            }

        }
        else
        {

            

             
            SqlParameter[] objParam = new SqlParameter[1];


            objParam[0] = new SqlParameter("@BranchId ", Branch);

            try
            {

                dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
                "master_sp_BranchGetById", objParam);


            }

            finally
            {
                objParam = null;
            }
        }
        return dr;
    }


    public SqlDataReader GetAllExceptLoginBranch()
    {
        SqlDataReader dr = null ;
        int LoginBranch = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        LoginBranch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        objParam[0] = new SqlParameter("@BranchId ", LoginBranch);

        try
        {

            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BranchGetExceptLogin", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }

    public SqlDataReader GetAllBranch()
    {
        SqlDataReader dr = null;
        
        SqlParameter[] objParam = new SqlParameter[0];
        

         try
        {

            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetAllBranch", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;
    }
    public Int16 InsertUpdate(Branches objBranch)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@BranchId ", objBranch.BranchId);
        objParam[1] = new SqlParameter("@BranchName", objBranch.BranchName);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@IsActive", objBranch.IsActive);
        objParam[4] = new SqlParameter("@UserId", objBranch.UserId);
        objParam[5] = new SqlParameter("@StateId", objBranch.StateId);
        objParam[6] = new SqlParameter("@CityId", objBranch.CityId);
        objParam[7] = new SqlParameter("@Address", objBranch.Address);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BranchInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objBranch.BranchId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public SqlDataReader GetById(Branches objBranch)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objBranch.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_BranchGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    //public SqlDataReader GetSaleRateAll(int BranchId, string ItemCode)
    //{
    //    SqlDataReader dr = null;
    //    SqlParameter[] objParam = new SqlParameter[2];
    //    objParam[0] = new SqlParameter("@BranchId", BranchId);
    //    objParam[1] = new SqlParameter("@ItemCode", ItemCode);
   
    //    try
    //    {
    //        dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
    //        "pos_sp_GetSaleRateAccToBranch", objParam);


    //    }

    //    finally
    //    {
    //        objParam = null;
    //    }
    //    return dr;

    //}
    public int Delete(Branches objBranch)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@BranchId", objBranch.BranchId);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BranchDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBranch.BranchId = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

    public SqlDataReader GetBranchState(Branches objBranch)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@BranchId", objBranch.BranchId);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetBranchcState", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }
}