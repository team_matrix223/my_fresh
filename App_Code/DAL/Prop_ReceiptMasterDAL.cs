﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for Prop_ReceiptMasterDAL
/// </summary>
public class Prop_ReceiptMasterDAL : Connection
{
    public Int32 InsertUpdate(Prop_ReceiptMaster objReceiptMaster)
    {

        Int32 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[11];
        objParam[0] = new SqlParameter("@Receipt_No", objReceiptMaster.Receipt_No);
        objParam[1] = new SqlParameter("@Receipt_Date", objReceiptMaster.Receipt_Date);
        objParam[2] = new SqlParameter("@CCode", objReceiptMaster.CCode);
        objParam[3] = new SqlParameter("@CName", objReceiptMaster.CName);
        objParam[4] = new SqlParameter("@Amount", objReceiptMaster.Amount);
        objParam[5] = new SqlParameter("@ModeOfPayment", objReceiptMaster.ModeOfPayment);
        objParam[6] = new SqlParameter("@Bank_Name", objReceiptMaster.Bank_Name);
        objParam[7] = new SqlParameter("@CHQNO", objReceiptMaster.CHQNO);
        objParam[8] = new SqlParameter("@userno", objReceiptMaster.userno);
        objParam[9] = new SqlParameter("@BranchId", objReceiptMaster.BranchId);
       

        objParam[10] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[10].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ReceiptMasterInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[10].Value);
            objReceiptMaster.Receipt_No = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public void GetByIdForReceiptMaster(Prop_ReceiptMaster objReceiptMaster)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@Receipt_No", objReceiptMaster.Receipt_No);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ReceiptMasterGetByReceiptNo", objParam);
            if (dr.HasRows)
            {
                dr.Read();
                objReceiptMaster.Receipt_No = Convert.ToInt32(dr["Receipt_No"]);
                objReceiptMaster.Receipt_Date = Convert.ToDateTime(dr["Receipt_Date"]);
                objReceiptMaster.CCode = Convert.ToString(dr["CCode"]);
                objReceiptMaster.CName = Convert.ToString(dr["CName"]);
                objReceiptMaster.Amount = Convert.ToDecimal(dr["Amount"]);
                objReceiptMaster.ModeOfPayment = Convert.ToString(dr["ModeOfPayment"]);
                objReceiptMaster.Bank_Name = Convert.ToString(dr["Bank_Name"]);
                objReceiptMaster.CHQNO = Convert.ToString(dr["CHQNO"]);
                objReceiptMaster.userno = Convert.ToInt32(dr["userno"]);
                objReceiptMaster.BranchId = Convert.ToInt32(dr["BranchId"]);
                    
                

            }


        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }


    }



    public DataSet ReceiptMasterGetByDate(string StartDate, string EndDate,int BranchId )
    {

        SqlParameter[] objParam = new SqlParameter[3];
        DataSet ds = new DataSet();
        objParam[0] = new SqlParameter("@StartDate", StartDate);
        objParam[1] = new SqlParameter("@EndDate", EndDate);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
   

        try
        {
            ds = SqlHelper.ExecuteDataset(sqlDataString, CommandType.StoredProcedure,
            "master_sp_ReceiptMasterGetByDate", objParam);
        }

        finally
        {
            objParam = null;
        }
        return ds;

    }


    public string GetOutstandingForReceiptMaster(string CustomerId,int BranchId)
    {
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CustomerId", CustomerId);
        objParam[1] = new SqlParameter("@BranchId", BranchId);

        string retVal = "";
        try
        {
            retVal = Convert.ToString(SqlHelper.ExecuteScalar(sqlDataString, CommandType.StoredProcedure,
            "master_sp_GetOutstandingForReceitpMaster", objParam));
        }
        finally
        {
            objParam = null;
        }
        return retVal;

    }
   
}