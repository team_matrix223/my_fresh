﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for CompanyDAL
/// </summary>
public class CompanyDAL:Connection
{
    public SqlDataReader GetById(Company objCompany)
    {
        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Company_ID", objCompany.Company_ID);

        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CompanyGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public SqlDataReader GetAll()
    {
        List<Company> CompanyList = new List<Company>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_CompanyGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }


    public SqlDataReader GetPSMCompanyAll()
    {
        List<Company> CompanyList = new List<Company>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "master_sp_PSMCompanyGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public Int16 InsertUpdate(Company objCompany,DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@Company_ID", objCompany.Company_ID);
        objParam[1] = new SqlParameter("@Company_Name", objCompany.Company_Name);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@UserId", objCompany.UserId);
        objParam[4] = new SqlParameter("@IsActive", objCompany.IsActive);
        objParam[5] = new SqlParameter("@dt", dt);
        
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CompanyInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);
            objCompany.Company_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Company objCompany)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Company_ID", objCompany.Company_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CompanyDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objCompany.Company_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

}