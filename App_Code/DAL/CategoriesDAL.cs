﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
public class CategoriesDAL : Connection
{

    public SqlDataReader NewGetAll(Int32 posid)
    {

        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@posid",posid );
        // objParam[0] = new SqlParameter("@posid", HttpContext.Current.Request.Cookies[Constants.posid].Value);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_Prop_GroupsGetActive", objParam);

        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

    public SqlDataReader GetAll()
    {
        
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@posid",HttpContext.Current.Request.Cookies[Constants.posid].Value);
        SqlDataReader dr=null;
         try
         {
             dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
             "pos_sp_Prop_GroupsGetActive", objParam);
 
         }

         finally
         {
             objParam = null;
         }
         return dr;

    }

    public SqlDataReader GetAllforCounterBill()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_Prop_GroupsGetActive_CounerBill", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



}