﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SaleDAL
/// </summary>
public class SaleDAL:Connection
{

    public int InsertNewDel(int BranchId, DataTable dt, string ItemType,int Pos)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@ItemType", ItemType);
        objParam[3] = new SqlParameter("@Type", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@Pos", Pos);



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateTempItemsNewDel", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }



    public int InsertItemToDelete(int BranchId, DataTable dt, string ItemType, int Pos)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@ItemType", ItemType);
        objParam[3] = new SqlParameter("@Type", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@Pos", Pos);



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateItemsToDelete", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }

    public SqlDataReader GetProductsByTypeNewDel(string ItemType, int BranchId,int Pos)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@ItemType", ItemType);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
        ObjParam[2] = new SqlParameter("@Pos", Pos);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Master_Sp_GettemporaryItemsNewDel", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }

    public SqlDataReader GetProductsToDeleteByTypeNewDel(string ItemType, int BranchId, int Pos)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[3];
        ObjParam[0] = new SqlParameter("@ItemType", ItemType);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);
        ObjParam[2] = new SqlParameter("@Pos", Pos);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Master_Sp_DelGettemporaryItemsNewDel", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }




    public int Insert( int BranchId,DataTable dt,string ItemType)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[4];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@ItemType", ItemType);
        objParam[3] = new SqlParameter("@Type", dt);
        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;



        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_InsertUpdateTempItems", objParam);
            retValue = Convert.ToInt32(objParam[2].Value);
          
        }
        finally
        {
            objParam = null;
        }
        return retValue;




    }


    public SqlDataReader GetProductsByType(string ItemType,int BranchId)
    {
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[2];
        ObjParam[0] = new SqlParameter("@ItemType", ItemType);
        ObjParam[1] = new SqlParameter("@BranchId", BranchId);


        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure, "Master_Sp_GettemporaryItems", ObjParam);

        }
        finally
        {
            ObjParam = null;
        }
        return dr;
    }
    public Int16 DeleteBillsNewDel(int BranchId, DataTable dt, string ItemType, string Saletype)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@BillType", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@Type", ItemType);
        objParam[4] = new SqlParameter("@SaleType", Saletype);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SaleDeletionNewDel", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public Int16 DeleteBills(int BranchId, DataTable dt, string ItemType, string Saletype)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@BillType", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@Type", ItemType);
        objParam[4] = new SqlParameter("@SaleType", Saletype);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_SaleDeletion", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public Int16 DeleteDeliveryNotes(int BranchId, DataTable dt)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@DeliveryType", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
     
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_DeleteDeliveryNoteWithReceive", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public Int16 DeleteItemBillsNewdel(int BranchId, DataTable dt, string ItemType, string Saletype, DateTime DateFrom, DateTime DateTo,int Pos)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[8];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@BillType", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@Type", ItemType);
        objParam[4] = new SqlParameter("@SaleType", Saletype);
        objParam[5] = new SqlParameter("@DateFrom", DateFrom);
        objParam[6] = new SqlParameter("@DateTo", DateTo);
        objParam[7] = new SqlParameter("@Pos", Pos);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ItemSaleDeletionNewDel", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }


    public Int16 DeleteItemBills(int BranchId, DataTable dt, string ItemType, string Saletype, DateTime DateFrom, DateTime DateTo)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[7];

        objParam[0] = new SqlParameter("@BranchId", BranchId);
        objParam[1] = new SqlParameter("@BillType", dt);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@Type", ItemType);
        objParam[4] = new SqlParameter("@SaleType", Saletype);
        objParam[5] = new SqlParameter("@DateFrom", DateFrom);
        objParam[6] = new SqlParameter("@DateTo", DateTo);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_ItemSaleDeletion", objParam);
            retValue = Convert.ToInt16(objParam[2].Value);

        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }
}