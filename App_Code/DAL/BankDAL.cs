﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for BankDAL
/// </summary>
public class BankDAL:Connection
{

    public SqlDataReader GetById(Banks objBanks)
    {

        SqlParameter[] objParam = new SqlParameter[1];

        objParam[0] = new SqlParameter("@Bank_ID", objBanks);
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_BanksGetById", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }




    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_BanksGetAll", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }



    public Int16 InsertUpdate(Banks objBank)
    {

        Int16 retValue = 0;
        SqlParameter[] objParam = new SqlParameter[6];

        objParam[0] = new SqlParameter("@Bank_ID", objBank.Bank_ID);
        objParam[1] = new SqlParameter("@Bank_Name", objBank.Bank_Name);
        objParam[2] = new SqlParameter("@ccode", objBank.CCODE);
        objParam[3] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[3].Direction = ParameterDirection.ReturnValue;
        objParam[4] = new SqlParameter("@UserId", objBank.UserId);
        objParam[5] = new SqlParameter("@IsActive", objBank.IsActive);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BankInsertUpdate", objParam);
            retValue = Convert.ToInt16(objParam[3].Value);
            objBank.Bank_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }

    public int Delete(Banks objBank)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@Bank_ID", objBank.Bank_ID);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_BankDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objBank.Bank_ID = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}