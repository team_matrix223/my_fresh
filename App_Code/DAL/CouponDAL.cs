﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for BillDAL
/// </summary>
public class CouponDAL:Connection
{


    public int CouponAmt(Coupons objCoupon)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@CouponNo", objCoupon.CouponNo);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CouponValue", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objCoupon.CouponAmt = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }

    public int Delete(Coupons objCoupon)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[2];
        objParam[0] = new SqlParameter("@RefNo", objCoupon.RefNo);

        objParam[1] = new SqlParameter("@retVal", SqlDbType.Int, 5);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "master_sp_CouponDeleteById", objParam);
            retValue = Convert.ToInt32(objParam[1].Value);
            objCoupon.RefNo = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }


    public Int32 Insert(Coupons objCoupon)
    {




        Int16 retval = 0;
        SqlParameter[] objParam = new SqlParameter[5];

        objParam[0] = new SqlParameter("@CouponNo", objCoupon.NoOfCoupons);
        objParam[1] = new SqlParameter("@CouponAmt", objCoupon.Amount);

        objParam[2] = new SqlParameter("@retVal", SqlDbType.Int, 4);
        objParam[2].Direction = ParameterDirection.ReturnValue;
        objParam[3] = new SqlParameter("@BillNo", objCoupon.BillNo);
        objParam[4] = new SqlParameter("@BranchId", objCoupon.BranchId);
       

        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_insertCoupon", objParam);
            retval = Convert.ToInt16(objParam[4].Value);
            objCoupon.RefNo = retval;
        }
        finally
        {
            objParam = null;
        }
        return retval;
    }

 

    public SqlDataReader GetAll()
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetAllCoupons", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

}