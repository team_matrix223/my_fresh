﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

/// <summary>
/// Summary description for KotPrintDAL
/// </summary>
public class KotPrintDAL:Connection
{
    public SqlDataReader KOTprint(int UserNo)
    {
        SqlParameter[] objParam = new SqlParameter[1];
        SqlDataReader dr = null;
        objParam[0] = new SqlParameter("@UserNo", UserNo);

        try
        {
           dr =  SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
           "pos_sp_PrintKOT", objParam);

        }
        finally
        {
            objParam = null;
        }

        return dr;
    }
}