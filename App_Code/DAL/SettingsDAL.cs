﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for SettingsDAL
/// </summary>
public class SettingsDAL : Connection
{
    public string InsertUpdate(Settings objSettings)
    {

        string retValue = "";
        SqlParameter[] objParam = new SqlParameter[2];

        objParam[0] = new SqlParameter("@Apptype", objSettings.Apptype);
       
        objParam[1] = new SqlParameter("@retVal", SqlDbType.Text, 4);
        objParam[1].Direction = ParameterDirection.ReturnValue;
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
          "POS_Settings_InsertUpdate", objParam);
            retValue = objParam[1].Value.ToString();
            objSettings.Apptype = retValue;
        }
        finally
        {
            objParam = null;
        }
        return retValue;
    }



    public SqlDataReader GetDiscountType()
    {

        SqlParameter[] objParam = new SqlParameter[0];
      
        SqlDataReader dr = null;
        try
        {
            dr = SqlHelper.ExecuteReader(sqlDataString, CommandType.StoredProcedure,
            "pos_sp_GetDiscountOption", objParam);


        }

        finally
        {
            objParam = null;
        }
        return dr;

    }

}