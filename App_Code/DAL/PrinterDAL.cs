﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

/// <summary>
/// Summary description for PrinterDAL
/// </summary>
public class PrinterDAL:Connection
{
    public int DeletePrinter(int UserNo)
    {
        int retValue = 0;
        SqlParameter[] objParam = new SqlParameter[1];
        objParam[0] = new SqlParameter("@UserNo", UserNo);
        try
        {
            SqlHelper.ExecuteNonQuery(sqlDataString, CommandType.StoredProcedure,
           "DeletePrinter", objParam);
           
        }
        finally
        {
            objParam = null;
        }
        return retValue;


    }
}