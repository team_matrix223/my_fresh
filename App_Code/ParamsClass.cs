﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for ParamClass
/// </summary>

public class ParamsClass
{


    public static string gstrApplicationPreffix = System.Configuration.ConfigurationManager.AppSettings["SITE_PREFIX"];


   public static string sqlDataString =Decrypt(HttpContext.Current.Request.Cookies[Constants.dbname].Value);

   //public static string sqlDataString = System.Configuration.ConfigurationManager.AppSettings["DB_CONN"];
    public static string gstrStoredProcPrefix = gstrApplicationPreffix + "SP_";

    public static string Decrypt(string cipherText)
        {
        string EncryptionKey = "0123FGHIJKLMNOPQRST456789ABCDEUVWXYZ";
        cipherText = cipherText.Replace(" ", "+");
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] {
            0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76
        });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

}