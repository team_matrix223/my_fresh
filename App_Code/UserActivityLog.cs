﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for UserActivityLog
/// </summary>
public static class UserActivityLog
{
    public static void  SetActivityLog(string type,string pagename)
    {

        try
        {
            string user = HttpContext.Current.Request.Cookies[Constants.AdminId].Value;
            if (user != "0")
            {

                Connection connn = new Connection();
                SqlConnection con = new SqlConnection(connn.sqlDataString);
                con.Open();
                SqlCommand cmd = new SqlCommand("strp_activitylog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
                cmd.Parameters.AddWithValue("@page_name", pagename);
                cmd.Parameters.AddWithValue("@activity_type", type);
                cmd.ExecuteNonQuery();
                con.Close();

            }
        }
        catch(Exception ex)
        {


        }
    }

    public static void SetActivityLog(string type, string pagename, string user)
    {

        try
        {
         
            if (user != "0")
            {

                Connection connn = new Connection();
                SqlConnection con = new SqlConnection(connn.sqlDataString);
                con.Open();
                SqlCommand cmd = new SqlCommand("strp_activitylog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", user);
                cmd.Parameters.AddWithValue("@page_name", pagename);
                cmd.Parameters.AddWithValue("@activity_type", type);
                cmd.ExecuteNonQuery();
                con.Close();

            }
        }
        catch (Exception ex)
        {


        }
    }
}