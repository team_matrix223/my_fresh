﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for OtherPaymentBLL
/// </summary>
public class OtherPaymentBLL
{
    public Int32 DeleteOtherPayment(OtherPayment objOtherPayment)
    {
        return new OtherPaymentDAL().Delete(objOtherPayment);
    }
    public void GetById(OtherPayment objOtherPayment)
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    objOtherPayment.OtherPayment_Name = dr["OtherPayment_Name"].ToString();
                 
                    objOtherPayment.OtherPayment_ID = Convert.ToInt16(dr["OtherPayment_ID"]);
                    objOtherPayment.UserId = Convert.ToInt16(dr["UserId"]);
                    objOtherPayment.IsActive = Convert.ToBoolean(dr["IsActive"]);

                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }


    }

    public List<OtherPayment> GetAll()
    {
        List<OtherPayment> OtherPaymentList = new List<OtherPayment>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    OtherPayment objOtherPayment = new OtherPayment()
                    {
                        OtherPayment_Name = dr["OtherPayment_Name"].ToString(),

                        OtherPayment_ID = Convert.ToInt16(dr["OtherPayment_ID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                    };
                    OtherPaymentList.Add(objOtherPayment);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return OtherPaymentList;

    }

    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["OtherPayment_ID"].ToString(), dr["OtherPayment_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public Int16 InsertUpdate(OtherPayment objOtherPayment)
    {

        return new OtherPaymentDAL().InsertUpdate(objOtherPayment);
    }

}