﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for OrdersBLL
/// </summary>
public class OrdersBLL
{

    public Int32 DeleteOrder(Orders objOrder)
    {
        return new OrderDAL().Delete(objOrder);
    }



    public Int32 LikeProduct(int ProductId)
    {
        return new OrderDAL().LikeProduct(ProductId);
    }
    public Int32 InsertUpdate(Orders objOrders, DataTable dt)
    {
        return new OrderDAL().InsertUpdate(objOrders, dt);
    }

    public List<Product> GetFavourites()
    {

        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];
        List<Product> lst = new List<Product>();
        try
        {
            dr = new OrderDAL().GetFavourites();
            if (dr.HasRows)
            {
                while (dr.Read())
                {


                    Product objProduct = new Product();
                    objProduct.ItemID = Convert.ToInt32(dr["ItemID"].ToString());
                    objProduct.Item_Code = dr["Item_Code"].ToString();
                    objProduct.Item_Name = dr["Item_Name"].ToString();
                    objProduct.Sale_Rate = Convert.ToDecimal(dr["Sale_Rate"].ToString());
                    objProduct.Tax_Code = Convert.ToDecimal(dr["Tax_Code"].ToString());
                    objProduct.Tax_ID = Convert.ToInt32(dr["Tax_ID"].ToString());
                    objProduct.SurVal = Convert.ToDecimal(dr["SurVal"].ToString());
                    objProduct.Likes = Convert.ToInt32(dr["Likes"].ToString());
                    objProduct.ItemImage = "productimages/T_" + dr["ImageUrl"].ToString();
                    lst.Add(objProduct);
                }


            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return lst;
    }

}