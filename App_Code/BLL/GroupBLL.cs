﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for GroupBLL
/// </summary>
public class GroupBLL
{
    public List<Group> GetAll()
    {
        List<Group> GroupList = new List<Group>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new GroupDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Group objGroups = new Group()
                    {
                        Group_Id = Convert.ToInt16(dr["Group_Id"]),
                        Group_Name = dr["Group_Name"].ToString(),
                        ImageUrl = dr["ImageUrl"].ToString(),
                        IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                        SerTax = Convert.ToDecimal(dr["SerTax"].ToString()),

                    };
                    GroupList.Add(objGroups);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return GroupList;

    }




    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new GroupDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Group_ID"].ToString(), dr["Group_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public List<Group> GetGroupsByTax(Int32  Tax)
    {
        List<Group> GroupList = new List<Group>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new GroupDAL ().GetGroupsByTax(Tax);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Group  objBill = new Group ()
                    {
                        Group_Id = Convert.ToInt32 (dr["Group_Id"].ToString()),
                        Group_Name = Convert.ToString (dr["Group_Name"]),
                       
                    };


                    GroupList .Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return GroupList ;

    }



    public List<Group> GetGroupsByTaxToTax(Int32 Tax)
    {
        List<Group> GroupList = new List<Group>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new GroupDAL().GetGroupsByTaxToTax(Tax);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Group objBill = new Group()
                    {
                        Group_Id = Convert.ToInt32(dr["Group_Id"].ToString()),
                        Group_Name = Convert.ToString(dr["Group_Name"]),

                    };


                    GroupList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return GroupList;

    }
}