﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for SettingExPrintingBLL
/// </summary>
public class SettingExPrintingBLL
{

    public string GetOptions()
    {
        
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new SettingExPrintingDAL().GetAll();
        if (dr.HasRows)
        {
            int Counter = 1;
            str.Append("<tr><th>DeptName</th><th>PrintName</th><th></th></tr>");
            while (dr.Read())
            {

                str.Append(string.Format("<tr><td>{0}</td><td style='width:350px' ><input type='text' value = '{1}' id = 'txtid" + Counter + "' /> </td><td style='width:80px'><div style='margin-bottom:5px'  class='btn btn-primary btn-small' onclick='javascript:UpdateRecord(\"{0}\"," + Counter + ");' name='dvUpdate'><b>Update</b></div></td></tr>", dr["Dep_Name"], dr["Print_Com"]));
                Counter++;

            }


        }
        return str.ToString();
    }


    public List<SettingExPrinting> GetAll()
    {

        List<SettingExPrinting> lst = new List<SettingExPrinting>();

        SqlDataReader dr = null;

        try
        {
            dr = new SettingExPrintingDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    SettingExPrinting objSale = new SettingExPrinting();
                    objSale.Dep_Name = Convert.ToString(dr["Dep_Name"].ToString());
                    objSale.Print_com = Convert.ToString(dr["Print_Com"]);
                    lst.Add(objSale);
                }
            }
        }
        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return lst;

    }
}