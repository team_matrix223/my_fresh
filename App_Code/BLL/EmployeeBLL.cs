﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for EmployeeBLL
/// </summary>
public class EmployeeBLL
{

    public Int32 DeleteEmployee(Employees objEmployee)
    {
        return new EmployeeDAL().Delete(objEmployee);
    }

    public Int16 InsertUpdate(Employees objEmployee)
    {

        return new EmployeeDAL().InsertUpdate(objEmployee);
    }
    public List<Employees> GetAll()
    {
        List<Employees> EmployeesList = new List<Employees>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new EmployeeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Employees objEmployees = new Employees()
                    {
                        Code = Convert.ToInt16(dr["Code"]),
                        Name = dr["Name"].ToString(),
                        Address1 = dr["Address1"].ToString(),
                        Address2 = dr["Address2"].ToString(),
                        Address3 = dr["Address3"].ToString(),
                        ContactNo = dr["ContactNo"].ToString(),
                        ContactPerson = dr["ContactPerson"].ToString(),
                        Auth = Convert.ToBoolean(dr["Auth"].ToString()),
                        UserId = Convert.ToInt32(dr["UserId"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        AreaId = Convert.ToInt32(dr["AreaId"]),
                        CityId = Convert.ToInt32(dr["CityId"]),
                        StateId = Convert.ToInt32(dr["StateId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"].ToString()),
                        KotPswrd = dr["KotPswrd"].ToString(),
                        ComplimentryValue = Convert.ToDecimal(dr["ComplimentryValue"].ToString()),
                        Discount = Convert.ToDecimal(dr["Discount"].ToString()),
                        Designation = dr["designation_id"].ToString(),

                    };
                    EmployeesList.Add(objEmployees);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return EmployeesList;

    }

    public void GetById(Employees objEmployee)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new EmployeeDAL().GetById(objEmployee);
            if (dr.HasRows)
            {
                dr.Read();

                objEmployee.Name = dr["Name"].ToString();
                objEmployee.Code = Convert.ToInt16(dr["Code"]);
                objEmployee.Address1 = dr["Address1"].ToString();
                objEmployee.Address2 = Convert.ToString(dr["Address2"]);
                objEmployee.Address3 = Convert.ToString(dr["Address3"]);
                objEmployee.ContactNo = Convert.ToString(dr["ContactNo"]);
                objEmployee.Auth = Convert.ToBoolean(dr["Auth"]);
                objEmployee.UserId = Convert.ToInt16(dr["UserId"]);
                objEmployee.BranchId = Convert.ToInt16(dr["BranchId"]);
                objEmployee.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objEmployee.AreaId = Convert.ToInt16(dr["AreaId"]);
                objEmployee.CityId = Convert.ToInt16(dr["CityId"]);
                objEmployee.StateId = Convert.ToInt16(dr["StateId"]);
                objEmployee.KotPswrd = Convert.ToString(dr["KotPswrd"]);
                objEmployee.ComplimentryValue = Convert.ToDecimal(dr["ComplimentryValue"]);
                objEmployee.Discount = Convert.ToDecimal(dr["Discount"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }

    public void GetCompByEmpId(Employees objEmployee)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new EmployeeDAL().GetCompByEmpId(objEmployee);
            if (dr.HasRows)
            {
                dr.Read();
                objEmployee.ComplimentryValue = Convert.ToDecimal(dr["CompVal"]);
                objEmployee.Balance = Convert.ToDecimal(dr["Balance"]);
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new EmployeeDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option value ='0'>Employee</option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Code"].ToString(), dr["Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



}