﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for CitiesBLL
/// </summary>
public class CouponBLL
{
    public Int32 DeleteCity(Cities objCities)
    {
        return new CitiesDAL().Delete(objCities);
    }
    public void GetById(Cities objCities)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new CitiesDAL().GetById(objCities);
            if (dr.HasRows)
            {
                dr.Read();


                objCities.City_Name = dr["City_Name"].ToString();
                objCities.City_ID = Convert.ToInt16(dr["City_ID"]);
                objCities.UserId = Convert.ToInt16(dr["UserId"]);
                objCities.IsActive = Convert.ToBoolean(dr["IsActive"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


    public List<Coupons> GetAll()
    {
        List<Coupons> CouponList = new List<Coupons>();

        SqlDataReader dr = null;
        try
        {
            dr = new CouponDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Coupons objCoupon = new Coupons()
                    {
                        RefNo = Convert.ToInt32(dr["RefNo"]),

                        CouponFrom = Convert.ToInt32(dr["CouponFrom"]),
                        CouponTo = Convert.ToInt32(dr["CouponTo"]),
                        Amount = Convert.ToInt32(dr["Amount"]),
                        NoOfCoupons = Convert.ToInt32(dr["NoOfCoupons"]),
                        BillNo = Convert.ToString(dr["BillNo"]),
                    };
                    CouponList.Add(objCoupon);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return CouponList;

    }


    public Int32 DeleteCoupon(Coupons objCoupon)
    {
        return new CouponDAL().Delete(objCoupon);
    }

    public Int32 CouponAmt(Coupons objCoupon)
    {
        return new CouponDAL().CouponAmt(objCoupon);
    }

    public Int32 InsertUpdate(Coupons objCoupon)
    {

        return new CouponDAL().Insert(objCoupon);
    }

}