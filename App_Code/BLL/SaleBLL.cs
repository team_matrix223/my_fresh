﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SaleBLL
/// </summary>
public class SaleBLL
{
    public int InsertnewDel(int BranchId, DataTable dt, string ItemType,int Pos)
    {

        return new SaleDAL().InsertNewDel(BranchId, dt, ItemType,Pos);
    }

    public int InsertItemToDelete(int BranchId, DataTable dt, string ItemType, int Pos)
    {

        return new SaleDAL().InsertItemToDelete(BranchId, dt, ItemType, Pos);
    }


    public List<Sale> GetProductsByTypeNewDel(string ItemType, int BranchId,int Pos)
    {

        List<Sale> lst = new List<Sale>();

        SqlDataReader dr = null;

        try
        {
            dr = new SaleDAL().GetProductsByTypeNewDel(ItemType, BranchId,Pos);
            if (dr.HasRows)
            {
                while (dr.Read())
                {



                    Sale objSale = new Sale();

                    objSale.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objSale.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objSale.MRP = Convert.ToDecimal(dr["MRP"]);
                    objSale.Rate = Convert.ToDecimal(dr["Rate"]);
                    objSale.Tax_Code = Convert.ToDecimal(dr["Tax_Code"]);
                    objSale.master_code = Convert.ToString(dr["master_code"]);
                    objSale.qty_to_less = Convert.ToDecimal(dr["qty_to_less"]);
                    objSale.godown_id = Convert.ToInt32(dr["godown_id"]);
                    objSale.BranchId = Convert.ToInt32(dr["BranchId"]);
                    objSale.Item_Type = Convert.ToString(dr["Item_Type"]);
                    lst.Add(objSale);

                }

            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }
        return lst;

    }



    public List<Sale> GetProductsToDeleteByTypeNewDel(string ItemType, int BranchId, int Pos)
    {

        List<Sale> lst = new List<Sale>();

        SqlDataReader dr = null;

        try
        {
            dr = new SaleDAL().GetProductsToDeleteByTypeNewDel(ItemType, BranchId, Pos);
            if (dr.HasRows)
            {
                while (dr.Read())
                {



                    Sale objSale = new Sale();

                    objSale.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objSale.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objSale.MRP = Convert.ToDecimal(dr["MRP"]);
                    objSale.Rate = Convert.ToDecimal(dr["Rate"]);
                    objSale.Tax_Code = Convert.ToDecimal(dr["Tax_Code"]);
                    objSale.master_code = Convert.ToString(dr["master_code"]);
                    objSale.qty_to_less = Convert.ToDecimal(dr["qty_to_less"]);
                    objSale.godown_id = Convert.ToInt32(dr["godown_id"]);
                    objSale.BranchId = Convert.ToInt32(dr["BranchId"]);
                    objSale.Item_Type = Convert.ToString(dr["Item_Type"]);
                    objSale.RRItem_Code = Convert.ToString(dr["RRItem_Code"].ToString());
                    objSale.RRItem_Name = Convert.ToString(dr["RRItem_Name"]);
                    objSale.RRMRP = Convert.ToDecimal(dr["RRItem_MRP"]);
                    objSale.RRRate = Convert.ToDecimal(dr["RRRate"]);
                    objSale.RRTax_Code = Convert.ToDecimal(dr["RRTax_Code"]);

                    lst.Add(objSale);

                }

            }

        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }
        return lst;

    }


    public int Insert(int BranchId, DataTable dt,string ItemType)
    {

        return new SaleDAL().Insert(BranchId, dt, ItemType);
    }

    public List<Sale> GetProductsByType(string ItemType, int BranchId)
    {

        List<Sale> lst = new List<Sale>();
      
        SqlDataReader dr = null;
        
        try
        {
            dr = new SaleDAL().GetProductsByType(ItemType, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {



                    Sale objSale = new Sale();

                    objSale.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objSale.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objSale.MRP = Convert.ToDecimal(dr["MRP"]);
                    objSale.Rate = Convert.ToDecimal(dr["Rate"]);
                    objSale.Tax_Code = Convert.ToDecimal(dr["Tax_Code"]);
                    objSale.master_code = Convert.ToString(dr["master_code"]);
                    objSale.qty_to_less = Convert.ToDecimal(dr["qty_to_less"]);
                    objSale.godown_id = Convert.ToInt32(dr["godown_id"]);
                    objSale.BranchId = Convert.ToInt32(dr["BranchId"]);
                    objSale.Item_Type = Convert.ToString(dr["Item_Type"]);
                    lst.Add(objSale);

                }

            }
           
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }
        return lst;

    }


    public Int16 DeleteBillsnewDel(int BranchId, DataTable dt, string ItemType, string Saletype)
    {
        return new SaleDAL().DeleteBillsNewDel(BranchId, dt, ItemType, Saletype);
    }
    public Int16 DeleteBills(int BranchId, DataTable dt, string ItemType, string Saletype)
    {
        return new SaleDAL().DeleteBills(BranchId, dt,ItemType,Saletype);
    }

    public Int16 DeleteDeliveryNotes(int BranchId, DataTable dt)
    {
        return new SaleDAL().DeleteDeliveryNotes(BranchId, dt);
    }

    public Int16 DeleteItemBillsNewDel(int BranchId, DataTable dt, string ItemType, string Saletype, DateTime DateFrom, DateTime DateTo,int Pos)
    {
        return new SaleDAL().DeleteItemBillsNewdel(BranchId, dt, ItemType, Saletype, DateFrom, DateTo,Pos);
    }

    public Int16 DeleteItemBills(int BranchId, DataTable dt, string ItemType, string Saletype, DateTime DateFrom, DateTime DateTo)
    {
        return new SaleDAL().DeleteItemBills(BranchId, dt, ItemType, Saletype, DateFrom, DateTo);
    }
}