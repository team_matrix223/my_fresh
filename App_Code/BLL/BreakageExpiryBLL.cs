﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
/// <summary>
/// Summary description for AttributesBLL
/// </summary>
/// 



public class ProductInGodown
{
    public decimal MRP { get; set; }
    public decimal SaleRate { get; set; }
    public string Pid { get; set; }
    public string BarCode { get; set; }
    public int MrpCount { get; set; }
    public int sQty { get; set; }
    public string ProductName { get; set; }

}
public class BreakageExpiryBLL
{

    public List<ProductInGodown> GetProductListByGodownId(int godownId,int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<ProductInGodown> ProdInGodown = new List<ProductInGodown>();
        SqlDataReader dr = null;
        try
        {
            dr = new BreakageExpiryDAL().GetByGodown(godownId,BranchId);

            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    ProductInGodown objPIG = new ProductInGodown()
                    {
                        BarCode = dr["Bar_Code"].ToString(),
                        MRP = Convert.ToDecimal(dr["StockMRP"]),
                        ProductName = dr["Item_Name"].ToString(),
                        MrpCount = Convert.ToInt32(dr["MrpCount"]),
                        Pid = Convert.ToString(dr["Item_Code"]),
                        sQty = Convert.ToInt32(dr["Qty"]),
                        SaleRate = Convert.ToDecimal(dr["Sale_Rate"]),


                    };
                    ProdInGodown.Add(objPIG);
                    // strBuilder.Append(string.Format("<option value='{0}' mrp='{2}' mrpcount='{6}' sqty='{5}' srate='{4}' pid='{3}'>{1}</option>", dr["BarCode"].ToString(), dr["Product_Name"].ToString(), dr["StockMRP"].ToString(), dr["Product_ID"].ToString(), dr["SPrice"].ToString(), dr["Qty"].ToString(), dr["MrpCount"].ToString()));

                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return ProdInGodown;

    }


    public List<StockExpiry> GetAll()
    {
        List<StockExpiry> breakageList = new List<StockExpiry>();
        SqlDataReader dr = new BreakageExpiryDAL().GetAll();


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    StockExpiry objBreakage = new StockExpiry()
                    {
                        Ref_No = Convert.ToInt32(dr["Ref_No"]),
                        Ref_Date = Convert.ToDateTime(dr["Ref_Date"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_Id"]),
                        Godown = Convert.ToString(dr["Godown"])
             



                    };
                    breakageList.Add(objBreakage);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return breakageList;

    }



    public List<StockExpiry> GetBreakageByDate(DateTime FromDate, DateTime ToDate,int BranchId)
    {
        List<StockExpiry> breakageList = new List<StockExpiry>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new BreakageExpiryDAL().GetByDate(FromDate, ToDate,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    StockExpiry objBreakage = new StockExpiry()
                    {
                        Ref_No = Convert.ToInt32(dr["Ref_No"]),
                        Ref_Date = Convert.ToDateTime(dr["Ref_Date"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_Id"]),
                        Godown = Convert.ToString(dr["Godown"])




                    };
                    breakageList.Add(objBreakage);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return breakageList;

    }




    public string GetBreakageExpiryDetails(StockExpiry objBreakageExpiry, out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = new BreakageExpiryDAL().GetBreakageExpiryDetails(objBreakageExpiry.Ref_No);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 0;

        try
        {

            if (dr.HasRows)
            {
                dr.Read();

                objBreakageExpiry.Ref_No = Convert.ToInt32(dr["Ref_No"]);
                objBreakageExpiry.Ref_Date = Convert.ToDateTime(dr["Ref_Date"]);
                objBreakageExpiry.Godown_ID = Convert.ToInt32(dr["Godown_ID"]);
                

            }

            dr.NextResult();

            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    counterId += 1;

                    string searchIcon = "";
                    if (Convert.ToInt32(dr["MrpCount"]) > 1)
                    {
                        searchIcon = "<div style='cursor:pointer' onclick='javascript:showQty(" + dr["Item_Code"].ToString() + "," + counterId + ")'><i class='glyphicon glyphicon-search'></i><div>";

                    }

                        strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");'    onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' style='width:70px'/></td>" +
                  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts'  counter='" + counterId + "'  style='height:30px;width:140px'>" + "<option sqty='" + dr["StockInHand"].ToString() + "' mrpcount='" + dr["MrpCount"].ToString() + "' pid='" + dr["Item_Code"].ToString() + "' value='" + dr["Item_Code"].ToString() + "' >" + dr["Item_Name"].ToString() + "</option>" + "</select></td>" +
                 //"<td><input type='text' id='txtStockQty" + counterId + "' readonly=readonly   counter='" + counterId + "' class='form-control input-small' name='txtStockQty' style='width:60px'  value='" + dr["StockInHand"].ToString() + "'/></td>" +

                  "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small' name='txtQty' style='width:60px'   value='" + dr["StockExpired"].ToString() + "' /></td>" +
                   "<td><div style='float:left'><input type='text' id='txtMRP" + counterId + "'   readonly=readonly counter='" + counterId + "'  class='form-control input-small' name='txtMRP' style='width:55px'   value='" + dr["Mrp"].ToString() + "' /></div><div name='dvQty' style='float:left;width:10px;padding-top:6px;padding-left:3px;display:none'>" + searchIcon + "</div></td>" +
              
                   "<td><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
                "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");


                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }



    public int RollBack(int BreakageId)
    {

        return new BreakageExpiryDAL().RollBack(BreakageId);
    }

    public int Insert(StockExpiry objBreakage, DataTable dt)
    {

        return new BreakageExpiryDAL().Insert(objBreakage, dt);
    }

    public int Update(StockExpiry objBreakage, DataTable dt)
    {

        return new BreakageExpiryDAL().Update(objBreakage, dt);
    }
}