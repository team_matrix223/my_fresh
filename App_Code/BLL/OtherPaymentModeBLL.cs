﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
/// <summary>
/// Summary description for OtherPaymentModeBLL
/// </summary>
public class OtherPaymentModeBLL
{
    public Int32 DeleteOtherPaymentMode(OtherPaymentMode objOtherPayment)
    {
        return new OtherPaymentModeDAL().Delete(objOtherPayment);
    }
    public void GetById(OtherPaymentMode objOtherPayment)
    {

        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentModeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    objOtherPayment.OtherPayment_Name = dr["PaymentModeID"].ToString();
                    objOtherPayment.OtherPayment_Name = dr["OtherPayment_Name"].ToString();
                    objOtherPayment.OtherPayment_ID = Convert.ToInt16(dr["OtherPayment_ID"]);
                    objOtherPayment.UserId = Convert.ToInt16(dr["UserId"]);
                    objOtherPayment.IsActive = Convert.ToBoolean(dr["IsActive"]);

                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }


    }

    public List<OtherPaymentMode> GetAll()
    {
        List<OtherPaymentMode> OtherPaymentModeList = new List<OtherPaymentMode>();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentModeDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    OtherPaymentMode objOtherPayment = new OtherPaymentMode()
                    {
                        OtherPayment_Name = dr["OtherPayment_Name"].ToString(),
                        PaymetMode = Convert.ToInt32(dr["PaymentModeID"]),
                        OtherPayment_ID = Convert.ToInt16(dr["PaymentModeID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                    };
                    OtherPaymentModeList.Add(objOtherPayment);
                }
            }

        }

        finally
        {
            objParam = null;
            dr.Close();
            dr.Dispose();
        }
        return OtherPaymentModeList;

    }

    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new OtherPaymentModeDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["OtherPayment_ID"].ToString(), dr["OtherPayment_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public Int16 InsertUpdate(OtherPaymentMode objOtherPayment)
    {

        return new OtherPaymentModeDAL().InsertUpdate(objOtherPayment);
    }
}