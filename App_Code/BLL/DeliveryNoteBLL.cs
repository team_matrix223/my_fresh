﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for DeliveryNoteBLL
/// </summary>
public class DeliveryNoteBLL
{
    public List<AccLedger> GetAllDealer()
    {
        List<AccLedger> DealerList = new List<AccLedger>();

        SqlDataReader dr = null;
        try
        {
            dr = new DeliveryNoteDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccLedger objCity = new AccLedger()
                    {
                        CCODE = dr["CCODE"].ToString(),
                        CNAME = dr["CNAME"].ToString(),
                        DIS_PER = Convert.ToDecimal(dr["DIS_PER"]),
                      
                    };
                    DealerList.Add(objCity);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DealerList;

    }


    public Int32 DeleteDeliveryNote(DeliveryMaster objDelivery)
    {
        return new DeliveryNoteDAL().Delete(objDelivery);
    }



    public int Insert(DeliveryMaster objDelivery, DataTable dt,bool Excise)
    {

        return new DeliveryNoteDAL().Insert(objDelivery, dt,Excise);
    }

    public int ReceiveDelivery(string BillNo, int BranchId, int GodownId)
    {

        return new DeliveryNoteDAL().ReceiveDelivery(BillNo, BranchId, GodownId);
    }


    public List<DeliveryMaster> GetAll(DateTime DateFrom,DateTime DateTo,Int32 BranchId)
    {
        List<DeliveryMaster> DeliveryList = new List<DeliveryMaster>();

        SqlDataReader dr = null;
        try
        {
            dr = new DeliveryNoteDAL().GetAllNote(DateFrom, DateTo, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DeliveryMaster objDelivery = new DeliveryMaster()
                    {
                        Bill_No = Convert.ToInt32(dr["Bill_No"]),
                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"]),
                        GR_No = Convert.ToString(dr["GR_No"]),
                        GR_Date = Convert.ToDateTime(dr["GR_Date"]),
                        Veh_No = Convert.ToString(dr["Veh_No"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        LocalOut = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        Bill_Value = Convert.ToInt32(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToInt32(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToInt32(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        Total_Amount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        Display_Amount = Convert.ToDecimal(dr["Display_Amount"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        CCODE = Convert.ToString(dr["CCODE"]),
                        Godown_ID = Convert.ToInt16(dr["Godown_ID"]),
                        Form_Name = Convert.ToString(dr["Form_Name"]),
                        Desp_Date = Convert.ToDateTime(dr["Desp_Date"]),
                        Ref_No = Convert.ToString(dr["Ref_No"]),
                        UpdateMaster = Convert.ToBoolean(dr["UpdateMaster"]),
                        PASSING = Convert.ToBoolean(dr["PASSING"]),
                        ChkRaw = Convert.ToString(dr["ChkRaw"]),
                        dr_grno = Convert.ToString(dr["dr_grno"]),
                        delchk = Convert.ToBoolean(dr["delchk"]),
                        Ctime = Convert.ToDateTime(dr["Ctime"]),
                        Mtime = Convert.ToDateTime(dr["Mtime"]),
                        Prefix = Convert.ToString(dr["Prefix"]),
                        Excise_Amt = Convert.ToDecimal(dr["Excise_Amt"]),
                        EduCess = Convert.ToDecimal(dr["EduCess"]),
                        HSCess = Convert.ToDecimal(dr["HSCess"]),
                        BillMode = Convert.ToString(dr["BillMode"]),
                        Dealer = Convert.ToString(dr["Dealer"]),
                        BillPrefix = Convert.ToString(dr["BillPrefix"]),
                        ForBranch = Convert.ToInt32(dr["ForBranch"]),
                        BranchName = Convert.ToString(dr["BranchName"]),


                    };
                    DeliveryList.Add(objDelivery);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DeliveryList;

    }




    public List<DeliveryMaster> GetAllReceived(DateTime DateFrom, DateTime DateTo, Int32 BranchId)
    {
        List<DeliveryMaster> DeliveryList = new List<DeliveryMaster>();

        SqlDataReader dr = null;
        try
        {
            dr = new DeliveryNoteDAL().GetAllReceivedNote(DateFrom,DateTo,BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DeliveryMaster objDelivery = new DeliveryMaster()
                    {
                        ReceivedBillNO = dr["Bill_No"].ToString(),
                        Bill_Date = Convert.ToDateTime(dr["Bill_Date"]),
                        GR_No = Convert.ToString(dr["GR_No"]),
                        GR_Date = Convert.ToDateTime(dr["GR_Date"]),
                        Veh_No = Convert.ToString(dr["Veh_No"]),
                        Dis1InRs = Convert.ToBoolean(dr["Dis1InRs"]),
                        Dis2InRs = Convert.ToBoolean(dr["Dis2InRs"]),
                        Dis2AftDedDis1 = Convert.ToBoolean(dr["Dis2AftDedDis1"]),
                        LocalOut = Convert.ToString(dr["LocalOut"]),
                        TaxAfterDis1 = Convert.ToBoolean(dr["TaxAfterDis1"]),
                        TaxAfterDis2 = Convert.ToBoolean(dr["TaxAfterDis2"]),
                        Remarks = Convert.ToString(dr["Remarks"]),
                        Bill_Value = Convert.ToInt32(dr["Bill_Value"]),
                        Dis1Amt = Convert.ToInt32(dr["Dis1Amt"]),
                        Dis2Amt = Convert.ToInt32(dr["Dis2Amt"]),
                        Dis3AftDis1PDis2 = Convert.ToBoolean(dr["Dis3AftDis1PDis2"]),
                        Dis3P = Convert.ToDecimal(dr["Dis3P"]),
                        Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]),
                        TaxP = Convert.ToDecimal(dr["TaxP"]),
                        TaxAmt = Convert.ToDecimal(dr["TaxAmt"]),
                        Total_Amount = Convert.ToDecimal(dr["Total_Amount"]),
                        ODisP = Convert.ToDecimal(dr["ODisP"]),
                        ODisAmt = Convert.ToDecimal(dr["ODisAmt"]),
                        Display_Amount = Convert.ToDecimal(dr["Display_Amount"]),
                        Adjustment = Convert.ToDecimal(dr["Adjustment"]),
                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                        PASSING = Convert.ToBoolean(dr["PASSING"]),
                        CCODE = Convert.ToString(dr["Supplier_ID"]),
                        Godown_ID = Convert.ToInt32(dr["Godown_ID"]),
                        ChkRaw = Convert.ToString(dr["ChkRaw"]),
                        delchk = Convert.ToBoolean(dr["delchk"]),
                        Excise_Amt = Convert.ToDecimal(dr["Excise_Amt"]),
                        Dealer = Convert.ToString(dr["Dealer"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        FromBranch = Convert.ToString(dr["FromBranch"]),

                    };
                    DeliveryList.Add(objDelivery);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DeliveryList;

    }


    public List<DeliveryDetail> GetDeliveryById(DeliveryMaster objDelivery)
    {

        SqlDataReader dr = null;
        List<DeliveryDetail> objDetail = new List<DeliveryDetail>();
        try
        {
            dr = new DeliveryNoteDAL().GetByID(objDelivery.Bill_No,objDelivery.Prefix);



            if (dr.HasRows)
            {
                dr.Read();

                        objDelivery.Bill_No = Convert.ToInt32(dr["Bill_No"].ToString());
                        objDelivery.Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString());
                        objDelivery.GR_No = Convert.ToString(dr["GR_No"].ToString());
                        objDelivery.GR_Date = Convert.ToDateTime(dr["GR_Date"]);
                        objDelivery.Veh_No = Convert.ToString(dr["Veh_No"]);
                        objDelivery.Bill_Value = Convert.ToDecimal(dr["Bill_Value"]);
                        objDelivery.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                        objDelivery.Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]);
                        objDelivery.TaxP = Convert.ToDecimal(dr["TaxP"]);
                        objDelivery.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                        objDelivery.Total_Amount = Convert.ToDecimal(dr["Total_Amount"]);
                        objDelivery.ODisP = Convert.ToDecimal(dr["ODisP"]);
                        objDelivery.ODisAmt = Convert.ToDecimal(dr["ODisAmt"]);
                        objDelivery.Display_Amount = Convert.ToDecimal(dr["Display_Amount"]);
                        objDelivery.Adjustment = Convert.ToDecimal(dr["Adjustment"]);
                        objDelivery.Net_Amount = Convert.ToDecimal(dr["Net_Amount"]);
                        objDelivery.CCODE = Convert.ToString(dr["CCODE"]);
                        objDelivery.Godown_ID = Convert.ToInt32(dr["Godown_ID"]);
                        objDelivery.Form_Name = Convert.ToString(dr["Form_Name"]);
                        objDelivery.Desp_Date = Convert.ToDateTime(dr["Desp_Date"]);
                        objDelivery.Ref_No = Convert.ToString(dr["Ref_No"]);
                        objDelivery.UpdateMaster = Convert.ToBoolean(dr["UpdateMaster"]);
                        objDelivery.PASSING = Convert.ToBoolean(dr["PASSING"]);
                        objDelivery.ChkRaw = Convert.ToString(dr["ChkRaw"]);
                        objDelivery.dr_grno = Convert.ToString(dr["dr_grno"]);
                        objDelivery.delchk = Convert.ToBoolean(dr["delchk"]);
                        objDelivery.Ctime = Convert.ToDateTime(dr["Ctime"]);
                        objDelivery.Mtime = Convert.ToDateTime(dr["Mtime"]);
                        objDelivery.Prefix = Convert.ToString(dr["Prefix"]);
                        objDelivery.Excise_Amt = Convert.ToDecimal(dr["Excise_Amt"]);
                        objDelivery.EduCess = Convert.ToDecimal(dr["EduCess"]);
                        objDelivery.HSCess = Convert.ToDecimal(dr["HSCess"]);
                        objDelivery.BillMode = Convert.ToString(dr["BillMode"]);
                        objDelivery.Dealer = Convert.ToString(dr["Dealer"]);
                        objDelivery.ForBranch = Convert.ToInt32(dr["ForBranch"]);
                        objDelivery.Type = Convert.ToString(dr["Type"]);
                        objDelivery.BranchId = Convert.ToInt32(dr["BranchId"]);
                       

            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DeliveryDetail objKD = new DeliveryDetail();


                    
                    objKD.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objKD.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objKD.Qty = Convert.ToDecimal(dr["Qty"]);
                    objKD.MRP = Convert.ToDecimal(dr["MRP"]);
                    objKD.Rate = Convert.ToDecimal(dr["Rate"]);
                    objKD.Amount = Convert.ToDecimal(dr["Amount"]);
                    objKD.CaseQty = Convert.ToDecimal(dr["CaseQty"]);
                    objKD.Qty_In_Case = Convert.ToDecimal(dr["CaseQty"]);
                    objKD.QtyInCase = Convert.ToDecimal(dr["QtyInCase"]);
                    objKD.QTY_TO_LESS = Convert.ToDecimal(dr["QTY_TO_LESS"]);
                    objKD.Excise_duty = Convert.ToDecimal(dr["Excise_duty"]);
                    objKD.Excise = Convert.ToDecimal(dr["Excise_duty"]);
                    objKD.Sale_Rate = Convert.ToDecimal(dr["Sale_Rate"]);
                    objKD.Abatement = Convert.ToInt32(dr["Abatement"]);
                    if (dr["Stock"] == null)
                    {
                        objKD.Stock = 0;
                    }
                    else
                    {
                        objKD.Stock = Convert.ToDecimal(dr["Stock"]);
                    }

                    objKD.TaxP = Convert.ToDecimal(dr["TaxP"]);
                    objKD.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                    objKD.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                   // objKD.Bill_Date = Convert.ToDateTime(dr["Bill_Date"]);
                   
                    objDetail.Add(objKD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }



    public List<DeliveryDetail> GetDeliveryByIdByBranch(DeliveryMaster objDelivery,int Branch)
    {

        SqlDataReader dr = null;
        List<DeliveryDetail> objDetail = new List<DeliveryDetail>();
        try
        {
            dr = new DeliveryNoteDAL().GetByIDByBranch(objDelivery.Bill_No, objDelivery.Prefix, Branch);



            if (dr.HasRows)
            {
                dr.Read();

                objDelivery.Bill_No = Convert.ToInt32(dr["Bill_No"].ToString());
                objDelivery.Bill_Date = Convert.ToDateTime(dr["Bill_Date"].ToString());
                objDelivery.GR_No = Convert.ToString(dr["GR_No"].ToString());
                objDelivery.GR_Date = Convert.ToDateTime(dr["GR_Date"]);
                objDelivery.Veh_No = Convert.ToString(dr["Veh_No"]);
                objDelivery.Bill_Value = Convert.ToDecimal(dr["Bill_Value"]);
                objDelivery.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                objDelivery.Dis3Amt = Convert.ToDecimal(dr["Dis3Amt"]);
                objDelivery.TaxP = Convert.ToDecimal(dr["TaxP"]);
                objDelivery.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                objDelivery.Total_Amount = Convert.ToDecimal(dr["Total_Amount"]);
                objDelivery.ODisP = Convert.ToDecimal(dr["ODisP"]);
                objDelivery.ODisAmt = Convert.ToDecimal(dr["ODisAmt"]);
                objDelivery.Display_Amount = Convert.ToDecimal(dr["Display_Amount"]);
                objDelivery.Adjustment = Convert.ToDecimal(dr["Adjustment"]);
                objDelivery.Net_Amount = Convert.ToDecimal(dr["Net_Amount"]);
                objDelivery.CCODE = Convert.ToString(dr["CCODE"]);
                objDelivery.Godown_ID = Convert.ToInt32(dr["Godown_ID"]);
                objDelivery.Form_Name = Convert.ToString(dr["Form_Name"]);
                objDelivery.Desp_Date = Convert.ToDateTime(dr["Desp_Date"]);
                objDelivery.Ref_No = Convert.ToString(dr["Ref_No"]);
                objDelivery.UpdateMaster = Convert.ToBoolean(dr["UpdateMaster"]);
                objDelivery.PASSING = Convert.ToBoolean(dr["PASSING"]);
                objDelivery.ChkRaw = Convert.ToString(dr["ChkRaw"]);
                objDelivery.dr_grno = Convert.ToString(dr["dr_grno"]);
                objDelivery.delchk = Convert.ToBoolean(dr["delchk"]);
                objDelivery.Ctime = Convert.ToDateTime(dr["Ctime"]);
                objDelivery.Mtime = Convert.ToDateTime(dr["Mtime"]);
                objDelivery.Prefix = Convert.ToString(dr["Prefix"]);
                objDelivery.Excise_Amt = Convert.ToDecimal(dr["Excise_Amt"]);
                objDelivery.EduCess = Convert.ToDecimal(dr["EduCess"]);
                objDelivery.HSCess = Convert.ToDecimal(dr["HSCess"]);
                objDelivery.BillMode = Convert.ToString(dr["BillMode"]);
                objDelivery.Dealer = Convert.ToString(dr["Dealer"]);
                objDelivery.ForBranch = Convert.ToInt32(dr["ForBranch"]);

            }

            dr.NextResult();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DeliveryDetail objKD = new DeliveryDetail();



                    objKD.Item_Code = Convert.ToString(dr["Item_Code"].ToString());
                    objKD.Item_Name = Convert.ToString(dr["Item_Name"]);
                    objKD.Qty = Convert.ToDecimal(dr["Qty"]);
                    objKD.MRP = Convert.ToDecimal(dr["MRP"]);
                    objKD.Rate = Convert.ToDecimal(dr["Rate"]);
                    objKD.Amount = Convert.ToDecimal(dr["Amount"]);
                    objKD.CaseQty = Convert.ToDecimal(dr["CaseQty"]);
                    objKD.Qty_In_Case = Convert.ToDecimal(dr["CaseQty"]);
                    objKD.QtyInCase = Convert.ToDecimal(dr["QtyInCase"]);
                    objKD.QTY_TO_LESS = Convert.ToDecimal(dr["QTY_TO_LESS"]);
                    objKD.Excise = Convert.ToDecimal(dr["Excise_duty"]);
                    objKD.Excise_duty = Convert.ToDecimal(dr["Excise_duty"]);
                    if (dr["Stock"] == null)
                    {
                        objKD.Stock = 0;
                    }
                    else
                    {
                        objKD.Stock = Convert.ToDecimal(dr["Stock"]);
                    }

                    objKD.TaxP = Convert.ToDecimal(dr["TaxP"]);
                    objKD.Dis3P = Convert.ToDecimal(dr["Dis3P"]);
                    objKD.TaxAmt = Convert.ToDecimal(dr["TaxAmt"]);
                    // objKD.Bill_Date = Convert.ToDateTime(dr["Bill_Date"]);

                    objDetail.Add(objKD);


                }

            }
            return objDetail;
        }

        finally
        {

            dr.Close();
            dr.Dispose();

        }


    }

    public List<DeliveryMaster> GetNonExciseByDate(DateTime FromDate, DateTime ToDate, int BranchId)
    {
        List<DeliveryMaster> NonExciseList = new List<DeliveryMaster>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new DeliveryNoteDAL().GetNonExciseByDate(FromDate, ToDate, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    DeliveryMaster objdelivery = new DeliveryMaster()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),


                        Net_Amount = Convert.ToDecimal(dr["Net_Amount"]),
                     
                    };


                    NonExciseList.Add(objdelivery);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return NonExciseList;

    }
}