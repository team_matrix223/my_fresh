﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;

/// <summary>
/// Summary description for AccGroupsBLL
/// </summary>
public class VoucherBLL
{
    public string GetVoucherDetail(int VouchNo,string VType, int BranchId, out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<AccTrans> VoucherList = new List<AccTrans>();
        SqlDataReader dr = new VoucherDAL().GetVoucherDetails(VouchNo,VType,BranchId);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 1;

        try
        {

            if (dr.HasRows)
            {
                if (VType == "CV")
                {

                    while (dr.Read())
                    {
                        counterId += 1;


                        strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Code"].ToString() + "' /></td>" +
                  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");' disabled='disabled'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + "<option value='" + dr["Code"].ToString() + "' >" + dr["Description"].ToString() + "</option>" + "</select></td>" +
                  "<td><input type='text' id='txtBalance" + counterId + "'  counter='" + counterId + "'  class='form-control input-small' name='txtBalance'  value = '0'/></td>" +
                  "<td><input type='text' id='txtPayment" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtPayment'  value = '" + dr["Payment"].ToString() + "'/></td>" +
                  "<td><input type='text' id='txtReceipt" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtReceipt'  value ='" + dr["Receipt"].ToString() + "'/></td>" +
                 "<td style='display:none'><input type='text' id='txtChqNo" + counterId + "'        counter='" + counterId + "'  class='form-control input-small' name='txtChqNo'    value=' ' /></td>" +
                 "<td style='display:none'><input type='text' id='txtDebit" + counterId + "'       counter='" + counterId + "'  class='form-control input-small' name='txtDebit'    value='0' /></td>" +
                  "<td style='display:none'><input type='text' id='txtCredit" + counterId + "'      counter='" + counterId + "'  class='form-control input-small' name='txtCredit'    value='0' /></td>" +

                 "<td><input type='text' id='txtNarr" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtNarr'    value='" + dr["Narration"].ToString() + " ' /></td>" +
                 "<td style='display:none'><input type='text' id='txtCCOde" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtCCOde'    value='" + dr["CCode"].ToString() + "' /></td>" +


                "<td style='display:none'><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
                "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");



                    }
                    cntr = counterId;
                }
                if(VType == "BV")
                {
                    while (dr.Read())
                    {
                        counterId += 1;


                        strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Code"].ToString() + "' /></td>" +
                  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");' disabled='disabled'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + "<option value='" + dr["Code"].ToString() + "' >" + dr["Description"].ToString() + "</option>" + "</select></td>" +
                  "<td><input type='text' id='txtBalance" + counterId + "'  counter='" + counterId + "'  class='form-control input-small' name='txtBalance'  value = '0'/></td>" +
                  "<td><input type='text' id='txtPayment" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtPayment'  value = '" + dr["Payment"].ToString() + "'/></td>" +
                  "<td><input type='text' id='txtReceipt" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtReceipt'  value ='" + dr["Receipt"].ToString() + "'/></td>" +
                 "<td><input type='text' id='txtChqNo" + counterId + "'        counter='" + counterId + "'  class='form-control input-small' name='txtChqNo'    value=' " + dr["ChequeNo"].ToString() + "' /></td>" +
                 "<td style='display:none'><input type='text' id='txtDebit" + counterId + "'       counter='" + counterId + "'  class='form-control input-small' name='txtDebit'    value='0' /></td>" +
                  "<td style='display:none'><input type='text' id='txtCredit" + counterId + "'      counter='" + counterId + "'  class='form-control input-small' name='txtCredit'    value='0' /></td>" +

                 "<td><input type='text' id='txtNarr" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtNarr'    value='" + dr["Narration"].ToString() + " ' /></td>" +
                 "<td style='display:none'><input type='text' id='txtCCOde" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtCCOde'    value='" + dr["CCode"].ToString() + "' /></td>" +


                "<td style='display:none'><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
                "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");



                    }
                    cntr = counterId;

                }

                if (VType == "JV")
                {
                    while (dr.Read())
                    {
                        counterId += 1;


                        strBuilder.Append("<tr><td><input type='text' onmouseover='javascript:ServiceClick(" + counterId + ");' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId'  value='" + dr["Code"].ToString() + "' /></td>" +
                  "<td><select id='ddlProducts" + counterId + "' onmouseover='javascript:ServiceClick(" + counterId + ");' disabled='disabled'    onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + "<option value='" + dr["Code"].ToString() + "' >" + dr["Description"].ToString() + "</option>" + "</select></td>" +
                  "<td><input type='text' id='txtBalance" + counterId + "'  counter='" + counterId + "'  class='form-control input-small' name='txtBalance'  value = '0'/></td>" +
                  "<td style='display:none'><input type='text' id='txtPayment" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtPayment'  value = '0'/></td>" +
                  "<td style='display:none'><input type='text' id='txtReceipt" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtReceipt'  value ='0'/></td>" +
                 "<td style='display:none'><input type='text' id='txtChqNo" + counterId + "'        counter='" + counterId + "'  class='form-control input-small' name='txtChqNo'    value=' ' /></td>" +
                 "<td ><input type='text' id='txtDebit" + counterId + "'       counter='" + counterId + "'  class='form-control input-small' name='txtDebit'    value='" + dr["Debit"].ToString() + "' /></td>" +
                  "<td ><input type='text' id='txtCredit" + counterId + "'      counter='" + counterId + "'  class='form-control input-small' name='txtCredit'    value='" + dr["Credit"].ToString() + "' /></td>" +

                 "<td><input type='text' id='txtNarr" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtNarr'    value='" + dr["Narration"].ToString() + " ' /></td>" +
                 "<td style='display:none'><input type='text' id='txtCCOde" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtCCOde'    value='" + dr["CCode"].ToString() + "' /></td>" +


                "<td style='display:none'><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus'></i></div> </td>" +
                "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");



                    }
                    cntr = counterId;

                }



            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }

    public void GetGridAccountsByBank(DropDownList ddl, int BranchId,string Bank)
    {
        StringBuilder strBuilder = new StringBuilder();

        DataSet ds = null;
        try
        {
            ds = new VoucherDAL().GetAllAccountsByBank(BranchId,Bank);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ListItem li2 = new ListItem();
                li2.Text = "";
                li2.Value = "";
                ddl.Items.Add(li2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ListItem li = new ListItem();
                    li.Value = ds.Tables[0].Rows[i]["CCODE"].ToString();
                    li.Text = ds.Tables[0].Rows[i]["CNAME"].ToString();


                    ddl.Items.Add(li);
                    //strBuilder.Append(string.Format("<option value={0} cost={2} tax={5} mrp={4} pid={3} srate={6}>{1}</option>", ds.Tables[0].Rows[i]["BarCode"].ToString(), ds.Tables[0].Rows[i]["Product_Name"].ToString(), ds.Tables[0].Rows[i]["SPrice"].ToString(), ds.Tables[0].Rows[i]["Product_ID"].ToString(), ds.Tables[0].Rows[i]["MRP"].ToString(), ds.Tables[0].Rows[i]["Tax_Rate"].ToString(), ds.Tables[0].Rows[i]["SPrice"].ToString()));

                }
            }

        }

        finally
        {

        }
        //return strBuilder.ToString();

    }
    public void GetGridAccounts(DropDownList ddl, int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();

        DataSet ds = null;
        try
        {
            ds = new VoucherDAL().GetAllAccounts(BranchId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ListItem li2 = new ListItem();
                li2.Text = "";
                li2.Value = "";
                ddl.Items.Add(li2);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    ListItem li = new ListItem();
                    li.Value = ds.Tables[0].Rows[i]["CCODE"].ToString();
                    li.Text = ds.Tables[0].Rows[i]["CNAME"].ToString();


                    ddl.Items.Add(li);
                    //strBuilder.Append(string.Format("<option value={0} cost={2} tax={5} mrp={4} pid={3} srate={6}>{1}</option>", ds.Tables[0].Rows[i]["BarCode"].ToString(), ds.Tables[0].Rows[i]["Product_Name"].ToString(), ds.Tables[0].Rows[i]["SPrice"].ToString(), ds.Tables[0].Rows[i]["Product_ID"].ToString(), ds.Tables[0].Rows[i]["MRP"].ToString(), ds.Tables[0].Rows[i]["Tax_Rate"].ToString(), ds.Tables[0].Rows[i]["SPrice"].ToString()));

                }
            }

        }

        finally
        {

        }
        //return strBuilder.ToString();

    }

    public List<AccLedger> GetAccountbybnksetting(Int32 BranchId)
    {
        List<AccLedger> AcountList = new List<AccLedger>();
        SqlDataReader dr = new VoucherDAL().GetAccountByBnkSeting(BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    AccLedger objAcount = new AccLedger()
                    {
                        CCODE = Convert.ToString(dr["CCODE"]),
                        CNAME = Convert.ToString(dr["CNAME"])




                    };
                    AcountList.Add(objAcount);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return AcountList;

    }





    public List<AccTrans> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<AccTrans> VoucherList = new List<AccTrans>();
        SqlDataReader dr = new VoucherDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    AccTrans objVoucher = new AccTrans()
                    {
                        VOUCH_NO = Convert.ToString(dr["Vouch_No"]),
                        TR_DATE = Convert.ToDateTime(dr["TR_Date"]),
                        CCODE = Convert.ToString(dr["CCODE"]),
                        CNAME = Convert.ToString(dr["CNAME"]),
                        Debit = Convert.ToDecimal(dr["Debit"]),
                        Credit = Convert.ToDecimal(dr["Credit"]),
                        VTYPE = Convert.ToString(dr["VType"]),
                    };
                    VoucherList.Add(objVoucher);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return VoucherList;

    }


    public int InsertUpdate(AccTrans objTrans, DataTable dt,string BankId,string multisingle)
    {

        return new VoucherDAL().InsertUpdate(objTrans, dt,BankId,multisingle);
    }

}