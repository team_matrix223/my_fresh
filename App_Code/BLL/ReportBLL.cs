﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ReportBLL
/// </summary>
public class ReportBLL
{
    public string GetCashMemoHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetAllCashMemoOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                   
                        html += "<tr><td><input class='cls_chk'  "+ dr["HideShow"] + "    type='checkbox' value='" + dr["Id"] + "' name='CashMemo' id='chkS_" + dr["GridColumn"] + "'/> <label for='chkS_" + dr["Id"] + "'>" + dr["GridColumn"] + "</label><input type='text' value = '" + dr["Width"] + "' class='cls_width' style='width: 50px;' name='cashmemowidth' id='chkS_" + dr["Id"] + "'  /></td></tr>";

                }
            }
            else
            {
                html = "<tr><td>No Option Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }

    public string GetGSTHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetAllGSTOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    html += "<tr><td><input class='cls_chk'  " + dr["HideShow"] + "    type='checkbox' value='" + dr["Id"] + "' name='CashMemo' id='chkS_" + dr["GridColumn"] + "'/> <label for='chkS_" + dr["Id"] + "'>" + dr["GridColumn"] + "</label><input type='text' value = '" + dr["Width"] + "' class='cls_width' style='width: 50px;' name='cashmemowidth' id='chkS_" + dr["Id"] + "'  /></td></tr>";

                }
            }
            else
            {
                html = "<tr><td>No Option Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }
    public string GetCashMemoDatedHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetAllCashMemoDatedOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    html += "<tr><td><input class='cls_chk'  " + dr["HideShow"] + "    type='checkbox' value='" + dr["Id"] + "' name='CashMemo' id='chkS_" + dr["GridColumn"] + "'/> <label for='chkS_" + dr["Id"] + "'>" + dr["GridColumn"] + "</label><input type='text' value = '" + dr["Width"] + "' class='cls_width' style='width: 50px;' name='cashmemowidth' id='chkS_" + dr["Id"] + "'  /></td></tr>";



                }
            }
            else
            {
                html = "<tr><td>No OPtion Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }

    public Int16 InsertCashMemo(DataTable dt)
    {

        return new ReportDAL().InsertCashMemo(dt);
    }


    public Int16 InsertPurchase(DataTable dt)
    {

        return new ReportDAL().InsertPurchase(dt);
    }
    public Int16 InsertGSTOptions(DataTable dt)
    {

        return new ReportDAL().InsertGSTOptions(dt);
    }
    public Int16 InsertCashMemoDated(DataTable dt)
    {

        return new ReportDAL().InsertCashMemoDated(dt);
    }
    public List<GridOption> GetAllcashmemo()
    {
        List<GridOption> OPtionList = new List<GridOption>();

        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL ().GetAllCashMemoOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    GridOption objgridoption = new GridOption()
                    {
                        GridColumn = dr["GridColumn"].ToString(),
                        HideShow = dr["HideShow"].ToString(),
                        Id = Convert.ToInt16(dr["Id"]),
                       
                    };
                    OPtionList.Add(objgridoption);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return OPtionList;

    }

    public List<GridOption> GetAllcashmemoDated()
    {
        List<GridOption> OPtionList = new List<GridOption>();

        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetCashMemoDated();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    GridOption objgridoption = new GridOption()
                    {
                        GridColumn = dr["GridColumn"].ToString(),
                        Id = Convert.ToInt16(dr["Id"]),

                    };
                    OPtionList.Add(objgridoption);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return OPtionList;

    }

    public List<GridOption> GetGSTCheckedOptions()
    {
        List<GridOption> OPtionList = new List<GridOption>();

        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetAllGSTOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    GridOption objgridoption = new GridOption()
                    {
                        GridColumn = dr["GridColumn"].ToString(),
                        HideShow = dr["HideShow"].ToString(),
                        Id = Convert.ToInt16(dr["Id"]),

                    };
                    OPtionList.Add(objgridoption);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return OPtionList;

    }


    public string GetPurchaseHtml()
    {
        string html = "";
        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetPurchaseOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    html += "<tr><td><input class='cls_chk'  " + dr["HideShow"] + "    type='checkbox' value='" + dr["Id"] + "' name='CashMemo' id='chkS_" + dr["GridColumn"] + "'/> <label for='chkS_" + dr["Id"] + "'>" + dr["GridColumn"] + "</label><input type='text' value = '" + dr["Width"] + "' class='cls_width' style='width: 50px;' name='cashmemowidth' id='chkS_" + dr["Id"] + "'  /></td></tr>";

                }
            }
            else
            {
                html = "<tr><td>No Option Available</td></tr>";
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }

        return html;

    }

    public List<GridOption> GetPurchaseOptions()
    {
        List<GridOption> OPtionList = new List<GridOption>();

        SqlDataReader dr = null;
        try
        {
            dr = new ReportDAL().GetPurchaseOptions();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    GridOption objgridoption = new GridOption()
                    {
                        GridColumn = dr["GridColumn"].ToString(),
                        HideShow = dr["HideShow"].ToString(),
                        Id = Convert.ToInt16(dr["Id"]),

                    };
                    OPtionList.Add(objgridoption);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return OPtionList;

    }

    public Int16 InsertTempReport(string TypeName,int Id)
    {

        return new ReportDAL().InsertTempReport(TypeName,Id);
    }
    public Int16 DeleteTempReport(string TypeName)
    {

        return new ReportDAL().DeleteTempReport(TypeName);
    }
}