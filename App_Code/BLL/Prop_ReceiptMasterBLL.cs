﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

/// <summary>
/// Summary description for Prop_ReceiptMasterBLL
/// </summary>
public class Prop_ReceiptMasterBLL
{
    public Int32 InsertUpdate(Prop_ReceiptMaster objReceiptMaster)
    {

        return new Prop_ReceiptMasterDAL().InsertUpdate(objReceiptMaster);
    }

    public string GetOutstandingForReceiptMaster(string CustomerId,int BranchId)
    {
        string count = new Prop_ReceiptMasterDAL().GetOutstandingForReceiptMaster(CustomerId, BranchId);
        return count;
    }
    public List<Prop_ReceiptMaster> ReceiptMastersGetByDate(string StartDate, string EndDate, int BranchId)
    {
        List<Prop_ReceiptMaster> ReceiptMasterList = new List<Prop_ReceiptMaster>();
        SqlParameter[] objParam = new SqlParameter[0];
        DataSet ds = new DataSet();
        try
        {
            ds = new Prop_ReceiptMasterDAL().ReceiptMasterGetByDate(StartDate, EndDate, BranchId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Prop_ReceiptMaster objReceiptMaster = new Prop_ReceiptMaster()
                    {

                        Receipt_No = Convert.ToInt32(ds.Tables[0].Rows[i]["Receipt_No"]),
                        Receipt_Date = Convert.ToDateTime(ds.Tables[0].Rows[i]["Receipt_Date"]),
                        CCode = Convert.ToString(ds.Tables[0].Rows[i]["CCode"]),
                        CName = Convert.ToString(ds.Tables[0].Rows[i]["CName"]),
                        Amount = Convert.ToDecimal(ds.Tables[0].Rows[i]["Amount"]),
                        ModeOfPayment = Convert.ToString(ds.Tables[0].Rows[i]["ModeOfPayment"]),
                        Bank_Name = Convert.ToString(ds.Tables[0].Rows[i]["Bank_Name"]),
                        CHQNO = Convert.ToString(ds.Tables[0].Rows[i]["CHQNO"]),
                        userno = Convert.ToInt32(ds.Tables[0].Rows[i]["userno"]),
                        BranchId = Convert.ToInt32(ds.Tables[0].Rows[i]["BranchId"]),
                       
                    };
                    ReceiptMasterList.Add(objReceiptMaster);
                }
            }

        }

        finally
        {
            objParam = null;

        }
        return ReceiptMasterList;

    }
    public void GetByIdForReceiptMaster(Prop_ReceiptMaster objReceiptMaster)
    {
        new Prop_ReceiptMasterDAL().GetByIdForReceiptMaster(objReceiptMaster);

    }
}