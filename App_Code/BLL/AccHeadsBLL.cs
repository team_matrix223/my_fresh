﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for AccHeadsBLL
/// </summary>
public class AccHeadsBLL
{
    public List<AccHeads> GetAll()
    {
        List<AccHeads> AccHeadsList = new List<AccHeads>();

        SqlDataReader dr = null;
        try
        {
            dr = new AccHeadsDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    AccHeads objAccHeads = new AccHeads()
                    {
                        H_NAME = dr["H_NAME"].ToString(),
                        H_CODE = dr["H_CODE"].ToString(),
                        H_Id = Convert.ToInt16(dr["H_Id"]),
                        DR_CR = Convert.ToString(dr["DR_CR"]),
                        AMOUNT = Convert.ToDecimal(dr["AMOUNT"]),
                        BAL_INC = Convert.ToString(dr["BAL_INC"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    AccHeadsList.Add(objAccHeads);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return AccHeadsList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new AccHeadsDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["H_CODE"].ToString(), dr["H_NAME"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(AccHeads objAccHeads)
    {

        return new AccHeadsDAL().InsertUpdate(objAccHeads);
    }

    public Int32 DeleteAccHead(AccHeads objAccHead)
    {
        return new AccHeadsDAL().Delete(objAccHead);
    }
}