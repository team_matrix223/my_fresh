﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Data;

/// <summary>
/// Summary description for HSNBLL
/// </summary>
public class HSNBLL
{
    public List<HSNCodeEntities> GetHsnByGroup(DataTable dt)
    {
        List<HSNCodeEntities> HsnList = new List<HSNCodeEntities>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new HSNDAL().GetHsnByGroup(dt);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HSNCodeEntities objBill = new HSNCodeEntities()
                    {
                       
                        Title = Convert.ToString(dr["HSNCode"]),

                    };


                    HsnList.Add(objBill);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return HsnList;

    }

    public Int32 DeleteHSN(HSNCodeEntities objHSN)
    {
        return new HSNDAL().Delete(objHSN);
    }
    public void GetById(HSNCodeEntities objHSN)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new HSNDAL().GetById(objHSN);
            if (dr.HasRows)
            {
                dr.Read();


                objHSN.Title = dr["Title"].ToString();
                objHSN.Id = Convert.ToInt16(dr["Id"]);
                objHSN.UserId = Convert.ToInt16(dr["UserId"]);
                objHSN.IsActive = Convert.ToBoolean(dr["IsActive"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }


    public List<HSNCodeEntities> GetAll()
    {
        List<HSNCodeEntities> HSNList = new List<HSNCodeEntities>();

        SqlDataReader dr = null;
        try
        {
            dr = new HSNDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    HSNCodeEntities objHSN = new HSNCodeEntities()
                    {
                        Title = dr["Title"].ToString(),
                        Id = Convert.ToInt16(dr["Id"]),
                        UserId = Convert.ToInt16(dr["UserId"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                    };
                    HSNList.Add(objHSN);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return HSNList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new HSNDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["Id"].ToString(), dr["Title"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(HSNCodeEntities objHSN)
    {

        return new HSNDAL().InsertUpdate(objHSN);
    }

}