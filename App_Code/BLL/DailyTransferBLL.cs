﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for StockAdjustmentBLL
/// </summary>
public class DailyTransferBLL
{


    public List<DailyTransfer> GetByRefNo(int RefNo,int BranchId)
    {
        List<DailyTransfer> TransferList = new List<DailyTransfer>();
        SqlDataReader dr = new DailyTransferDAL().GetByRefNo(RefNo,BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DailyTransfer objDailyTransfer = new DailyTransfer()
                    {
                        RefNo = Convert.ToInt32(dr["RefNo"]),
                        Date = Convert.ToString(dr["Date"]),
                        Amount=Convert.ToDecimal(dr["Amount"]),
                        MRP = Convert.ToDecimal(dr["MRP"]),
                        Rate = Convert.ToDecimal(dr["Rate"]),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Stock = Convert.ToDecimal(dr["Stock"]),
                        Item_Code =dr["Item_Code"].ToString(),
                        Item_Name = dr["Item_Name"].ToString(),

                        
                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }


    public string GetTransferOptions(int RefNo, int BranchId, out int cntr)
    {
        StringBuilder strBuilder = new StringBuilder();
        List<DailyTransfer> PurchaseDetailList = new List<DailyTransfer>();
        SqlDataReader dr = new DailyTransferDAL().GetByRefNo(RefNo, BranchId);
        StringBuilder strProducts = new StringBuilder();
        int counterId = 1;

        try
        {

            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    counterId += 1;



                    strBuilder.Append("<tr><td><input type='text' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small cls_itemcode' name='txtServiceId'  value='" + dr["Item_Code"].ToString() + "' /></td>" +
              "<td><select id='ddlProducts" + counterId + "' class='cls_ddlproduct' disabled='disabled'  name='ddlProducts' >" + "<option pid='" + dr["Item_Code"].ToString() + "'  value='" + dr["Item_Code"].ToString() + "'>" + dr["Item_Name"].ToString() + "</option>" + "</select></td>" +
             
             "<td><input type='text' id='txtQty" + counterId + "' counter='" + counterId + "'  class='form-control input-small cls_qty' name='txtQty'    value='" + dr["Qty"].ToString() + "' /></td>" +
            
              "<td><input type='text' id='txtRate" + counterId + "' counter='" + counterId + "'  readonly=readonly   class='form-control input-small  validate' name='txtRate'    value='" + dr["Rate"].ToString() + "' /></td>" +

            
             "<td><input type='text' id='txtMRP" + counterId + "'  counter='" + counterId + "' readonly=readonly  class='form-control input-small  validate float' name='txtMRP'    value='" + dr["MRP"].ToString() + "' /></td>" +

            "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "'  readonly=readonly   class='form-control input-small' name='txtAmount'    value='" + dr["Amount"].ToString() + "' /></td>" +
           

            
            "<td ><div id='btnAddRow" + counterId + "'  style='cursor:pointer' counter='" + counterId + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>" +
            "<td><div id='btnRemove" + counterId + "' style='cursor:pointer' class='cls_remove'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>");






                }
                cntr = counterId;
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        cntr = counterId;
        return strBuilder.ToString();

    }










    public List<DailyTransfer> GetByDate(DateTime DateFrom, DateTime DateTo, int BranchId)
    {
        List<DailyTransfer> TransferList = new List<DailyTransfer>();
        SqlDataReader dr = new DailyTransferDAL().GetByDate(DateFrom, DateTo, BranchId);


        try
        {

            if (dr.HasRows)
            {
                while (dr.Read())
                {

                    DailyTransfer objDailyTransfer = new DailyTransfer()
                    {
                        RefNo = Convert.ToInt32(dr["RefNo"]),
                        Date = Convert.ToString(dr["Date"]),
                      Godown_ID=Convert.ToInt32(dr["Godown_ID"]),

                    };
                    TransferList.Add(objDailyTransfer);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        dr.Close();
        dr.Dispose();
        return TransferList;

    }


    public int Delete(int RefNo,int BranchId)
    {

        return new DailyTransferDAL().Delete(RefNo,BranchId);
    }

    public int InsertUpdate(DateTime Date, Int32 GodownId, int BranchId, int RefNo, DataTable dt, int UserNo)
    {

        return new DailyTransferDAL().InsertUpdate(Date,GodownId,BranchId,RefNo,dt,UserNo);
    }
}