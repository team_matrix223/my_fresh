﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

/// <summary>
/// Summary description for HappyHourBLL
/// </summary>
public class HappyHourBLL
{
    public Int32 DeleteHappyHour(HappyHours objHappyHour)
    {
        return new HappyHourDAL().Delete(objHappyHour);
    }


    public Int16 InsertUpdate(HappyHours objHappyHour,DataTable dt)
    {

        return new HappyHourDAL().InsertUpdate(objHappyHour,dt);
    }

    public void GetHappyHours(HappyHours objHappyHour)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new HappyHourDAL().GetHappyHours(objHappyHour);
            if (dr.HasRows)
            {
                dr.Read();


                objHappyHour.CategoryId = Convert.ToString(dr["CategoryId"]);
                objHappyHour.TimeFrom = Convert.ToString(dr["TimeFrom"]);
                objHappyHour.TimeTo = Convert.ToString(dr["TimeTo"]);
                objHappyHour.IsActive = Convert.ToBoolean(dr["IsActive"]);
                
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }
}