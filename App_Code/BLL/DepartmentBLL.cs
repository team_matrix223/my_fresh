﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;

/// <summary>
/// Summary description for DepartmentBLL
/// </summary>
public class DepartmentBLL
{

    public Int32 DeleteDepartment(Departments objDepartment)
    {
        return new DepartmentDAL().Delete(objDepartment);
    }

    public void GetById(Departments objDepartments)
    {

        SqlDataReader dr = null;
        try
        {
            dr = new DepartmentDAL().GetById(objDepartments);
            if (dr.HasRows)
            {
                dr.Read();


                objDepartments.Prop_Name = dr["Prop_Name"].ToString();
                objDepartments.Prop_ID = Convert.ToInt16(dr["Prop_ID"]);
                objDepartments.IsActive = Convert.ToBoolean(dr["IsActive"]);
                objDepartments.UserId = Convert.ToInt32(dr["UserId"]);
                objDepartments.showinmenu =Convert.ToBoolean( dr["showinmenu"]);

            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }






    public List<Departments> GetAll()
    {
        List<Departments> DepartmentsList = new List<Departments>();

        SqlDataReader dr = null;
        try
        {
            dr = new DepartmentDAL().GetAll();
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    Departments objDeaprtment = new Departments()
                    {
                        Prop_Name = dr["Prop_Name"].ToString(),

                        Prop_ID = Convert.ToInt16(dr["Prop_ID"]),
                        IsActive = Convert.ToBoolean(dr["IsActive"]),
                        UserId = Convert.ToInt32(dr["UserId"]),
                    };
                    DepartmentsList.Add(objDeaprtment);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return DepartmentsList;

    }





    public string GetOptions()
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlParameter[] objParam = new SqlParameter[0];
        SqlDataReader dr = null;
        try
        {
            dr = new DepartmentDAL().GetAll();
            if (dr.HasRows)
            {
                strBuilder.Append("<option></option>");
                while (dr.Read())
                {

                    strBuilder.Append(string.Format("<option value={0}>{1}</option>", dr["PROP_ID"].ToString(), dr["PROP_Name"].ToString()));
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
            objParam = null;
        }
        return strBuilder.ToString();

    }



    public Int16 InsertUpdate(Departments objDepartment)
    {

        return new DepartmentDAL().InsertUpdate(objDepartment);
    }

    public string GetProductByItemWise(int Dept,string ItemWise, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new DepartmentDAL().GetProductByItemWise(Dept,ItemWise);
        if (dr.HasRows)
        {


            str.Append("<tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Price</th><th>Tax</th><th>HSNCode</th><th>Department</th><th>Group</th><th></th></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td>{1}</td><td>{2}</td><td><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}' style='width:100px'/><td><input type='text' name='pprice' id='txtPrice_{5}' value='{4}' style='width:100px'/></td><td><input type='text' name='Tax' id='txttax_{5}' value='{6}' style='width:100px'/></td><td><input type='text' name='Tax' id='txthsncode_{5}' value='{7}' style='width:100px'/></td><td><select class='cls_dpt' id='dd_dpt_{5}'></select></td><td><select class='cls_grp' id='dd_grp_{5}'></select></td><td id='getgrp_{5}' class='cls_getgrp' style='display:none'>{9}</td><td id='getdpt_{5}' class='cls_getdpt' style='display:none'>{10}<td><div class='btn btn-primary btn-small' data={5} name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"], dr["Tax"], dr["HSNCode"], dr["Tax"], dr["GROUP_ID"],dr["Department"]));

            }


        }
        return str.ToString();
    }

    public string GetProductByDept(int Dept, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new DepartmentDAL().GetProductByDept(Dept);
        if (dr.HasRows)
        {

          
            str.Append("<tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Price</th><th>Tax</th><th>HSNCode</th><th>Department</th><th>Group</th><th></th></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td>{1}</td><td>{2}</td><td><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}' style='width:100px'/><td><input type='text' name='pprice' id='txtPrice_{5}' value='{4}' style='width:100px'/></td><td><input type='text' name='Tax' id='txttax_{5}' value='{6}' style='width:100px'/></td><td><input type='text' name='Tax' id='txthsncode_{5}' value='{7}' style='width:100px'/></td><td><select class='cls_dpt' id='dd_dpt_{5}'></select></td><td><select class='cls_grp' id='dd_grp_{5}'></select></td><td id='getgrp_{5}' class='cls_getgrp' style='display:none'>{9}<td><div class='btn btn-primary btn-small' data={5} name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"], dr["Tax"], dr["HSNCode"], dr["Tax"], dr["GROUP_ID"]));

            }


        }
        return str.ToString();
    }

    public string GetProductByDeptforHSN(int Dept,string HSncode,string ItemWise, out int TotalRows)
    {
        TotalRows = 0;
        StringBuilder str = new StringBuilder();
        SqlDataReader dr = new DepartmentDAL().GetProductByDeptforHSN(Dept,HSncode,ItemWise);
        if (dr.HasRows)
        {


            str.Append("<tr><th>Sno</th><th style='display:none'>ItemId</th><th>ItemCode</th><th>Name</th><th>MRP</th><th>Price</th><th>Tax</th><th>HSNCode</th><th>Department</th><th>Group</th><th></th></tr>");
            while (dr.Read())
            {
                TotalRows = Convert.ToInt16(dr["TotalRows"]);
                str.Append(string.Format("<tr><td>{0}</td><td style='display:none'>{5}</td><td>{1}</td><td>{2}</td><td><input type='text' name='pmrp' id='txtMrp_{5}' value='{3}' style='width:100px'/><td><input type='text' name='pprice' id='txtPrice_{5}' value='{4}' style='width:100px'/></td><td><input type='text' name='Tax' id='txttax_{5}' value='{6}' style='width:100px'/></td><td><input type='text' name='Tax' id='txthsncode_{5}' value='{7}' style='width:100px'/></td><td><select class='cls_dpt' id='dd_dpt_{5}'></select></td><td><select class='cls_grp' id='dd_grp_{5}'></select></td><td id='getgrp_{5}' class='cls_getgrp' style='display:none'>{9}</td><td id='getdpt_{5}' class='cls_getdpt' style='display:none'>{10}<td><div class='btn btn-primary btn-small' data={5} name='dvUpdate'><b>Update</b></div></td></tr>", dr["Sno"], dr["Item_Code"], dr["Item_Name"], dr["Max_Retail_Price"], dr["Sale_Rate"], dr["ItemID"], dr["Tax"], dr["HSNCode"], dr["Tax"], dr["GROUP_ID"],dr["Department"]));

            }


        }
        return str.ToString();
    }


}