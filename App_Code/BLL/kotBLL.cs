﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Data.SqlClient;

/// <summary>
/// Summary description for kotBLL
/// </summary>
public class kotBLL:Connection
{

    public Int32 EmployeeLoginCheck(Employees objEmployee, int BranchId)
    {
        return new kotDAL().EmployeeLoginCheck(objEmployee, BranchId);
    }



    public Int32 cancelKOt(KOT objKot)
    {
        return new kotDAL().CancelKOt(objKot);
    }
    public Int32 Insert(KOT objKOT, DataTable dt,int UserNo)
    {
        return new kotDAL().Insert(objKOT, dt,UserNo);
    }

    public Int32 UpdateKOTDeActive(int TableId, int BranchId)
    {
        return new kotDAL().UpdateKOTDeActive(TableId, BranchId);
    }

    public Int32 BillSettlement(Bill objBill)
    {
        return new kotDAL().BillSettlement(objBill);
    }

    public Int32 TransferTable(int FromTable, int ToTable, int BranchId, string qry)
    {
        return new kotDAL().TransferTable(FromTable, ToTable,BranchId,qry);
    }
    public string GetTable(int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();

        SqlDataReader dr = null;
        try
        {
            dr = new kotDAL().GetAll(BranchId);
            if (dr.HasRows)
            {

                while (dr.Read())
                {
                    if (dr["homedelivery"].ToString()=="1")
                    {
                        strBuilder.Append(string.Format("<div class='box' style='height:40px;width:46px' onclick='javascript:GetByItemCode(this,\"{0}\")'><h3>{1}<br/><span class='chkhd'>HD</span><span class='tableno' style='color:wheat;text-transform:uppercase;display:none'>{0}</span><span class='spn_cst_id' style='color:wheat;text-transform:uppercase;display:none'>{2}</span></h3></div>", dr["tableid"], dr["kotno_short"],dr["cst_id"]));
                        HttpContext.Current.Session["cst_id"] = dr["cst_id"];
                    }

                    else if (dr["TakeAway"].ToString() == "True")
                    {
                        strBuilder.Append(string.Format("<div class='box' style='height:40px;width:46px' onclick='javascript:GetByItemCode(this,\"{0}\")'><h3>{1}<br/>TA<span class='tableno' style='color:wheat;text-transform:uppercase;display:none'>{0}</span><span class='spn_cst_id' style='color:wheat;text-transform:uppercase;display:none'>{2}</span></h3></div>", dr["tableid"], dr["kotno_short"], dr["cst_id"]));
                        HttpContext.Current.Session["cst_id"] = dr["cst_id"];

                    }
                    else {

                        strBuilder.Append(string.Format("<div class='box' style='height:40px;width:46px' onclick='javascript:GetByItemCode(this,\"{0}\")'><h3>{0}<br/><span class='tableno' style='color:wheat;text-transform:uppercase;display:none'>{0}</span><span class='spn_cst_id' style='color:wheat;text-transform:uppercase;display:none'>{1}</span></h3></div>", dr["TableID"], dr["cst_id"]));
                        HttpContext.Current.Session["cst_id"] = dr["cst_id"];


                    }
                   
                }
                }

        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }


    public List<KotDetail> GetByKotNoDet(int Tableid, int BranchId)
    {
        List<KotDetail> KotList = new List<KotDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().GetAllKOTDet(Tableid, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotDetail objKot = new KotDetail()
                    {

                        KOTNo = Convert.ToDecimal(dr["KOTNo"].ToString()),
                        ProductCode = Convert.ToString(dr["ProductCode"].ToString()),
                        SaleRate = Convert.ToDecimal(dr["SaleRate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        DisPercentage = Convert.ToDecimal(dr["DisPercentage"].ToString()),
                        DisAmount = Convert.ToDecimal(dr["DisAmount"]),
                        TaxCode = Convert.ToDecimal(dr["TaxCode"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        Happy = Convert.ToBoolean(dr["Happy"]),
                        Rcode = Convert.ToInt32(dr["Rcode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        ItemName = Convert.ToString(dr["Item_Name"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        TaxType = Convert.ToString(dr["TaxType"])

                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }

    public List<KotDetail> GetByKotNoDetForSettlement(string BillNowPrefix, int BranchId)
    {
        List<KotDetail> KotList = new List<KotDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().GetAllKOTDetForSettelment(BillNowPrefix, BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotDetail objKot = new KotDetail()
                    {

                        KOTNo = Convert.ToDecimal(dr["KOTNo"].ToString()),
                        ProductCode = Convert.ToString(dr["ProductCode"].ToString()),
                        SaleRate = Convert.ToDecimal(dr["SaleRate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        DisPercentage = Convert.ToDecimal(dr["DisPercentage"].ToString()),
                        DisAmount = Convert.ToDecimal(dr["DisAmount"]),
                        TaxCode = Convert.ToDecimal(dr["TaxCode"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        Happy = Convert.ToBoolean(dr["Happy"]),
                        Rcode = Convert.ToInt32(dr["Rcode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        ItemName = Convert.ToString(dr["Item_Name"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),

                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }

    public List<KotDetail> GetByKotNoDetail(int Tableid, int BranchId)
    {
        List<KotDetail> KotList = new List<KotDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().GetAllKOTDetail(Tableid, BranchId);
  
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotDetail objKot = new KotDetail()
                    {

                        KOTNo = Convert.ToDecimal(dr["KOTNo"].ToString()),
                        ProductCode = Convert.ToString(dr["ProductCode"].ToString()),
                        SaleRate = Convert.ToDecimal(dr["SaleRate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        DisPercentage = Convert.ToDecimal(dr["DisPercentage"].ToString()),
                        DisAmount = Convert.ToDecimal(dr["DisAmount"]),
                        TaxCode = Convert.ToDecimal(dr["TaxCode"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        Happy = Convert.ToBoolean(dr["Happy"]),
                        Rcode = Convert.ToInt32(dr["Rcode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        ItemName = Convert.ToString(dr["Item_Name"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),
                        AddOn = Convert.ToString(dr["AddOn"]),
                        PaxNo = Convert.ToInt32(dr["PaxNo"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        TakeAway = Convert.ToBoolean(dr["TakeAway"]),
                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }

    public Int32 Insert(Bill objBill, DataTable dt, DataTable dt1)
    {
        return new kotDAL().Insert(objBill, dt, dt1);
    }

    public void GetByKot(KOT objKot,int BranchId)
    {




        SqlDataReader dr = null;
        try
        {
            dr = new kotDAL().GetAllKot(objKot.TableID,BranchId);
            if (dr.HasRows)
            {
                dr.Read();

               
                objKot.Time = Convert.ToDateTime(dr["Time"]);
                objKot.TableID = Convert.ToInt32(dr["TableID"]);
                objKot.PaxNo = Convert.ToInt32(dr["PaxNo"]);
                objKot.Value = Convert.ToDecimal(dr["Value"]);
                objKot.ServiceCharges = Convert.ToDecimal(dr["ServiceCharges"]);
                objKot.TotalAmount = Convert.ToDecimal(dr["TotalAmount"]);

                objKot.Complementary = Convert.ToBoolean(dr["Complementary"]);
                objKot.Happy = Convert.ToBoolean(dr["Happy"]);
                objKot.EmpName = Convert.ToString(dr["EmpCode"]);
                objKot.EmpCode = Convert.ToInt32(dr["EmpId"]);
                objKot.TaxAmount = Convert.ToDecimal(dr["TaxAmount"]);
                objKot.DisAmount = Convert.ToDecimal(dr["DisAmount"]);
                objKot.TakeAway = Convert.ToBoolean(dr["TakeAway"]);
               
              


            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }


    }



    public List<KOT> GetAllKotByTableNo(int Tableid, int BranchId)
    {
        List<KOT> KotList = new List<KOT>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().GetAllKotByTable(Tableid, BranchId);

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KOT objKot = new KOT()
                    {

                        KOTNo = Convert.ToInt32(dr["KotNo"].ToString()),
                        KotDate2 = Convert.ToString(dr["KotDate"].ToString()),
                        KotTime = Convert.ToString(dr["Time"].ToString()),
                        TotalAmount = Convert.ToDecimal(dr["TotalAmount"].ToString()),
                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }


    public string GetBill(int BranchId)
    {
        StringBuilder strBuilder = new StringBuilder();
        SqlDataReader dr = null;
        try
        {
            dr = new kotDAL().GetAllBill(BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    if (Convert.ToInt32( dr["tableno"]) >0)
                    {
                        if (dr["remarks"].ToString()== "HomeDelivery")
                        {
                            strBuilder.Append(string.Format("<div class='box cls_tableclick{2}' style='height:40px;width:50px' onclick='javascript:GetByBillNowPrefix(\"{0}\",\"{3}\")'><h5>{2}<br/>HD<span style='color:wheat;text-transform:uppercase;display:none'>{0}</span></h5><span class'getcst_id' style='display: none'>{3}</span></div>", dr["BillNowPrefix"], dr["Bill_No"], dr["tableno_short"],dr["cst_id"]));
                        }

                        else if  (dr["remarks"].ToString() == "TakeAway")
                        {
                            strBuilder.Append(string.Format("<div class='box cls_tableclick{2}' style='height:40px;width:50px' onclick='javascript:GetByBillNowPrefix(\"{0}\",\"{3}\")'><h5>{2}<br/>TA<span style='color:wheat;text-transform:uppercase;display:none'>{0}</span></h5><span class'getcst_id' style='display: none'>{3}</span></div>", dr["BillNowPrefix"], dr["Bill_No"], dr["tableno_short"], dr["cst_id"]));
                        }
                        else
                        {

                            strBuilder.Append(string.Format("<div class='box cls_tableclick{2}' style='height:40px;width:50px' onclick='javascript:GetByBillNowPrefix(\"{0}\",\"{3}\")'><h5>{2}<br/><span style='color:wheat;text-transform:uppercase;display:none'>{0}</span></h5><span class'getcst_id' style='display: none'>{3}</span></div>", dr["BillNowPrefix"], dr["Bill_No"], dr["tableno"], dr["cst_id"]));

                        }

                      
                    }
                }
            }
        }

        finally
        {
            dr.Close();
            dr.Dispose();
        }
        return strBuilder.ToString();

    }

    public void GetByBillNowPrefix(Settlement objSettlement)
    {
        SqlDataReader dr = null;
        try
        {
            dr = new kotDAL().GetByBillNowPrefix(objSettlement.BillNowPrefix);
            if (dr.HasRows)
            {
                dr.Read();
                objSettlement.BillNo = Convert.ToInt32(dr["Bill_No"]);
                objSettlement.BillDate = Convert.ToDateTime(dr["Bill_Date"]);
                objSettlement.CustomerName = Convert.ToString(dr["Customer_Name"]);
                objSettlement.BillMode = Convert.ToString(dr["BillMode"]);
                objSettlement.NetAmount = Convert.ToDecimal(dr["Net_Amount"]);
                objSettlement.tableNo = Convert.ToInt32(dr["tableno"]);
                objSettlement.EmpName = Convert.ToString(dr["Name"]);
            }
        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
    }



    public List<KotPrint> GetDataByUserNo(int UserNo)
    {
        List<KotPrint> KotList = new List<KotPrint>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().KOTprint(UserNo);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotPrint objKot = new KotPrint()
                    {
                        BillNowPrefix = Convert.ToString(dr["BillNowPrefix"].ToString()),

                        DepartmentName = Convert.ToString(dr["DepartmentName"]),


                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }

    public List<KOT> GetAllKotforcancel(int BranchId)
    {
        List<KOT> KOTList = new List<KOT>();

        SqlDataReader dr = null;
        try
        {
            dr = new kotDAL().GetAllKotforcancel(BranchId);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KOT objKOt = new KOT()
                    {
                        KOTNo = Convert.ToInt32(dr["KOtNo"].ToString()),

                        KotDate = Convert.ToDateTime(dr["KotDate"]),
                        MKOTNo = Convert.ToInt32(dr["MKOTNo"]),
                        Time = Convert.ToDateTime(dr["Time"]),
                        TableID = Convert.ToInt32(dr["TableID"]),
                        PaxNo = Convert.ToInt32(dr["PaxNo"]),
                        R_Code = Convert.ToInt32(dr["R_Code"]),
                        M_Code = Convert.ToInt32(dr["M_Code"]),
                        Value = Convert.ToInt32(dr["Value"]),
                        DisPercentage = Convert.ToDecimal(dr["DisPercentage"]),
                        DisAmount = Convert.ToDecimal(dr["DisAmount"]),
                        ServiceCharges = Convert.ToDecimal(dr["ServiceCharges"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        TotalAmount = Convert.ToDecimal(dr["TotalAmount"]),
                        Active = Convert.ToBoolean(dr["Active"]),
                        Happy = Convert.ToBoolean(dr["Happy"]),
                        Complementary = Convert.ToBoolean(dr["Complementary"]),
                        EmpCode = Convert.ToInt32(dr["EmpCode"]),
                        BillNoWPrefix = Convert.ToString(dr["BillNoWPrefix"]),
                        KotPrinted = Convert.ToInt32(dr["KotPrinted"]),
                        Pass = Convert.ToBoolean(dr["Pass"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        EmpName = Convert.ToString(dr["Name"]),
                    };
                    KOTList.Add(objKOt);
                }
            }

        }

        finally
        {
            dr.Close();
            dr.Dispose();

        }
        return KOTList;

    }



    public List<KotDetail> GetByKotNo(int KotNo)
    {
        List<KotDetail> KotList = new List<KotDetail>();
        SqlDataReader dr = null;
        SqlParameter[] ObjParam = new SqlParameter[0];

        try
        {
            dr = new kotDAL().GetByKotNo(KotNo);
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    KotDetail objKot = new KotDetail()
                    {

                        KOTNo = Convert.ToDecimal(dr["KOTNo"].ToString()),
                        TableNo = Convert.ToInt32(dr["TableNo"].ToString()),
                        ProductCode = Convert.ToString(dr["ProductCode"].ToString()),
                        SaleRate = Convert.ToDecimal(dr["SaleRate"].ToString()),
                        Qty = Convert.ToDecimal(dr["Qty"]),
                        Amount = Convert.ToDecimal(dr["Amount"]),
                        DisPercentage = Convert.ToDecimal(dr["DisPercentage"].ToString()),
                        DisAmount = Convert.ToDecimal(dr["DisAmount"]),
                        TaxCode = Convert.ToDecimal(dr["TaxCode"]),
                        TaxAmount = Convert.ToDecimal(dr["TaxAmount"]),
                        Happy = Convert.ToBoolean(dr["Happy"]),
                        Rcode = Convert.ToInt32(dr["Rcode"]),
                        BranchId = Convert.ToInt32(dr["BranchId"]),
                        ItemID = Convert.ToInt32(dr["ItemID"]),
                        ItemName = Convert.ToString(dr["Item_Name"]),
                        Tax_ID = Convert.ToDecimal(dr["Tax_ID"]),

                    };


                    KotList.Add(objKot);
                }
            }
        }
        finally
        {
            dr.Dispose();
            dr.Close();
            ObjParam = null;
        }
        return KotList;

    }
	
	 public Int32 DeleteKot(string ProductCode, decimal KotNo,int UserId)
    {
        return new kotDAL().DeleteKot(ProductCode, KotNo,UserId);
    }


}