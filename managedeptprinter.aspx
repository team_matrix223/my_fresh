﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managedeptprinter.aspx.cs" Inherits="managedeptprinter" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <link href="css/customcss/setup.css" rel="stylesheet" />
 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   <script language="javascript" type="text/javascript">

       var ProductCollection = [];
       function clsProduct() {

           this.Dep_Name = "";
           this.Print_Com = "";

       }



       function UpdateRecord(DeptName, Id) {


           var PrintCom = $("#txtid" + Id).val();


           if (PrintCom == "")
            {
               alert("Please Enter Printer Name");
               return;
            }

           $.ajax({
               type: "POST",
               data: '{ "DepartmentName": "' + DeptName + '","Printer":"' + PrintCom + '"}',
               url: "managedeptprinter.aspx/UpdateDepartmentPrinter",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);
                   alert("Information is updated");

               },
               complete: function (msg) {

               }

           });
       }


       function BindRows() {


           
           var html = "";

           for (var i = 0; i < ProductCollection.length; i++)
           {

               html += "<tr>";
               html += "<td>" + ProductCollection[i]["Dep_Name"] + "</td>";
               html += "<td> <input type='text' value = '" + ProductCollection[i]["Print_Com"] + "' /> </td>";
               html += "<td> <div class='btn btn-success'   onclick='javascript:UpdateRecord(" + ProductCollection[i]["Dep_Name"] + ");' id='btnAdd'>Update</div> </td>";
               html += "</tr>";
           }

           $("#tbDept").html(html);

       }


       function BindPrinter() {


           
           $.ajax({
               type: "POST",
               data: '{ }',
               url: "managedeptprinter.aspx/GetAll",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   $("#tbDept").html(obj.DeptLists);

               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {

                   $.uiUnlock();
               }

           });



       }



       $(document).ready(
       function () {
           BindPrinter();


           
       }
       );



   </script>

<style>
    .setup_title {
        background: #ffffff;
        color: #73879C;
    }
    .manage-dep-printer-x-panel-scnd
    {
        max-height:484px;
        min-height:484px;
        overflow:auto;
    }
    @media(min-width:992px) and (max-width:1200px)
    {
        .manage-dep-printer-x-panel-scnd {
	        max-height: 405px;
	        min-height: 405px;
	        overflow: auto;
        }
    }
</style>


<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Department Wise Printing</h3>
                        </div>
                 <!--       <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="clearfix"></div>
                     

             

                     

                    <div class="x_title setup_title">
                            <h2>Add/Edit Department Wise Printing</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                    <!--<div class="x_panel">
                        
                        <div class="x_content">

                             

                        </div>
                    </div>-->

            <!--        <div class="x_title setup_title">
                            <h2>Manage Department Printers</h2>
                             
                            <div class="clearfix"></div>
                    </div> -->
                    <div class="x_panel manage-dep-printer-x-panel-scnd">
                        
                        <div class="x_content">


    

 <table class="table table-striped Department_table">
                                         <thead>
                                         <%--<tr><th>Department Name</th><th>Printer</th><th></th></tr>--%>


                                         
                                         </thead>
                                         <tbody id="tbDept">
                                         </tbody>





                                    </table>



                                    
                        </div>

                        


                    </div>

                     
                </div>
                <!-- /page content -->

              

            </div>

            <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
            <!-- /footer content -->
 
</form>

            


</asp:Content>



