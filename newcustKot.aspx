﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="newcustKot.aspx.cs" Inherits="newcustKot" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Touchcss/bootstrap.min.css" rel="stylesheet" />
    <link href="Touchcss/css.css" rel="stylesheet" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
    <link rel="stylesheet" href="Touchcss/jquery-ui.css">
    <script src="Touchjs/jquery-1.10.2.js"></script>
    <script src="Touchjs/jquery-ui.js"></script>
    <script src="Touchjs/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="Touchjs/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Touchcss/bootstrap-glyphicons.css" />
    <link href="Touchjs/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Touchjs/jquery.uilock.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/customcss/customerkot.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.css" />
    <link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/keyboard.css" rel="stylesheet" /> 
    <script type="text/javascript" src="js/jquery.keyboard.js"></script>
    <%--<script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>--%>
    <style type="text/css">
        .form-control
        {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }
        
        #tbProductInfo tr
        {
            border-bottom: dotted 1px silver;
        }
        
        #tbProductInfo tr td
        {
            padding: 3px;
        }
        
           #tbkotinfo tr
        {
            border-bottom: dotted 1px silver;
        }
        
        #tbkotinfo tr td
        {
            padding: 3px;
        }
        
        #tboption tr
        {
            border-bottom: solid 1px black;
        }
        
        #tboption tr td
        {
            padding: 10px;
        }
        .Search1 {
    background-color: #f0626e;
    border-radius: 2px;
    float: left;
    margin: 0 0 5px -10px;
    padding: 10px;
    width: 103%;
}

    </style>
   </head>
<body style="background:#fffff6">
    <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
          <iframe id="reportkot" width="0" height="0" onload="processingComplete()"></iframe>


 <form id="Form1" runat="server">
    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />

 <asp:HiddenField ID="hdnempid" runat="server" />

     <section class="mobile_view">
         <div class="mob_header">
            <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
               <ul id="categories">
                     </ul>
                
            </div>

            <span class="catagories_togle" style="cursor:pointer" onclick="openNav()">&#9776; Catagories</span>
             <div class="pintu_midle">   <asp:Label ID="lblusername" runat="server"></asp:Label></div>
           
                 
             <button type='button' class='btn btn-demo order_btn' data-toggle='modal' data-target='#myModal2'>Order Items<span class="badge" id="btn_batch">0</span>    </button>
        </div>

         <div class="search_catagories">
             <ul>
                 <li><span>Table</span></li>
                 <li> <asp:DropDownList id="ddlTableOpt" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList></li>
                                 
                 <li><input class="form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Enter Code" aria-describedby="basic-addon2" id="txtItemCode" aria-haspopup="true" role="textbox" type="text"></li>
                 <li><span id="btnGetByItemCode" value="Search">
                     <img src="images/plusicon.png" alt=""></span>
                 </li>
                 <li><input class="form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Search By Name" aria-describedby="basic-addon2" id="txtSearch" aria-haspopup="true" role="textbox" type="text"></li>
                 <li><span id="btnSearch" value="Search">
                     <img src="images/search-button.png" alt=""></span>
                 </li>
             </ul>
             <ul>
                 <li><span>Pax</span></li>
                 <li style="margin-left: 9px;"> <asp:DropDownList id="ddlpax" class="form-control" ClientIDMode="Static" runat="server">
           <asp:ListItem Value="0">Choose</asp:ListItem>
           <asp:ListItem Value="1">1</asp:ListItem>
           <asp:ListItem Value="2">2</asp:ListItem>
           <asp:ListItem Value="3">3</asp:ListItem>
           <asp:ListItem Value="4">4</asp:ListItem>
           <asp:ListItem Value="5">5</asp:ListItem>
           <asp:ListItem Value="6">6</asp:ListItem>
           <asp:ListItem Value="7">7</asp:ListItem>
           <asp:ListItem Value="8">8</asp:ListItem>
           <asp:ListItem Value="9">9</asp:ListItem>
           <asp:ListItem Value="10">10</asp:ListItem></asp:DropDownList></li>
             </ul>
         </div>

         <%--<h1 class="best_food">Best Food</h1>--%>

         <div class="products_main">
                <ul class="cls_categories">
                     </ul>
                 <div class="mobile_view_products">
                   <div id="products"></div>
                 </div>
             <%--<div class="mobile_view_products">
                <div class="product_leftside">
                    <ul>
                    <li><p><i class="far fa-dot-circle"></i>MIX VEG RAITA</p></li>
                    <li><p class="item_price">100.00</p></li>
                   </ul>
                </div>

                 <div class="product_right_side">
                     <ul>
                     <li><input type="text" class="form-control"></li>
                     <li><button type="button" class="btn">Add</button></li>
                     </ul>
                 </div>
             </div>--%>

         </div>

         <div class="mobile_footer">

             <div class="cate2 modal_cate">
                 <table class="left_btn">
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tbody>
                                        <tr>
                                            <%--<td>
                                                <input type="text" placeholder="Mobile Number" style="border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all"
                                                    aria-haspopup="true" role="textbox">
                                            </td>
                                            <td>
                                                <div id="btnCustomer">
                                                    <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>
                                            </td>--%>
                                         <%--   <td style="padding-left: 10px">
                                                <asp:DropDownList ID="ddloption" runat="server" Style="width: 100px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>--%>
                                           <%-- <td>
                                             <select id="ddlTable" style="height: 25px; width: 90px" class="form-control">
                                        
                                            </td>--%>

                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 1px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2">
                                                    <tr>
                                                        <td>
                                                            <label style="font-weight: bold">
                                                                Cash Customer:</label><label id="lblCashCustomerName" style="font-weight: normal;
          margin-top: 1px"></label>
                                                        </td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                         <tr><td>
                          <table>
          
     
          <%-- <tr><td style="color:White;font-size:13px;font-weight:bold">Password:</td><td style="padding-left:3px;font-size:17px;padding-top:2px"><input id="txtPwd" type="text" style="width:131px;font-size:17px;"></td></tr>--%>
            
                             <tr class="total_amount_btns">
                      


                            <td>
                                <div class="button1" id="btnsaveKot">
                                     Save</div></td>

                              <td>
                                <div class="button1" id="btnTotal" style="display: none;">
                                     Total Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnExit">
                                    Exit</div>
                            </td>
                         <%--   <td>
                                <div class="button1" id="btnRaiseBill" style="width:85px;font-size:medium">
                                   Raise Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnSettlement" style="width:85px;font-size:medium">
                                   Settlement</div>
                            </td>
                             <td>
                                <div class="button1" id="btnTransfer" style="width:85px;font-size:medium">
                                    Transfer</div>
                            </td>--%>
                             
                        </tr>
                                
                                </table>
                         
                         </td></tr>
                       <%-- <tr>
                        <td>
                        <div class="button1" id="btnproceed" style="width:85px;font-size:medium">
                                     Proceed</div>
                        
                        </td>
                        </tr>--%>
                        
                        <tr>
                           
                            <td>
                                <div class="button1" id="btnCash"  style="display:none">
                                    Update KOT</div>
                            </td>
                           
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left; display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table id="tbamountinfo">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td class="footer_nt_amt">
                                           <span> Total:</span>
                                             <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    
                                    </tr>
                                    <tr style="display:none">
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            Discount:
                                        </td>
                                        <td style="width: 30px; text-align: center;">
                                            <div id="dvdisper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;">
                                            <div id="dvdiscount">
                                            </div>
                                        </td>
                                    </tr>
                                   
                                   <tr id ="trServicetax">
                                        <td style="width: 60%;font-weight: bold; text-align: right">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;"  >
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;"  >
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trKKC">
                                        <td style="width: 60%;font-weight: bold; text-align: right;">
                                            KKC:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;" >
                                            <div id="dvKKCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvKKCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trSBC">
                                        <td style="width: 60%;font-weight: bold; text-align: right">
                                            SBC:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;" >
                                            <div id="dvSBCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvSBCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="vatIncOrExc">
                                        <td style="font-weight: bold; text-align: right">
                                            Vat Amount:
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr style="display:none;" id ="DelCharges">
                                        <td>
                                           Delv Chrgs:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-small"  id="dvDeliveryChrg" value ="0" />
                                        </td>
                                    </tr>

<%--
                                     <tr id ="DelCharges" style="display:block">
                                        <td style="font-weight: bold; text-align: right;display:block">
                                            Delv Chrgs:
                                        </td>
                                        <td style="width: 100px; text-align: right;display:block" >
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvDeliveryChrg" value ="0" />

                                                    </td>
                                                    </tr>--%>
                                           
                                     
                                     <tr style="display:none;">
                                        <td>
                                            RoundOff:
                                        </td>
                                        <td>
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr style="display:none">
                                        <td class="footer_nt_amt">
                                            <span> Net Amt:</span>

                                            <div id="dvnetAmount" >
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                    
                </div>

             <%--<div class="container total_modal">
              
                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Total Amount</button>

               
                  <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                         
                          <h4 class="modal-title">Total Amount</h4>
                        </div>
                        <div class="modal-body">
                   
           </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>--%>
           </div>

    </section>


     <!--Order Items Modal-->
     <div class="container demo">
<%--	<div class="text-center">
		<button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
			Right Sidebar Modal
		</button>
	</div>--%>

	
	<!-- Modal -->
	<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel2">Items</h4>
				</div>

				<div class="modal-body">
					<div id="dvProductList" style="display: block">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tbody><tr>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <%--<th>Amt</th>--%>
                               <th>Delete</th>
                               <th>Add-Ons</th>
                                
                               
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="cate product_popup" id="dvProductInfo">
                        <table id="tbProductInfo">
                            <tbody><tr>
                                <td>
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                     </div>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
	
	
</div><!-- container -->

</form>



</body>
</html>
