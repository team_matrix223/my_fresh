﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class managepos : System.Web.UI.Page
{
    pos pos = new pos();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            bindgride();

        }
    }

    protected void bindgride()
    {
        pos.req = "bindgrid";

        DataTable dt = pos.bindgride();
        gv_display.DataSource = dt;
        gv_display.DataBind();
    }


    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {
        gv_display.PageIndex = e.NewPageIndex;
        gv_display.DataBind();
        bindgride();
    }


    protected void gv_display_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {

        int posid = Convert.ToInt32(e.CommandArgument);
        ViewState["glbl_posid"] = posid;
        if (e.CommandName == "select")
        {

            pos.req = "getpos";
            pos.posid = posid;
            pos.get_pos();
            tbtitle.Value = pos.title;
            ddstatus.SelectedValue = pos.status;
            btnsave.Text = "Update";

        }
        else if (e.CommandName == "del")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('POS will not remove if it is in use!');", true);
            pos.req = "deletepos";
            pos.posid = posid;
            pos.del_pos();
            bindgride();


        }
    }
    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "Submit")
        {
            pos.req = "insert";
            pos.title = tbtitle.Value.Trim();
            pos.status = ddstatus.SelectedValue;
            pos.insert_update_pos();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('POS Added Successfully!');", true);
        }
        else
        {
          
            pos.req = "update";
            pos.posid = Convert.ToInt32(ViewState["glbl_posid"]);
            pos.title = tbtitle.Value.Trim();
            pos.status = ddstatus.SelectedValue;
            btnsave.Text = "Submit";
            pos.insert_update_pos();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('POS Updated Successfully!');", true);
        }
        tbtitle.Value = "";
        bindgride();

    }
}