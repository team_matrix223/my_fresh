﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class managedashimg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)  //fileupload control contains a file  
            try
            {
              


                Connection conn = new Connection();
                Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
                //System.IO.Directory.Delete(Server.MapPath("/images/DashImg/"), true);
                //System.IO.Directory.CreateDirectory(Server.MapPath("/images/DashImg/"));
                string filename = Branch + "_" + FileUpload1.FileName;
                FileUpload1.SaveAs(Server.MapPath("/images/DashImg/") + filename);
                using (SqlConnection con = new SqlConnection(conn.sqlDataString))
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("update Mastersetting_Basic set  dash_img='"+ filename + "'  where branchid=" + Branch + "", con);
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
                    lblmsg.Text = "File Uploaded Sucessfully !! " + FileUpload1.PostedFile.ContentLength + "mb";     // get the size of the uploaded file  
            }   
            catch (Exception ex)
            {
                System.IO.Directory.CreateDirectory(Server.MapPath("/images/DashImg/"));
                // lblmsg.Text = "File Not Uploaded!!" + ex.Message.ToString();
                lblmsg.Text = "File Not Uploaded.Please Try Again!";

            }
        else
        {
            lblmsg.Text = "Please Select File and Upload Again";

        }
    }
}