﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ModifyBill : System.Web.UI.Page
{
    public string m_getdate { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            CheckRole();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
           
        }
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BILLDISCOUNT));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string CheckUsers(string Username,string Password)
    {

        User objuser = new User()
        {
            User_ID = Username,
            UserPWD = Password

        };

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        Int32 status = new UserBLL().SuperUserLoginCheck(objuser, Branch);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            Status = status

        };
        return ser.Serialize(JsonData);
    }

}