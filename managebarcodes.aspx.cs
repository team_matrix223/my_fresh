﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using DevExpress.XtraPrinting;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using DevExpress.XtraReports.UI;


using DevExpress.XtraPrinting.Preview;

public partial class managebarcodes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdnDate.Value = DateTime.Now.ToShortDateString();

        //ddlBranch.DataSource = new BranchBLL().GetAll();
        //ddlBranch.DataValueField = "BranchId";
        //ddlBranch.DataTextField = "BranchName";
        //ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);
    }

    public void print_wht_barcode(int whtItemId,int whtQty,string whtDate,bool whtBestExp,string whtDay,string whtweight)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            rptBarcode r = new rptBarcode(whtItemId, whtQty, Convert.ToDateTime(whtDate), whtBestExp, whtDay, whtweight);
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.OutputStream.Write(report, 0, report.Length);
         
            r.CreateDocument();
            r.Print();

        }
    }
    [WebMethod]
    public static string BindIemDetail(int ItemId)
    {

        ItemMaster ObjItem = new ItemMaster();
        ObjItem.ItemID = ItemId;

        List<NutritionFacts> lst = new ItemMasterBLL().GetProductById(ObjItem);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        
        var JsonData = new
        {
            ItemOptions = ObjItem,
            NutritionFact = lst
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Bindbranches()
    {

        string BranchData = new BranchBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BranchOptions = BranchData

        };
        return ser.Serialize(JsonData);
    }


    //[WebMethod]
    //public static string GetSaleRate(int BranchId,string ItemCode)
    //{
    //    decimal SaleRate = 0;
    //    decimal MRP = 0;
    //    SqlDataReader dr = null;
    //    dr = new BranchDAL().GetSaleRateAll(BranchId, ItemCode);
    //        if (dr.HasRows)
    //        {
    //            dr.Read();
    //            SaleRate = Convert.ToDecimal(dr["Sale_Rate"].ToString());
    //            MRP = Convert.ToDecimal(dr["MRP"].ToString());

    //        }
               
    //    JavaScriptSerializer ser = new JavaScriptSerializer();
    //    ser.MaxJsonLength = int.MaxValue;

    //    var JsonData = new
    //    {
    //        saleRate = SaleRate,
    //        mRP = MRP

    //    };
    //    return ser.Serialize(JsonData);
    //}


    [WebMethod]
    public static void insertupdate_whtbarcode(int wht_itemcode, string wht_rate, string wht_weight,int wht_Qty,int wht_ItemId,string wht_Date,bool wht_BestExp,string wht_Day,string wht_Branch)
    {
       Connection con = new Connection();
       using(SqlConnection conn=new SqlConnection (con.sqlDataString))
       {
           conn.Open();
           SqlCommand cmd = new SqlCommand("insertupdate_whtbarcode", conn);
           cmd.CommandType = CommandType.StoredProcedure;
           cmd.Parameters.AddWithValue("@wht_itemcode", wht_itemcode);
           cmd.Parameters.AddWithValue("@wht_rate", wht_rate);
           cmd.Parameters.AddWithValue("@wht_weight", wht_weight);
           cmd.ExecuteNonQuery();
       }

        //managebarcodes mng_barcode = new managebarcodes();
        //mng_barcode.print_wht_barcode(wht_ItemId, wht_Qty, wht_Date, wht_BestExp, wht_Day, wht_weight);
    

    }


    public void btnPrint_Click(object sender, EventArgs e)
    {
        
        //int qty = Convert.ToInt32(txtqty.Text);

        //   for (int i = 0; i < qty; i++)
        //   {
        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        rptBarcode r = new rptBarcode();
        //        r.CreateDocument();
        //        PdfExportOptions opts = new PdfExportOptions();
        //        opts.ShowPrintDialogOnOpen = true;
        //        r.ExportToPdf(ms, opts);
        //        ms.Seek(0, SeekOrigin.Begin);
        //        byte[] report = ms.ToArray();
        //        Page.Response.ContentType = "application/pdf";
        //        Page.Response.Clear();
        //        Page.Response.OutputStream.Write(report, 0, report.Length);
        //        Page.Response.End();
        //    }
        //}


    }  
  
    //protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //      //  string con = ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString;
    //      //  SqlConnection conn = new SqlConnection(con);
    //      ////  string com = "select Sale_Rate from branch_master where branchid=" + ddlBranch.SelectedValue;
    //      //  SqlCommand cmd = new SqlCommand(com, conn);
    //      //  conn.Open();
    //      //  SqlDataReader DR1 = cmd.ExecuteReader();
    //      //  if (DR1.Read())
    //      //  {
    //      //      ddlsalerate.Text = DR1.GetValue(0).ToString();
    //      //  }
    //      //  conn.Close();
       
    //}
}