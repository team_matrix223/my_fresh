﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class KotRaiseBIll : System.Web.UI.Page
{
   
    public string m_getdate { get; set; }
  
  public string Id { get { return Request.QueryString["Id"] != null ? Convert.ToString(Request.QueryString["Id"]) : string.Empty; } }

  public string tblno { get { return Request.QueryString["tblno"] != null ? Convert.ToString(Request.QueryString["tblno"]) : string.Empty; } }

  public string Status { get { return Request.QueryString["Status"] != null ? Convert.ToString(Request.QueryString["Status"]) : string.Empty; } }


  public string cashcustcode { get { return Request.QueryString["cashcustcode"] != null ? Convert.ToString(Request.QueryString["cashcustcode"]) : string.Empty; } }

  public string cashcustName { get { return Request.QueryString["cashcustName"] != null ? Convert.ToString(Request.QueryString["cashcustName"]) : string.Empty; } }
  public string cashcustmob { get { return Request.QueryString["cashcustmob"] != null ? Convert.ToString(Request.QueryString["cashcustmob"]) : string.Empty; } }
    public string companycustid { get { return Request.QueryString["compnycustid"] != null ? Convert.ToString(Request.QueryString["compnycustid"]) : string.Empty; } }
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    protected void Page_Load(object sender, EventArgs e)
  {
      
        if (!IsPostBack)
        {
            dayopencloseprm();
            //Session.Remove("cst_id");
            Session["RetValue"] = -22;
           // Response.Cookies[Constants.posid].Value = "3";
            hdntbleno.Value = tblno;
      hdndisplay.Value = Id;
      hdndd.Value = Status;
      hdncashcustid.Value = cashcustcode;
      hdncashcustname.Value = cashcustName;
      lblcstname.Text = cashcustName;
      lblcstmob.Text = cashcustmob;
      hdncompnycustid.Value = companycustid;
      hdnDate.Value = DateTime.Now.ToShortDateString();
      ltDateTime.Text = "<span style='font-weight:bold;'>Today - " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now) + "</span>";
      m_getdate = DateTime.Now.ToString("ddddd,MMMM-dd-yyyy");

            //string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            //if (strDate == "")
            //{

            //    Response.Redirect("index.aspx?DayOpen=Close");
            //}

            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
      gvTax.DataBind();

    
          
    }

    CheckRole();
    BindSteward();
          BindCreditCustomers();
        BindDropDownList();
        BindOtherPayment();
  }


    public void BindDropDownList()
    {

        DataSet ds = new CommonMasterDAL().GetAll();

        cm_ddlCities.DataSource = ds.Tables[0];
        cm_ddlCities.DataValueField = "CIty_ID";
        cm_ddlCities.DataTextField = "City_Name";
        cm_ddlCities.DataBind();

        cm_ddlArea.DataSource = ds.Tables[1];
        cm_ddlArea.DataTextField = "Area_Name";
        cm_ddlArea.DataValueField = "Area_ID";
        cm_ddlArea.DataBind();


        cm_ddlState.DataSource = ds.Tables[2];
        cm_ddlState.DataTextField = "State_Name";
        cm_ddlState.DataValueField = "STATE_ID";
        cm_ddlState.DataBind();



        cm_ddlPrefix.DataSource = ds.Tables[3];
        cm_ddlPrefix.DataTextField = "PROP_NAME";
        cm_ddlPrefix.DataValueField = "PROP_NAME";
        cm_ddlPrefix.DataBind();


    }

    void BindOtherPayment()
    {

        ddlOtherPayment.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOtherPayment.DataValueField = "OtherPayment_ID";
        ddlOtherPayment.DataTextField = "OtherPayment_Name";
        ddlOtherPayment.DataBind();

        ddlOtherPaymentnew.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOtherPaymentnew.DataValueField = "OtherPayment_ID";
        ddlOtherPaymentnew.DataTextField = "OtherPayment_Name";
        ddlOtherPaymentnew.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Other--";
        //li1.Value = "0";
        //ddlOtherPayment.Items.Insert(0, li1);

    }
    public void dayopencloseprm()
    {

        string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
        string PendingKot = new DayOpenCloseDAL().ChkPendingkot(Branch);

        if (strDate == "" && PendingKot == "")
        {

            Response.Redirect("index.aspx?DayOpen=Close");
        }

    }
  public void BindSteward()
  {
    ddlsteward.DataSource = new EmployeeBLL().GetAll();
    ddlsteward.DataTextField = "Name";
    ddlsteward.DataValueField = "Code";
    ddlsteward.DataBind();

    ListItem li1 = new ListItem();
    li1.Text = "Choose";
    li1.Value = "0";

    ddlsteward.Items.Insert(0, li1);

  }

  public void CheckRole()
  {
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

    string[] arrRoles = sesRoles.Split(',');

    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString()
                select m;

    int len = roles.Count();
    if (len == 0)
    {
      Response.Redirect("index.aspx");

    }
    else
    {
      Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
    }

  }


  [WebMethod]
  public static string GetTable()
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    string prodData = new kotBLL().GetTable(Branch);
    var JsonData = new
    {
      productData = prodData
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string GetKotDet(int KotNo)
  {

    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    List<KotDetail> lstKot = new kotBLL().GetByKotNoDet(KotNo, Branch);

    var JsonData = new
    {
      productLists = lstKot
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }
    [WebMethod]
    public static string GetKotDetForSettelment(string BillNowPrefix)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<KotDetail> lstKot = new kotBLL().GetByKotNoDetForSettlement(BillNowPrefix, Branch);

        var JsonData = new
        {
            productLists = lstKot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
  public static string UpdateKotDeActive(int TableID)
  {
    int status = 0;
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    JavaScriptSerializer ser = new JavaScriptSerializer();

    status = new kotBLL().UpdateKOTDeActive(TableID, Branch);
    var JsonData = new
    {

      Status = status
    };
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string GetKot(int KotNo)
  {
    KOT objkot = new KOT();
    objkot.TableID = KotNo;
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    new kotBLL().GetByKot(objkot, Branch);

    var JsonData = new
    {
      KOT = objkot
    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;
    return ser.Serialize(JsonData);
  }


  [WebMethod]
  public static string BindEmployees()
  {

    string EmployeesData = new EmployeeBLL().GetOptions();


    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {
      EmployeeOptions = EmployeesData

    };
    return ser.Serialize(JsonData);
  }


  void BindCreditCustomers()
  {

    ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
    ddlChosseCredit.DataValueField = "CCODE";
    ddlChosseCredit.DataTextField = "CNAME";
    ddlChosseCredit.DataBind();
    ListItem li1 = new ListItem();
    li1.Text = "--Choose Customer--";
    li1.Value = "0";
    ddlChosseCredit.Items.Insert(0, li1);


        ddlChosseCreditnew.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCreditnew.DataValueField = "CCODE";
        ddlChosseCreditnew.DataTextField = "CNAME";
        ddlChosseCreditnew.DataBind();
        ListItem li11 = new ListItem();
        li11.Text = "--Choose Customer--";
        li11.Value = "0";
        ddlChosseCreditnew.Items.Insert(0, li11);

    }

  [WebMethod]
  public static string LoadUserControl(string counter)
  {
    using (Page page = new Page())
    {
      UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
      (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
      page.Controls.Add(userControl);
      using (StringWriter writer = new StringWriter())
      {
        page.Controls.Add(userControl);
        HttpContext.Current.Server.Execute(page, writer, false);
        return writer.ToString();
      }
    }
  }


  [WebMethod]
  public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {
      productLists = lstProducts

    };
    return ser.Serialize(JsonData);

  }


  [WebMethod]
  public static string Reprint(string PrintType, string BillNowPrefix)
  {

    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    new CommonMasterDAL().Reprint(UserNo, PrintType, Branch, BillNowPrefix);


    JavaScriptSerializer ser = new JavaScriptSerializer();
    ser.MaxJsonLength = int.MaxValue;

    var JsonData = new
    {


    };
    return ser.Serialize(JsonData);

  }







  [WebMethod]
  public static string GetAllBillSetting()
  {
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    CommonSettings ObjSettings = new CommonSettings();
    ObjSettings.Type = "Retail";
    ObjSettings.BranchId = Branch;
    List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
    var JsonData = new
    {

      setttingData = ObjSettings,
      DiscountDetail = lst

    };
    JavaScriptSerializer ser = new JavaScriptSerializer();
    return ser.Serialize(JsonData);
  }



  [WebMethod]
  public static string Delete(string BillNowPrefix)
  {


    int Status = 0;
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

    string[] arrRoles = sesRoles.Split(',');


    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                select m;


    if (roles.Count() == 0)
    {
      Status = -10;
    }
    Bill objBill = new Bill()
    {
      BillNowPrefix = BillNowPrefix,

    };

    JavaScriptSerializer ser = new JavaScriptSerializer();

    Status = new BillBLL().DeleteBill(objBill);
    var JsonData = new
    {
      billmaster = objBill,
      status = Status
    };
    return ser.Serialize(JsonData);
  }

  [WebMethod]
  public static string InsertUpdate(string CustomerId, string CustomerName, decimal BillValue, decimal DiscountPer, decimal DiscountAmt, decimal AddTaxAmt, decimal NetAmt, string BillMode, string CreditBank, decimal CashAmt, decimal CreditAmt, decimal CrCardAmt, decimal RoundAmt, int CashCustCode, string CashCustName, int TableNo, decimal SerTax, string Remarks, Int32 OrderNo, string Type, Int32 EmpCode, string BillNowPrefix, string itemcodeArr, string qtyArr, string priceArr, string taxArr, string orgsalerateArr, string SurPerArr, string AmountArr, string TaxAmountArr, string SurValArr, string ItemRemarksArr, string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, decimal OnlinePayment, decimal DeliveryCharges, decimal KKCPer, decimal KKCAmt, decimal SBCPer, decimal SBCAmt, string BillRemarks, string FreeArr,string CustomerPhone,string Order_No,string OTP,string Rdopaymode,string cstid)
    {
  
        int status = 0;
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

    string[] arrRoles = sesRoles.Split(',');

     
    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
                select m;


    if (roles.Count() == 0)
    {
      status = -11;
    }






    int Status = 0;
    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //User objuser = new User()
        //{
        //    UserNo = UserNo

        //};

        //Int32 status2 = new UserBLL().chkIsLogin(objuser);

        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            Bill_Value = BillValue,
            DiscountPer = DiscountPer,
            Less_Dis_Amount = DiscountAmt,
            Add_Tax_Amount = AddTaxAmt,
            Net_Amount = NetAmt,
            BillMode = BillMode,
            CreditBank = CreditBank,
            UserNO = UserNo,
            Bill_Type = 1,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CrCard_Amount = CrCardAmt,
            Round_Amount = RoundAmt,
            Bill_Printed = false,
            Passing = false,
            CashCust_Code = CashCustCode,
            CashCust_Name = CashCustName,
            Tax_Per = 0,
            R_amount = RoundAmt,
            tableno = TableNo,
            remarks = Remarks,
            servalue = SerTax,
            ReceiviedGRNNo = 0,
            EmpCode = EmpCode,
            OrderNo = OrderNo,
            BranchId = Branch,
            OnlinePayment = OnlinePayment,
            DeliveryCharges = DeliveryCharges,
            KKCPer = KKCPer,
            KKCAmt = KKCAmt,
            SBCPer = SBCPer,
            SBCAmt = SBCAmt,
            BillRemarks = BillRemarks,
            Order_No = Order_No,
            OTP = OTP,
            RdoPaymode =Rdopaymode,
            cst_id=Convert.ToInt32(cstid)

    };
    //if (status2 != 0)
    //{
    string[] ItemCode = itemcodeArr.Split(',');
    string[] Qty = qtyArr.Split(',');
    string[] Price = priceArr.Split(',');
    string[] Tax = taxArr.Split(',');
    string[] OrgSaleRate = orgsalerateArr.Split(',');
    string[] SurPer = SurPerArr.Split(',');
    string[] Amount = AmountArr.Split(',');
    string[] TaxAmount = TaxAmountArr.Split(',');
    string[] SurVal = SurValArr.Split(',');
    string[] remarksdata = ItemRemarksArr.Split(',');
    string[] Freedata = FreeArr.Split(',');

    string[] taxdendata = arrTaxden.Split(',');
    string[] vatamtdendata = arrVatAmtden.Split(',');
    string[] vatdendata = arrVatden.Split(',');
    string[] surchrgdendata = arrSurden.Split(',');

    DataTable dt = new DataTable();
    dt.Columns.Add("ItemCode");
    dt.Columns.Add("Rate");
    dt.Columns.Add("Qty");
    dt.Columns.Add("Amount");
    dt.Columns.Add("Tax");
    dt.Columns.Add("Tax_Amount");
    dt.Columns.Add("OrgSaleRate");
    dt.Columns.Add("SurPer");
    dt.Columns.Add("SurVal");
    dt.Columns.Add("Remarks");
    dt.Columns.Add("Free");

    DataTable dt1 = new DataTable();
    dt1.Columns.Add("Tax");
    dt1.Columns.Add("VatAmt");
    dt1.Columns.Add("Vat");
    dt1.Columns.Add("SurCharge");



    for (int i = 0; i < ItemCode.Length; i++)
    {
      DataRow dr = dt.NewRow();
      dr["ItemCode"] = ItemCode[i];
      dr["Rate"] = Convert.ToDecimal(Price[i]);
      dr["Qty"] = Convert.ToDecimal(Qty[i]);
      if (Convert.ToBoolean(Freedata[i]) == true)
      {
        dr["Amount"] = 0;
      }
      else
      {
        dr["Amount"] = Convert.ToDecimal(Amount[i]);
      }

      dr["Tax"] = Convert.ToDecimal(Tax[i]);
      dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
      dr["OrgSaleRate"] = Convert.ToDecimal(OrgSaleRate[i]);
      dr["SurPer"] = Convert.ToDecimal(SurPer[i]);
      dr["SurVal"] = Convert.ToDecimal(SurVal[i]);
      dr["Remarks"] = Convert.ToString(remarksdata[i]);
      dr["Free"] = Convert.ToBoolean(Freedata[i]);
      dt.Rows.Add(dr);
    }


    for (int j = 0; j < taxdendata.Length; j++)
    {

      DataRow dr1 = dt1.NewRow();
      dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
      dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
      dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
      dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
      dt1.Rows.Add(dr1);
    }

    if (objBill.BillNowPrefix == "")
    {
      Status = new kotBLL().Insert(objBill, dt, dt1);
    }
    else
    {
      Status = new BillBLL().Update(objBill, dt, dt1);
    }
    Connection con = new Connection();
    SqlParameter[] Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@MessageText", "Order_Bill_Msg");
    var Message = SqlHelper.ExecuteScalar(con.sqlDataString, CommandType.StoredProcedure, "Proc_getbillingsms", Param);
    if (Message != null) 
    {
      //Message = Message.ToString().Replace("$CN$", CustomerName).Replace("$AM$", NetAmt.ToString());
      //SendSms SendMessage = new SendSms();
      //SendMessage.SMSSend(CustomerPhone, Message.ToString());
    }
    //}
    //else
    //{
    //    Status = -5;


    //}
    JavaScriptSerializer ser = new JavaScriptSerializer();


    var JsonData = new
    {
      Bill = objBill,
      BNF = objBill.BillNowPrefix,
      Status = Status
    };
    return ser.Serialize(JsonData);
  }

    [WebMethod]
    public static string GeneratePDF(string BillNowPrefix)
    {
        string rtnval = "1";
        try
        {
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            bool Barcode = false;
            bool PrintSign = false;
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select BarcodePrintOnBill,Print_Sign from mastersetting_Basic ", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Barcode = Convert.ToBoolean(dt.Rows[0]["BarcodePrintOnBill"].ToString());
                PrintSign = Convert.ToBoolean(dt.Rows[0]["Print_Sign"].ToString());
            }

            using (MemoryStream ms = new MemoryStream())
            {

                // rptBarcode r = new rptBarcode(ItemId, Qty, Date, BestExp, Day, wt);

                ARetailBill r = new ARetailBill(BillNowPrefix, Barcode, PrintSign);
                SqlCommand _Cmd = null;
                _Cmd = new SqlCommand();
                _Cmd.Connection = con;
                _Cmd.CommandText = "Report_sp_ExciseChallanReportSubBillReport";
                _Cmd.Parameters.AddWithValue("@BranchId", Convert.ToInt32(Branch));
                _Cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
                _Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpex = new SqlDataAdapter(_Cmd);
                DataSet dse = new DataSet();
                adpex.Fill(dse);
                var subReport = r.FindControl("xrSubreport1", true) as DevExpress.XtraReports.UI.XRSubreport;
                if (subReport != null)
                {
                    subReport.ReportSource.DataSource = dse;
                    //subReport.ReportSource.DataMember = _DataSet.Tables[1].TableName; //Here

                    //  subReport.ReportSource.Parameters.Add();
                    subReport.ReportSource.CreateDocument(true);
                }

                r.PrintingSystem.ShowMarginsWarning = true;

                r.CreateDocument();

                string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
                string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");



                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                //if (!Directory.Exists(@"E:/pdf/"+dbname+""))
                //{
                //    Directory.CreateDirectory(@"E:/pdf/" + dbname + "");
                //}
                // SavePDF(r, "C:/inifdpdf/" + BillNowPrefix + ".pdf", "data");
                DevExpress.XtraPrinting.PdfExportOptions opts = new DevExpress.XtraPrinting.PdfExportOptions();
                opts.ShowPrintDialogOnOpen = true;
                // r.ExportToPdf(@"F:/pdf/" + dbname + "/" + BillNowPrefix + ".pdf");
                if ((System.IO.File.Exists(filepath + "/" + BillNowPrefix + ".pdf")))
                {
                    System.IO.File.Delete(filepath + "/" + BillNowPrefix + ".pdf");
                    r.ExportToPdf(filepath + "/" + BillNowPrefix + ".pdf");

                }
                else
                {
                    r.ExportToPdf(filepath + "/" + BillNowPrefix + ".pdf");


                }


            }



        }
        catch
        {


        }
        return rtnval;
    }


}