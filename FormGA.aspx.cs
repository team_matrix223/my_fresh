﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class FormGA : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{

	}

    [WebMethod]
    public static string Getdata(string date,int req,int pos) {

        Connection conn = new Connection();
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        StringBuilder str = new StringBuilder();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

           
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_GroupSale", con);
            cmd.CommandType = CommandType.StoredProcedure;
 
            cmd.Parameters.AddWithValue("@FromDate", date.Trim());
            cmd.Parameters.AddWithValue("@ToDate", date.Trim());
            cmd.Parameters.AddWithValue("@BranchId", Branch);
            cmd.Parameters.AddWithValue("@pos_id", pos);
            cmd.Parameters.AddWithValue("@req", req);
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                str.Append(string.Format("<tr><td> "+rd["Group_Name"].ToString() + " </td><td><input class='cls_actual' type = 'text' maxlength = '50' readonly value=" + rd["Amount"].ToString() + " /></td><td><input type = 'number' class='cls_reqsale' maxlength = '50' value='0' /></td><td><input type = 'number' class='cls_addlessale' maxlength = '50' value='0' /></td><td><button type = 'button' value=" + rd["Group_ID"].ToString() + "  class='cls_btnupdate btn-danger' >Update</button></td></tr>"));

            }


            var JsonData = new
            {
                option = str.ToString()
      
            };

            return ser.Serialize(JsonData);



        }
    }
}