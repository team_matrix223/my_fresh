﻿<%@ WebHandler Language="C#" Class="ManageBills" %>

using System;
using System.Web;

public class ManageBills : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

	public void ProcessRequest(HttpContext context)
	{

		System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
		string strOperation = forms.Get("oper");
		string strResponse = string.Empty;

		int BranchId = 0;
		string dateFrom = context.Request.QueryString["dateFrom"];
		string dateTo = context.Request.QueryString["dateTo"];
		int UserId = Convert.ToInt32(context.Request.QueryString["UserId"]);
		int PosId = Convert.ToInt32(context.Request.QueryString["PosId"]);
		//int BranchValue = Convert.ToInt32(context.Request.QueryString["BranchId"]);
		int BranchValue = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

		if (BranchValue == 0)
		{
			BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);


		}
		else
		{
			BranchId = BranchValue;

		}



		int UserNo = 0;
		if (UserId == 0)
		{
			UserNo = 0;


		}
		else
		{
			UserNo = UserId;

		}

		//oper = null which means its first load.
		var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

		jsonSerializer.MaxJsonLength = Int32.MaxValue;
		var xss = jsonSerializer.Serialize(
		new BillBLL().GetBillByDateByUsersbypos(Convert.ToDateTime(dateFrom), Convert.ToDateTime(dateTo), BranchId, UserNo,PosId)
		   );

		context.Response.Write(xss);

	}




	public bool IsReusable
	{
		get
		{
			return false;
		}
	}
}