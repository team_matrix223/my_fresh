﻿<%@ WebHandler Language="C#" Class="CashCustomerSearch" %>

using System;
using System.Web;

public class CashCustomerSearch : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {


        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");


       
        string strSearchText = context.Request.QueryString["stext"];
       
        string strResponse = string.Empty;




        if (strSearchText != null)
        {

            //oper = null which means its first load.

            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            context.Response.Write(jsonSerializer.Serialize(
             new CustomerBLL().AdvancedSearchcashcustomer( strSearchText)
                 ));
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}