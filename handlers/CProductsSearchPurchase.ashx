﻿<%@ WebHandler Language="C#" Class="CProductsSearch" %>

using System;
using System.Web;

public class CProductsSearch : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {


        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");


       
        string strSearchText = context.Request.QueryString["stext"];
       
        string strResponse = string.Empty;

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);   


        if (strSearchText != null)
        {

            //oper = null which means its first load.

            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            context.Response.Write(jsonSerializer.Serialize(
             new ProductBLL().AdvancedSearchItemsforPurchase( strSearchText,BranchId)
                 ));
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}