﻿<%@ WebHandler Language="C#" Class="lAdvanceReceiptEntry" %>

using System;
using System.Web;

public class lAdvanceReceiptEntry : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    
    public void ProcessRequest (HttpContext context) 
    {
        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");
        string strResponse = string.Empty;
        
        Int32 orderno = Convert.ToInt32(context.Request.QueryString["orderno"]);
        Int32 BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);


        if (strOperation == null)
        {
            //oper = null which means its first load.
            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            var x = jsonSerializer.Serialize(

                new BookingBLL().GetAllReceiptEntry(orderno,BranchId));

            context.Response.Write(x);
        }
       

        
       
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
 
   

}