﻿<%@ WebHandler Language="C#" Class="CProductsSearch" %>

using System;
using System.Web;

public class CProductsSearch : IHttpHandler {

    public void ProcessRequest(HttpContext context)
    {


        System.Collections.Specialized.NameValueCollection forms = context.Request.Form;
        string strOperation = forms.Get("oper");


       
        string Keyword = context.Request.QueryString["Keyword"];
        string Type = context.Request.QueryString["Type"];
             string AccountType = context.Request.QueryString["AccountType"];
             string Group = context.Request.QueryString["Group"];
             string ItemType = context.Request.QueryString["ItemType"];
            string Godown = context.Request.QueryString["Godown"];
        string strResponse = string.Empty;

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);   


        if (Keyword != null)
        {

            //oper = null which means its first load.

            var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            jsonSerializer.MaxJsonLength = int.MaxValue;
            context.Response.Write(jsonSerializer.Serialize(
             new ProductBLL().AdvanceSearchForStock( Keyword, Type, AccountType,Convert.ToInt32(Group), ItemType,Convert.ToInt32(Godown), BranchId)
                 ));
        }

    }




    public bool IsReusable
    {
        get
        {
            return false;
        }
    }


}