﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class managecustomer_rate : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
            //binditemlist();
            bind_ddcustomer();
            BinDDept();
            //bind_dditem();

        }
    }
    public void BinDDept()
    {
        ddlCat2.DataSource = new DepartmentBLL().GetAll();
        ddlCat2.DataValueField = "Prop_ID";
        ddlCat2.DataTextField = "Prop_Name";
        ddlCat2.DataBind();
        ListItem li1 = new ListItem();
        
        li1.Text = "--Choose Department--";
        li1.Value = "-1";
        
        ddlCat2.Items.Insert(0, li1);


        ddlGroup.DataSource = new GroupBLL().GetAll();
        ddlGroup.DataValueField = "Group_Id";
        ddlGroup.DataTextField = "Group_Name";
        ddlGroup.DataBind();
        ListItem li2 = new ListItem();

        li2.Text = "--Choose Group--";
        li2.Value = "-1";

        ddlGroup.Items.Insert(0, li2);



        ddlsbgrp.DataSource = new SubGroupBLL().GetAll();
        ddlsbgrp.DataValueField = "SGroup_Id";
        ddlsbgrp.DataTextField = "SGroup_Name";
        ddlsbgrp.DataBind();
        ListItem li3 = new ListItem();

        li3.Text = "--Choose SubGroup--";
        li3.Value = "-1";

        ddlsbgrp.Items.Insert(0, li3);
    }
    protected void bindlist()
    {
        if (rdbGroup.Checked == true)
        {
            baseon = "Group";

        }
        if (rdbSubGroup.Checked == true)
        {
            baseon = "SubGroup";

        }
        if (rdbDepartment.Checked == true)
        {
            baseon = "Department";

        }

        msr.req = "bindlistnew";
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        msr.Dept_id = Convert.ToInt32(ddlCat2.SelectedValue);
        msr.grp_id = Convert.ToInt32(ddlGroup.SelectedValue);
        msr.sbgrp_id = Convert.ToInt32(ddlsbgrp.SelectedValue);
        msr.baseon = baseon;
        DataTable dt = msr.bind_item_ddnew();
        rpt_itemlist.DataSource = dt;
        rpt_itemlist.DataBind();
        if (dt.Rows.Count > 0)
        {

            //btnsave.Visible = true;
        }

    }

    public  void bind_ddcustomer()
    {
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Customer-", "0");
        dd_customername.Items.Insert(0, listItem1);
     
    }

    //public void bind_dditem()
    //{
    //    msr.req = "bind_dditem";
    //    DataTable dt = msr.bind_gride_dd();
    //    dd_itemname.DataSource = dt;
    //    dd_itemname.DataTextField = "item_name";
    //    dd_itemname.DataValueField = "item_Code";
    //    dd_itemname.DataBind();
    //    System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Item-", "0");
    //    dd_itemname.Items.Insert(0, listItem1);
    //}



    protected void btnsave_Click(object sender, EventArgs e)
    {
        msr.req = "del_cst";
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        msr.insert_update_customer();
        foreach (RepeaterItem item in rpt_itemlist.Items)
        {
            string itemcode =((Label)item.FindControl("lblitemcode")).Text;
            decimal rate = Convert.ToDecimal(((System.Web.UI.HtmlControls.HtmlInputControl)item.FindControl("tbrate")).Value);
            //if(rate>0)
            //{
            msr.req = "insert";
            msr.item_Code = itemcode;
            msr.rate = rate;
            msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
            msr.insert_update_customer();
            //}

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);
    }

    protected void SubmiteDetails(object sender, EventArgs e)
    {
        RepeaterItem item = (sender as Button).NamingContainer as RepeaterItem;
        string itemcode = (item.FindControl("lblitemcode") as Label).Text;
        string rate = (item.FindControl("tbrate") as TextBox).Text;

        msr.req = "insert";
        msr.item_Code = itemcode;
        msr.rate = Convert.ToDecimal(rate);
        msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        msr.insert_update_customer();

        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);

    }
    protected void rpt_itemlist_Itemcommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "SaveItem")
        //{
        //    string itemcode = e.CommandArgument.ToString();
        //    System.Web.UI.HtmlControls.HtmlInputText rate =(System.Web.UI.HtmlControls.HtmlInputText)e.Item.FindControl("tbrate");
        //    msr.req = "insert";
        //    msr.item_Code = itemcode;
        //    msr.rate =Convert.ToDecimal(rate);
        //    msr.cst_id = Convert.ToInt32(dd_customername.SelectedValue);
        //    msr.insert_update_customer();

        //}
    }
    string baseon = "";
    protected void dd_customername_SelectedIndexChanged(object sender, EventArgs e)
    {
        
       
        bindlist();
    }


    protected void rdbsubgroup_CheckedChanged(object sender, EventArgs e)
    {

        if (rdbSubGroup.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbDepartment.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = true;
        }
       
    }

    protected void rdbgroup_changed(object sender, EventArgs e)
    {

        if (rdbGroup.Checked == true)
        {
            rdbSubGroup.Checked = false;
            rdbDepartment.Checked = false;
            ddlGroup.Visible = true;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = false;
        }

    }

    protected void rdbdepartment_changed(object sender, EventArgs e)
    {

        if (rdbDepartment.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = true;
            ddlsbgrp.Visible = false;
        }

    }
    protected void rdblist_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}