﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DForm : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
        }
       
    }


   


    //void BindBranches()
    //{

    //    ddlBranch.DataSource = new BranchBLL().GetAll();
    //    ddlBranch.DataValueField = "BranchId";
    //    ddlBranch.DataTextField = "BranchName";
    //    ddlBranch.DataBind();
    //    ListItem li1 = new ListItem();
    //    li1.Text = "--Choose Branch--";
    //    li1.Value = "0";
    //    ddlBranch.Items.Insert(0, li1);

    //}


   


    [WebMethod]
    public static void insert_bill_bytax(string[] arr_billno, string fromtax, string totax,int POS)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("bill_no");
        for (int i = 0; i < arr_billno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["bill_no"] = arr_billno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_insertbill_bytax", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromtax", fromtax);
            cmd.Parameters.AddWithValue("@totax", totax);
            cmd.Parameters.AddWithValue("@bill_no", dt);
           // cmd.Parameters.AddWithValue("@POS", POS);
            cmd.CommandTimeout = 5000;
            cmd.ExecuteNonQuery();

        }

    }


    [WebMethod]
    public static void InsertintoDeleteBill( DateTime DateFrom)
    {

        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_insertDeleteBill", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@FromDate", DateFrom);


            cmd.ExecuteNonQuery();

        }

    }

    [WebMethod]
    public static string CheckDeleteBill()
    {
        Bill objBill = new Bill() { };
        
        new BillBLL().checkdeletebill(objBill);

        var JsonData = new
        {
            productData = objBill
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static void InsertintoBill(DateTime DateFrom)
    {

        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_insertintoBill", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@FromDate", DateFrom);


            cmd.ExecuteNonQuery();

        }

    }

}