﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class managevoucherentry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    
    {
        if (!IsPostBack)
        {
            hdntodaydate.Value = DateTime.Now.ToShortDateString();
            BindDropDownList();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

            ddlBranch.DataSource = new BranchBLL().GetAllBranch();
            ddlBranch.DataTextField = "BranchName";
            ddlBranch.DataValueField = "BranchId";
            ddlBranch.DataBind();

            ddlBranch.Items.FindByValue(Convert.ToString(Branch)).Selected = true;


            ddlBank.DataSource = new VoucherBLL().GetAccountbybnksetting(Branch);
            ddlBank.DataTextField = "CName";
            ddlBank.DataValueField = "CCode";
            ddlBank.DataBind();
            ListItem li = new ListItem();
            li.Text = "--SELECT--";
            li.Value = "0";
            ddlBank.Items.Insert(0, li);
           
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
            if (strDate == "")
             {

              Response.Redirect("index.aspx?DayOpen=Close");
             }
        }
        CheckRole();

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    public void bindaccounts(string Bank)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new VoucherBLL().GetGridAccountsByBank(hdnProducts, Branch,Bank);
    }


  
    public void BindDropDownList()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new VoucherBLL().GetGridAccounts(hdnProducts, Branch);
    }

    [WebMethod]
    public static string GetBalance(string CCODE)
    {

        string val = "0";
        Connection con = new Connection();
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("Report_sp_VoucherBal", conn);
            cmd.Parameters.AddWithValue("@CCODE", CCODE);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Debit"].ToString() == "0.00")
                {
                    val = dt.Rows[0]["Credit"].ToString() + ' ' + dt.Rows[0]["DRCR"].ToString();
                }
                else if (dt.Rows[0]["Credit"].ToString() == "0.00")
                {
                   val = dt.Rows[0]["Debit"].ToString() + ' ' + dt.Rows[0]["DRCR"].ToString();
                }
            }

        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            counter = val,
            

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Insert(string VouchNo,string TrDate, string BankId, string VoucherType, 
        string codeArr, string NameArr, string PaymentArr,
        string ReceiptArr, string ChequeArr, string DebitArr, string CreditArr, string NarrArr, string CCODEArr,Int32 BranchId,string multisingle)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');



       // Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        AccTrans objTrans = new AccTrans()
        {
            VOUCH_NO = VouchNo,
            TR_DATE = Convert.ToDateTime(TrDate),
            VTYPE = VoucherType,
            PUR_NO = "",
            BILL_NO = "",
            USERNO = UserNo,
            PASS = true,
            Ref_No = "",
            PartyBank = "",
            RowType = "E",
            AutoVoucher="",
            SrNo =0,
            ShortName ="",
            BranchId =BranchId,
            

            
        };

        string[] codeData = codeArr.Split(',');
        string[] NameData = NameArr.Split(',');
        string[] PaymentData = PaymentArr.Split(',');
        string[] ReceiptData = ReceiptArr.Split(',');
        string[] ChequeData = ChequeArr.Split(',');
        string[] DebitData = DebitArr.Split(',');
        string[] CreditData = CreditArr.Split(',');
        string[] NarrData = NarrArr.Split(',');
        string[] CCODEData = CCODEArr.Split(',');
       

        DataTable dt = new DataTable();
        dt.Columns.Add("Code");
        dt.Columns.Add("Name");
        dt.Columns.Add("Payment");
        dt.Columns.Add("Receipt");
        dt.Columns.Add("Cheque");
        dt.Columns.Add("Debit");
        dt.Columns.Add("Credit");
        dt.Columns.Add("Narr");
        dt.Columns.Add("CCODE");
       



        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
           
            dr["Code"] = Convert.ToInt32(codeData[i]);
            dr["Name"] = Convert.ToString(NameData[i]);
            dr["Payment"] = Convert.ToDecimal(PaymentData[i]);
            dr["Receipt"] = Convert.ToDecimal(ReceiptData[i]);
            dr["Cheque"] = Convert.ToString(ChequeData[i]);
            dr["Debit"] = Convert.ToDecimal(DebitData[i]);
            dr["Credit"] = Convert.ToDecimal(CreditData[i]);
           
            dr["Narr"] = Convert.ToString(NarrData[i]);
            dr["CCODE"] = "";
           
            dt.Rows.Add(dr) ;

        }



        status = new VoucherBLL().InsertUpdate(objTrans, dt,BankId,multisingle);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Voucher = objTrans

        };
        return ser.Serialize(JsonData);
        
    }




    [WebMethod]
    public static string Update(Int64 GrnNo, string GrnDate, string BillNo, string BillDate, string GrNo, string GrDate,
        string VehNo, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
        string freeArr, string dis1Arr, string dis2Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr,
        string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, string arrExcise, string arrMargin, decimal SurchrgAmt, string PurType, decimal ExciseAmt, string ExciseAmtArr, string HsnArr
        )
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;
        if (roles.Count() == 0)
        {
            status = -11;
        }
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Purchase objPurchase = new Purchase()
        {
            GrnNo = Convert.ToInt32(GrnNo),
            GRNDate = Convert.ToDateTime(GrnDate),
            BillNo = BillNo,
            BillDate = Convert.ToDateTime(BillDate),
            GRNo = GrNo,
            GRDate = Convert.ToDateTime(GrDate),
            VehNo = VehNo,
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3P = dis3per,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            SurchrgAmt = SurchrgAmt,
            TaxAmt = taxamt,
            RawChk = "PUR",
            Purtype = PurType,
            Tcs_AccCode = "test",
            ExciseAmt = ExciseAmt,
            BranchId = Branch,
            UserNo = UserNo,

        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
        string[] freeData = freeArr.Split(',');
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');
        string[] Excisedata = arrExcise.Split(',');
        string[] Margindata = arrMargin.Split(',');
        string[] ExciseAmtdata = ExciseAmtArr.Split(',');
        string[] HsnArrdata = HsnArr.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("GrnNo");
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("Excise");
        dt.Columns.Add("ExciseAmt");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
        dt.Columns.Add("ItemMargin");
        dt.Columns.Add("ItemBasRateWTax");
        dt.Columns.Add("surval");
        dt.Columns.Add("hsn");
        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");



        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["GrnNo"] = Convert.ToInt16(1);
            dr["OrderNo"] = "0";
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = dis3per;
            dr["Excise"] = Convert.ToDecimal(Excisedata[i]);
            dr["ExciseAmt"] = Convert.ToDecimal(ExciseAmtdata[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);
            dr["Free"] = Convert.ToDecimal(freeData[i]);
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);
            dr["ItemMargin"] = Convert.ToDecimal(Margindata[i]);
            dr["ItemBasRateWTax"] = Convert.ToDecimal(dis2Data[i]);
            dr["surval"] = Convert.ToDecimal(dis2Data[i]);
            dr["hsn"] = Convert.ToString(HsnArrdata[i]);
            dt.Rows.Add(dr);

        }

        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }


        status = new PurchaseBLL().InsertUpdate(objPurchase, dt, dt1);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Purchase = objPurchase

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindVoucherDetail(int pid,string VType)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int cntr = 0;
        string serviceData = new VoucherBLL().GetVoucherDetail(pid,VType,Branch, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
            Counter = cntr

        };
        return ser.Serialize(JsonData); 
    }



    [WebMethod]
    public static string GetBank(Int32 pid)
    {


        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
       
        string rtnval = "1";
        Connection con1 = new Connection();
        SqlConnection con = new SqlConnection(con1.sqlDataString);
        SqlCommand cmd = new SqlCommand("select CCode as [Code],CName as [Description],NARR as [Narration],Chq_No as [ChequeNo],case when TR_DRCR= 'DR' then TR_Amount else 0 end as [Payment],case when TR_DRCR= 'CR' then TR_Amount else 0 end as [Receipt] from Acc_Trans where Vouch_No="+pid+" and cash_bankcode <> '' and BranchId = "+ Branch + " ", con);
        DataTable dt = new DataTable();
        dt.Clear();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            rtnval = Convert.ToString(dt.Rows[0]["Code"].ToString());
        }

 

        return rtnval;
    }









}

