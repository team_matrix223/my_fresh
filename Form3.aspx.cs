﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBranches();
            BindPos();
        }
       
    }


    public void BindPos()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        ddlPos.DataSource = dt;
        ddlPos.DataTextField = "title";
        ddlPos.DataValueField = "posid";
        ddlPos.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose POS--";
        li1.Value = "0";
        ddlPos.Items.Insert(0, li1);
    }




    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string GetOrdersBYTax(DateTime DateFrom, DateTime DateTo, string tax, int Branch,int POS)
    {
        List<Bill> lstBills = new BillBLL().GetOrderBillsByTax(DateFrom, DateTo, tax, Branch, POS);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills
        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetGroupsBYTaxID(Int32  tax)
    {
        List<Group > lstGroups = new GroupBLL ().GetGroupsByTax( tax);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            GroupsList = lstGroups
        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetGroupsBYTaxIDTaxTo(Int32 tax)
    {
        List<Group> lstGroups = new GroupBLL().GetGroupsByTaxToTax(tax);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            GroupsList = lstGroups
        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string GetHsnBYGroup(string[] arr_grpno)
    {

        DataTable dt = new DataTable();

        dt.Columns.Add("grp_no");
        for (int i = 0; i < arr_grpno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["grp_no"] = arr_grpno[i];
            dt.Rows.Add(dr);
        }
        List<HSNCodeEntities> lstHsn = new HSNBLL().GetHsnByGroup(dt);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            HsnList = lstHsn
        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static void insert_bill_bytax(string[] arr_billno, string fromtax, string totax,int POS)
    {
        DataTable dt = new DataTable();
        dt.Columns.Add("bill_no");
        for (int i = 0; i < arr_billno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["bill_no"] = arr_billno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_insertbill_bytax", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromtax", fromtax);
            cmd.Parameters.AddWithValue("@totax", totax);
            cmd.Parameters.AddWithValue("@bill_no", dt);
           // cmd.Parameters.AddWithValue("@POS", POS);
            cmd.CommandTimeout = 5000;
            cmd.ExecuteNonQuery();

        }

    }


    [WebMethod]
    public static void updategrpsbytax( string[] arr_grpno,string convertto)
    {
        DataTable dt = new DataTable();
       
        dt.Columns.Add("grp_no");
        for (int i = 0; i < arr_grpno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["grp_no"] =  arr_grpno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("updategrpsbytax", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@convertto", convertto);
            //cmd.Parameters.AddWithValue("@totax", totax);
            cmd.Parameters.AddWithValue("@grp_no", dt);
            cmd.ExecuteNonQuery();

        }

    }


    [WebMethod]
    public static void updatehsn(string[] arr_hsn,string[] arr_grpno,string convertto)
    {


        DataTable dt1 = new DataTable();

        dt1.Columns.Add("grp_no");
        for (int i = 0; i < arr_grpno.Length; i++)
        {
            DataRow dr = dt1.NewRow();
            dr["grp_no"] = arr_grpno[i];
            dt1.Rows.Add(dr);
        }


        DataTable dt = new DataTable();

        dt.Columns.Add("hsncode");
        for (int i = 0; i < arr_hsn.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["hsncode"] = arr_hsn[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("updateHsnCode", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@hsncode", dt);
            cmd.Parameters.AddWithValue("@grp_no", dt1);
            cmd.Parameters.AddWithValue("@convertto", convertto);

            cmd.ExecuteNonQuery();

        }

    }




    [WebMethod]
    public static void updategrpsbytaxfrm(string[] arr_grpno, string convertto, DateTime DateFrom, DateTime DateTo, int Branch)
    {
        DataTable dt = new DataTable();

        dt.Columns.Add("grp_no");
        for (int i = 0; i < arr_grpno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["grp_no"] = arr_grpno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("updategrpsbytaxfrm", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@convertto", convertto);
           
            cmd.Parameters.AddWithValue("@grp_no", dt);
            cmd.Parameters.AddWithValue("@FromDate", DateFrom);
            cmd.Parameters.AddWithValue("@ToDate", DateTo);
            cmd.Parameters.AddWithValue("@BranchId", Branch);
            cmd.ExecuteNonQuery();

        }

    }

}