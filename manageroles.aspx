﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageroles.aspx.cs" Inherits="manageroles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/manageusers.css" rel="stylesheet" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   <script language="javascript" type="text/javascript">
       var m_RoleId = 0;

       function ResetControls() {
           m_RoleId = 0;
           var txtTitle = $("#txtTitle");
           var btnAdd = $("#btnAdd");
           var btnUpdate = $("#btnUpdate");
           txtTitle.focus();
           $("#chkIsActive").prop("checked", "checked");
           txtTitle.val("");
           txtTitle.focus();

           btnAdd.css({ "display": "block" });
           btnAdd.html("Add Role");

           btnUpdate.css({ "display": "none" });
           btnUpdate.html("Update Role");

           $("#btnReset").css({ "display": "none" });
           $("#hdnId").val("0");
           validateForm("detach");
       }



       function TakeMeTop() {
           $("html, body").animate({ scrollTop: 0 }, 500);
       }

       function RefreshGrid() {
           $('#jQGridDemo').trigger('reloadGrid');

       }

       function InsertUpdate() {

           if (!validateForm("frmCity")) {
               return;
           }
           var Id = m_RoleId;
           var Title = $("#txtTitle").val();
           if ($.trim(Title) == "") {
               $("#txtTitle").focus();

               return;
           }


           var IsActive = false;

           if ($('#chkIsActive').is(":checked")) {
               IsActive = true;
           }

           $.uiLock('');
           $.ajax({
               type: "POST",
               data: '{"RoleId":"' + Id + '", "Title": "' + Title + '","IsActive": "' + IsActive + '"}',
               url: "manageroles.aspx/Insert",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == -1) {

                       alert("Insertion Failed.Role with duplicate name already exists.");
                       return;
                   }

                   if (Id == "0") {
                       ResetControls();

                       jQuery("#jQGridDemo").jqGrid('addRowData', obj.Role.DesgID, obj.Role, "last");

                       alert("Role is added successfully.");
                   }
                   else {
                       ResetControls();

                       var myGrid = $("#jQGridDemo");
                       var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                       myGrid.jqGrid('setRowData', selRowId, obj.Role);
                       alert("Role is Updated successfully.");
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }

       $(document).ready(
       function () {
           BindGrid();

           $("#btnAdd").click(
           function () {


               m_RoleId = 0;
               InsertUpdate();
           }
           );


           $("#btnUpdate").click(
           function () {

               var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
               if ($.trim(SelectedRow) == "") {
                   alert("No Role is selected to Edit");
                   return;
               }

               InsertUpdate();
           }
           );





           $('#txtTitle').focus();
           $('#txtTitle').keypress(function (event) {


               var keycode = (event.keyCode ? event.keyCode : event.which);

               if (keycode == '13') {

                   InsertUpdate();
               }


           });


           $("#btnReset").click(
           function () {

               ResetControls();

           }
           );


       }
       );

</script>


<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col manage-role-right-col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Roles</h3>
                        </div>
                   <!--     <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                               <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div> 
                            </div>
                        </div>-->
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel edit_manage_user">
                        <div class="x_title ">
                            <h2>Add/Edit Role</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed manageuser_leftform" >
                     
                   
                     <tr><td class="headings">Title:</td><td>  <input type="text"  name="txtTitle" maxlength="50" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" /></td></tr>
                      
                       <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                      
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Role</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" >Update Role</div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" >Cancel</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel edit_manage_user">
                        <div class="x_title">
                            <h2>Manage Roles</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content manage-roles-second-section">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>

     
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

              

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageRoles.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['Role Id', 'Role', 'IsActive'],
                        colModel: [
                                    { name: 'RoleId', key: true, index: 'RoleId', width: 100, stype: 'text', sorttype: 'int', hidden: true },

                                    { name: 'Title', index: 'Title', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },


                                     { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'RoleId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Roles List",

                        editurl: 'handlers/ManageDesignations.ashx',
                        ignoreCase: true,
                        toolbar: [true, "top"],


                    });


                    var $grid = $("#jQGridDemo");
                    // fill top toolbar
                    $('#t_' + $.jgrid.jqID($grid[0].id))
                        .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                    $("#globalSearchText").keypress(function (e) {
                        var key = e.charCode || e.keyCode || 0;
                        if (key === $.ui.keyCode.ENTER) { // 13
                            $("#globalSearch").click();
                        }
                    });
                    $("#globalSearch").button({
                        icons: { primary: "ui-icon-search" },
                        text: false
                    }).click(function () {
                        var postData = $grid.jqGrid("getGridParam", "postData"),
                            colModel = $grid.jqGrid("getGridParam", "colModel"),
                            rules = [],
                            searchText = $("#globalSearchText").val(),
                            l = colModel.length,
                            i,
                            cm;
                        for (i = 0; i < l; i++) {
                            cm = colModel[i];
                            if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                                rules.push({
                                    field: cm.name,
                                    op: "cn",
                                    data: searchText
                                });
                            }
                        }
                        postData.filters = JSON.stringify({
                            groupOp: "OR",
                            rules: rules
                        });
                        $grid.jqGrid("setGridParam", { search: true });
                        $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                        return false;
                    });







                    $("#jQGridDemo").jqGrid('setGridParam',
                           {
                               onSelectRow: function (rowid, iRow, iCol, e) {
                                   m_RoleId = 0;
                                   validateForm("detach");

                                   var txtTitle = $("#txtTitle");
                                   m_RoleId = $('#jQGridDemo').jqGrid('getCell', rowid, 'RoleId');

                                   


                                   if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                       $('#chkIsActive').prop('checked', true);
                                   }
                                   else {
                                       $('#chkIsActive').prop('checked', false);

                                   }
                                   txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'Title'));
                                   txtTitle.focus();
                                   $("#btnAdd").css({ "display": "none" });
                                   $("#btnUpdate").css({ "display": "block" });
                                   $("#btnReset").css({ "display": "block" });
                                   TakeMeTop();
                               }
                           });



                    var DataGrid = jQuery('#jQGridDemo');
                    DataGrid.jqGrid('setGridWidth', '500');

                    $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                                     {
                                         refresh: false,
                                         edit: false,
                                         add: false,
                                         del: false,
                                         search: false,
                                         searchtext: "Search",
                                         addtext: "Add",
                                     },

                                     {//SEARCH
                                         closeOnEscape: true

                                     }

                                       );



                }





    </script>


</asp:Content>

