﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class index : System.Web.UI.Page
{
    public string DayOpen { get { return Request.QueryString["DayOpen"] != null ? Convert.ToString(Request.QueryString["DayOpen"]) : ""; } }
    public string DashIMG;
    protected void Page_Load(object sender, EventArgs e)
    {
        //  DirectoryInfo di = new DirectoryInfo(Server.MapPath("/images/DashImg/"));
        //FileInfo fi = di.GetFiles()[0];
        //this.DashIMG = "/images/DashImg/"+ fi;

        if (DayOpen == "Close")
        {
            Response.Write("<script>alert('Day is Not Opened.Please Open Your Day')</script>");

        }
        GetImage();
    }

    public void GetImage() {
        Connection conn = new Connection();
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        using (SqlConnection con=new SqlConnection (conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select dash_img from Mastersetting_Basic where branchid="+ Branch + "", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                Image1.ImageUrl = "./images/DashImg/"+rd["dash_img"].ToString() +"";
            }
        }



    }

    [WebMethod]
    public static string CheckUserSession(string pagename)
    {
        string rtnval = "1";
        UserActivityLog.SetActivityLog("OUT", pagename);
        return rtnval;
    }
}