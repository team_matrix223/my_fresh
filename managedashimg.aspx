﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managedashimg.aspx.cs" Inherits="managedashimg" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <script>


        $(document).ready(function () {
            $("#spnimg").click(function () {

                $("#FileUpload1").click();

            });
        });
    </script>
<style>
    .x_panel{
        min-height:520px;
        max-height:520px;
        overflow:auto;
    }
    @media(max-width:480px)
    {
        .upload_img img {
	        width: 95px !important;
        }
    }
    @media(max-width:991px)
    {
        .x_panel{
            min-height:auto;
            max-height:auto;
        }
    }
     @media (min-width:992px) and (max-width:1200px)
    {
        .x_panel{
            min-height:440px;
            max-height:440px;
        }
    }
</style>
<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
    <link href="css/customcss/setup.css" rel="stylesheet" />
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                    <%--    <div class="title_left">
                            <h3>Dash\Login Image</h3>
                        </div>--%>

                    </div>
                    <div class="clearfix"></div>
                     
                    <div class="x_title setup_title">
                            <h2>Add/Edit Dash/Login Image</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                    <div class="x_panel">
                        
                        <div class="x_content upload_img">
                                <asp:FileUpload ID="FileUpload1" runat="server" style="display:none;"  ClientIDMode="Static"/>
                                <span id="spnimg"><img alt="" src="images/upload.png" /></span>
                                <span><asp:Button ID="btnsubmit" class="setup_btn" runat="server" Text="Upload" OnClick="Button1_Click"/></span>
                        </div>
                              <asp:Label ID="lblmsg" runat="server" ForeColor="Red"></asp:Label>
                    </div>

                    </div>
            </div>

    <!-- footer content -->
          <footer>
              <uc1:ucfooter ID="ucfooter1" runat="server" />
          </footer>
    <!-- /footer content -->
 
</form>
</asp:Content>

