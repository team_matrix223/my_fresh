﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="testgrid.aspx.cs" Inherits="testgrid" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
        runat="server" AutoGenerateColumns="false">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ItemStyle-Width="30" />
            <asp:BoundField DataField="Name" HeaderText="Name" ItemStyle-Width="150" />
            <asp:BoundField DataField="Country" HeaderText="Country" ItemStyle-Width="150" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:TextBox runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:DropDownList runat="server">
                        <asp:ListItem Text="text1" />
                        <asp:ListItem Text="text2" />
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox Text="text" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField>
                <ItemTemplate>
                    <asp:RadioButton Text="text" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
        </div>
    </form>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
   <script type="text/javascript">
	   var lastsel2 = '';
	   var myCheckbox = {
		   edittype: 'checkbox',
		   editoptions: {
			   value: "True:False"
		   },
		   formatter: "checkbox"
	   };
	   var serverResponse = {
		   "page": 1,
		   "total": 1,
		   "records": 23,
		   "rows": [{
			   "id": "1471676",
			   "cell": ["1471676", 133172, 4566, "GSH209C26D", 19, "GSH 209 C26D", "209 C26D", 19, "1", 19, null, "B7", true]
		   }, {
			   "id": "1471683",
			   "cell": ["1471683", 133172, 6056, "S88BL", 84, "S88 BLACK", "S88BL\/S", 84, "1", 84, null, "B9", true]
		   }, {
			   "id": "1471654",
			   "cell": ["1471654", 133172, 1881, "6216-1\/8X3\/8BLK 1", 45, "6216 1\/8in X 3\/8in BLACK", "6216 1\/8in X 3\/8in BLACK", 45, "2", 45, null, "C1", true]
		   }, {
			   "id": "1471713",
			   "cell": ["1471713", 133172, 587, "1546SC26D", 2, "1546S C26D", "1546S C26D", 2, "2", 2, null, "C5", false]
		   }, {
			   "id": "1471719",
			   "cell": ["1471719", 133172, 2674, "8215LNBC26D", 6, "8215 LNB C26D", "8215 LNB C26D", 6, "2", 6, null, "D19, D3, D9, W11", false]
		   }, {
			   "id": "1471723",
			   "cell": ["1471723", 133172, 56270, "8271LNBC26D24V", 4, "8271 LNB C26D 24V", "8271 LNB L\/C C26D 24V", 4, "3", 4, null, "D20", false]
		   }, {
			   "id": "1471652",
			   "cell": ["1471652", 133172, 1621, "4875C26D", 2, "4875 L\/C C26D", "4875 L\/C C26D", 2, "3", 2, null, "G10", false]
		   }, {
			   "id": "1471696",
			   "cell": ["1471696", 133172, 154839, "1078", 7, "1078", "GEC1078-N", 7, "3", 7, null, "G10", false]
		   }, {
			   "id": "1471734",
			   "cell": ["1471734", 133172, 227708, "1431OEN", 5, "1431 O EN", "1431 O EN", 5, "4", 5, null, "G4, P13, P17, W15", false]
		   }, {
			   "id": "1471678",
			   "cell": ["1471678", 133172, 4615, "GSH401C26D", 4, "GSH 401 C26D", "8402 C26D", 4, null, 4, null, "G8", false]
		   }, {
			   "id": "1471729",
			   "cell": ["1471729", 133172, 210664, "QC-C1500P", 6, "QC-C1500P  15FT HARNESS X 8PIN\/4PIN SGL CONNECTOR", "QC-C1500P", 6, "4", 6, null, "G8, H9", false]
		   }, {
			   "id": "1471684",
			   "cell": ["1471684", 133172, 6662, "W24S 36", 1, "W24S X 36in AL", "W24S X 36in AL", 1, "5", 1, null, "J30, J31, J42", false]
		   }, {
			   "id": "1471650",
			   "cell": ["1471650", 133172, 564, "150C26D", 2, "150 C26D", "150 C26D", 2, "5", 2, null, "L190", false]
		   }, {
			   "id": "1471681",
			   "cell": ["1471681", 133172, 95041, "GSH403C26D", 2, "GSH403 C26D", "GSH403 C26D", 2, "5", 2, null, "L27, L28, L30", false]
		   }, {
			   "id": "1471736",
			   "cell": ["1471736", 133172, 224590, "TA7144545CC4C26D", 4, "TA714 CC4  4.5 X 4.5 C26D", "TA714 4.5 X 4.5 CC4 C26D", 4, "6", 4, null, "L292", false]
		   }, {
			   "id": "1471704",
			   "cell": ["1471704", 133172, 210663, "QC-C300P", 6, "QC-C300P 38IN HARNESS X 8PIN\/4PIN SGL CONNECTOR", "QC-C300P  38 INCH", 6, "6", 6, null, "L39", false]
		   }, {
			   "id": "1471711",
			   "cell": ["1471711", 133172, 173852, "TA714454C15", 24, "TA714 4.5 X 4 C15", "TA714 4.5 X 4 C15", 24, "6", 24, null, "L450, L460, P6", false]
		   }, {
			   "id": "1471656",
			   "cell": ["1471656", 133172, 2238, "7253AL", 3, "7253 AL", "7253 SP28", 3, "7", 3, null, "L482, L483", false]
		   }, {
			   "id": "1471712",
			   "cell": ["1471712", 133172, 517, "1431MCOENLH", 1, "1431 O MC EN LH", "1431 O EN MC LH", 1, "7", 1, null, "P12", false]
		   }, {
			   "id": "1471680",
			   "cell": ["1471680", 133172, 139667, "P1703BCHCOVAL", 2, "P1703BC H - COV 689", "1703BCH - COV 689", 2, "7", 2, null, "P2, P3", false]
		   }, {
			   "id": "1471728",
			   "cell": ["1471728", 133172, 279053, "RTS88-03-105NHO689", 2, "RTS88 - 105 NHO 689 5MM EXTENSION", "RTS88 105 SZ3 NHO 689 5MM", 2, "8", 2, null, "P9", false]
		   }, {
			   "id": "1471689",
			   "cell": ["1471689", 133172, 98732, "GSH80A8X34.5C32DTAPE", 3, "GSH80A 8in X 34.5in TAPE C32D", "GSH80A 8 X 34.5 C32D TAPE", 3, "8", 3, null, "U6, U7, U8", true]
		   }, {
			   "id": "1471735",
			   "cell": ["1471735", 133172, 302923, "GSH80A8X38.5C32DTAPE", 1, "GSH80A 8in X 38.5in TAPE C32D", "GSH80A 8 X 38.5 C32D TAPE", 1, "8", 1, null, "U6, U8", true]
		   }]
	   };
	   $(function () {
		   "use strict";
		   var list = $("#list");
		   list.jqGrid({
			   url: "/echo/json/", // use JSFiddle echo service
			   datatype: "json",
			   mtype: 'POST',
			   postData: {
				   json: JSON.stringify(serverResponse) // needed for JSFiddle echo service
			   },
			   colNames: ['', 'Carton #', 'Total Packed', 'Total Picked', 'Ordered Qty', 'Item #', 'Location', 'Desc 2', 'Vendor Part #', 'id', 'PackPK', 'IPK', 'PackHierarchySequence', 'Group Printing'],
			   colModel: [{
				   name: "PackPK",
				   align: "center",
				   index: "o.PackPK",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.1",
				   width: 100,
				   search: false
			   }, {
				   name: "PackCartonNo",
				   align: "left",
				   index: "o.PackCartonNo",
				   editable: true,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.8",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   },
				   cellattr: function (rowId, tv, rawObject, cm, rdata) {
					   if (rdata.PackCartonNo == null || rdata.PackCartonNo == '') {
						   return ' class="requiredCell"';
					   } else {
						   return '';
					   }
				   }
			   }, {
				   name: "TotalPacked",
				   align: "left",
				   index: "o.TotalPacked",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.4",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "TotalPicked",
				   align: "left",
				   index: "o.TotalPicked",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.7",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "PackOrderQty",
				   align: "left",
				   index: "o.PackOrderQty",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.9",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "IItemNumID",
				   align: "left",
				   index: "o.IItemNumID",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.3",
				   width: 300,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "PackBinNum",
				   align: "left",
				   index: "o.PackBinNum",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.11",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "PackDesc2",
				   align: "left",
				   index: "o.PackDesc2",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.5",
				   width: 300,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "IVendorItemNum",
				   align: "left",
				   index: "o.IVendorItemNum",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.6",
				   width: 300,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   }
			   }, {
				   name: "id",
				   align: "left",
				   index: "o.id",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.0",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   },
				   hidden: true
			   }, {
				   name: "PackPK",
				   align: "left",
				   index: "o.PackPK",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.1",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   },
				   hidden: true
			   }, {
				   name: "IPK",
				   align: "left",
				   index: "o.IPK",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.2",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   },
				   hidden: true
			   }, {
				   name: "PackHierarchySequence",
				   align: "left",
				   index: "o.PackHierarchySequence",
				   editable: false,
				   formatoptions: {
					   disabled: false
				   },
				   jsonmap: "cell.10",
				   width: 100,
				   searchoptions: {
					   sopt: ["cn", "eq", "ne", "lt", "le", "gt", "ge", "bw", "ew", "nc"]
				   },
				   hidden: true
			   }, {
				   name: "PackGroupPrinting",
				   align: "center",
				   index: "o.PackGroupPrinting",
				   editable: false,
				   formatoptions: {
					   disabled: true
				   },
				   jsonmap: "cell.12",
				   width: 100,
				   template: myCheckbox,
				   search: false
			   }],
			   jsonReader: {
				   repeatitems: false
			   },
			   width: 1600,
			   height: 600,
			   gridview: true,
			   autoencode: false,
			   shrinkToFit: false,
			   pager: '#pager',
			   rowNum: 50,
			   rowTotal: 1000000,
			   rowList: [50, 100, 500, 1000],
			   rownumWidth: 40,
			   viewrecords: true,
			   caption: '',
			   sortname: 'o.PackBinNum',
			   sortorder: 'asc',
			   sortable: true,
			   footerrow: true,
			   iconSet: "fontAwesome",
			   autoResizing: {
				   compact: true
			   },
			   loadonce: false,
			   onSelectRow: function (rowid, status, e) {
				   var $self = $(this),
					   $td = $(e.target).closest("tr.jqgrow>td"),
					   p = $self.jqGrid("getGridParam"),
					   cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null,
					   cmName = cm !== 0 && cm.editable ? cm.name : 'PackCartonNo';
				   if (rowid) {
					   if (lastsel2 != "" && typeof lastsel2 !== 'undefined') {
						   $self.jqGrid('saveRow', lastsel2);
					   }
					   lastsel2 = rowid;
				   }
				   $self.jqGrid('editRow', rowid, {
					   keys: true,
					   focusField: 'PackCartonNo',
				   });
				   return true;
			   },
		   });
		   list.jqGrid('navGrid', '#pager', {
			   del: false,
			   add: false,
			   edit: false
		   }, {}, {}, {}, {
			   multipleSearch: true
		   });
		   list.jqGrid('filterToolbar', {
			   searchOperators: true,
			   stringResult: true,
			   sopt: ['cn', 'eq', 'ne', 'lt', 'le', 'gt', 'ge', 'bw', 'ew', 'nc'],
			   searchOnEnter: true,
			   beforeSearch: function () {
			   }
		   }).navButtonAdd("#pager", {
			   id: "firstRecord",
			   title: "First Record",
			   caption: "",
			   buttonicon: "fa-step-backward",
			   onClickButton: function () {
				   var selectedRow = list.getGridParam('selrow');
				   if (selectedRow == null) return;
				   var ids = list.getDataIDs();
				   var index = list.getInd(selectedRow);
				   if (ids.length < 2) return;
				   index = 1;
				   //if (index > ids.length) index = 1;
				   list.setSelection(ids[index - 1], true);
			   }
		   }).navButtonAdd("#pager", {
			   id: "previousRecord",
			   title: "Previous Record",
			   caption: "",
			   buttonicon: "fa-chevron-left",
			   onClickButton: function () {
				   var selectedRow = list.getGridParam('selrow');
				   if (selectedRow == null) return;
				   var ids = list.getDataIDs();
				   var index = list.getInd(selectedRow);
				   if (ids.length < 2) return;
				   index--;
				   //if (index > ids.length) index = 1;
				   list.setSelection(ids[index - 1], true);
			   }
		   }).navButtonAdd("#pager", {
			   id: "nextReocrd",
			   title: "Next Record",
			   caption: "",
			   buttonicon: "fa-chevron-right",
			   onClickButton: function () {
				   var selectedRow = list.getGridParam('selrow');
				   if (selectedRow == null) return;
				   var ids = list.getDataIDs();
				   var index = list.getInd(selectedRow);
				   if (ids.length < 2) return;
				   index++;
				   //if (index > ids.length) index = 1;
				   list.setSelection(ids[index - 1], true);
			   }
		   }).navButtonAdd("#pager", {
			   id: "lastReocrd",
			   title: "Last Record",
			   caption: "",
			   buttonicon: "fa-step-forward",
			   onClickButton: function () {
				   var selectedRow = list.getGridParam('selrow');
				   if (selectedRow == null) return;
				   var ids = list.getDataIDs();
				   var index = list.getInd(selectedRow);
				   if (ids.length < 2) return;
				   index = ids.length;
				   list.setSelection(ids[index - 1], true);
			   }
		   });
		   list.jqGrid('gridResize');
		   list.jqGrid('bindKeys');
	   });
	   $(document).keydown(function (e) {
		   switch (e.which) {
			   case 40: // down
				   var list = $('#list'),
					   $td = $(e.target).closest("tr.jqgrow>td"),
					   p = list.jqGrid("getGridParam"),
					   cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
				   var cmName = cm !== 0 && cm.editable ? cm.name : 'PackCartonNo';
				   var selectedRow = list.jqGrid('getGridParam', 'selrow');
				   if (selectedRow == null) return;
				   var ids = list.getDataIDs();
				   var index = list.getInd(selectedRow);
				   if (ids.length < 2) return;
				   index++;
				   list.setSelection(ids[index - 1], false, e);
				   list.jqGrid('saveRow', selectedRow);
				   //lastsel2 = selectedRow;
				   list.jqGrid('editRow', ids[index - 1], {
					   keys: true,
					   focusField: cmName
				   });
				   e.preventDefault();
				   break;

			   default:
				   return;
		   }
	   });

   </script>
 
</body>
</html>
