﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="placeordernewtemplate.aspx.cs" Inherits="placeorder" %>

<%@ Register src="Templates/OrderTemplates/newOrderBooking.ascx" tagname="OrderBooking" tagprefix="uc1" %>
<%@ Register src="Templates/OrderTemplates/newOrderDispatch.ascx" tagname="OrderDispatch" tagprefix="uc2" %>
<%@ Register src="Templates/AddCashCustomer.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


<style type="text/css">
    

    .x_title
    {
        margin-bottom: 5px;
        padding: 0px;
    }

    .x_panel
    {
        padding-top: 0px;
        margin-bottom: 5px;
    }

    .x_content
    {
        margin-top: 2px;
    }
</style>

    <form id="form1" runat="server">
     <asp:HiddenField ID="hdnDate" runat="server"/>
    <asp:HiddenField ID="hdnRoles" runat="server"/>
  
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="css/customcss/PlaceOrder.css" rel="stylesheet" />
	 <link href="css/customcss/placeordernewtemplate.css" rel="stylesheet" />
	 <link href="css/customcss/NewOrderDispatch.css" rel="stylesheet" />
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    
    <script src="Scripts/knockout-3.0.0.js"></script>
    <script src="ViewModel/OrderViewModel.js"></script>
    <script src="ViewModel/PopUp.js" type="text/javascript"></script>
    <script src="Scripts/knockout.validation.js" type="text/javascript"></script>
    <script src="js/jquery.uilock.js" type="text/javascript"></script>

             <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <style type="text/css">
        #tbSearchResult
        {
            padding-left: 0px;
        }

            #tbSearchResult li
            {
                padding: 5px;
                border-bottom: 1px solid #ddd;
                list-style-type: none;
                cursor: pointer;
            }

                #tbSearchResult li:hover
                {
                    background: seashell;
                    padding: 5px;
                    border-bottom: 1px solid #ddd;
                    list-style-type: none;
                    cursor: pointer;
                }

        #tbCustSearchResult
        {
            padding-left: 0px;
        }

            #tbCustSearchResult li
            {
                padding: 5px;
                border-bottom: 1px solid #ddd;
                list-style-type: none;
                cursor: pointer;
            }

                #tbCustSearchResult li:hover
                {
                    background: seashell;
                    padding: 5px;
                    border-bottom: 1px solid #ddd;
                    list-style-type: none;
                    cursor: pointer;
                }

        .selected
        {
            background: rgba(0, 0, 0, 0.075);
        }

        .customTextBox
        {
            width: 62px;
            margin-right: 2px;
            margin-left: 2px;
        }

        #tbCal tr td
        {
            padding: 2px;
        }

        .validationMessage
        {
            color: Red;
        }
    </style>
    
        
          
 
<div class="right_col place_order_right_col" role="main">
                <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Order Booking</h3>
                            </div>
                       <!--     <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                   <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                
                    <div class="clearfix"></div>

                    <div class="row place-order-row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel place-roder-x-panel">
                                <div class="x_title">
                                    <h2>ORDERS LIST</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group" style="margin-bottom:10px;">

                                 <div class="form-group">
                                
                                <table style="width:450px;margin-bottom:10px" class="place-order-date-table">
                                <tr><td>Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                
                                </td><td>
                                
                                </td>
                                <td>Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                 
                                </td>
                                <td><div id="btnGo"  class="btn btn-primary btn-small"  >
                                 <i class="fa fa-search"></i>
                                </div></td>
                                </tr>
                                </table>

                <table id="jQGridDemo">
                </table>
                <div id="jQGridDemoPager">
                </div>

                </div>
                                </div>


                                   <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12 plc-ord-btn">
                                                <button type="button" data-bind="click:$root.NewOrder" class="btn btn-primary">
                                                <i class="fa fa-external-link"></i>
                                                New</button>
                                                <button type="button" id="btnEdit"  style="display:none" data-bind="click:EditOrder" class="btn btn-danger">
                                                <i class="fa fa-edit m-right-xs"></i>
                                                Edit</button>
                                                <button type="button" id="btnDispatch" class="btn btn-success">
                                                <i class="fa fa-send"></i>
                                                Dispatch</button>


                                                 <button type="button" id="btnrecadv" class="btn btn-success">
                                                <i class="fa fa-send"></i>
                                                Receive Advance</button>

                                                 <button type="button" id="btnDelete" style="display:none" class="btn btn-danger">
                                                <i class="fa fa-trash m-right-xs"></i>
                                                Cancel</button>


                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>


 <div id="packageDialog" style="display:none" title="Order Info">
  <table width="100%" cellpadding="3" class="order-info-table-package-dialog">
                     <tr>
                     <td align="center">
                   
                     </td>
                     </tr>

                     <tr>
                     <td>
                     <table style="width:100%">
                     <tr>
                     <td valign="top"  >
                     <table  cellpadding="3" style="border:solid 1px silver;height: 140px;width:100%">
                         <tr style="background-color:#E6E6E6;font-weight:bold;height:10px;" class="order-infor-package-tr">
                         <td colspan="100%">Order Information</td>
                         </tr>


                     <tr>

                     <td>
                     <table cellpadding="0" cellspacing="0" class="order-infor-package-tbl-data">

                          <tr>
                   
                     <td>Order No:</td><td style="padding-left:10px" ><input type="text"  readonly ="readonly" id="txtOrderNoadv"  class="form-control input-small" value="Auto" style="width:130px;height:25px;background-color:White"  /></td>
                     
                   
                     <td>Order Date:</td><td style="padding-left:10px" ><input type="text"    class="form-control input-small" style="width:130px;height:25px;background-color:White" readonly="readonly"  id="txtBillDate"/></td>
                     </tr>

                     <tr><td>Order Value</td><td style="padding-left:10px"><input disabled="disabled" type="text" id="txtOrderValAdv" class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                   
                   
                     <td>Advance Paid:</td><td style="padding-left:10px" ><input disabled="disabled" type="text" id="txtAdvancePaidAdv" class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                     </tr>


                     <tr>
                   
                     <td>Balance:</td><td style="padding-left:10px" ><input type="text" disabled="disabled" id="txtBalAdv"  class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                     
                   
                     <td></td><td></td>
                     </tr>
                     
                      <tr>
                   
                     <td>Received Amt:</td><td style="padding-left:10px" ><input type="text" id="txtRecAdv"  class="form-control input-small"  style="width:130px;height:25px;background-color:White"  /></td>
                     
                   
                     <td>PayMode:</td>
                     <td style="padding-left:10px" ><select id="ddlpaymode" style="height:30px;width:130px"></select></td>
                     </tr>

                     </table>


                     <table>
                     <tr><td>  <div id="btnSavePrint"  class="btn btn-primary btn-small" style="width:100px;" >Save</div></td>
                  
                     <td>  <div id="btnCancel" style="width:100px;" class="btn btn-danger btn-small"  >Cancel</div></td></tr>
                   
                   
                     </table>
                     </td>
                     </tr>

                    
                    
                    
                    
                    </table>
                    
                    </td>
                    </tr>
                   
                     </table>
                     
                     </td>
                     
                     
                

                     </tr>
                      </table>
                     
                <table id="jQGridDemoAdv">
                </table>
                <div id="jQGridDemoPagerAdv">
                </div>
                <table>
                     <tr><td>  <div id="btnDeleteAdv"  class="btn btn-danger btn-small" style="width:100px;" >Delete</div></td></tr>
                   
                   
                     </table>
                    


               

                 
  
  </div> 
                     

<div class="row order_dialog" id="orderDialog" style="display:none" title="<%= Request.Cookies[Constants.BranchName].Value %>">
<uc1:OrderBooking ID="ucOrderBooking" runat="server" />
</div>




<div class="row" id="dispatchDialog" style="display:none" title="<%= Request.Cookies[Constants.BranchName].Value %>">
 <uc2:OrderDispatch ID="ucOrderDispatch" runat="server" /> 
    

</div>


<div  id="dvTaxDenomination" style="float:left;display:none;  background-color: rgb(76, 73, 77);color:white;bottom:0px;right:0px;border:solid 1px silver;border-radius:10px;padding:5px;-webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
          box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
 <table>
 
 <tr><td colspan="2">
 <table><tr><td></td><td>Tax Denomination:</td>
 <td></td></tr></table></td></tr>
 <tr><td>Tax</td><td>VatAmt</td><td style="width:70px">Vat</td><td>SurChg</td></tr>
 <tr><td id="gridTax" colspan ="100%"><asp:Repeater ID="gvTax" runat ="server">
 <ItemTemplate>
 
 <tr><td ><div  name="tax"><%#Eval("Tax_Rate") %></div></td><td ><div myid='amt_<%#Eval("Tax_Rate") %>' name="amt">0.00</div></td><td><div myid='vat_<%#Eval("Tax_Rate") %>' name="vat">0.00</div></td><td ><div myid='sur_<%#Eval("Tax_Rate") %>' name="sur">0.00</div></td></tr>
 </ItemTemplate>
 </asp:Repeater></td></tr>
 


 </table>
  
 </div>


<div class="row" id="CustomerDialog" style="display:none">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>

<%--<div id="dvCustomerSearch"></div>--%>
                    
                  
                        <script type="text/javascript">  
                               function BindPaymodes() {


                                $.ajax({
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    url: "placeorder.aspx/GetPayModes",
                                    data: {},
                                    async: false,
                                    dataType: "json",
                                    success: function (msg) {


                                        var obj = jQuery.parseJSON(msg.d);



										var html = "<option value='0'>Cash</option><option value='1'>Credit Card</option><option value='2'>Credit</option>";

                                        for (var i = 0; i < obj.APaymodes.length; i++) {

                                            html = html + "<option value='" + obj.APaymodes[i]["Id"] + "'>" + obj.APaymodes[i]["Name"] + "</option>";
                                        }
                                        $("#ddlPrefix").html(html);

                                       

										$("#ddlpaymode").html(html);

                                      
                                      

                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function () {

                                    }
                                });

                            }



                            function ApplyRoles(Roles) {


                                $("#<%=hdnRoles.ClientID%>").val(Roles);
                            }



                            var CustomerID = 0;
                            var TotalVat = 0;
                            var TotalAmount = 0;
                            var m_OrderNo = 0;
                            var ProductCollection = [];
                            var RoundBillAmount = false;



                            function clsProduct() {

                                this.ItemCode = 0;
                                this.ItemName = 0;
                                this.Qty = 0;
                                this.DispatchdQty = 0;
                                this.Weight = 0;
                                this.DispatchQty = 0;
                                this.Rate = 0;
                                this.Amount = 0;
                                this.MRP = 0;
                                this.TaxPer = 0;
                                this.Surcharge = 0;
                                this.SurchargeAmt = 0;
                                this.Vat = 0;
                            }

                            function QtyChange(counterId) {
								$("#txtQtyV").val(ProductCollection[counterId]["Qty"]);
                                $("#txtWeightV").val(ProductCollection[counterId]["Weight"]);
								$("#txtDispatchedV").val(ProductCollection[counterId]["DispatchdQty"]);
                                var Qty = ProductCollection[counterId]["Qty"];
                                var Disptched = ProductCollection[counterId]["DispatchdQty"];

                                var DQty = $("#txtQty" + counterId).val();
                                if (DQty > Qty - Disptched) {
                                    DQty = Qty - Disptched;

                                }

                                $("#txtQty" + counterId).val(DQty);
                                ProductCollection[counterId]["DispatchQty"] = DQty;
                                var ItemCode = ProductCollection[counterId]["ItemCode"];

                                var Rate = ProductCollection[counterId]["Rate"];
                                var m_Weight = ProductCollection[counterId]["Weight"];
                                var Amount = Number(Number(Rate) * Number(DQty) * Number(m_Weight));

                                $("#lblamount" + counterId).text(Amount);
                                ProductCollection[counterId]["Amount"] = Amount;
                                var DisAmt = Number(Amount) * Number($("#txtDiscountPer").val()) / 100;
                                var Afterdis = Number(Amount) - Number(DisAmt);
                               
                                if (BillBasicType == "I") {

                                   
                                    if (ProductCollection[counterId]["Surcharge"] == "0") {
                                        var Tax = (Number(Afterdis) * Number(ProductCollection[counterId]["TaxPer"]) / (100 + Number(ProductCollection[counterId]["TaxPer"])));
                                    }
                                    else {
                                      
                                        var Survalll = ((Number(ProductCollection[counterId]["Surcharge"]) * Number(ProductCollection[counterId]["TaxPer"])) / 100);
                                        
                                        var Tax = (Number(Afterdis) * Number(ProductCollection[counterId]["TaxPer"]) / (100 + Number(ProductCollection[counterId]["TaxPer"]) + Survalll));
                                       
                                    }
                                }
                                else {

                                    var Tax = (Number(Afterdis) * Number(ProductCollection[counterId]["TaxPer"]) / 100);


                                }


                                var Sur = (Number(Tax) * Number(ProductCollection[counterId]["Surcharge"])) / 100;

                             


                                //var Tax = Number(Afterdis) * Number(ProductCollection[counterId]["TaxPer"]) / 100;

                                //var Sur = Number(Tax) * Number(ProductCollection[counterId]["Surcharge"]) / 100;



                              

                                $("#lblTax" + counterId).text((Number(Tax) + Number(Sur)).toFixed(2));
                                ProductCollection[counterId]["SurchargeAmt"] = Number(Sur);
                                ProductCollection[counterId]["Vat"] = (Number(Tax) + Number(Sur)).toFixed(2);
                                commoncalculation();
                                TaxDenomination();


                            }



                            function InsertUpdateDispatch() {

								var Paymode = "";
								AdvanceMode = $("#ddlPrefix").val();

								if (AdvanceMode == "0") {
									Paymode = "Cash";
								}
                                else if (AdvanceMode == "1") {
                                    AdvanceMode = "0";
									Paymode = "CreditCard";
                                }
								else if (AdvanceMode == "2") {
									AdvanceMode = "0";
									Paymode = "Credit";
								}
								else {
									Paymode = "OnlinePayment";
                                }
                               

                                var OrderNo = $("#txtOrderNo").val();
                                var CustomerCode = CustomerID;
                                var DispatchValue = $("#txtBillValue").val();
                                var DiscountPer = $("#txtDiscountPer").val();
                                var Discount = $("#txtDiscountAmt").val();
                                var TaxAmount = $("#txtTotalVat").val();
                                var NetAmount = $("#txtTotalNetAmount").val();
                                var CreditCustomerName = "";
                            
                                var CreditCustomerId = 0;
                                var CreditCardAmount = 0;
                                var CardNumber = "";
                                var CardType = "";
                                var Bank =0;


                                if (NetAmount <= 0) {

                                    alert("Please choose Quantity to Dispatch");
                                    return;
                                }

								var FinalBal = (parseFloat($("#txtOValue").val())-(parseFloat($("#txtAdv").val())));

                               
                                if (parseFloat($("#txtCashRecvd").val()) > parseFloat(FinalBal))
                               // if (parseFloat($("#txtCashRecvd").val()) > parseFloat($("#txtOValue").val()) -(parseFloat($("#txtAdv").val())+parseFloat($("#txtDispatchedAmt").val())))   
                                {
									
									
                                    alert("Payment cannot Exceed Actual Order Value");
                                    $("#txtCashRecvd").val(FinalBal);
                                    alert(FinalBal);
                                   // $("#txtCashRecvd").val((parseFloat($("#txtOValue").val()) - (parseFloat($("#txtAdv").val()) + parseFloat($("#txtDispatchedAmt").val()))).toFixed(2));
                                    return;
                                }
                               
                                if ($("#ddlPrefix").val() == "0") {

                                    if (parseFloat($("#txtTotalNetAmount").val()) > (parseFloat($("#txtAdv").val())  + parseFloat($("#txtCashRecvd").val()))) {

                                        var PendingAmt = parseFloat($("#txtTotalNetAmount").val()) - (parseFloat($("#txtAdv").val())   + parseFloat($("#txtCashRecvd").val()));
                                        alert("Rupees " + PendingAmt + " /- Need to be Paid More.");
                                        $("#txtCashRecvd").focus();
                                        return;
                                    }
                                }
                                else if ($("#ddlPrefix").val() == "2") {

                                    CreditCustomerName = $("#ddlCreditCustomers option:selected").text();
                                    CreditCustomerId = $("#ddlCreditCustomers").val();
                                }


                                else if ($("#ddlPrefix").val() == "1") {


                                    if (parseFloat($("#txtTotalNetAmount").val()) > (parseFloat($("#txtAdv").val()) + parseFloat($("#txtCashRecvd").val()))) {

                                        var PendingAmt = parseFloat($("#txtTotalNetAmount").val()) - (parseFloat($("#txtAdv").val()) + parseFloat($("#txtCashRecvd").val()));
                                        alert("Rupees " + PendingAmt + " /- Need to be Paid More.");
                                        $("#txtCashRecvd").focus();
                                        return;
                                    }



                                    CardNumber = $("#Text14").val();
                                    CreditCardAmount = $("#txtCashRecvd").val();
                                    CardType = $("#ddlType").val();
                                    Bank = $("#ddlBank").val();

                                    
                                    

                                    if (CardNumber.trim() == "") {

                                        alert("Please enter Credit Card Number");
                                        return
                                    }


                                    if (CardType == "") {

                                        alert("Please enter Credit Card Type");
                                        return
                                    }
                                    
                                
                                    if (Bank == "") {

                                        alert("Please enter Credit Card Bank");
                                        return
                                    }
                                   
                                }

                                $.uiLock('');


                           
                                var CrPay = 0;
                                var CrPayLeft = 0;
                                CrPay = $("#txtCashRecvd").val();

                                if (Paymode == "Credit") {
                                
                                    CrPayLeft = Number(NetAmount) - Number(CrPay);
                                }

                                var OnlinePaymnt = 0;
                                if (Paymode == "OnlinePayment") {
									OnlinePaymnt = $("#txtCashRecvd").val();
                                }

                                var ItemCode = [];
                                var ItemName = [];
                                var Weight = [];
                                var DispatchQty = [];
                                var MRP = [];
                                var Amount = [];
                                var Rate = [];
                                var Vat = [];
                                var TaxPer = [];
                                var SurchargePer = [];
                                var Surcharge = [];
                               

                                var TaxDen = [];
                                var VatAmtDen = [];
                                var VatDen = [];
                                var SurDen = [];


                                for (var i = 0; i < ProductCollection.length; i++) {

                                    if (ProductCollection[i]["DispatchQty"] != "0")
                                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                                    ItemName[i] = ProductCollection[i]["ItemName"];
                                    Weight[i] = ProductCollection[i]["Weight"];
                                    DispatchQty[i] = ProductCollection[i]["DispatchQty"];

                                    MRP[i] = ProductCollection[i]["MRP"];
                                    Rate[i] = ProductCollection[i]["Rate"];
                                    Amount[i] = ProductCollection[i]["Amount"];
                                    Vat[i] = ProductCollection[i]["Vat"];
                                    TaxPer[i] = ProductCollection[i]["TaxPer"];
                                    SurchargePer[i] = ProductCollection[i]["Surcharge"];
                                    Surcharge[i] = ProductCollection[i]["SurchargeAmt"];
                                   
                                }


                                 
                                $("div[name='tax']").each(
                function (y) {
                    TaxDen[y] = $(this).html();
                }
                );
                                $("div[name='amt']").each(
                function (z) {
                    VatAmtDen[z] = $(this).html();
                }
                );
                                $("div[name='vat']").each(
                function (a) {
                    VatDen[a] = $(this).html();
                }
                );
                                $("div[name='sur']").each(
                function (a) {
                    SurDen[a] = $(this).html();
                }
                );



                                $.ajax({
                                    type: "POST",
									data: '{"OrderNo": "' + OrderNo + '","CustomerCode": "' + CustomerCode + '","DispatchValue": "' + DispatchValue + '","Discount": "' + Discount + '","DiscountPer": "' + DiscountPer + '","TaxAmount": "' + TaxAmount + '","NetAmount": "' + NetAmount + '","PayMode": "' + Paymode + '","CrPay": "' + CrPay + '","CrPayLeft": "' + CrPayLeft + '","arrItemCode": "' + ItemCode + '","arrItemName": "' + ItemName + '","arrWeight": "' + Weight + '","arrDispQty": "' + DispatchQty + '","arrMRP": "' + MRP + '","arrRate": "' + Rate + '","arrVat": "' + Vat + '","arrAmount": "' + Amount + '","arrTaxPer": "' + TaxPer + '","arrSurchargePer": "' + SurchargePer + '","arrSurcharge": "' + Surcharge + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","CreditCustomerId":"' + CreditCustomerId + '","CreditCustomer":"' + CreditCustomerName + '","CreditCardAmount":"' + CreditCardAmount + '","CardNumber":"' + CardNumber + '","CardType":"' + CardType + '","Bank":"' + Bank + '","AdvanceMode":"' + AdvanceMode + '","OnlinePaymnt":"' + OnlinePaymnt + '"}',

                                    url: "placeorder.aspx/InsertUpdateDispatch",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {


                                        var obj = jQuery.parseJSON(msg.d);

                                        alert("Order Dispatched Successfully");
                                        $("#dispatchDialog").dialog("close");


                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function () {
                                        $.uiUnlock();

                                    }

                                });
                            }


                            function commoncalculation() {
                                TotalAmount = 0;
                                TotalVat = 0;
                                for (var i = 0; i < ProductCollection.length; i++) {

                                    TotalAmount = Number(TotalAmount) + Number(ProductCollection[i]["Amount"]);
                                    TotalVat = Number(TotalVat) + Number(ProductCollection[i]["Vat"]);

                                }

                                $("#txtBillValue").val(TotalAmount);
                                var DiscountAmt = Number(TotalAmount) * Number($("#txtDiscountPer").val()) / 100;
                                $("#txtDiscountAmt").val(DiscountAmt.toFixed(2));
                                if (RoundBillAmount == 1) {
                                    if (BillBasicType == "E") {
                                        $("#txtTotalVat").val(TotalVat.toFixed(2));
                                        $("#txtTotalNetAmount").val(Math.round(((Number(TotalAmount) - Number(DiscountAmt)) + Number(TotalVat)).toFixed(2)));
                                    }
                                    else {
                                        $("#txtTotalVat").val(0);
                                        $("#txtTotalNetAmount").val(Math.round((Number(TotalAmount) - Number(DiscountAmt))));

                                    }
                                }
                                else {
                                    if (BillBasicType == "E") {
                                        $("#txtTotalVat").val(TotalVat.toFixed(2));
                                        $("#txtTotalNetAmount").val(((Number(TotalAmount) - Number(DiscountAmt)) + Number(TotalVat)).toFixed(2));
                                    }
                                    else {
                                        $("#txtTotalVat").val(0);
                                        $("#txtTotalNetAmount").val((Number(TotalAmount) - Number(DiscountAmt)));

                                    }
                                }
                            }

							function BindForAdvance() {

								$("#txtOrderValAdv").val(0);
								$("#txtAdvancePaidAdv").val(0);
								$("#txtBalAdv").val(0);
								$("#txtRecAdv").val(0);



								$.ajax({
									type: "POST",
									data: '{ "OrderNo": "' + m_OrderNo + '"}',
									url: "placeorder.aspx/GetDispatchDetailByOrderNo",
									contentType: "application/json",
									dataType: "json",
									success: function (msg) {


										var obj = jQuery.parseJSON(msg.d);

										$("#txtOrderNoadv").val(obj.Booking.OrderNo);
										$("#txtOrderValAdv").val(obj.Booking.NetAmount);
										if (obj.Booking.Advance > 0) {
											$("#txtAdvancePaidAdv").val(obj.Booking.Advance);
										}
										else {
											$("#txtAdvancePaidAdv").val(0);

										}
										$("#txtBalAdv").val(Number($("#txtOrderValAdv").val()) - (Number($("#txtAdvancePaidAdv").val())));


									},
									complete: function (msg) {

										$("#packageDialog").dialog({
											autoOpen: true,

											width: 600,
											resizable: false,
											modal: false
										});
										BindGridAdv();



									}

								});
							}

         

                            function BindProducts() 
                            {

                                $("#txtBillValue").val(0);
                                $("#txtDiscountAmt").val(0);
                                $("#txtTotalVat").val(0);
                                $("#txtTotalNetAmount").val(0);
                                $("#txtCashRecvd").val(0);

                                ProductCollection = [];

                                $('#tbDispatchProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

                                $.ajax({
                                    type: "POST",
                                    data: '{ "OrderNo": "' + m_OrderNo + '"}',
                                    url: "placeorder.aspx/GetDispatchDetailByOrderNo",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {

                                        ProductCollection = [];
                                        CustomerID = "0";
                                        var tr = "";
                                        var obj = jQuery.parseJSON(msg.d);

                                        $("#txtOrderNo").val(obj.Booking.OrderNo);
                                        CustomerID = obj.Booking.Customer_ID;
                                        $("#txtCname").val(obj.Booking.CustomerName);
                                        if (obj.Booking.Advance > 0) {
                                            $("#txtAdv").val(obj.Booking.Advance);
                                        }
                                        else {
                                            $("#txtAdv").val(0);

                                        }

                                        $("#txtOValue").val(obj.Booking.NetAmount);

                                      
                                        if (obj.Booking.PaymentMode != "") {

                                            $("#ddlPrefix option").removeAttr("selected");
                                            $("#ddlPrefix option[value='" + obj.Booking.PaymentMode + "']").prop("selected", true);
                                            $("#ddlPrefix").prop("disabled", true);

                                            if (obj.Booking.PaymentMode == "Credit")
                                            {

                                                $("#ddlCreditCustomers").html("<option value='" + obj.Booking.CCODE + "'>" + obj.Booking.CNAME + "</option>");

                                                $("#lblCreditCustomerName").html(obj.Booking.CNAME);

                                                $("#txtddlCreditCustomers").val(obj.Booking.CNAME);
                                                $("#dvCreditCust").show();
                                                $("#dvCreditCard").hide();
                                            }
                                            else if (obj.Booking.PaymentMode == "CreditCard")
                                            {
                                                $("#dvCreditCard").show();
                                                $("#dvCreditCust").hide();
                                            }

                                        }
                                        else
                                        {
                                            $("#ddlPrefix option").removeAttr("selected");
                                            $("#ddlPrefix").prop("disabled", false);

                                        }
                                        $("#txtDispatchedAmt").val(obj.Booking.TotalDispatchedAmt);


                                        $("#txtDiscountPer").val(obj.Booking.DisPer);

                                        if (parseFloat(obj.Booking.Advance) > parseFloat(obj.Booking.TotalDispatchedAmt))
                                        {
											$("#txtLastBalance").val(obj.Booking.LeftPayRecd)

                                            //$("#txtLastBalance").val(Number($("#txtOValue").val()) -( Number($("#txtAdv").val()) + Number($("#txtDispatchedAmt").val())));
                                        }
                                        else
                                        {
											$("#txtLastBalance").val(obj.Booking.LeftPayRecd)


                                           // $("#txtLastBalance").val(Number($("#txtOValue").val()) -( Number($("#txtAdv").val()) + Number($("#txtDispatchedAmt").val())));
                                        }
                                        var counterId = 0;
                                        for (var i = 0; i < obj.BookingData.length; i++) {

                                        	tr = tr + "<tr><td style='display:none;'>" + obj.BookingData[i]["Code"] + "</td><td>" + obj.BookingData[i]["Name"] + "</td><td style='display:none;'>"
                                         + obj.BookingData[i]["Weight"] + "</td><td style='display:none;'>" + obj.BookingData[i]["Qty"] + "</td><td style='display:none;'>" + obj.BookingData[i]["DisptchdQty"] + "</td><td>"
                                         + obj.BookingData[i]["Rate"] + "</td><td><input type='text' style ='width:80px' value ='0' id='txtQty"
                                         + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' /></td><td><label id='lblamount" + counterId + "' readonly='readonly'>0</label></td><td><label id='lblTax" + counterId + "' readonly='readonly'>0</label></td></tr>";


                                            var PO = new clsProduct();
                                            PO.ItemCode = obj.BookingData[i]["Code"];
                                            PO.ItemName = obj.BookingData[i]["Name"];
                                            PO.Qty = obj.BookingData[i]["Qty"];
                                            PO.DispatchdQty = obj.BookingData[i]["DisptchdQty"];
                                            PO.Weight = obj.BookingData[i]["Weight"];
                                            PO.DispatchQty = "0";
                                            PO.Rate = obj.BookingData[i]["Rate"];
                                            PO.Amount = 0;
                                            PO.MRP = obj.BookingData[i]["MRP"];
                                            PO.TaxPer = obj.BookingData[i]["TaxPer"];
                                            PO.Surcharge = obj.BookingData[i]["Surcharge"];
                                            PO.Vat = 0;
                                            ProductCollection.push(PO);


                                            //   TotalAmount = TotalAmount + ProductCollection[counterId]["Amount"];
                                            counterId = counterId + 1;


                                        }

                                        $("#tbDispatchProducts").append(tr);
                                        //commoncalculation();

                                    },
                                    complete: function (msg) {



                                        $("#dispatchDialog").dialog({
                                            autoOpen: true,

                                            width: 1115,
                                            resizable: false,
                                            modal: true
                                        });

                                    }

                                });
                            }

                          



                            function TaxDenomination() 
                            
                            {





                                $("div[name='tax']").each(
                              function (y) {

                                  var TotAmtt = 0;
                                  var FinalTotAmtt = 0;

                                  for (var i = 0; i < ProductCollection.length; i++) {
                                      var Taxx = $(this).html();

                                      var Amount = Number(ProductCollection[i]["Amount"]);

                                      var TaxPer = Number(ProductCollection[i]["TaxPer"]);
                                      var Dis1 = 0;
                                      Dis1 = $("#txtDiscountPer").val();

                                      if (Taxx == TaxPer) {

                                          TotAmtt = Number(TotAmtt.toFixed(2)) + Number(Amount);
                                          FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amount);
                                          var billval = (Number(Amount) * Number(Dis1)) / 100;
                                          FinalTotAmtt = Number(FinalTotAmtt) - (Number(billval));
                                          TotAmtt = Number(TotAmtt) - (Number(billval));


                                          $.ajax({
                                              type: "POST",
                                              data: '{"TaxRate":"' + Taxx + '"}',
                                              url: "placeorder.aspx/GetByTaxStructure",
                                              contentType: "application/json",
                                              dataType: "json",
                                              success: function (msg) {

                                                  var obj = jQuery.parseJSON(msg.d);
                                                  Surcharge = obj.PurchaseData.SurValue;





                                              },
                                              error: function (xhr, ajaxOptions, thrownError) {

                                                  var obj = jQuery.parseJSON(xhr.responseText);
                                                  alert(obj.Message);
                                              },
                                              complete: function () {


                                                  var Total = ((Number(TotAmtt) * Number(Taxx)) / 100);

                                                  var Sur = (Number(Total.toFixed(2)) * Number(Surcharge.toFixed(2)) / 100);

                                                  FinalTotAmtt = (Number(FinalTotAmtt.toFixed(2)));

                                                  $("div[myid='amt_" + Taxx + "']").html(FinalTotAmtt.toFixed(2));
                                                  $("div[myid='vat_" + Taxx + "']").html(Total.toFixed(2));
                                                  $("div[myid='sur_" + Taxx + "']").html(Sur.toFixed(2));


                                              }


                                          });



                                      }
                                  }

                              }
                              );



                            }





                            function BindItemData(code, name, qty, mrp, rate, taxper, surval) {
                                $("#txtCode").val(param1).change();

                            }

                       

                            function GetPluginData(Type) {

                             
                                    var customerId = $("#ddlCreditCustomers").val();
                                    CrdCustSelId = customerId;
                                    $("#hdnCreditCustomerId").val(customerId);


                                    $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
                                    CrdCustSelName = $("#ddlCreditCustomers option:selected").text();


                                    $("#creditCustomer").css("display", "block");
                                 
                            }


                            $(document).ready(function () {
                                BindPaymodes();


								$("#btnDeleteAdv").click(function () {

									var SelectedRow = jQuery('#jQGridDemoAdv').jqGrid('getGridParam', 'selrow');
									if ($.trim(SelectedRow) == "") {
										alert("No Id is selected to Delete");
										return;
									}

									var Id = $('#jQGridDemoAdv').jqGrid('getCell', SelectedRow, 'Receipt_No')


									if (confirm("Are You Sure to delete the records?")) {
										$.ajax({
											type: "POST",
											data: '{"Orderno":"' + Id + '"}',
											url: "placeorder.aspx/DeleteAdvance",
											contentType: "application/json",
											dataType: "json",
											success: function (msg) {

												var obj = jQuery.parseJSON(msg.d);

												alert("Record Deleted Successfully");


											},
											error: function (xhr, ajaxOptions, thrownError) {

												var obj = jQuery.parseJSON(xhr.responseText);
												alert(obj.Message);
											},
											complete: function () {

												$("#packageDialog").dialog('close');
												$("#txtOrderValAdv").val(0);
												$("#txtAdvancePaidAdv").val(0);
												$("#txtBalAdv").val(0);
												$("#txtRecAdv").val(0);
												$("#ddlpaymode").val("Cash");
												$("#txtOrderNoadv").val("Auto");


											}

										});
									}

								});

								$("#btnCancel").click(
									function () {
										$("#packageDialog").dialog('close');
										$("#txtOrderValAdv").val(0);
										$("#txtAdvancePaidAdv").val(0);
										$("#txtBalAdv").val(0);
										$("#txtRecAdv").val(0);
										$("#ddlpaymode").val("Cash");
										$("#txtOrderNoadv").val("Auto");
									}
								);

								$("#btnSavePrint").click(
									function () {

										if (Number($("#txtRecAdv").val()) > Number($("#txtBalAdv").val())) {
											alert("Received Amount Can't be Greater than Balance Amount");
											$("#txtRecAdv").val($("#txtBalAdv").val());
											return;


										}
										var OrderNo = m_OrderNo;
										var RecAmt = $("#txtRecAdv").val();
										var Paymode = $("#ddlpaymode").val();
										$.ajax({
											type: "POST",
											data: '{"Orderno":"' + OrderNo + '","RecAmt":"' + RecAmt + '","Paymode":"' + Paymode + '" }',
											url: "placeorder.aspx/UpdateAdvance",
											contentType: "application/json",
											dataType: "json",
											success: function (msg) {

												var obj = jQuery.parseJSON(msg.d);

												alert("Advance added Successfully");


											},
											error: function (xhr, ajaxOptions, thrownError) {

												var obj = jQuery.parseJSON(xhr.responseText);
												alert(obj.Message);
											},
											complete: function () {

												$("#packageDialog").dialog('close');
												$("#txtOrderValAdv").val(0);
												$("#txtAdvancePaidAdv").val(0);
												$("#txtBalAdv").val(0);
												$("#txtRecAdv").val(0);
												$("#ddlpaymode").val("Cash");
												$("#txtOrderNoadv").val("Auto");


											}

										});


									}
								);


								$("#btnrecadv").click(
									function () {

										if (m_OrderNo == "0") {
											alert("No Order is selected");
											return;
										}

										BindForAdvance();

									}
								);


                                $.ajax({
                                    type: "POST",
                                    data: '{ }',
                                    url: "BillScreen.aspx/GetAllBillSetting",
                                    contentType: "application/json",
                                    dataType: "json",
                                    success: function (msg) {

                                        var obj = jQuery.parseJSON(msg.d);
                                        BillBasicType = obj.setttingData.retail_bill;
                                        RoundBillAmount = obj.setttingData.roundamt;

                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function () {



                                    }

                                });




                                ValidateRoles();

                                function ValidateRoles() {

                                    var arrRole = [];
                                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                                    for (var i = 0; i < arrRole.length; i++) {
                                        if (arrRole[i] == "2") {

                                            $("#btnDelete").show();
                                            $("#btnDelete").click(
                   function () {

                       var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

                       if ($.trim(SelectedRow) == "") {
                           alert("No Order is selected to Cancel");
                           return;
                       }
                     
                       var OrderNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OrderNo')
                       if (confirm("Are You sure to delete this record")) {
                           $.uiLock('');
                         
                           $.ajax({
                               type: "POST",
                               data: '{"OrderNo":"' + OrderNo + '"}',
                               url: "placeorder.aspx/Delete",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);

                                   if (obj.status == -1) {
                                       alert("This Order can not be Canceled.");
                                   }
                                   else {

                                       BindGrid();
                                       alert("Order is Canceled successfully.");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();
                               }
                           });

                       }


                   }
);

                                        }

                                        else if (arrRole[i] == "3") {


                                            $("#btnEdit").show();
                                        }
                                        else if (arrRole[i] == "16") {
                                            $("#cm_Discount").prop('disabled', false);
                                        }
                                        else if (arrRole[i] == "15") {
                                            $("#cm_FOC").prop('disabled', false);
                                        }
                                    }
                                }




                                $("#ddlCreditCustomers").supersearch({
                                    Type: "Accounts",
                                    Caption: "Please enter Customer Name/Code ",
                                    AccountType: "D",
                                    Width: 150,
                                    DefaultValue: 0,
                                    Godown: 0
                                });

                                $("#ddlPrefix").change(
                                    function ()
                                    {

                                        if ($(this).val() == "Cash") {

                                            $("#dvCreditCust").hide();
                                            $("#dvCreditCard").hide();

                                        }
                                        else if ($(this).val() == "Credit") {


                                            $("#dvCreditCust").show();
                                            $("#dvCreditCard").hide();

                                        }
                                        else if ($(this).val() == "CreditCard") {

                                            $("#dvCreditCust").hide();
                                            $("#dvCreditCard").show();


                                        }
                                    }
                                    );

                                $("#btnDispatchCancel").click(
                                function () {


                                    $("#dispatchDialog").dialog("close");
                                }

                                );

                                $("#ddlOrderPaymentModes").change(
                                    function () {
                                        if ($(this).val() == "Credit") {

                                            $("#trCreditCardNo").show();
                                            $("#txtCreditCardNumber").focus();
                                        }
                                        else {
                                            $("#trCreditCardNo").hide();
                                            $("#txtCreditCardNumber").val("");
                                        }

                                    }
                                    );


                                $("#btnAddCustomer").click(
                                function () {

                                    $("#CustomerDialog").dialog({
                                        autoOpen: true,

                                        width: 1115,
                                        resizable: false,
                                        modal: true
                                    });
                                }
                                );


								$("#txtDateFrom,#txtDateTo,#txtBillDate").val($("#<%=hdnDate.ClientID%>").val());
                           
                            
                            BindGrid();

                           

                                          

                            $("#btnDispatch").click(
                                                     function () {

                                                         if (m_OrderNo == "0") {
                                                             alert("No Order is selected for dispatch");
                                                             return;
                                                         }


                                                         BindProducts();
                                                     }
                                                      );




                            $("#btnBillSave").click(
                         function () {

                             InsertUpdateDispatch();


                         }
                          );






                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                      );



 
                            var today = new Date();
                            $('#txtOrderDate').val($.datepicker.formatDate('mm/dd/yy', today));

                            $('#txtDeliveryDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                $('#txtDeliveryDate').change();

                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

								$('#txtBillDate').daterangepicker({
									singleDatePicker: true,
									calender_style: "picker_1"
								}, function (start, end, label) {
									console.log(start.toISOString(), end.toISOString(), label);
								});
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>
            
    </form>

     
  <script language="javascript">
   
     
      function GetCustomerSearchResult(CustomerId, Name, Mobile, Address) {


          $("#txtMobile").val(Mobile).change();
          $("#txtCustomerName").val(Name).change();
          $("#txtAddress").val(Address).change();
          $("#txtId").val(CustomerId).change();
      }
      
   
      function BindGrid() 
      {
     
       

          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          
          
         
          jQuery("#jQGridDemo").GridUnload();
           
           
          jQuery("#jQGridDemo").jqGrid({
              url: 'handlers/manageorderbookings.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['OrderNo','ManualNo', 'OrderDate', 'CustomerID', 'CustomerName', 'Address', 'DeliveryType', 'DeliveryTime', 'DeliveryAddress', 'NetAmount', 'VatAmount', 'Discount', 'AdvancePaid', 'PayMode', 'Balance', 'Remarks'],
              colModel: [
                { name: 'OrderNo', key: true, index: 'OrderNo', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: false },
                { name: 'ManualOrderNo', index: 'ManualOrderNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strOD', index: 'strOD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'CustomerName', index: 'CustomerName', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'Address', index: 'Address', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'DeliveryType', index: 'DeliveryType', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'strDD', index: 'strDD', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                        { name: 'DeliveryAddress', key: true, index: 'DeliveryAddress', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'NetAmount', index: 'NetAmount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'VatAmount', index: 'VatAmount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'DisAmt', index: 'DisAmt', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Advance', index: 'Advance', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'PaymentMode', index: 'PaymentMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'LeftPayRecd', index: 'LeftPayRecd', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Remarks', key: true, index: 'Remarks', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
              ],
              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPager',
              sortname: 'OrderNo',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Order Bookings List",




          });

          var $grid = $("#jQGridDemo");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemo").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {


              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                $("#btnDelete").css({ "display": "none" });
           
                for (var i = 0; i < arrRole.length; i++) {

                   
                    if (arrRole[i] == 2) {

//                        $("#btnDelete").css({ "display": "block" });
                         $("#btnDelete").show();
                    }
                  
                }
                m_OrderNo = 0;
                m_OrderNo = $('#jQGridDemo').jqGrid('getCell', rowid, 'OrderNo');



          }
      });





          $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                  {
                      refresh: false,
                      edit: false,
                      add: false,
                      del: false,
                      search: false,
                      searchtext: "Search",
                      addtext: "Add",
                  },

                  {//SEARCH
                      closeOnEscape: true

                  }

                    );


          var DataGrid = jQuery('#jQGridDemo');
          DataGrid.jqGrid('setGridWidth', '700');


      }



	  function BindGridAdv() {



		  var orderno = $("#txtOrderNoadv").val();



		  jQuery("#jQGridDemoAdv").GridUnload();

		  jQuery("#jQGridDemoAdv").jqGrid({
			  url: 'handlers/lAdvanceReceiptEntry.ashx?orderno=' + orderno,
			  ajaxGridOptions: { contentType: "application/json" },
			  datatype: "json",

			  colNames: ['RecieptNo', 'RecieptDate', 'CCode', 'CName', 'Amount', 'ModeOfPayment', 'Bank_Name', 'CHQNO', 'userno', 'BranchId'],
			  colModel: [
				  { name: 'Receipt_No', key: true, index: 'Receipt_No', stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: false },
				  { name: 'Receipt_Date', index: 'Receipt_Date', stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
				  { name: 'CCode', index: 'CCode', stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
				  { name: 'CName', index: 'CName', stype: 'text', sortable: true, editable: false, hidden: false, editrules: { required: true } },
				  { name: 'Amount', index: 'Amount', stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
				  { name: 'ModeOfPayment', index: 'ModeOfPayment', stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
				  { name: 'Bank_Name', index: 'Bank_Name', stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
				  { name: 'CHQNO', index: 'CHQNO', stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },

				  { name: 'userno', key: true, index: 'userno', stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
				  { name: 'BranchId', index: 'BranchId', stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },

			  ],
			  rowNum: 10,

			  mtype: 'GET',
			  toolbar: [true, "top"],
			  loadonce: true,
			  rowList: [10, 20, 30],
			  pager: '#jQGridDemoPagerAdv',
			  sortname: 'OrderNo',
			  viewrecords: true,
			  height: "100%",
			  width: "500px",
			  sortorder: 'desc',
			  ignoreCase: true,
			  caption: "ReceiptMasterList",




		  });

		  var $grid = $("#jQGridDemoAdv");
		  // fill top toolbar
		  $('#t_' + $.jgrid.jqID($grid[0].id))
			  .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
		  $("#globalSearchText").keypress(function (e) {
			  var key = e.charCode || e.keyCode || 0;
			  if (key === $.ui.keyCode.ENTER) { // 13
				  $("#globalSearch").click();
			  }
		  });
		  $("#globalSearch").button({
			  icons: { primary: "ui-icon-search" },
			  text: false
		  }).click(function () {
			  var postData = $grid.jqGrid("getGridParam", "postData"),
				  colModel = $grid.jqGrid("getGridParam", "colModel"),
				  rules = [],
				  searchText = $("#globalSearchText").val(),
				  l = colModel.length,
				  i,
				  cm;
			  for (i = 0; i < l; i++) {
				  cm = colModel[i];
				  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
					  rules.push({
						  field: cm.name,
						  op: "cn",
						  data: searchText
					  });
				  }
			  }
			  postData.filters = JSON.stringify({
				  groupOp: "OR",
				  rules: rules
			  });
			  $grid.jqGrid("setGridParam", { search: true });
			  $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
			  return false;
		  });



		  $('#jQGridDemoAdv').jqGrid('navGrid', '#jQGridDemoAdvPager',
			  {
				  refresh: false,
				  edit: false,
				  add: false,
				  del: false,
				  search: false,
				  searchtext: "Search",
				  addtext: "Add",
			  },

			  {//SEARCH
				  closeOnEscape: true

			  }

		  );


		  var DataGrid = jQuery('#jQGridDemoAdv');
		  DataGrid.jqGrid('setGridWidth', '580');


	  }

  </script>
            
</asp:Content>

