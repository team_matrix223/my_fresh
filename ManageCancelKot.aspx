﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ManageCancelKot.aspx.cs" Inherits="ManageCancelKot" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
 
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
<%--    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-gl  yphicons.css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <style type="text/css">
        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }
    </style>
     
   
<script language="javascript" type="text/javascript">

    function ApplyRoles(Roles) {


        $("#<%=hdnRoles.ClientID%>").val(Roles);
    }


    var m_ItemId = 0;
    var m_ItemCode = "";
    var m_ItemName = "";
    var m_Qty = 0;
    var m_Price = 0;
    var m_TaxRate = 0;
    var m_Surval = 0;
  
    var m_EditVal = 0;

    var ProductCollection = [];
    function clsproduct() {
        this.ItemId = 0;
        this.ItemCode = "";
        this.ItemName = "";
        this.Qty = 0;
        this.Price = 0;
        this.TaxCode = 0;
        this.SurVal = 0;
        this.ProductAmt = 0;
        this.Producttax = 0;
        this.ProductSurchrg = 0;
        this.TaxId = 0;
       
        this.EditVal = 0;
    }


    function Bindtr() {

        $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        for (var i = 0; i < ProductCollection.length; i++) {
            if ((ProductCollection[i]["EditVal"]) == "1") {

                var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center' disabled = 'disabled'>-</div></td><td style='width:40px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center' disabled = 'disabled'>+</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'></td><td style='width:50px;text-align:center'></td></tr>";

            }
            else {
                var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:40px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'><i id='dvAddon' style='cursor:pointer'><img src='images/addon.png'/></i></td></tr>";
            }


            $("#tbProductInfo").append(tr);

//            $(function () {
//                var wtf = $('#dvProductInfo');
//                var height = wtf[0].scrollHeight;
//                wtf.scrollTop(height);
//            });

  

        }

        if (ProductCollection.length == 0) {
            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
        }


    }



    function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, B_Qty, EditVal) {

       
        m_ItemId = ProductId;
        m_ItemCode = Code;
        m_Price = Price;
        m_ItemName = Name;
        m_EditVal = EditVal;
        if (B_Qty == 0) {
            if (modeRet == "Return") {
                m_Qty = -1;
            }
            else {
                m_Qty = 1;
            }
        }
        else {
            m_Qty = B_Qty;
        }
        m_TaxRate = TaxCode;
        m_Surval = SurVal;

      
      
        TO = new clsproduct();

        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
       

      
        TO.EditVal = m_EditVal;

        ProductCollection.push(TO);
        


        Bindtr();



    }

    function GetKotDetail(KotNo) {

       
        $.uiLock('');

        $.ajax({
            type: "POST",
            data: '{ "KotNo": "' + KotNo + '"}',
            url: "managecancelkot.aspx/GetByKotNo",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                ProductCollection = [];

                if (obj.productLists.length != "0") {
                    for (var i = 0; i < obj.productLists.length; i++) {

                        $("#txtKotTable").val(obj.productLists[i]["TableNo"]);
                        $("#txtKot").val(obj.productLists[i]["KOTNo"]);
                        $("#txtKotTable").attr("disabled", "disabled");
                        $("#txtKot").attr("disabled", "disabled");
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Qty"], 1);

                    }
                }
                else {
                    ProductCollection = [];
                }




            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }

        });





    }

    $(document).ready(
   function () {
       BindGrid();

       $("#btnCash").click(
          function () {

              var KotNo = $("#txtKot").val();
              if (confirm("Are You sure to delete this record")) {
                  $.uiLock('');

                  $.ajax({
                      type: "POST",
                      data: '{"KotNo":"' + KotNo + '"}',
                      url: "ManageCancelkot.aspx/Delete",
                      contentType: "application/json",
                      dataType: "json",
                      success: function (msg) {

                          var obj = jQuery.parseJSON(msg.d);

                          if (obj.status == 0) {
                              alert("Deletion Failed. City is in Use.");
                              return
                          }
                          else if (obj.status == 1) {
                              BindGrid();
                              $("#screenDialog").dialog('close');
                              alert("KOT is Deleted successfully.");

                          }




                      },
                      error: function (xhr, ajaxOptions, thrownError) {

                          var obj = jQuery.parseJSON(xhr.responseText);
                          alert(obj.Message);
                      },
                      complete: function () {
                          $.uiUnlock();
                      }
                  });
              }

          }
       );

       $("#btnClear").click(
          function () {
              ProductCollection = [];

              $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

              $("#txtKotTable").val("");
              $("#txtKot").val("");

              $("#screenDialog").hide();
          }
       );


       $("#btnDelete").click(
     function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Kot is selected to Delete");
             return;
         }

         var KotNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'KOTNo')
         GetKotDetail(KotNo);
         $("#screenDialog").dialog({
             autoOpen: true,

             width: 500,
             resizable: false,
             modal: false
         });




     }
     );












   });


</script>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
    
     <asp:HiddenField ID="hdnDate" runat="server"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>KOT</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage KOT's</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>

         <table cellspacing="0" cellpadding="0" style="margin-top:5px">
                                            <tr>
                                           
                                            <td> <div id="btnDelete"  style="display:block;" class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i>
 Delete</div></td>
                                            </tr>
                                            </table>
      
                    </div>

                        </div>
                    </div>

         <div id="screenDialog" style="background: url('images/nikbg2.jpg');display:none" >

    <input type="hidden" id="hdnnetamt" value="0" />
     <input type="hidden" id="hdnsteward" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
    <div class="container">
        <div class="row">
            <div class="nav1" style="padding-top:0px;margin-top:-5px;display:none"> 
                <ul id="categories">
                </ul>
            </div>
        </div>
        <div class="row">

         <div class="col-xs-12">
         <div class ="col-xs-12">

                
                <div class="Search" style="padding-left:10px">
                    <div class="input-group">
                        <table width="100%">
                         
                          <tr><td style="color:White;font-weight:bold">TableNo</td><td style="padding-left:5px;padding-top:5px"><input type="text" id ="txtKotTable" style="width:100px"  /></td>
                          <td style="color:White;font-weight:bold;padding-left:10px">KotNo</td><td style="padding-left:5px;padding-top:5px"><input type="text" id ="txtKot" style="width:100px"  /></td></td></tr>
                        </table>
                    </div>
                </div>
                </div>

                </div>
                

                <div class="col-xs-12" style="display:none;margin-bottom:5px" id="colProducts">
              
                    </div>





             <div class="col-xs-12">
             
            <div class="col-xs-12" style="padding-left: 0px" id="colDvProductList">
                <div id="dvProductList" style="display: block;margin-top:-10px">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 200px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>

                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Qty
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amt
                                </th>
                                
                                  <th style="width: 200px;text-align:center;color: #FFFFFF;display:none">
                                    Add-On
                                </th>


                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
               

                    <table>
                      
                 
                        <tr>
                           
                            <td>
                            <div class="button1" id="btnCash" style="width:70px;">
                                   Save</div>
                          
                               
                            </td>
                            

                             <td>
                               <div class="button3" id="btnClear" style="width:75px">
                                    Exit </div>
                            </td>


                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                      
                    </table>
                </div>
       

            
            </div>
            
        </div></div>
    </div>            
                </div>
                <!-- /page content -->

           

            </div>


 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageKots.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['KOTNo','KotDate','MKotNo','TableNo','PaxNo','Value','Steward'],
                        colModel: [
                                    { name: 'KOTNo', key: true, index: 'KOTNo', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                     { name: 'strKD', index: 'strKD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'MKOTNo', index: 'MKOTNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'TableID', index: 'TableID', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'PaxNo', index: 'PaxNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Value', index: 'Value', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'EmpName', index: 'EmpName', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                          
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'KOTNo',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "KOT List",

                        editurl: 'handlers/ManageKots.ashx',


 ignoreCase: true,
                         toolbar: [true, "top"],

                    });



                     var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });


            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>



</asp:Content>


