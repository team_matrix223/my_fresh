﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class signout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Response.Cookies["accessval"].Expires = DateTime.Now.AddDays(-1);
        HttpCookie mycookie = new HttpCookie("accessval");
        mycookie.Expires = DateTime.Now.AddDays(-1d);
        Response.Cookies.Add(mycookie);
        //Response.Cookies.Remove("accessval");
        Response.Redirect("Login.aspx");
    }
}