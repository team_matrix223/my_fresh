﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="DForm.aspx.cs" Inherits="DForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
	<link href="css/customcss/taxswapNew.css" rel="stylesheet" />
      <script type="text/javascript" src="js/jquery.uilock.js"></script>
<script>
    $(document).ready(function () {
      
		CheckDeleteBill();

   //     $("#txtStartDate").blur(function () {
           
			
			
			//$.ajax({
			//	type: "POST",
			//	data: '{ }',
			//	url: "DForm.aspx/CheckDeleteBill",
			//	contentType: "application/json",
			//	dataType: "json",
			//	success: function (msg) {
			//		var $result = $(msg).find(this);
			//		var obj = jQuery.parseJSON(msg.d);


			//		if (obj.productData.Bill_No == " ") {
						
			//			$("#txtStartDate").removeAttr('disabled');

			//		}
			//		else {
						
			//			$("#txtStartDate").val(obj.productData.strBD);

			//			$("#txtStartDate").attr('disabled', 'disabled');
			//			$("#btnShow").attr('disabled', 'disabled');
			//			alert("Complete Your Modification");
			//			return;
			//		}


			//	},
			//	error: function (xhr, ajaxOptions, thrownError) {

			//		var obj = jQuery.parseJSON(xhr.responseText);
			//		alert(obj.Message);
			//	},
			//	complete: function () {


					
			//	}

			//});

       

   //     });
       
       

       

        $('#txtStartDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        
       
        $("#btnShow").click(function () {
			var val = validation();
			if (val != "") {
				alert(val)
				return false;
            }

			var DateFrom = $("#txtStartDate").val();
            
			$.uiLock('');
			$.ajax({
                type: "POST",
				data: '{"DateFrom":"' + DateFrom + '"}',
				url: "DForm.aspx/InsertintoDeleteBill",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);




				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);

				},
				complete: function () {

					setTimeout(
						function () {
							$.uiUnlock();
                        }, 1000);
                    
					
				}

			});

		});

		//$("#btnReport").click(function () {
		//	window.location = "RptTaxWiseSaleNew.aspx";
		//});

		//$("#btnForm1").click(function () {
		//	window.location = "Form1.aspx";
  //      });

		//$("#btnForm3").click(function () {
		//	window.location = "Form3.aspx";
			
		//});
     
		$("#btnsubmit").click(function () {
			var val = validation();
			if (val != "") {
				alert(val)
				return false;
			}
			

			var DateFrom = $("#txtStartDate").val();

			$.uiLock('');
			$.ajax({
				type: "POST",
				data: '{"DateFrom":"' + DateFrom + '"}',
				url: "DForm.aspx/InsertintoBill",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);




				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);

				},
				complete: function () {

					setTimeout(
						function () {
							$.uiUnlock();
						}, 1000);

					$("#txtStartDate").val("");
					

					$("#txtStartDate").removeAttr('disabled');
					$("#btnShow").removeAttr('disabled');
				}

			});

		});

		function CheckDeleteBill() {



			$.ajax({
				type: "POST",
				data: '{ }',
				url: "DForm.aspx/CheckDeleteBill",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {
					var $result = $(msg).find(this);
					var obj = jQuery.parseJSON(msg.d);




					if (obj.productData.Bill_No == " ") {

						$("#txtStartDate").removeAttr('disabled');
						$("#txtStartDate").val();

					}
					else {

						$("#txtStartDate").val(obj.productData.strBD);

						$("#txtStartDate").attr('disabled', 'disabled');
						$("#btnShow").attr('disabled', 'disabled');
						
					}




				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {



				}

			});



		}
       
       
        function validation() {

            var errorval = "";

            if ($("#txtStartDate").val() == "") {

                errorval = "Please Select Date!";

            }
          
            //else if ($("#dd_convertto").val() == $("#dd_choostax").val()) {

            //	errorval = "Both Tax Shouldn't Be Same!";

            //}
         
          

            return errorval;

        }
    
      


    })

</script>
      <div class="right_col" role="main">
		<div class="x_panel x-panel-title-frmone" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Manage Sale</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>
			  </div>
			<div class="choose-branch-taxswap">
               <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                              
						 <div id="panal">
							 <div class="taxswap-first-panel">

							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-date-section">
									  <label class="control-label">Date:</label>
									  
									  <input type="text" class="form-control" autocomplete="off"  id="txtStartDate" style="width:160px;height: 30px;margin-top: 5px;margin-bottom: 5px;" />
								  <div class="taxwap-btn-section btnshow-div">                                
                                
                                 
									  <button type="button" class="btn btn-success" id="btnShow" >Ok</button>
								  </div>
									  </div>

                                  	
                                  

							  </div>


							 
							 </div>
								 <div class="taxswap-second-panel">
							

                               
							<%-- <div class="col-md-4 col-sm-4 col-xs-12">
								 
								 
							 </div>--%>
							
							</div>
							 <div class="taxswap-third-panel">
  
							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-btn-section">                                
                                
                                 
									  <a  href="RptTaxWiseSaleNew.aspx" class="btn btn-success"   target="_blank">Tax Wise Sale Report</a>
								  </div>
							 </div>
								 </div>
                              <div class="taxswap-third-panel">
  
							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-btn-section">                                
                                
                                 
									  <a href="Form1.aspx" class="btn btn-success"   target="_blank" >Form1</a>
								  </div>
							 </div>
								 </div>


                              <div class="taxswap-third-panel">
  
							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-btn-section">                                
                                
                                 
									  <a href="Form3.aspx" class="btn btn-success"  target="_blank" >Form3</a>
								  </div>
							 </div>
								 </div>
						
                   </div>
                                       
                                    
                                </div>
                   </div>
			</div>

                   
            <button type="button" class="btn btn-success" id="btnsubmit">Modification Complete</button>
                                      
                          
          </div>
    
</asp:Content>

