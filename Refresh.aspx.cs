﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;


public partial class Refresh : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDb.Value = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select siteurl from mastersetting_Basic  ", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                string str = dt.Rows[0]["siteurl"].ToString();
                string[] str1 = str.Split('W');
                hdnurl.Value = str1[0];
            }
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REFRESH));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]

    public static void RefreshSettings()
    {
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Pos_sp_DataRefresh", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 1000;
            cmd.ExecuteNonQuery();


           
        }

    }

}


