﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Form3.aspx.cs" Inherits="Form3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
	<link href="css/customcss/taxswapNew.css" rel="stylesheet" />
      <script type="text/javascript" src="js/jquery.uilock.js"></script>
<script>
    $(document).ready(function () {


        CheckDeleteBill();

        var BillCollection = [];

        var ItemBillCollection = [];


        function clsBill() {

            this.BillNo = "";
            this.Amount = 0;
            this.BillId = 0;
            this.Net_Amount = 0;


        }

        function clsgroup() {

            this.GroupId = 0;
            this.GroupName = "";



        }

		function clsHsn() {

            
            this.Title = "";



		}

        BindVariableMaster();

        function BindVariableMaster() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "manageitemmaster.aspx/GetVariableMasters",
                data: {},
                async: false,
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);
                    var html = "<option value=0>-Select-</option>";

                    for (var i = 0; i < obj.TaxRates.length; i++) {

                        html = html + "<option value='" + obj.TaxRates[i]["Id"] + "'>" + obj.TaxRates[i]["Title"] + "</option>";
                    }
                    $("#dd_choostax").html(html);
                    $("#dd_convertto").html(html);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }
            });

        }


		function CheckDeleteBill() {



			$.ajax({
				type: "POST",
				data: '{ }',
				url: "DForm.aspx/CheckDeleteBill",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {
					var $result = $(msg).find(this);
					var obj = jQuery.parseJSON(msg.d);




					$("#txtStartDate").val(obj.productData.strBD);




				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {



				}

			});



		}

        //$("#dd_choostax").change(function () {
        //	var GroupCollection1 = [];
        //	$("#td_taxgrp").show();
        //	$("#btngrp").show();



        //	$.uiLock('');
        //	$.ajax({
        //		type: "POST",
        //		data: '{"tax":"' + $(this).val() + '"}',
        //		url: "Form3.aspx/GetGroupsBYTaxID",
        //		contentType: "application/json",
        //		dataType: "json",
        //		success: function (msg) {

        //			var obj = jQuery.parseJSON(msg.d);



        //			if (obj.GroupsList.length > 0) {
        //				for (var i = 0; i < obj.GroupsList.length; i++) {


        //					TO = new clsgroup();

        //					TO.GroupId = obj.GroupsList[i]["Group_Id"];
        //					TO.GroupName = obj.GroupsList[i]["Group_Name"];

        //					GroupCollection1.push(TO);

        //				}
        //				var html = "";

        //				for (var i = 0; i < GroupCollection1.length; i++) {




        //					html += "<tr id='tr_" + GroupCollection1[i]["Group_Id"] + "'>";
        //					html += "<td><input type='checkbox' class='cls_chkgrp' name= 'chk_" + GroupCollection1[i]["GroupId"] + "' data=" + GroupCollection1[i]["GroupId"] + " val = " + GroupCollection1[i]["GroupId"] + " id='chk_" + GroupCollection1[i]["GroupId"] + "' /></td>";
        //					html += "<td class='cls_Grpno'>" + GroupCollection1[i]["GroupName"] + "</td>";




        //					html += "</tr>";


        //				}


        //				$("#tbShowGroups").html(html);


        //			}



        //		},
        //		error: function (xhr, ajaxOptions, thrownError) {

        //			var obj = jQuery.parseJSON(xhr.responseText);
        //			alert(obj.Message);
        //		},
        //		complete: function () {

        //			$.uiUnlock();

        //		}



        //	});

        //      });


        $("#dd_convertto").change(function () {
            var GroupCollection = [];
            $("#td_taxgrp").show();
            $("#btngrp").show();



            $.uiLock('');
            $.ajax({
                type: "POST",
                data: '{"tax":"' + $(this).val() + '"}',
                url: "Form3.aspx/GetGroupsBYTaxIDTaxTo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);



                    if (obj.GroupsList.length > 0) {
                        for (var i = 0; i < obj.GroupsList.length; i++) {


                            TO = new clsgroup();

                            TO.GroupId = obj.GroupsList[i]["Group_Id"];
                            TO.GroupName = obj.GroupsList[i]["Group_Name"];

                            GroupCollection.push(TO);

                        }
                        var html = "";

                        for (var i = 0; i < GroupCollection.length; i++) {




                            html += "<tr id='tr_" + GroupCollection[i]["Group_Id"] + "'>";
                            html += "<td><input type='checkbox' class='cls_chkgrpto' name= 'chkto_" + GroupCollection[i]["GroupId"] + "' data=" + GroupCollection[i]["GroupId"] + " val = " + GroupCollection[i]["GroupId"] + " id='chkto_" + GroupCollection[i]["GroupId"] + "' /></td>";
                            html += "<td class='cls_Grpnoto'>" + GroupCollection[i]["GroupName"] + "</td>";




                            html += "</tr>";


                        }


                        $("#tbShowGroups").html(html);


                    }



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();

                }



            });

        });
       
        $("#td_conertfrm").hide();
		$("#btngrpfrm").hide();


        //$("#txtStartDate").blur(function () {
           
            $("#td_conertfrm").show();
           
       

       // });
       
       
         
        $("#dd_choostax").change(function () {

   //         var errorval = "";

			//if ($("#txtStartDate").val() == "") {

			//	errorval = "Please Select Date!";

			//	return errorval;

			//}


			var GroupCollection = [];
			$("#td_taxgrpfrm").show();
			$("#btngrpfrm").show();



			$.uiLock('');
			$.ajax({
				type: "POST",
				data: '{"tax":"' + $(this).val() + '"}',
				url: "Form3.aspx/GetGroupsBYTaxID",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);



					if (obj.GroupsList.length > 0) {
						for (var i = 0; i < obj.GroupsList.length; i++) {


							TO = new clsgroup();

							TO.GroupId = obj.GroupsList[i]["Group_Id"];
							TO.GroupName = obj.GroupsList[i]["Group_Name"];

							GroupCollection.push(TO);

						}
						var html = "";

						for (var i = 0; i < GroupCollection.length; i++) {




							html += "<tr id='tr_" + GroupCollection[i]["Group_Id"] + "'>";
							html += "<td><input type='checkbox' class='cls_chkgrp' name= 'chk_" + GroupCollection[i]["GroupId"] + "' data=" + GroupCollection[i]["GroupId"] + " val = " + GroupCollection[i]["GroupId"] + " id='chk_" + GroupCollection[i]["GroupId"] + "' /></td>";
							html += "<td class='cls_Grpno'>" + GroupCollection[i]["GroupName"] + "</td>";




							html += "</tr>";


						}


						$("#tbShowGroupsfrm").html(html);


					}



				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {

					$.uiUnlock();

				}



			});




            if ($(this).val()!=0) {
                $("#td_conertto").show();
				

            }
           else{
                $("#td_conertto").hide();
				//$("#td_taxgrp").hide();
            }


        })

        $('#txtStartDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#txtStartDate').attr('disabled', 'disabled');
        $('#txtEndDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $("#btnShow").click(function () {
        	var val = validation();
        	if (val != "") {
        		alert(val)
        		return false;
        	}
            $.uiLock();

            BillCollection = [];
           
            ItemBillCollection = [];
            var DateFrom = $("#txtStartDate").val(); 
			var DateTo = $("#txtStartDate").val(); 
            var Branch = $("#ddlBranch").val();
			var Pos = $("#ddlPos").val();
            var convertfrom_tax = $("#dd_choostax :selected").text()
            $.ajax({
                type: "POST",
                data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","tax": "' + convertfrom_tax + '","Branch": "' + Branch + '","POS": "' + Pos + '"}',
                url: "form3.aspx/GetOrdersBYTax",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    if (obj.BillsList.length > 0) {
                        for (var i = 0; i < obj.BillsList.length; i++) {


                            TO = new clsBill();

                            TO.BillNo = obj.BillsList[i]["BillNowPrefix"];
                            TO.Amount = obj.BillsList[i]["Amount"];
                            TO.Net_Amount = obj.BillsList[i]["Net_Amount"];
                            TO.BillId = obj.BillsList[i]["Bill_No"];
                            BillCollection.push(TO);

                        }
                        $("#btnDelete").show();
                        BindBills();
                
                    }
                    else {
                        $("#btnDelete").hide();
                        alert("No Bills Found.");
                        $("#btnpanel").hide();
                        return;

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }

            });
        });

        $("#chk_All").click(function () {
            var chk_alltotal=0
            if ($(this).prop('checked') == true) {
                $(".cls_chkbox").prop('checked', true);
                $(".cls_chkbox").each(function () {

                    var amount = $(this).parent().next('td').next('td').text();
                    chk_alltotal = chk_alltotal + Number(amount);

                })

            }
            else {
                $(".cls_chkbox").prop('checked', false);
           
            }
            $("#total_amt").text(Number(chk_alltotal).toFixed(2));

        })
        function BindBills() {

            var html = "";

            for (var i = 0; i < BillCollection.length; i++) {




                html += "<tr id='tr_" + BillCollection[i]["BillId"] + "'>";
                html += "<td><input type='checkbox' class='cls_chkbox' name= 'chk_" + BillCollection[i]["BillId"] + "' data=" + BillCollection[i]["BillId"] + " val = " + BillCollection[i]["Amount"] + " id='chk_" + BillCollection[i]["BillId"] + "' /></td>";
                html += "<td class='cls_billno'>" + BillCollection[i]["BillNo"] + "</td>";
                html += "<td class='cls_amount'>" + BillCollection[i]["Amount"] + "</td>";
                html += "<td class='cls_netamount'>" + BillCollection[i]["Net_Amount"] + "</td>";



                html += "</tr>";


            }


            $("#tbShowBills").html(html);

            $("#tax_per").text("("+$("#dd_choostax :selected").text()+"%)");

        }


        $("#txtBarcode").keyup(function (event) {
            event.preventDefault();

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                for (var i = 0; i < BillCollection.length; i++) {

                    Billprefix = BillCollection[i]["BillNo"];
                    if (Billprefix == $("#txtBarcode").val()) {
                        billid = BillCollection[i]["BillId"];

                        if ($("#chk_" + billid).prop('checked') != true) {
                            $("#chk_" + billid).prop('checked', true);
                            $("#tr_" + billid)[0].scrollIntoView(false);


                        }
                    }


                }

                $("#txtBarcode").val("");
                var total = 0;
                $(".cls_chkbox").each(function () {
                    if ($(this).prop('checked') == true) {
                        var amount = $(this).parent().next('td').next('td').text();
                        total = total + Number(amount);
                    }

                })


                $("#total_amt").text(Math.abs(Number(total).toFixed(2)));
            }
            $("#tbShowBills tr").removeAttr('style');
            $("#tr_" + billid)[0].scrollIntoView();
            $('.main_container')[0].scrollIntoView(true);
            $("#tr_" + billid).attr('style', 'background-color: chartreuse');

        

        });


        $("#btnreset").click(function () {

            window.location = "Form3.aspx";
        })

		$("#btngrpfrm").click(function () {

			var arr_grpno = new Array();
			if ($(".cls_chkgrp:checked").length == 0) {
				alert("Please Select At Least One Group!")
				return false;

            }

		
			var DateFrom = $("#txtStartDate").val();
			var DateTo = $("#txtStartDate").val();
			var Branch = $("#ddlBranch").val();
			//var convertto = $("#dd_choostax :selected").text()

			//$("#tbShowGroupsfrm tr").each(function () {

			//	var currentrow = $(this).closest('tr');


			//	var GroupNo = currentrow.find('.cls_chkgrp').attr('data');

			//	if ($('input[name=chk_' + GroupNo + ']').prop('checked') == true) {

			//		arr_grpno.push(GroupNo);


			//	}

			//});

			var convertto = $("#dd_choostax :selected").text()

			$("#tbShowGroupsfrm tr").each(function () {

				var currentrow = $(this).closest('tr');


				var GroupNo = currentrow.find('.cls_chkgrp').attr('data');

				if ($('input[name=chk_' + GroupNo + ']').prop('checked') == true) {

					arr_grpno.push(GroupNo);


				}
			});

			$.uiLock();
			$.ajax({
                type: "POST",
                data: JSON.stringify({ arr_grpno: arr_grpno, convertto: convertto, DateFrom: DateFrom, DateTo : DateTo,Branch:Branch }),
				
				url: "Form3.aspx/updategrpsbytaxfrm",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);



				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);

				},
				complete: function () {

					setTimeout(
						function () {
							$.uiUnlock();
						}, 1000);
					$("#td_taxgrpfrm").hide();
					$("#btngrpfrm").hide();

				}

			});





        });
        var hsnlenth = 0;
        function bindhsn() {
            var HsnCollection = [];
			var arr_grpno2 = new Array();

			$("#tbShowGroups tr").each(function () {

				var currentrow = $(this).closest('tr');


				var GroupNo = currentrow.find('.cls_chkgrpto').attr('data');

				if ($('input[name=chkto_' + GroupNo + ']').prop('checked') == true) {

                    arr_grpno2.push(GroupNo);


				}

			});
				$.ajax({
                    type: "POST",
                    data: JSON.stringify({ arr_grpno: arr_grpno2 }),

					url: "Form3.aspx/GetHsnBYGroup",
					contentType: "application/json",
					dataType: "json",
					success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        hsnlenth = obj.HsnList.length;
						if (obj.HsnList.length > 0) {
							for (var i = 0; i < obj.HsnList.length; i++) {


								TO = new clsHsn();

								
								TO.Title = obj.HsnList[i]["Title"];

                                HsnCollection.push(TO);

							}
							var html = "";
                            var counthsn = 1;
                            for (var i = 0; i < HsnCollection.length; i++) {




                                html += "<tr id='tr_" + HsnCollection[i]["Title"] + "'>";
                                html += "<td><input type='checkbox' class='cls_chkHsn' name= 'chkH_" + counthsn + "' data=" + HsnCollection[i]["Title"] + " val = " + HsnCollection[i]["Title"] + " id='chkH_" + HsnCollection[i]["Title"] + "' /></td>";
                                html += "<td class='cls_Hsn'>" + HsnCollection[i]["Title"] + "</td>";




								html += "</tr>";

                                counthsn = counthsn + 1;
							}


							$("#tbShowHsn").html(html);


						}



					},
					error: function (xhr, ajaxOptions, thrownError) {

						var obj = jQuery.parseJSON(xhr.responseText);
						alert(obj.Message);
					},
					complete: function () {

						$.uiUnlock();

					}



				});

            

        };

		$("#btnhsn").click(function () {

            var arr_grpno1 = new Array();
			
			if ($(".cls_chkgrpto:checked").length == 0) {
				alert("Please Select At Least One Group!")
				return false;
			}
			var convertto = $("#dd_convertto :selected").text()

			$("#tbShowGroups tr").each(function () {

				var currentrow = $(this).closest('tr');


				var GroupNo = currentrow.find('.cls_chkgrpto').attr('data');

				if ($('input[name=chkto_' + GroupNo + ']').prop('checked') == true) {

					arr_grpno1.push(GroupNo);
				}

			});
            var arr_hsn = new Array();
			
			if ($(".cls_chkHsn:checked").length == 0) {
				alert("Please Select At Least One HSN!")
				return false;

			}
			
			//$("#tbShowHsn tr").each(function () {

			//	var currentrow = $(this).closest('tr');
   //             var HsnCode = currentrow.find('.cls_chkHsn').attr('data');
                var len = hsnlenth;
               
            for (var i = 0; i <= len; i++) {
                
                    if ($('input[name=chkH_'+i+ ']').prop('checked') == true) {
                        
                        var hsncode = $('input[name=chkH_' + i + ']').attr('data');
                        
						arr_hsn.push(hsncode);
                    }

                }

				
               


				

			//});

			

			$.uiLock();
			$.ajax({
                type: "POST",
				data: JSON.stringify({ arr_hsn: arr_hsn, arr_grpno: arr_grpno1, convertto: convertto }),

				url: "Form3.aspx/updatehsn",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);



				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);

				},
				complete: function () {

					setTimeout(
						function () {
							$.uiUnlock();
						}, 1000);
					
					$("#dvhsn").hide();
					$("#btnhsn").hide();

					

				}

			});





		});

       
        $("#btngrp").click(function () {

			var arr_grpno1 = new Array();
			if ($(".cls_chkgrpto:checked").length == 0) {
				alert("Please Select At Least One Group!")
				return false;

			}
			var convertto = $("#dd_convertto :selected").text()
			
			$("#tbShowGroups tr").each(function () {

				var currentrow = $(this).closest('tr');

				
                var GroupNo = currentrow.find('.cls_chkgrpto').attr('data');
                
				if ($('input[name=chkto_' + GroupNo + ']').prop('checked') == true) {
					
					arr_grpno1.push(GroupNo);


                }

			});

				//var convertto = $("#dd_convertto :selected").text()

				//$("#tbShowGroups tr").each(function () {

				//	var currentrow = $(this).closest('tr');


				//	var GroupNo = currentrow.find('.cls_chkgrp').attr('data');

				//	if ($('input[name=chk_' + GroupNo + ']').prop('checked') == true) {

				//		arr_grpno.push(GroupNo);


				//	}
    //        });

			$.uiLock();
			$.ajax({
                type: "POST",
                data: JSON.stringify({ arr_grpno: arr_grpno1, convertto: convertto }),

				url: "Form3.aspx/updategrpsbytax",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);



				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);

				},
				complete: function () {

					setTimeout(
						function () {
							$.uiUnlock();
                        }, 1000);
					$("#td_taxgrp").hide();
                    $("#btngrp").hide();
                    $("#dvhsn").show();
					$("#btnhsn").show();
					
                    bindhsn();

				}

			});



		

        });

        $("#btnsubmit").click(function () {

            var val = validation();
            if (val != "") {
                alert(val)
                return false;
            }
             if ($(".cls_chkbox:checked").length == 0) {
             	alert("Please Select At Least One Bill !")
            	return false;

            }
            insert_bill_bytax();

        })
        function insert_bill_bytax() {

        	var arr_billno = new Array();
        	var ConvertTo = $("#dd_convertto :selected").text();
        	var ConvertFrom = $("#dd_choostax :selected").text();
        	$("#tbShowBills tr").each(function () {

        		var currentrow = $(this).closest('tr');

        		var billno = currentrow.find('.cls_billno').text();
        		var billid = currentrow.find('.cls_chkbox').attr('data');
        		if ($('input[name=chk_' + billid + ']').prop('checked') == true) {

        			arr_billno.push(billno);


        		}
            });
			var Pos = $("#ddlPos").val();
        	$.uiLock();
        	$.ajax({
                type: "POST",
                data: JSON.stringify({ arr_billno: arr_billno, fromtax: ConvertFrom, totax: ConvertTo, POS: Pos }),

        		url: "Form3.aspx/insert_bill_bytax",
        		contentType: "application/json",
        		dataType: "json",
        		success: function (msg) {

        			var obj = jQuery.parseJSON(msg.d);



        		},
        		error: function (xhr, ajaxOptions, thrownError) {

        			var obj = jQuery.parseJSON(xhr.responseText);
        			alert(obj.Message);

        		},
                complete: function () {
					
                       
                   
        			setTimeout(
           function () {
           	$.uiUnlock();
           }, 1000);
					window.location = "Form3.aspx";
        		}

        	});



        }
        function validation() {

            var errorval = "";
            if ($("#ddlBranch").val() == 0) {

                errorval = "Please Select Branch!";

            }
            else if ($("#txtStartDate").val() == "") {

                errorval = "Please Select Date!";

            }
            else if ($("#dd_choostax").val() == 0) {

                errorval = "Please Select 'Convert From Tax'!";

            }

            else if ($("#dd_convertto").val() == 0) {

                errorval = "Please Select 'Convert To Tax'!";

            }
            //else if ($("#dd_convertto").val() == $("#dd_choostax").val()) {

            //	errorval = "Both Tax Shouldn't Be Same!";

            //}
         
          

            return errorval;

        }
    
        $(document).on('change', '.cls_chkbox', function () {
            var total = 0;
          
  
                $(".cls_chkbox").each(function () {
                    if ($(this).prop('checked') == true) {
                        var amount = $(this).parent().next('td').next('td').text();
                        total = total + Number(amount);
                    }
                  
                })

    
            $("#total_amt").text(Math.abs(Number(total).toFixed(2)));
        });





    })

</script>
      <div class="right_col" role="main">
		<div class="x_panel x-panel-title-frmone" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Manage Tax</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>
			  </div>
			<div class="choose-branch-taxswap">
               <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                              
						 <div id="panal">
							 <div class="taxswap-first-panel">

							 <div class="col-md-4 col-sm-4 col-xs-12">
								 <div class="taxwap-branch-section">
									  <label class="control-label">Choose Branch:</label>
									  <select id="ddlBranch" Class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>
								</div>
                                 <div class="taxwap-branch-section">
									  <label class="control-label">Choose POS:</label>
									  <select id="ddlPos" Class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>
								</div>
                                
								 <div id="td_conertfrm" style="display:none" class="taxwap-tax-section">
									 <label class="control-label">Convert Tax From:</label>
									 <select id="dd_choostax" style="width: 160px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>
								 </div>
							
                                 	 <div class="taxwap-scan-section">
									 <label class="control-label">Scan Barcode:</label>
									 <input type="text"  id="txtBarcode" style="width: 450px;"  class="form-control customTextBox"  />
								 </div>

							 </div>
							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-date-section">
									  <label class="control-label">Date:</label>
									  <input type="text" class="form-control" autocomplete="off"  id="txtStartDate" style="width:160px;height: 30px;margin-top: 5px;margin-bottom: 5px;" />
								  </div>

                                  	  <div class="taxwap-date-section"  style="display:none">
									  <label class="control-label">To:</label>
									  <input type="text" class="form-control" autocomplete="off"  id="txtEndDate" style="width:160px;height: 30px;margin-top: 5px;margin-bottom: 5px;" />
								  </div>

                                   <div id="td_conertto" style="display:none" class="taxwap-tax-to-section">
									 <label class="control-label">Convert Tax To:</label>
									 <select id="dd_convertto" style="width: 160px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                             </select>

								 </div>

							  </div>


							 <div class="col-md-4 col-sm-4 col-xs-12">
								<div>
       <div id ="td_taxgrpfrm" style="display:none" class="taxwap-tax-section">
 <label class="control-label">Groups:</label>
            <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll; width:65%;margin-bottom:20px;">
                    <table class="table table-striped"  style="font-size:12px;margin-top:0px;">
                                        
										<tbody id="tbShowGroupsfrm">


										</tbody>
                                    </table> 
                      </div>
            <div class="taxwap-btn-section update-groups-btn">
									  <button type="button" class="btn btn-success" id="btngrpfrm" style="display:none" >Update Groups</button>
								  </div> 
       </div>


   </div>

                              <%--   /*----second group----*/--%>
                                   <div>
       <div id ="td_taxgrp" style="display:none" class="taxwap-tax-section">
 <label class="control-label">Groups:</label>
            <div style="border:solid 1px silver;border-style: inset;height:200px;overflow-y:scroll;width:66%;margin-bottom:20px;">
                    <table class="table table-striped"  style="font-size:12px;margin-top:0;">
                                        
										<tbody id="tbShowGroups">


										</tbody>
                                    </table> 
                      </div>
       </div>


   </div>
                             <%--    /*----second group----*/--%>
                                  
                                   <div class="taxwap-btn-section second-update-btn">
									  <button type="button" class="btn btn-success" id="btngrp" style="display:none" >Update Groups</button>
								  </div>
                                  <div id="dvhsn" style="border:solid 1px silver;display:none;border-style: inset;height:200px;overflow-y:scroll;width:66%;margin-bottom:20px;">
                    <table class="table table-striped"  style="font-size:12px;margin-top:0;">
                                        
										<tbody id="tbShowHsn">


										</tbody>
                                    </table> 
                      </div>
                                  <div class="taxwap-btn-section second-update-btn">
									  <button type="button" class="btn btn-success" id="btnhsn" style="display:none" >Update</button>
								  </div>

							 </div>
							 </div>
								 <div class="taxswap-second-panel">
							<%-- <div class="col-md-4 col-sm-4 col-xs-12">
								 <div id="td_conertfrm" style="display:none" class="taxwap-tax-section">
									 <label class="control-label">Convert Tax From:</label>
									 <select id="dd_choostax" style="width: 160px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>
								 </div>
							 </div>--%>




                                  
                                <%--  <div class="taxwap-btn-section">
									  <button type="button" class="btn btn-success" id="btngrpfrm" >Update Groups</button>
								  </div>--%>









                               
							 <div class="col-md-4 col-sm-4 col-xs-12">
								<%-- <div id="td_conertto" style="display:none" class="taxwap-tax-to-section">
									 <label class="control-label">Convert Tax To:</label>
									 <select id="dd_convertto" style="width: 160px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                             </select>

								 </div>--%>
							 </div>
							 <div class="col-md-4 col-sm-4 col-xs-12">

							 </div>
							</div>
							 <div class="taxswap-third-panel">
							 <%-- <div class="col-md-8 col-sm-8 col-xs-12">
								 <div class="taxwap-scan-section">
									 <label class="control-label">Scan Barcode:</label>
									 <input type="text"  id="txtBarcode" style="width: 450px;"  class="form-control customTextBox"  />
								 </div>
							 </div>--%>


                                 <%--  <div class="taxwap-btn-section second-update-btn">
									  <button type="button" class="btn btn-success" id="btngrp" >Update Groups</button>
								  </div>--%>
  
							  <div class="col-md-4 col-sm-4 col-xs-12">
								  <div class="taxwap-btn-section"><%--                                 /*----second group was here----*/
                                
                                  /*----second group was here----*/--%>
									  <button type="button" class="btn btn-success" id="btnShow" >Show</button>
								  </div>
								   <div class="taxwap-total-section">
									 <label>Total Amount:</label><label id="total_amt">0</label>
								 </div>
							 </div>
								 </div>
						
                   </div>
                                       
                                      <%-- <table style="width:100%;"  >

                                  <tr>
                                      <td  style="width: 150px">
                                    
                                    </td>

                                         <td>      
                                    


                                    </td>
                                      </tr>
                                             <tr>
                                                 <td style="width:80px"></td>  <td  style="width:130px"> </td>
                                                 </tr>

                                  <tr>
                                      <td></td><td colspan="3">
                                     
                                             

                                       </td>

                                      <td >
                                           
                                     
  
                                           
                                      </td>
                                  </tr>
                                            <tr><td ></td><td colspan="3"></td></tr>
                                           <tr>  <td></td><td></td></tr>
                                 </table>--%>
                                </div>
                   </div>
			</div>

                      <div class="x_panel x-panel-frmone-colone taxswap-below-tbl" >

                                <div class="x_content">

                                    <table class="table table-striped"  style="font-size:12px;margin-top:-18px;">
                                         <thead>
										<tr><th style="padding-left: 2px;"><input type="checkbox" id="chk_All" /></th><th>BillNo</th><th>Taxable Amt&nbsp;<span id="tax_per"></span></th><th>Net Amount</th></tr>
										</thead>
										<tbody id="tbShowBills">


										</tbody>
                                    </table>	
                                 
                                </div>
                            </div>
            <button type="button" class="btn btn-success" id="btnsubmit">Update Bill(s)</button>
                                      	<button type="button" class="btn btn-danger" id="btnreset">Reset</button>
                          
          </div>
    
</asp:Content>

