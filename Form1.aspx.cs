﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindBranches();
            BindPos();
        }
    }



    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }
    public void BindPos()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        ddlPos.DataSource = dt;
        ddlPos.DataTextField = "title";
        ddlPos.DataValueField = "posid";
        ddlPos.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose POS--";
        li1.Value = "0";
        ddlPos.Items.Insert(0, li1);
    }

    [WebMethod]
    public static string InsertUpdate(DeliveryDetail[] objDeliveryDetails, string ItemType,int Branch)
    {
       
        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Scheme");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1P");
        dt.Columns.Add("Dis2P");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("Dis3P");
        dt.Columns.Add("Godown_ID");
        dt.Columns.Add("RowNum");
        dt.Columns.Add("Excise_Duty");
        dt.Columns.Add("Excise_Amt");
        DataRow dr;


        Int32 RowNumm = 1;
        foreach (var item in objDeliveryDetails)
        {
            dr = dt.NewRow();
            dr["Item_Code"] = item.Item_Code;
            dr["Scheme"] = 0;
            dr["Qty"] = 0;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = 0;
            dr["Dis1P"] = 0;
            dr["Dis2P"] = 0;
            dr["TaxP"] = 0;
            dr["Dis3P"] = 0;
            dr["Godown_ID"] = 0;
            dr["RowNum"] = RowNumm;
            dr["Excise_Duty"] = 0;
            dr["Excise_Amt"] = 0;
            dt.Rows.Add(dr);
            RowNumm = RowNumm + 1;


        }

        int Status = new SaleBLL().Insert(Branch, dt, ItemType);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetProductsByType(string ItemType,int Branch)
    {
        
        List<Sale> lstProducts = new SaleBLL().GetProductsByType(ItemType, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetOrdersBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch, int POS,int RdoVal)
    {
        List<Bill> lstBills = new BillBLL().GetOrderBillsByDateByPOS(DateFrom,DateTo,ItemType, Branch,POS,RdoVal);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills
        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static void UpdteTemp(string BillNowPrefix,int ischk, int saletype)
    {

        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
        con.Open();
        //SqlCommand cmd = new SqlCommand("update tbl_grp_container set isactive=@IsChk where BillNowPrefix=@BillNowPrefix and tagged=1", con);
        SqlCommand cmd = new SqlCommand("strp_tagging", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
        cmd.Parameters.AddWithValue("@IsChk", ischk);
        cmd.Parameters.AddWithValue("@SaleType", saletype);
        cmd.ExecuteNonQuery();
        }
    }
    [WebMethod]
    public static void UpdteItemforDel(string BillNowPrefix, int ischk)
    {
        string qry;
        if (ischk == 1)
        {
            qry = "update bill_detail set Tagged=4 where BillNowPrefix='"+ BillNowPrefix + "' and tagged=1 and billmode=1";
      

        }
        else {
            qry = "update bill_detail set Tagged=1 where BillNowPrefix='" + BillNowPrefix + "' and tagged=4 and billmode=1";
     
        }

        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(qry, con);
            cmd.CommandType = CommandType.Text;
            cmd.ExecuteNonQuery();
        }
    }

    [WebMethod]
    public static void UpdteItemforInsert(string[] BillNowPrefix, bool[] ischk, string rdoval,int pos)
    {
    
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            string qry="";

            DataTable dt = new DataTable();
            dt.Columns.Add("BillNowPrefix");
            dt.Columns.Add("ischk");
            dt.Columns.Add("rdoval");
            for (int i = 0; i < BillNowPrefix.Count(); i++)
            {
                DataRow dr = dt.NewRow();
                dr["BillNowPrefix"] = BillNowPrefix[i];
                dr["ischk"] = ischk[i];
                dr["rdoval"] = rdoval;

                dt.Rows.Add(dr);

                //if (rdoval=="1")
                //{


                //if (ischk[i] == true)
                //{
                //    qry = "update bill_master set KKCPer=1 where BillNowPrefix='" + BillNowPrefix[i] + "'and KKCPer=0";


                //}
                ////else
                ////{
                ////    qry = "update bill_master set KKCPer=0 where BillNowPrefix='" + BillNowPrefix[i] + "' and KKCPer=1";

                ////}
                //}
                //else if (rdoval == "0")
                //{
                //    if (ischk[i] == true)
                //    {
                //        qry = "update bill_detail set Tagged=4 where BillNowPrefix='" + BillNowPrefix[i] + "' and tagged=1 and billmode=1";

                //    }
                //    //else
                //    //{
                //    //    qry = "update bill_detail set Tagged=1 where BillNowPrefix='" + BillNowPrefix[i] + "' and tagged=4 and billmode=1";

                //    //}
                //}
     
            }
            SqlCommand cmd = new SqlCommand("strp_UpdteItemforInsertByPos", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DataDetail", dt);
            cmd.Parameters.AddWithValue("@POS", pos);

            cmd.ExecuteNonQuery();

        }
    }
    [WebMethod]
    public static string GetOrderItemBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch)
    {


        List<BillDetail> lstBills = new BillBLL().GetOrdersBillItemByDate(DateFrom, DateTo, ItemType, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetOptions(string ProcessType)
    {


        List<Bill> lstOptions = new BillBLL().GetOptions(ProcessType);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            lstOptions = lstOptions

        };
        return ser.Serialize(JsonData);

    }



    [WebMethod]
    public static string Insert(string BillNowPrefix, string Amount, string BillNo, string ItemType, string Saletype,int Branch)
    {
        
        string[] BillNowPrefixData = BillNowPrefix.Split(',');
        string[] AmountData = Amount.Split(',');
        string[] BillNoData = BillNo.Split(',');

        DataTable dt = new DataTable();

        dt.Columns.Add("BillNowPrefix");
        dt.Columns.Add("Amount");
        dt.Columns.Add("BillNo");



        for (int i = 0; i < BillNowPrefixData.Length; i++)
        {
            if (BillNowPrefixData[i] != "")
            {
                DataRow dr = dt.NewRow();

                dr["BillNowPrefix"] = Convert.ToString(BillNowPrefixData[i]);
                dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                dr["BillNo"] = Convert.ToInt32(BillNoData[i]);

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteBills(Branch, dt,ItemType,Saletype);
        var JsonData = new
        {

           Status = status
        };
        return ser.Serialize(JsonData);



    }


    [WebMethod]
    public static void updatetag_4(string[] arr_billno,int ischk,int saletype)
    {


        DataTable dt = new DataTable();
        dt.Columns.Add("bill_no");
        for (int i = 0; i < arr_billno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["bill_no"] = arr_billno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_updatetag_4", con);
            cmd.Parameters.AddWithValue("@ischk", ischk);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bill_no", dt);
            cmd.Parameters.AddWithValue("@saletype", saletype);
            cmd.ExecuteNonQuery();

        }

    }

    [WebMethod]
    public static void RefreshRecords()
    {



        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_resetbills", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();

        }

    }
    [WebMethod]
    public static void insert_bill_bygroup(int group_id,string[] arr_billno,decimal addless,int req,int saletype, int pos)
    {
        string proc = "";
        if (saletype==0)
        {
            proc = "strp_deletebill_bygroup";
        }
        else if (saletype == 1)
        {
            proc = "strp_insertbill_bygroup";
        }

        DataTable dt = new DataTable();
        dt.Columns.Add("bill_no");
        for (int i = 0; i < arr_billno.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["bill_no"] = arr_billno[i];
            dt.Rows.Add(dr);
        }
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand(proc, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@group_id", group_id);
            cmd.Parameters.AddWithValue("@addless", addless);
            cmd.Parameters.AddWithValue("@req", req);
            cmd.Parameters.AddWithValue("@sale_type", saletype);
            cmd.Parameters.AddWithValue("@bill_no", dt);
            cmd.Parameters.AddWithValue("@POS", pos);
            cmd.CommandTimeout = 5000;
            cmd.ExecuteNonQuery();

        }

    }

    [WebMethod]
    public static string DeleteItemsBill(string ItemCode, string Amount,  string ItemType, string Saletype, int Branch,DateTime DateFrom,DateTime DateTo)
    {

        string[] ItemCodeData = ItemCode.Split(',');
        string[] AmountData = Amount.Split(',');
       

        DataTable dt = new DataTable();

     
        dt.Columns.Add("Amount");
        dt.Columns.Add("ItemCode");



        for (int i = 0; i < ItemCodeData.Length; i++)
        {
            if (ItemCodeData[i] != "")
            {
                DataRow dr = dt.NewRow();

              
                dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                dr["ItemCode"] = Convert.ToString(ItemCodeData[i]);

              

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteItemBills(Branch, dt, ItemType, Saletype,DateFrom,DateTo);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }

}


