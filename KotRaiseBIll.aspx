﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="KotRaiseBIll.aspx.cs" Inherits="KotRaiseBIll" %>
  <%@ Register src="Templates/AddCashCustomer.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">




    <form id="form1" runat="server">

        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
        <link href="css/customcss/billform.css" rel="stylesheet" />
<%--    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-gl  yphicons.css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/kotraisebill.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
    
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <style type="text/css">

        .rbl input[type="radio"]
{
    margin-left: 21px;
    margin-right: 15px;
    width: 100%;
    height: 33px;
    float: right;

}

        h3, .h3 {
    font-size: 14px;
    font-weight: bold;
}
        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }


        .cls_paymode {

            display:none
        }
        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }
    </style>
                            
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
             <script>

                 //$(function () {
                 //    $('input[type="text"]').keyboard();

                 //    $('textarea').keyboard();
                    

                 //});
         

                 $(document).on('click', 'input[type="text"]', function () {
                     $.ajax({
                         type: "POST",
                         url: "billscreen.aspx/Keyboard",
                         contentType: "application/json",
                         dataType: "json",

                         error: function (xhr, ajaxOptions, thrownError) {

                             var obj = jQuery.parseJSON(xhr.responseText);
                             alert(obj.Message);
                         },
                         complete: function (msg) {
                         }

                     });
                     $(this).focus();
                 });
    </script>
    <script language="javscript" type="text/javascript">
        var TableeId = 0;
      
        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
		var PrintRoute = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode = "";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var AllowKKC = false;
        var AllowSBC = false;
        var KKC = 0;
        var SBC = 0;
        var cst_id = 0;
        var rdopaymode = "";
     


        function ResetCashCustmr() {

            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            $("#txtddlMobSearchBox").val("CASH");
            $("#txtcustId").val("CASH");

        }

        $(document).ready(function () {
           
          cashcustcode = $("#hdncashcustid").val();
        })
        $(window).keydown(function (e) {
            $.uiLock();

            switch (e.keyCode) {
                case 112:   // F1 key is left or up
                    $("#btnCash").click();
                    $.uiUnlock();
                    return false;
                case 115: //F4 key is left or down
                    $("#KOT").click();
                    return false; //"return false" will avoid further events
                case 119: //F8 key is left or down
                    $("#settlement").click();
                    $.uiUnlock();
                    return false;
                case 120: //F9 key is left or down

                    $("#btnhome_del").click();
                    $.uiUnlock();
                    return false;

                case 117: //F6 key is left or down

                    $("#btnCal").click();
                    $.uiUnlock();
                    return false;

                case 118: //F7 key is left or down
                    window.location.href = 'BillScreen.aspx';

                    $.uiUnlock();
                    return false;

            }
            $.uiUnlock();
            return; //using "return" other attached events will execute
        });

        function processingComplete() {
            $.uiUnlock();

        }


        var CshCustSelId = 0;
        var CshCustSelName = "";
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var m_BillNowPrefix = "";
        var m_DiscountType = "";
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var billingmode = ""
        var OrderId = "";
        var Type = "";
        var TotalItems = 0;
        chkhomedel = "";

      
        var cashcustName = "";

        //.....................................


        function EditBill(BillNowPrefix) {


            BillNowPrefix = m_BillNowPrefix;
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);
                    $("#dvDeliveryChrg").val(obj.productLists[0]["KKCPer"]);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], false, obj.productLists[i]["TaxType"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                   // //$("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }




        function GenerateBill(OrderNo) {
            RestControls();
            OrderId = OrderNo;

            $.ajax({
                type: "POST",
                data: '{ "OrderNo": "' + OrderNo + '"}',
                url: "screen.aspx/GetByOrderNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["ItemDiscount"], 1, obj.productLists[i]["Edit_SaleRate"], false);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                   // //$("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }





        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }



            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }




        //...................................




        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;
        var KKCTAmt = 0;
        var SBCTAmt = 0;
        var onlineoption = "COD";




            function RestControls() {
            $("#lblCreditCustomerName").text("");
            $("#textotp").val("");
            $("ddlCreditCustomers").hide();
            $("ddlChosseCredit").hide();
            $("ddlChosseCredit").val(0); 
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            $("#txtorderno").val("");
            $("#txtcustId").val(0);
            BillNowPrefix1 = "";
            Type = "";
            $("#CashCustomer").css("display", "none");
            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:19px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            $("#dvdisper").val(0);
            $("#dvdiscount").val(0);
            $("#dvnetAmount").html("0");
            $("#dvRound").html("0");
            $("#dvTax").html("0");
            $("#dvVat").html("0");
            $("#dvDeliveryChrg").val("0");
            $("#dvsertaxper").html("0");
            $("#dvKKCPer").html("0");
            $("#dvKKCAmt").html("0");

            $("#dvSBCPer").html("0");
            $("#dvSBCAmt").html("0");

            $("#dvsbtotal").html("0");
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;
            $("#hdnsteward").val("0");

            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            $("#ddlTable option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
           // //$("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvProductList").show();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            $("#txtCashReceived").val("0").prop("readonly", false);
            $("#txtPax").val("");
            $("#txtKotTable").val("");
            $("#txtkotsteward").val("");
            $("#txtcustId").val("");
            $("#txtddlMobSearchBox").val("");
            $("#txtKotDate").val($("#<%=hdnDate.ClientID%>").val());
            CrdCustSelId = 0;
            CshCustSelId = 0;
            CshCustSelName = "";
            CrdCustSelName = "";
            TableeId = 0;
            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            $("#btnEdit").css({ "display": "none" });
            $("#btnShowScreen").css({ "display": "none" });

            $("#btnDelete").css({ "display": "none" });

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "9") {

                    $("#btnShowScreen").css({ "display": "block" });
                }

                if (arrRole[i] == "3") {

                    $("#btnEdit").css({ "display": "block" });
                }

                if (arrRole[i] == "2") {

                    $("#btnDelete").css({ "display": "block" });
                }
            }

            $("#chkhone").prop('checked', false);
            $("#<%=ddlsteward.ClientID %>").attr("disabled", "disabled");
            $("#DelCharges").css("display", "none");
            option = "Dine";
            $("#<%=ddlsteward.ClientID %>").val("0");





        }

        function InsertOnlineOtherPayment(billno) {


            var Id = 0;
            var OtherPayment_ID = $("#ddlOtherPaymentnew").val();
            //var OtherPayment_ID = OtherPayment_ID;

            var BillNo = billno;
            var Billmode = "";

            var CashAmt = $("#txtCODAmountnew").val();
            if (CashAmt > 0) {
                Billmode = "COD"
            }
            else {
                Billmode = "OnlinePayment"
            }


            var OnlineAmt = $("#txtpaymentAmountnew").val();
            //var Coupan= Coupan;
            if (Billmode == "COD") {
                var Coupan = [];
                var CoupanAmt = [];
            }
            else {

                var Coupan = [];
                var CoupanAmt = [];

                for (var i = 0; i < $("#holder input").length; i++) {
                    if ($("#txtCoupan" + i).val() > "0") {
                        Coupan[i] = $("#txtCoupan" + i).val();
                    }
                }
                // var Coupan= $("#holder input").val();

                for (var i = 0; i < $("#holder2 input").length; i++) {
                    if ($("#txtCoupanAmt" + i).val() > "0") {
                        CoupanAmt[i] = $("#txtCoupanAmt" + i).val();
                    }
                }
            }

            $.ajax({

                type: "POST",
                data: '{ "ID": "' + Id + '","OtherPayment_ID": "' + OtherPayment_ID + '","Bill_No": "' + BillNo + '","CoupanNo": "' + Coupan + '","CoupanAmt": "' + CoupanAmt + '","Mode": "' + Billmode + '","CashAmt": "' + CashAmt + '","OnlineAmt": "' + OnlineAmt + '"}',
                url: "settlement.aspx/InsertOnlineOtherPayment",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }
   
        function InsertUpdate() {

          

            var BillNowPrefix = "";
            var BillRemarks = "";
            ////////////////////////////////////////////////
            if (cst_id == '-1')
            {
                
            var FinalAmt = $("#txtFinalBillAmount").val();
            var CashRecieve = $("#txtCashReceived").val();
            var CrCardAmt = $("#Text13").val();
            var COD = $("#txtCODAmount").val();
            var OnlinePayment = $("#txtpaymentAmount").val();
            var AmtTotal = 0;
            var TransectionID = 0;
            AmtTotal = parseFloat(CashRecieve) + parseFloat(CrCardAmt) + parseFloat(COD) + parseFloat(OnlinePayment);
            TransectionID = $("#trnid").val();
            if (AmtTotal != FinalAmt) {

                alert("Please check your entered amount!");
                return false;
            }

           

            $("#btnSave").attr('disabled', 'disabled');
            if ($("#txtKotBillNo").val() == "") {
                alert("First Choose BillNo for settlement");
                return;
            }

            var billtype = $("#ddlbillttype").val();
            if (billtype == "") {
                alert("Select BillType First");
                $("#ddlbillttype").focus();
                return;

            }


            if (billtype == "Credit") {

                if (CrdCustSelId == 0) {

                    alert("Please select Credit Customer");
                    $("#txtddlCreditCustomers").focus();
                    $("#btnSave").removeAttr('disabled');
                    return;
                }


            }
            var txtcreditcardcheck = $("#Text13").val();

            if (txtcreditcardcheck != "0") {
                if ($("#ddlType").val() == "") {
                    $.uiUnlock();
                    alert("Please Select Credit Card Type");
                    $("#ddlType").focus();
                    $("#btnSave").removeAttr('disabled');
                    return;
                }
                //if ($("#Text14").val() == "") {
                //    $.uiUnlock();
                //    alert("Please Enter Credit Card No");
                //    $("#Text14").focus();
                //    return;
                //}

            }

            var txtchequecheck = $("#Text15").val();
            if (txtchequecheck != "0") {
                if ($("#ddlBank").val() == "") {
                    $.uiUnlock();
                    alert("Please Select Bank");
                    $("#ddlBank").focus();
                    $("#btnSave").removeAttr('disabled');
                    return;
                }
                if ($("#Text16").val() == "") {
                    $.uiUnlock();
                    alert("Please Enter Cheque No");
                    $("#Text16").focus();
                    $("#btnSave").removeAttr('disabled');
                    return;
                }
            }


            var cashamount = $("#txtCashReceived").val();


            if (billtype == "Cash" || billtype == "CreditCard" || billtype == "OnlinePayment") {
                if (Number($("#txtBalanceReturn").val()) < 0) {
                    $.uiUnlock();
                    alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                    $("#btnSave").removeAttr('disabled');
                    return;
                }
                else {
                    cashamount = cashamount - Number($("#txtBalanceReturn").val());
                }

            }


            //                  if (Number(cashamount) < 0) {
            //                      $.uiUnlock();
            //                      alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
            //                      $("#btnSave").removeAttr('disabled');
            //                      return;
            //                  }


            if (billtype == "CreditCard") {


                var CustomerId = $("#ddlBank option:selected").text();
                var CustomerName = $("#ddlBank option:selected").text();
                var billmode = billtype;
                var NetAmt = $("#txtFinalBillAmount").val();

                var CashAmt = 0
                var creditAmt = 0;
                var OnlinePayment = 0;
                var BIllValue = $("#dvsbtotal").html();


                var CreditBank = $("#ddlBank").val();
                var CreditCardAmt = $("#Text13").val();


                var cashcustcode = 0;
                var cashcustName = "";

                cashcustcode = 0;
                cashcustName = "CASH"


            }
            if (billtype == "Cash") {




                var CustomerId = "CASH";
                var CustomerName = "CASH";
                var billmode = billtype;
                var NetAmt = $("#txtFinalBillAmount").val();

                //var CashAmt = $("#txtFinalBillAmount").val();
                var CashAmt = $("#txtCashReceived").val();
                var creditAmt = 0;
                var OnlinePayment = 0;
                var BIllValue = $("#dvsbtotal").html();


                var CreditBank = $("#ddlBank").val();
                var CreditCardAmt = $("#Text13").val();


                var cashcustcode = 0;
                var cashcustName = "";

                cashcustcode = 0;
                cashcustName = "CASH"


            }


            if (billtype == "OnlinePayment") {


                var CustomerId = "CASH";
                var CustomerName = "CASH";
                var billmode = billtype;
                var NetAmt = $("#txtFinalBillAmount").val();

                var CashAmt = 0;
                if (onlineoption == "COD") {
                    CashAmt = $("#txtCODAmount").val();
                }

                var creditAmt = 0;

                var OnlinePayment = $("#txtpaymentAmount").val();


                var CreditBank = "";
                var CreditCardAmt = 0;


                var cashcustcode = 0;
                var cashcustName = "";

                cashcustcode = 0;
                cashcustName = "CASH"

                BillRemarks = $("#trnid").val();


            }

            else if (billtype == "Credit") {


                var CustomerId = 0;
                var CustomerName = "";
                if (CrdCustSelId == "0") {
                    CustomerId = 0;
                    CustomerName = "";
                }
                else {
                    CustomerId = CrdCustSelId;
                    CustomerName = CrdCustSelName;
                }

                var NetAmt = $("#txtFinalBillAmount").val();

                var billmode = billtype;

                var CreditBank = $("#ddlBank").val();
                var OnlinePayment = 0;
                var CashAmt = $("#txtCashReceived").val();

                var BIllValue = $("#dvsbtotal").html();
                var creditAmt = 0;
                if (billmode == "Credit") {

                    creditAmt = Number(NetAmt) - Number(CashAmt);


                }

                var CreditCardAmt = $("#Text13").val();


                var cashcustcode = 0;
                var cashcustName = "";




            }
            }

            //////////////////////////////////////////////////////
            else
                {
            var CustomerId = "0";
            var CustomerName = "CASH";
            var billmode = "Cash";
            var NetAmt = $("#dvnetAmount").html();
            var CreditBank = "";
            var CashAmt = "0.00";
            var creditAmt = "0.00";
            var CreditCardAmt = "0.00";

            var OnlinePayment = "0.00";
            var OTPval = "";
            cashcustcode = $("#txtcustId").val();
            cashcustName = $("#txtddlMobSearchBox").val();
           rdopaymode=$("#<%= RdoPaymode.ClientID %> input:checked").val(); 
      
            if (cst_id != 0)
            {
               billmode = "OnlinePayment";

                if (rdopaymode != "Online" && rdopaymode != "COD")
                {
                    alert("Please select 'Mode' for payment!");
                    return;
                }
                if (rdopaymode == "Online") {
                    OnlinePayment = NetAmt;
                    
                }
                else if (rdopaymode == "COD") {
                    //if (cst_id = '-1')
                    //{
                    //    billmode = "Cash";

                    //}
                    CashAmt = NetAmt;
                    
                }

            }
            }
            if ($("#txtcustId").val() == "") {
                cashcustcode = 0;
            }
            

            if ($("#chktakeway").prop('checked') == true) {
                // if (cashcustcode == "0") {
                //   alert("Please Choose Customer");
                //   return;
                //}

            }

            var OrderNo = OrderId;


            if (OrderNo == "") {
                OrderNo = "0";
            }

            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();

            var lessdisamt = $("#dvdiscount").val();
            var Order_No = $("#txtorderno").val();
            var addtaxamt = $("#dvVat").html();
            var RoundAmt = 0;
            var hdnNetamt = $("#hdnnetamt").val();
            RoundAmt = Number(NetAmt) - Number(hdnNetamt);

            var DeliveryCharges = $("#dvDeliveryChrg").val();
            var EmpCode = $("#hdnsteward").val();

            var Order_No = $("#txtorderno").val();
            if (Order_No == "") {

                Order_No = 0;

            }

         
            var remarks = option;


            var Tableno = TableeId;
            var setatx = $("#dvTax").html();
            var ServiceTax = $("#dvsertaxper").html();
            var KKCPer = $("#dvKKCPer").html();
            var KKCAmt = $("#dvKKCAmt").html();
            var SBCPer = $("#dvSBCPer").html();
            var SBCAmt = $("#dvSBCAmt").html();
         
            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];
            var OrgSaleRate = [];
            var PAmt = [];
            var Ptax = [];
            var PSurChrg = [];
            var ItemRemarks = [];
            var SurPer = [];
            var free = [];
            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {

                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                OrgSaleRate[i] = ProductCollection[i]["Price"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                //ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                ItemRemarks[i] = ProductCollection[i]["Tax_type"];
                SurPer[i] = ProductCollection[i]["SurVal"];
                free[i] = ProductCollection[i]["Free"];
            }

            TaxDen = [];
            VatAmtDen = [];
            VatDen = [];
            SurDen = [];

            $("div[name='tax']").each(
           function (y) {
               TaxDen[y] = $(this).html();
           }
           );
            $("div[name='amt']").each(
           function (z) {
               VatAmtDen[z] = $(this).html();
           }
           );
            $("div[name='vat']").each(
           function (a) {
               VatDen[a] = $(this).html();
           }
           );
            $("div[name='sur']").each(function (a) {
               SurDen[a] = $(this).html();
           });
         
            $.uiLock();
            var Result = "";
            var CustomerUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
            try {
                var CustomerPhone = CustomerUrl[4].replace('cashcustmob=', '');
            } catch (e) {
                CustomerPhone = '0';
            }
            OTPval = $("#textotp").val();
         
            $.ajax({
                type: "POST",
				data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","FreeArr":"' + free + '","CustomerPhone":"' + CustomerPhone + '","Order_No": "' + Order_No + '","OTP": "' + OTPval + '","Rdopaymode": "' + rdopaymode + '","cstid": 0 }',
                url: "kotRaiseBill.aspx/InsertUpdate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    BillNowPrefix = obj.BNF;
                   
						GeneratePdf(BillNowPrefix);

					
					
                    
                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == -5) {
                        alert("Please Login Again and Try Again..");
                        return;
                    }

                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {

                        
                        Result = "Yes";
                        $("#lblAmmmt").html(NetAmt);
                        $("#dvbillSave").toggle();

                        setTimeout(function () {
                            $("#btnMsgClose").focus();
                        });
                  
                        // Printt(BillNowPrefix);
                        $.uiUnlock();

                        BindGrid();
                        $("#tbProductInfo tr").remove();

                    }
             
                    ProductCollection.length = 0;
                    $("#dvsbtotal").text(0.00); 
                    $("#dvnetAmount").text(0.00);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
          
                    if (obj.Message == 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.') {

                        alert('Previous bill is under process.Please Wait..!');

                    }

                    else {

                        alert(obj.Message);
                    }
                },
                complete: function () {
                    $("#btnBillWindowOk").removeAttr('disabled');

                    if (Result == "Yes") {

                      UpdateKotDeActive(TableeId);
                     var Tableval = localStorage.getItem("Tableno");
                     if (Tableval == '0') {
                     }
                     else
                       RestControls();


                    }

                   // InsertOnlineOtherPayment(BillNowPrefix);
                    GetTable();
                    $.uiUnlock();



                }

            });

        }

		function DeletePdf(BillNowPrefix) {

			$.ajax({
				type: "POST",
				data: '{"BillNowPrefix": "' + BillNowPrefix + '"}',
				url: "BillScreen.aspx/DeletePDF",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);


				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {
					


				}

			});


		}

		function printbill(BillNowPrefix) {

			$.ajax({
				type: "POST",
				data: '{"BillNowPrefix": "' + BillNowPrefix + '"}',
				url: "BillScreen.aspx/BillPrint",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);


				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {

					if (PrintRoute != 1) {
						DeletePdf(BillNowPrefix);
					}
					


				}

			});

		}



		function GeneratePdf(BillNowPrefix) {

			$.ajax({
				type: "POST",
				data: '{"BillNowPrefix": "' + BillNowPrefix + '"}',
				url: "KotRaiseBill.aspx/GeneratePDF",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
                complete: function () {
					printbill(BillNowPrefix);

				}

			});


		}


		function isNumberKey(evt) {
			var charCode = (evt.which) ? evt.which : evt.keyCode;
			if (charCode != 46 && charCode > 31
				&& (charCode < 48 || charCode > 57))
				return false;

			return true;
		}


		function ClearCustomerDialog() {

			m_CustomrId = -1;

			$("#cm_ddlPrefix").val();
			$("#cm_Name").val("");
			$("#cm_Address1").val("");
			$("#cm_Address2").val("");
			// $("#cm_DOB").val("");
			//$("#cm_DOA").val("");
			$("#cm_DOB").val("1/1/1");
			$("#cm_DOA").val("1/1/1");
			$("#cm_Discount").val("0");
			$("#cm_Tag").val("0");
			$("#cm_ContactNumber").val("");
			$("#cm_EmailId").val("");
			$("#dvAddCustomer").dialog("close");
			$("#txtGSTNo").val("");
			$('#cm_IsActive').prop("checked", true);

			$('#cm_IsActive').prop("checked", false);




		}

		function InsertUpdateCustomer() {

			if ($("#txtGSTNo").val() != "0") {


				if ($("#txtGSTNo").val().length < 15) {
					alert("InValid GST No.!")
					return;
				}
			}
			var objCustomer = {};
			objCustomer.Customer_ID = m_CustomrId;
			objCustomer.Prefix = $("#cm_ddlPrefix").val();
			objCustomer.Customer_Name = $("#cm_Name").val();
			objCustomer.Address_1 = $("#cm_Address1").val();
			objCustomer.Address_2 = $("#cm_Address2").val();
			objCustomer.Area_ID = $("#cm_ddlArea").val();
			objCustomer.Pincode = $("#cm_Pincode").val();

			objCustomer.City_ID = $("#cm_ddlCities").val();
			objCustomer.State_ID = $("#cm_ddlState").val();

			objCustomer.Date_Of_Birth = $("#cm_DOB").val();



			objCustomer.Date_Anniversary = $("#cm_DOA").val();







			objCustomer.Discount = $("#cm_Discount").val();
			objCustomer.Contact_No = $("#cm_ContactNumber").val();

			objCustomer.EmailId = $("#cm_EmailId").val();
			objCustomer.Tag = $("#cm_Tag").val();
			//objCustomer.GSTNo = $("#txtGSTNo").val();
			var Foc = false;

			if ($("#cm_Tag").val() == "") {
				objCustomer.Tag = '0';
			}
			else {
				objCustomer.Tag = $("#cm_Tag").val();
			}

			if ($("#txtGSTNo").val() == "") {
				objCustomer.GSTNo = '0';
			}
			else {
				objCustomer.GSTNo = $("#txtGSTNo").val();
			}
			if ($('#cm_FOC').is(":checked")) {
				Foc = true;
			}

			objCustomer.FocBill = Foc;
			var IsActive = false;
			if ($('#cm_IsActive').is(":checked")) {
				IsActive = true;
			}
			var IsCredit = 0;
			if ($('#cm_Credit').is(":checked")) {
				IsCredit = 1;
			}
			objCustomer.IsActive = IsActive;
			objCustomer.IsCredit = IsCredit;
			var DTO = { 'objCustomer': objCustomer };

			$.ajax({
				type: "POST",
				contentType: "application/json; charset=utf-8",
				url: "managecashcustomers.aspx/InsertUpdateCustomer",
				data: JSON.stringify(DTO),
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

					if (obj.Status == -1) {

						alert("Sorry. Contact Number Already Registered with our Database");
						$("#cm_ContactNumber").focus();
						return;
					}
					else {
						alert("CustomerSaved Successfully");

						if ($('#cm_Credit').is(":checked")) {



                        } else {


                            var Discount = obj.customer.Discount;
                            
							$("#txtddlMobSearchBox").val(obj.customer.Customer_Name);
                            $("#lblCashCustomerName").text(obj.customer.Prefix + "  " + obj.customer.Customer_Name + " " + obj.customer.Address_1 + " " + obj.customer.Contact_No);
							$("#txtcustId").val(obj.customer.Customer_ID);
							CshCustSelId = obj.customer.Customer_ID;
							CshCustSelName = obj.customer.Customer_Name;

							$("#CashCustomer").css("display", "block");

							if (EnableCustomerDiscount == 1) {
								$("#dvdisper").val(Discount.toFixed(2));

							}
							
							
						}
						CommonCalculation();

						//BindCreditCst();
						ClearCustomerDialog();


					}
				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


				}

			});


		}



		function GetVMResponse(Id, Title, IsActive, Status, Type) {
			$("#dvVMDialog").dialog("close");

			var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
			if (Type == "Area") {

				$("#cm_ddlArea").append(opt);

			}

			if (Type == "City") {

				$("#cm_ddlCities").append(opt);

			}

			if (Type == "State") {

				$("#cm_ddlState").append(opt);

			}





		}

		function OpenVMDialog(Type) {


			$.ajax({
				type: "POST",
				data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
				url: "managearea.aspx/LoadUserControl",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					$("#dvVMDialog").remove();
					$("body").append("<div id='dvVMDialog'/>");
					$("#dvVMDialog").html(msg.d).dialog({ modal: true });
				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {

				}
			});

		}


        var option = "";










        function UpdateKotDeActive(TableID) {


            $.ajax({
                type: "POST",
                data: '{"TableID": "' + TableID + '"}',
                url: "KotRaiseBIll.aspx/UpdateKotDeActive",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    GetTable();

                    $.uiUnlock();
                }

            });


        }



        function Printt(celValue) {

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");

            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";

            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);

            document.getElementById('reportout').contentWindow.location = "Reports/Retailbillrpt.aspx?BillNowPrefix=" + celValue;



        }
        function getcst_dis(Cstid) {
            
            $.ajax({
                type: "POST",
                data: "{'PaymentModeID':" + Cstid + "}",
                url: "BillScreen.aspx/GetCstDis",
                contentType: "application/json",
                async: false,
                dataType: "json",
                success: function (msg) {
                    if (msg.d.length != 0) {
                        if (Cstid != 0) {
                            $("#dvdisper").val(msg.d);
                            $("#dvdisper").attr('disabled', 'disabled');
                        }
                        else {

                            $("#dvdisper").val(0);
                            $("#dvdisper").removeAttr('disabled');

                        }
                    }
                    else {

                        $("#dvdisper").val(0);
                        $("#dvdisper").removeAttr('disabled');
                    }

                 
                    
                    
                },
            });
        }


        function GetByItemCode2(KotNo) {

            
          <%--  $("#<%= RdoPaymode.ClientID %> input[type=radio]").prop('checked',false);  --%>  
            cst_id = $("#hdncompnycustid").val();
        
  

            if (tablenum==0 ) {
                $("#trpaymode").hide(); 

                $("#dvotp").hide();
                $("#tdorderno").hide();
                if (chkhomedel == "HD") {
                    option = "HomeDelivery"
                }

                else {
                    option = "Dine"
                }

            }
            else {
       
                $("#dvotp").show();
                $("#trpaymode").show();
                $("#tdorderno").show(); 
                $("#DelCharges").css('display', 'none');
                if (cst_id == '-1')
                {

                $("#txtblno").hide();
                $("#dvproducts").css('display', 'none');
                $("#dv_thirdpanel").removeAttr('class');
                $("#dv_thirdpanel").attr('class', 'col-xs-6');
                $("#trpaymode").css('display', 'none');
                $("#dvotp").css('display', 'none');
                $("#dvBillWindow").show();

                $("#tblbilltype").show();

                setTimeout(function () {
                    $("#ddlbillttype").focus();
                });
             
                
                }
                
                
                option = "Dine";
            }
            TableeId = KotNo;

            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "KotRaiseBill.aspx/GetKotDet",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], "", obj.productLists[i]["DisAmount"], obj.productLists[i]["Qty"], obj.productLists[i]["SaleRate"], false, obj.productLists[i]["TaxType"]);
                       

                    }

                    callKot(KotNo);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                 if (cst_id > 0)
                    {
                        setTimeout(function () {
                            $("#btnCash").focus();
                        });



                    }
                    $.uiUnlock();
                }

            });

            getcst_dis(cst_id);


        }

   
            function GetByItemCode(div, KotNo) {
            RestControls();
           // CheckKotType(KotNo)
            $("#txtblno").val(KotNo);
            $("#<%= RdoPaymode.ClientID %> input[type=radio]").prop('checked',false);    
    
            cst_id = $(div).find('.spn_cst_id').text();
            chkhomedel = $(div).find('.chkhd').text();
            if (cst_id == 0 ) {
                $("#trpaymode").hide(); 

                $("#dvotp").hide();
                $("#tdorderno").hide();
                if (chkhomedel == "HD") {
                    option = "HomeDelivery"
                }

                else {
                    option = "Dine"
                }

            }
            else {
                $("#dvotp").show();
                $("#trpaymode").show();
                $("#tdorderno").show();
                option = "Dine";
            }
            TableeId = KotNo;

            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "KotRaiseBill.aspx/GetKotDet",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], "", obj.productLists[i]["DisAmount"], obj.productLists[i]["Qty"], obj.productLists[i]["SaleRate"], false, obj.productLists[i]["TaxType"]);
                       

                    }


                    callKot(KotNo);





                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });





        }

            function CheckKotType(KotNo)
            {
                alert(KotNo);
            }
        function callKot(KotNo) {

            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "KotRaiseBill.aspx/GetKot",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    CrdCustSelId = "0";
                    CshCustSelName = "CASH";
                    $("#txtkotsteward").val(obj.KOT.EmpName);
                    $("#txtKotTable").val(obj.KOT.TableID);
                    $("#txtPax").val(obj.KOT.PaxNo);
                    $("#hdnsteward").val(obj.KOT.EmpCode);


                    if (obj.KOT.TakeAway == true) {

                        option = "TakeAway";
                        $("#chktakeway").prop('checked', true);
                    }
                    else {
                        option = "Dine";
                        $("#chktakeway").prop('checked', false);
                    }

                    if (chkhomedel == "HD" && cst_id==0)
                    {

                        option = "HomeDelivery";
                        $("#DelCharges").show();


                    }
           


                    $("#txtPax").attr("disabled", "disabled");
                    $("#txtKotTable").attr("disabled", "disabled");
                    $("#txtkotsteward").attr("disabled", "disabled");
                    $("#txtddlMobSearchBox").val(CshCustSelName);
                    $("#txtcustId").val(CrdCustSelId);
                    $("#txtcustId").val(cashcustcode);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });
        }


        function GetPluginData(Type) {

            if (Type == "CashCustomer") {


                var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
                $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));
                $("#txtcustId").val($("#ddlMobSearchBox option:selected").val());
                CshCustSelId = $("#ddlMobSearchBox option:selected").val();
                CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

                $("#CashCustomer").css("display", "block");

                if (EnableCustomerDiscount == 1) {
                    $("#dvdisper").val(Discount.toFixed(2));

                }

                $("#CustomerSearchWindow").hide();
                $("#dvProductList").show();
                //$("#dvBillWindow").hide();
                $("#dvHoldList").hide();
                $("#dvOrderList").hide();
                CommonCalculation("0");





            }
            else if (Type == "Accounts") {
                var customerId = $("#ddlCreditCustomers").val();
                CrdCustSelId = customerId;
                $("#hdnCreditCustomerId").val(customerId);


                $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
                CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



                $("#creditCustomer").css("display", "block");
            }
        }

        var tablenum = "0";
        $(document).ready(
            function () {
                $("#OnlineChknew").change(function () {
                    if ($("#OnlineChknew").prop('checked')) {
                        onlineoption = "OnlinePayment";
                        var FinalAmount = $("#txtFinalBillAmount").val();
                        $("#txtpaymentAmountnew").attr('readonly', 'readonly');
                        $("#txtCODAmountnew").attr('readonly', 'readonly');

                        $("#txtpaymentAmountnew").val(FinalAmount);
                        $("#txtCODAmountnew").val(0);
                        $("#codChknew").prop('checked', false);
                    }
                })
                $("#codChknew").change(function () {
                    if ($("#codChknew").prop('checked')) {
						onlineoption = "COD";
                        var FinalAmount = $("#txtFinalBillAmount").val();
                        $("#txtpaymentAmountnew").attr('readonly', 'readonly');
                        $("#txtCODAmountnew").attr('readonly', 'readonly');
                        $("#txtpaymentAmountnew").val(0);

                        $("#txtCODAmountnew").val(FinalAmount);
                        $("#OnlineChknew").prop('checked', false);
                    }
                })
                $.ajax({
                    type: "POST",
                    data: '{}',
                    url: "screen.aspx/BindBanks",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#ddlBankNew").html(obj.BankOptions);
                        $("#ddlBankNew option[value='" + DefaultBank + "']").prop("selected", true);

                    }
                });

                tablenum = $("#hdntbleno").val();
                cashcustcode = $("#hdncashcustid").val();
                cashcustName = $("#hdncashcustname").val();
                $("#txtcustId").val(cashcustcode);


                if ($("#hdndisplay").val() == "A") {

                    RestControls();
                    m_BillNowPrefix = "";
                    m_BillMode = "";
                    $("#ddlbillttype").removeAttr("disabled");
                    $("#btnCreditCard").css("background", "#f06671");
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers").val("");

                    $("#btnCash").css("background", "rgba(0, 0, 0, 0.9)");
                    $("#btnhold").css("background", "#f06671");

                    $("#screenDialog").dialog({
                        autoOpen: true,
                        closeOnEscape: false,
                        height: 600,
                        width: 1115,
                        resizable: false,
                        modal: false
                    });

                    if (tablenum > 0 && $("#hdncompnycustid").val() != 0) {

                        GetByItemCode2(tablenum);

                    }




                }
                else {

                }




                $('#KOT').click(function () {
                    window.location.href = 'manageKotScreen.aspx';
                    //window.top.close();
                    return false;
                });


                $('#settlement').click(function () {

                    window.location.href = 'settlement.aspx';
                    return false;
                });


                // $("#chkhone").click(
                // function () {
                //   if ($("#chkhone").prop('checked') == true) {
                //      $("#<%=ddlsteward.ClientID %>").removeAttr("disabled");
                //      $("#DelCharges").css("display", "block");

                //  }
                //  else {
                //     $("#<%=ddlsteward.ClientID %>").attr("disabled", "disabled");
                $("#DelCharges").css("display", "none");
                // }

                // });

                $("#chkhone").click(
                    function () {
                        if ($("#chkhone").prop('checked') == true) {
                            $("#<%=ddlsteward.ClientID %>").removeAttr("disabled");
                            //$("#DelCharges").css("display", "block");
                            option = "HomeDelivery"

                            if (minbillvalue == 0 || DeliveryCharges == 0) {
                                $("#DelCharges").show();
                                $("#dvDeliveryChrg").removeAttr("disabled");
                                $("#chkDelivery").show();
                                $("#lbldelcharges").show();

                            }
                            else {

                                $("#dvDeliveryChrg").val(DeliveryCharges);
                                $("#dvDeliveryChrg").attr("disabled", "disabled");
                                $("#chkDelivery").hide();
                                $("#lbldelcharges").hide();
                                $("#DelCharges").show();
                            }
                            CommonCalculation("1");

                        }
                        else {
                            $("#<%=ddlsteward.ClientID %>").attr("disabled", "disabled");
                            $("#DelCharges").css("display", "none");
                            option = "Dine";
                            CommonCalculation("1");


                        }

                    });


				$("#btnCash").css("display", "block");
				$("#btnCashBill").css("display", "none");
				$("#btnOnline").css("display", "none");
				$("#btnCreditCardBill").css("display", "none");
				$("#btnCreditBill").css("display", "none");

                if ($("#hdndd").val() == "C") {
                    $("#btnCash").css("display", "block");
                    $("#btnCashBill").css("display", "none");
                    $("#btnOnline").css("display", "none");
                    $("#btnCreditCardBill").css("display", "none");
                    $("#btnCreditBill").css("display", "none");
                }
                else {
                    $("#btnCash").css("display", "none");
                    $("#btnCashBill").css("display", "block");
                    $("#btnOnline").css("display", "block");
                    $("#btnCreditCardBill").css("display", "block");
                    $("#btnCreditBill").css("display", "block");
                }


                GetTable();




                $(window).keydown(function (e) {


                    switch (e.keyCode) {
                        case 112:   // F1 key is left or up

                            CashSave();

                            return false;
                        case 113: //F2 key is left or down

                            ClearData();

                            return false; //"return false" will avoid further events
                        case 114: //F3 key is left or down
                            ReturnItem();

                            return false;
                        case 119: //F8 key is left or down

                            CreditCardSave();


                            return false;

                        case 122: //F11 key is left or down
                            BillUnhold();
                            return false;

                        case 123: //F12 key is left or down
                            InsertHoldBill();
                            return false;

                        case 27: //Esc key is left or down
                            $("#ddlbillttype").val("0");
                            return false;


                    }
                    return; //using "return" other attached events will execute
                });




                $("#chkDelivery").change(function () {

                    //                if($('#chkDelivery').prop('checked') == true)

                    //                {
                    //                $("#DelCharges").show();
                    //                }
                    //                else{

                    //                $("#DelCharges").hide();
                    //                }

                });



                $("#btnMsgClose").click(
                    function () {
                        $("#txtblno").val("");
                        $("#txtblno").focus();
                        $("#dvbillSave").css("display", "none");
                        //if (tablenum <= 0)
                        //{ window.close(); }
                        if (tablenum > 0 && $("#hdncompnycustid").val() != 0) {
                            window.location = ("manageKotScreen.aspx");

                        }



                    }
                )
                    ;


                $("#btnReprint").click(
                    function () {

                        var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
                        if ($.trim(SelectedRow) == "") {
                            alert("No Bill is selected to Reprint");
                            return;
                        }

                        m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillNowPrefix')

                        $.uiLock('');
                        var PrintType = "RetailBill";

                        $.ajax({
                            type: "POST",
                            data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
                            url: "BillScreen.aspx/Reprint",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);


                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {

                                Printt(m_BillNowPrefix);
                                $.uiUnlock();

                            }

                        });





                    }
                );



                $("#colDvProductList").mouseenter(
                    function () {

                        $("#colProducts").slideUp(200);
                    }

                );

                $("#ddlCreditCustomers").val("").removeAttr("disabled");
                $("#ddlCreditCustomers").css({ "display": "none" });

                $("#ddlChosseCredit").css({ "display": "none" });
                $("#ddlChosseCredit").val(0);

                $.ajax({
                    type: "POST",
                    data: '{ }',
                    url: "BillScreen.aspx/GetAllBillSetting",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        BillBasicType = obj.setttingData.retail_bill;
                        TakeAwayDine = obj.setttingData.TakeAwayDine;
                        AllowServicetax = obj.setttingData.ServiceTax;
                        if (AllowServicetax == 1) {
                            Sertax = obj.setttingData.SerTax_Per;

                            AllowKKC = obj.setttingData.AllowKKC;

                            AllowSBC = obj.setttingData.AllowSBC;
                            KKC = obj.setttingData.KKC;
                            SBC = obj.setttingData.SBC;
                            $("#trSBC").show();
                            $("#trKKC").show();
                            $("#trServicetax").show();
                        }
                        else {
                            $("#trServicetax").hide();
                            Sertax = 0;
                            AllowKKC = false;
                            AllowSBC = false;
                            KKC = 0;
                            SBC = 0;
                            $("#trSBC").hide();
                            $("#trKKC").hide();
                        }

                        Takeaway = obj.setttingData.AlloServicetax_TakeAway;
                        HomeDelCharges = obj.setttingData.homedel_charges;

                        minbillvalue = obj.setttingData.min_bill_value;
                        DeliveryCharges = obj.setttingData.del_charges;
                        EnableCashCustomer = obj.setttingData.CashCustomer;
                        DefaultBank = obj.setttingData.defaultBankID;
                        RoundBillAmount = obj.setttingData.roundamt;

                        PrintShortName = obj.setttingData.shortname;
                        PrintRoute = obj.setttingData.PrintRoute;
                        FocAffect = obj.setttingData.focaffect;
                        NEgativeStock = obj.setttingData.NegtiveStock;

                        DefaultPaymode = obj.setttingData.defaultpaymodeID;
                        AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;

                        if (AllowDiscountOnBilling == 1) {
                            EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                            EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                            DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                            BackEndDiscount = obj.setttingData.Back_End_Discount;

                        }


                        if (HomeDelCharges == 1) {

                            if (minbillvalue == 0 || DeliveryCharges == 0) {
                                $("#DelCharges").show();
                                $("#dvDeliveryChrg").removeAttr("disabled");
                                $("#chkDelivery").show();
                                $("#lbldelcharges").show();

                            }
                            else {

                                $("#dvDeliveryChrg").val(DeliveryCharges);
                                // $("#dvDeliveryChrg").attr("disabled", "disabled");
                                $("#chkDelivery").hide();
                                $("#lbldelcharges").hide();
                                //$("#DelCharges").show();
                            }

                        }
                        else {
                            $("#DelCharges").hide();
                            $("#chkDelivery").hide();
                            $("#lbldelcharges").hide();
                            $("#dvDeliveryChrg").val(0);
                        }


                        if (EnableCashCustomer == 1) {

                            $("#btnCash").removeAttr('disabled')
                            //$("#txtMobSearchBox,#imgSrchCashCust").show();
                            $("#txtddlMobSearchBox").removeAttr('disabled');

                        }
                        else {

                            $("#btnCash").prop("disabled", true).css("background", "gray");
                            //$("#txtMobSearchBox,#imgSrchCashCust").hide();

                            $("#txtddlMobSearchBox").attr('disabled', 'disabled');
                        }


                        if (BillBasicType == "I") {

                            $("#vatIncOrExc").hide();
                        }
                        else {
                            $("#vatIncOrExc").show();
                        }



                        if (DiscountOnBillValue == 1) {

                            DiscountValues = obj.DiscountDetail;



                        }

                        if (EnableDiscountAmount == 1) {

                            $("#dvdisper").removeAttr('disabled');
                            $("#dvdiscount").removeAttr('disabled');

                            //if (cst_id != "10062") {

                            //    $("#dvdisper").val("0");

                            //}

                            //$("#dvdiscount").val("0");
                        }
                        else {

                            //$("#dvdisper").attr('disabled', 'disabled');
                            $("#dvdiscount").attr('disabled', 'disabled');
                            //if (cst_id != "10062") {

                            //    $("#dvdisper").val("0");

                            //}
                            //$("#dvdiscount").val("0");
                        }

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                    }

                });



                ValidateRoles();

                function ValidateRoles() {

                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                    for (var i = 0; i < arrRole.length; i++) {

                        if (arrRole[i] == "18") {
                            RoleForEditRate = true;


                        }
                        else if (arrRole[i] == "9") {
                            $("#btnShowScreen").click(
                                function () {
                                    RestControls();
                                    m_BillNowPrefix = "";
                                    m_BillMode = "";
                                    $("#ddlbillttype").removeAttr("disabled");
                                    $("#btnCreditCard").css("background", "#f06671");
                                    $("#ddlCreditCustomers").html("");
                                    $("#txtddlCreditCustomers").val("");

                                    $("#btnCash").css("background", "rgba(0, 0, 0, 0.9)");
                                    $("#btnhold").css("background", "#f06671");

                                    $("#screenDialog").dialog({
                                        autoOpen: true,
                                        closeOnEscape: false,

                                        height: 600,
                                        width: 1115,
                                        resizable: false,
                                        modal: false
                                    });

                                }

                            );

                        }

                        else if (arrRole[i] == "2") {
                            $("#btnDelete").show();

                            $("#btnDelete").click(
                                function () {

                                    var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');

                                    if ($.trim(SelectedRow) == "") {
                                        alert("No Bill is selected to Cancel");
                                        return;
                                    }

                                    var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
                                    if (confirm("Are You sure to delete this record")) {
                                        $.uiLock('');


                                        $.ajax({
                                            type: "POST",
                                            data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
                                            url: "BillScreen.aspx/Delete",
                                            contentType: "application/json",
                                            dataType: "json",
                                            success: function (msg) {

                                                var obj = jQuery.parseJSON(msg.d);

                                                BindGrid();
                                                alert("Bill is Canceled successfully.");

                                            },
                                            error: function (xhr, ajaxOptions, thrownError) {

                                                var obj = jQuery.parseJSON(xhr.responseText);
                                                alert(obj.Message);
                                            },
                                            complete: function () {
                                                $.uiUnlock();
                                            }
                                        });

                                    }


                                }
                            );

                        }




                        else if (arrRole[i] == "3") {
                            $("#btnEdit").show();
                            $("#btnEdit").click(
                                function () {
                                    RestControls();

                                    var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
                                    if ($.trim(SelectedRow) == "") {
                                        alert("No Product is selected to add");
                                        return;
                                    }
                                    CshCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code');
                                    CshCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Name');
                                    CrdCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_ID');
                                    CrdCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name');

                                    m_BillMode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillMode');


                                    $("#btnhold").css("background", "gray");




                                    if (CshCustSelId != 0) {

                                        $("#ddlMobSearchBox").html("<option value='" + CshCustSelId + "'>" + CshCustSelName + "</option>");
                                        $("#txtddlMobSearchBox").val(CshCustSelName);
                                        $("#CashCustomer").show();
                                        $("#lblCashCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCustAddress'));
                                    }


                                    $("#ddlbillttype").prop("disabled", true);


                                    if (m_BillMode == "Credit") {

                                        $("#btnCash").css("background", "gray");
                                        $("#btnCreditCard").css("background", "gray");

                                        $("#lblCreditCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CreditCustAddress'));

                                    }
                                    else if (m_BillMode == "CreditCard") {
                                        $("#btnCash").css("background", "gray");
                                        $("#btnCreditCard").css("background", "#f06671");

                                    }
                                    else if (m_BillMode == "Cash") {
                                        $("#btnCash").css("background", "rgba(0, 0, 0, 0.9)");
                                        $("#btnCreditCard").css("background", "gray");
                                    }

                                    var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');



                                    if (TableNo == "0") {
                                        option = "TakeAway";
                                    }
                                    else {
                                        option = "Dine";
                                    }



                                    //option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');




                                    //           $("#<%=ddloption.ClientID%>").val(option);
                                    //           if (option == "Dine") {
                                    //               $("#ddlTable").removeAttr("disabled");

                                    //               $("#ddlTable option").removeAttr("selected");

                                    //               $('#ddlTable option[value=' + TableNo + ']').prop('selected', 'selected');
                                    //           }

                                    //           else if (option == "HomeDelivery") {
                                    //               $("#ddlTable").hide();

                                    //               $("#ddlEmployees").show();
                                    //               $("#ddlTable option").removeAttr("selected");
                                    //               var empcode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'EmpCode');

                                    //               $('#ddlEmployees option[value=' + empcode + ']').prop('selected', 'selected');

                                    //               $("#DelCharges").show();
                                    //               $("#dvDeliveryChrg").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DeliveryCharges'));
                                    //               $("#dvDeliveryChrg").attr("disabled", "disabled");

                                    //           }


                                    $("#dvdisper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));
                                    $("#txtKotTable").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno'))
                                    $("#txtcustId").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code'));
                                    $("#txtKotDate").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'strBD'));
                                    $("#txtkotsteward").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'EmpName'));


                                    EditBill(m_BillNowPrefix);
                                    $("#dvsertaxper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'servalue'));

                                    $("#screenDialog").dialog({
                                        autoOpen: true,
                                        closeOnEscape: false,
                                        width: 850,
                                        resizable: false,
                                        modal: false
                                    });

                                }

                            );

                        }
                        else if (arrRole[i] == "10") {
                            $("#btnhold").click(
                                function () {

                                    InsertHoldBill();
                                });

                        }



                    }

                }



                $("#btnGetByItemCode").click(
                    function () {


                        var ItemCode = $("#txtItemCode");

                        if (ItemCode.val().trim() == "") {
                            ItemCode.focus();
                            return;
                        }


                        GetByItemCode(this, ItemCode.val());

                    }

                );


                $("#btnhome_del").click(
                    function () {

                        ClearControls();
                        window.location = ("assign_homedelvry.aspx");
                    }

                );

                $("#btnClear").click(
                    function () {

                        window.location = "BillScreen.aspx";
                    }

                );


                $("#ddlMobSearchBox").supersearch({
                    Type: "CashCustomer",
                    Caption: "CustName/Mobile ",
                    AccountType: "D",
                    Width: 50,
                    DefaultValue: 0,
                    Godown: 0
                });


                $(document).on("click", ".ui-icon-closethick", function (event) {


                    $("#ddlbillttype").val("0");

                });

                $("#txtblno").keyup(
                    function EnterEvent(e) {


                        var tblno = $(this).val();
                        if (tblno > 0 && e.keyCode == 13) {
                            $("#btnCash").focus();
                            GetByItemCode(0, tblno);


                        }
                    });
                setTimeout(function () {
                    $('#txtblno').focus();
                });
                $("#dvDeliveryChrg").keyup(
                    function EnterEvent(e) {

                        if (e.keyCode == 13) {
                            var regex = /^[0-9\.]*$/;


                            var value = jQuery.trim($(this).val());
                            var count = value.split('.');

                            if (value.length >= 1) {
                                if (!regex.test(value) || value <= 0 || count.length > 2) {

                                    $(this).val(0);

                                }
                            }

                            if (value > 100) {
                                $(this).val(100);

                            }

                            CommonCalculation("1");

                        }

                    });


                $("#dvdisper").keyup(
                    function EnterEvent(e) {

                        if (e.keyCode == 13) {
                            var regex = /^[0-9\.]*$/;


                            var value = jQuery.trim($(this).val());
                            var count = value.split('.');


                            if (value.length >= 1) {
                                if (!regex.test(value) || value <= 0 || count.length > 2) {

                                    $(this).val(0);


                                }
                            }

                            if (value > 100) {
                                $(this).val(100);

                            }




                            CommonCalculation("1");

                        }

                    });


                //             $("#dvdisper").keyup(
                //     function () {

                //         var regex = /^[0-9\.]*$/;


                //         var value = jQuery.trim($(this).val());
                //         var count = value.split('.');


                //         if (value.length >= 1) {
                //             if (!regex.test(value) || value <= 0 || count.length > 2) {

                //                 $(this).val(0);


                //             }
                //         }

                //         if (value > 100) {
                //             $(this).val(100);

                //         }

                //       
                //      

                //         CommonCalculation("1");

                //     }
                //     );

                $("#dvdiscount").keyup(
                    function EnterEvent(e) {
                        if (e.keyCode == 13) {
                            var regex = /^[0-9]*$/;


                            var value = jQuery.trim($(this).val());

                            //if (value.length >= 1) {
                            //    if (!regex.test(value) || value <= 0) {

                            //        $(this).val(0);


                            //    }
                            //}

                            var ttlAmt = $("#dvsbtotal").html();

                            if (value > Number(ttlAmt)) {
                                $(this).val(ttlAmt);

                            }

                            $("#dvdisper").val(($("#dvdiscount").val() * 100 / $("#dvsbtotal").html()).toFixed(2));
                            CommonCalculation("1");

                        }
                    }
                );

                $("#btnAddCustomer").click(function () {

                    $("#dvAddCustomer").dialog({
                        autoOpen: true,
                        closeOnEscape: false,
                        width: 580,
                        resizable: false,
                        modal: true
                    });

                    $("#dvAddCustomer").parent().addClass('kotcustomer-pop');

                });


				$("#btnCreditBilling").click(
                    function () {

						if (CrdCustSelId == 0) {

							alert("Please select Credit Customer");


							return;
						}

						var BillNowPrefix = "";
						var BillRemarks = "";

						var FinalAmt = $("#txtFinalBillAmount").val();
                        var CashRecieve = 0;
						var CrCardAmt = 0
						var COD = 0
						var OnlinePayment = 0
						var AmtTotal = 0;
						var TransectionID = 0;
						AmtTotal = $("#txtFinalBillAmount").val();
						TransectionID = $("#trnid").val();

						if ($("#txtKotBillNo").val() == "") {
							alert("First Choose BillNo for settlement");
							return;
						}

						var billtype = "Credit";


                        var cashamount = 0;


                        var CustomerId = CrdCustSelId;
                        var CustomerName = CrdCustSelName;
						var billmode = billtype;
						var NetAmt = $("#txtFinalBillAmount").val();

						//var CashAmt = $("#txtFinalBillAmount").val();
                        var CashAmt = 0;
						var creditAmt = $("#txtFinalBillAmount").val();
						var OnlinePayment = 0;

						var BIllValue = $("#dvsbtotal").html();


						var CreditBank = 0
						var CreditCardAmt = 0


						var cashcustcode = 0;
						var cashcustName = "";

						cashcustcode = 0;
						cashcustName = "CASH"

						var OTPval = "";


						var OrderNo = 0;
						var BIllValue = $("#dvsbtotal").html();

						var DisPer = $("#dvdisper").val();

						var lessdisamt = $("#dvdiscount").val();
						var Order_No = $("#txtorderno").val();
						var addtaxamt = $("#dvVat").html();
						var RoundAmt = 0;
						var hdnNetamt = $("#hdnnetamt").val();
						RoundAmt = Number(NetAmt) - Number(hdnNetamt);

						var DeliveryCharges = $("#dvDeliveryChrg").val();
						var EmpCode = $("#hdnsteward").val();

						var Order_No = $("#txtorderno").val();
						if (Order_No == "") {

							Order_No = 0;

						}


						var remarks = option;


						var Tableno = TableeId;
						var setatx = $("#dvTax").html();
						var ServiceTax = $("#dvsertaxper").html();
						var KKCPer = $("#dvKKCPer").html();
						var KKCAmt = $("#dvKKCAmt").html();
						var SBCPer = $("#dvSBCPer").html();
						var SBCAmt = $("#dvSBCAmt").html();

						var ItemCode = [];
						var Price = [];
						var Qty = [];
						var Tax = [];
						var OrgSaleRate = [];
						var PAmt = [];
						var Ptax = [];
						var PSurChrg = [];
						var ItemRemarks = [];
						var SurPer = [];
						var free = [];
						if (ProductCollection.length == 0) {
							alert("Please first Select ProductsFor Billing");

							return;
						}

						for (var i = 0; i < ProductCollection.length; i++) {

							ItemCode[i] = ProductCollection[i]["ItemCode"];
							Qty[i] = ProductCollection[i]["Qty"];
							Price[i] = ProductCollection[i]["Price"];
							Tax[i] = ProductCollection[i]["TaxCode"];
							OrgSaleRate[i] = ProductCollection[i]["Price"];
							PAmt[i] = ProductCollection[i]["ProductAmt"];
							Ptax[i] = ProductCollection[i]["Producttax"];
							PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
							//ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
							ItemRemarks[i] = ProductCollection[i]["Tax_type"];
							SurPer[i] = ProductCollection[i]["SurVal"];
							free[i] = ProductCollection[i]["Free"];
						}

						TaxDen = [];
						VatAmtDen = [];
						VatDen = [];
						SurDen = [];

						$("div[name='tax']").each(
							function (y) {
								TaxDen[y] = $(this).html();
							}
						);
						$("div[name='amt']").each(
							function (z) {
								VatAmtDen[z] = $(this).html();
							}
						);
						$("div[name='vat']").each(
							function (a) {
								VatDen[a] = $(this).html();
							}
						);
						$("div[name='sur']").each(function (a) {
							SurDen[a] = $(this).html();
						});

						$.uiLock();
						var Result = "";
						var CustomerUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
						try {
							var CustomerPhone = CustomerUrl[4].replace('cashcustmob=', '');
						} catch (e) {
							CustomerPhone = '0';
						}
						OTPval = $("#textotp").val();

						var cstid = 0;



						////////////////////


						$.ajax({
                            type: "POST",
                            data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","FreeArr":"' + free + '","CustomerPhone":"' + CustomerPhone + '","Order_No": "' + Order_No + '","OTP": "' + OTPval + '","Rdopaymode": "' + rdopaymode + '","cstid":"' + cstid + '"}',
							url: "kotRaiseBill.aspx/InsertUpdate",
							contentType: "application/json",
							dataType: "json",
							success: function (msg) {

								var obj = jQuery.parseJSON(msg.d);
								BillNowPrefix = obj.BNF;
								
								if (obj.Status == -11) {
									alert("You don't have permission to perform this action..Consult Admin Department.");
									return;
								}


								if (obj.Status == -5) {
									alert("Please Login Again and Try Again..");
									return;
								}

								if (obj.Status == 0) {
									alert("An Error Occured. Please try again Later");
									return;

								}
								
								else {

									$("#dvCreditWindow").dialog('close');
									Result = "Yes";
									$("#lblAmmmt").html(NetAmt);
									$("#dvbillSave").toggle();

									setTimeout(function () {
										$("#btnMsgClose").focus();
									});

									// Printt(BillNowPrefix);
									$.uiUnlock();

									BindGrid();
									$("#tbProductInfo tr").remove();

								}

								ProductCollection.length = 0;
								$("#dvsbtotal").text(0.00);
								$("#dvnetAmount").text(0.00);
							},
							error: function (xhr, ajaxOptions, thrownError) {

								var obj = jQuery.parseJSON(xhr.responseText);

								if (obj.Message == 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.') {

									alert('Previous bill is under process.Please Wait..!');

								}

								else {

									alert(obj.Message);
								}
							},
							complete: function () {
								$("#btnBillWindowOk").removeAttr('disabled');

								if (Result == "Yes") {

									UpdateKotDeActive(TableeId);
									var Tableval = localStorage.getItem("Tableno");
									if (Tableval == '0') {
									}
									else
										RestControls();


								}

                               // InsertOnlineOtherPayment(BillNowPrefix);
								GetTable();
								$.uiUnlock();



							}

						});


					}
				);


				$("#btnCreditCardBilling").click(
					function () {
						var BillNowPrefix = "";
						var BillRemarks = "";

						var FinalAmt = $("#txtFinalBillAmount").val();
                        var CashRecieve = 0;
						var CrCardAmt = $("#txtFinalBillAmount").val();
						var COD = 0
						var OnlinePayment = 0
						var AmtTotal = 0;
						var TransectionID = 0;
						AmtTotal = $("#txtFinalBillAmount").val();
						TransectionID = $("#trnid").val();

						if ($("#txtKotBillNo").val() == "") {
							alert("First Choose BillNo for settlement");
							return;
						}

						var billtype = "CreditCard";


						var cashamount = 0


						var CustomerId = $("#ddlBankNew").val();
						var CustomerName = $("#ddlBankNew option:selected").text();
						var billmode = billtype;
						var NetAmt = $("#txtFinalBillAmount").val();

						//var CashAmt = $("#txtFinalBillAmount").val();
                        var CashAmt = 0;
						var creditAmt = 0;
						var OnlinePayment = 0;

						var BIllValue = $("#dvsbtotal").html();


                        var CreditBank = $("#ddlBankNew").val();

						if (CreditBank == "") {
							alert("Choose Bank");
							
							return;
						}

						var CreditCardAmt = $("#txtFinalBillAmount").val();


						var cashcustcode = 0;
						var cashcustName = "";

						cashcustcode = 0;
						cashcustName = "CASH"

						var OTPval = "";


						var OrderNo = 0;
						var BIllValue = $("#dvsbtotal").html();

						var DisPer = $("#dvdisper").val();

						var lessdisamt = $("#dvdiscount").val();
						var Order_No = $("#txtorderno").val();
						var addtaxamt = $("#dvVat").html();
						var RoundAmt = 0;
						var hdnNetamt = $("#hdnnetamt").val();
						RoundAmt = Number(NetAmt) - Number(hdnNetamt);

						var DeliveryCharges = $("#dvDeliveryChrg").val();
						var EmpCode = $("#hdnsteward").val();

						var Order_No = $("#txtorderno").val();
						if (Order_No == "") {

							Order_No = 0;

						}


						var remarks = option;


						var Tableno = TableeId;
						var setatx = $("#dvTax").html();
						var ServiceTax = $("#dvsertaxper").html();
						var KKCPer = $("#dvKKCPer").html();
						var KKCAmt = $("#dvKKCAmt").html();
						var SBCPer = $("#dvSBCPer").html();
						var SBCAmt = $("#dvSBCAmt").html();

						var ItemCode = [];
						var Price = [];
						var Qty = [];
						var Tax = [];
						var OrgSaleRate = [];
						var PAmt = [];
						var Ptax = [];
						var PSurChrg = [];
						var ItemRemarks = [];
						var SurPer = [];
						var free = [];
						if (ProductCollection.length == 0) {
							alert("Please first Select ProductsFor Billing");

							return;
						}

						for (var i = 0; i < ProductCollection.length; i++) {

							ItemCode[i] = ProductCollection[i]["ItemCode"];
							Qty[i] = ProductCollection[i]["Qty"];
							Price[i] = ProductCollection[i]["Price"];
							Tax[i] = ProductCollection[i]["TaxCode"];
							OrgSaleRate[i] = ProductCollection[i]["Price"];
							PAmt[i] = ProductCollection[i]["ProductAmt"];
							Ptax[i] = ProductCollection[i]["Producttax"];
							PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
							//ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
							ItemRemarks[i] = ProductCollection[i]["Tax_type"];
							SurPer[i] = ProductCollection[i]["SurVal"];
							free[i] = ProductCollection[i]["Free"];
						}

						TaxDen = [];
						VatAmtDen = [];
						VatDen = [];
						SurDen = [];

						$("div[name='tax']").each(
							function (y) {
								TaxDen[y] = $(this).html();
							}
						);
						$("div[name='amt']").each(
							function (z) {
								VatAmtDen[z] = $(this).html();
							}
						);
						$("div[name='vat']").each(
							function (a) {
								VatDen[a] = $(this).html();
							}
						);
						$("div[name='sur']").each(function (a) {
							SurDen[a] = $(this).html();
						});

						$.uiLock();
						var Result = "";
						var CustomerUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
						try {
							var CustomerPhone = CustomerUrl[4].replace('cashcustmob=', '');
						} catch (e) {
							CustomerPhone = '0';
						}
						OTPval = $("#textotp").val();

						var cstid = 0;



						////////////////////


						$.ajax({
                            type: "POST",
                            data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","FreeArr":"' + free + '","CustomerPhone":"' + CustomerPhone + '","Order_No": "' + Order_No + '","OTP": "' + OTPval + '","Rdopaymode": "' + rdopaymode + '","cstid":"' + cstid + '"}',
							url: "kotRaiseBill.aspx/InsertUpdate",
							contentType: "application/json",
							dataType: "json",
							success: function (msg) {

								var obj = jQuery.parseJSON(msg.d);
								BillNowPrefix = obj.BNF;
								
								if (obj.Status == -11) {
									alert("You don't have permission to perform this action..Consult Admin Department.");
									return;
								}


								if (obj.Status == -5) {
									alert("Please Login Again and Try Again..");
									return;
								}

								if (obj.Status == 0) {
									alert("An Error Occured. Please try again Later");
									return;

								}

								else {

									$("#dvCreditCardWindow").dialog('close');
									
									Result = "Yes";
									$("#lblAmmmt").html(NetAmt);
									$("#dvbillSave").toggle();

									setTimeout(function () {
										$("#btnMsgClose").focus();
									});

									// Printt(BillNowPrefix);
									$.uiUnlock();

									BindGrid();
									$("#tbProductInfo tr").remove();

								}

								ProductCollection.length = 0;
								$("#dvsbtotal").text(0.00);
								$("#dvnetAmount").text(0.00);
							},
							error: function (xhr, ajaxOptions, thrownError) {

								var obj = jQuery.parseJSON(xhr.responseText);

								if (obj.Message == 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.') {

									alert('Previous bill is under process.Please Wait..!');

								}

								else {

									alert(obj.Message);
								}
							},
							complete: function () {
								$("#btnBillWindowOk").removeAttr('disabled');

								if (Result == "Yes") {

									UpdateKotDeActive(TableeId);
									var Tableval = localStorage.getItem("Tableno");
									if (Tableval == '0') {
									}
									else
										RestControls();


								}

								//InsertOnlineOtherPayment(BillNowPrefix);
								GetTable();
								$.uiUnlock();



							}

						});


					}
				);



                $("#btnCreditCardBill").click(
                    function () {
                        $("#dvCreditCardWindow").dialog({

                            modal: true



                        });




                    }
                );

				

				$("#btnOnlineBilling").click(
					function () {
						var BillNowPrefix = "";
						var BillRemarks = "";

						var FinalAmt = $("#txtFinalBillAmount").val();
                        var CashRecieve = 0;
						var CrCardAmt = 0
						var COD = 0
						var OnlinePayment = 0
						var AmtTotal = 0;
						var TransectionID = 0;
						AmtTotal = $("#txtFinalBillAmount").val();
						TransectionID = $("#trnid").val();

						if ($("#txtKotBillNo").val() == "") {
							alert("First Choose BillNo for settlement");
							return;
						}

						var billtype = "OnlinePayment";


						var cashamount = 0;
						if ($("#ddlOtherPaymentnew").val() === "0") {
							alert("Please select online payment mode");
							return;
						}
						if ($("#ddlOtherPaymentnew").val() != 0) {

							cstid = $("#ddlOtherPaymentnew").val();


						}

						if ($("#txtCODAmountnew").val() == 0 && $("#txtpaymentAmountnew").val() == 0) {
							alert("Choose Atleast One Option Either COD or ONLINE");
							return;
						}


						var CustomerId = "";
						var CustomerName = "";
						var billmode = billtype;
						var NetAmt = $("#txtFinalBillAmount").val();

						//var CashAmt = $("#txtFinalBillAmount").val();
						var CashAmt =0;
						var creditAmt = 0;
						var OnlinePayment = 0;
                        if (onlineoption == "COD") {
                            CashAmt = $("#txtCODAmountnew").val();
                        }

                       
							OnlinePayment = $("#txtpaymentAmountnew").val();
                     
                       


						var BIllValue = $("#dvsbtotal").html();


						var CreditBank = 0
						var CreditCardAmt = 0


						var cashcustcode = 0;
						var cashcustName = "";

						cashcustcode = 0;
						cashcustName = "CASH"

						var OTPval = "";


						var OrderNo = 0;
						var BIllValue = $("#dvsbtotal").html();

						var DisPer = $("#dvdisper").val();

						var lessdisamt = $("#dvdiscount").val();
						var Order_No = $("#txtorderno").val();
						var addtaxamt = $("#dvVat").html();
						var RoundAmt = 0;
						var hdnNetamt = $("#hdnnetamt").val();
						RoundAmt = Number(NetAmt) - Number(hdnNetamt);

						var DeliveryCharges = $("#dvDeliveryChrg").val();
						var EmpCode = $("#hdnsteward").val();

						var Order_No = $("#txtorderno").val();
						if (Order_No == "") {

							Order_No = 0;

						}


						var remarks = option;


						var Tableno = TableeId;
						var setatx = $("#dvTax").html();
						var ServiceTax = $("#dvsertaxper").html();
						var KKCPer = $("#dvKKCPer").html();
						var KKCAmt = $("#dvKKCAmt").html();
						var SBCPer = $("#dvSBCPer").html();
						var SBCAmt = $("#dvSBCAmt").html();

						var ItemCode = [];
						var Price = [];
						var Qty = [];
						var Tax = [];
						var OrgSaleRate = [];
						var PAmt = [];
						var Ptax = [];
						var PSurChrg = [];
						var ItemRemarks = [];
						var SurPer = [];
						var free = [];
						if (ProductCollection.length == 0) {
							alert("Please first Select ProductsFor Billing");

							return;
						}

						for (var i = 0; i < ProductCollection.length; i++) {

							ItemCode[i] = ProductCollection[i]["ItemCode"];
							Qty[i] = ProductCollection[i]["Qty"];
							Price[i] = ProductCollection[i]["Price"];
							Tax[i] = ProductCollection[i]["TaxCode"];
							OrgSaleRate[i] = ProductCollection[i]["Price"];
							PAmt[i] = ProductCollection[i]["ProductAmt"];
							Ptax[i] = ProductCollection[i]["Producttax"];
							PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
							//ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
							ItemRemarks[i] = ProductCollection[i]["Tax_type"];
							SurPer[i] = ProductCollection[i]["SurVal"];
							free[i] = ProductCollection[i]["Free"];
						}

						TaxDen = [];
						VatAmtDen = [];
						VatDen = [];
						SurDen = [];

						$("div[name='tax']").each(
							function (y) {
								TaxDen[y] = $(this).html();
							}
						);
						$("div[name='amt']").each(
							function (z) {
								VatAmtDen[z] = $(this).html();
							}
						);
						$("div[name='vat']").each(
							function (a) {
								VatDen[a] = $(this).html();
							}
						);
						$("div[name='sur']").each(function (a) {
							SurDen[a] = $(this).html();
						});

						$.uiLock();
						var Result = "";
						var CustomerUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
						try {
							var CustomerPhone = CustomerUrl[4].replace('cashcustmob=', '');
						} catch (e) {
							CustomerPhone = '0';
						}
						OTPval = $("#textotp").val();

						var cstid = 0;



                        ////////////////////
                        
						$.ajax({
							type: "POST",
							data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","FreeArr":"' + free + '","CustomerPhone":"' + CustomerPhone + '","Order_No": "' + Order_No + '","OTP": "' + OTPval + '","Rdopaymode": "' + rdopaymode + '","cstid":"' + cstid + '"}',
							url: "kotRaiseBill.aspx/InsertUpdate",
							contentType: "application/json",
							dataType: "json",
							success: function (msg) {

								var obj = jQuery.parseJSON(msg.d);
								BillNowPrefix = obj.BNF;
								
								if (obj.Status == -11) {
									alert("You don't have permission to perform this action..Consult Admin Department.");
									return;
								}


								if (obj.Status == -5) {
									alert("Please Login Again and Try Again..");
									return;
								}

								if (obj.Status == 0) {
									alert("An Error Occured. Please try again Later");
									return;

								}

								else {
									$("#dvOnlineWindow").dialog('close');
									
									Result = "Yes";
									$("#lblAmmmt").html(NetAmt);
									$("#dvbillSave").toggle();

									setTimeout(function () {
										$("#btnMsgClose").focus();
									});

									// Printt(BillNowPrefix);
									$.uiUnlock();

									BindGrid();
									$("#tbProductInfo tr").remove();

								}

								ProductCollection.length = 0;
								$("#dvsbtotal").text(0.00);
								$("#dvnetAmount").text(0.00);
							},
							error: function (xhr, ajaxOptions, thrownError) {

								var obj = jQuery.parseJSON(xhr.responseText);

								if (obj.Message == 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.') {

									alert('Previous bill is under process.Please Wait..!');

								}

								else {

									alert(obj.Message);
								}
							},
							complete: function () {
								$("#btnBillWindowOk").removeAttr('disabled');

								if (Result == "Yes") {

									UpdateKotDeActive(TableeId);
									var Tableval = localStorage.getItem("Tableno");
									if (Tableval == '0') {
									}
									else
										RestControls();


								}

								 InsertOnlineOtherPayment(BillNowPrefix);
								GetTable();
								$.uiUnlock();



							}

						});


					}
				);






				$("#btnCashBill").click(
					function () {
						var BillNowPrefix = "";
                        var BillRemarks = "";
                        
                        var FinalAmt = $("#txtFinalBillAmount").val();
                        var CashRecieve = $("#txtFinalBillAmount").val();
                        var CrCardAmt = 0
                        var COD = 0
                        var OnlinePayment = 0
                        var AmtTotal = 0;
                        var TransectionID = 0;
                        AmtTotal = $("#txtFinalBillAmount").val();
                        TransectionID = $("#trnid").val();

						if ($("#txtKotBillNo").val() == "") {
                            alert("First Choose BillNo for settlement");
                            return;
                        }

                        var billtype = "Cash";


                        var cashamount = $("#txtFinalBillAmount").val();


                        var CustomerId = "";
                        var CustomerName = "";
                        var billmode = billtype;
                        var NetAmt = $("#txtFinalBillAmount").val();

                        //var CashAmt = $("#txtFinalBillAmount").val();
                        var CashAmt = $("#txtFinalBillAmount").val();
                        var creditAmt = 0;
                        var OnlinePayment = 0;

                        var BIllValue = $("#dvsbtotal").html();


                        var CreditBank = 0
                        var CreditCardAmt = 0


                        var cashcustcode = 0;
                        var cashcustName = "";

                        cashcustcode = 0;
                        cashcustName = "CASH"

                        var OTPval = "";


                        var OrderNo = 0;
						var BIllValue = $("#dvsbtotal").html();

                        var DisPer = $("#dvdisper").val();

                        var lessdisamt = $("#dvdiscount").val();
                        var Order_No = $("#txtorderno").val();
                        var addtaxamt = $("#dvVat").html();
                        var RoundAmt = 0;
                        var hdnNetamt = $("#hdnnetamt").val();
                        RoundAmt = Number(NetAmt) - Number(hdnNetamt);

                        var DeliveryCharges = $("#dvDeliveryChrg").val();
                        var EmpCode = $("#hdnsteward").val();

                        var Order_No = $("#txtorderno").val();
                        if (Order_No == "") {

                            Order_No = 0;

                        }


                        var remarks = option;


                        var Tableno = TableeId;
                        var setatx = $("#dvTax").html();
                        var ServiceTax = $("#dvsertaxper").html();
                        var KKCPer = $("#dvKKCPer").html();
                        var KKCAmt = $("#dvKKCAmt").html();
                        var SBCPer = $("#dvSBCPer").html();
                        var SBCAmt = $("#dvSBCAmt").html();

                        var ItemCode = [];
                        var Price = [];
                        var Qty = [];
                        var Tax = [];
                        var OrgSaleRate = [];
                        var PAmt = [];
                        var Ptax = [];
                        var PSurChrg = [];
                        var ItemRemarks = [];
                        var SurPer = [];
                        var free = [];
                        if (ProductCollection.length == 0) {
                            alert("Please first Select ProductsFor Billing");

                            return;
                        }

                        for (var i = 0; i < ProductCollection.length; i++) {

                            ItemCode[i] = ProductCollection[i]["ItemCode"];
                            Qty[i] = ProductCollection[i]["Qty"];
                            Price[i] = ProductCollection[i]["Price"];
                            Tax[i] = ProductCollection[i]["TaxCode"];
                            OrgSaleRate[i] = ProductCollection[i]["Price"];
                            PAmt[i] = ProductCollection[i]["ProductAmt"];
                            Ptax[i] = ProductCollection[i]["Producttax"];
                            PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                            //ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                            ItemRemarks[i] = ProductCollection[i]["Tax_type"];
                            SurPer[i] = ProductCollection[i]["SurVal"];
                            free[i] = ProductCollection[i]["Free"];
                        }

                        TaxDen = [];
                        VatAmtDen = [];
                        VatDen = [];
                        SurDen = [];

                        $("div[name='tax']").each(
                            function (y) {
                                TaxDen[y] = $(this).html();
                            }
                        );
                        $("div[name='amt']").each(
                            function (z) {
                                VatAmtDen[z] = $(this).html();
                            }
                        );
                        $("div[name='vat']").each(
                            function (a) {
                                VatDen[a] = $(this).html();
                            }
                        );
                        $("div[name='sur']").each(function (a) {
                            SurDen[a] = $(this).html();
                        });

                        $.uiLock();
                        var Result = "";
                        var CustomerUrl = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&')
                        try {
                            var CustomerPhone = CustomerUrl[4].replace('cashcustmob=', '');
                        } catch (e) {
                            CustomerPhone = '0';
                        }
                        OTPval = $("#textotp").val();

                        var cstid = 0;



                        ////////////////////


						$.ajax({
                            type: "POST",
							data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","FreeArr":"' + free + '","CustomerPhone":"' + CustomerPhone + '","Order_No": "' + Order_No + '","OTP": "' + OTPval + '","Rdopaymode": "' + rdopaymode + '","cstid":"' + cstid + '"}',
                            url: "kotRaiseBill.aspx/InsertUpdate",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);
                                BillNowPrefix = obj.BNF;
								
                                if (obj.Status == -11) {
                                    alert("You don't have permission to perform this action..Consult Admin Department.");
                                    return;
                                }


                                if (obj.Status == -5) {
                                    alert("Please Login Again and Try Again..");
                                    return;
                                }

                                if (obj.Status == 0) {
                                    alert("An Error Occured. Please try again Later");
                                    return;

                                }

                                else {

                                   
                                    Result = "Yes";
                                    $("#lblAmmmt").html(NetAmt);
                                    $("#dvbillSave").toggle();

                                    setTimeout(function () {
                                        $("#btnMsgClose").focus();
                                    });

                                    // Printt(BillNowPrefix);
                                    $.uiUnlock();

                                    BindGrid();
                                    $("#tbProductInfo tr").remove();

                                }

                                ProductCollection.length = 0;
                                $("#dvsbtotal").text(0.00);
                                $("#dvnetAmount").text(0.00);
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);

                                if (obj.Message == 'Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 0, current count = 1.') {

                                    alert('Previous bill is under process.Please Wait..!');

                                }

                                else {

                                    alert(obj.Message);
                                }
                            },
                            complete: function () {
                                $("#btnBillWindowOk").removeAttr('disabled');

                                if (Result == "Yes") {

                                    UpdateKotDeActive(TableeId);
                                    var Tableval = localStorage.getItem("Tableno");
                                    if (Tableval == '0') {
                                    }
                                    else
                                        RestControls();


                                }

                               // InsertOnlineOtherPayment(BillNowPrefix);
                                GetTable();
                                $.uiUnlock();



                            }

                        });


					}
				);



				$("#btnCreditBill").click(
					function () {
						$("#dvCreditWindow").dialog({

							modal: true



						});




					}
				);

				$("#btnOnline").click(
					function () {
						$("#txtpaymentAmountnew").val($("#txtFinalBillAmount").val());

						$("#OnlineChknew").prop('checked', true);
						$("#dvOnlineWindow").dialog({

							modal: true



						});




					}
				);




				$("#btnAddCreditCustomer").click(function () {

					$("#dvAddCustomer").dialog({
						autoOpen: true,
						closeOnEscape: false,
						width: 580,
						resizable: false,
						modal: true
					});



				});



             $("#btnCash").click(
           function () {


               if ($("#ddlbillttype").val() == 0 && cst_id=='-1')
               {

                   alert("Please Select PayMode.")
                   return false;

               }
               // TenderAmount();
               $("#btnMsgClose").focus();
               InsertUpdate();

           });





             function ClearData() {
                 m_BillNowPrefix = "";
                 m_BillMode = "";
                 $("#ddlbillttype").removeAttr("disabled");
                 $("#btnCreditCard").css("background", "#f06671");
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");
                 $("#ddlChosseCredit").val(0);
                 $("#btnCash").css("background", "rgba(0, 0, 0, 0.9)");
                 $("#btnhold").css("background", "#f06671");

                 RestControls();
             }
























             //if (cst_id != "10062") {
               
             //    $("#dvdisper").val("0");
             //    $("#dvdiscount").val("0");

             //}

        

             $("#btnSearch").click(
           function () {

               var Keyword = $("#txtSearch");
               if (Keyword.val().trim() != "") {
                   $("#colProducts").slideDown(200);

               }
               else {
                   Keyword.focus();
               }
               $("#txtSearch").val("");

           });



             $("#txtSearch").keyup(
             function (event) {

                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {


                     var Keyword = $("#txtSearch");
                     if (Keyword.val().trim() != "") {
                         $("#colProducts").slideDown(200);

                         $("#txtSearch").val("");
                     }
                     else {
                         Keyword.focus();
                     }


                 }


             }

             );










             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindCategories",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                     $("#categories").html(obj.categoryData);
                     //                     if (AllowServicetax == 1) {
                     //                         Sertax = obj.setttingData.SerTax;
                     //                     }
                     //                     else {
                     //                         Sertax = 0;
                     //                     }
                     //  Takeaway = obj.setttingData.TakeAway;

                     Takeawaydefault = obj.setttingData.TakeAwayDefault;

                     var CatId = obj.CategoryId;


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
         );


             function ClearControls() {
                 $("#txtPax").val("");
                 $("#txtKotTable").val("");
                 $("#txtkotsteward").val("");
                 $("#txtKotDate").val("");
                 $("#txtcustId").val("");
                 $("#txtddlMobSearchBox").val("");
                 $("#dvdisper").val(0);
                 $("#dvdiscount").val(0);
                 $("#dvnetAmount").html("0");
                 $("#dvRound").html("0");
                 $("#dvTax").html("0");
                 $("#dvsertaxper").html("0");
                 $("#dvKKCPer").html("0");
                 $("#dvKKCAmt").html("0");
                 $("#dvSBCPer").html("0");
                 $("#dvSBCAmt").html("0");
                 $("#dvsbtotal").html("0");
                 ProductCollection = [];
                 $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
             }
             
             $(document).on("keyup", "#ddlbillttype", function (event) {

     

                     var keycode = (event.keyCode ? event.keyCode : event.which);
                     if(keycode=='13')
                     {
                 if ($(this).val() != 0) {
                     
                 $("#dvBillWindow").dialog({
                     autoOpen: true,
                     top:50,
                     width: 356,
                     resizable: false,
                     modal: false,
                     open: function (event, ui) {
                         $('.ui-dialog').css('z-index', 100);
                         $('.ui-widget-overlay').css('z-index', 100);
                     },
                 });
            
             
                 $(".cls_paymode").show();

                 }
                else if ($(this).val() == 0) {
                     $(".cls_paymode").hide();
                 }

						


            if ($(this).val() == "Credit") {


                $("#dvOuter_ddlCreditCustomers").show();
                $("#lblCashHeading").text("Receipt Amt:");
                $("#ddlbillttype option[value='Credit']").prop("selected", true);
                $("#txtCashReceived").val("0").prop("readonly", false);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", true);
                $("#Text16").val("").prop("readonly", true);
                $("#ddlType").prop("disabled", true);
                $("#ddlOtherPayment").hide();
                $("#rdoCOD").hide();
                $("#rdoOnline").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                $("#COD").hide();
                $("#Online").hide();

                $("#txtpaymentAmount").hide();

                $("#No").hide();
                $("#Amt").hide();
                $("#Coupans").hide();
                $("#OP").hide();
                $("#coupan").hide();
                $("#holder input").hide();
                $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#ddlBank").prop("disabled", true);
                $("#ddlChosseCredit").show();
                $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                $("#lblCustomer").show();
                var FinalAmount = $("#txtFinalBillAmount").val();
                $("#txtCashReceived").val(parseFloat(FinalAmount).toFixed(2))
                $("#txtCashReceived").hide();
                $("#lblCashHeading").hide();
                $("#Text13").hide();
                $("#Text14").hide();
                $("#Text15").hide();
                $("#Text16").hide();
                $("#ddlType").hide();

                $("#CrCardlbl").hide();
                $("#CClbl").hide();
                $("#lblbank").hide();
                $("#ddlBank").hide();
                $("#ddlType").hide();
                $("#lbltype").hide();
                $("#txtBalanceReturn").val(0);
                $("#tr_transection").hide();
            }
            else if ($(this).val() == "CreditCard") {
                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");
                $("#Coupans").hide();
                $("#OP").hide();
                $("#coupan").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#COD").hide();
                $("#Online").hide();
                $("#rdoOnline").hide();
                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");
                $("#ddlType").prop("disabled", true);
                $("#ddlOtherPayment").hide();
                $("#txtpaymentAmount").hide();
                $("#rdoCOD").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                $("#ddlChosseCredit").hide();
                $("#holder input").hide(); $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#codChk").hide();
                $("#OnlineChk").hide();
                //                   $("#ddlOtherPayment").hide();

                $("#txtCashReceived").val("0").prop("readonly", true);
                $("#Text13").val("0").prop("readonly", false);
                $("#Text14").val("").prop("readonly", false);
                $("#Text15").val("0").prop("readonly", false);
                $("#Text16").val("").prop("readonly", false);
                $("#ddlType").prop("disabled", false);
                $("#ddlBank").prop("disabled", false);
                $("#Text13").val($("#txtFinalBillAmount").val());
                $("#txtBalanceReturn").val(" ");
                $("#lblCustomer").hide();
                $("#CrCardlbl").show();
                $("#CClbl").show();
                $("#lblbank").show();
                $("#ddlBank").show();
                $("#ddlType").show();
                $("#lbltype").show();
                $("#Text13").show();
                $("#Text14").show();
                $("#Text16").show();
                $("#txtBalanceReturn").val(0);
                $("#tr_transection").hide();
            }

            else if ($(this).val() == "Cash") {


                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");
                $("#Coupans").hide();
                $("#OP").hide();
                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");
                $("#coupan").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                $("#ddlChosseCredit").hide();
                $("#ddlType").prop("disabled", true);
                $("#ddlOtherPayment").hide();
                $("#COD").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#Online").hide();
                $("#txtpaymentAmount").hide();
                $("#rdoCOD").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                //                   $("#ddlOtherPayment").hide();
                $("#holder input").hide();
                $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#rdoOnline").hide();
                $("#txtCashReceived").val("0").prop("readonly", false);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", true);
                $("#Text16").val("").prop("readonly", true);
                $("#ddlType").prop("disabled", true);
                $("#ddlBank").prop("disabled", true);
                $("#txtCashReceived").val($("#txtFinalBillAmount").val());
                $("#txtCashReceived").show();
                $("#txtBalanceReturn").val(" ");
                $("#lblCustomer").hide();
                $("#CrCardlbl").hide();
                $("#CClbl").hide();
                $("#lblbank").hide();
                $("#ddlBank").hide();
                $("#ddlType").hide();
                $("#lbltype").hide();
                $("#lblCashHeading").show();
                $("#txtBalanceReturn").val(0);
                $("#Text13").hide();
                $("#Text14").hide();
                $("#tr_transection").hide();
            }
            else if ($(this).val() == "OnlinePayment") {

                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");

                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");
                $("#coupan").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                $("#ddlChosseCredit").hide();
                $("#COD").show();
                $("#Online").show();
                $("#No").show();
                $("#Amt").show();
                $("#ddlOtherPayment").show();
                $("#txtpaymentAmount").show();
                // $("#rdoOnline").show();
                //$("#rdoCOD").show();
                if ($("#txtCODAmount").val() != 0) {
                    onlineoption = "COD"


                }
                $("#codChk").show();
                $("#OnlineChk").show();
                $("#Coupans").val("0").prop("readonly", true);
                $("#Td1").show();
                $("#txtCODAmount").show();
                $("#holder2").hide();
                $("#holder").hide();
                $("#No").hide();
                $("#Amt").hide();

                //                 if ($("#rdoOnline").prop('checked') == true) {
                //                     onlineoption = "OnlinePayment"
                //                 }

                $("#Coupans").show();
                $("#coupan").show();
                $("#OP").show();
                $("#txtCashReceived").val("0").prop("readonly", true);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", false);
                $("#Text16").val("").prop("readonly", false);
                $("#ddlType").prop("disabled", true);
                $("#ddlBank").prop("disabled", true);
                $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                $("#lblCustomer").hide();
                $("#CrCardlbl").hide();
                $("#CClbl").hide();
                $("#lblbank").hide();
                $("#ddlBank").hide();
                $("#ddlType").hide();
                $("#lbltype").hide();
                $("#Text13").hide();
                $("#lblCashHeading").hide();
                $("#txtCashReceived").hide();
                $("#Text14").hide();
                $("#tr_transection").show();
            }
                     }
        });

             $("#Text13").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }


                 DEVBalanceCalculation();

             }
             );
             $("#Text15").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }

                 DEVBalanceCalculation();

             });



        

             $("#txtCashReceived").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }


         DEVBalanceCalculation();

     }
     );

             $("#txtpaymentAmount").keyup(
     function () {
         var regex = /^[0-9\.]*$/;
         if ($("#txtCODAmount").val() != 0) {
             onlineoption = "COD"

         }
         else {
             onlineoption = "Onlinepayment"
             $("#Td1").hide();
             //  $("#txtCODAmount").hide();
             // $("#txtPaymentAmount").show();
             //  $("#txtCODAmount").val("");
             $("#txtPaymentAmount").val("");
             $("#Coupans").val("0").prop("readonly", false);
         }
         if ($("#txtCODAmount").val() == "") {
             $("#txtCODAmount").val(0);
         }

         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         var txtcodAmount = $("#txtCODAmount").val();
         var txtFinalBillAmount = $("#txtFinalBillAmount").val();
         var txtPaymentAmount = $("#txtpaymentAmount").val();
         //var txtCODAmount = $("#txtCODAmount");
         //txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
         if (Number(txtPaymentAmount) > 0) {
             $("#txtCODAmount").val(0);
         }

         DEVBalanceCalculation();


     }
     );



             $("#txtCredit").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         DEVBalanceCalculation();



     }
     );



             $("#txtCard").keyup(
     function () {
         var regex = /^[0-9\.]*$/;


         var value = jQuery.trim($(this).val());
         var count = value.split('.');


         if (value.length >= 1) {
             if (!regex.test(value) || value <= 0 || count.length > 2) {

                 $(this).val(0);


             }
         }

         DEVBalanceCalculation();



     }
                );

                $("#ddlbillttype").change(
                    function () {

						if ($(this).val() != 0) {

							$("#dvBillWindow").dialog({
								autoOpen: true,
								top: 50,
								width: 356,
								resizable: false,
								modal: false,
								open: function (event, ui) {
									$('.ui-dialog').css('z-index', 100);
									$('.ui-widget-overlay').css('z-index', 100);
								},
							});


							$(".cls_paymode").show();

						}
						else if ($(this).val() == 0) {
							$(".cls_paymode").hide();
						}




						if ($(this).val() == "Credit") {


							$("#dvOuter_ddlCreditCustomers").show();
							$("#lblCashHeading").text("Receipt Amt:");
							$("#ddlbillttype option[value='Credit']").prop("selected", true);
							$("#txtCashReceived").val("0").prop("readonly", false);
							$("#Text13").val("0").prop("readonly", true);
							$("#Text14").val("").prop("readonly", true);
							$("#Text15").val("0").prop("readonly", true);
							$("#Text16").val("").prop("readonly", true);
							$("#ddlType").prop("disabled", true);
							$("#ddlOtherPayment").hide();
							$("#rdoCOD").hide();
							$("#rdoOnline").hide();
							$("#Td1").hide();
							$("#txtCODAmount").hide();
							$("#COD").hide();
							$("#Online").hide();

							$("#txtpaymentAmount").hide();

							$("#No").hide();
							$("#Amt").hide();
							$("#Coupans").hide();
							$("#OP").hide();
							$("#coupan").hide();
							$("#holder input").hide();
							$("#holder2 input").hide();
							$("#No").hide();
							$("#Amt").hide();
							$("#codChk").hide();
							$("#OnlineChk").hide();
							$("#ddlBank").prop("disabled", true);
							$("#ddlChosseCredit").show();
							$("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
							$("#lblCustomer").show();
							var FinalAmount = $("#txtFinalBillAmount").val();
							$("#txtCashReceived").val(parseFloat(FinalAmount).toFixed(2))
							$("#txtCashReceived").hide();
							$("#lblCashHeading").hide();
							$("#Text13").hide();
							$("#Text14").hide();
							$("#Text15").hide();
							$("#Text16").hide();
							$("#ddlType").hide();

							$("#CrCardlbl").hide();
							$("#CClbl").hide();
							$("#lblbank").hide();
							$("#ddlBank").hide();
							$("#ddlType").hide();
							$("#lbltype").hide();
							$("#txtBalanceReturn").val(0);
							$("#tr_transection").hide();
						}
						else if ($(this).val() == "CreditCard") {
							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId").val(0);
							$("#lblCreditCustomerName").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers").html("");
							$("#txtddlCreditCustomers").val("");
							$("#Coupans").hide();
							$("#OP").hide();
							$("#coupan").hide();
							$("#No").hide();
							$("#Amt").hide();
							$("#COD").hide();
							$("#Online").hide();
							$("#rdoOnline").hide();
							$("#lblCashHeading").text("Cash Rec:");
							$("#creditCustomer").css("display", "none");
							$("#ddlType").prop("disabled", true);
							$("#ddlOtherPayment").hide();
							$("#txtpaymentAmount").hide();
							$("#rdoCOD").hide();
							$("#Td1").hide();
							$("#txtCODAmount").hide();
							$("#dvOuter_ddlCreditCustomers").hide();
							$("#ddlChosseCredit").hide();
							$("#holder input").hide(); $("#holder2 input").hide();
							$("#No").hide();
							$("#Amt").hide();
							$("#codChk").hide();
							$("#OnlineChk").hide();
							//                   $("#ddlOtherPayment").hide();

							$("#txtCashReceived").val("0").prop("readonly", true);
							$("#Text13").val("0").prop("readonly", false);
							$("#Text14").val("").prop("readonly", false);
							$("#Text15").val("0").prop("readonly", false);
							$("#Text16").val("").prop("readonly", false);
							$("#ddlType").prop("disabled", false);
							$("#ddlBank").prop("disabled", false);
							$("#Text13").val($("#txtFinalBillAmount").val());
							$("#txtBalanceReturn").val(" ");
							$("#lblCustomer").hide();
							$("#CrCardlbl").show();
							$("#CClbl").show();
							$("#lblbank").show();
							$("#ddlBank").show();
							$("#ddlType").show();
							$("#lbltype").show();
							$("#Text13").show();
							$("#Text14").show();
							$("#Text16").show();
							$("#txtBalanceReturn").val(0);
							$("#tr_transection").hide();
						}

						else if ($(this).val() == "Cash") {


							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId").val(0);
							$("#lblCreditCustomerName").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers").html("");
							$("#txtddlCreditCustomers").val("");
							$("#Coupans").hide();
							$("#OP").hide();
							$("#lblCashHeading").text("Cash Rec:");
							$("#creditCustomer").css("display", "none");
							$("#coupan").hide();
							$("#dvOuter_ddlCreditCustomers").hide();
							$("#ddlChosseCredit").hide();
							$("#ddlType").prop("disabled", true);
							$("#ddlOtherPayment").hide();
							$("#COD").hide();
							$("#No").hide();
							$("#Amt").hide();
							$("#Online").hide();
							$("#txtpaymentAmount").hide();
							$("#rdoCOD").hide();
							$("#Td1").hide();
							$("#txtCODAmount").hide();
							//                   $("#ddlOtherPayment").hide();
							$("#holder input").hide();
							$("#holder2 input").hide();
							$("#No").hide();
							$("#Amt").hide();
							$("#codChk").hide();
							$("#OnlineChk").hide();
							$("#rdoOnline").hide();
							$("#txtCashReceived").val("0").prop("readonly", false);
							$("#Text13").val("0").prop("readonly", true);
							$("#Text14").val("").prop("readonly", true);
							$("#Text15").val("0").prop("readonly", true);
							$("#Text16").val("").prop("readonly", true);
							$("#ddlType").prop("disabled", true);
							$("#ddlBank").prop("disabled", true);
							$("#txtCashReceived").val($("#txtFinalBillAmount").val());
							$("#txtCashReceived").show();
							$("#txtBalanceReturn").val(" ");
							$("#lblCustomer").hide();
							$("#CrCardlbl").hide();
							$("#CClbl").hide();
							$("#lblbank").hide();
							$("#ddlBank").hide();
							$("#ddlType").hide();
							$("#lbltype").hide();
							$("#lblCashHeading").show();
							$("#txtBalanceReturn").val(0);
							$("#Text13").hide();
							$("#Text14").hide();
							$("#tr_transection").hide();
						}
						else if ($(this).val() == "OnlinePayment") {

							var customerId = 0;
							CrdCustSelId = 0;
							CrdCustSelName = "";
							$("#hdnCreditCustomerId").val(0);
							$("#lblCreditCustomerName").text("");
							CrdCustSelName = "";
							$("#ddlCreditCustomers").html("");
							$("#txtddlCreditCustomers").val("");

							$("#lblCashHeading").text("Cash Rec:");
							$("#creditCustomer").css("display", "none");
							$("#coupan").hide();
							$("#dvOuter_ddlCreditCustomers").hide();
							$("#ddlChosseCredit").hide();
							$("#COD").show();
							$("#Online").show();
							$("#No").show();
							$("#Amt").show();
							$("#ddlOtherPayment").show();
							$("#txtpaymentAmount").show();
							// $("#rdoOnline").show();
							//$("#rdoCOD").show();
							if ($("#txtCODAmount").val() != 0) {
								onlineoption = "COD"


							}
							$("#codChk").show();
							$("#OnlineChk").show();
							$("#Coupans").val("0").prop("readonly", true);
							$("#Td1").show();
							$("#txtCODAmount").show();
							$("#holder2").hide();
							$("#holder").hide();
							$("#No").hide();
							$("#Amt").hide();

							//                 if ($("#rdoOnline").prop('checked') == true) {
							//                     onlineoption = "OnlinePayment"
							//                 }

							$("#Coupans").show();
							$("#coupan").show();
							$("#OP").show();
							$("#txtCashReceived").val("0").prop("readonly", true);
							$("#Text13").val("0").prop("readonly", true);
							$("#Text14").val("").prop("readonly", true);
							$("#Text15").val("0").prop("readonly", false);
							$("#Text16").val("").prop("readonly", false);
							$("#ddlType").prop("disabled", true);
							$("#ddlBank").prop("disabled", true);
							$("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
							$("#lblCustomer").hide();
							$("#CrCardlbl").hide();
							$("#CClbl").hide();
							$("#lblbank").hide();
							$("#ddlBank").hide();
							$("#ddlType").hide();
							$("#lbltype").hide();
							$("#Text13").hide();
							$("#lblCashHeading").hide();
							$("#txtCashReceived").hide();
							$("#Text14").hide();
							$("#tr_transection").show();
						}

                    });

             $("#ddlChosseCredit").change(
                 function () {


                     if ($("#ddlChosseCredit").val() != "0") {


                         CrdCustSelId = $("#ddlChosseCredit").val();
                         $("#hdnCreditCustomerId").val(CrdCustSelId);
                         CrdCustSelName = $("#ddlChosseCredit option:selected").text();

                         $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())

                         $("#creditCustomer").css("display", "block");

                     }




                 });

				$("#ddlChosseCreditnew").change(
					function () {


						if ($("#ddlChosseCreditnew").val() != "0") {


							CrdCustSelId = $("#ddlChosseCreditnew").val();
							$("#hdnCreditCustomerId").val(CrdCustSelId);
							CrdCustSelName = $("#ddlChosseCreditnew option:selected").text();

							$("#lblCreditCustomerNamenew").text($("#ddlChosseCreditnew option:selected").text())

							$("#creditCustomer").css("display", "block");

						}




					});


             $("#OnlineChk").change(function () {
                 if ($("#OnlineChk").prop('checked')) {
                     var FinalAmount = $("#txtFinalBillAmount").val();
                     $("#txtpaymentAmount").attr('readonly', 'readonly');
                     $("#txtCODAmount").attr('readonly', 'readonly');
                     $("#txtBalanceReturn").val(0)
                     $("#txtpaymentAmount").val(FinalAmount);
                     $("#txtCODAmount").val(0);
                     $("#codChk").prop('checked', false);
                 }
             })
             $("#codChk").change(function () {
                 if ($("#codChk").prop('checked')) {
                     var FinalAmount = $("#txtFinalBillAmount").val();
                     $("#txtpaymentAmount").attr('readonly', 'readonly');
                     $("#txtCODAmount").attr('readonly', 'readonly');
                     $("#txtpaymentAmount").val(0);
                     $("#txtBalanceReturn").val(0)
                     $("#txtCODAmount").val(FinalAmount);
                     $("#OnlineChk").prop('checked', false);
                 }
             })
             function ResetBillCntrols() {


                 $("#txtFinalBillAmount").val("");
                 $("#ddlbillttype").val("");
                 $("#txtCashReceived").val("0");
                 $("#Text13").val("0");
                 $("#Text15").val("0");
                 $("#Text14").val("");
                 $("#ddlType").val("");
                 $("#ddlBank").val("");

                 $("#Text16").val("");
                 $("#txtBalanceReturn").val("0");
                 modeRet = "";
                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 //$("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();



                 $("#hdnCreditCustomerId").val("0");
                 $("#ddlbillttype option[value='" + DefaultPaymode + "']").prop("selected", true);
                 $("#creditCustomer").css("display", "none");


                 if (m_BillNowPrefix != "") {

                     if (m_BillMode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (m_BillMode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (m_BillMode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }
                 }

                 else {


                     if (DefaultPaymode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (DefaultPaymode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();




                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (DefaultPaymode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }



                 }








                 if (m_BillNowPrefix != "") {
                     $("#ddlbillttype option[value='" + m_BillMode + "']").prop("selected", true);
                 }
                 if (m_BillMode == "Credit") {

                     $("#ddlCreditCustomers").html("<option value='" + CrdCustSelId + "'>" + CrdCustSelName + "</option>");
                     $("#txtddlCreditCustomers").val(CrdCustSelName);
                     $("#creditCustomer").show();

                     if ($("#ddlChosseCredit").val() != "0") {
                         CrdCustSelId = $("#ddlChosseCredit").val();
                         CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                     }


                 }


             }








             $("#btnCustomer").click(
         function () {

             if (ProductCollection.length == 0) {
                 alert("Please first Select ProductsFor Billing");

                 return;
             }
             else {
                 $("#CustomerSearchWindow").show();
                 $("#dvProductList").hide();
                 //$("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();
                 bindGrid("M", $("#txtMobSearchBox").val());

             }



             //             $('#dvSearch').dialog(
             //            {
             //            autoOpen: false,

             //            width:720,
             //     
             //          
             //            resizable: false,
             //            modal: true,
             //                  
             //            });
             //            linkObj = $(this);
             //            var dialogDiv = $('#dvSearch');
             //            dialogDiv.dialog("option", "position", [238, 36]);
             //            dialogDiv.dialog('open');
             //            return false;



         });


             $(document).on("click", "#dvClose", function (event) {

                 var RowIndex = Number($(this).closest('tr').index());
                 var tr = $(this).closest("tr");
                 tr.remove();

                 ProductCollection.splice(RowIndex, 1);

                 if (ProductCollection.length == 0) {

                     //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");


                     RestControls();



                 }
                 Bindtr();

             });





             $(document).on("click", "a[name='categories']", function (event) {

                 $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
                 $(this).addClass("ancSelected").removeClass("ancBasic");



             });






             $("#btnSearch1").click(
         function () {

             bindGrid("N", "");

         }
         );

         });

        var m_ItemId = 0;
        var m_ItemCode = "";
        var m_ItemName = "";
        var m_Qty = 0;
        var m_Price = 0;
        var m_TaxRate = 0;
        var m_Surval = 0;
        var m_ItemDiscount = 0;
        var m_EditSaleRate = 0;
        var m_Free = false;

        var ProductCollection = [];
        function clsproduct() {
            this.ItemId = 0;
            this.ItemCode = "";
            this.ItemName = "";
            this.Qty = 0;
            this.Price = 0;
            this.TaxCode = 0;
            this.SurVal = 0;
            this.ProductAmt = 0;
            this.Producttax = 0;
            this.ProductSurchrg = 0;
            this.TaxId = 0;
            this.ItemRemarks = "";
            this.ItemDiscount = 0;
            this.EditSaleRate = false;
            this.Free = false;
        }
        var m_Total = 0;
        function Bindtr() {

            DiscountAmt = 0;
            VatAmt = 0;
            Total = 0;
            var fPrice = 0;
            TotalItems = 0;
            $("div[name='vat']").html("0.00");
            $("div[name='amt']").html("0.00");
            $("div[name='sur']").html("0.00");

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            var counterId = 1;
            BEndDiscountAmt = 0;
            var Count = 0;
            for (var i = 0; i < ProductCollection.length; i++) {
                BillBasicType = ProductCollection[i].Tax_type;
                if (RoleForEditRate == true) {
					var tr = "<tr><td><input type='checkbox' id = 'chk_" + counterId + "' name ='chkfree'/></td><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' name='txtBillQty' disabled='disabled' style='width:40px;text-align:center;font-size:15px;'  value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillAmt' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] * ProductCollection[i]["Qty"] + ".00' id='txtBillAmt'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";

                }
                else {
                    if (ProductCollection[i]["EditSaleRate"] == false) {

						var tr = "<tr><td><input type='checkbox' id = 'chk_" + counterId + "' name ='chkfree'/></td><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' disabled='disabled'disabled='disabled'  name='txtBillQty' style='width:40px;text-align:center;font-size:15px;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillAmt' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] * ProductCollection[i]["Qty"] + ".00' id='txtBillAmt'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                    }
                    else {
						var tr = "<tr><td><input type='checkbox' id = 'chk_" + counterId + "' name ='chkfree'/></td><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' disabled='disabled'  name='txtBillQty' style='width:40px;text-align:center;font-size:15px;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillAmt' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] * ProductCollection[i]["Qty"] + ".00' id='txtBillAmt'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                    }

                }

                $("#tbProductInfo").append(tr);

                var searchInput = $("#txtBillQty" + counterId + "");

                searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });


                counterId = counterId + 1;

                Count = Count + 1;

                fPrice = 0;
                fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
                var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);
                var Peritemdiscount = (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
                if (BillBasicType == "I") {


                    if (ProductCollection[i]["SurVal"] == "0") {
                        var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                    }
                    else {

                        var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
                        var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"]) + Survalll));

                    }
                }
                else {

                    var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);


                }


                var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
                var tottax = Number(TAx) + Number(surchrg);



                if (BillBasicType == "E") {

                    VatAmt = VatAmt + tottax;
                }


           


                Total = Total + fPrice;
                ProductCollection[i]["ProductAmt"] = fPrice;
                ProductCollection[i]["Producttax"] = TAx;
                ProductCollection[i]["ProductSurchrg"] = surchrg;


                var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
                var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
                var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
                $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
                $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
                $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


                BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
                TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);


            }


            CommonCalculation("0");

            if (ProductCollection.length == 0) {
                var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
                $("#tbProductInfo").append(tr);
            }

            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            //$("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();


        }


        function CommonCalculation(type) {

            if (type == "0") {

                m_Total = Total.toFixed(2);
            } else {
                m_Total = $("#dvsbtotal").html();


            }
            if (option == "HomeDelivery") {
                if (HomeDelCharges == 1) {

                    if (minbillvalue != 0) {
                        if (m_Total > minbillvalue) {
                            $("#dvDeliveryChrg").val(0)

                        }
                        else {
                            var Amount = Amount = parseFloat($("#dvsbtotal").text());;
                            var DelCharge = parseFloat($("#dvDeliveryChrg").val());
                            var TotalAmount = 0;
                            $("#dvnetAmount").html(TotalAmount);
                            if (DelCharge < DeliveryCharges && DelCharge!=0)
                            {
                                TotalAmount = parseFloat(Amount + DelCharge);
                             $("#dvnetAmount").html(TotalAmount);
                            }
                            else if(DelCharge > DeliveryCharges && DelCharge!=0)
                            {
                                TotalAmount = parseFloat(Amount + DelCharge);
                                $("#dvnetAmount").html(TotalAmount);
                            }
                            else if(DeliveryCharges!=0)
                            {
                                $("#dvDeliveryChrg").val(DeliveryCharges)
                            }
                        }
                    }
                    else {
                        DeliveryCharges = $("#dvDeliveryChrg").val();
                    }
                }

            }



            var BEndDiscountPer = 0;


            if (BackEndDiscount == 1) {



                BEndDiscountPer = BEndDiscountAmt * 100 / m_Total;

                $("#dvdisper").val(BEndDiscountPer.toFixed(2));
            }


            if (DiscountOnBillValue == 1) {

                for (var i = 0; i < DiscountValues.length; i++) {

                    var StartValue = DiscountValues[i]["StartValue"];
                    var EndValue = DiscountValues[i]["EndValue"];
                    var DisPer = DiscountValues[i]["DisPer"];


                    if (parseFloat(m_Total) >= parseFloat(StartValue) && parseFloat(m_Total) <= parseFloat(EndValue)) {

                        $("#dvdisper").val(DisPer.toFixed(2));
                        break;

                    }
                    else {
                        $("#dvdisper").val(0);

                    }



                }
            }



            DiscountAmt = (Number(m_Total) * Number($("#dvdisper").val())) / 100;


            $("#dvdiscount").val(DiscountAmt.toFixed(2));
            if (BillBasicType == "E") {
                var DisVatAmt = (Number(VatAmt) * Number($("#dvdisper").val())) / 100;
                VatAmt = Number(VatAmt) - Number(DisVatAmt);

            }
           

            $("div[id='dvsbtotal']").html(m_Total);


            // m_Total = (Number(m_Total)-Number(DiscountAmt).toFixed(2));
            m_Total = Number(m_Total);
            if (BillBasicType == "E") {
                $("div[id='dvVat']").html(VatAmt.toFixed(2));
            }

 
            TaxAmt = GetServiceTax();
            KKCTAmt = GetKKC();
            SBCTAmt = GetSBC();


            if (BillBasicType == "E") {

                if (option == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

                }
                else if (option == "TakeAway") {
                    if (Takeaway == 1) {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
                    else {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }


                }
                else if (option == "HomeDelivery") {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }





                //        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2)));
                //        $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2));
            }
            else {

                if (option == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2))))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

                }
                else if (option == "TakeAway") {
                    if (Takeaway == 1) {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
                    else {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }


                }
                else if (option == "HomeDelivery") {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }

            }



            if (RoundBillAmount == 1) {
                if (BillBasicType == "E") {

                    if (option == "Dine") {

                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else if (option == "TakeAway") {

                        if (Takeaway == 1) {
                            $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        }
                        else {
                            $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        }
                    }
                    else if (option == "HomeDelivery") {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }




                    //                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));

                }
                else {

                    if (option == "Dine") {

                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else if (option == "TakeAway") {

                        if (Takeaway == 1) {
                            $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        }
                        else {
                            $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                        }
                    }
                    else if (option == "HomeDelivery") {
                        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }

                }
            }
            else {
                if (BillBasicType == "E") {

                    if (option == "Dine") {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    } else if (option == "TakeAway") {

                        if (Takeaway == 1) {
                            $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                        } else {
                            $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                        }
                    }
                    else if (option == "HomeDelivery") {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }


                    //   $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
                else {
                    if (option == "Dine") {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    } else if (option == "TakeAway") {

                        if (Takeaway == 1) {
                            $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                        } else {
                            $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                        }
                    }
                    else if (option == "HomeDelivery") {
                        $("div[id='dvnetAmount']").html((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }


                }


            }


            if (BillBasicType == "E") {


                if (option == "Dine") {
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
                else if (option == "TakeAway") {

                    if (Takeaway == 1) {
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    } else {
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
                }
                else if (option == "HomeDelivery") {

                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }



                // $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

            }
            else {

                if (option == "Dine") {
                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
                else if (option == "TakeAway") {

                    if (Takeaway == 1) {
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2)) + Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    } else {
                        $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
                }
                else if (option == "HomeDelivery") {

                    $("#hdnnetamt").val((Number(Number(Number(m_Total.toFixed(2)) + Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }

            }


            //var netamount = $("div[id='dvnetAmount']").html();
            //var hdnNetamt = $("#hdnnetamt").val();
            //var Round = Number(netamount) - Number(hdnNetamt)

            var netamount = $("div[id='dvnetAmount']").html();
            $("div[id='dvnetAmount']").html(Math.round(netamount));
            var hdnNetamt = $("#hdnnetamt").val();
            var Round = Math.round(netamount) - (hdnNetamt)

            if (BillBasicType == "I") {
                var b = 0;
                b = Number(Number(hdnNetamt) + Number($("div[id='dvVat']").html()));
                $("div[id='dvnetAmount']").html(Number(b + Round).toFixed());
            }

            $("div[id='dvRound']").html(Round.toFixed(2));


            $("#lblNoItems").html(TotalItems);
            $("#txtFinalBillAmount").val($("#dvnetAmount").html());
            //alert(Total);
            //alert(TaxAmt);
            //alert(VatAmt);
            //alert(DiscountAmt);

            //alert($("#hdnnetamt").val());
            //alert($("div[id='dvnetAmount']").html());


        }
        $("#dvDeliveryChrg").keyup(function () {

        })
        function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount, B_Qty, EditSaleRate, Free, TaxType) {


            m_ItemId = ProductId;
            m_ItemCode = Code;
            m_Price = Price;
            m_ItemName = Name;
            m_EditSaleRate = EditSaleRate;

            if (B_Qty == 0) {
                if (modeRet == "Return") {
                    m_Qty = -1;
                }
                else {
                    m_Qty = 1;
                }
            }
            else {
                m_Qty = B_Qty;
            }
            m_TaxRate = TaxCode;
            m_Surval = SurVal;
            m_Tax_Id = Tax_Id;
            m_ItemDiscount = Discount;
            m_Free = Free;


            TO = new clsproduct();
            TO.ItemId = m_ItemId;
            TO.ItemCode = m_ItemCode
            TO.ItemName = m_ItemName;
            TO.Qty = m_Qty;
            TO.Price = m_Price;
            TO.TaxCode = m_TaxRate;
            TO.SurVal = m_Surval;
            TO.TaxId = m_Tax_Id;
            TO.ItemRemarks = Item_Remarks;
            TO.ItemDiscount = m_ItemDiscount;
            TO.EditSaleRate = m_EditSaleRate;
            TO.Free = m_Free;
            TO.Tax_type = TaxType;
            ProductCollection.push(TO);
            Bindtr();



        }


        function GetServiceTax() {


            var TaxAmt = 0;


            $("#dvsertaxper").html(Sertax);
            var Dis = $("#dvdiscount").val();


            if (option == "TakeAway") {
                if (Takeaway == "1") {

                    TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
                    $("#dvTax").html(TaxAmt.toFixed(2));


                }
                else {
                    $("#dvsertaxper").html("0");
                    TaxAmt == "0";
                    $("#dvTax").html(TaxAmt.toFixed(2));
                }
            }
            else if (option == "Dine") {

                TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
                $("#dvTax").html(TaxAmt.toFixed(2));


            }


            return TaxAmt;

        }


        function GetKKC() {


            $("#dvKKCPer").html(KKC);


            if (option == "TakeAway") {
                if (Takeaway == "1") {



                    KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
                    $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


                }
                else {

                    $("#dvKKCPer").html("0");
                    KKCTAmt == "0";
                    $("#dvKKCAmt").html(0.00);
                }
            }
            else if (option == "Dine") {

                KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
                $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


            }



            return KKCTAmt;

        }


        function GetSBC() {


            $("#dvSBCPer").html(SBC);

            if (option == "TakeAway") {
                if (Takeaway == "1") {

                    SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
                    $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



                }
                else {
                    $("#dvSBCPer").html("0");
                    SBCTAmt == "0.00";
                    $("#dvSbCAmt").html(SBCTAmt.toFixed(2));

                }
            }
            else if (option == "Dine") {
                SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
                $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



            }


            return SBCTAmt;

        }




        function GetTable() {



            $.ajax({
                type: "POST",
                data: '{}',
                url: "KotRaiseBill.aspx/GetTable",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#products").html(obj.productData);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
       );


        }



        //........................................



        $(document).on("click", "input[name='chkfree']", function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            var RowIndex = Number($(this).closest('tr').index());


            var Price = ProductCollection[RowIndex]["Price"];
            var Qty = ProductCollection[RowIndex]["Qty"];
            var a = $("#dvsbtotal").html();
            if ($(this).prop('checked') == true) {
                a = Number(a) - (Number(Price) * Number(Qty));
            }
            else {
                a = Number(a) + (Number(Price) * Number(Qty));
            }

            $("#dvsbtotal").html(a);
            ProductCollection[RowIndex]["Free"] = $(this).prop('checked');
            CommonCalculation("1");




        });


        $(document).on("keyup", "input[name='txtBillQty']", function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {
                var RowIndex = Number($(this).closest('tr').index());
                var PId = ProductCollection[RowIndex]["ItemId"];

                var Mode = "Plus";

                var Qty = ProductCollection[RowIndex]["Qty"];
                var Price = ProductCollection[RowIndex]["Price"];



                var fQty = $(this).val();
                if (isNaN(fQty)) {
                    fQty = 1;

                }
                ProductCollection[RowIndex]["Qty"] = fQty;

                Bindtr();

                $("#txtItemCode").focus();
            }

        });


        $(document).on("change", "#txtBillPrice", function (event) {


            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];

            var Mode = "Plus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];



            var fPrice = $(this).val();

            ProductCollection[RowIndex]["Price"] = fPrice;

            Bindtr();

        });









        //............................................

    </script>

     <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script>
        $(function () {
            //            $('#txtddlMobSearchBox,#txtItemCode').keyboard();



            //                $('#txtItemCode').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '1 2 3 4 5',
            //    '6 7 8 9 0',
            //    ' {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 10,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();




            //                $('#txtCashReceived').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '9 8 7 6 5',
            //    '4 3 2 1 0',
            //    ' . {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 6,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();


        });
	</script>


    
 <div id="dvAddOn"></div>
<div class="right_col kot-rise-bill-right-col" role="main">
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="display:  none;">
                                <div class="x_title">
                                    <h2>Bills List</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group"  margin-bottom:10px">

                                 <div class="form-group">
                                
                                <table>
                                <tr><td style="font-size:20px">Date From:</td><td style="font-size:20px">
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:130px;background-color:White;font-size:20px;height:37px;"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td style="font-size:20px">Date To:</td><td style="font-size:20px"><input type="text" readonly="readonly"  class="form-control input-small" style="width:130px;background-color:White;font-size:20px;height:37px;"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo" style="font-size:20px">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>

                <table id="jQGridDemoBill">
                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                </div>


                                   
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table>
                                                    <tr><td><div class="btn btn-primary" style="display: block;height:50px;width:100px;font-size:20px;" id="btnShowScreen">
                                           <i class="fa fa-external-link"></i> New</div></td>
                                           <td><div class="btn btn-success" style="display:none;height:50px;width:100px;font-size:20px;" id="btnEdit">
                                           <i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                           
                                           <td><div class="btn btn-danger" style="display: block;height:50px;width:112px;font-size:20px;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> Reprint</div></td>
                                            <td >
                                        <div id="btnDelete" style="display:none;height:50px;width:130px;font-size:20px;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i>Cancel Bill</div>
                                    </td>
                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     








                    
                    <script type="text/javascript">

                        jQuery.fn.putCursorAtEnd = function () {

                            return this.each(function () {

                                // Cache references
                                var $el = $(this),
        el = this;

                                // Only focus if input isn't already
                                if (!$el.is(":focus")) {
                                    $el.focus();
                                }

                                // If this function exists... (IE 9+)
                                if (el.setSelectionRange) {

                                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                                    var len = $el.val().length * 2;

                                    // Timeout seems to be required for Blink
                                    setTimeout(function () {
                                        el.setSelectionRange(len, len);
                                    }, 1);

                                } else {

                                    // As a fallback, replace the contents with itself
                                    // Doesn't work in Chrome, but Chrome supports setSelectionRange
                                    $el.val($el.val());

                                }

                                // Scroll to the bottom, in case we're in a tall textarea
                                // (Necessary for Firefox and Chrome)
                                this.scrollTop = 999999;

                            });

                        };

                        (function () {

                            var searchInput = $("#search");

                            searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });

                        })();

						var m_CustomrId = -1;
                        $(document).ready(function () {

							$('#cm_DOB').daterangepicker({
								singleDatePicker: true,
								calender_style: "picker_1"
							}, function (start, end, label) {
								console.log(start.toISOString(), end.toISOString(), label);
							});

							$('#cm_DOA').daterangepicker({
								singleDatePicker: true,
								calender_style: "picker_1"
							}, function (start, end, label) {


								console.log(start.toISOString(), end.toISOString(), label);
							});

							$("#cm_ddlPrefix").val();
							$("#cm_Name").val("");
							$("#cm_Address1").val("");
							$("#cm_Address2").val("");
							//                                           
							$("#cm_DOB").val("1900-01-01");
							$("#cm_DOA").val("1900-01-01");
							$("#cm_Discount").val("0");
							$("#cm_Tag").val("0");
							$("#cm_ContactNumber").val("");
							$('#cm_IsActive').prop("checked", true);




                            if ($("#ddlbillttype").val() == "Cash") {
                                var customerId = 0;
                                CrdCustSelId = 0;
                                CrdCustSelName = "";
                                $("#hdnCreditCustomerId").val(0);
                                $("#lblCreditCustomerName").text("");
                                CrdCustSelName = "";
                                $("#ddlCreditCustomers").html("");
                                $("#txtddlCreditCustomers").val("");

                                $("#lblCashHeading").text("Cash Rec:");
                                $("#creditCustomer").css("display", "none");

                                $("#dvOuter_ddlCreditCustomers").hide();

                                $("#ddlChosseCredit").hide();
                                $("#ddlChosseCredit").val(0);

                                $("#txtCashReceived").val("0").prop("readonly", false);
                                $("#Text13").val("0").prop("readonly", false);
                                $("#Text14").val("").prop("readonly", false);
                                $("#Text15").val("0").prop("readonly", false);
                                $("#Text16").val("").prop("readonly", false);
                                $("#ddlType").prop("disabled", false);
                                $("#ddlBank").prop("disabled", false);


                            }


                            $("#txtDateFrom,#txtDateTo,#txtKotDate").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();



                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                      );




                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtKotDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });


                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


<div id="dvbillSave" style="float: left; display:none; position:absolute;z-index:1000;left:340px;width:300px;bottom:215px;background-color: #293C52;
                                                                color: white; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                                <table style="width:100%;">
                                                                    <tr style="text-align:center">
                                                                    <td style="text-align:center">BILL SAVED SUCCESSFULLY</td>
                                                                        
                                                                    </tr>
                                                                    <tr><td style="text-align:center;font-size:20px">AMOUNTED Rs.<asp:Label ID ="lblAmmmt" runat="server" ClientIDMode ="Static"></asp:Label></td></tr>
                                                                    <tr><td>
                                                                    <button type="button" class="button1" style="background-color:White;color:Black;width:60px;box-shadow: -2px -5px 5px transparent;margin-left:41%;height:40px;"  id="btnMsgClose">OK</button>
                                                                   </td></tr>
                                                                    
                                                                  
                                                                </table>
                                                            </div>


<%--<div class="row" id="CustomerDialog" style="display:none">
<div class="form-horizontal form-label-left input_mask">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>


</div>--%>

<div id="screenDialog" class="raisebill_dialoge" title = <%=m_getdate %>>

    <input type="hidden" id="hdnnetamt" value="0" />
     <input type="hidden" id="hdnsteward" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
	<asp:HiddenField ID = "hdndd" runat="server" ClientIDMode = "Static" />
	<asp:HiddenField ID = "hdndisplay" runat="server" ClientIDMode = "Static" />
    <asp:HiddenField ID = "hdntbleno" runat="server" ClientIDMode = "Static" />
    <asp:HiddenField ID = "hdncashcustid" runat="server" ClientIDMode = "Static" />
    <asp:HiddenField ID = "hdncashcustname" runat="server" ClientIDMode = "Static" />
       <asp:HiddenField ID = "hdncompnycustid" runat="server" ClientIDMode = "Static" />
    <div class="container">
        <div class="row">
            <div class="nav1" style="padding-top:0px;margin-top:-5px;display:none"> 
                <ul id="categories">
                </ul>
            </div>
        </div>
        <div class="row">

         <div class="col-xs-12">
         <div class ="col-xs-12">

                
                <div class="Search raise_bill_search">
                    <div class="input-group">
                        <table width="100%">
                          <tr>
                          <td class="raise-bill-no-td">Bill No:</td>
                          <td><input type="text" id ="txtKotBillNo" disabled="disabled" value="Auto" /></td>
                          <td>Bill Date:</td>
                          <td><input type="text" disabled="disabled" id ="txtKotDate"></td>
                          <td class="raise-cust-id-td">Cust Id:</td>
                          <td><input type="text" disabled="disabled" id ="txtcustId" /></td>
                          <td class="raise-cust-name-td">Cust Name:</td>
                          <td class="txt">  <select id="ddlMobSearchBox" class="txt"> </select></td>
                          <td>
                                              <img id="btnAddCustomer" src="images/adduser-white.png"/>
												</td>
                          <td style="display:none;">
                            <asp:Label ID="lblcstname" runat="server" Font-Size="large" Text="Label" ForeColor="White"></asp:Label>
                            <asp:Label ID="lblcstmob" Font-Size="large" ForeColor="White" runat="server" Text="Label"></asp:Label>
                          </td>
                          </tr>

                          <tr>
                          <td>Pax No:</td>
                          <td><input type="text"  id ="txtPax" class="txt" /></td>
                          <td>Steward:</td>
                          <td><input type="text" id ="txtkotsteward" class="txt" /></td>

                          <td colspan="2" id="tdorderno" style="display:none"><label>Order No:</label><input type="text" id="txtorderno" class="txt form-control" placeholder="Order No" aria-describedby="basic-addon2"  style="padding: 10px 10px;width: 91%;border: 0px;height: 35px"/></td>
                                <td>Table No:</td>
                          <td><input type="text" id ="txtblno" class="txt" autocomplete="off"/></td>
                          <td colspan="2" rowspan="2">
                               <div style="width:100%;margin-top:5px; background-color:black; text-align:center;color:white;font-size:17px;font-weight: bold;">
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2"; margin:0 auto !important;>
                                                    <tr>
                                                       

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="lblCashCustomerName" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <td style="padding-left:50px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                                   </div>  </td>
                          
                          
                          </tr>
                          <tr style="display:none;"><td style="color:White;font-weight:bold;font-size:17px;">TableNo</td><td style="padding-left:5px;padding-top:5px"><input type="text" id ="txtKotTable" style="width:100px;font-size:17px;" class="txt"/></td><td><label style="color:White;font-size:17px" id="lblchhk">TakeAway</label> </td><td style="color:White;font-weight:bold;padding-left:10px"> <input type="checkbox" data-index="27" style="font-size:17px;width:25px;height:25px;padding-top:5px" id="chktakeway" /></td>  <td colspan="4" style="padding-left:10px"></td></tr>
                        </table>
                    </div>
                </div>
                </div>

                </div>
                

                <div class="col-xs-12" style="display:none;margin-bottom:5px" id="colProducts">
              
                    </div>

             <div class="col-xs-12 raisebill_middle">

             <div class="col-xs-12 col-md-3 col-sm-3"  id="dvproducts" style="overflow: auto;">
               <div id="products">
                </div>
             </div>

            <div class="col-xs-12 col-sm-6 col-md-6" style="padding-left: 0px" id="colDvProductList">
                <div id="dvProductList">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                            <th style="width: 20px; text-align: center; color: #FFFFFF;">
                                    Free
                                </th>

                                <th style="width: 200px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>

                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Qty
                                </th>
                                 <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amt
                                </th>
                                
                                  <th style="width: 200px;text-align:center;color: #FFFFFF;display:none">
                                    Add-On
                                </th>


                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 19px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
           <%--     <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    <div class="cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            
                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="txtFinalBillAmount" />
                                </td>
                                <td>
                                    Bill Type:
                                </td>
                                <td>
                                    <select id="ddlbillttype" style="height: 27px; width: 120px; padding-left: 0px" class="form-control">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>
                                <td>

                                  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                             <option value="0"></option>
                                             </select>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="txtCashReceived" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                    Type:
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 25px; width: 90px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" style="height: 25px; width: 90px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 64px">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>--%>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" checked="checked" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan = "100%">
                             <table>
                             <td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </table>
                                              
                                           
                                           
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Table
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <div class="cate2">
                    <table id="tbamountinfo" style="width: 43%; text-align: right; background: white;padding:0px;font-size:17px;margin-left:178px"><tr>
                            <td valign="top" style="padding-top:10px">
							<asp:Literal id="ltDateTime" runat="Server" Visible="false"/>

                                   
                                <div>
                                <table>
                                     <tr><td>No Of Items: </td><td colspan="100%">
                            
                              <div style="text-align:justify;width:60%;margin:0px 0px 0px;border-left:0px;border-top:0px;padding:5px">
                                  <asp:Label ID ="lblNoItems"  runat="server" ClientIDMode ="Static"></asp:Label>
                                  </div>
                            
                                           </td>
                        </tr>

                                </table>

                                </div>

                            </td>

                            
                            
                                           
                                </table>
                            </div>


                    <table>
                      

                 
                        <tr class="raisebill_buttons">
                           
                            <td id="tdbtnCash">
                                <%--<div class="button1" id="btnCash">Save (F1)</div>  --%> 
                                <button type="button" class="button1" id="btnCash">Save (F1)</button>
							</td>

							<td>
                                    <button  type="button" class="button1" id="KOT">KOT (F4)</button>
                                <%--<div class="button1" id="KOT">KOT (F4)</div>--%>
							</td>

                            <td>
                                   <button type="button" class="button1" id="settlement">Settlement (F8)</button>
                             <%--   <div class="button1" id="settlement">Settlement (F8)</div>--%>
                            </td>
							 
                             <td>
                                    <button type="button" class="button1" id="btnhome_del">Assign Home Deliveries (F9)</button>
							<%--    <div class="button1" id="btnhome_del">Assign Home Deliveries (F9)</div>--%>
                            </td>

                             <td>
                                     <button type="button" class="button1" id="btnClear">Exit</button>
						     <%--   <div class="button1" id="btnClear">Exit</div>--%>
                            </td>


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>
                           
                        </tr>

                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>

                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>

        <div class="col-xs-12 col-sm-3 col-md-3" id="dv_thirdpanel">
            <td valign="top" align="right" style="padding-right:10px" >


    

                <div style="border:solid 1px silver;display:none; background-color:#F5FFFA; box-shadow:2px 2px 3px silver;width:110%;margin:0px 0px 8px;border-left:0px;border-top:0px;padding:5px;">
                                                    <table>
                                                      <tr>
                            <td colspan="100%" align="right" style="padding-right:10px">
                                <table>
                                    <tbody>
                                        <tr>  <td><b>Choose Service</b></td>
                                              </tr>
                                        
                                        <tr>
                                            <td><b><asp:DropDownList ID="ddloption" runat="server" 
                                                    Style="width: 115px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                    <asp:ListItem Text="HomeDelivery" Value="HomeDelivery"></asp:ListItem>
                                                </asp:DropDownList></b>
                                                 <b><select id="ddlEmployees" style="float:right;height: 25px; width: 86px;margin:0 0 0 5px; display:none" class="form-control"></select></b>  <b><select id="ddlTable" style="height: 25px; width: 115px" class="form-control"></select></b>
                                                
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                                                    </table>
                 </div>

                                <div class="net_amount">

                                
                                <table  id="myCustomTable" style="font-size:17px">

                                     <tr style="display:none;">
                                        <td>
                                            HomeDel:
                                        </td>
                                        <td colspan="100%">
                                            <input type="checkbox" id = "chkhone">
                                        </td>
                                    </tr>

                                    <tr style="display:none;">
                                        <td style="padding-right: 10px;height: 25px;color:white;  text-align: left"   >
                                            Employee:
                                        </td>
                                        <td style="width: 100px; text-align: left;" colspan="100%">
                                          <asp:DropDownList ID ="ddlsteward" runat="server" Enabled = "false"></asp:DropDownList>
                                        </td>
                                    </tr>
                               


                                    <tr>
                                        <td>
                                            Amount:
                                        </td>
                                        <td colspan="100%">
                                            <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Discount:
                                        </td>
                                        <td colspan="100%">
                                            <table>
                                                <tr>
													
                                                    <td style="padding-left:0;">
                                                    <input type="text" class="form-control input-small" id="dvdisper" value ="0"/>
                                                    </td>
                                                    <td>
                                                        %
                                                    </td>
                                                    <td colspan="3">
                                                       <input type="text" class="form-control input-small" id="dvdiscount" value ="0" />
                                                    </td>
                                                </tr>

                                            </table>

                                        </td>


                                        
                                    </tr>

                                    <tr id ="trServicetax">
                                        <td>
                                            ServiceTax:
                                        </td>
                                        <td colspan="3">
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td colspan="50%">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trKKC">
                                        <td>
                                            KKC:
                                        </td>
                                        <td colspan="3">
                                            <div id="dvKKCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td colspan="50%">
                                            <div id="dvKKCAmt">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr id ="trSBC">
                                        <td>
                                            SBC:
                                        </td>
                                        <td colspan="3">
                                            <div id="dvSBCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td colspan="50%">
                                            <div id="dvSBCAmt">
                                            </div>
                                        </td>
                                    </tr>

                                    <tr id ="vatIncOrExc">
                                        <td>
                                           GST Amount:
                                        </td>
                                        <td colspan="100%">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges">
                                        <td>
                                            DelvChrg:
                                        </td>
                                        <td colspan="100%">
                                           <input type="text" class="form-control input-small" id="dvDeliveryChrg" value ="0" />
                                        </td>
                                     </tr>
                                           
                                     
                                     <tr>
                                        <td>
                                            RoundOff:
                                        </td>
                                        <td colspan="100%">
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>
                                            Net Amt:
                                        </td>
                                        <td colspan="100%">

                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        
<td colspan="100%" class="td-btncash-btn"><div class="button1" id="btnCashBill">
                                  Cash</div>
                    <%--</td><td>--%> <div class="button1" id="btnCreditCardBill">
                                    Credit Card</div><%--</td>--%>
                                   <%-- </tr>
                                    <tr>--%>
<%--<td>--%><div class="button1" id="btnCreditBill">
                                    Credit</div><%--</td><td>--%><div class="button1" id="btnOnline">
                                    Online Payment</div></td>
                                    </tr>

                             
                 
                                    <tr id="trpaymode"  style="border: solid;border-bottom-color: wheat;border-top-color: wheat;display: none;">
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold;padding:15px">
                                            Mode:
                                        </td>
                                        <td>
                                          
                                        <asp:RadioButtonList ID="RdoPaymode" runat="server" CssClass="rbl" RepeatDirection="Horizontal" ForeColor="White" CellSpacing="-1">

                                            <asp:ListItem Text="COD" Value="COD"></asp:ListItem>


                                            <asp:ListItem Text="Online" Selected="True" Value="Online"></asp:ListItem>
                                        </asp:RadioButtonList>
                                            </td>
                                    </tr>

                                    <tr></tr>
                                     </table>
                                     <table class="table" id="tblbilltype" style="display:none">
                            <tbody>
                                 <tr>
                                    
                                    <td colspan="8">
                                    <select id="ddlbillttype" class="form-control" multiple style="display:none">
                                           <option value="0">--Select PayMode--</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>
                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>
                                    </select>
                                    </td>
                                     </tr>
                                </tbody>
                                         </table>
                                       <div id="dvBillWindow" class="settlement_window" style="display:none" title="PayMode">
               <%--     <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>--%>
                    
                    
               
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tbody>
             <%--                    <tr>
                                    <td colspan="8">
                                    <select id="ddlbillttype" class="form-control">
                                           <option value="0">--Select PayMode--</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>
                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>
                                    </select>
                                    </td>
                                </tr>--%>
                                <tr class="cls_paymode">
                              <%--  <td>
                                    Amount:
                                </td>--%>
                                <td colspan="100%">
                                    <table>
                                        <tbody>
                                            <tr>         <td>
                                    Amount:
                                </td>  <td>
                                    <input  class="form-control input-small" id="txtFinalBillAmount" type="text" />
                                </td></tr>

                                            <tr>

                          
                               
                                <td>
                                    <span id="lblCustomer">Customer Name</span>

                                  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;float: left;font-size: 11px;min-width: 0;padding: 0;width: 222px;height:35px;display:none;">
                                             <option value="0"></option>
                                             </select>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server">
                                           
                                             </asp:DropDownList></td>
                                              
                                        </tr>
                                      
                                    </tbody></table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr class="cls_paymode">
                                <td>
                                    <label id="lblCashHeading">
                                        Cash Rec:</label>
                                </td>
                              
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>


                                        <input  class="form-control input-small" value="0" id="txtCashReceived" type="text">
                                  
                                            </td>
                                            
                                            <td>

                                                  <table width="100%" id="creditCustomer" class="select_customer" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                             <tr class="cls_paymode">
                                <td id="CrCardlbl">
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tbody><tr>
                                              <td style="width: 38%;">
                                    <input  class="form-control input-small" value="0" id="Text13" type="text"  style="width: 80%;" />
                                </td>
                                <td id="lbltype">
                                    Type:
                                </td>
                                <td>
                                    <select  id="ddlType"class="form-control">
                                     
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>

                            
                            </tr>
                                 <tr class="cls_paymode">
                                <td id="CClbl">
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input  class="form-control input-small" id="Text14" type="text">
                                </td>
                            </tr>
                                    <tr class="cls_paymode">
                                <td id="lblbank">
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" class="form-control"><option value="1">HSBC</option><option value="2">PAYTM</option></select>

                                    <input class="form-control input-small" id="Text15" value="0" type="text" style="display:none">
                                </td>

                            </tr>
                            
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input class="form-control input-small" id="Text16" type="text">
                                </td>
                            </tr>

                            <tr class="cls_paymode"><td style="display: table-cell;" id="OP">Online payment</td>

                             <td> <select id="ddlOtherPayment" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select></td>
                                        
                                        
                                             </tr>

                                                <tr class="cls_paymode"><td  style="text-align:left;width:50px">
                         <input type="checkbox" id="codChk" style="width:15px;height:19px" />  <label class="headings" id="COD" for="rdoCOD">COD</label></td>
                            <%--     <td id="Td1">Rs.</td>--%>

                             <td colspan="3">
                                    <table>
                                        <tbody><tr>
                                         <td style="width: 34%;">
                                   <input class="form-control input-small" id="txtCODAmount" type="text" value="0" style="width: 85%;"/>&nbsp
                                </td>
                                <td>
                                    <input type="checkbox" id="OnlineChk" style="width:15px;height:19px"/>  <label class="headings" id="Online" for="rdoOnline">Online</label>
                                </td>
                                <td style="width: 30%;">
                                  <input type="text" class="form-control input-small" value="0" id="txtpaymentAmount" />
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>








                               
                 
 </tr>


                                <tr id="tr_transection" style="display:none" class="cls_paymode">
                                      <td style="display: table-cell;" >Trans. ID</td>
                                      <td>
                                    <input  class="form-control input-small" value="0" id="trnid" type="text">
                                        </td>
                                </tr>

                                            <tr style="display:none;" >          <td style="display: table-cell;" id="coupan">Enter Coupan Qty</td>

                               <td>
                                    <input  class="form-control input-small" value="0" id="Coupans" type="text">
                                        </td>
                                <%--      <td style="width: 50px"><label id="No" style="display: none; margin-left: -150px;">CoupanNo</label>
                                      <div id="Div3" style="display: none; margin-left: -150px;"></div></td>--%>
                                       <td style="width: 50px"><label id="No" style="margin-left: -85px">CoupanNo</label>
                                      <div style="margin-left: -125px;color:Black" id="holder"></div></td>
                                      <td style="width: 50px"><label  id="Amt">CoupanAmt</label>
                                      <div style="margin-left:-26px;color:Black" id="holder2"></div></td>
                                       
                                        </tr>
                                <tr class="cls_paymode">
                            <%--    <td">
                                                Balance:
                                            </td>--%>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tbody>
                                              <tr class="cls_paymode">
                                            
                                            <td>Balance:</td>
                                            <td style="width: 90px">
                                                <input class="form-control input-small" id="txtBalanceReturn"  type="text" value="0" readonly="readonly">
                                            </td>
                                           
                                           
                                        </tr>
                                       
                                    </tbody></table>
                                </td>
                            </tr>

                             <tr> <td style="width: 90px">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tbody>
                                                       

                                                </tbody></table>
                                            </td></tr>
                        </tbody></table>
                    
                </div>
                                    <div style="border:  solid;border-bottom-color:  wheat; padding:4px;display:none" id="dvotp">
                                    <label style="font-weight:  bold;font-size: larger;">OTP:</label><input type="text" id="textotp" style="margin-left: 30px;"/></div>
                                            </div>
                                            </td>
            </div>


            
            </div>
            
        </div></div>
    </div>
         <div id="TenderAmounts" title="Tender Amount">
            <div class="row kot-raise-bill-hide">
            <div class="col-md-12">
              <div class="col-md-6">
              <label>Net Amount:</label>
              </div>
              <div class="col-md-6">
                <input type="text" id="TotalNetAmount" disabled="disabled" />
              </div>
              </div>
            </div>
             <div class="row kot-raise-bill-hide">
            <div class="col-md-12" style="margin-top:10px;">
              <div class="col-md-6">
              <label>Paid Amount:</label>
              </div>
              <div class="col-md-6">
                <input type="text" id="TenderAmount"/>
              </div>
              </div>
            </div>
              <div class="row kot-raise-bill-hide">
                  <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-6"><label>Tender Amount:</label></div>
                <div class="col-md-6">
                  <label id="TotalTenderAmount"></label>
                </div>

                  </div>
              </div>
          </div>
        <div id="dvAddCustomer" style="display:none">
						
							 <div class="form-div">
								 <div class="form-lbl">Prefix <span>*</span></div>
								 <div class="form-inp">
									 <div class="prefix-inp">
									  <asp:DropDownList class="form-control" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                       <input type="text" class="txt form-control validate required alphanumeric" placeholder="Customer Name" id="cm_Name">
								</div>
									 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">Address1 <span>*</span></div>
								 <div class="form-inp">
									  <textarea class="txt form-control validate required" id="cm_Address1"></textarea>
								 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">State <span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <asp:DropDownList  class="form-control" ID="cm_ddlState" ClientIDMode="Static"  runat="server" ></asp:DropDownList>
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">
											 City <span class="required">*</span> 
										 </div>
										 <div class="sec-input">
											 <asp:DropDownList  class="form-control " ID="cm_ddlCities" ClientIDMode="Static"  runat="server"></asp:DropDownList>
                                                 
                                            <span class="fa fa-plus city-plus" onclick="javascript:OpenVMDialog('City')"></span>
										 </div>
									 </div>
								 </div>
							 </div>

							 <div class="form-div">
								  <div class="form-lbl">Area <span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <asp:DropDownList class="form-control" ID="cm_ddlArea" ClientIDMode="Static" runat="server">

                                        </asp:DropDownList>
                                        <span class="fa fa-plus area-plus" onclick="javascript:OpenVMDialog('Area')"></span>
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Pincode <span class="required">*</span></div>
										 <div class="sec-input">
											 <input type="number" class="txt form-control validate required alphanumeric" onkeypress="return isNumberKey(event)" placeholder="Pincode" id="cm_Pincode">
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">Contact<span class="required">*</span></div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <input type="text" class="txt form-control validate required valNumber" placeholder="Mobil no" id="cm_ContactNumber" onkeypress="return isNumberKey(event)" maxlength= "10">
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Email</div>
										 <div class="sec-input">
											<input type="text" class="txt form-control" placeholder="Email" id="cm_EmailId" >
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">Discount</div>
								 <div class="form-inp">
									 <div class="first-inp">
										 <input type="text" class="txt form-control validate  valNumber" onkeypress="return isNumberKey(event)" value="0" id="cm_Discount" />
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">Tag</div>
										 <div class="sec-input">
											 <input type="text" class="txt form-control alphanumeric" id="cm_Tag" >
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">DOB</div>
								 <div class="form-inp">
									 <div class="first-inp">
										  <input type="text"   class="form-control"   id="cm_DOB" aria-describedby="inputSuccess2Status" />
									 </div>
									 <div class="sec-inp">
										 <div class="sec-lbl">An. Date</div>
										 <div class="sec-input">
											 <input type="text"   class="form-control"   id="cm_DOA" aria-describedby="inputSuccess2Status" />
										 </div>
									 </div>
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">GST No</div>
								 <div class="form-inp">
									 <input type="text" class="txt form-control" required="required" value="0" id="txtGSTNo">
								 </div>
							 </div>

							<div class="form-div">
								  <div class="form-lbl">IsActive <span class="required">*</span></div>
								 <div class="form-inp">
									  <div class="first-check">
										 <input type="checkbox" checked="checked" id="cm_IsActive" />
										</div>
									 	 <div class="first-check">
										 
											<label for="first-name" class="control-label ">FOC <span class="required">*</span></label>
                                            <input type="checkbox" id="cm_FOC"  />
										</div>
									 	 <div class="first-check">
											<label for="first-name" class="control-label ">Credit</label>
											<input type="checkbox"  id="cm_Credit" />
                                                            
									 </div>
								 </div>
							 </div>
							
                                <div class="form-div cashcustomer_btns">
                                    <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                    <button class="btn btn-danger"  onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                    <%-- <button class="btn btn-danger"  id="closkey" type="button"><i class="fa fa-mail-reply-all"></i> Close keyboard</button>--%>
                                </div>




						
					</div>

        <div id="dvCreditCardWindow" style="display:none">
                                    <table>
                                     
                                    <tr><td>Bank:</td><td>
                                        <select id="ddlBankNew" class="form-control"></select>
                                       <%-- <select id="ddlCreditCardBank"></select>--%>

                                                      </td>
                                    <td>
                                    
                                    <div  class="btn btn-primary" id="btnCreditCardBilling">
                                    Settle Bill
                                     </div> 
                                    </td>
                                    
                                    </tr>
                                  

                                    </table>
                                    
                                    </div>
           <div id="dvOnlineWindow" style="display:none">
                                    
                                     <div class="online-select"> <select id="ddlOtherPaymentnew" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select></div>

                                                          <table class="online-tbl">   <tr class="rdocod-tr"><td  style="text-align:left;width:50px"><input type="radio" style="display:none"  id="rdoCOD" name="abc" />
                         <input type="checkbox" disabled="disabled" id="codChknew" style="width:15px;height:19px" />  <label class="headings" id="CODnew" for="rdoCOD">COD</label></td>
                            <%--     <td id="Td1">Rs.</td>--%>
<td>
                                   <input class="form-control input-small" disabled="disabled" id="txtCODAmountnew" type="text" value="0"/>&nbsp
                                </td>
                                <td>
                                    <input type="checkbox" id="OnlineChknew" style="width:15px;height:19px"/>  <label class="headings" id="Onlinenew" for="rdoOnline">Online</label>
                                </td>
                                <td>
                                  <input type="text" class="form-control input-small" value="0" id="txtpaymentAmountnew" />
                                </td>

                               
                                        </tr>
							
</table>

                                        
                                    <div  class="btn btn-primary" id="btnOnlineBilling">
                                    Settle Bill
                                     </div> 
                                   
                                    
                                    </div>
 <div id="dvCreditWindow" style="display:none">
                                                               <div class="credit-select">  <asp:DropDownList id="ddlChosseCreditnew" ClientIDMode="Static" runat="server">
                                           
                                             </asp:DropDownList></div>
                                           <div class="credit-img"><img id="btnAddCreditCustomer" style="cursor: pointer;width: 28px;background-color: black;"src="images/adduser-white.png"></div>
                                    <label id="lblCreditCustomerNamenew">
                                                </label>
                                    <div  class="btn btn-primary" id="btnCreditBilling">
                                    Settle Bill
                                     </div> 
                                   

                                   
                                    </div>


            
    </form>
  <script type="text/javascript">
    function TenderAmount() {
      var NetAmount = parseFloat($("#dvnetAmount").html());
      $("#TotalNetAmount").val(NetAmount);
      $("#TenderAmounts").dialog({
        autoOpen: true,
        top:50,
        width: 356,
        resizable: false,
        modal: false,
        open: function (event, ui) {
          $('.ui-dialog').css('z-index', 100);
          $('.ui-widget-overlay').css('z-index', 100);
        },
      });
    }
    $("#TenderAmount").keyup(function () {
      var TenderVal = parseFloat($("#TenderAmount").val());
      var TotalNetAmountval = parseFloat($("#TotalNetAmount").val());
      var MinusTenderAmount =  TenderVal-TotalNetAmountval;
      $("#TotalTenderAmount").html(MinusTenderAmount);
    });
  </script>
     
  <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          jQuery("#jQGridDemoBill").GridUnload();

          jQuery("#jQGridDemoBill").jqGrid({
              url: 'handlers/ManageBills.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CreditBank','UserName','Time', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer','CashCustAddr','CreditCustAddr','DeliveryCharges','KKCPer','KKCAmt','SBCPer','SBCAmt','EmpName'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                            { name: 'UserName', index: 'UserName', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Time', index: 'Time', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },

                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCPer', index: 'KKCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCAmt', index: 'KKCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCPer', index: 'SBCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCAmt', index: 'SBCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'EmpName', index: 'EmpName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },

              ],

              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'BillNowPrefix',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Bills List",




          });

          var $grid = $("#jQGridDemoBill");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\" class='txt'></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $(function () {
              $('#globalSearchText').keyboard();




          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


              $("#btnEdit").css({ "display": "none" });
              $("#btnDelete").css({ "display": "none" });
                $("#btnShowScreen").css({ "display": "none" });


                for (var i = 0; i < arrRole.length; i++) {

                    if (arrRole[i] == 9) {

                        $("#btnShowScreen").css({ "display": "block" });
                    }
                    if (arrRole[i] == 2) {

                        $("#btnDelete").css({ "display": "block" });
                    }
                    if (arrRole[i] == 3) {
                        $("#btnEdit").css({ "display": "block" });
                        m_BillNowPrefix = 0;
                        m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

                    }
                }




             


              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
            //  $("#txtddlMobSearchBox").val(CashCustName);

            //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

          }
      });





          $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
                  {
                      refresh: false,
                      edit: false,
                      add: false,
                      del: false,
                      search: false,
                      searchtext: "Search",
                      addtext: "Add",
                  },

                  {//SEARCH
                      closeOnEscape: true

                  }

                    );


          var DataGrid = jQuery('#jQGridDemoBill');
          DataGrid.jqGrid('setGridWidth', '700');


      }
  </script>
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe><script>

            $(".txt").click(function () {
            $.ajax({
                type: "POST",
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",

                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {
                }

            });
            $(this).focus();
        });
    </script>
        
</asp:Content>

