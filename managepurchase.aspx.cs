﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class managepurchase : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

          


            hdntodaydate.Value = DateTime.Now.ToShortDateString();
            BindDropDownList();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();
            ddlSupplier.DataSource = new PurchaseBLL().GetAllSupplier();
            ddlSupplier.DataTextField = "CName";
            ddlSupplier.DataValueField = "CCode";
            ddlSupplier.DataBind();
            ListItem li = new ListItem();
            li.Text = "--SELECT--";
            li.Value = "0";
            ddlSupplier.Items.Insert(0, li);
            ddlGodown.DataSource = new GodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_Id";
            ddlGodown.DataBind();
            ddlGodown.Items.Insert(0, li);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);
            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }
        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string BindServices()
    {
        string serviceData = new ProductBLL().GetActiveOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;

        var JsonData = new
        {
            ServiceOptions = serviceData,


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string BindItems()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string serviceData = new ProductBLL().GetPurchaseOptionsnew(Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;

        var JsonData = new
        {
            ItemOptions = serviceData,


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string BindProducts(string itemcode)
    {
        string Branch = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string serviceData = new ProductBLL().KeywordSearch(itemcode, "", Branch);



        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;

        var JsonData = new
        {
            ProductOptions = serviceData,


        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetLast3rec(string itemcode)
    {

        int count = 1;
        Connection con = new Connection();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = Int32.MaxValue;
        StringBuilder str = new StringBuilder();
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select top 3 al.CNAME,pm.Bill_No,cast(pm.GR_Date as varchar(12)) as GR_Date,pd.Qty,pd.rate from Purchase_master as pm join  Purchase_Detail as pd on  pm.GRN_No=pd.GRN_No join  ACC_LEDGERS as al on  pm.Supplier_id=al.CCODE where pd.item_code=@itemcode order by GR_DATE desc", conn);
            cmd.Parameters.AddWithValue("@itemcode", itemcode);
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            SqlDataReader rd = cmd.ExecuteReader();
            while (rd.Read())
            {
                str.Append(string.Format("<tr><td>" + count + "</td><td>" + rd["Bill_No"].ToString() + "</td><td>" + rd["GR_Date"].ToString() + "</td><td>" + rd["CNAME"].ToString() + "</td><td>" + rd["Qty"].ToString() + "</td><td>" + rd["rate"].ToString() + "</td></tr>"));
                count = count + 1;
            }

        }
        var JsonData = new
        {
            Options = str.ToString(),


        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string chkbillno(string billno, string supplier)
    {

        string val = "0";
        Connection con = new Connection();
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select bill_no from Purchase_Master where  bill_no=@billno and supplier_id=@supplier", conn);
            cmd.Parameters.AddWithValue("@billno", billno);
            cmd.Parameters.AddWithValue("@supplier", supplier);
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                val = "1";
            }

        }

        return val;
    }


    public void BindDropDownList()
    
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new ProductBLL().GetPurchaseOptions2(hdnProducts, Branch);
    }

    [System.Web.Services.WebMethod]
    public static string GetServerDate(string format)
    {
        if (format.Equals("utc"))
        {
            return DateTime.Now.ToUniversalTime().ToString();
        }
        else
        {
            return DateTime.Now.ToLocalTime().ToString();
        }
    }



    [WebMethod]
    public static string GetByTaxStructure(decimal TaxRate)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        TaxStructure objTaxStructure = new TaxStructure()
        {
            Tax_Rate = TaxRate
        };

        new TaxStructureBLL().GetTaxStructure(objTaxStructure, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {

            PurchaseData = objTaxStructure,


        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindProducts()
    {
        return "Hello";
        // string productData = new ProductBLL().GetPurchaseOptions();
        //return productData;

        //JavaScriptSerializer ser = new JavaScriptSerializer();
        //ser.MaxJsonLength = int.MaxValue;

        //var JsonData = new
        //{
        //    ProductOptions = productData,


        //};
        //return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Insert(string GrnDate, string BillNo, string BillDate, string GrNo, string GrDate,
        string VehNo, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
        string freeArr, string dis1Arr, string dis2Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr,
        string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, string arrExcise, string arrMargin, decimal SurchrgAmt, string PurType, decimal ExciseAmt, string ExciseAmtArr,
        string HsnArr,string NetRateArr, string gstvat, string PurchaseType)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');


        if (GrNo == "0")
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Purchase objPurchase = new Purchase()
        {

            GRNDate = Convert.ToDateTime(GrnDate),
            BillNo = BillNo,
            BillDate = Convert.ToDateTime(BillDate),
            GRNo = GrNo,
            GRDate = Convert.ToDateTime(GrDate),
            VehNo = VehNo,
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3P = dis3per,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            SurchrgAmt = SurchrgAmt,
            TaxAmt = taxamt,
            RawChk = "PUR",
            Purtype = PurType,
            Tcs_AccCode = "test",
            ExciseAmt = ExciseAmt,
            BranchId = Branch,
            UserNo = UserNo,
            gstvat = gstvat,
            PurchaseType = PurchaseType,

        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
        string[] freeData = freeArr.Split(',');
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');
        string[] Excisedata = arrExcise.Split(',');
        string[] Margindata = arrMargin.Split(',');
        string[] ExciseAmtdata = ExciseAmtArr.Split(',');
        string[] HsnArrdata = HsnArr.Split(',');
        string[] NetrateArrdata = NetRateArr.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("GrnNo");
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("Excise");
        dt.Columns.Add("ExciseAmt");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
        dt.Columns.Add("ItemMargin");
        dt.Columns.Add("ItemBasRateWTax");
        dt.Columns.Add("surval");
        dt.Columns.Add("hsn");
        dt.Columns.Add("NetRate");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");



        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["GrnNo"] = Convert.ToInt64(1);
            dr["OrderNo"] = "0";
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = dis3per;
            dr["Excise"] = Convert.ToDecimal(Excisedata[i]);
            dr["ExciseAmt"] = Convert.ToDecimal(ExciseAmtdata[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);
            dr["Free"] = Convert.ToDecimal(freeData[i]);
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);
            dr["ItemMargin"] = Convert.ToDecimal(Margindata[i]);
            dr["ItemBasRateWTax"] = Convert.ToDecimal(dis2Data[i]);
            dr["surval"] = Convert.ToDecimal(dis2Data[i]);
            dr["hsn"] = Convert.ToString(HsnArrdata[i]);
            dr["NetRate"] = Convert.ToString(NetrateArrdata[i]);
            dt.Rows.Add(dr);

        }

        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }

        status = new PurchaseBLL().InsertUpdate(objPurchase, dt, dt1);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Purchase = objPurchase

        };
        return ser.Serialize(JsonData);
    }




    [WebMethod]
    public static string Update(Int64 GrnNo, string GrnDate, string BillNo, string BillDate, string GrNo, string GrDate,
        string VehNo, int SupplierId, bool Dis1InRs, bool Dis2InRs, bool Dis2After1, string IsLocal, bool TaxBeforeDis1,
        bool TaxBeforeDis2, int GodownId, decimal TotalAmount, decimal NetAmount,
        decimal Adjustments, decimal DisplayAmount, decimal ODisAmt, decimal ODisPer, decimal dis3per, decimal dis3amt, bool dis3afterdis1dis2, decimal dis1amt, decimal dis2amt, decimal billvalue, decimal taxp, decimal taxamt, string codeArr, string qtyArr, string amountArr,
        string freeArr, string dis1Arr, string dis2Arr, string taxArr, string pidArr, string mrpArr, string rateArr, string srateArr, string taxaamtarr,
        string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, string arrExcise, string arrMargin, decimal SurchrgAmt, string PurType, decimal ExciseAmt, string ExciseAmtArr, string HsnArr,string NetRateArr
        )
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.PURCHASE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;
        if (roles.Count() == 0)
        {
            status = -11;
        }
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Purchase objPurchase = new Purchase()
        {
            GrnNo = Convert.ToInt32(GrnNo),
            GRNDate = Convert.ToDateTime(GrnDate),
            BillNo = BillNo,
            BillDate = Convert.ToDateTime(BillDate),
            GRNo = GrNo,
            GRDate = Convert.ToDateTime(GrDate),
            VehNo = VehNo,
            SupplierId = SupplierId,
            Dis1InRs = Dis1InRs,
            Dis2InRs = Dis2InRs,
            Dis2AftDedDis1 = Dis2After1,
            IsLocal = IsLocal,
            TaxAfterDis1 = TaxBeforeDis1,
            TaxAfterDis2 = TaxBeforeDis2,
            GodownId = GodownId,
            TotalAmount = TotalAmount,
            NetAmount = NetAmount,
            Adjustment = Adjustments,
            DisplayAmount = DisplayAmount,
            ODisAmt = ODisAmt,
            ODisP = ODisPer,
            Dis3P = dis3per,
            Dis3Amt = dis3amt,
            Dis3AftDis1PDis2 = dis3afterdis1dis2,
            Dis1Amt = dis1amt,
            Dis2Amt = dis2amt,
            BillValue = billvalue,
            TaxP = taxp,
            SurchrgAmt = SurchrgAmt,
            TaxAmt = taxamt,
            RawChk = "PUR",
            Purtype = PurType,
            Tcs_AccCode = "test",
            ExciseAmt = ExciseAmt,
            BranchId = Branch,
            UserNo = UserNo,

        };

        string[] codeData = codeArr.Split(',');
        string[] qtyData = qtyArr.Split(',');
        string[] amountData = amountArr.Split(',');
        string[] pidData = pidArr.Split(',');
        string[] taxData = taxArr.Split(',');
        string[] freeData = freeArr.Split(',');
        string[] dis1Data = dis1Arr.Split(',');
        string[] dis2Data = dis2Arr.Split(',');
        string[] mrpData = mrpArr.Split(',');
        string[] rateData = rateArr.Split(',');
        string[] srateData = srateArr.Split(',');
        string[] taxaamtdata = taxaamtarr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');
        string[] Excisedata = arrExcise.Split(',');
        string[] Margindata = arrMargin.Split(',');
        string[] ExciseAmtdata = ExciseAmtArr.Split(',');
        string[] HsnArrdata = HsnArr.Split(',');
        string[] NetrateArrdata = NetRateArr.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("GrnNo");
        dt.Columns.Add("OrderNo");
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1");
        dt.Columns.Add("Dis2");
        dt.Columns.Add("Dis3");
        dt.Columns.Add("Excise");
        dt.Columns.Add("ExciseAmt");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("TaxAmt");
        dt.Columns.Add("Free");
        dt.Columns.Add("SaleRate");
        dt.Columns.Add("ItemMargin");
        dt.Columns.Add("ItemBasRateWTax");
        dt.Columns.Add("surval");
        dt.Columns.Add("hsn");
        dt.Columns.Add("NetRate");
        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");



        for (int i = 0; i < codeData.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["GrnNo"] = Convert.ToInt16(1);
            dr["OrderNo"] = "0";
            dr["ItemCode"] = Convert.ToString(codeData[i]);
            dr["Qty"] = Convert.ToDecimal(qtyData[i]);
            dr["MRP"] = Convert.ToDecimal(mrpData[i]);
            dr["Rate"] = Convert.ToDecimal(rateData[i]);
            dr["Amount"] = Convert.ToDecimal(amountData[i]);
            dr["Dis1"] = Convert.ToDecimal(dis1Data[i]);
            dr["Dis2"] = Convert.ToDecimal(dis2Data[i]);
            dr["Dis3"] = dis3per;
            dr["Excise"] = Convert.ToDecimal(Excisedata[i]);
            dr["ExciseAmt"] = Convert.ToDecimal(ExciseAmtdata[i]);
            dr["TaxP"] = Convert.ToDecimal(taxData[i]);
            dr["TaxAmt"] = Convert.ToDecimal(taxaamtdata[i]);
            dr["Free"] = Convert.ToDecimal(freeData[i]);
            dr["SaleRate"] = Convert.ToDecimal(srateData[i]);
            dr["ItemMargin"] = Convert.ToDecimal(Margindata[i]);
            dr["ItemBasRateWTax"] = Convert.ToDecimal(dis2Data[i]);
            dr["surval"] = Convert.ToDecimal(dis2Data[i]);
            dr["hsn"] = Convert.ToString(HsnArrdata[i]);
            dr["NetRate"] = Convert.ToString(NetrateArrdata[i]);
            dt.Rows.Add(dr);

        }

        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }


        status = new PurchaseBLL().InsertUpdate(objPurchase, dt, dt1);

        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Status = status,
            Purchase = objPurchase

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string BindPurchaseDetail(int pid)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int cntr = 0;
        string serviceData = new PurchaseBLL().GetPurchaseDetailOptions(pid, Branch, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindItemsByKeyWord(string stext)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        
        string serviceData = new PurchaseBLL().GetProductsByKeywrod(stext,Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            ServiceData = serviceData,
           
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertGodown(string title)
    {

        Godowns objGodowns = new Godowns()
        {
            Godown_Name = title.Trim().ToUpper()

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();

        int status = new GodownsBLL().InsertUpdate(objGodowns);
        var JsonData = new
        {
            Godown = objGodowns,
            Status = status
        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string FillSettings()
    {
        Int32 AdminId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        string PRate;
        string PSRate;
        string PMRP;
        string PAmt;
        string PDis1;
        string PDis2;
        string PTax1;
        string PTax2;

        new CommonSettingsBLL().GetPurchaseReceiptSettings(out PRate, out PSRate, out PMRP, out PAmt, out PDis1, out PDis2, out PTax1, out PTax2, AdminId);
        var JsonData = new
        {

            PRate = PRate,
            PSRate = PSRate,
            PMRP = PMRP,
            PAmt = PAmt,
            PDis1 = PDis1,
            PDis2 = PDis2,
            PTax1 = PTax1,
            PTax2 = PTax2,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetPurchaseOrderImport(int orderId)
    {
        PurchaseOrder objPurchaseOrder = new PurchaseOrder() { OrderNo = orderId };

        int cntr = 0;
        string serviceData = new PurchaseOrderBLL().GetPurchaseOrderImport(objPurchaseOrder, out cntr);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            PurchaseData = objPurchaseOrder,
            ServiceData = serviceData,
            Counter = cntr

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string FillBasicSettings()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        BasicPurchaseSetting ObjSettings = new BasicPurchaseSetting() { BranchId = Branch };
        new BasicPurchaseSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    //protected void btnchkkey_Click(object sender, EventArgs e)
    //{
    //    if (!IsPostBack)
    //    {
    //        BindDropDownList();
            
    //    }
    //}


}

