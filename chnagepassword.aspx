﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="chnagepassword.aspx.cs" Inherits="chnagepassword" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
        <script src="ViewModel/UserViewModel.js" type="text/javascript"></script>
        <script src="Scripts/jquery-2.0.3.min.js"></script>
        <script src="Scripts/knockout-3.0.0.js"></script>
        <script src="Scripts/knockout.validation.js" type="text/javascript"></script>
        <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
        <script src="js/grid.locale-en.js" type="text/javascript"></script>
        <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
        <link href="css/customcss/manageusers.css" rel="stylesheet" />

        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />

        <script language="javascript" type="text/javascript">
            $(document).ready(

                function() {

                });
        </script>
        <style>
            table#frmCity td.headings {
                width: 200px;
            }
            .change-pass-btn {
                width: 140px !important;
                font-size: 14px !important;
            }
            @media(max-width:400px)
            {
                #frmCity input[type="password"] {
	                width: 110px;
                }
            }
            @media(max-width:480px) {
                table#frmCity td.headings {
                    width: 150px;
                }
            }
        </style>
        <form runat="server" id="formID" method="post">
            <asp:HiddenField ID="hdnRoles" runat="server" />

            <div class="right_col change-password-right-col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Change Password</h3>
                        </div>
                   <!--     <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">

                                </div> 
                            </div>
                        </div> -->
                    </div>
                    <div class="clearfix"></div>

                    <div class="x_panel edit_manage_user">
                        <div class="x_title">
                            <h2>Add/Edit Password</h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                            <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed manageuser_leftform">

                                <tr>
                                    <td class="headings">Old Password:</td>
                                    <td>
                                        <input type="Password" name="txtOldPassword" class="form-control validate required alphanumeric" data-index="1" id="txtOldPassword" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="headings">New Password:</td>
                                    <td>
                                        <input type="Password" name="txtNewPassword" class="form-control validate required alphanumeric" data-index="1" id="txtNewPassword" runat="server" />
                                    </td>
                                </tr>

                                <tr>
                                    <td class="headings">Confirm Password:</td>
                                    <td>
                                        <input type="Password" name="txtCnfrmPassword" class="form-control validate required alphanumeric" data-index="1" id="txtConfirmPassword" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td></td>
                                                <td>

                                                    <asp:Button ID="btnAdd" Text="Change Password" class="btn btn-primary btn-small change-pass-btn" runat="server" onclick="btnAdd_Click" />

                                                </td>

                                            </tr>
                                        </table>
                                    </td>

                                </tr>

                            </table>

                        </div>
                    </div>

                </div>

            </div>

        </form>

    </asp:Content>