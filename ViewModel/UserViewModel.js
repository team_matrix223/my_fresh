﻿/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />

 
 
function User(data) {
    this.UserNo = ko.observable(data.UserNo);
    this.User_ID = ko.observable(data.User_ID);
    this.UserPWD = ko.observable(data.UserPWD);
    this.Discontinued = ko.observable(data.Discontinued);
    this.Counter_NO = ko.observable(data.Counter_NO);
    this.BranchId=ko.observable(data.BranchId);

}
function Designation(data) {
    this.DesgID = ko.observable(data.DesgID);
    this.DesgName = ko.observable(data.DesgName);
  
}

 
 function Branch(data) {
    this.BranchId= ko.observable(data.BranchId);
    this.BranchName = ko.observable(data.BranchName);
  
}
 


function UserViewModel() {
    var self = this;
    self.UserNo = ko.observable(0);
    self.User_ID = ko.observable().extend({ required: true }); ;
    self.UserPWD = ko.observable().extend({ required: true }); ;
    self.Discontinued = ko.observable(true);
    self.Counter_NO = ko.observable(0);
    self.Users = ko.observableArray([]);
    self.Designations = ko.observableArray([]);
    self.ButtonText=ko.observable("Save");
    self.IsVisible=ko.observable(false);
    self.BranchId=ko.observable(0);
    self.Branches=ko.observableArray([]);

       self.errors = ko.validation.group([self.User_ID, self.UserPWD]);
    self.hasError = function () {
        return self.errors().length > 0;
    };


    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    function ResetControls()
    {
      self.IsVisible(false);
    self.ButtonText("Save");
    self.UserNo(0);    
    self.User_ID("");
    self.UserPWD("");
    self.Discontinued(false);
    self.Counter_NO(0);
    self.BranchId(0);
      self.removeErrors();

    }


    self.Cancel=function(objUser)
    {
  ResetControls();
    };


    self.InsertUpdate = function (objUser) {

        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }



                $.ajax({
                    type: "POST",
                    url: 'manageusers.aspx/InsertUpdate',

                     data: ko.toJSON({ objUser: objUser }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (results) {
                    var obj = jQuery.parseJSON(results.d);
                     if(obj.Status==-1)
                     {
                     alert("User Name Already Exists");

                     }
                     else
                     {
                     
                     if(objUser.UserNo()==0)
                     {
                     alert("User Added Successfully");

                     }
                     else
                     {
                     alert("User Updated Successfully");

                     }
                     ResetControls();
                     BindGrid();
                     
                     }


                    },
                    error: function (err) {
                        alert(err.status + " - " + err.statusText);
                    }
                });

        ResetControls();


    }

    


  //------------------FETCH ALL Users  And Designations ON PAGE LOAD-------------------


    $.ajax({
        type: "POST",
        url: 'manageusers.aspx/GetDesignations',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var designations = $.map(results.d, function (item) {
                return new Designation(item)
            });
            self.Designations(designations);


        },

        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        }

    });




    
    $.ajax({
        type: "POST",
        url: 'manageusers.aspx/GetBranches',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var designations = $.map(results.d, function (item) {
                return new Branch(item)
            });
            self.Branches(designations);


        },

        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        }

    });


  
    
    //$.ajax({
    //    type: "POST",
    //    url: 'manageusers.aspx/GetAllUsers',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (results) {
    //        var users = $.map(results.d, function (item) {
    //          return new User(item)
    //        });
    //        self.Users(users);


    //    },

    //    error: function (xhr, ajaxOptions, thrownError) {

    //                    var obj = jQuery.parseJSON(xhr.responseText);
    //                    alert(obj.Message);
    //                }
        
    //});
 

    //--------------------------------------------------------------------------

    BindGrid();
           function BindGrid()
     {
  
     jQuery("#jQGridDemo").GridUnload();

              jQuery("#jQGridDemo").jqGrid({
           url: 'handlers/ManageUsers.ashx',
            ajaxGridOptions: { contentType: "application/json" },
            datatype: "json",
        
            colNames: ['UserId','User Name','Password',  'Designation','BranchId','Discontinued'],
            colModel: [
              { name: 'UserNo',key:true, index: 'UserNo', width: 50, stype: 'text',searchoptions : { sopt: ['eq']},sortable: true,hidden:false },
                         { name: 'User_ID', index: 'User_ID', width: 150, stype: 'text', sortable: true,hidden:false, hidden: false, editable: true ,editrules: { required: true }}, 
                         { name: 'UserPWD', index: 'UserPWD', width: 150, stype: 'text', sortable: true, hidden: true, editable: true ,editrules: { required: true }},                      
                         { name: 'Counter_NO', index: 'Counter_NO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                         { name: 'BranchId', index: 'BranchId', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                         
                         { name: 'Discontinued', index: 'Discontinued', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },

                       ],
            rowNum: 10,
          
            mtype: 'GET',
              toolbar: [true, "top"],
            loadonce: true,
            rowList: [10, 20, 30],
            pager: '#jQGridDemoPager',
            sortname: 'UserNo',
            viewrecords: true,
            height: "100%",
            width:"700px",
            sortorder: 'desc',
             ignoreCase: true,
            caption: "Users List",
         
           
                    
             
        });

        var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



                        $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  
                    
                  self.UserPWD($('#jQGridDemo').jqGrid('getCell', rowid, 'UserPWD'));
                  self.User_ID($('#jQGridDemo').jqGrid('getCell', rowid, 'User_ID'));
                  self.UserNo($('#jQGridDemo').jqGrid('getCell', rowid, 'UserNo'));
                  self.Counter_NO($('#jQGridDemo').jqGrid('getCell', rowid, 'Counter_NO'));

                  self.BranchId($('#jQGridDemo').jqGrid('getCell', rowid, 'BranchId'));

                   self.ButtonText("Update");
                   self.IsVisible(true);
               if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Discontinued') == "true") {
                     self.Discontinued(true);
                
                    }
                    else {
                        self.Discontinued(false);
                

                    }
              
                  
                   
                }
            });

           $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                   {
                       refresh:false,
                       edit: false,
                       add: false,
                       del: false,
                       search: false,
                       searchtext: "Search",
                       addtext: "Add",
                     } ,
                  
                   {//SEARCH
                       closeOnEscape: true

                   }

                     );
         
    
        var DataGrid = jQuery('#jQGridDemo');
        DataGrid.jqGrid('setGridWidth', '700');
        

      }

    //--------------------------------------------------------------------------

}


$(document).ready(function () {
    ko.applyBindings(new UserViewModel());
});