﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;

public partial class Default3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CheckRole();
            ddcustomertype();
        }
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.OtherPayment));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }

    [WebMethod]
    public static string Insert(int OtherPaymentId, string Title, bool IsActive, int PaymetMode)
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.OtherPayment));

        string[] arrRoles = sesRoles.Split(',');


        if (OtherPaymentId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;
            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        OtherPaymentMode objOtherPayment = new OtherPaymentMode()
        {
            OtherPayment_ID = OtherPaymentId,
            OtherPayment_Name = Title.Trim().ToUpper(),
            PaymetMode = PaymetMode,
            IsActive = IsActive,
            UserId = Id,

        };





        status = new OtherPaymentModeBLL().InsertUpdate(objOtherPayment);
        var JsonData = new
        {
            company = objOtherPayment,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
    public void ddcustomertype()
    {
        mst_customer_rate msr = new mst_customer_rate();
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose", "0");
        dd_customername.Items.Insert(0, listItem1);

    }

    [WebMethod]
    public static string Delete(Int32 OtherPaymentId)
    {
        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.OtherPayment));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }

        OtherPaymentMode objOtherPayment = new OtherPaymentMode()
        {
            OtherPayment_ID = OtherPaymentId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new OtherPaymentModeBLL().DeleteOtherPaymentMode(objOtherPayment);
        var JsonData = new
        {
            OtherPayment = objOtherPayment,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

}


