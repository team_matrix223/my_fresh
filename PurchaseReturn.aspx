﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="PurchaseReturn.aspx.cs" Inherits="PurchaseReturn" %>

<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/Inventory.css" rel="stylesheet" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script src="js/jscommoncalculation.js" type="text/javascript"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js"></script>
    <script>

        function ApplyRoles(Roles) {

            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


        $(function () {
            $("#txtBreakageDate1").datepicker({
                yearRange: '1900:2030',
                changeMonth: true,
                changeYear: true,
                dateFormat: 'mm-dd-yy'
            });

        });

    </script>
    <style>
    
        #addDialog {
            /* height:1000px !important;*/
            height: 616px !important;
            min-height:616px !important;
            max-height:616px !important;
            background-color:#fff !important;
        }

        .page-title {
            height: auto;
            padding:0;
        }

        div#gbox_jQGridDemo {
            overflow: auto;
            max-height: 350px;
            z-index:1;
        }
        td.jq-cls {
            z-index: -1;
        }
        td.jq-cls.jq-zin {
            z-index: 99;
        }
       div#dvlast3Dialog {
            left: 360px;
            top: 120px;
        }
       button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
            display: none;
        }
       button#dvImport {
            font-size: 12px;
            padding: 4px 10px;
       }
       table.manage_table_top.purchase-manage-table-top {
            margin-top: 0;
        }
       .manage_table_top td {
            padding: 1px 10px;
        }
       .manage_table_top select, .manage_table_top input {
            height: 17px;
        }
       .manage_table_top.purchase-manage-table-top.mng-pur-top-tbl td{
            vertical-align: top;
        }
       [name="ddlProducts"] {
            width: 240px;
        }
       [name="txtTaxPer"] {
            width: 45px !important;
        }
       .manage_table_top input {
            padding: 0 2px;
        }
       .btn-sub-section-bottom {
          /*  background: #fff;
            padding: 30px 0;
            box-sizing: border-box;*/
            float: left;
            width: 100%;
        }
       .purchase-row{
           margin-left:0;
           margin-right:0;
       }
       div#btnSubmit, div#btncncl {
            margin: 1px;
        }   
       @media(max-width:480px)
       {
           div#dvlast3Dialog 
           {
                left: 0;
                top: 70px;
                overflow: auto;
                height: 200px;
                width: 100%;
            }
           #innerSave
           {
                width: 296px !important;
                overflow: auto !important;
                height: 178px !important;
           }
           .bill-value-mng-pur-tbl {
	            top: 200px !important;
	            left: 12px !important;
            }
           .tax-tble-mage-pur {
	            top: 20px !important;
	            left: 10px !important;
            }

       }

        @media (min-width:481px) and (max-width:567px)
       {
           div#dvlast3Dialog 
           {
                left: 0;
                top: 70px;
                overflow: auto;
                width: 100%;
            }
           #innerSave
           {
                width: 270px !important;
                overflow: auto !important;
                
           }
           .bill-value-mng-pur-tbl {
	            top: 40px !important;
	            left: 193px !important;
            }
           .tax-tble-mage-pur{
	            top: 266px !important;
	            left: 9px !important;
            }
           #dvTaxDenomination {
	            width: 185px;
            }

       }

       @media (min-width:568px) and (max-width:667px)
       {
           div#dvlast3Dialog 
           {    
                left: 0;
                top: 70px;
                overflow: auto;
           }
           
           .bill-value-mng-pur-tbl {
	            top: 40px !important;
	            left: 252px !important;
            }
           .tax-tble-mage-pur{
	            top: 287px !important;
	            left: 9px !important;
            }
            #innerSave {
                width: 300px;
            }

       }
        @media (min-width:668px) and (max-width:767px)
       {
           div#dvlast3Dialog 
           {    
                left: 0;
                top: 70px;
                overflow: auto;
           }
           
           .bill-value-mng-pur-tbl {
	            top: 40px !important;
	            left: 252px !important;
            }
           .tax-tble-mage-pur{
	            top: 239px !important;
	            left: 9px !important;
            }
           
       }
       @media (min-width:768px) and (max-width:991px)
       {
           div#dvlast3Dialog {
	            left: 16%;
	           
            }
           .bill-value-mng-pur-tbl {
	            left: 360px !important;
            }
           .tax-tble-mage-pur{
	            left: 118px !important;
            }
            .btn-sub-section-bottom {
                padding: 0px 0;
            }

       }
        @media (min-width:992px) and (max-width:1024px) {
            div#dvlast3Dialog {
	            left: 23%;
	            top: 120px;
            }
            .bill-value-mng-pur-tbl {
	            left: 467px !important;
            }
           .tax-tble-mage-pur{
	            left: 224px !important;
            }
            .mng-pur-rturn-wdth {
	            width: auto;
            }
           /* .btn-sub-section-bottom {
	            padding: 0px 0 50px;
            }*/
           .manage_table_top table td {
	            font-size: 11px;
	        }
            [name="txtServiceId"] {
	            width: 30px;
                font-size:13px;
            }
            [name="ddlProducts"] {
	            width: 116px;
            }
            [name="txtMargin"], [name="txtBalQty"], [name="txtFree"], [name="txthsn"] {
	            width: 30px;
            }
            [name="txtQty"] {
	            width: 30px !important;
            }
            [name="txtRate"] {
	            width: 53px !important;
            }
            [name="txtSRate"] {
	            width: 53px !important;
            }
            [name="txtMRP1"] {
	            width: 53px !important;
            }
            [name="txtAmount"] {
	            width: 53px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 39px !important;
            }
            [name="txtTaxper"]{
	            width: 35px !important;
            }
        }
        @media(min-width:1025px) and (max-width:1100px)
        {
            [name="txtServiceId"] {
	            width: 30px;
            }
            [name="ddlProducts"] {
	            width: 135px;
            }
            [name="txtMargin"], [name="txtBalQty"], [name="txtFree"], [name="txthsn"] {
	            width: 31px;
            }
            [name="txtQty"] {
	            width: 31px !important;
            }
            [name="txtRate"], [name="txtSRate"], [name="txtMRP1"],  [name="txtAmount"] {
	            width: 55px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 39px !important;
            }
            [name="txtTaxper"]{
	            width: 35px !important;
            }
           /* .btn-sub-section-bottom {
	            padding: 0px 0 50px;
            }*/

        }
        @media(min-width:1101px) and (max-width:1200px)
        {
            [name="txtServiceId"] {
	            width: 42px;
            }
            [name="ddlProducts"] {
	            width: 170px;
            }
            [name="txtMargin"], [name="txtBalQty"], [name="txtFree"], [name="txthsn"] {
	            width: 35px;
            }
            [name="txtQty"] {
	            width: 35px !important;
            }
            [name="txtRate"], [name="txtSRate"], [name="txtMRP1"],  [name="txtAmount"] {
	            width: 60px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 35px !important;
            }
            [name="txtTaxper"]{
	            width: 40px !important;
            }
           /* .btn-sub-section-bottom {
	            padding: 0px 0 50px;
            }*/
        }
         @media(min-width:1201px) and (max-width:1310px)
        {
            [name="txtServiceId"] {
	            width: 42px;
            }
            [name="ddlProducts"] {
	            width: 200px;
            }
            [name="txtMargin"], [name="txtBalQty"], [name="txtFree"], [name="txthsn"] {
	            width: 40px;
            }
            [name="txtQty"] {
	            width: 40px !important;
            }
            [name="txtRate"], [name="txtSRate"], [name="txtMRP1"],  [name="txtAmount"] {
	            width: 60px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 40px !important;
            }
            [name="txtTaxper"]{
	            width: 40px !important;
            }
          /*  .btn-sub-section-bottom {
	            padding: 0px 0 50px;
            }*/
        }
        @media (min-width:1311px) and (max-width:1400px) {
            /*  .btn-sub-section-bottom {
	                padding: 0px 0 50px;
                }*/
              .inv_sub_canc {
	                margin: 0px 0 20px;
	           }
        }
        @media (min-width:1025px) and (max-width:1400px)
        {
            .mng-pur-rturn-wdth {
	            width: auto;
            }
        }
   
         @media (min-width:1025px) and (max-width:1300px) {
              
            div#dvlast3Dialog {
	            left: 23%;
	           
            }
            .bill-value-mng-pur-tbl {
	            left: 467px !important;
            }
           .tax-tble-mage-pur{
	            left: 224px !important;
            }
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#btnSubmit").click(function () {
                $(".jq-cls").addClass("jq-zin");
            }); 
            $("#btnSave, #btnCloseMe").click(function () {
                $(".jq-cls").removeClass("jq-zin");
            });
                    
        });
    </script>
    <script>
          $( function() {
              $("#dvlast3Dialog, #dvTaxDenomination , #innerSave").draggable();
          } );
     </script>
    <form id="form1" runat="server" autocomplete="off">
        <%--<asp:HiddenField ID="hdnDate" runat="server"/>--%>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <input type="hidden" id="hdnFirstTime" value="1" />
        <input type="hidden" id="hdnCounter" value="1" />
        <input type="hidden" id="hdnUpdateCounter" value="0" />
        <input type="hidden" id="status" value="I" />
        <input type="hidden" id="hdnGrnNo" value="0" />
        <input type="hidden" id="hdntodaydate" runat="server" value="0" />
        <asp:DropDownList ID="hdnProducts" Style="display: none" runat="server">
        </asp:DropDownList>
        <div class="right_col" role="main">
            <div class="">

                <!--  <div class="title_right">
                    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                   <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search for..."style="height: 34px;">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    Go!</button>
                            </span>
                        </div>
                    </div>
                </div>-->
            </div>
            <div class="clearfix">
            </div>
            <div class="row purchase-row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Purchase</h3>
                        </div>
                    </div>

                    <div class="x_panel inventory_xpanal">
                        <div class="x_title">
                            <h2>Add/Edit Purchase</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a> </li>
                                        <li><a href="#">Settings 2</a> </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                            </ul>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="x_content">
                            <br />
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px; background-color: White"
                                                        id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td></td>
                                                <td>Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px; background-color: White"
                                                        id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="importDialog" style="display: none">
                            <table id="tbImport">
                                <tr>
                                    <td>Order Number:
                                    </td>
                                    <td>
                                        <input type="text" id="txtOrderNumber" class="form-control input-small" />
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div id="addDialog" class="inventry_adddialog" style="display: none;">

                            <div class="col-md-12 col-sm-12 col-xs-12  mng-pur-rturn-wdth">

                                <div>

                                    <table class="manage_table_top purchase-manage-table-top mng-pur-top-tbl">
                                        <tr>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>GRN No:</td>
                                                        <td>
                                                            <input type="text" id="txtGrnNo" class="form-control input-small" disabled="disabled" value="Automatic" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>GRN Date:</td>
                                                        <td>
                                                            <input type="text" id="txtGrnDate" class="form-control input-small" tabindex="-1" /></td>

                                                       
                                                    </tr>
                                                    <tr>
                                                         <td>Bill No:</td>
                                                        <td>
                                                            <input type="text" id="txtBillNo" class="form-control input-small validate required" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Bill Date:</td>
                                                        <td>
                                                            <input type="text" id="txtBillDate" class="form-control input-small"  tabindex="-1"/></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>GR No:</td>
                                                        <td>
                                                            <input type="text" id="txtGrNo" class="form-control input-small" /></td>
                                                       
                                                    </tr>
                                                    <tr>
                                                        <td>GR Date:</td>
                                                        <td>
                                                            <input type="text" id="txtGrDate" class="form-control input-small" /></td>
                                                     
                                                    </tr>
                                                    <tr>
                                                        <td>Veh No:</td>
                                                        <td>
                                                            <input type="text" id="txtVehNo" class="form-control input-small" /></td>
                                                     
                                                    </tr>
                                                    <tr>
                                                        <td>Supplier:</td>
                                                        <td>
                                                            <%--<select id="ddlSupplier"  style="width:93px;height:30px"> <option></option> </select>--%>
                                                            <asp:DropDownList ID="ddlSupplier" ClientIDMode="Static" Style="width: 100%;" runat="server"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <div style="cursor: pointer;" id="dvSupplier">
                                                                <i class="glyphicon glyphicon-plus-sign" style="color: white; display: none"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td>
                                              <table>
                                                  <tr>
                                                       <td>
                                                            <input type="checkbox" id="chkDis1InRs" />
                                                            <label for="chkDis1InRs">Dis 1 In (Rs)</label>
                                                        </td>
                                                  </tr>
                                                  <tr>
                                                         <td>
                                                            <input type="checkbox" id="chkDis2InRs" />
                                                            <label for="chkDis2InRs">Dis 2 In (Rs)</label>
                                                        </td>
                                                  </tr>
                                                  <tr>
                                                         <td>
                                                            <input type="checkbox" id="chkDis2After1" />
                                                            <label for="chkDis2After1; color:White;">Dis 2 Aft Ded Dis 1</label>
                                                        </td>
                                                  </tr>

                                                   <tr>
                                                        <td>
                                                            <input type="checkbox" id="txtTaxBeforeDis1"  name="tax" />
                                                            <label for="txtTaxBeforeDis1">GST Before Dis 1</label>
                                                        </td>
                                                    </tr>

                                                   <tr>
                                                        <td>
                                                            <input type="checkbox" id="txtTaxBeforeDis2"  name="tax" />
                                                            <label for="txtTaxBeforeDis2">GST Before Dis 2</label>
                                                        </td>
                                                    </tr>
                                              </table>
                                            </td>
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" id="rdbcash" name="cash" checked="checked" />
                                                            <label for="rdbcash">Cash</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="rdbcredit" name="cash" />
                                                            <label for="rdbcredit">Credit</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <input type="radio" id="rbLocal" name="local" checked="checked" />
                                                            <label for="rbLocal">Local</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="rbOutStation" name="local" />
                                                            <label for="rbOutStation">Outstation</label>
                                                        </td>
                                                    </tr>

                                                     <tr>
                                                        <td>
                                                            <input type="radio" id="rbGST" name="gstvat" checked="checked" />
                                                            <label for="rbGST">GST</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="rbVAT" name="gstvat" />
                                                            <label for="rbVAT">VAT</label>
                                                        </td>
                                                    </tr>
                                                     <tr>

                                                          <td>
                                                            <input type="radio" id="rbExclusive" name="Exclusive" checked="checked" />
                                                            <label for="rbExclusive">Exclusive</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="rbInclusive" name="Exclusive"  />
                                                            <label for="rbInclusive">Inclusive</label>
                                                        </td>
                                                       
                                                    </tr>
                                                   
                                                   
                                                    <tr>
                                                        <td>Godown:</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlGodown" runat="server" Style="width: 100%;" ClientIDMode="Static"></asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            <div style="cursor: pointer;" id="dvGodown">
                                                                <i class="glyphicon glyphicon-plus-sign" style="color: white; display: none"></i>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table style='width: 100%'>
                                        <tr>
                                            <td colspan="100%">
                                                <div style="overflow-y: auto; max-height: 323px;min-height:323px;">
                                                    <table class="manage_table_top" style="margin: 0px;" id="tbProducts">
                                                        <thead>
                                                            <tr>
                                                                <th>Code</th>
                                                                <th>Product</th>
                                                                <th>HSN</th>
                                                                <th>Margin</th>
                                                                <th>Bal Qty</th>
                                                                <th>Qty</th>
                                                                <th>Free</th>
                                                                <th>Rate</th>
                                                                <th>S.Rate</th>
                                                                <th>MRP</th>
                                                                <th>Amount</th>
                                                                <th id="th1">Dis1%</th>
                                                                <th id="th2">Dis2%</th>
                                                            
                                                                <th>GST%</th>
                                                             
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>

                                        <table style="width: 100%;">
                                                    <tr>
                                                        <td>
                                                            <table class="manage_table_top purchase-manage-table-top">
                                                          
                                                                    <td>
                                                                        <label>Remark:</label>
                                                                    <textarea id="txtarea" style="margin: 0px;width:339px;"></textarea>


                                                                    </td>
                                                                    <td colspan="100%">
                                                                        <table style="float: right; width: 350px;">
                                                                            <tr>
                                                                                <td>Total Amount:</td>
                                                                                <td>Rs.<label id="lblTotalAmount">0</label></td>
                                                                            </tr>
                                                                                                               
                                                                              <tr>
                                                                           <td>Less Dis:
                                                                        </td>
                                                                        <td>
                                                                         
                                                                            <input type="text" id="txtCombineDis" class="form-control input-small" value="0" readonly="readonly"/>
                                                                        </td>
                                                                            </tr>
                                                                               <tr>
                                                                           <td><label id="lblgsttxt">Add GST/IGST:</label> 
                                                                        </td>
                                                                        <td>
                                                                         
                                                                            <input type="text" id="txttaxamt" class="form-control input-small" value="0" readonly="readonly"/>
                                                                        </td>
                                                                            </tr>
                                                                              <tr>
                                                                           <td><label id="lblCESStxt">CESS:</label> 
                                                                        </td>
                                                                        <td>
                                                                         
                                                                            <input type="text" id="txtCessamt" class="form-control input-small" value="0" readonly="readonly"/>
                                                                        </td>
                                                                            </tr>
                                                                           
                                                     

                                                                             <tr>
                                                                           <td>Net Package Cost:
                                                                        </td>
                                                                        <td>
                                                                         
                                                                          <input type="text" class="form-control input-small" readonly="readonly" id="txtNetCost" value="0" /></td>
                                                                        </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                           
                                                                <tr>
                                                                    <%--<td colspan="100%">
                                                                        <div id="dvImport" style="width: 155px; color:White; cursor: pointer; color: #428BCA; text-decoration: underline;">
                                                                            Import Purchase Order</div>
                                                                    </td>--%>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td class="jq-cls  tax-tble-mage-pur" style="position: fixed;top: 241px;left: 344px;">
                                                            <div id="dvTaxDenomination" style="display: none; background-color: rgb(76, 73, 77); color: white; border: solid 1px silver; border-radius: 10px; padding: 5px;">
                                                                <table>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td></td>
                                                                                    <td style="color: White;"><label id="lblgstdeno"></label>GST Denomination:
                                                                                    </td>
                                                                                    <td></td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="color: White;"><label id="lblgstper">GST %</label>
                                                                        </td>
                                                                        <td style="color: White;"><label  id="lblgstAmt">GSTAmt</label>
                                                                        </td>
                                                                        <td style="width: 70px" style="color: White;"><label id="lblgst">GST</label>
                                                                        </td>
                                                                        <td>SurChg
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td id="gridTax" colspan="100%">
                                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                                <ItemTemplate>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <div name="tax">
                                                                                                <%#Eval("Tax_Rate") %>
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' class="txt_taxdenm_amt" name="amt">
                                                                                                0.00
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' class="txt_taxdenm_vat" name="vat">
                                                                                                0.00
                                                                                            </div>
                                                                                        </td>
                                                                                        <td>
                                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' class="txt_taxdenm_sur" name="sur">
                                                                                                0.00
                                                                                            </div>
                                                                                        </td>
                                                                                    </tr>
                                                                                </ItemTemplate>
                                                                            </asp:Repeater>
                                                                        </td>
                                                                    </tr>
                                                                   

                                                                </table>
                                                            </div>
                                                        </td>
                                                        <td class="jq-cls bill-value-mng-pur-tbl" style="position: fixed;top:45px;left: 590px;">
                                                            <div id="innerSave" style="position: relative; display: none; background-color: rgb(76, 73, 77); color: white; bottom: 0px; border: solid 1px silver; border-radius: 10px; padding: 5px;">
                                                                <table>
                                                                    <tr>
                                                                        <td colspan="2">
                                                                            <table>
                                                                                <tr>
                                                                                    <td>Bill Value:
                                                                                    </td>
                                                                                    <td style="width: 99px">
                                                                                        <input type="text" style="width: 50px; display: none" id="txtExciseAmt" />
                                                                                    </td>
                                                                                    <td>
                                                                                        <input type="text" id="txtBillValue" readonly="readonly" style="color: Black;width: 167px;" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Discount 1:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtDiscount1" style="color: Black; width: 167px;" readonly="readonly" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Discount 2:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtDiscount2" style="color: Black; width: 167px;" readonly="readonly" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <input type="checkbox" id="chkdis3" /><label>Dis3 after(Dis1 & Dis2)</label>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" style="width: 40px; color: Black" id="txtDis3Per" /><input type="text"
                                                                                id="txtDis3Amt" style="width: 127px; color: Black" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="display:none">
                                                                        <td>Excise:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtExcise" style="color: Black; width: 167px;" readonly="readonly" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr  id="trGST">
                                                                        <td><label id="lblgsttr">GST:</label>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txttax" style="width: 40px; color: Black;" readonly="readonly"/><input type="text" id="txttaxamt2" style="width: 127px; color: Black" readonly="readonly"/>
                                                                        </td>
                                                                    </tr>
                                                                                            <tr id="trSGST" style="display:none">
                                                                        <td>SGST:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtSGSTtax" style="width: 40px; color: Black;" readonly="readonly"/><input type="text" id="txttSGSTaxamt" style="width: 127px; color: Black" readonly="readonly"/>
                                                                        </td>
                                                                    </tr>
                                                                           <tr id="trIGST" style="display:none">
                                                                        <td>IGST:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtIGSTtax" style="width: 40px; color: Black;" readonly="readonly"/><input type="text" id="txtIGSTtaxamt" style="width: 127px; color: Black" readonly="readonly"/>
                                                                        </td>
                                                                    </tr>
                                                         
                                                                    <tr>
                                                                        <td>Surcharge:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtSurcharge" style="width: 40px; color: Black;" />
                                                                            <input type="text" id="txtSurchargeAmt" style="width: 127px; color: Black" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Amount:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtTotalAmount" style="color: Black; width: 167px;" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Other Discount:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtODisPer" style="width: 40px; color: Black" /><input type="text"
                                                                                id="txtODisAmt" style="width: 127px; color: Black" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Display Amount:
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtDisplayAmount" style="color: Black; width: 167px;"  />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Adj.(-)Less/(+)Add
                                                                        </td>
                                                                        <td>
                                                                            <input type="text"  style="color: Black; width: 167px;" id="txtAdjustments" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Net Amount
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" id="txtNAmount" style="color: Black; width: 167px;" readonly="readonly" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td></td>
                                                                        <td>
                                                                            <div id="btnSave" class="btn btn-primary btn-small">
                                                                                Save
                                                                            </div>
                                                                            <div id="btnCloseMe" class="btn btn-primary btn-small">
                                                                                Close
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div id="dvlast3Dialog" style="position: fixed; display: none; background-color: rgb(76, 73, 77); color: white; border: solid 1px silver; border-radius: 10px; padding: 5px;">

                                                                <table class="table">
                                                                    <tr>
                                                                        <td colspan="6" style="text-align: center; font-weight: bold; text-decoration: underline; background-color: #1479b8;">Previous Purchase(s) Of This Item</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Sr.no.</th>
                                                                        <th>BillNo</th>
                                                                        <th>Date</th>
                                                                        <th>PartyName</th>
                                                                        <th>Qty</th>
                                                                        <th>Rate</th>
                                                                    </tr>
                                                                    <tbody id="tbllastrec"></tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <button type="button" id="closetast3" class="btn btn-danger">Close</button>

                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </table>
                                                            </div>

                                                        </td>
                                                    </tr>
                                        
                                                </table>
                                            </td>
                                          
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="btn-sub-section-bottom">
                              <div class="btn btn-primary inv_sub_canc" data-toggle="modal" id="btnSubmit">
                                    <i class="fa fa-save"></i> Submit
                                </div>
                                <div id="btncncl" data-toggle="modal" class="btn   btn-danger inv_sub_canc">
                                    <i></i>Cancel
                                </div>
                            </div>
                        </div>
                        <div id="btnAdd" data-toggle="modal" class="btn btn-primary" style="margin-left: 6px">
                            <i class="fa fa-external-link"></i>New
                        </div>
                        <div id="btnEdit" data-toggle="modal" class="btn  btn-success">
                            <i class="fa fa-edit m-right-xs"></i>Edit
                        </div>
                    </div>
                </div>
            </div>
            <script>
          
                $(function () {
                    $("#txtGrnDate").datepicker({
                        yearRange: '1900:2030',
               
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'mm-dd-yy'
                    });

                });

                $(function () {
                    $("#txtBillDate").datepicker({
                        yearRange: '1900:2030',
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'mm-dd-yy'
                    });

                });


                $(function () {
                    $("#txtGrDate").datepicker({
                        yearRange: '1900:2030',
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'mm-dd-yy'
                    });

                });
            </script>
            <script language="javascript" type="text/javascript">

                function processingComplete() {


                    $.uiUnlock();
                }


                function Printt(celValue) {

                    $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                    var iframe = document.getElementById('reportout');

                    iframe = document.createElement("iframe");

                    iframe.setAttribute("id", "reportout");

                    iframe.style.width = 0 + "px";

                    iframe.style.height = 0 + "px";

                    document.body.appendChild(iframe);


                    document.getElementById('reportout').contentWindow.location = "Reports/rptPurchaseBill.aspx?Id=" + celValue;

                }


                function Settings() {


                    $.ajax({
                        type: "POST",
                        data: '{ }',
						url: "PurchaseReturn.aspx/FillSettings",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {


                            var obj = jQuery.parseJSON(msg.d);

                            $("input[name='txtServiceId']").each(
                            function (x) {

                                var counterId = $(this).attr("counter");

                                //if (obj.PRate == "0")
                                // {
                                //   $("#txtRate" + counterId).attr('disabled', 'disabled');
                                // }
                                //else 
                                // {
                                //   $("#txtRate" + counterId).removeAttr('disabled');
                                // }

                                if (obj.PSRate == "0") {
                                    $("#txtSRate" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtSRate" + counterId).removeAttr('disabled');
                                }

                                if (obj.PMRP == "0") {
                                    $("#txtMRP" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtMRP" + counterId).removeAttr('disabled');
                                }

                                if (obj.PAmt == "0") {
                                    $("#txtAmount" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtAmount" + counterId).removeAttr('disabled');
                                }


                                if (obj.PDis1 == "0") {
                                    $("#txtDis1Per" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtDis1Per" + counterId).removeAttr('disabled');
                                }



                                if (obj.PDis2 == "0") {
                                    $("#txtDis2Per" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtDis2Per" + counterId).removeAttr('disabled');
                                }


                                if (obj.PTax1 == "0") {
                                    $("#txtTaxPer" + counterId).attr('disabled', 'disabled');
                                }
                                else {
                                    $("#txtTaxPer" + counterId).removeAttr('disabled');
                                }








                            }

                        );


                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });

                }




                function BasicSettings() {


                    $.ajax({
                        type: "POST",
                        data: '{ }',
						url: "PurchaseReturn.aspx/FillBasicSettings",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {


                            var obj = jQuery.parseJSON(msg.d);

                            $("input[name='txtServiceId']").each(
                            function (x) {

                                var counterId = $(this).attr("counter");

                                var checkMRP = obj.setttingData.Check_MRP;
                                if (checkMRP == true) {

                                    var MRP = $("#txtMRP" + counterId + "").val();
                                    var SaleRate = $("#txtSRate" + counterId + "").val();
                                    var PRate = $("#txtRate" + counterId + "").val();
                                    var Product = $("#txtServiceId" + counterId).val();
                                    if (Number(MRP) < Number(SaleRate)) {
                                        alert("MRP Should be Greater Than Mrp for code " + Product + "");
                                        return;
                                    }
                                    if (Number(SaleRate) < Number(PRate)) {
                                        alert("SaleRate Should be Greater Than Purchase Rate for code " + Product + "");
                                    }
                                }







                            }

                        );


                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });

                }


                function TaxDenomination(counterId) {


                    //alert($("#ddlProducts" + counterId + ' option:selected').attr('net'));
                    var TotAmtt = 0;
                    var FinalTotAmtt = 0;
                    var Amount = $("#txtAmount" + counterId).val();
                    var TaxPer = $("#txtTaxPer" + counterId).val();

					
                    var TaxId = $("#ddlProducts" + counterId + ' option:selected').attr('taxid');


                    var TaxPer1 = $("#ddlProducts" + counterId + ' option:selected').attr('Tax');

                    //var Dis1 = $("#txtDis1Per" + counterId).val();
                    // var Dis2 = $("#txtDis2Per" + counterId).val();
                    $("input[name='txtTaxPer']").each(
                     function () {
                         var counterId1 = $(this).attr('counter');
                         if ($("#txtTaxPer" + counterId1).val() == TaxPer) {

                             var Amt = $("#txtAmount" + counterId1).val();

                             if ($("#rbLocal").prop('checked') == true) {

                                 var dis1 = $("#txtDis1Per" + counterId1).val();
                                 var dis2 = $("#txtDis2Per" + counterId1).val();
                                 var dis3 = $("#txtDis3Per" + counterId1).val();

                                 TotAmtt = Number(TotAmtt.toFixed(2)) + Number(Amt);
                                
                                 if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {


                                     FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amt);

                                     var billval = (Number(Amt) * Number(dis1)) / 100;
                                     var billval1 = (Number(Amt) * Number(dis2)) / 100;
                                     FinalTotAmtt = Number(FinalTotAmtt) - (Number(billval) + Number(billval1));

                                 }
                                 else if (($("#txtTaxBeforeDis1").prop('checked') == true) && ($("#txtTaxBeforeDis2").prop('checked') == false)) {

                                     FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amt);
                                     var newbill = 0;

                                     if ($("#chkDis1InRs").prop('checked') == true) {

                                         newbill = Number(Amt) - Number(dis2);
                                         TotAmtt = newbill;
                                         FinalTotAmtt = Number(FinalTotAmtt) - (Number(dis1) + Number(dis2));

                                     }
                                     else {


                                         var billval = (Number(Amt) * Number(dis1)) / 100;
                                         var billval1 = (Number(Amt) * Number(dis2)) / 100;
                                         newbill = Number(Amt) - Number(billval1);

                                         TotAmtt = newbill;
                                         FinalTotAmtt = Number(FinalTotAmtt) - (Number(billval) + Number(billval1));



                                     }


                                 }
                                 else if (($("#txtTaxBeforeDis1").prop('checked') == false) && ($("#txtTaxBeforeDis2").prop('checked') == true)) {


                                     FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amt);
                                     var newbill = 0;

                                     if ($("#chkDis2InRs").prop('checked') == true) {
                                         newbill = Number(Amt) - Number(dis1);
                                         TotAmtt = newbill;
                                         FinalTotAmtt = Number(FinalTotAmtt) - (Number(dis1) + Number(dis2));

                                     }
                                     else {


                                         var billval = (Number(Amt) * Number(dis1)) / 100;
                                         var billval1 = (Number(Amt) * Number(dis2)) / 100;
                                         newbill = Number(TotAmtt) - Number(billval);

                                         TotAmtt = newbill;
                                         FinalTotAmtt = Number(FinalTotAmtt) - (Number(billval) + Number(billval1));



                                     }

                                 }
                                 else if (($("#txtTaxBeforeDis1").prop('checked') == false) && ($("#txtTaxBeforeDis2").prop('checked') == false)) {



                                     FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amt);

                                     var newbill = 0;
                                     var newbill1 = 0;
                                     var newbill2 = 0;
                                     var newbill3 = 0;
                                     if ($("#chkDis1InRs").prop('checked') == true) {
                                         newbill1 = Number(dis1);
                                     }
                                     else {
                                         var billval = (Number(Amt) * Number(dis1)) / 100;
                                         newbill1 = Number(billval);
                                     }

                                     if ($("#chkDis2InRs").prop('checked') == true) {
                                         newbill2 = Number(dis2);
                                     }
                                     else {
                                         var billval1 = (Number(Amt) * Number(dis2)) / 100;
                                         newbill2 = Number(billval1);
                                     }

                                     var billval3 = (Number(Amt) * Number(dis1)) / 100;
                                     newbill3 = Number(billval3);

                                     newbill = Number(Amt) - (Number(newbill1) + Number(newbill2) + Number(newbill3));

                                     TotAmtt = newbill;
                                     FinalTotAmtt = newbill;

                                 }

                             }
                             else {

                                 TotAmtt = Number(TotAmtt.toFixed(2)) + Number(Amt);
                                 FinalTotAmtt = Number(FinalTotAmtt.toFixed(2)) + Number(Amt);
                             }

                         }



                     });

                    var Total = 0;

                    var Sur = 0;

                    $.ajax({
                        type: "POST",
                        data: '{"TaxRate":"' + TaxPer + '"}',
						url: "PurchaseReturn.aspx/GetByTaxStructure",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);
                            Surcharge = obj.PurchaseData.SurValue;
                           
                            if (obj.PurchaseData.ChkSur) {
								
                                Total = ((Number(TotAmtt) * (Number(TaxPer1) - Number(Surcharge))) / 100);
                                Sur = (Number(TotAmtt.toFixed(2)) * Number(Surcharge.toFixed(2)) / 100);
                            }
                            else {
								
                                Total = ((Number(TotAmtt) * (Number(TaxPer1))) / 100);
                                 Sur = (Number(Total.toFixed(2)) * Number(Surcharge.toFixed(2)) / 100);
                            }

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {

                            $("div[myid='amt_" + TaxId + "']").html(FinalTotAmtt.toFixed(2));
                            $("div[myid='vat_" + TaxId + "']").html(Total.toFixed(2));
                            $("div[myid='sur_" + TaxId + "']").html(Sur.toFixed(2));
                            //var sur_total = 0, gst_total=0
                            //$(".txt_taxdenm_sur").each(function () {
                          
                            //    var val = $(this).text();
                            //    sur_total +=Number(val);

                            //})
                            //$(".txt_taxdenm_vat").each(function () {

                            //    var val = $(this).text();
                            //    gst_total += Number(val);

                            //})
                            //$("#txtSurchargeAmt").val(sur_total.toFixed(2));
                            //$("#txttaxamt").val(gst_total.toFixed(2)); 
                            //$("#txttaxamt2").val(gst_total.toFixed(2));
                            //$("#txttSGSTaxamt").val(gst_total.toFixed(2));
                        }

                    });


                }

                function ServiceClick(counterId) {


                    var x = $("#ddlProducts" + counterId).val();
                    $("#ddlProducts" + counterId).html($("#<%=hdnProducts.ClientID%>").html());


         $("#ddlProducts" + counterId + " option").removeAttr("selected");
         $('#ddlProducts' + counterId + ' option[value=' + x + ']').prop('selected', 'selected');

         document.getElementById("ddlProducts" + counterId).onmouseover = null;
         document.getElementById("txtServiceId" + counterId).onmouseover = null;


     }
     function QtyChange(counterId) {


         var Rate = $("#txtRate" + counterId).val();
         var Qty = $("#txtQty" + counterId).val();


         $("#txtAmount" + counterId).val(Number(Rate) * Number(Qty));

         CommonCalculations();
         TaxDenomination(counterId);


                }

				function RateChange(counterId) {

                    if ($('#rbInclusive').is(":checked")) {
                        var SurvAl = $("#ddlProducts" + counterId + ' option:selected').attr('SurValue');
                        
                        var Rate = $("#txtRate" + counterId).val();
                        var Tax = $("#txtTaxPer" + counterId).val();
                        Tax = Tax - SurvAl;
                        var TAxAmt = ((Number(Rate) * Number(Tax)) / (100 + Number(Tax))).toFixed(2);

                        var finlRate = (Number(Rate) - Number(TAxAmt)).toFixed(2);
                        $("#txtRate" + counterId).val(finlRate);
                        var QTy = $("#txtQty" + counterId).val()
                        $("#txtAmount" + counterId).val((Number(QTy) * Number(finlRate)).toFixed(2));
                        CommonCalculations();
                        TaxDenomination(counterId);
                    }
                   


				}
     function AmtChange(counterId) {


         var Amount = $("#txtAmount" + counterId).val();
         var Qty = $("#txtQty" + counterId).val();


         $("#txtRate" + counterId).val(Number(Amount) / Number(Qty));

         CommonCalculations();
         TaxDenomination(counterId);


     }


     function ExciseChange(counterId) {


        // var Excise = $("#txtExcisePer" + counterId).val();
         var Excise = 0;
         var Amount = 0;
         $("#txtExciseAmt" + counterId).val((Number(Excise) * Number(Amount)) / 100);


         CommonCalculations();
         TaxDenomination(counterId);


     }


     function Discount1TextChange(counterId) {



         CommonCalculations();
         TaxDenomination(counterId);


     }

     function Discount2TextChange(counterId) {


         CommonCalculations();
         TaxDenomination(counterId);
     }

     function Discount3TextChange(counterId) {


         CommonCalculations();
         TaxDenomination(counterId);
     }


     function DeleteRow(counterId) {


         var len = $("input[name='txtServiceId']").length;
         if (len == 1) {
             alert("Row deletion failed. Package must contain atleast one service");
             return;
         }
         var tr = $("#btnRemove" + counterId).closest("tr");
         tr.remove();
         CommonCalculations();
         $("div[name='amt']").html('0.00');
         $("div[name='vat']").html('0.00');
         $("div[name='sur']").html('0.00');




     }

     var global_counterId = "";
     function GetLast3rec(itemcode) {

         $.ajax({
             type: "POST",
             data: '{ "itemcode": "' + itemcode + '"}',
			 url: "PurchaseReturn.aspx/GetLast3rec",
             contentType: "application/json",
             asyn: false,
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 if (obj.Options != "") {
                     $("#tbllastrec").empty();
                     $("#tbllastrec").append(obj.Options);

                    // $("#dvlast3Dialog").show();
                 }

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {
                 $("#closetast3").focus();
                 


             }




         });

     }

     // register jQuery extension
     jQuery.extend(jQuery.expr[':'], {
         focusable: function (el, index, selector) {
             return $(el).is('a, button, :input, [tabindex]');
         }
     });
     var counter_cant = 0;
     $(document).on('keypress', 'input,select', function (e) {

         var focus_on;
         var counter;
         $(":focus").each(function () {
             focus_on = this.name;
             try {
                 counter = this.attributes.counter.value;
             } catch (e) {
                 counter = "";
             }
          
         });
   
         if (focus_on != "txtServiceId") {
             if (e.which == 13) {
         
                 if (focus_on == "txtTaxPer" || focus_on==undefined) {
                     if ($("#tbProducts tr").length ==counter) {
                     
                         addTR();

                      
                     }
                     if (focus_on == undefined) {
                       
                             addTR();
               
                      
                     }
                  
                     $("#txtServiceId" + counter + 1 + "").focus();
                     e.preventDefault();
                     // Get all focusable elements on the page
                     var $canfocus = $(':focusable');
                     var index = $canfocus.index(document.activeElement) + 1;
                     if (index >= $canfocus.length) index = 0;
                     $canfocus.eq(index).focus();
                     $canfocus.eq(index).select();
                 }
                 else {
                     e.preventDefault();
                     // Get all focusable elements on the page
                     var $canfocus = $(':focusable');
                     var index = $canfocus.index(document.activeElement) + 1;
                     if (index >= $canfocus.length) index = 0;
                     $canfocus.eq(index).focus();
                     $canfocus.eq(index).select();
                 }
             }
       }

       
     });


     $("#closetast3").click(function () {

         $("#dvlast3Dialog").hide();
         $("#txtQty" + global_counterId + "").focus();


     });


     function CodeSearch(counterId, e) {


         if (e.keyCode == 13) {
             global_counterId = counterId;

             var serviceId = $("#txtServiceId" + counterId).val().toUpperCase();
             var cntr = 0;
             $("select[name='ddlProducts']").each(
                 function () {

                     if (serviceId == $(this).val()) {
                         cntr++;

                     }

                 });


             //if (cntr == 1) {

             //    alert("Product Already Added");

             //    return;
             //}
             //  var ddlServiceVal = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').val();
             var ddlServiceVal = "";
             var cost = "0";
             var amount = "0";
             var HSN = "";

             $('#ddlProducts' + counterId + ' option').each(
                 function () {

                     if ($(this).val() == serviceId) {
                         ddlServiceVal = $(this).val();
                         cost = $(this).attr('cost');
                         amount = $(this).attr('cost');

                     }
                 });

             if (ddlServiceVal == "") {



                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $("#txtAmount" + counterId).val("0.0");
                 $("#txtRate" + counterId).val("");
                 $("#txtQty" + counterId).val("");
                 $("#txtServiceId" + counterId).val("");

                 $("#txtMRP" + counterId).val("");
                 $("#txtSRate" + counterId).val("");
                 $("#txtTaxPer" + counterId).val("");
                 $("#txtFree" + counterId).val("");
                 $("#txtDis1Per" + counterId).val("");
                 $("#txtDis2Per" + counterId).val("");
                // alert("Product Not Found!!");

             }
             else {
                 GetLast3rec(ddlServiceVal);
                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $('#ddlProducts' + counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');

                 cost = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");

                 amount = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");

                 tax = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("tax");

                 mrp = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrp");

                 srate = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("srate");
                 HSN = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("hsn");
                
				 //var finlpurrate = 0;

				 //if ($('#rbInclusive').is(":checked")) {
					// finlpurrate = Math.round((Number(cost) - ((Number(cost) * Number(tax)) / (100 + Number(tax)))), 2);
					// amount = finlpurrate
				 //}
				 //else {
					// finlpurrate = cost;
					// finlpurrate = cost;

				 //}
                 $("#txtAmount" + counterId).val(amount);
                 $("#txtRate" + counterId).val(cost);
				 if ($("#txtQty" + counterId).val() > 1) {
				 }
				 else {
					 $("#txtQty" + counterId).val("1");
				 }
                 $("#txtMRP" + counterId).val(mrp);
                 $("#txtSRate" + counterId).val(srate);
                 $("#txtTaxPer" + counterId).val(tax);
                 $("#txthsn" + counterId).val(HSN);
                 $("#txtFree" + counterId).val("0");
                 $("#txtDis1Per" + counterId).val("0.0");
                 $("#txtDis2Per" + counterId).val("0.0");
                 $("#txtQty" + counterId).select();
                 $("#txtServiceId" + counterId).val(ddlServiceVal);
                 CommonCalculations();
                 TaxDenomination(counterId);
             }

         }


     }


     function ServiceChange(counterId) {


         var serviceId = $("#ddlProducts" + counterId).val();
         global_counterId = counterId;
         var cntr = 0;
         $("select[name='ddlProducts']").each(
                 function () {


                     if (serviceId == $(this).val()) {
                         cntr++;
                     }

                 });


         //if (cntr > 1) {
         //    $("#ddlProducts" + counterId + " option").removeAttr("selected");
         //    $("#txtAmount" + counterId).val("0.0");
         //    $("#txtRate" + counterId).val("");
         //    $("#txtQty" + counterId).val("");
         //    $("#txtServiceId" + counterId).val("");

         //    $("#txtMRP" + counterId).val("");
         //    $("#txtSRate" + counterId).val("");
         //    $("#txtTaxPer" + counterId).val("");
         //    $("#txtFree" + counterId).val("");
         //    $("#txtDis1Per" + counterId).val("");
         //    $("#txtDis2Per" + counterId).val("");


         //    alert("Product Already Added");
         //    return;
         //}
         var ddlServiceVal = "";
         var cost = "0";
         var amount = "0";
         var HSN = "";


         ddlServiceVal = serviceId;

         cost = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");

         amount = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");

         tax = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("tax");

         mrp = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("mrp");

         srate = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("srate");
         HSN = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("hsn");
         GetLast3rec(serviceId);
   //      var finlpurrate = 0;
		 //if ($('#rbInclusive').is(":checked")) {
   //          finlpurrate = Math.round((Number(cost) - ((Number(cost) * Number(tax)) / (100 + Number(tax)))), 2);
   //          amount = finlpurrate;
   //      }
		 //else {
   //          finlpurrate = cost;
   //          amount = cost;
		 //}
         $("#txtQty" + counterId).focus();
         $("#txtAmount" + counterId).val(amount);
         $("#txtRate" + counterId).val(cost);
         if ($("#txtQty" + counterId).val() > 1) {
         }
         else {
             $("#txtQty" + counterId).val("1");
         }
         $("#txtMRP" + counterId).val(mrp);
         $("#txtSRate" + counterId).val(srate);
         $("#txtTaxPer" + counterId).val(tax);
         $("#txthsn" + counterId).val(HSN);
         $("#txtFree" + counterId).val("0");
         $("#txtDis1Per" + counterId).val("0.0");
         $("#txtDis2Per" + counterId).val("0.0");

         $("#txtServiceId" + counterId).val(ddlServiceVal);
         $("select[name='ddlProducts']").each(function () {
             if ($(this).val() != "0" && $(this).val() != "0.0" && $(this).val() != "") {

                    if ($(this).val()!="") {
                        CommonCalculations();
                        TaxDenomination(counterId);
                    }
                 }
                });

         




     }

     function addTRUpdate() {
         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

         $("#tbProducts").append("<tr id='loading'><td colspan='100%' style='text-align:center'><img src='images/ajax-loader.gif' alt='loading please wait...'/></td></tr>");

         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

         $.ajax({
             type: "POST",
             data: '{ "pid": "' + $("#hdnGrnNo").val() + '"}',
			 url: "PurchaseReturn.aspx/BindPurchaseDetail",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 var tr = "";
      
                 $("#tbProducts").append(obj.ServiceData);
                 $("#hdnCounter").val(obj.Counter);
               
                 for (var i = 0; i < 20 - Number(obj.Counter) ; i++) {
                     addTR();
                 }
             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {


                 $('#tbProducts tr#loading').remove();
                 $.uiUnlock();
             }




         });





     }


     $(document).keydown(function (e) {
         if (e.keyCode == 27) return false;
     });
				$("#tbProducts").keydown(function (e) {

					var cellindex = $(this).parents('td').index();

					if (e.which == 40) {
						$(e.target).closest('tr').nextAll('tr').find('td').eq(cellindex).find(':text').focus();
					}
                    if (e.which == 38) {
                        
						$(e.target).closest('tr').prevAll('tr').first().find('td').eq(cellindex).find(':text').focus();
					}

				});		
				//$(document).keydown(function (e) {
    //                if (e.keyCode == 38) {
    //                    alert("Hello");
    //                   // $("#txtServiceId" + counterId-1).focus();
    //                }
				//});
     function addTR() {
         var counterId = Number($("#hdnCounter").val()) + 1;
         $("#hdnCounter").val(counterId);
         var tr = "";
         tr = "<tr><td><input type='text' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' /></td>" +
              "<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +
              "<td><input type='text' id='txthsn" + counterId + "' counter='" + counterId + "'  class='form-control input-small' name='txthsn'  value = '0'/></td>" +
              "<td><input type='text' id='txtMargin" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtMargin'  value = '0.0'/></td>" +
              "<td><input type='text' id='txtBalQty" + counterId + "' readonly=readonly  counter='" + counterId + "'  class='form-control input-small' name='txtBalQty'  value ='0.0'/></td>" +
             "<td><input type='text' id='txtQty" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small validate' name='txtQty'  value = '0'/></td>" +
             "<td><input type='text' id='txtFree" + counterId + "' onkeyup='javascript:FreeChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small  validate' name='txtFree'  value = '0'/></td>" +
			 "<td><input type='text' id='txtRate" + counterId + "'  onchange='javascript:RateChange(" + counterId + ");' counter='" + counterId + "'  class='form-control input-small  validate' name='txtRate'   value = '0.0'/></td>" +

             "<td><input type='text' id='txtSRate" + counterId + "'    onkeyup='javascript:FreeChange(1);' counter='" + counterId + "'  class='form-control input-small  validate float' name='txtSRate'  value = '0.0'/></td>" +
             "<td><input type='text' id='txtMRP" + counterId + "'   onkeyup='javascript:FreeChange(1);' counter='" + counterId + "' class='form-control input-small  validate float' name='txtMRP1'  value = '0.0'/></td>" +

            "<td><input type='text' id='txtAmount" + counterId + "' onkeyup='javascript:AmtChange(" + counterId + ");'  counter='" + counterId + "'  class='form-control input-small' name='txtAmount'  value='0.0' /></td>" +
            "<td><input type='text' id='txtDis1Per" + counterId + "' onkeyup='javascript:Discount1TextChange(" + counterId + ");'   counter='" + counterId + "'    class='form-control input-small  validate' name='txtDis1Per'  value='0.0' /></td>" +
            "<td><input type='text' id='txtDis2Per" + counterId + "'  onkeyup='javascript:Discount2TextChange(" + counterId + ");'   counter='" + counterId + "'   class='form-control input-small  validate' name='txtDis2Per'  value='0.0' /></td>" +

            "<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "'  readonly=readonly class='form-control input-small  validate float' name='txtTaxPer'  value = '0.0'/></td>";



         tr = tr + "<td style='display:none'><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>";

         $("#tbProducts").append(tr);
        
         Settings();
         BasicSettings();



     }

     function SaveRecords() {



         if (!validateForm("addDialog")) {
             return;
         }





         var grnNo = $("#hdnGrnNo").val();
         var grnDate = $("#txtGrnDate").val();
         var grNo = $("#txtGrNo").val();

         var grDate = $("#txtGrDate").val();
         var billNo = $("#txtBillNo").val();

         var billDate = $("#txtBillDate").val();
         var supplierId = $("#ddlSupplier").val();
         if (supplierId == "0") {
             alert("Choose Supplier");
             $("#ddlSupplier").focus();
             return;

         }
         var vehNo = $("#txtVehNo").val();
         var displayAmount = $("#txtDisplayAmount").val();
         var adjustment = $("#txtAdjustments").val();
         if (adjustment == "") {
             adjustment = "0";
         }
         var NAmount = $("#txtNAmount").val();
         var oDisPer = $("#txtODisPer").val();
         if (oDisPer == "") {
             oDisPer = "0";
         }
         var oDisAmt = $("#txtODisAmt").val();

         var dis3per = $("#txtDis3Per").val();
         if (dis3per == "") {
             dis3per = "0";
         }
         var dis3amt = $("#txtDis3Amt").val();
         var dis1amt = $("#txtDiscount1").val();
         var dis2amt = $("#txtDiscount2").val();
         var billvalue = $("#txtBillValue").val();
         var taxp = $("#txttax").val();
         if (taxp == "") {
             taxp = "0";
         }
         var taxamt = $("#txttaxamt").val();



         var dis1InRs = false;

         if ($('#chkDis1InRs').is(":checked")) {

             dis1InRs = true;
         }


         var dis2InRs = false;

         if ($('#chkDis2InRs').is(":checked")) {

             dis2InRs = true;
         }

         var dis2After1 = false;
         if ($('#chkDis2After1').is(":checked")) {

             dis2After1 = true;
         }

         var isLocal = "Out";
         if ($('#rbLocal').is(":checked")) {

             isLocal = "Local";
         }
         var PurType = "CASh";
         if ($('#rdbcredit').is(":checked")) {

             PurType = "CREDIT";
         }

         var gstvat = "GST";
		 if ($('#rbVAT').is(":checked")) {

			 gstvat = "VAT";
		 }


		 var PurchaseType = "Exclusive";
		 if ($('#rbInclusive').is(":checked")) {

             PurchaseType = "Inclusive";
		 }
         var dis3afterdis1dis2 = false;
         if ($('#chkdis3').is(":checked")) {
             dis3afterdis1dis2 = true;
         }

         var taxBeforeDis1 = false;
         var taxBeforeDis2 = false;




         if ($('#txtTaxBeforeDis1').is(":checked")) {

             taxBeforeDis1 = true;

         }

         if ($('#txtTaxBeforeDis2').is(":checked")) {
             taxBeforeDis2 = true;
         }
         var SurchrgAmt = $("#txtSurchargeAmt").val();
         var godownId = $("#ddlGodown").val();
         if (godownId == "0") {
             alert("Choose Godown");
             $("#ddlGodown").focus();
             return;

         }
         var netCost = $("#txtNetCost").val();
         var totalAmount = $("#lblTotalAmount").html();
         var len = $("input[name='txtServiceId']").length;

         Code = [];
         Qty = [];
         Free = [];
         Amount = [];
         Rate = [];
         Id = [];
         Dis1 = [];
         Dis2 = [];
		// Dis1Amt = [];
		// Dis2Amt = [];
         Tax = [];
         MRP = [];
         SRate = [];
         Taxxamt = [];
         TaxDen = [];
         VatAmtDen = [];
         VatDen = [];
         SurDen = [];
         Excise = [];
         Margin = [];
         ExciseAmtA = [];
         Hsn = [];
         $("input[name='txtServiceId']").each(
            function (x) {

                var counterId = $(this).attr("counter");
                if ($('#ddlProducts' + counterId).val()!="") {

             
                Code[x] = $('#ddlProducts' + counterId).val();
                Qty[x] = $("#txtQty" + counterId).val();
                Amount[x] = $("#txtAmount" + counterId).val();
                Free[x] = $("#txtFree" + counterId).val();
                Dis1[x] = $("#txtDis1Per" + counterId).val();
                Dis2[x] = $("#txtDis2Per" + counterId).val();
                Tax[x] = $("#txtTaxPer" + counterId).val();
                MRP[x] = $("#txtMRP" + counterId).val();
                Rate[x] = $("#txtRate" + counterId).val();
                    SRate[x] = $("#txtSRate" + counterId).val();
     //               Dis1Amt[x] = ((Number(Amount[x]) * Number(Dis1[x])) / 100);
					//Dis2Amt[x] = ((Number(Amount[x]) * Number(Dis2[x])) / 100);
					//if (taxBeforeDis1 == false || taxBeforeDis2 == false) {
					//	Taxxamt[x] = ((Number(Number(Amount[x]) - Number(Dis1[x]) - Number(Dis2[x])) * Number(Tax[x])) / 100);

     //               }
     //               else {
                        Taxxamt[x] = ((Number(Amount[x]) * Number(Tax[x])) / 100);
                   // }
                Id[x] = $('#ddlProducts' + counterId).find(":selected").attr("pid");
                //Excise[x] = $("#txtExcisePer" + counterId).val();
                Excise[x] = 0;
                Margin[x] = $("#txtMargin" + counterId).val();
                    // ExciseAmtA[x] = $("#txtExciseAmt" + counterId).val();
                ExciseAmtA[x] = 0;
                Hsn[x] = $("#txthsn" + counterId).val();
                 }
				 
            });


         $("div[name='tax']").each(
            function (y) {
                TaxDen[y] = $(this).html();
            }
            );
         $("div[name='amt']").each(
            function (z) {
                VatAmtDen[z] = $(this).html();
            }
            );
         $("div[name='vat']").each(
            function (a) {
                VatDen[a] = $(this).html();
            }
            );
         $("div[name='sur']").each(
            function (a) {
                SurDen[a] = $(this).html();
            }
            );

         var ExciseAmt = $("#txtExcise").val();

         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
         if ($("#status").val() == "I") {


             $.ajax({
                 type: "POST",
                 data: '{"GrnDate": "' + grnDate + '","BillNo": "' + billNo + '","BillDate": "' + billDate + '","GrNo": "' + grNo + '","GrDate": "' + grDate + '","VehNo": "' + vehNo + '","SupplierId": "' + supplierId + '","Dis1InRs": "' + dis1InRs + '","Dis2InRs": "' + dis2InRs + '","Dis2After1": "' + dis2After1 + '","IsLocal": "' + isLocal + '","TaxBeforeDis1": "' + taxBeforeDis1 + '","TaxBeforeDis2": "' + taxBeforeDis2 + '", "GodownId": "' + godownId + '","TotalAmount": "' + netCost + '","NetAmount": "' + NAmount + '","Adjustments": "' + adjustment + '","DisplayAmount": "' + displayAmount + '","ODisAmt": "' + oDisAmt + '","ODisPer": "' + oDisPer + '","dis3per":"' + dis3per + '","dis3amt": "' + dis3amt + '","dis3afterdis1dis2":"' + dis3afterdis1dis2 + '","dis1amt":"' + dis1amt + '","dis2amt":"' + dis2amt + '","billvalue":"' + billvalue + '","taxp":"' + taxp + '","taxamt":"' + taxamt + '","codeArr": "' + Code + '","qtyArr": "' + Qty + '","amountArr": "' + Amount + '","freeArr": "' + Free + '","dis1Arr": "' + Dis1 + '","dis2Arr": "' + Dis2 + '","taxArr": "' + Tax + '","pidArr": "' + Id + '","mrpArr": "' + MRP + '","rateArr": "' + Rate + '","srateArr": "' + SRate + '","taxaamtarr":"' + Taxxamt + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","SurchrgAmt":"' + SurchrgAmt + '","arrExcise":"' + Excise + '","arrMargin":"' + Margin + '","PurType":"' + PurType + '","ExciseAmt":"' + ExciseAmt + '","ExciseAmtArr":"' + ExciseAmtA + '","HsnArr":"' + Hsn + '","gstvat":"' + gstvat + '","PurchaseType":"' + PurchaseType + '"}',

				 url: "PurchaseReturn.aspx/Insert",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -11) {
                         alert("You don't have permission to perform this action.");
                         return;
                     }
                     if (obj.Status == "0") {
                         alert("Insertion Failed. Please try again later.");
                         return;
                     }
                     jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.Purchase, "last");

                     alert("Purchase added Successfully");
                     $("#addDialog").dialog("close");

                     $("#innerSave").css("display", "none");
                     $("#dvTaxDenomination").css("display", "none");


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
					 window.location = "PurchaseReturn.aspx";
                 }


             });


         }
         else {




             $.ajax({
                 type: "POST",
                 data: '{"GrnNo":"' + grnNo + '","GrnDate": "' + grnDate + '","BillNo": "' + billNo + '","BillDate": "' + billDate + '","GrNo": "' + grNo + '","GrDate": "' + grDate + '","VehNo": "' + vehNo + '","SupplierId": "' + supplierId + '","Dis1InRs": "' + dis1InRs + '","Dis2InRs": "' + dis2InRs + '","Dis2After1": "' + dis2After1 + '","IsLocal": "' + isLocal + '","TaxBeforeDis1": "' + taxBeforeDis1 + '","TaxBeforeDis2": "' + taxBeforeDis2 + '", "GodownId": "' + godownId + '","TotalAmount": "' + totalAmount + '","NetAmount": "' + netCost + '","Adjustments": "' + adjustment + '","DisplayAmount": "' + displayAmount + '","ODisAmt": "' + oDisAmt + '","ODisPer": "' + oDisPer + '","dis3per":"' + dis3per + '","dis3amt": "' + dis3amt + '","dis3afterdis1dis2":"' + dis3afterdis1dis2 + '","dis1amt":"' + dis1amt + '","dis2amt":"' + dis2amt + '","billvalue":"' + billvalue + '","taxp":"' + taxp + '","taxamt":"' + taxamt + '","codeArr": "' + Code + '","qtyArr": "' + Qty + '","amountArr": "' + Amount + '","freeArr": "' + Free + '","dis1Arr": "' + Dis1 + '","dis2Arr": "' + Dis2 + '","taxArr": "' + Tax + '","pidArr": "' + Id + '","mrpArr": "' + MRP + '","rateArr": "' + Rate + '","srateArr": "' + SRate + '","taxaamtarr":"' + Taxxamt + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","SurchrgAmt":"' + SurchrgAmt + '","arrExcise":"' + Excise + '","arrMargin":"' + Margin + '","PurType":"' + PurType + '","ExciseAmt":"' + ExciseAmt + '","ExciseAmtArr":"' + ExciseAmtA + '","HsnArr":"' + Hsn + '"}',

				 url: "PurchaseReturn.aspx/Update",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -11) {
                         alert("You don't have permission to perform this action.");
                         return;
                     }
                     if (obj.Status == "0") {
                         alert("Updation Failed. Please try again later.");
                         return;
                     }
                     var myGrid = $("#jQGridDemo");
                     var selRowId = myGrid.jqGrid('getGridParam', 'selrow');
                     myGrid.jqGrid('setRowData', selRowId, obj.Purchase);

                     alert("Purchase Updated Successfully");
                     $("#addDialog").dialog("close");
                     $("#innerSave").css("display", "none");
                     $("#dvTaxDenomination").css("display", "none");

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
                 }


             });




         }



     }



     $(document).ready(
    function () {



        $('#txtDateFrom').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#txtDateTo').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $("#txtGrnDate").val($("#<%=hdntodaydate.ClientID %>").val());

                            $("#txtBillDate").val($("#<%=hdntodaydate.ClientID %>").val());

        $("#txtGrDate").val($("#<%=hdntodaydate.ClientID %>").val());
        $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());
        $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());



        BindGrid();

        $("#btnGo").click(
                   function () {

                       BindGrid();

                   }
                    );




        $("#btnSubmit").click(
        function () {
            //var flag = 0;
            //$("input[name='txtTaxPer']").each(function () {


            //    if ($(this).val() == "") {
            //        alert("Please select Products from Grid");

            //        flag = 1;

            //    }


            //});

            //if (flag == 1) {
            //    flag = 0;
            //    return false;

            //}
            if ($("#txtNetCost").val() <= 0) {
                alert("Please select Products from Grid");

                return;
            }

            $("#innerSave").toggle();
            $("#dvTaxDenomination").toggle();
            //$("input[name='txtTaxPer']").each(
            //                     function () {


            //                         if ($(this).val() == "") {
            //                             alert("Please select Products from Grid");

            //                             return false;
            //                         }

            //                     });
            $("input[name='txtTaxPer']").each(
            function () {

                var counterId1 = $(this).attr('counter');
                if ($("#ddlProducts" + counterId1).val() != "") {


                    TaxDenomination(counterId1);
                }
            });


        });


        $("#btnprint").click(
         function () {

             var myGrid = $('#jQGridDemo'),
             selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
             celValue = myGrid.jqGrid('getCell', selRowId, 'GrnNo');

             Printt(celValue);
         }
         );

			
        $("#chkDis1InRs").change(
          function () {

              if ($("#txtNetCost").val() <= 0) {


                  return;
              }

              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
          }
             );
             $("#rbVAT").change(
                 function () {
                     if ($('#rbVAT').is(":checked")) {
                         $("#lblgsttxt").text("Add VAT");
                         $("#lblgsttr").text("VAT");
						 $("#lblgstper").text("VAT %");
                         $("#lblgstdeno").text("VAT Denomination");
                         $("#lblgstAmt").text("VATAmt");
						 $("#lblgst").text("VAT");
						 
						 
                     }
                    
					 
                 });

			 $("#rbGST").change(
				 function () {
					 if ($('#rbGST').is(":checked")) {
                         $("#lblgsttxt").text("Add GST/IGST");
                         $("#lblgsttr").text("GST");
                         $("#lblgstper").text("GST %");
                         $("#lblgstdeno").text("GST Denomination");
                         $("#lblgstAmt").text("GSTAmt");
						 $("#lblgst").text("GST");
						 
					 }
					 

				 });
        $("#chkDis2InRs").change(
          function () {

              if ($("#txtNetCost").val() <= 0) {


                  return;
              }

              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
          }
          );
        $("#chkDis2After1").change(
          function () {

              if ($("#txtNetCost").val() <= 0) {


                  return;
              }



              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
          }
          );
        $("#txtTaxBeforeDis1").change(
          function () {
              if ($("#txtNetCost").val() <= 0) {


                  return;
              }



              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {
                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });


          }
          );
        $("#txtTaxBeforeDis2").change(
          function () {


              if ($("#txtNetCost").val() <= 0) {


                  return;
              }

              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
          }
          );



        $("#rbLocal").change(
          function () {
              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
              $("#trGST").show();
              $("#trIGST").hide();
             // $("#trSGST").hide();
          });
        $("#rbOutStation").change(
          function () {
              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                    TaxDenomination(counterId);
                }
            });
              $("#trGST").hide();
              $("#trIGST").show();
              //$("#trSGST").show();
            });

			

        $("#txttax").keyup(
          function () {

              CommonCalculations();
          }
             );
			
        $("#txtODisPer").keyup(
          function () {
              CommonCalculations();
          }
             );
             $("#txtDisplayAmount").change(
                 function () {
					 var BillVal = $("#txtTotalAmount").val();
                     var DIspAmt = $("#txtDisplayAmount").val();
					 $("#txtNAmount").val(Number(BillVal) - Number(DIspAmt));
					 
				 }
	     );
        $("#txtODisAmt").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#txtDis3Per").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#txtDis3Amt").keyup(
          function () {
              CommonCalculations();
          }
        );
        $("#chkdis3").change(
          function () {
              CommonCalculations();
          }
        );
        //        $("#txtAdjustments").keyup(
        //          function () {
        //              CommonCalculations();
        //          }
        //        );




        $(document).on('click', '#dvImport', function (e) {


            $('#importDialog').dialog(
        {
            autoOpen: false,

            width: 265,
            resizable: false,
            modal: true,
            buttons: {


                "Import": function () {

                    $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();


                    $("#tbImport").append("<tr id='loading'><td colspan='100%' style='text-align:center'><img src='images/ajax-loader.gif' alt='loading please wait...'/></td></tr>");




                    var oid = $("#txtOrderNumber").val();

                    $.ajax({
                        type: "POST",
                        data: '{"orderId":"' + oid + '"}',
						url: "PurchaseReturn.aspx/GetPurchaseOrderImport",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);

                            $("#tbProducts").append(obj.ServiceData);
                            $("#hdnCounter").val(obj.Counter);



                            $("#ddlSupplier").val(obj.PurchaseData.SupplierId);
                            $("#GodownId").val(obj.PurchaseData.GodownId);


                            if ((obj.PurchaseData.Dis1InRs) == true) {
                                $('#chkDis1InRs').prop('checked', true);

                            }
                            else {

                                $('#chkDis1InRs').prop('checked', false);

                            }





                            if ((obj.PurchaseData.Dis2InRs) == true) {



                                $('#chkDis2InRs').prop('checked', true);

                            }
                            else {

                                $('#chkDis2InRs').prop('checked', false);

                            }


                            if ((obj.PurchaseData.Dis2AftDedDis1) == true) {
                                $('#chkDis2After1').prop('checked', true);

                            }
                            else {

                                $('#chkDis2After1').prop('checked', false);

                            }

                            if ((obj.PurchaseData.TaxAfterDis1) == true) {
                                $('#txtTaxBeforeDis1').prop('checked', true);

                            }
                            else {

                                $('#txtTaxBeforeDis1').prop('checked', false);

                            }

                            if ((obj.PurchaseData.TaxAfterDis2) == true) {
                                $('#txtTaxBeforeDis2').prop('checked', true);

                            }
                            else {

                                $('#txtTaxBeforeDis2').prop('checked', false);

                            }

                            if ((obj.PurchaseData.IsLocal) == "Local") {

                                $('#rbLocal').prop('checked', true);
                                $('#rbOutStation').prop('checked', false);
                      
                                $("#trGST").show();
                                //$("#trSGST").hide();
                               $("#trIGST").hide();
                            }
                            else {

                                $('#rbLocal').prop('checked', false);
                                $('#rbOutStation').prop('checked', true);
                                $("#trGST").hide();
                                //$("#trSGST").show();
                               $("#trIGST").show();

                            }




                        },
                        complete: function () {
                            $('#tbImport tr#loading').remove();
                            $('#importDialog').dialog("close");
                            CommonCalculations();

                            var Id = $("#hdnCounter").val();

                            for (var i = 1; i <= Number(Id) ; i++) {
                                TaxDenomination(i);
                            }



                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        }



                    });

                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#importDialog');
            dialogDiv.dialog("option", "position", [400, 200]);
            dialogDiv.dialog('open');
            return false;

        }

        );



        $("#btnSave").click(
        function () {


            SaveRecords(true);
            $("#innerSave").toggle();
            $("#dvTaxDenomination").toggle();
      
        }

        );

        $("#btnCloseMe").click(
        function () {

            $("#innerSave").toggle();
            $("#dvTaxDenomination").toggle();

        });

        //        $("#txtTaxBeforeDis1").change(function () {


        //            if ($(this).prop('checked') == true) {

        //                $("#txtTaxBeforeDis2").prop('checked', false);
        //            }


        //        });

        //        $("#txtTaxBeforeDis2").change(function () {

        //            if ($(this).prop('checked') == true) {
        //                $("#txtTaxBeforeDis1").prop('checked', false);
        //            }


        //        });


        $("#chkDis2After1").change(function () {

            CommonCalculations();
            $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });


        });


        //        $("#txtTaxBeforeDis1").change(function () {

        //            CommonCalculations();
        //            $("input[name='txtServiceId']").each(
        //            function () {

        //                var counterId = $(this).attr("counter");
        //                TaxDenomination(counterId);
        //            });


        //        });

        //        $("#txtTaxBeforeDis2").change(function () {

        //            CommonCalculations();
        //            $("input[name='txtServiceId']").each(
        //            function () {

        //                var counterId = $(this).attr("counter");
        //                TaxDenomination(counterId);
        //            });


        //        });


        $("#txtDisPercent").keyup(function () {
            CommonCalculations();
            $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });

        });

        $("#chkDis1InRs").change(
        function () {

            if ($('#chkDis1InRs').is(":checked")) {

                $("#thDis1").html("Dis1र");


                CommonCalculations();
                $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });
            }
            else {
                $("#thDis1").html("Dis1%");

                CommonCalculations();
                $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });
            }
        }
        );

        $("#chkDis2InRs").change(
                function () {

                    if ($('#chkDis2InRs').is(":checked")) {
                        $("#thDis2").html("Dis2र");
                        CommonCalculations();
                        $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });
                    }
                    else {
                        $("#thDis2").html("Dis2%");
                        CommonCalculations();
                        $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                TaxDenomination(counterId);
            });
                    }
                }
                );




        $("#txtDisAmount").keyup(function () {

            $("#txtDisPercent").val($("#txtDisAmount").val() * 100 / $("#lblTotalAmount").html());
            CommonCalculations();

        });


        $("#txtServiceId").change(function () {

            var sid = $("#txtServiceId").val();


            $.ajax({
                type: "POST",
                data: '{ "Id": "' + sid + '"}',

				url: "PurchaseReturn.aspx/ValidateServiceID",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == "n") {

                        $("#lblMsg").css({ "color": "red" });
                        $("#lblMsg").html("Sorry Product Id already Exists");
                        $("#hdnStatus").val("0");


                    }
                    else {
                        $("#lblMsg").css({ "color": "green" });

                        $("#lblMsg").html("Product Id Accepted!!");
                        $("#hdnStatus").val("1");

                    }
                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });



        }
        );



        function BindContols() {



        }









        //----------------------btnADD------------------------

        $("#ddlstate").change(
        function () {

            BindCities($("#ddlstate").val());

        }

        );


        function BindCities(sid) {
            $("#ddlcities").html('');
            var stateId = sid;
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            $.ajax({
                type: "POST",
                data: '{ "stateId": "' + stateId + '"}',
                url: "ManageSupplier.aspx/BindCities",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#ddlcities").html(obj.CitiesOptions);

                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {

                    $.uiUnlock();

                    $('#ddlcities option[value=' + $('#jQGridDemo').jqGrid('getCell', CurrentRowId, 'City') + ']').prop('selected', 'selected');

                }


            });
        }





        function InsertUpdateSupplier() {


            var suppliername = $("#txtSupplier").val();
            if (suppliername.trim() == "") {
                alert("Please enter suppliername");
                return;
            }

            var Address = $("#txtAddress").val();
            if (Address.trim() == "") {
                alert("Please enter Address");
                return;
            }

            var Address1 = $("#txtAddress1").val();
            if (Address1.trim() == "") {
                alert("Please enter Address1");
                return;
            }
            var City = $("#ddlcities").val();
            if (City == null) {
                alert("Please enter City");
                return;
            }

            var State = $("#ddlstate").val();
            if (State == null) {
                alert("Please enter State");
                return;
            }

            var ContactNumber = $("#txtContactNumber").val();
            if (ContactNumber.trim() == "") {
                alert("Please enter ContactNumber");
                return;
            }
            var ContactPerson = $("#txtContactPerson").val();
            if (ContactPerson.trim() == "") {
                alert("Please enter ContactPerson");
                return;
            }

            var CSTNo = $("#txtCSTNo").val();
            if (CSTNo == null) {
                alert("Please enter CSTNo");
                return;
            }
            var TINNo = $("#txtTINNo").val();
            if (TINNo == null) {
                alert("Please enter TINNo");
                return;
            }

            var IsActive = false;
            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{"Supplier": "' + suppliername + '","Address": "' + Address + '","Address1": "' + Address1 + '","City": "' + City + '","State": "' + State + '","ContactNumber": "' + ContactNumber + '","Contactperson": "' + ContactPerson + '","CSTNo": "' + CSTNo + '","TINNo": "' + TINNo + '","isActive": "' + IsActive + '"}',
				url: "PurchaseReturn.aspx/InsertSupplier",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Insertion Failed.Supplier with duplicate name already exists.");
                        return;

                    }

                    var option = "<option value='" + obj.Supplier.SupplierId + "'>" + obj.Supplier.Supplier + "</option>";
                    $("#ddlSupplier").append(option);

                    $("#ddlstate").html('');
                    $("#ddlcities").html('');
                    $("#txtSupplier").val("");
                    $("#txtAddress").val("");
                    $("#txtAddress1").val("");
                    $("#txtContactPerson").val("");
                    $("#txtContactNumber").val("");
                    $("#txtCSTNo").val("");
                    $("#txtTINNo").val("");



                    alert("Supplier added successfully.");

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }



            });


        }




        $("#dvSupplier").click(
        function () {

            $('#radhika').dialog(
        {
            autoOpen: false,

            width: 600,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateSupplier();
                    $(this).dialog("close");


                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }



        });

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


            $.ajax({
                type: "POST",
                data: '{}',
                url: "ManageSupplier.aspx/BindStates",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlstate").html(obj.StateOptions);


                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }


            });












            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#radhika');
            dialogDiv.dialog("option", "position", [100, 200]);
            dialogDiv.dialog('open');
            return false;


        }
        );



        $("#dvGodown").click(
        function () {

            $('#AddGodown').dialog(
        {
            autoOpen: false,

            width: 300,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateGodown();
                    $(this).dialog("close");



                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });



            function InsertUpdateGodown() {


                var Title = $("#txtTitle").val();
                if (Title.trim() == "") {
                    alert("Please enter Title");
                    return;
                }

                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


                $.ajax({
                    type: "POST",
                    data: '{ "title": "' + Title + '"}',
					url: "PurchaseReturn.aspx/InsertGodown",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        if (obj.Status == 0) {

                            alert("Insertion Failed.Godown with duplicate name already exists.");
                            return;

                        }
                        jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.Godown, "last");

                        var option = "<option value='" + obj.Godown.GodownId + "'>" + obj.Godown.Title + "</option>";
                        $("#ddlGodown").append(option);

                        $("#txtTitle").val("");
                        alert("Godown added successfully.");

                    }
                     , error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                    complete: function () {


                        $.uiUnlock();
                    }


                });

            }


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#AddGodown');
            dialogDiv.dialog("option", "position", [400, 200]);
            dialogDiv.dialog('open');
            return false;


        }
        );





        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "1") {

                    $("#btnAdd").click(function () {
                        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                        Settings();
                        $("#txtGrnNo").focus();
                        $("#status").val("I");
                        $("#hdnGrnNo").val("0");

                        var IsFirstTime = $("#hdnFirstTime").val();

                        //if (IsFirstTime == "1") {
                            BindContols();
                            for (var i = 0; i < 15; i++) {
                                addTR();
                         
                             
                            }
                      
                            //BindProducts();
                   

                            $("#hdnFirstTime").val("0");
                        //}
                        //else {

                        //    ResetControls();
                        //    Settings();
                        //}


                        $('#addDialog').dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            height: 600,
                            width: 1115,
                            resizable: false,
                            modal: false,
                        });
                      
                        $("#ui-id-1").html('<div style="margin-top: -5px;margin-bottom: -10px;"><button type="button" id="dvImport" class="btn btn-danger">Import Purchase</button></div>');

                        //change the title of the dialgo
                        linkObj = $(this);
                        var dialogDiv = $('#addDialog');
                        dialogDiv.dialog("option", "position", [70, 50]);
                        dialogDiv.dialog('open');
                        return false;

                    });


                }
                else if (arrRole[i] == "3") {

                    $("#btnEdit").click(
       function () {

           $("#status").val("U");
           addTRUpdate();
           $("#hdnFirstTime").val("0");

           var pid = $("#hdnGrnNo").val();

           if (pid == "0") {
               alert("No Row Selected");
               return;
           }

           //$("#innerSave").show();

           $('#addDialog').dialog(
       {
           autoOpen: false,
           height: 600,
           width: 1115,
           resizable: false,
           modal: false,

       });


           //change the title of the dialgo
           linkObj = $(this);
           var dialogDiv = $('#addDialog');
           dialogDiv.dialog("option", "position", [70, 50]);
           var viewUrl = "customeradd.aspx";
           dialogDiv.dialog('open');
           return false;


       }
       );

                }


            }

        }




        $("#btncncl").click(
        function () {


            //$("#addDialog").dialog("close");
				window.location = "PurchaseReturn.aspx";
        });



    });

                function chkbillno() {

                    if ($("#hdnGrnNo").val() == "0") {


                        $.ajax({
                            type: "POST",
                            data: '{ "billno": "' +  $("#txtBillNo").val() + '","supplier": "' + $("#ddlSupplier").val() + '"}',
							url: "PurchaseReturn.aspx/chkbillno",
                            contentType: "application/json",
                            asyn: false,
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);

                                if (obj == 1) {
                                    alert("Bill No already Exists!");

                                    $("#txtBillNo").val("");
                                    $("#txtBillNo").focus();

                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {
                                $("#closetast3").focus();



                            }




                        });
                    }
                }

                $("#txtBillNo").blur(function () {

                    chkbillno();
 
                });

                $("#ddlSupplier").change(function () {


                    chkbillno();



                });
    function BindProducts() {



        PageMethods.BindProducts(OnSuccess, OnFailure);

        //        $.ajax({
        //            type: "POST",
        //            data: '{}',
        //            url: "managepurchase.aspx/BindProducts",
        //            contentType: "application/json",
        //            dataType: "json",
        //            success: function (msg) {

        //                var obj = jQuery.parseJSON(msg.d);

        //                $("#hdnProducts").html("");

        //                $("#hdnProducts").append("<option></option>");
        //                $("#hdnProducts").append(obj.ProductOptions);





        //            },
        //            complete: function () {

        //           ResetControls();
        //            },
        //            error: function (xhr, ajaxOptions, thrownError) {

        //                var obj = jQuery.parseJSON(xhr.responseText);
        //                alert(obj.Message);
        //            }


        //        });
    }


    function ResetControls() {

        validateForm("detach");
        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        $("#txtPackageName").val("");
        $("#txtShortName").val("");


        $("#txtValidFor").val("");
        $("#lblTotalAmount").html("");
        $("#hdnCounter").val(1);

        tr = "<tr><td><input type='text' autocomplete='off' onkeyup='javascript:CodeSearch(1,event);' id='txtServiceId1'  counter='1' class='form-control input-small' name='txtServiceId' /></td>" +
             "<td><select id='ddlProducts1' onchange='javascript:ServiceChange(1);' name='ddlProducts' >" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +
              "<td><input type='text' id='txtMargin1' readonly=readonly  counter='1'  class='form-control input-small' name='txtMargin'  value = '0.0'/></td>" +
              "<td><input type='text' id='txtBalQty1' readonly=readonly  counter='1'  class='form-control input-small' name='txtBalQty'  value ='0.0'/></td>" +
             "<td><input type='text' id='txtQty1' onkeyup='javascript:QtyChange(1);' counter='1'  class='form-control input-small validate' name='txtQty' /></td>" +
             "<td><input type='text' id='txtFree1' onkeyup='javascript:FreeChange(1);' counter='1'  class='form-control input-small validate' name='txtFree' /></td>" +
              "<td><input type='text' id='txtRate1'  onkeyup='javascript:RateChange(1);' counter='1'  class='form-control input-small validate' name='txtRate'  /></td>" +

             "<td><input type='text' id='txtSRate1'    onkeyup='javascript:FreeChange(1);' counter='1'  class='form-control input-small validate' name='txtSRate' /></td>" +
             "<td><input type='text' id='txtMRP1'   onkeyup='javascript:FreeChange(1);' counter='1' class='form-control input-small validate' name='txtMRP1' /></td>" +

            "<td><input type='text' id='txtAmount1' onkeyup='javascript:AmtChange(1);'  counter='1' class='form-control input-small validate' name='txtAmount' value='0.0'  /></td>" +
            "<td><input type='text' id='txtDis1Per1' onkeyup='javascript:Discount1TextChange(1);'   counter='1'    class='form-control input-small validate float' name='txtDis1Per'  value='0.0' /></td>" +
            "<td><input type='text' id='txtDis2Per1'  onkeyup='javascript:Discount2TextChange(1);'   counter='1'   class='form-control input-small validate float' name='txtDis2Per'  value='0.0' /></td>" +

            "<td><input type='text' id='txtTaxPer1'   counter='1'  readonly=readonly class='form-control input-small validate float' name='txtTaxPer'/></td>";



        tr = tr + "<td style='display:none'><div id='btnAddRow1'  onclick='javascript:addTR();' style='cursor:pointer'  counter='1'><i class='fa fa-plus'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove1' onclick='javascript:DeleteRow(1);'  style='cursor:pointer'  counter='1'><i class='fa fa-remove'></i></div> </td></tr>";




         $("#tbProducts").append(tr);




         $("#txtDisPercent").val("");
         $("#txtDisAmount").val("");
         $("#txtNetCost").val("0.00");
         $("#chkDiscountDeduct").prop("checked", false);
         $("#chkBridalPackage").prop("checked", false);
         $("#txtGrnDate").val($("#<%=hdntodaydate.ClientID %>").val());
         $("#txtBillNo").val("");
         $("#txtBillDate").val($("#<%=hdntodaydate.ClientID %>").val());
         $("#txtGrNo").val("");
         $("#txtGrDate").val($("#<%=hdntodaydate.ClientID %>").val());
         $("#txtVehNo").val("");
         $("#chkDis1InRs").prop("checked", false);
         $("#chkDis2InRs").prop("checked", false);
         $("#chkDis2After1").prop("checked", false);
         $("#txtTaxBeforeDis1").prop("checked", false);
         $("#txtTaxBeforeDis2").prop("checked", false);
         $("#txtBillValue").val("");
         $("#txtDiscount1").val("");
         $("#txtDiscount2").val("");
         $("#txtDis3Per").val("");
         $("#txtDis3Amt").val("");
         $("#txttax").val("");
         $("#txttaxamt").val("");
         $("#txtTotalAmount").val("");
         $("#txtODisPer").val("");
         $("#txtODisAmt").val("");
         $("#txtDisplayAmount").val("");
         $("#txtAdjustments").val("");
         $("#txtNAmount").val("");
         $("div[name='amt']").html('0.00');
         $("div[name='vat']").html('0.00');
         $("div[name='sur']").html('0.00');
         $("#ddlSupplier").val(0);
         $("#ddlGodown").val(0);
         BindGrid();



     }


     function OnSuccess(data) {
         if (data) {


             // $("#hdnProducts").html("");
             // $("#hdnProducts").append("<option></option>");
             // $("#hdnProducts").append(data);
             ResetControls();
         }
     }
     function OnFailure(error) {
         alert(error);
     }
            </script>
        </div>
        <!-- /page content -->

        </div>
        <!-- footer content -->
        <footer>                 
                   <uc1:ucfooter ID="ucfooter1" runat="server" />                
                </footer>
        <!-- /footer content -->

        <script type="text/javascript">
            function BindGrid() {
                var DateFrom = $("#txtDateFrom").val();
                var DateTo = $("#txtDateTo").val();
                jQuery("#jQGridDemo").GridUnload();

                jQuery("#jQGridDemo").jqGrid({
					url: 'handlers/ManagePurchaseReturn.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
                    ajaxGridOptions: { contentType: "application/json" },
                    datatype: "json",
                    colNames: ['GrnNo', 'GRNDate', 'BillNo', 'BillDate', 'GRNo', 'GRDate', 'VehNo', 'Dis1InRs', 'Dis2InRs', 'Dis2AftDedDis1',
                    'IsLocal', 'TaxAfterDis1', 'TaxAfterDis2', 'Remarks', 'BillValue', 'Dis1Amt', 'Dis2Amt',
                    'Dis3AftDis1PDis2', 'Dis3P', 'Dis3Amt', 'TaxP', 'TaxAmt', 'TotalAmount', 'ODisP', 'ODisAmt', 'DisplayAmount',
                     'Adjustment', 'NetAmount', 'SupplierId', 'Supplier', 'GodownId', 'ExciseAmt', 'SurchrgAmt', 'User'
                    ],
                    colModel: [

                            { name: 'GrnNo', key: true, index: 'GrnNo', width: 100, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: false },
        //   		            { name: 'strgrndate',key:true, index: 'strgrndate',formatter:"date",formatoptions:{newformat:"m/d/y"}, sorttype: 'date',  searchoptions : { sopt: ['eq']}, width: 100, stype: 'text',sortable: true,hidden:false },
                            { name: 'strgrndate', index: 'strgrndate', width: 100, stype: 'text', sortable: true, hidden: false },
                            { name: 'BillNo', key: true, index: 'BillNo', width: 100, searchoptions: { sopt: ['eq'] }, stype: 'text', sortable: true, hidden: false },
                              { name: 'strbilldate', index: 'strbilldate', width: 100, stype: 'text', sortable: true, hidden: false },
                            //{ name: 'BillDate',key:true, index: 'BillDate',formatter:"date",formatoptions:{newformat:"m/d/y"},sorttype: 'date',  searchoptions : { sopt: ['eq']}, width: 100, stype: 'text',sortable: true,hidden:false },
                             { name: 'GRNo', index: 'GRNo', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'strgrdate', index: 'strgrdate', width: 100, stype: 'text', sortable: true, hidden: true },
                            // { name: 'GRDate', index: 'GRDate', width: 100,formatter:"date",formatoptions:{newformat:"m/d/y"},sorttype: 'date',  searchoptions : { sopt: ['eq']}, stype: 'text', sortable: true,hidden:false },

                             { name: 'VehNo', index: 'VehNo', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis1InRs', index: 'Dis1InRs', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis2InRs', index: 'Dis2InRs', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis2AftDedDis1', index: 'Dis2AftDedDis1', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'IsLocal', index: 'IsLocal', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'TaxAfterDis1', index: 'TaxAfterDis1', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'TaxAfterDis2', index: 'TaxAfterDis2', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Remarks', index: 'Remarks', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'BillValue', index: 'BillValue', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis1Amt', index: 'Dis1Amt', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis2Amt', index: 'Dis2Amt', width: 100, stype: 'text', sortable: true, hidden: true },
                             { name: 'Dis3AftDis1PDis2', index: 'Dis3AftDis1PDis2', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'Dis3P', index: 'Dis3P', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'Dis3Amt', index: 'Dis3Amt', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'TaxP', index: 'TaxP', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'TaxAmt', index: 'TaxAmt', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'TotalAmount', index: 'TotalAmount', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'ODisP', index: 'ODisP', width: 100, stype: 'text', sortable: true, hidden: true },
                               { name: 'ODisAmt', index: 'ODisAmt', width: 100, stype: 'text', sortable: true, hidden: true },

                               { name: 'DisplayAmount', index: 'DisplayAmount', width: 100, stype: 'text', sortable: true, hidden: true },
                                   { name: 'Adjustment', index: 'Adjustment', width: 100, stype: 'text', sortable: true, hidden: true },
                                   { name: 'NetAmount', index: 'NetAmount', width: 100, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, hidden: false },
                                   { name: 'SupplierId', index: 'SupplierId', width: 100, stype: 'text', sortable: true, hidden: true },
                                    { name: 'SupplierName', index: 'SupplierName', width: 100, stype: 'text', sortable: true, hidden: false },
                                   { name: 'GodownId', index: 'GodownId', width: 100, stype: 'text', sortable: true, hidden: true },
                                   { name: 'ExciseAmt', index: 'ExciseAmt', width: 100, stype: 'text', sortable: true, hidden: true },
                                    { name: 'SurchrgAmt', index: 'SurchrgAmt', width: 100, stype: 'text', sortable: true, hidden: true },
                                     { name: 'UserName', index: 'UserName', width: 100, stype: 'text', sortable: true, hidden: false },
                    ],
                    rowNum: 10,
                    mtype: 'GET',
                    loadonce: true,
                    toppager: true,
                    rowList: [10, 20, 30],
                    pager: '#jQGridDemoPager',
                    sortname: 'PackageId',
                    viewrecords: true,
                    height: "100%",
                    width: "600px",
                    sortorder: 'desc',
                    caption: "Purchase List",
                    editurl: 'handlers/ManagePackages.ashx',

                    toolbar: [true, "top"],
                    ignoreCase: true,
                });
                var $grid = $("#jQGridDemo");
                // fill top toolbar
                $('#t_' + $.jgrid.jqID($grid[0].id))
                    .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                $("#globalSearchText").keypress(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    if (key === $.ui.keyCode.ENTER) { // 13
                        $("#globalSearch").click();
                    }
                });
                $("#globalSearch").button({
                    icons: { primary: "ui-icon-search" },
                    text: false
                }).click(function () {
                    var postData = $grid.jqGrid("getGridParam", "postData"),
                        colModel = $grid.jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#globalSearchText").val(),
                        l = colModel.length,
                        i,
                        cm;
                    for (i = 0; i < l; i++) {
                        cm = colModel[i];
                        if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                            rules.push({
                                field: cm.name,
                                op: "cn",
                                data: searchText
                            });
                        }
                    }
                    postData.filters = JSON.stringify({
                        groupOp: "OR",
                        rules: rules
                    });
                    $grid.jqGrid("setGridParam", { search: true });
                    $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                    return false;
                });



                $("#jQGridDemo").jqGrid('setGridParam',
               {
                   onSelectRow: function (rowid, iRow, iCol, e) {


                       $("#hdnGrnNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GrnNo'));

                       $("#txtGrnNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GrnNo'));
                       $("#txtGrnDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strgrndate'));

                       $("#txtBillNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BillNo'));
                       $("#txtBillDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strbilldate'));
                       $("#txtGrNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'GRNo'));
                       $("#txtGrDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strgrdate'));
                       $("#txtVehNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'VehNo'));
					   $("#lblTotalAmount").html($('#jQGridDemo').jqGrid('getCell', rowid, 'BillValue'));
                       $("#txtNetCost").val($('#jQGridDemo').jqGrid('getCell', rowid, 'NetAmount'));

                       $("#txtODisPer").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ODisP'));
                       $("#txtODisAmt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ODisAmt'));
                       $("#txtDisplayAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'DisplayAmount'));
                       $("#txtAdjustments").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Adjustment'));
                       $("#txtNAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'NetAmount'));
                       $("#txtDis3Per").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis3P'));
                       $("#txtDis3Amt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis3Amt'));
                       $("#txtDiscount2").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2Amt'));
                       $("#txtDiscount1").val($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis1Amt'));
                       $("#txtExcise").val($('#jQGridDemo').jqGrid('getCell', rowid, 'ExciseAmt'));
                       $("#txtBillValue").val($('#jQGridDemo').jqGrid('getCell', rowid, 'BillValue'));
                       $("#txtTotalAmount").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TotalAmount'));
                       var txtamt = $('#jQGridDemo').jqGrid('getCell', rowid, 'TaxAmt');
                       $("#txttaxamt").val(txtamt);
                       $("#txttaxamt2").val(txtamt);
                       $("#txttSGSTaxamt").val(txtamt);
                       $("#txtIGSTtaxamt").val(txtamt);
                       $("#txttax").val($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxP'));
                       $("#txtSurchargeAmt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'SurchrgAmt'));
					   $("#txtCessamt").val($('#jQGridDemo').jqGrid('getCell', rowid, 'SurchrgAmt'));


                       var supplierID = $('#jQGridDemo').jqGrid('getCell', rowid, 'SupplierId');

                       $("#ddlSupplier option").removeAttr("selected");
                       $('#ddlSupplier option[value=' + supplierID + ']').prop('selected', 'selected');


                       $("#ddlGodown option").removeAttr("selected");
                       $('#ddlGodown option[value=' + $('#jQGridDemo').jqGrid('getCell', rowid, 'GodownId') + ']').prop('selected', 'selected');



                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis1InRs') == "true") {
                           $('#chkDis1InRs').prop('checked', true);

                       }
                       else {
                           $('#chkDis1InRs').prop('checked', false);

                       }



                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2InRs') == "true") {
                           $('#chkDis2InRs').prop('checked', true);
                       }
                       else {
                           $('#chkDis2InRs').prop('checked', false);

                       }


                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'Dis2AftDedDis1') == "true") {
                           $('#chkDis2After1').prop('checked', true);
                       }
                       else {
                           $('#chkDis2After1').prop('checked', false);

                       }

                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxAfterDis1') == "true") {
                           $('#txtTaxBeforeDis1').prop('checked', false);
                       }
                       else {
                           $('#txtTaxBeforeDis1').prop('checked', false);
                       }
                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'TaxAfterDis2') == "true") {
                           $('#txtTaxBeforeDis2').prop('checked', false);
                       }

                       else {
                           $('#txtTaxBeforeDis2').prop('checked', false);
                       }

                       if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsLocal') == "Local") {

                           $('#rbLocal').prop('checked', true);
                           $('#rbOutStation').prop('checked', false);

                           $("#trGST").show();
                           $("#trSGST").hide();
                           //$("#trIGST").hide();

                       }
                       else {

                           $('#rbLocal').prop('checked', false);
                           $('#rbOutStation').prop('checked', true);
                           $("#trGST").hide();
                           $("#trSGST").show();
                           //$("#trIGST").show();



                       }
                   }
               });





                $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                           {
                               refresh: false,
                               edit: false,
                               add: false,
                               del: false,
                               search: true,
                               searchtext: "Search",
                               addtext: "Add",
                           },

                           {//SEARCH
                               closeOnEscape: true

                           });

                var DataGrid = jQuery('#jQGridDemo');
                DataGrid.jqGrid('setGridWidth', '700');

            }


        </script>
    </form>
</asp:Content>
