﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.SqlClient;
using System.Data;

public partial class managecashcustomers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


            hdnDate.Value = DateTime.Now.ToShortDateString();
          
        }
        CheckRole();

    }

    protected void btnexprt_cst_Click(object sender, EventArgs e)
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        String SqlQuery = "select Customer_Name,Address_1,Address_2,State.State_name,Contact_no,Emailid from Customer_Master,state where Customer_Master.state_id=state.state_id and cast(createddate as date) between @todate  and  @fromdate";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@todate", tb_todate.Text.Trim());
        cmd.Parameters.AddWithValue("@fromdate", tb_fromdate.Text.Trim());
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        string attachment = "attachment; filename=CashCustomer.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {

            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                Response.Write(tab + "\"=\"\"" + dr[i].ToString() + "\"\"\"");
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.CASHCUSTOMERS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string Delete(Int32 CustomerId)
    {
        Customers objCustomer = new Customers()
        {
            Customer_ID = CustomerId,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new CustomerBLL().DeleteCustomer(objCustomer);
        var JsonData = new
        {
            Customer = objCustomer,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdateCustomer(Customers objCustomer)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objCustomer.UserId = Id;
        objCustomer.BranchId = Branch;
        int status = new CustomerBLL().InsertUpdate(objCustomer);
        var JsonData = new
        {
            customer = objCustomer,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByCustomerId(int CustomerId)
    {
        Customers objCustomers = new Customers()
        {
            Customer_ID = CustomerId,
        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        new CustomerBLL().GetById(objCustomers);
        var JsonData = new
        {
            Customer = objCustomers


        };
        return ser.Serialize(JsonData);
    }


}