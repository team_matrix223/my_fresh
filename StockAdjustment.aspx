﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="StockAdjustment.aspx.cs" Inherits="StockAdjustment" %>

    <%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
  

<%@ Register Src="~/Templates/StockAdjustment.ascx" TagName="AddKit" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
        <link href="css/customcss/Inventory.css" rel="stylesheet" />
    <link href="css/customcss/stockadjustment.css" rel="stylesheet" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPluginStock.js"></script>
    <script language="javascript" type="text/javascript">

        var m_KitID = 0;
        var ProductCollection = [];
        var mrpeditable = false;
        var saleRateeditable = false;
        var mItmId = 0;

        function clsProduct() {
            this.Item_ID = 0;
            this.Code = "";
            this.IName = "";
            this.ActualStock = 0;
            this.StockAdjust = 0;
            this.Rate = 0;
            this.MRP = 0;
            this.StockAdjusted = 0;
            this.Amount = 0;
        

        }

        var m_KitID = 0;

        function CommonCalculation() {

            var Adjusted = Number($("#txtQty").val()) - Number($("#txtStock").val());
            $("#txtAdjusted").val(Adjusted);

            var Amt = Number(Adjusted) * $("#txtRate").val();

            $("#txtAmount").val(Amt);
        }


        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());

            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });



        function BindRows() {
           

            var TotalAmount = 0;
            var html = "";

            for (var i = 0; i < ProductCollection.length; i++) {



                html += "<tr>";
                html += "<td>" + ProductCollection[i]["Code"] + "</td>";
                html += "<td>" + ProductCollection[i]["IName"] + "</td>";
                html += "<td>" + ProductCollection[i]["ActualStock"] + "</td>";
                html += "<td>" + ProductCollection[i]["StockAdjust"] + "</td>";
                html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
                html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
                html += "<td>" + ProductCollection[i]["StockAdjusted"] + "</td>";
                html += "<td>" + ProductCollection[i]["Amount"] + "</td>";
                html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer;width:20px'   /></td>";
                html += "</tr>";

                TotalAmount += parseFloat(ProductCollection[i]["Amount"])

            }

         
            $("#tbKitProducts").html(html);



        }


       


        function ResetControls() {

            m_KitID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtddlItems").val("").removeAttr("disabled");
            $("#ddlItems").html("<option value='0'></option>")
            $("#txtGrNo").val("");
            $("#txtVehNo").val("");
            $("#ddlDealer").val("0");
          
            $("#ddlRefno").val("0");
            $("#txtBillval").val("");
            $("#txtDisAmt").val("");
            $("#txtAdj").val("");
            $("#txtnetAmt").val("");
            ProductCollection = [];
            ResetList();
        }

        function ResetList() {


            $("#txtCode").val("");
            $("#txtName").val("");
            $("#txtQty").val("");
            $("#txtRate").val("");
            $("#txtMarketPrice").val("");
            $("#txtAmount").val("");
            $("#txtTax").val("");
            $("#ddlProducts").html("<option value='0'></option>");
            $("#txtddlProducts").val("").focus();
            $("#txtStock").val("");



        }



        function GetPluginData(Type) {



            
            if ($("#ddlGodown").val() == "0") {
                alert("Please choose Godown");
                return;
            }
            


                var m_ItemCode = $("#ddlProducts option:selected").attr("item_code");

                $("#txtCode").val(m_ItemCode);
                $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
                $("#txtQty").val("").focus();
                $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
                $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
                $("#txtAmount").val(0);


                var CurrentStock = 0;
                for (var i = 0; i < ProductCollection.length; i++) {

                    if (m_ItemCode == ProductCollection[i]["Item_Code"]) {

                        CurrentStock = parseFloat(CurrentStock) + parseFloat(ProductCollection[i]["Qty"]);
                    }

                }

                $("#txtStock").val(parseFloat($("#ddlProducts option:selected").attr("stock_qty")) - parseFloat(CurrentStock));
            
        }

        $(document).ready(function () {



            $("#btnGo").click(
                   function () {

                       BindGrid();

                   }
                    );
            $("#txtServiceId").change(
                function () {
                    CodeSearch($("#txtServiceId").val());
                });
            var ItemType = "";

			function CheckItemCode(ItemCode) {
				$.ajax({
					type: "POST",
					data: '{"ItemCode": "' + ItemCode + '"}',
					url: "StockAdjustment.aspx/ChkItemCode",
					contentType: "application/json",
					dataType: "json",
					success: function (msg) {

						var obj = jQuery.parseJSON(msg.d);

						var isexist = obj.rtnval;

						if (isexist == 1) {

							BindGridProducts(ItemCode);
							$("#ItemGrid1").dialog({
								modal: true,
								closeOnEscape: false,
							});

							$("#ItemGrid1").parent().addClass('Itemgrid-pop');


						}
						else {
							alert("Item Code Does not Exists");
							$("#txtServiceId").focus();
							return;
						}

					},
					error: function (xhr, ajaxOptions, thrownError) {

						var obj = jQuery.parseJSON(xhr.responseText);
						alert(obj.Message);
					},
					complete: function () {


						$.uiUnlock();
					}

				});

			}


			function BindGridProducts(stext) {
				if ($("#rdbFinished").prop("checked") == true) {
					ItemType = "3";
				}
				else if ($("#rdbRaw").prop("checked") == true) {

					ItemType = "1";
				}
				else if ($("#rdbSemiFinished").prop("checked") == true) {
					ItemType = "5";
				}



				var Type = "";
				if ($("#rdbGroup").is(':checked') == true) {
					Type = "G";
				}
				else if ($("#rdbDept").is(':checked') == true) {
					Type = "D";
				}
				else if ($("#rdbcompany").is(':checked') == true) {
					Type = "C";
				}
				var Group = $("#ddlGroup").val();
				var Godown = $("#ddlGodown").val()

				var list = $("#jQGridProduct");
				jQuery("#jQGridProduct").GridUnload();

                jQuery("#jQGridProduct").jqGrid({
					url: 'handlers/CProductForStock.ashx?Keyword=' + stext + '&Type=' + Type + '&AccountType=' + "" + '&Group=' + Group + '&ItemType=' + ItemType + '&Godown=' + Godown,
					ajaxGridOptions: { contentType: "application/json" },
					datatype: "json",

					colNames: ['Code', 'Name', 'Rate', 'MRP', 'TaxId', 'Tax', 'StockQty'
					],
					colModel: [
						{ name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, hidden: false },
						{ name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, hidden: false },
						{ name: 'Sale_Rate', index: 'Sale_Rate', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'Tax_ID', index: 'Tax_ID', width: 50, stype: 'text', sortable: true, hidden: true },
						{ name: 'Tax_Code', index: 'Tax_Code', width: 50, stype: 'text', sortable: true, hidden: false },
						{ name: 'StockQty', index: 'StockQty', width: 50, stype: 'text', sortable: true, hidden: false },

					],
					//rowNum: 10,
					mtype: 'GET',
					loadonce: true,
					//toppager: true,
					//rowList: [10, 20, 30],
					//pager: '#jQGridProductPager',
					sortname: 'Item_Code',
					viewrecords: true,
					height: "100%",
					width: "800px",

					sortorder: 'desc',
					caption: "",
					editurl: 'handlers/CProductForStock.ashx',

					//toolbar: [true, "top"],
					ignoreCase: true,



				});




				var $grid = $("#jQGridProduct");





				$("#jQGridProduct").jqGrid('setGridParam',
					{
						ondblClickRow: function (rowid, iRow, iCol, e) {


							var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
							CodeSearch(serviceId);
							$("#ItemGrid1").dialog("close");
							event.stopPropagation();
							isfirst = 1;
							isupfirst = 1;
							selrowf = true;

						}
					});







				list.jqGrid('gridResize');
				list.jqGrid('bindKeys');

				var DataGrid = jQuery('#jQGridProduct');


				DataGrid.jqGrid('setGridWidth', '400');
				DataGrid.jqGrid('setSelection', 1, true);
				//list.setSelection("selectRow", 0);



			}

			var selrowf = true;

			var isfirst = 0;
			var isupfirst = 0;
			var currow = 0;

			$(document).on("keydown", "#jQGridProduct", function (e) {



				if (e.keyCode == 40) {

					currow = 0;

					//arrow("next");

					if (selrowf == true) {

						$("#jQGridProduct").setSelection(1);
						selrowf = false;

					}
					var list = $('#jQGridProduct'),

						$td = $(e.target).closest("tr.jqgrow>td"),
						p = list.jqGrid("getGridParam"),
						//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						cm = "Item_Code";

					var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					var selectedRow = list.jqGrid('getGridParam', 'selrow');


					if (isfirst == 0) {

						selectedRow = selectedRow - 1;

					}


					if (selectedRow == null) return;

					var ids = list.getDataIDs();
					var index = list.getInd(selectedRow);

					if (ids.length < 2) return;
					index++;

					list.setSelection(ids[index - 1], false, e);
					currow = index;

					var rows = document.querySelectorAll('#jQGridProduct tr');

					var line = document.querySelector(1);



					rows[line].scrollTop({
						behavior: 'smooth',
						block: 'nearest'
					});

					//                  var w = $(window);

					//                  var row = $('#jQGridProduct').find('tr').eq(line);

					//                  if (row.length) {

					//                      w.scrollTop(row.offset().top - (w.height / 2));
					//}
					e.preventDefault();




				}

				if (e.keyCode == 38) {

					currow = 0;
					//arrow("prev");
					var list = $('#jQGridProduct'),

						$td = $(e.target).closest("tr.jqgrow>td"),
						p = list.jqGrid("getGridParam"),
						//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						cm = "Item_Code";

					var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					var selectedRow = list.jqGrid('getGridParam', 'selrow');


					if (isupfirst == 0) {
						selectedRow = Number(selectedRow) + Number(1);

					}

					if (selectedRow == null) return;
					var ids = list.getDataIDs();

					var index = list.getInd(selectedRow);

					if (ids.length < 2) return;
					index--;

					list.setSelection(ids[index - 1], false, e);
					currow = index;

					var rows = document.querySelectorAll('#jQGridProduct tr');

					var line = document.querySelector(1);



					rows[line].scrollTop({
						behavior: 'smooth',
						block: 'nearest'
					});

					//var w = $(window);
					//var row = $('#jQGridProduct').find('tr').eq(line);

					//                  if (row.length) {
					//                      list.scrollTop(row.offset().top + (12/ 2));
					//}
					e.preventDefault();


				}

				if (e.ctrlKey && e.keyCode == 13) {

					if (currow == 0) {
						currow = 1;
					}
					var rowid = currow;
					var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
					
						$('#txtServiceId').val(serviceId);
					
					CodeSearch(serviceId);
					$("#ItemGrid1").dialog("close");
					event.stopPropagation();
					isfirst = 1;
					isupfirst = 1;
					selrowf = true;
				}
				// }

			});





			function CodeSearch(ItemCode) {

				var serviceId = ItemCode;
				

				if ($("#rdbFinished").prop("checked") == true) {
					ItemType = "3";
				}
                else if ($("#rdbRaw").prop("checked") == true) {
                    
					ItemType = "1";
				}
				else if ($("#rdbSemiFinished").prop("checked") == true) {
					ItemType = "5";
				}



				var Type = "";
				if ($("#rdbGroup").is(':checked') == true) {
					Type = "G";
				}
				else if ($("#rdbDept").is(':checked') == true) {
					Type = "D";
				}
				else if ($("#rdbcompany").is(':checked') == true) {
					Type = "C";
                }
                var Group = $("#ddlGroup").val();
                var Godown = $("#ddlGodown").val()
               
				if (serviceId != "") {

					$.ajax({
                        type: "POST",
                        data: '{ "Keyword": "' + serviceId + '", "Type": "' + Type + '","AccountType": " ","GroupId": "' + Group + '","ItemType": "' + ItemType + '","Godown": "' + Godown + '"}',
						url: "StockAdjustment.aspx/GetByItemCode",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);


							if (obj.productData.Item_Name == "") {

								CheckItemCode(serviceId);


								return;

							}

                            else {

                                var m_ItemCode = serviceId

								$("#txtCode").val(m_ItemCode);
								$("#txtName").val(obj.productData.Item_Name);
								$("#txtQty").val("").focus();
								$("#txtRate").val(obj.productData.Sale_Rate);
								$("#txtMarketPrice").val(obj.productData.Max_Retail_Price);
								$("#txtAmount").val(0);


								var CurrentStock = 0;
								for (var i = 0; i < ProductCollection.length; i++) {

									if (m_ItemCode == ProductCollection[i]["Item_Code"]) {

										CurrentStock = parseFloat(CurrentStock) + parseFloat(ProductCollection[i]["Qty"]);
									}

								}

								$("#txtStock").val(parseFloat(obj.productData.StockQty) - parseFloat(CurrentStock));



                                mItmId = obj.productData.ItemID;
									



							}


						}, error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function (msg) {

							$.uiUnlock();



						}


					});



				}




			}




            function SuperSearch() {

                $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
                var ItemType = "";

                if ($("#rdbFinished").prop("checked") == true) {
                    ItemType = "3";
                }
                else if ($("#rdbRaw").prop("checked") == true) {
                    ItemType = "1";
                }
                else if ($("#rdbSemiFinished").prop("checked") == true) {
                    ItemType = "5";
                }



                var Type = "";
                if ($("#rdbGroup").is(':checked') == true) {
                    Type = "G";
                }
                else if ($("#rdbDept").is(':checked') == true) {
                    Type = "D";
                }
                else if ($("#rdbcompany").is(':checked') == true) {
                    Type = "C";
                }


                $("#ddlProducts").supersearch({
                    Type: Type,
                    Caption: "Please enter Item Name/Code ",
                    AccountType: "",
                    Group: $("#ddlGroup").val(),
                    ItemType: ItemType,
                    Godown: $("#ddlGodown").val(),
                    Width: 214,
                    DefaultValue: 0
                });
            }

            $("#rdbcfinished").change(
          function () {

					if ($("#rdbcfinished").prop("checked") == true) {
						ItemType = "3";
					}
          }
          );


            $("#rdbraw").change(
          function () {

					if ($("#rdbRaw").prop("checked") == true) {
						ItemType = "1";
					}
          }
          );

			$("#rdbSemiFinished").change(
          function () {

					if ($("#rdbSemiFinished").prop("checked") == true) {
						ItemType = "5";
					}
          }
          );

            $("#txtBreakageDate").val($("#<%=hdntodaydate.ClientID %>").val());

            $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());




            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });



            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtBreakageDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });






            ResetList();

            $("#btnCancelDialog").click(
           function () {
               BindGrid();
               $("#KitDialog").dialog("close");

           }
           );




            $("#btnSave").click(
            function () {


                if (!validateForm("dvDialog")) {
                    return;
                }


                if (ProductCollection.length == 0) {

                    alert("Please choose Delivery Items");
                    $("#ddlProducts").focus();
                    return;
                }




                var RefNo = $("#txtRefNo").val();
                if (RefNo == "Auto") {
                    RefNo = "0"
                }

                var RefDate = $("#txtBreakageDate").val();
                var GodownId = $("#ddlGodown").val();





                var DTO = { 'RefNo': RefNo, 'RefDate': RefDate, 'GodownId': GodownId, 'objStockMaster': ProductCollection };



                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "StockAdjustment.aspx/InsertUpdate",
                    data: JSON.stringify(DTO),
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                       
                        m_KitID = obj.status;

                        if (m_KitID == 0) {
                            alert("Some Error Occured.Please try Again");
                        }
                        else {
                            alert("Stock Adjusted Successfully");
                        }



                        BindGrid();
                        $("#KitDialog").dialog("close");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                      
                        window.location.href = "Reports/StockAdjustment.aspx?RefNo=" + m_KitID + "";

                    }
                });









            });


            $("#btnAddKitItems").click(
           function () {

               //               if (!(Number($("#txtQty").val()) < 0)) {
               //                   $("#txtQty").focus();
               //                   return;

               //               }

                    TO = new clsProduct();
                    TO.Item_ID = mItmId;
                    TO.Code = $("#txtCode").val();
					TO.IName = $("#txtName").val();
               TO.ActualStock = $("#txtStock").val();
               TO.StockAdjust = $("#txtQty").val();
					TO.Rate = $("#txtRate").val();
					TO.MRP = $("#txtMarketPrice").val();
               TO.StockAdjusted = $("#txtAdjusted").val();
               TO.Amount = $("#txtAmount").val();
               ProductCollection.push(TO);

               BindRows();
               ResetList();

           }

           );



            $("#txtQty").keyup(
           function () {

               CommonCalculation();

           }
           );

            $("#txtRate").keyup(
           function () {
               CommonCalculation();


           }
           );





            $("#ddlGroup").change(
            function () {




              //  $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
                var ItemType = "";

                if ($("#rdbFinished").prop("checked") == true) {
                    ItemType = "3";
                }
                else if ($("#rdbRaw").prop("checked") == true) {
                    ItemType = "1";
                }
                else if ($("#rdbSemiFinished").prop("checked") == true) {
                    ItemType = "5";
                }

                var Type = "";
                if ($("#rdbGroup").is(':checked') == true) {
                    Type = "G";
                }
                else if ($("#rdbDept").is(':checked') == true) {
                    Type = "D";
                }
                else if ($("#rdbcompany").is(':checked') == true) {
                    Type = "C";
                }



                //$("#ddlProducts").supersearch({
                //    Type: Type,
                //    Caption: "Please enter Item Name/Code ",
                //    AccountType: "",
                //    Group: $("#ddlGroup").val(),
                //    ItemType: ItemType,
                //    Godown: $("#ddlGodown").val(),
                //    Width: 214,
                //    DefaultValue: 0
                //});

            }
            );



            BindGrid();
            $("#btnNew").click(
                            function () {


                                ResetControls();
                                $("#txtRefNo").val("Auto");
                                $("#KitDialog").dialog({ autoOpen: true,
                                height: 600,
                                    width: 1115,
                                    resizable: false,
                                    modal: true
                                });
                            }
                            );




            $("#btnPrint").click(
               function () {

                   var myGrid = $('#jQGridDemo'),
             selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
             celValue = myGrid.jqGrid('getCell', selRowId, 'Ref_No');

                   if (celValue == '') {
                       alert("Please select a row from Grid");
                       return;
                   }


                   window.location.href = "Reports/StockAdjustment.aspx?RefNo=" + celValue + "";

               }
            );


        });
   
    </script>
    <style>
        h2.manage_title_top{
            font-size:15px;
            font-weight:400;
        }
        button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
            display: none;
        }
        .form-control-feedback.left {
            left: 0px;
        }
        .form-control-feedback {
            margin-top: 0;
        }
        label.stk-lbl
        {
            font-size:12px;
        }
        input#txtRefNo {
            padding-left: 6px;
            padding-right: 6px;
            width:90%;
        }
        input#txtBreakageDate {
            width: 90%;
        }
        .stock-frm-grp
        {
            margin-bottom:0;
        }
        div#showColumn {
            top: -4px;
        }
        table.manage_table_top.stk-adjs-type-tbl td {
            text-align: center !important;
        }
        .stk-adj-x-panel{
            max-height:150px;
            overflow-y:auto;
            min-height:150px;
            padding:0;
        }
        .stk-adj-x-panel table.table.table-striped.manage_table_top th {
            padding: 7px 0 !important;
        }
        .inventry_adddialog
        {
            background:#ffffff !important;
        }
        div#KitDialog {
            background: #ffffff;
            height: 610px !important;
        }
        .stk-adjs-type-tbl td {
            padding-bottom: 0;
        }
        .page-title {
            padding: 0;
        }
        @media(max-width:767px)
        {
            .stk-adj-mrp-tbl select {
	            width: 100px;
            }
            .stk-adj-mrp-tbl input {
	            width: 150px;
            }
        }
    </style>
    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnRoles" runat="server" />
      <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
    <asp:HiddenField ID="hdnDate" runat="server" />
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Stock Adjustment</h3>
                </div>
                <div class="x_panel">
                    <div class="form-group">
                        <div class="youhave" style="padding-left: 1px">
                          

                             <table width="100%">
                                <tr>
                                    <td align="left">
                                        <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>
                                                    Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                         <table id="jQGridDemo">
                            </table>
                                    </td>
                                </tr>
                            </table>

                            <table cellspacing="0" cellpadding="0" style="margin-top: 5px">
                                <tr>
                                    <td>
                                        <!--&nbsp;-->
                                    </td>
                                    <td style="padding: 0;">
                                        <div id="btnNew" class="btn btn-primary" style="margin-right:7px;">
                                        <i class="fa fa-external-link"></i>
                                            New</div>
                                    </td>
                                    <td>
                                       <!--&nbsp;-->
                                    </td>
                                    <td style="padding: 0;display:none;">
                                        <div id="btnEdit" class="btn btn-success" style="margin-right:7px;">
                                        <i class="fa fa-edit m-right-xs"></i>
                                            Edit</div>
                                    </td>
                                    <td>
                                       <!--&nbsp;-->
                                    </td>
                                    <td style="padding: 0;">
                                        <div id="btnPrint" class="btn btn-success">
                                         <i class="fa fa-edit m-right-xs"></i>
                                            Re-Print</div>
                                    </td>
                                </tr>
                            </table>
                            <div id="jQGridDemoPager">
                            </div>
                        </div>
                        <div class="row" id="KitDialog" style="display: none" title="<%= Request.Cookies[Constants.BranchName].Value %>">
                            <uc1:AddKit ID="ucAddKit" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            
       
        </div>
    </form>
    <script type="text/javascript">

        

                function BindGrid() {
                      var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/manageStockAdjustment.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['RefNo','RefDate','Godown','GodownName','Amount'],
  


                        colModel: [
                                   { name: 'Ref_No', index: 'Ref_No', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'strrefdate', index: 'strrefdate', width:200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                     { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Godown', index: 'Godown', width:400, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Amount', index: 'Amount', width: 400, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                   

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Kit_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Stock Adjusted List",

                        editurl: 'handlers/managedelivery.ashx',
                           ignoreCase: true,
                         toolbar: [true, "top"],


                    });

                     var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  ResetControls();
            m_KitID = $('#jQGridDemo').jqGrid('getCell', rowid, 'Bill_No');
 
             
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>

