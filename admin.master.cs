﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.Services;

public partial class admin : System.Web.UI.MasterPage
{
    public string EmployeeName { get; set; }
    public string BranchName { get; set; }
    public string pagename { get; set; }
    //public string m_smessage { get; set; }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Request.Cookies.AllKeys.Contains(Constants.AdminId))
        {
            Response.Redirect("loginerror.aspx");
         
        }

        EmployeeName = Request.Cookies[Constants.EmployeeName].Value;
        BranchName = Request.Cookies[Constants.BranchName].Value;
        dspltitle.InnerText= Request.Cookies[Constants.DisplayCompanyName_].Value;
        //User objUser = new User();
        //new UserBLL().GetSiteUrl(objUser);
        //m_smessage = objUser.Title;
        if (IsPostBack==false)
        {

       
        string currentPageName = Request.Url.Segments[Request.Url.Segments.Length - 1];


        pagename = currentPageName;
        UserActivityLog.SetActivityLog("IN", pagename);
        }

    }


    }
