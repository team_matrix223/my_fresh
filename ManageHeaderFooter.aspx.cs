﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class ManageHeaderFooter : System.Web.UI.Page
{
    Connection conn = new Connection();

    int sid = 0;
    int rowIndex = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {

            Bindgride();
       }
    }
    public void Bindgride()
    {


        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select * from setting_billprint", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@req", "getdata");
            cmd.Parameters.AddWithValue("@BranchId", Request.Cookies[Constants.BranchId].Value);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            gv_display.DataSource = dt;
            gv_display.DataBind();
        }

    }
    protected void gv_display_RowEditing(object sender, System.Web.UI.WebControls.GridViewEditEventArgs e)
    {
        //NewEditIndex property used to determine the index of the row being edited.  
        gv_display.EditIndex = e.NewEditIndex;
        Bindgride();
    }
    protected void gv_display_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            Label SNO = gv_display.Rows[e.RowIndex].FindControl("lbl_sno") as Label;
            TextBox Text = gv_display.Rows[e.RowIndex].FindControl("txt_headertxt") as TextBox;
            con.Open();
            SqlCommand cmd = new SqlCommand("update setting_billprint set HEADERTEXT=@HEADERTEXT where SNO=@SNO", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@HEADERTEXT", Text.Text.Trim());
            cmd.Parameters.AddWithValue("@SNO", SNO.Text);
            cmd.ExecuteNonQuery();
            gv_display.EditIndex = -1;
            Bindgride();
        }
    }
    protected void gv_display_RowCancelingEdit(object sender, System.Web.UI.WebControls.GridViewCancelEditEventArgs e)
    {
        //Setting the EditIndex property to -1 to cancel the Edit mode in Gridview  
        gv_display.EditIndex = -1;
        Bindgride();
    }


}