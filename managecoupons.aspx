﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managecoupons.aspx.cs" Inherits="managecoupons" %>

<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 <link href="css/customcss/setup.css" rel="stylesheet" />

     <script src="js/jquery-ui.js"></script>

<%--<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
   
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
  <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
       <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>--%>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
     
   
<script language="javascript" type="text/javascript">

      
    $(document).ready(
   function () {
       BindGrid();


            $("#btnAdd").click(
                function () {
					$("#dvCoupon").dialog({
						autoOpen: true,

						width: 500,
						resizable: false,
						modal: false
					});
                });



			$("#btnDelete").click(
				function () {

					var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
					if ($.trim(SelectedRow) == "") {
						alert("No Record is selected to Delete");
						return;
					}

					var RefNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'RefNo')
					if (confirm("Are You sure to delete this record")) {
						$.uiLock('');

						$.ajax({
							type: "POST",
							data: '{"RefNo":"' + RefNo + '"}',
							url: "managecoupons.aspx/Delete",
							contentType: "application/json",
							dataType: "json",
							success: function (msg) {

								var obj = jQuery.parseJSON(msg.d);

								if (obj.status == -1) {
									alert("Deletion Failed. ");
									return
								}

								BindGrid();
								alert("Coupon is Deleted successfully.");




							},
							error: function (xhr, ajaxOptions, thrownError) {

								var obj = jQuery.parseJSON(xhr.responseText);
								alert(obj.Message);
							},
							complete: function () {
								$.uiUnlock();
							}
						});
					}


				}
			);





			$("#btnSave").click(
				function () {
                    var BillNo = $("#txtBillNo").val();
					var NoOfCoupon = $("#txtCouponNo").val();
                    var Amount = $("#txtAmount").val();
                    if (NoOfCoupon == "") {
                        alert("Enter No Of Coupons");
						
                        return;
                    }
                    if (BillNo == "") {
						alert("Enter Bill No");

						return;
                    }
                    if (Amount == "") {
						alert("Enter Amount");

						return;
                    }

					$.ajax({
                        type: "POST",
                        data: '{ "BillNo": "' + BillNo + '","NoOfCoupons": "' + NoOfCoupon + '","Amount": "' + Amount + '"}',
						url: "managecoupons.aspx/Insert",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);
							
							
							if (obj.Status == -1) {
								alert("Bill NO does not exist.");
								return;
							}


							else {
                                alert("Coupon Added Successfully.");
                                $("#txtCouponNo").val("");
								$("#txtBillNo").val("");
								$("#txtAmount").val("");
								BindGrid();
								

							}

							
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {

							
						}

					});




				});





   });


</script>
<style>
.setup_title
{
    background :#ffffff;
    color:#73879C;
}
.manage-cities-xpanel
{
    min-height:484px;
    max-height:484px;
    overflow: auto;
}

@media(max-width:667px)
{
    .manage-cities-xpanel .x_content {
	    overflow: auto;
    }
}
@media(min-width:992px) and (max-width:1200px)
{
    .manage-cities-xpanel {
	    min-height: 405px;
	    max-height: 405px;
	    overflow: auto;
    }
}
</style>
<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Coupons</h3>
                        </div>
                      
                    </div>
                 
                    <div class="x_title setup_title">
                            <h2>Coupons</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                    <div id="dvCoupon" style="display:none">
                        <table>
<tr><td class="headings">Bill No:</td><td colspan="3">  <input type="text"  maxlength="50" name="txtBillNo" class="form-control validate required "  data-index="1" id="txtBillNo"/></td></tr>
                            <tr><td class="headings">No Of Coupons:</td><td colspan="3">  <input type="text"  maxlength="50" name="txtCouponNo" class="form-control validate required "  data-index="1" id="txtCouponNo"/></td></tr>
                            <tr><td class="headings">Amount:</td><td colspan="3">  <input type="text"  maxlength="50" name="txtAmount" class="form-control validate required "  data-index="1" id="txtAmount"/></td></tr>
                           <tr> <td> <div id="btnSave"  style="display:block;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Save</div></td></tr>

                        </table>
                    </div>
     <div class="x_panel manage-cities-xpanel">
                        
                        <div class="x_content">

                               <div class="youhave">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>

         <table cellspacing="0" cellpadding="0" style="margin-top:5px">
                                            <tr>
                                             <td> <div id="btnAdd"  style="display:block;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add</div></td>                                          
                                            <td> <div id="btnDelete"  style="display:block;" class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i>
 Delete</div></td>
                                            </tr>
                                            </table>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                

            </div>

                <!-- footer content -->
              
                <!-- /footer content -->
 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageCoupons.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

						colNames: ['Ref No', 'Bill No', 'No of Coupons','From','To','Amount'],
                        colModel: [
							{ name: 'RefNo', key: true, index: 'RefNo', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
							{ name: 'BillNo', index: 'BillNo', width: 100, stype: 'text', sortable: true, editable: true, editrules: { required: true } },

							{ name: 'NoOfCoupons', index: 'NoOfCoupons', width: 100, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
							{ name: 'CouponFrom', index: 'CouponFrom', width: 100, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
							{ name: 'CouponTo', index: 'CouponTo', width: 100, stype: 'text', sortable: true, editable: true, editrules: { required: true } },    
							{ name: 'Amount', index: 'Amount', width: 100, stype: 'text', sortable: true, editable: true, editrules: { required: true } },    

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
						sortname: 'RefNo',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Coupons List",

                        editurl: 'handlers/ManageCoupons.ashx',


 ignoreCase: true,
                         toolbar: [true, "top"],

                    });



                     var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });





                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                   
                    
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>



</asp:Content>
