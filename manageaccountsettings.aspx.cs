﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_manageaccountsettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            BindBranches();
            BindBanks();
            BindAccounts();
        }
        CheckRole();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ITEMSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    void BindAccounts()
    {
        ddlpurchase.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlpurchase.DataValueField = "CCODE";
        ddlpurchase.DataTextField = "CNAME";
        ddlpurchase.DataBind();
        ListItem li123 = new ListItem();
        li123.Text = "--Choose Account--";
        li123.Value = "0";
        ddlpurchase.Items.Insert(0, li123);


        ddlcashinhand.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlcashinhand.DataValueField = "CCODE";
        ddlcashinhand.DataTextField = "CNAME";
        ddlcashinhand.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account--";
        li1.Value = "0";
        ddlcashinhand.Items.Insert(0, li1);

        ddldisplay.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddldisplay.DataValueField = "CCODE";
        ddldisplay.DataTextField = "CNAME";
        ddldisplay.DataBind();
        ListItem li11 = new ListItem();
        li11.Text = "--Choose Account--";
        li11.Value = "0";
        ddldisplay.Items.Insert(0, li11);

        ddldiscount.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddldiscount.DataValueField = "CCODE";
        ddldiscount.DataTextField = "CNAME";
        ddldiscount.DataBind();
        ListItem li12 = new ListItem();
        li12.Text = "--Choose Account--";
        li12.Value = "0";
        ddldiscount.Items.Insert(0, li12);


        ddladjustment.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddladjustment.DataValueField = "CCODE";
        ddladjustment.DataTextField = "CNAME";
        ddladjustment.DataBind();
        ListItem li13 = new ListItem();
        li13.Text = "--Choose Account--";
        li13.Value = "0";
        ddladjustment.Items.Insert(0, li13);

        ddlbillround.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlbillround.DataValueField = "CCODE";
        ddlbillround.DataTextField = "CNAME";
        ddlbillround.DataBind();
        ListItem li14 = new ListItem();
        li14.Text = "--Choose Account--";
        li14.Value = "0";
        ddlbillround.Items.Insert(0, li14);

        ddlcstsale.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlcstsale.DataValueField = "CCODE";
        ddlcstsale.DataTextField = "CNAME";
        ddlcstsale.DataBind();
        ListItem li15 = new ListItem();
        li15.Text = "--Choose Account--";
        li15.Value = "0";
        ddlcstsale.Items.Insert(0, li15);

        ddlcstsaletax.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlcstsaletax.DataValueField = "CCODE";
        ddlcstsaletax.DataTextField = "CNAME";
        ddlcstsaletax.DataBind();
        ListItem li16 = new ListItem();
        li16.Text = "--Choose Account--";
        li16.Value = "0";
        ddlcstsaletax.Items.Insert(0, li16);

        ddlgstded.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlgstded.DataValueField = "CCODE";
        ddlgstded.DataTextField = "CNAME";
        ddlgstded.DataBind();
        ListItem li17 = new ListItem();
        li17.Text = "--Choose Account--";
        li17.Value = "0";
        ddlgstded.Items.Insert(0, li17);

        ddlincometax.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlincometax.DataValueField = "CCODE";
        ddlincometax.DataTextField = "CNAME";
        ddlincometax.DataBind();
        ListItem li18 = new ListItem();
        li18.Text = "--Choose Account--";
        li18.Value = "0";
        ddlincometax.Items.Insert(0, li18);


        ddljobwrk.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddljobwrk.DataValueField = "CCODE";
        ddljobwrk.DataTextField = "CNAME";
        ddljobwrk.DataBind();
        ListItem li19 = new ListItem();
        li19.Text = "--Choose Account--";
        li19.Value = "0";
        ddljobwrk.Items.Insert(0, li19);


        ddljournal.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddljournal.DataValueField = "CCODE";
        ddljournal.DataTextField = "CNAME";
        ddljournal.DataBind();
        ListItem li110 = new ListItem();
        li110.Text = "--Choose Account--";
        li110.Value = "0";
        ddljournal.Items.Insert(0, li110);

        ddllabour.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddllabour.DataValueField = "CCODE";
        ddllabour.DataTextField = "CNAME";
        ddllabour.DataBind();
        ListItem li111 = new ListItem();
        li111.Text = "--Choose Account--";
        li111.Value = "0";
        ddllabour.Items.Insert(0, li111);

        ddlmse.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlmse.DataValueField = "CCODE";
        ddlmse.DataTextField = "CNAME";
        ddlmse.DataBind();
        ListItem li112 = new ListItem();
        li112.Text = "--Choose Account--";
        li112.Value = "0";
        ddlmse.Items.Insert(0, li112);

        ddlotherded.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlotherded.DataValueField = "CCODE";
        ddlotherded.DataTextField = "CNAME";
        ddlotherded.DataBind();
        ListItem li113 = new ListItem();
        li113.Text = "--Choose Account--";
        li113.Value = "0";
        ddlotherded.Items.Insert(0, li113);


        ddloutpurchase.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddloutpurchase.DataValueField = "CCODE";
        ddloutpurchase.DataTextField = "CNAME";
        ddloutpurchase.DataBind();
        ListItem li114 = new ListItem();
        li114.Text = "--Choose Account--";
        li114.Value = "0";
        ddloutpurchase.Items.Insert(0, li114);


        ddlouttax.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlouttax.DataValueField = "CCODE";
        ddlouttax.DataTextField = "CNAME";
        ddlouttax.DataBind();
        ListItem li115 = new ListItem();
        li115.Text = "--Choose Account--";
        li115.Value = "0";
        ddlouttax.Items.Insert(0, li115);


        ddlrar.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlrar.DataValueField = "CCODE";
        ddlrar.DataTextField = "CNAME";
        ddlrar.DataBind();
        ListItem li116 = new ListItem();
        li116.Text = "--Choose Account--";
        li116.Value = "0";
        ddlrar.Items.Insert(0, li116);

        ddlreserve.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlreserve.DataValueField = "CCODE";
        ddlreserve.DataTextField = "CNAME";
        ddlreserve.DataBind();
        ListItem li117 = new ListItem();
        li117.Text = "--Choose Account--";
        li117.Value = "0";
        ddlreserve.Items.Insert(0, li117);

        ddlsale.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlsale.DataValueField = "CCODE";
        ddlsale.DataTextField = "CNAME";
        ddlsale.DataBind();
        ListItem li118 = new ListItem();
        li118.Text = "--Choose Account--";
        li118.Value = "0";
        ddlsale.Items.Insert(0, li118);

        ddlsecurity.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlsecurity.DataValueField = "CCODE";
        ddlsecurity.DataTextField = "CNAME";
        ddlsecurity.DataBind();
        ListItem li119 = new ListItem();
        li119.Text = "--Choose Account--";
        li119.Value = "0";
        ddlsecurity.Items.Insert(0, li119);

        ddlservicecharge.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlservicecharge.DataValueField = "CCODE";
        ddlservicecharge.DataTextField = "CNAME";
        ddlservicecharge.DataBind();
        ListItem li120 = new ListItem();
        li120.Text = "--Choose Account--";
        li120.Value = "0";
        ddlservicecharge.Items.Insert(0, li120);

        ddlservicetax.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlservicetax.DataValueField = "CCODE";
        ddlservicetax.DataTextField = "CNAME";
        ddlservicetax.DataBind();
        ListItem li121 = new ListItem();
        li121.Text = "--Choose Account--";
        li121.Value = "0";
        ddlservicetax.Items.Insert(0, li121);


        ddltcsaccount.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddltcsaccount.DataValueField = "CCODE";
        ddltcsaccount.DataTextField = "CNAME";
        ddltcsaccount.DataBind();
        ListItem li122 = new ListItem();
        li122.Text = "--Choose Account--";
        li122.Value = "0";
        ddlcreditnote.Items.Insert(0, li122);
        ddlcreditnote.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlcreditnote.DataValueField = "CCODE";
        ddlcreditnote.DataTextField = "CNAME";
        ddlcreditnote.DataBind();
        ListItem li124 = new ListItem();
        li124.Text = "--Choose Account--";
        li124.Value = "0";
        ddlcreditnote.Items.Insert(0, li124);
        ddldebitnote.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddldebitnote.DataValueField = "CCODE";
        ddldebitnote.DataTextField = "CNAME";
        ddldebitnote.DataBind();
        ListItem li125 = new ListItem();
        li125.Text = "--Choose Account--";
        li125.Value = "0";
        ddldebitnote.Items.Insert(0, li125);
        ddlOrderAdv.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlOrderAdv.DataValueField = "CCODE";
        ddlOrderAdv.DataTextField = "CNAME";
        ddlOrderAdv.DataBind();
        ListItem li126 = new ListItem();
        li126.Text = "--Choose Account--";
        li126.Value = "0";
        ddlOrderAdv.Items.Insert(0, li126);

        ddlOrderSale.DataSource = new AccountsBLL().GetAllAccountsByCodeA();
        ddlOrderSale.DataValueField = "CCODE";
        ddlOrderSale.DataTextField = "CNAME";
        ddlOrderSale.DataBind();
        ListItem li127 = new ListItem();
        li127.Text = "--Choose Account--";
        li127.Value = "0";
        ddlOrderSale.Items.Insert(0, li127);

    }
    void BindBanks()
    {
        ltBanks.Text = new AccountsBLL().GetAccountHtml();
    }

    [WebMethod]
    public static string FillSettings(int Type)
    {
        NewAccounts ObjSettings = new NewAccounts();
        ObjSettings.BranchId = Type;
        new AccountSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Insert(string CN_Acc2DebitCode, string CN_Acc2DebitName, string OrderSaleAcnt, string OrderSaleAcntName, string OrderAdvAcnt, string OrderAdvAcntName, string DebitNoteAcnt, string DebitNoteAcntName, string CompAcnt, string CompAcntName, string DisplayAcnt, string DisplayAcntName, string AdjAcnt, string AdjAcntName, string DiscntAcnt, string DiscntAcntName, string TcsAcnt, string TcsAcntName, string BillRoundAcnt, string BillRoundAcntName, string ServiceChrgAcnt, string ServiceChrgAcntName, string ServiceTaxAcnt, string ServiceTaxAcntName, string IncomeTaxAcnt, string IncomeTaxAcntName, string LabourAcnt, string LabourAcntName, string GstdedAcnt, string GstdedAcntName, string OtherdedAcnt, string OtherdedAcntName, string SecurityAcnt, string SecurityAcntName, string ResrveAcnt, string ResrveAcntName, string JobwrkAcnt, string JobwrkAcntName, string RarAcnt, string RarAcntName, string SaleAcnt, string SaleAcntName, string PurchaseAcnt, string PurchaseAcntName, string JournalAcnt, string JournalAcntName, string MSEAcnt, string MSEAcntName, string OutpurAcnt, string OutpurAcntName, string OutTaxAcnt, string OutTaxAcntName, string CstsaleAcnt, string CstsaleAcntName, string CstsaleTaxAcnt, string CstsaleTaxAcntName, string BankAccounts)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);

        var Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //objSettings.UserId = Id;
        //objSettings.BranchId = Branch;
        string[] arrAccounts = BankAccounts.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("Id");
        DataRow dr;

        for (int i = 0; i < arrAccounts.Length; i++)
        {
            dr = dt.NewRow();
            dr["Id"] = arrAccounts[i];
            dt.Rows.Add(dr);
        }

        NewAccounts objAcc = new NewAccounts()
        {
            CN_Acc2DebitCode = CN_Acc2DebitCode,
            CN_Acc2DebitName = CN_Acc2DebitName,
            OrderSaleAcnt = OrderSaleAcnt,
            OrderSaleAcntName = OrderSaleAcntName,
            OrderAdvAcnt = OrderAdvAcnt,
            OrderAdvAcntName = OrderAdvAcntName,
            DebitNoteAcnt = DebitNoteAcnt,
            DebitNoteAcntName = DebitNoteAcntName,
            CompAcnt = CompAcnt,
            CompAcntName = CompAcntName,
            DisplayAcnt = DisplayAcnt,
            DisplayAcntName = DisplayAcntName,
            AdjAcnt = AdjAcnt,
            AdjAcntName = AdjAcntName,
            DiscntAcnt = DiscntAcnt,
            DiscntAcntName = DiscntAcntName,
            TcsAcnt = TcsAcnt,
            TcsAcntName = TcsAcntName,
            BillRoundAcnt = BillRoundAcnt,
            BillRoundAcntName =BillRoundAcntName,
            ServiceChrgAcnt = ServiceChrgAcnt,
            ServiceChrgAcntName = ServiceChrgAcntName,
            ServiceTaxAcnt = ServiceTaxAcnt,
            ServiceTaxAcntName = ServiceTaxAcntName,
            IncomeTaxAcnt = IncomeTaxAcnt,
            IncomeTaxAcntName = IncomeTaxAcntName,
            LabourAcnt= LabourAcnt,
            LabourAcntName = LabourAcntName,
            GstdedAcnt = GstdedAcnt,
            GstdedAcntName = GstdedAcntName,
            OtherdedAcnt = OtherdedAcnt,
            OtherdedAcntName = OtherdedAcntName,
            SecurityAcnt=SecurityAcnt,
            SecurityAcntName=SecurityAcntName,
            ResrveAcnt= ResrveAcnt,
            ResrveAcntName= ResrveAcntName,
            JobwrkAcnt = JobwrkAcnt,
            JobwrkAcntName = JobwrkAcntName,
            RarAcnt = RarAcnt,
            RarAcntName=RarAcntName,
            SaleAcnt=SaleAcnt,
            SaleAcntName=SaleAcntName,
            PurchaseAcnt = PurchaseAcnt,
            PurchaseAcntName = PurchaseAcntName,
            JournalAcnt= JournalAcnt,
            JournalAcntName= JournalAcntName,
            MSEAcnt=MSEAcnt,
            MSEAcntName = MSEAcntName,
            OutpurAcnt=OutpurAcnt,
            OutpurAcntName=OutpurAcntName,
            OutTaxAcnt = OutTaxAcnt,
            OutTaxAcntName=OutTaxAcntName,
            CstSaleAcnt= CstsaleAcnt,
            CstSaleAcntName = CstsaleAcntName,
            CstsaleTaxAcnt = CstsaleTaxAcnt,
            CstsaleTaxAcntName= CstsaleTaxAcntName,
            UserId = Id,
            BranchId = Branch,


        };
        int status = new AccountSettingBLL().UpdateAccountSettings(objAcc,dt);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }
}