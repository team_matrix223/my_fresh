﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerKotScreen_m.aspx.cs" Inherits="CustomerKotScreen_m" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="Touchcss/bootstrap.min.css" rel="stylesheet" />
    <link href="Touchcss/css.css" rel="stylesheet" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
    <link rel="stylesheet" href="Touchcss/jquery-ui.css">
    <script src="Touchjs/jquery-1.10.2.js"></script>
    <script src="Touchjs/jquery-ui.js"></script>
    <script src="Touchjs/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="Touchjs/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="Touchcss/bootstrap-glyphicons.css" />
    <link href="Touchjs/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="Touchjs/jquery.uilock.js"></script>
    <script src="js/bootstrap-select.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link href="css/customcss/customerkot.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.css" />
    <link href="css/bootstrap-select.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/keyboard.css" rel="stylesheet" /> 
    <script type="text/javascript" src="js/jquery.keyboard.js"></script>
    <%--<script type="text/javascript" src="Scripts/jquery-1.8.3.min.js"></script>--%>
    <style type="text/css">
        .form-control
        {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }
        
        #tbProductInfo tr
        {
            border-bottom: dotted 1px silver;
        }
        
        #tbProductInfo tr td
        {
            padding: 3px;
        }
        
           #tbkotinfo tr
        {
            border-bottom: dotted 1px silver;
        }
        
        #tbkotinfo tr td
        {
            padding: 3px;
        }
        
        #tboption tr
        {
            border-bottom: solid 1px black;
        }
        
        #tboption tr td
        {
            padding: 10px;
        }
        .Search1 {
    background-color: #f0626e;
    border-radius: 2px;
    float: left;
    margin: 0 0 5px -10px;
    padding: 10px;
    width: 103%;
}

    </style>
    <script language="javscript" type="text/javascript">
        var KotAmt = 0;
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var indexR = 0;
        //.....................................
        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode = "";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var AllowKKC = false;
        var AllowSBC = false;
        var KKC = 0;
        var SBC = 0;



        function ResetCashCustmr() {


            $("#txtAddon").val("");
            $("#dvAdd").hide();

        }



        function Reset() {

            $("#dvdisper").val(0);
            $("#dvdiscount").val(0);
            $("#dvnetAmount").html("0");
            $("#dvRound").html("0");
            $("#dvTax").html("0");
            $("#dvsertaxper").html("0");
            $("#dvKKCPer").html("0");
            $("#dvKKCAmt").html("0");
            $("#dvSBCPer").html("0");
            $("#dvSBCAmt").html("0");
            $("#dvsbtotal").html("0");
			$("#dvsbtotalnew").html("0");
            $(".cls_txtBillQty").val(0);
            $("#btn_batch").text(0);

        }


        function TakeMeTop() {
            $("html, body").animate({ scrollTop: $(document).height() }, 500);
        }

        function BindTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                     $("#ddlTableOpt").html(obj.TableOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }


            });


        }




        //...................................

        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;
        var KKCTAmt = 0;
        var SBCTAmt = 0;





        function RestControls() {


            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            //              $("#dvdisper").html("0");
            //              $("#dvdiscount").html("0");
            //              $("#dvnetAmount").html("0");
            //              $("#dvTax").html("0");
            //              $("#dvVat").html("0");
            //              $("#dvsertaxper").html("0");
            $("#dvsbtotal").html("0");
			$("#dvsbtotalnew").html("0");
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;
            m_AddOn = "";

            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            $("#ddlTableOpt option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            $("#dvBillWindow").hide();


        }



        function GetKKC() {


            $("#dvKKCPer").html(KKC);


            if ($("#chktakeway").prop('checked') == true) {
                if (Takeaway == "1") {

                    KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
                    $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


                }
                else {
                    $("#dvKKCPer").html("0");
                    KKCTAmt == "0";
                    $("#dvKKCAmt").html(KKCTAmt.toFixed(2));
                }
            }
            else if ($("#chktakeway").prop('checked') == false) {
                KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
                $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


            }


            return KKCTAmt;

        }


        function GetSBC() {


            $("#dvSBCPer").html(SBC);

            if ($("#chktakeway").prop('checked') == true) {
                if (Takeaway == "1") {

                    SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
                    $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



                }
                else {
                    $("#dvSBCPer").html("0");
                    SBCTAmt == "0";
                    $("#dvSbCAmt").html(SBCTAmt.toFixed(2));

                }
            }
            else if ($("#chktakeway").prop('checked') == false) {
                SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
                $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



            }


            return SBCTAmt;

        }


        function kotPrint(BillNowPrefix) {



            $.ajax({
                type: "POST",
                data: '{}',
                url: "manageKotscreen.aspx/KOTprint",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i <= obj.kotprint.length; i++) {



                        Printtkot(BillNowPrefix, obj.kotprint[i]["DepartmentName"], 'Kot', obj.kotprint.length, i);

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        }

        function InsertUpdate() {
            var Countprod = 0;

            for (var i = 0; i < ProductCollection.length; i++) {
                if (ProductCollection[i]["EditVal"] == "0") {
                    Countprod = Countprod + 1;
                }
            }


            if (Countprod == "0") {

                alert("No New Item Is Added To KOT");
                return;

            }
            else {



                if ($("#ddlTableOpt").val() == "0" && $("#chktakeway").prop("checked") == false) {
                    alert("Choose Table No");
                    return;
                }

                //                            if ($("#ddlsteward").val() == "0") {
                //                                alert("Choose Steward First");
                //                                return;
                //                            }

                //if ($("#ddlpax").val() == "0") {
                //    alert("Choose Pax First");
                //    return;
                //}


                var MKotNo = "1";

                var Tableno = $("#ddlTableOpt").val();
                if (Tableno == "0") {
                    Tableno = "0";
                }

                var TakeAway = false;
                if ($('#chktakeway').is(":checked")) {
                    TakeAway = true;
                }


                var pax = $("#ddlpax").val();
                if (pax == "") {
                    pax = "1";
                }
                var R_Code = "1";
                var M_Code = "1";
                var Value = KotAmt;
                var DisPer = 0;
                var DisAmt = 0;
                var ServiceCharges = 0;
                var TaxAmt = 0;
                var TotalAmt = KotAmt;
                //           var DisPer = $("#dvdisper").html();   
                //           var DisAmt = $("#dvdiscount").html();  
                //           var ServiceCharges =0;     
                //           var TaxAmt = $("#dvTax").html();
                //           var TotalAmt = $("#dvnetAmount").html(); 
                var Complementary = false;
                var Happy = false;
                if ($("#chkhappyhours").prop('checked') == true) {
                    Happy = true;
                }
               
                var Empcode = $('#<%=hdnempid.ClientID%>').val();

                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var PAmt = [];
                var Ptax = [];
                var AddOn = [];
                var EditVal = [];


                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");

                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {


                    ItemCode[i] = ProductCollection[i]["ItemCode"];
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = ProductCollection[i]["Price"];
                    Tax[i] = ProductCollection[i]["TaxCode"];
                    PAmt[i] = ProductCollection[i]["ProductAmt"];
                    Ptax[i] = ProductCollection[i]["Producttax"];
                    AddOn[i] = ProductCollection[i]["Addon"];
                    EditVal[i] = ProductCollection[i]["EditVal"];

                }




                var BillNowPrefix = "";
                $.uiLock();

                $.ajax({
                    type: "POST",
                    data: '{ "MKOTNo": "' + MKotNo + '","TableID": "' + Tableno + '","PaxNo": "' + pax + '","R_Code": "' + R_Code + '","M_Code": "' + M_Code + '","Value": "' + Value + '","DisPercentage": "' + DisPer + '","DisAmount": "' + DisAmt + '","ServiceCharges": "' + ServiceCharges + '","TaxAmt": "' + TaxAmt + '","TotalAmount": "' + TotalAmt + '","Complementary": "' + Complementary + '","Happy": "' + Happy + '","EmpCode": "' + Empcode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","AddOnArr": "' + AddOn + '","EditValArr": "' + EditVal + '","TakeAway": "' + TakeAway + '"}',
                    url: "CustomerKotScreen_m.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        BillNowPrefix = obj.BNF;



                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else {
                            alert("KOT Saved Successfully");
                            Reset();
                            //                            $("#ddlsteward").val(0);
                            $("#ddlpax").val(0);
                            $('#chktakeway').prop('checked', false);

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        //kotPrint(BillNowPrefix);


                        RestControls();
                        Reset();
                        //window.location.reload();
                        $.uiUnlock();

                    }

                });





            }

        }


        var option = "";



        var HoldQtyFrnt = 0;
        function GetByItemCode(div, ItemCode) {

            if ($("#ddlTableOpt").val() == 0)
            {

                alert('Please Select Table!')
                return false;
            }
            var attr = $('.Minus_frnt' + ItemCode).attr('disabled');
            if (typeof attr !== typeof undefined && attr !== false)
            {
                HoldQtyFrnt = $("#txtBillQty" + ItemCode).val();
                $('.Minus_frnt' + ItemCode).removeAttr('disabled');

            }
   

            if ($("#ddlTableOpt").val() == "0" && $("#chktakeway").prop("checked") == false) {
                alert("Choose Table No");
                return;
            }

            if ($("#ddlsteward").val() == "0") {
                alert("Choose Steward First");
                return;
            }

            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + ItemCode + '","billtype":0}',
                url: "screen.aspx/GetByItemCode",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    if (obj.productData.ItemID == 0) {
                        alert("No Item Found Corresponding to Code " + obj.productData.Item_Code);
                        $("#txtItemCode").val("").focus();

                    }
                    else {
                        addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, "", 0)
                        // addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, " ")

                    }
                    BindQtyFront(0);
                    //if (HoldQtyFrnt == $("#txtBillQty" + ItemCode).val()) {

                    //    $('.Minus_frnt' + ItemCode).attr('disabled', 'disabled');
                    //    return false;


                    //}
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });
        
        }


        function ManageNewItems(Falg,ItemCode) {

            //$(document).on("click", "#btnMinus_frnt", function (event) {});

            $(".btnPlus" + ItemCode).each(function () {

                var attr = $('.Minus_frnt' + ItemCode).attr('disabled');
                if (typeof attr !== typeof undefined && attr !== false) {
                  
                    $(this).click();

                }
            });


        }

        function GetKotDetail(KotNo) {


            $.uiLock('');
            OnLoad =1;
            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "manageKotscreen.aspx/GetKotDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    ProductCollection = [];

                    $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                    if (obj.productLists.length != "0") {
                        for (var i = 0; i < obj.productLists.length; i++) {


                            var takeway = obj.productLists[i].TakeAway;
                            if (takeway == true) {

                                $('#chktakeway').prop('checked', true);

                            }
                            else {
                                $('#chktakeway').prop('checked', false);

                            }

                            $("#ddlpax option[value='" + obj.productLists[i]["PaxNo"] + "']").prop("selected", true);
                            // $("#ddlsteward option[value='" + obj.productLists[i]["EmpCode"] + "']").prop("selected", true);
                            //addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, obj.productData.AddOn)
                            addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Qty"], obj.productLists[i]["AddOn"], 1);
                            BindQtyFront(1);

                   

                        }
                        $("#btnTotal").show();
                        $(".order_btn").click();
                    }
                    else {
                        $("#btnTotal").hide();
                        ProductCollection = [];
                        $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                    }




                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    OnLoad = 0;
                    $.uiUnlock();
                }

            });





        }


        function GetKotByTableNo(TableNo) {


            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{ "TableNo": "' + TableNo + '"}',
                url: "manageKotscreen.aspx/GetKotByTableNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    KotCollection = [];

                    if (obj.KOT.length != "0") {
                        for (var i = 0; i < obj.KOT.length; i++) {

                            //addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, obj.productData.AddOn)
                            addToKotList(obj.KOT[i]["KOTNo"], obj.KOT[i]["KotDate2"], obj.KOT[i]["KotTime"], obj.KOT[i]["TotalAmount"]);
                        }
                    }
                    else {
                        KotCollection = [];
                    }




                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();
                }

            });





        }


        function Printtkot(celValue, DeptValue, PType, max, i) {



            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportkot" + i);
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
            document.getElementById('reportkot' + i).contentWindow.location = "Reports/PrintKOT.aspx?BillNowPrefix=" + celValue + "&DepartmentName=" + DeptValue;

            if (max == i + 1) {
                DeletePrinter(celValue);
            }

        }


        function DeletePrinter(m_BillNowPrefix) {

            $.ajax({
                type: "POST",
                data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
                url: "BillScreen.aspx/DeletePrinter",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }



        //        function StewardLoginCheck(employee, pswrd) {

        //            $.ajax({
        //                type: "POST",
        //                data: '{"Steward":"' + employee + '","Password":"' + pswrd + '"}',
        //                url: "CustomerKotScreen_m.aspx/EmployeeLoginCheck",
        //                contentType: "application/json",
        //                dataType: "json",
        //                success: function (msg) {

        //                    var obj = jQuery.parseJSON(msg.d);
        //                 
        //                    if (obj.Status > 0) {
        //                        InsertUpdate();
        //                    }
        //                    else {

        //                        alert("Kindly Enter Valid Password");
        //                        return;
        //                    }


        //                },
        //                error: function (xhr, ajaxOptions, thrownError) {

        //                    var obj = jQuery.parseJSON(xhr.responseText);
        //                    alert(obj.Message);
        //                },
        //                complete: function () {
        //                    $.uiUnlock();
        //                }
        //            });




        //        }


        $(document).ready(
        function () {




            $.ajax({
                type: "POST",
                data: '{ }',
                url: "BillScreen.aspx/GetAllBillSetting",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    BillBasicType = obj.setttingData.retail_bill;
                    TakeAwayDine = obj.setttingData.TakeAwayDine;
                    AllowServicetax = obj.setttingData.ServiceTax;
                    if (AllowServicetax == 1) {
                        Sertax = obj.setttingData.SerTax_Per;


                        AllowKKC = obj.setttingData.AllowKKC;
                        AllowSBC = obj.setttingData.AllowSBC;
                        KKC = obj.setttingData.KKC;
                        SBC = obj.setttingData.SBC;
                        $("#trSBC").show();
                        $("#trKKC").show();
                        $("#trServicetax").show();
                    }
                    else {
                        $("#trServicetax").hide();
                        Sertax = 0;
                        AllowKKC = false;
                        AllowSBC = false;
                        KKC = 0;
                        SBC = 0;
                        $("#trSBC").hide();
                        $("#trKKC").hide();
                    }

                    Takeaway = obj.setttingData.AlloServicetax_TakeAway;

                    HomeDelCharges = obj.setttingData.homedel_charges;

                    minbillvalue = obj.setttingData.min_bill_value;
                    DeliveryCharges = obj.setttingData.del_charges;
                    EnableCashCustomer = obj.setttingData.CashCustomer;
                    DefaultBank = obj.setttingData.defaultBankID;
                    RoundBillAmount = obj.setttingData.roundamt;

                    PrintShortName = obj.setttingData.shortname;

                    FocAffect = obj.setttingData.focaffect;
                    NEgativeStock = obj.setttingData.NegtiveStock;

                    DefaultPaymode = obj.setttingData.defaultpaymodeID;
                    AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;

                    if (AllowDiscountOnBilling == 1) {
                        EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                        EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                        DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                        BackEndDiscount = obj.setttingData.Back_End_Discount;

                    }


                    if (HomeDelCharges == 1) {

                        if (minbillvalue == 0 || DeliveryCharges == 0) {
                           // $("#DelCharges").show();
                            $("#dvDeliveryChrg").removeAttr("disabled");
                            $("#chkDelivery").show();
                            //                            $("#lbldelcharges").show();

                        }
                        else {

                            $("#dvDeliveryChrg").val(DeliveryCharges);
                            $("#dvDeliveryChrg").attr("disabled", "disabled");
                            $("#chkDelivery").hide();
                            //                            $("#lbldelcharges").hide();
                            //$("#DelCharges").show();
                        }

                    }
                    else {
                        $("#DelCharges").hide();
                        $("#chkDelivery").hide();
                        //                        $("#lbldelcharges").hide();
                        $("#dvDeliveryChrg").val(0);
                    }


                    if (EnableCashCustomer == 1) {

                        $("#btnCash").removeAttr('disabled')
                        //$("#txtMobSearchBox,#imgSrchCashCust").show();
                        $("#txtddlMobSearchBox").removeAttr('disabled');

                    }
                    else {

                        $("#btnCash").prop("disabled", true).css("background", "gray");
                        //$("#txtMobSearchBox,#imgSrchCashCust").hide();

                        $("#txtddlMobSearchBox").attr('disabled', 'disabled');
                    }


                    if (BillBasicType == "I") {

                        $("#vatIncOrExc").hide();
                    }
                    else {
                        $("#vatIncOrExc").show();
                    }



                    if (DiscountOnBillValue == 1) {

                        DiscountValues = obj.DiscountDetail;



                    }

                    if (EnableDiscountAmount == 1) {

                        $("#dvdisper").removeAttr('disabled');
                        $("#dvdiscount").removeAttr('disabled');
                        $("#dvdisper").val("0");
                        $("#dvdiscount").val("0");
                    }
                    else {

                        $("#dvdisper").attr('disabled', 'disabled');
                        $("#dvdiscount").attr('disabled', 'disabled');
                        $("#dvdisper").val("0");
                        $("#dvdiscount").val("0");
                    }
                    
                 },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                }

            });




            $("#btnTotal").click(
            function () {
                var tableno = $("#ddlTableOpt").val();
                window.location = "TotalBill.aspx?Id=A&tblno=" + tableno + "&cashcustcode=0&cashcustName=Cash&cashcustmob=0&compnycustid=-1"
            });

            BindTables();
            $("#dvKotList").show();
            $("#btnGetByItemCode").click(
        function () {


            var ItemCode = $("#txtItemCode");

            if (ItemCode.val().trim() == "") {
                ItemCode.focus();
                return;
            }


            GetByItemCode(this, ItemCode.val());


        }

        );


            $("#ddlSourceTable").change(
         function () {

             var Tableval = $("#ddlSourceTable").val();

             GetKotByTableNo(Tableval);

         });







            $("#ddlTableOpt").change(
         function () {

             var Tableval = $("#ddlTableOpt").val();
             Reset();
         
             GetKotDetail(Tableval);


         });





            $("#dvSave").click(
            function () {
                var sourcetable = 0;
                var DestTable = 0;
                sourcetable = $("#ddlSourceTable").val();
                if (sourcetable == "0") {
                    alert("Choose Source Table First");
                    $("#ddlSourceTable").focus();

                    return;
                }


                DestTable = $("#ddlDestTable").val();
                if (DestTable == "0") {
                    alert("Choose Destination Table");
                    $("#ddlDestTable").focus();

                    return;
                }

                var KotNos = [];


                if (KotCollection.length == 0) {
                    alert("No Kot Against This Table");

                    return;
                }

                for (var i = 0; i < KotCollection.length; i++) {


                    KotNos[i] = KotCollection[i]["KotNo"];


                }

                $.ajax({
                    type: "POST",
                    data: '{ "FromTable": "' + sourcetable + '","ToTable": "' + DestTable + '","qry": "' + KotNos + '"}',
                    url: "manageKotscreen.aspx/TransfrTable",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);


                        if (obj.Status != 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }

                        else {
                            alert("Table Transfered Successfully");

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $.uiUnlock();
                    }

                });



            });

            $("#btnTransfer").click(
        function () {



            $("#TransferDialog").dialog({
                autoOpen: true,
                left: 367,
                width: 350,
                resizable: false,
                modal: true
            });



        });



            $("#btnproceed").click(
        function () {

            $("#dvtable").dialog({
                autoOpen: true,
                left: 367,
                width: 350,
                resizable: false,
                modal: true
            });

        }
            );



            $("#btnsaveKot").click(
   function () {



       InsertUpdate();
       //       var employee = $("#ddlsteward option:selected").text();
       //       var pswrd = $("#txtPwd").val();
       //       if (pswrd == "") {
       //           alert("Enter Password");
       //           $("#txtPwd").focus();
       //           return;
       //       }
       //       else {

       //           StewardLoginCheck(employee, pswrd);

       //       }
   });




            //         $("div[id='dvdisper']").html("0");
            //           $("div[id='dvdiscount']").html( "0.00");

            $("#btnSearch").click(
        function () {

            var Keyword = $("#txtSearch");
            if (Keyword.val().trim() != "") {

                Search(0, Keyword.val());
            }
            else {
                Keyword.focus();
            }

        });



            $("#txtSearch").keyup(
            function (event) {

                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {


                    var Keyword = $("#txtSearch");
                    if (Keyword.val().trim() != "") {

                        Search(0, Keyword.val());
                    }
                    else {
                        Keyword.focus();
                    }


                }

            }

            );





            $("#txtItemCode").keyup(
            function (event) {






                var keycode = (event.keyCode ? event.keyCode : event.which);

                if (keycode == '13') {



                    if ($(this).val().trim() != "") {

                        GetByItemCode(this, $(this).val());
                        $(this).val("").focus();
                    }
                    else {
                        $(this).focus().val("");
                    }


                }

            });





            //            $("#chktakeway").change(function () {
            //                if ($("#chktakeway").prop('checked') == true) {
            //                    option = "TakeAway";
            //                    $("#ddlTableOpt").attr("disabled", "disabled");
            //                    $("#ddlTableOpt").val(0);

            //                }
            //                else {
            //                    option = "Dine";
            //                    $("#ddlTableOpt").removeAttr("disabled");

            //                }


            //            });




            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#categories").html(obj.categoryData);
                    $("#categories").clone().appendTo(".cls_categories");
                    // Sertax = obj.setttingData.SerTax;



                    //  Takeaway = obj.setttingData.TakeAway;
                    Takeawaydefault = obj.setttingData.TakeAwayDefault;

                    var CatId = obj.CategoryId;

                    Search(CatId, "");
                   
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );



            $("#dvExit").click(
        function () {

            $("#TransferDialog").dialog('close');

        });



            $("#btnExit").click(
        function () {

            Reset();
            $('#chktakeway').prop('checked', false);
            $("#ddlpax").val(0);
            RestControls();
            // window.location = "index.aspx";

        });




            $("#btnRaiseBill").click(
        function () {

            window.location = "KotRaiseBill.aspx";

        });


            $("#btnSettlement").click(
        function () {
            window.location = "Settlement.aspx"
        }
            );



            $(document).on("click", "#dvClose", function (event) {

                var RowIndex = Number($(this).closest('tr').index());
                var tr = $(this).closest("tr");
                tr.remove();

                ProductCollection.splice(RowIndex, 1);

                if (ProductCollection.length == 0) {

                    //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");


                    RestControls();



                }
                Bindtr();

            });



            $(document).on("click", "#dvAddon", function (event) {
                indexR = 0;
                indexR = Number($(this).closest('tr').index());
                $("#dvAdd").show();

            });



            $(document).on("click", "a[name='categories']", function (event) {

                $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
                $(this).addClass("ancSelected").removeClass("ancBasic");



            });



            $("#btnAddonsave").click(
        function () {
            var fAddon = $("#txtAddon").val();

            ProductCollection[indexR]["Addon"] = fAddon;
            $("#txtAddon").val("");
            $("#dvAdd").hide();
            Bindtr();
        });





        });

        var m_ItemId = 0;
        var m_ItemCode = "";
        var m_ItemName = "";
        var m_Qty = 0;
        var m_Price = 0;
        var m_TaxRate = 0;
        var m_Surval = 0;
        var m_AddOn = "";
        var m_EditVal = 0;

        var ProductCollection = [];
        function clsproduct() {
            this.ItemId = 0;
            this.ItemCode = "";
            this.ItemName = "";
            this.Qty = 0;
            this.Price = 0;
            this.TaxCode = 0;
            this.SurVal = 0;
            this.ProductAmt = 0;
            this.Producttax = 0;
            this.ProductSurchrg = 0;
            this.TaxId = 0;
            this.Addon = "";
            this.EditVal = 0;
        }

        var m_KotNo = 0;
        var m_KotDate = "";
        var m_KotTime = "";
        var m_Value = 0;

        var KotCollection = [];
        function clsKot() {
            this.KotNo = 0;
            this.KotDate = "";
            this.KotTime = "";
            this.Value = 0;

        }

        var m_Total = 0;

        $(document).on("keyup", "input[name='txtBillQty']", function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {
                var RowIndex = Number($(this).closest('tr').index());
                var PId = ProductCollection[RowIndex]["ItemId"];

                var Mode = "Plus";

                var Qty = ProductCollection[RowIndex]["Qty"];
                var Price = ProductCollection[RowIndex]["Price"];
                var fQty = $(this).val();
                if (isNaN(fQty)) {
                    fQty = 1;
                }
                ProductCollection[RowIndex]["Qty"] = fQty;

                Bindtr();

                $("#txtSearch").focus();
            }

        });

        function Bindtr() {

            KotAmt = 0;
            DiscountAmt = 0;
            VatAmt = 0;
            Total = 0;
            var fPrice = 0;
            $("div[name='vat']").html("0.00");
            $("div[name='amt']").html("0.00");
            $("div[name='sur']").html("0.00");
            var counterId = 1;
            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            for (var i = 0; i < ProductCollection.length; i++) {
                if ((ProductCollection[i]["EditVal"]) == "1")
                {

                    // var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center' disabled = 'disabled'>-</div></td><td style='width:40px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center' disabled = 'disabled'>+</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:80px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td><td style='width:50px;text-align:center'></td><td style='width:50px;text-align:center'></td></tr>";
                    var tr = "<tr><td class='cls_ItemCode' style='width:80px;text-align:center;font-size:15px;font-weight:normal;display:none;'>" + ProductCollection[i]["ItemCode"] + "</td><td>" + ProductCollection[i]["ItemName"] + "</td><td><div id='btnMinus' class='btn btn-primary btn-small btnMinus" + ProductCollection[i]["ItemCode"] + "' disabled='disabled'>-</div><input type = 'text' id ='txtBillQty" + counterId + "' class='cls_txtBillQty" + ProductCollection[i]["ItemCode"] + "' disabled = 'disabled' name = 'txtBillQty' value = '" + ProductCollection[i]["Qty"] + "'/><div id='btnPlus'  class='btn btn-primary btn-small  disabled = 'disabled'>+</div></td><td>" + ProductCollection[i]["Price"] + "</td><td style='width:0px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td></tr>";

                }
                else {

                    //var tr = "<tr><td style='width:80px;text-align:center'>" + ProductCollection[i]["ItemId"] + "</td><td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:30px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:40px;text-align:center'>" + ProductCollection[i]["Qty"] + "</td><td style='width:30px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:80px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'><i id='dvAddon' style='cursor:pointer'><img src='images/addon.png'/></i></td></tr>";
                    var tr = "<tr><td  class='cls_ItemCode' style='width:80px;text-align:center;font-size:15px;font-weight:normal;display:none;'>" + ProductCollection[i]["ItemCode"] + "</td><td>" + ProductCollection[i]["ItemName"] + "</td><td><div id='btnMinus' class='btn btn-primary btn-small btnMinus" + ProductCollection[i]["ItemCode"] + "'>-</div><input type = 'text'  id ='txtBillQty" + counterId + "'  class='cls_txtBillQty" + ProductCollection[i]["ItemCode"] + "'  name = 'txtBillQty' value = '" + ProductCollection[i]["Qty"] + "'/><div id='btnPlus'  class='btn btn-primary btn-small btnPlus" + ProductCollection[i]["ItemCode"] + "' >+</div></td><td>" + ProductCollection[i]["Price"] + "</td><td style='width:0px;text-align:center'>" + ProductCollection[i]["Addon"] + "</td><td><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td><td><i id='dvAddon' style='cursor:pointer'><img src='images/addon.png'/></i></td></tr>";
                    var amt = 0;
                    amt = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

                    KotAmt = KotAmt + amt;



                }


                $("#tbProductInfo").append(tr);
                fPrice = 0;
                fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
                var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);
                var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
                var tottax = TAx + surchrg;
                VatAmt = VatAmt + tottax;
                Total = Total + fPrice;
                ProductCollection[i]["ProductAmt"] = fPrice;
                ProductCollection[i]["Producttax"] = TAx;
                ProductCollection[i]["ProductSurchrg"] = surchrg;

                var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
                var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
                var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
                $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
                $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
                $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


                $(function () {
                    var wtf = $('#dvProductInfo');
                    var height = wtf[0].scrollHeight;
                    wtf.scrollTop(height);
                });





                CommonCalculation();


            }
            $("#btn_batch").text(ProductCollection.length);
            if (ProductCollection.length == 0) {
                var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
                $("#tbProductInfo").append(tr);

                Reset();
            }

           
        
        }


        function CommonCalculation() {
            m_Total = Total.toFixed(2);
            var ttl = Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2)));
            $("div[id='dvsbtotalnew']").html(ttl.toFixed(2));
			$("div[id='dvsbtotal']").html(Total.toFixed(2));
            $("div[id='dvVat']").html(VatAmt.toFixed(2));
            TaxAmt = GetServiceTax();
            KKCTAmt = GetKKC();
            SBCTAmt = GetSBC();
            var KKCVal = $("#dvKKCAmt").html();
            var SBCVal = $("#dvSBCAmt").html();
            $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCVal) + Number(SBCVal) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
            $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2)) + Number(KKCVal) + Number(SBCVal)) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

            var netamount = $("div[id='dvnetAmount']").html();
            var hdnNetamt = $("#hdnnetamt").val();
            var Round = Number(netamount) - Number(hdnNetamt)
            $("div[id='dvRound']").html(Round.toFixed(2));
        
       

        }


        function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, B_Qty, AddOn, EditVal) {
    
        
            if ($(".btnPlus" + Code).length > 0) {
                $(".btnPlus" + Code).click();
                return false;
            }

            m_AddOn = AddOn;
            m_ItemId = ProductId;
            m_ItemCode = Code;
            m_Price = Price;
            m_ItemName = Name;
            m_EditVal = EditVal;
            if (B_Qty == 0) {
                if (modeRet == "Return") {
                    m_Qty = -1;
                }
                else {
                    m_Qty = 1;
                }
            }
            else {
                m_Qty = B_Qty;
            }
            m_TaxRate = TaxCode;
            m_Surval = SurVal;
            m_Tax_Id = Tax_Id;

                     var item = $.grep(ProductCollection, function (item) {
                         return item.ItemId == m_ItemId;
                     });

                     if (item.length) {
                         var SumQty = 0;
                    
                           for (var i = 0; i < ProductCollection.length; i++)
                           {
                               if(ProductCollection[i]["ItemId"] == m_ItemId && EditVal==1)
                               {
                                   var qty = ProductCollection[i]["Qty"];
                                   SumQty = m_Qty + qty;
                                   ProductCollection[i]["Qty"] = SumQty;
                                   Bindtr();
                                   return;
                               }

                           }


                     }

                    
            TO = new clsproduct();

            TO.ItemId = m_ItemId;
            TO.ItemCode = m_ItemCode
            TO.ItemName = m_ItemName;
            TO.Qty = m_Qty;
            TO.Price = m_Price;
            TO.TaxCode = m_TaxRate;
            TO.SurVal = m_Surval;
            TO.TaxId = m_Tax_Id;

            TO.Addon = m_AddOn;
            TO.EditVal = m_EditVal;

            ProductCollection.push(TO);
       
            Bindtr();



        }


        function addToKotList(KotNo, KotDate, KotTime, Value) {

            m_KotNo = KotNo;
            m_KotDate = KotDate;
            m_KotTime = KotTime;
            m_Value = Value;

            TO = new clsKot();

            TO.KotNo = m_KotNo;
            TO.KotDate = m_KotDate
            TO.KotTime = m_KotTime;
            TO.Value = m_Value;


            KotCollection.push(TO);

            BindKottr();

        }



        function BindKottr() {

            $('#tbkotinfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            for (var i = 0; i < KotCollection.length; i++) {

                var tr = "<tr><td style='width:80px;text-align:center'>" + KotCollection[i]["KotNo"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["KotDate"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["KotTime"] + "</td><td style='width:80px;text-align:center'>" + KotCollection[i]["Value"] + "</td></tr>";

                //  var tr = "<tr> <td style='width:180px;text-align:center'>" + ProductCollection[i]["ItemName"] + "</td>";
                //             tr=tr+"<td style='text-align:center'><table ><tr style='border:0px'><td><div id='btnMinus' class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>-</div></td><td>" + ProductCollection[i]["Qty"] + " </td><td><div id='btnPlus'  class='btn btn-primary btn-small' style='height:25px;width:25px;text-align: center'>+</div></td></tr></table> </td><td style='width:50px;text-align:center'>" + ProductCollection[i]["Price"] + "</td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                //            

                $("#tbkotinfo").append(tr);


            }

        }






        function GetServiceTax() {


            var TaxAmt = 0;


            $("#dvsertaxper").html(Sertax);


            if ($("#chktakeway").prop('checked') == true) {
                if (Takeaway == "1") {
                    TaxAmt = (Number(Total) * Number(Sertax)) / 100;
                    $("#dvTax").html(TaxAmt.toFixed(2));


                }
                else {
                    $("#dvsertaxper").html("0");
                    TaxAmt == "0";
                    $("#dvTax").html(TaxAmt.toFixed(2));
                }
            }
            else if ($("#chktakeway").prop('checked') == false) {
                TaxAmt = (Number(Total) * Number(Sertax)) / 100;
                $("#dvTax").html(TaxAmt.toFixed(2));


            }


            return TaxAmt;

        }


        function Search(CatId, Keyword) {



            $.ajax({
                type: "POST",
                data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
                url: "screen.aspx/AdvancedSearch_stwrd",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#products").html(obj.productData);
                    //$.each(obj.productData.ItemCodeVal, function (key, value) {

                    //    var a = value;
                    //});
                   // BindQtyFront(obj.productData.ItemCodeVal, 0);
                    BindQtyFront(1);
                    closeNav();

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
            
                }

            }
        );


        }

        //........................................

        $(document).on("click", "#btnPlus", function (event) {


            var RowIndex = Number($(this).closest('tr').index());
            var PId = ProductCollection[RowIndex]["ItemId"];
            var ItemCode = ProductCollection[RowIndex]["ItemCode"];

            var Mode = "Plus";

            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];
            var fQty = 0;
            if (Qty < 0)
            {
                fQty = Number(Qty) + (-1);
            }
            else
            {
                fQty = Number(Qty) + 1;
            }
            ProductCollection[RowIndex]["Qty"] = fQty;
            Bindtr();

            BindQtyFront(0);

        });

        function BindQtyFront(OnLoad)
        {
       
            //if ($('.Minus_frnt' + ItemCode).has('disabled')) {
            //    HoldQty_frnt = $("#txtBillQty" + ItemCode).val();
            //    $('.Minus_frnt' + ItemCode).removeAttr('disabled');
            //}
        
            $('#dvProductInfo tr').each(function () {
               
                var currentRow = $(this).closest("tr");
                //var ItemCodeFrnt = $(this).attr('name');
                var SumQty = 0
                var OrderItemScreenItemCode = currentRow.find(".cls_ItemCode").text();
                $('#products #btnMinus_frnt').each(function () {
                    var ItemCode = $(this).attr('name');
                if (ItemCode == OrderItemScreenItemCode)
                {

                    var Qty = $(".cls_txtBillQty" + ItemCode).val();
                  
                    $(".cls_txtBillQty" + ItemCode).each(function () {


                        SumQty = Number(SumQty) + Number($(this).val());


                    });
                    $("#txtBillQty" + ItemCode).val(SumQty);
                    if (OnLoad == 1) {
                       // $("#txtBillQty" + ItemCode).attr('disabled', 'disabled');

                        $(".Minus_frnt" + ItemCode).attr('disabled', 'disabled');

                    }
                    else {


                        //$("#txtBillQty" + ItemCode).removeAttr('disabled');
                        if ($("#txtBillQty" + ItemCode).val() == HoldQtyFrnt) {
                            $('.Minus_frnt' + ItemCode).attr('disabled', 'disabled');
                            return false;

                        }
                       // $(".Minus_frnt" + ItemCode).removeAttr('disabled');
                    }
                }
                if (OrderItemScreenItemCode == "")
                {
                    $("#txtBillQty" + ItemCode).val(0);

                }
            
               
            });
      
            });

        } 

        $(document).on("click", "#btnMinus_frnt", function (event) {
            var ItemCodeFrnt = $(this).attr('name');
            $(".btnMinus" + ItemCodeFrnt).last().click();
            if ($(".btnMinus" + ItemCodeFrnt).length > 0) {
                //$(".btnMinus" + ItemCodeFrnt).click();
            }
            else {

                $("#txtBillQty" + ItemCodeFrnt).val(0);
            }
          
        
        });

        $(document).on("click", "#btnMinus", function (event) {

            var RowIndex = Number($(this).closest('tr').index());
        
            var PId = ProductCollection[RowIndex]["ItemId"];
            var ItemCode = ProductCollection[RowIndex]["ItemCode"];
            var Mode = "Minus";
            //var elems = document.querySelectorAll(".btnMinus" + ItemCode);
            //var len = elems.length;
            //var lastelement = len < 1 ? "" : elems[len - 2];
            //var lstindex=lastelement
          
     
            var Qty = ProductCollection[RowIndex]["Qty"];
            var Price = ProductCollection[RowIndex]["Price"];

            var fQty = 0;
            if (Qty < 0) {
                fQty = Number(Qty) - (-1);
            }
            else {
                fQty = Number(Qty) - 1;
            }

            ProductCollection[RowIndex]["Qty"] = fQty;
            if (fQty == "0") {
                ProductCollection.splice(RowIndex, 1);
            }
     
            Bindtr();

            BindQtyFront(0);

        });



        //............................................





    </script>

    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script>
        $(function () {
            //$('#txtSearch,#txtItemCode').keyboard();



            //                $('#txtItemCode').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '1 2 3 4 5',
            //    '6 7 8 9 0',
            //    ' {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 10,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();




            //                $('#txtCashReceived').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '9 8 7 6 5',
            //    '4 3 2 1 0',
            //    ' . {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 6,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();


        });
	</script>
</head>
<body style="background:#fffff6">
    <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
          <iframe id="reportkot" width="0" height="0" onload="processingComplete()"></iframe>


 <form id="Form1" runat="server">
    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />

 <asp:HiddenField ID="hdnempid" runat="server" />

     <section class="mobile_view">
         <div class="mob_header">
            <div id="mySidenav" class="sidenav">
              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
               <ul id="categories">
                     </ul>
                
            </div>

            <span class="catagories_togle" style="cursor:pointer" onclick="openNav()">&#9776; Catagories</span>
             <div class="pintu_midle">   <asp:Label ID="lblusername" runat="server"></asp:Label></div>
           
                 
             <button type='button' class='btn btn-demo order_btn' data-toggle='modal' data-target='#myModal2'>Order Items<span class="badge" id="btn_batch">0</span>    </button>
        </div>

         <div class="search_catagories">
             <ul>
                 <li><span>Table</span></li>
                 <li> <asp:DropDownList id="ddlTableOpt" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList></li>
                                 
                 <li><input class="form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Enter Code" aria-describedby="basic-addon2" id="txtItemCode" aria-haspopup="true" role="textbox" type="text"></li>
                 <li><span id="btnGetByItemCode" value="Search">
                     <img src="images/plusicon.png" alt=""></span>
                 </li>
                 <li><input class="form-control ui-keyboard-input ui-widget-content ui-corner-all" placeholder="Search By Name" aria-describedby="basic-addon2" id="txtSearch" aria-haspopup="true" role="textbox" type="text"></li>
                 <li><span id="btnSearch" value="Search">
                     <img src="images/search-button.png" alt=""></span>
                 </li>
             </ul>
             <ul>
                 <li><span>Pax</span></li>
                 <li style="margin-left: 9px;"> <asp:DropDownList id="ddlpax" class="form-control" ClientIDMode="Static" runat="server">
           <asp:ListItem Value="0">Choose</asp:ListItem>
           <asp:ListItem Value="1">1</asp:ListItem>
           <asp:ListItem Value="2">2</asp:ListItem>
           <asp:ListItem Value="3">3</asp:ListItem>
           <asp:ListItem Value="4">4</asp:ListItem>
           <asp:ListItem Value="5">5</asp:ListItem>
           <asp:ListItem Value="6">6</asp:ListItem>
           <asp:ListItem Value="7">7</asp:ListItem>
           <asp:ListItem Value="8">8</asp:ListItem>
           <asp:ListItem Value="9">9</asp:ListItem>
           <asp:ListItem Value="10">10</asp:ListItem></asp:DropDownList></li>
             </ul>
         </div>

         <%--<h1 class="best_food">Best Food</h1>--%>

         <div class="products_main">
                <ul class="cls_categories">
                     </ul>
                 <div class="mobile_view_products">
                   <div id="products"></div>
                 </div>
             <%--<div class="mobile_view_products">
                <div class="product_leftside">
                    <ul>
                    <li><p><i class="far fa-dot-circle"></i>MIX VEG RAITA</p></li>
                    <li><p class="item_price">100.00</p></li>
                   </ul>
                </div>

                 <div class="product_right_side">
                     <ul>
                     <li><input type="text" class="form-control"></li>
                     <li><button type="button" class="btn">Add</button></li>
                     </ul>
                 </div>
             </div>--%>

         </div>

         <div class="mobile_footer">

             <div class="cate2 modal_cate">
                 <table class="left_btn">
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tbody>
                                        <tr>
                                            <%--<td>
                                                <input type="text" placeholder="Mobile Number" style="border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="ui-keyboard-input ui-widget-content ui-corner-all"
                                                    aria-haspopup="true" role="textbox">
                                            </td>
                                            <td>
                                                <div id="btnCustomer">
                                                    <img src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>
                                            </td>--%>
                                         <%--   <td style="padding-left: 10px">
                                                <asp:DropDownList ID="ddloption" runat="server" Style="width: 100px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>--%>
                                           <%-- <td>
                                             <select id="ddlTable" style="height: 25px; width: 90px" class="form-control">
                                        
                                            </td>--%>

                                        </tr>
                                        <tr>
                                            <td>
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 1px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2">
                                                    <tr>
                                                        <td>
                                                            <label style="font-weight: bold">
                                                                Cash Customer:</label><label id="lblCashCustomerName" style="font-weight: normal;
          margin-top: 1px"></label>
                                                        </td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>
                                                </table>
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                         <tr><td>
                          <table>
          
     
          <%-- <tr><td style="color:White;font-size:13px;font-weight:bold">Password:</td><td style="padding-left:3px;font-size:17px;padding-top:2px"><input id="txtPwd" type="text" style="width:131px;font-size:17px;"></td></tr>--%>
            
                             <tr class="total_amount_btns">
                      


                            <td>
                                <div class="button1" id="btnsaveKot">
                                     Save</div></td>

                              <td>
                                <div class="button1" id="btnTotal" style="display: none;">
                                     Total Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnExit">
                                    Exit</div>
                            </td>
                         <%--   <td>
                                <div class="button1" id="btnRaiseBill" style="width:85px;font-size:medium">
                                   Raise Bill</div>
                            </td>
                            <td>
                                <div class="button1" id="btnSettlement" style="width:85px;font-size:medium">
                                   Settlement</div>
                            </td>
                             <td>
                                <div class="button1" id="btnTransfer" style="width:85px;font-size:medium">
                                    Transfer</div>
                            </td>--%>
                             
                        </tr>
                                
                                </table>
                         
                         </td></tr>
                       <%-- <tr>
                        <td>
                        <div class="button1" id="btnproceed" style="width:85px;font-size:medium">
                                     Proceed</div>
                        
                        </td>
                        </tr>--%>
                        
                        <tr>
                           
                            <td>
                                <div class="button1" id="btnCash"  style="display:none">
                                    Update KOT</div>
                            </td>
                           
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left; display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <table id="tbamountinfo">
                        <tr>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td class="footer_nt_amt">
                                           <span> Total:</span>
                                             <div style="display:none" id="dvsbtotal">
                                            </div>
                                             <div id="dvsbtotalnew">
                                            </div>
                                        </td>
                                    
                                    </tr>
                                    <tr style="display:none">
                                        <td style="width: 60%; font-weight: bold; text-align: right">
                                            Discount:
                                        </td>
                                        <td style="width: 30px; text-align: center;">
                                            <div id="dvdisper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;">
                                            <div id="dvdiscount">
                                            </div>
                                        </td>
                                    </tr>
                                   
                                   <tr id ="trServicetax">
                                        <td style="width: 60%;font-weight: bold; text-align: right">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;"  >
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;"  >
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trKKC">
                                        <td style="width: 60%;font-weight: bold; text-align: right;">
                                            KKC:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;" >
                                            <div id="dvKKCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvKKCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trSBC">
                                        <td style="width: 60%;font-weight: bold; text-align: right">
                                            SBC:
                                        </td>
                                        <td style="width: 30px; text-align: center;font-weight: bold;" >
                                            <div id="dvSBCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvSBCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="vatIncOrExc">
                                        <td style="font-weight: bold; text-align: right">
                                            Vat Amount:
                                        </td>
                                        <td style="width: 100px; text-align: center;font-weight: bold;" >
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr style="display:none;" id ="DelCharges">
                                        <td>
                                           Delv Chrgs:
                                        </td>
                                        <td>
                                            <input type="text" class="form-control input-small"  id="dvDeliveryChrg" value ="0" />
                                        </td>
                                    </tr>

<%--
                                     <tr id ="DelCharges" style="display:block">
                                        <td style="font-weight: bold; text-align: right;display:block">
                                            Delv Chrgs:
                                        </td>
                                        <td style="width: 100px; text-align: right;display:block" >
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvDeliveryChrg" value ="0" />

                                                    </td>
                                                    </tr>--%>
                                           
                                     
                                     <tr style="display:none;">
                                        <td>
                                            RoundOff:
                                        </td>
                                        <td>
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr style="display:none">
                                        <td class="footer_nt_amt">
                                            <span> Net Amt:</span>

                                            <div id="dvnetAmount" >
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                    
                </div>

             <%--<div class="container total_modal">
              
                  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Total Amount</button>

               
                  <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog modal-sm">
                      <div class="modal-content">
                        <div class="modal-header">
                         
                          <h4 class="modal-title">Total Amount</h4>
                        </div>
                        <div class="modal-body">
                   
           </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>--%>
           </div>

    </section>


     <!--Order Items Modal-->
     <div class="container demo">
<%--	<div class="text-center">
		<button type="button" class="btn btn-demo" data-toggle="modal" data-target="#myModal2">
			Right Sidebar Modal
		</button>
	</div>--%>

	
	<!-- Modal -->
	<div class="modal right fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel2">Items</h4>
				</div>

				<div class="modal-body">
					<div id="dvProductList" style="display: block">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tbody><tr>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <%--<th>Amt</th>--%>
                               <th>Delete</th>
                               <th>Add-Ons</th>
                                
                               
                            </tr>
                        </tbody></table>
                    </div>
                    <div class="cate product_popup" id="dvProductInfo">
                        <table id="tbProductInfo">
                            <tbody><tr>
                                <td>
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </tbody></table>
                    </div>
                     </div>
				</div>

			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
	
	
</div><!-- container -->

</form>

<script>

    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
    }
</script>

</body>
</html>