﻿/*Validation: 1*/
ko.validation.rules['isLargerThan'] = {
    validator: function (val, otherVal) {
        /*important to use otherValue(), because ko.computed returns a function, 
        and the value is inside that function*/
        otherVal = otherVal();                
        return parseFloat(val) > parseFloat(otherVal);
    },
    message: 'This need to be larger than the First Number '
};
ko.validation.registerExtenders();


/*Validation: 2*/
ko.validation.rules['isBetween'] = {
    validator: function (val, otherVal) {
        /*important to use otherValue(), because ko.computed returns a function, 
        and the value is inside that function*/
        otherVal = otherVal();
        return parseFloat(val) >= parseFloat(otherVal.firstNumber)
            && parseFloat(val) <= parseFloat(otherVal.secondNumber);
    },
    message: 'This need to be between than the First Number and Second number'
};
ko.validation.registerExtenders();


/*view model*/
function ViewModel() {
    var self = this;
    /*fields*/
    self.firstNumber = ko.observable('').extend({ required: true, min: 0, digit: true});
    self.secondNumber = ko.observable('').extend({
        required: true,
        min: 0, 
        digit: true,
        isLargerThan: ko.computed(function () {
            return self.firstNumber();          //assigning observable
        })
    });
    self.thirdNumber = ko.observable('').extend({
        required: true,
        min: 0,
        digit: true,
        isBetween: ko.computed(function () {
            return {                            //assigning multiple observables
                firstNumber: self.firstNumber(),
                secondNumber: self.secondNumber()
            };
        })
    });


    /*errors*/
    self.errors = ko.validation.group([self.firstNumber, self.secondNumber, self.thirdNumber]);
    self.hasError = function() {
        return self.errors().length > 0;
    };
    self.showErrors = function() {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };


    /*start validation, if no error create */
    self.create = function() {
        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }
        alert('No error found');
    };


    /*initialize vm*/
    self.reset = function() {
        self.firstNumber('');
        self.secondNumber('');
        self.thirdNumber('');
        self.removeErrors();
    };
    self.init = function() {
        self.reset();
    };
}


$(document).ready(function() {
    var vm = new ViewModel();
    ko.applyBindingsWithValidation(vm);
});