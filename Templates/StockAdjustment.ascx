﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="StockAdjustment.ascx.cs" Inherits="Templates_StockAdjustment" %>

<script type ="text/javascript">


    $(document).ready(
    function () {


        $("#rdbDept").change(function () {

            BindDealer();

        });

        $("#rdbGroup").change(function () {

            BindDealer();

        });
        $("#rdbcompany").change(function () {

            BindDealer();

        });

        BindDealer();


        function BindDealer() {


            var Type = "";
            if ($("#rdbGroup").is(':checked') == true) {
                Type = "G";
            }
            else if ($("#rdbDept").is(':checked') == true) {
                Type = "D";
            }
            else if ($("#rdbcompany").is(':checked') == true) {
                Type = "C";
            }


            $("#ddlGroup").html("<option ></option>");

            $.ajax({
                type: "POST",

                contentType: "application/json; charset=utf-8",
                url: "StockAdjustment.aspx/BindDealers",
                data: '{"Type":"' + Type + '"}',
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html1 = "<option value = 0>--SELECT--</option>";

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }

                    $("#ddlGodown").html(html1);
                    $("#ddlGroup").append(obj.GroupStock);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });




                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>

<div id="ItemGrid1" title="Press CTRL+ENTER To Select Item" style="display:none;">
						
							<table id="jQGridProduct">
												</table>
												<div id="jQGridProductPager">
												</div>
						</div>


<div class="inventry_adddialog" id="dvDialog">

    <table class="manage_table_top">
        <tr>
            <td style="padding:0px;">
                <h2 class="manage_title_top">Stock Adjustment</h2>
            </td>
        </tr>
        <tr>

            <td style="vertical-align:top;padding:5px 2px 0;">

                <div class="form-group stock-frm-grp">

                    <label class="col-sm-1 control-label stk-lbl">RefNo:</label>
                    <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="txtRefNo">

                    </div>

                    <label class="col-sm-1 control-label stk-lbl">Date:</label>
                    <div class="col-md-2 xdisplay_inputx form-group has-feedback">
                        <input type="text" class="form-control has-feedback-left" id="txtBreakageDate" placeholder="MM/DD/YYYY" aria-describedby="inputSuccess2Status">
                        <span class="fa fa-calendar  form-control-feedback left" aria-hidden="true"></span>
                        <span id="Span1" class="sr-only">(success)</span>
                    </div>

                    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div id="showColumn" class="btn-group" data-toggle="buttons">

                            <input type="radio" id="rdbFinished" name="Finished" value="Finished" checked="checked" />
                            <label for="rdbFinished">Finished</label>
                            <input type="radio" id="rdbSemiFinished" name="Finished" value="Semifinished" />
                            <label for="rdbSemiFinished">Semi Finished</label>
                            <%--<input type="radio"  id ="rdbRaw" name="Finished" value="Raw"   /> --%>
                                <input id="rdbRaw" type="radio" value="Raw" name="Finished">
                                <%--<label for="rdbRaw"style="font-weight:bold; color:White">Raw</label>  --%>
                                    <label for="rdbRaw">Raw</label>

                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <table class="manage_table_top stk-adjs-type-tbl">

        <tr>
            <th>Type</th>
            <th>Sort by</th>
            <%--<th style="font-weight:bold; color:White">Type</th>--%>
                <th>Godown</th>
                <th><span id="spName">Group</span></th>
                <th style="display:none;"></th>
        </tr>
        <tr>
            <td>
                <input type="radio" id="rdbcompany" onchange="javascript:ChangeText('Company')" name="Department" value="Company" />
                <label for="rdbcompany">Company</label>
                <input type="radio" id="rdbDept" onchange="javascript:ChangeText('Department')" name="Department" value="Department" />
                <label for="rdbDept">Department</label>
                <input type="radio" onchange="javascript:ChangeText('Group')" id="rdbGroup" name="Department" value="Group" checked="" required />
                <label for="rdbGroup">Group</label>
            </td>

             <td>
                <input type="radio" id="rdbCode" name="Code" value="Code" />
                <label for="rdbCode;">Sort By Code </label>
                <input type="radio" id="rdbName" name="Code" value="Name" checked="" required />
                <label for="rdbName">Sort By Name </label>

            </td>

            <td>
                <select id="ddlGodown">
                    <option></option>
                </select>
            </td>

            <td>
                <select id="ddlGroup">
                    <option></option>
                </select>
            </td>
        </tr>

        <tr>
           
            <%-- <td style="text-align:right;padding-right:20px">
                                                 <input type="radio"  id="rdbUnit" name="Column" value="Unit"  /> &nbsp; <label style="font-weight:normal" for="rdbUnit">Show Unit Column </label>&nbsp;

                                                        <input type="radio" id="rdbRate" name="Column" value="Rate" checked="" required /> <label style="font-weight:normal" for="rdbRate">Sort Rate Column</label>

                                        </td>--%>
                <td></td>
                <td></td>

        </tr>

    </table>

    <table class="manage_table_top stk-adj-mrp-tbl">
        <thead>
            <tr>
                <th>Item/Code</th>
                <th>Code</th>
                <th>Name</th>
                <th>Stock</th>
                <th>ActualStock</th>
                <th>Rate</th>
                <th>MRP</th>
                <th>Adjusted</th>
                <th>Amount</th>
                <th></th>

            </tr>

        </thead>
        <tbody>
            <tr>
                <td id="DKID">
                    <input type='text' autocomplete='off' id="txtServiceId" placholder="Enter Item Code/Name" class='form-control input-small name'  />

                  <%--  <select id="ddlProducts"  class="form-control"></select>--%>
                </td>
                <td>
                    <input type="text" id="txtCode" readonly="readonly" class="form-control customTextBox" />
                </td>
                <td>
                    <input id="txtName" type="text" class="form-control customTextBox" />
                </td>
                <td>
                    <input type="text" id="txtStock" class="form-control customTextBox" readonly="readonly" />
                </td>
                <td>
                    <input type="text" id="txtQty" class="form-control customTextBox" />
                </td>
                <td>
                    <input type="text" id="txtRate" class="form-control customTextBox" readonly="readonly" />
                </td>
                <td>
                    <input type="text" id="txtMarketPrice" class="form-control customTextBox" readonly="readonly" />
                </td>
                <td>
                    <input type="text" id="txtAdjusted" class="form-control customTextBox" />
                </td>
                <td>
                    <input type="text" class="form-control customTextBox" readonly="readonly" id="txtAmount" />
                </td>
                <td>
                    <%--<button id="Button1" class="btn btn-success" style="width: 52px; margin-right: -5px; background-color: Black; border-color: white; font-weight: bold; height: 26px; margin-bottom: 0px; margin-top: -1px;" type="button">Add</button>--%>

                        <button type="button" class="btn btn-primary" id="btnAddKitItems">Add</button>
                </td>
            </tr>
        </tbody>

    </table>

    <div class="x_panel stk-adj-x-panel">
        <%--  <div class="x_title">
                                                            <h2>Kit ITEMS</h2>

                                                            <div class="clearfix"></div>
                                                        </div>--%>
            <table class="table table-striped manage_table_top">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Bal.Stock</th>
                        <th>ActualStock</th>
                        <th>Rate</th>
                        <th>MRP</th>
                        <th>Adjusted</th>
                        <th>Amount</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody id="tbKitProducts">

                </tbody>
            </table>
    </div>

    <div id="btnSave" data-bind="click: $root.PlaceOrder" class="btn btn-primary inv_sub_canc"><i class="fa fa-save"></i> Save</div>
    <button id="btnCancelDialog" data-bind="click: $root.CancelOrder" class="btn btn-danger inv_sub_canc"> <i class="fa fa-mail-reply-all"></i> Cancel</button>

</div>
  
