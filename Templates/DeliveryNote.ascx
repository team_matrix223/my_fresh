﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeliveryNote.ascx.cs" Inherits="Templates_DeliveryNote" %>

<%@ Register Assembly="TimePicker" Namespace="MKB.TimePicker"  TagPrefix="cc1" %>
<script type ="text/javascript">


    $(document).ready(function () {
      
        BindDealer();
        BindBranches();
        GetBillType();
        BindProductDropDown(1);
        OnLoadContent();
       
        function OnLoadContent() {
          
            //for (var i = 0; i < 8; i++) {
                addTR();
           // }
        
        };
        function BindGridData(counter_id) {


                var _Itemid = $("#ddlProducts" + counter_id).val();
                if (_Itemid != "") {

            TO = new clsProduct();
            TO.Item_ID = _Itemid
            TO.Item_Code = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("item_code")
            TO.Item_Name = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("item_name");
            TO.Qty_In_Case = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("Qty_In_Case");
            TO.Scheme = "false";

            TO.Qty = $("#txtQty" + counter_id + "").val();
            TO.Rate = $("#txtRate" + counter_id + "").val();
            //TO.Sale_Rate = $("#ddlProducts option:selected").attr("Sale_Rate");
            TO.MRP = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("mrp");
            TO.Amount = $("#txtAmount" + counter_id + "").val();


            TO.QTY_TO_LESS = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("Qty_To_Less");
            TO.Stock = $("#txtStock" + counter_id + "").val();
            TO.Tax_ID = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("tax_id")
            TO.TaxP = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("tax_code");
            TO.Excise = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("Excise");

            TO.Abatement = $('#ddlProducts' + counter_id + ' option[value=' + _Itemid + ']').attr("Abatement");
            // TO.Dis3P = $("#ddlDealer option:selected").attr("dis");
            TO.Dis3P = "0";

            TO.Bill_Date = '1/1/1';
            ProductCollection.push(TO);

                }
        }


        function GetBillType() {
            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managedeliverynote.aspx/GetAllBillSetting",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    BillBasicType = obj.setttingData.retail_bill;



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                }

            });

        }
        $(document).on('keyup', '.cls_qty', function (e) {
            var counter_ = $(this).attr('counter');
            var Amt = Number($("#txtQty" + counter_ + "").val()) * $("#txtRate" + counter_ + "").val();

            $("#txtAmount" + counter_ + "").val(Amt);
            CommonCalculation();
           

        });

		$(window).keydown(function (e) {
			if (e.keyCode == 27) {


				$("#ItemGrid1").dialog("close");
				event.stopPropagation();
				isfirst = 1;
				isupfirst = 1;
				selrowf = true;


			}

		});


		$(document).keydown(function (e) {
			if (e.keyCode == 27) {
				//return false;
			}


		});
		$("#tbProducts").keydown(function (e) {

            var cellindex = $(this).parents('td').index();
            cellindex = cellindex + 1;
			if (e.which == 40) {
                
				$(e.target).closest('tr').nextAll('tr').find('td').eq(cellindex).find(':text').focus();
			}
            if (e.which == 38) {
                
				$(e.target).closest('tr').prevAll('tr').first().find('td').eq(cellindex).find(':text').focus();
			}

		});

                    $("#btnNew").click(
                            function () {

                                $("#hdnpassing").val(true);
                                $("#btnSave").removeAttr('disabled');

                                ResetControls();
                                $("#txtBillNo").val("Auto");
                                $("#KitDialog").dialog({ 
                                    autoOpen: true,
                                    closeOnEscape: false,
                                height: 600,
                                    width: 1115,
                                    resizable: false,
                                    modal: true
                                });
                            });
                    $("#btnEdit").click(

           function () {


               var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');

               var Prefix = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Prefix');

               var BillNo = Prefix + m_KitID;



               $("#txtddlItems").prop("disabled", true);

               $.ajax({
                   type: "POST",
                   contentType: "application/json; charset=utf-8",
                   url: "managedeliverynote.aspx/GetById",
                   data: '{"BillNo":"' + BillNo + '"}',
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       $("#txtBillNo").val(obj.Delivery.Bill_No);
                       $("#txtGrNo").val(obj.Delivery.GR_No);
                       $("#txtBillDate").val(obj.Delivery.strBillDate);
                       $("#txtDate").val(obj.Delivery.strGrDate);
                       $("#txtDispDate").val(obj.Delivery.strDespDate);
                       $("#txtVehNo").val(obj.Delivery.Veh_No);
                       $("#ddlBranch").val(obj.Delivery.BranchId);
                       $("#ddlGodown").val(obj.Delivery.Godown_ID);
                       $("#ddlToBranch").val(obj.Delivery.ForBranch);
                       $("#ddlRefno").val(obj.Delivery.Form_Name);
                       $("#txtBillval").val(obj.Delivery.Bill_Value);
                       $("#txtDisAmt").val(obj.Delivery.Dis3Amt);
                       $("#txtDisPer").val(obj.Delivery.Dis3P);
                       $("#txtAdj").val(obj.Delivery.Adjustment);
                       $("#txtnetAmt").val(obj.Delivery.Net_Amount);

                       $("#txtExcise").val(obj.Delivery.Excise_Amt);



                       if (obj.Delivery.Type == "1") {


                           $("#rdbExcise").prop("checked", true);
                           $("#rdbnonexcise").prop("checked", false);
                           $("#rdballexcise").prop("checked", false);

                       }
                       else if (obj.Delivery.Type == "0") {

                           $("#rdbExcise").prop("checked", false);
                           $("#rdbnonexcise").prop("checked", true);
                           $("#rdballexcise").prop("checked", false);
                       }

                       else if (obj.Delivery.Type == "2") {

                           $("#rdbExcise").prop("checked", false);
                           $("#rdbnonexcise").prop("checked", false);
                           $("#rdballexcise").prop("checked", true);
                       }


                       $("#hdnpassing").val(obj.Delivery.delchk);

                       //ProductCollection = obj.DeliveryDetail;
                  
                       $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       for (var i = 0; i < obj.DeliveryDetail.length; i++) {
                           var tr = "";


                           tr = "<tr><td><input type='text'  id='txtServiceId" + i + "'  counter='" + i + "'  class='form-control input-small cls_itemcode'  value=" + obj.DeliveryDetail[i].Item_Code + " name='txtServiceId' ></td>" +
							   "<td><select id='ddlProducts" + i + "' onchange='javascript:ServiceChange(" + i + ");' name='ddlProducts' counter='" + i + "'  class='cls_ddlproduct' disabled='disabled'></select></td>" +

                               "<td><input type='text' id='txtQty" + i + "'  counter='" + i + "'  class='form-control input-small cls_qty' name='txtQty'  value=" + obj.DeliveryDetail[i].Qty + "></td>" +

                                "<td><input type='text' id='txtRate" + i + "'  counter='" + i + "' readonly=readonly   class='form-control input-small validate cls_rate' name='txtRate'   value=" + obj.DeliveryDetail[i].Sale_Rate + "></td>" +


                               "<td><input type='text' id='txtMRP" + i + "'    counter='" + i + "' readonly=readonly  class='form-control input-small validate float cls_mrp' name='txtMRP1' value=" + obj.DeliveryDetail[i].MRP + "></td>" +

                              "<td><input type='text' id='txtAmount" + i + "'  counter='" + i + "' readonly=readonly   class='form-control input-small' name='txtAmount'  value=" + obj.DeliveryDetail[i].Amount + " ></td>" +
                              "<td><input type='text' id='txtDis3Per" + i + "'    counter='" + i + "'   readonly=readonly  class='form-control input-small  validate' name='txtDis1Per'  value=" + obj.DeliveryDetail[i].Dis3Amt + "></td>" +
                              "<td><input type='text' id='txtstock" + i + "'   counter='" + i + "'  readonly=readonly  class='form-control input-small  validate' name='txtstock'  value=" + obj.DeliveryDetail[i].Stock + " ></td>" +
                              "<td><input type='text' id='txtTaxPer" + i + "'   counter='" + i + "' readonly=readonly  readonly=readonly class='form-control input-small  validate float' name='txtTaxPer'  value=" + obj.DeliveryDetail[i].TaxP + " /></td>" +
                              "<td><input type='text' id='txtExcise" + i + "'   counter='" + i + "'  readonly=readonly   class='form-control input-small  validate' name='txtExcise'   value=" + obj.DeliveryDetail[i].Excise + " ></td>";



                           tr = tr + "<td stytle='color:white'><div id='btnAddRow" + i + "' style='cursor:pointer'  counter='" + i + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

                           tr = tr + "<td><div id='btnRemove" + i + "'  style='cursor:pointer' class='cls_remove' counter='" + i + "'><i class='fa fa-remove'></i></div> </td></tr>";

                           $("#tbProducts").append(tr);

                           $("#txtServiceId" + i + "").focus();

                           $("#ddlProducts" + i + "").append(DropDownData);
                           var itemid = $('#ddlProducts' + i + ' option[item_code=' + obj.DeliveryDetail[i].Item_Code + ']').attr("value")
                           $("#ddlProducts" + i + "").val(itemid);
                       }
                       $("#hdnCounter").val(obj.DeliveryDetail.length);
                  
                       BindRows();

                       $("#KitDialog").dialog({ autoOpen: true,

                           width: 860,
                           resizable: false,
                           modal: true
                       });


                       CommonCalculation();
                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {


                       if ($("#hdnpassing").val() == "true") {

                           $("#btnSave").attr('disabled', 'disabled');

                       }
                       else {

                           $("#btnSave").removeAttr('disabled');

                       }



                       $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
      

                       if ($("#rdbcfinished").prop("checked") == true) {
                           ItemType = "3";
                       }
                       else if ($("#rdbraw").prop("checked") == true) {
                           ItemType = "1";
                       }
                       else if ($("#rdbsemi").prop("checked") == true) {
                           ItemType = "5";
                       }


                       var Excise = "0";
                       if ($("#rdbExcise").prop("checked") == true) {
                           Excise = "1";

                       }
                       else if ($("#rdbnonexcise").prop("checked") == true) {
                           Excise = "2";
                       }

                       else if ($("#rdballexcise").prop("checked") == true) {
                           Excise = "2";
                       }


                       $("#ddlProducts").supersearch({
                           Type: "Product",
                           Caption: "Please enter Item Name/Code ",
                           AccountType: "",
                           Godown: $("#ddlGodown").val(),
                           ItemType: ItemType,
                           Excise: Excise,
                           Width: 214,
                           DefaultValue: 0
                       });

                   }
               });



                        });


		var curcounter = 0;
		function BindGridProducts(stext, counterId) {
			curcounter = counterId;
			var list = $("#jQGridProduct");
			jQuery("#jQGridProduct").GridUnload();

			jQuery("#jQGridProduct").jqGrid({
				url: 'handlers/CProductsSearch.ashx?stext=' + stext,
				ajaxGridOptions: { contentType: "application/json" },
				datatype: "json",

				colNames: ['Code', 'Name', 'Cost', 'Tax', 'TaxId', 'MRP', 'Hsn', 'Unit'
				],
				colModel: [
					{ name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, hidden: false },
					{ name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, hidden: false },
					{ name: 'Sale_Rate', index: 'Sale_Rate', width: 50, stype: 'text', sortable: true, hidden: false },
					{ name: 'Tax_Code', index: 'Tax_Code', width: 50, stype: 'text', sortable: true, hidden: false },
					{ name: 'Tax_ID', index: 'Tax_ID', width: 50, stype: 'text', sortable: true, hidden: true },
					{ name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 50, stype: 'text', sortable: true, hidden: false },
					{ name: 'hsncode', index: 'hsncode', width: 50, stype: 'text', sortable: true, hidden: true },
					{ name: 'Sales_In_Unit', index: 'Sales_In_Unit', width: 50, stype: 'text', sortable: true, hidden: true },

				],
				//rowNum: 10,
				mtype: 'GET',
				//loadonce: true,
				//toppager: true,
				//rowList: [10, 20, 30],
				//pager: '#jQGridProductPager',
				sortname: 'Item_Code',
				viewrecords: true,
				height: "100%",
				width: "800px",

				sortorder: 'desc',
				caption: "",
				editurl: 'handlers/CProductsSearch.ashx',

				//toolbar: [true, "top"],
				ignoreCase: true,



			});

			//$("#jQGridProduct tr:nth-child(2)").attr('aria-selected', true);

			//$("#jQGridProduct tr:nth-child(2)").addClass('ui-state-highlight');



			var $grid = $("#jQGridProduct");
			// fill top toolbar
			//$('#t_' + $.jgrid.jqID($grid[0].id))
			//	.append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchTextItem\" type=\"text\"></input>&nbsp;<button id=\"globalSearchItem\" type=\"button\">Search</button></div>"));
			//$("#globalSearchTextItem").keypress(function (e) {
			//	var key = e.charCode || e.keyCode || 0;
			//	if (key === $.ui.keyCode.ENTER) { // 13
			//		$("#globalSearchItem").click();
			//	}
			//});
			//$("#globalSearchItem").button({
			//	icons: { primary: "ui-icon-search" },
			//	text: false
			//}).click(function () {
			//	var postData = $grid.jqGrid("getGridParam", "postData"),
			//		colModel = $grid.jqGrid("getGridParam", "colModel"),
			//		rules = [],
			//		searchText = $("#globalSearchTextItem").val(),
			//		l = colModel.length,
			//		i,
			//		cm;
			//	for (i = 0; i < l; i++) {
			//		cm = colModel[i];
			//		if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
			//			rules.push({
			//				field: cm.name,
			//				op: "cn",
			//				data: searchText
			//			});
			//		}
			//	}
			//	postData.filters = JSON.stringify({
			//		groupOp: "OR",
			//		rules: rules
			//	});
			//	$grid.jqGrid("setGridParam", { search: true });ss
			//	$grid.trigger("reloadGrid", [{ page: 1, current: true }]);
			//	return false;
			//});




			$("#jQGridProduct").jqGrid('setGridParam',
				{
					ondblClickRow: function (rowid, iRow, iCol, e) {

						var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');

                        var ItemId = $('#ddlProducts' + counterId + ' option[item_code=' + serviceId + ']').attr("value");
                       
                        $('#ddlProducts' + counterId + '  option[value=' + ItemId + ']').prop('selected', 'selected');
                        FillData(counterId, ItemId);

						

						$("#ItemGrid1").dialog("close");



					}
				});





			//$('#jQGridProduct').jqGrid('navGrid', '#jQGridProductPager',
			//	{
			//		refresh: false,
			//		edit: false,
			//		add: false,
			//		del: false,
			//		search: true,
			//		searchtext: "Search",
			//		addtext: "Add",
			//	},

			//	{//SEARCH
			//		//closeOnEscape: true

			//	});

			list.jqGrid('gridResize');
			list.jqGrid('bindKeys');

			var DataGrid = jQuery('#jQGridProduct');


			DataGrid.jqGrid('setGridWidth', '400');
			DataGrid.jqGrid('setSelection', 1, true);
			//list.setSelection("selectRow", 0);



		}

		var selrowf = true;

		var isfirst = 0;
		var isupfirst = 0;
		var currow = 0;


		$(document).on("keydown", "#jQGridProduct", function (e) {



			if (e.keyCode == 40) {



				//arrow("next");

				if (selrowf == true) {

					$("#jQGridProduct").setSelection(1);
					selrowf = false;

				}
				var list = $('#jQGridProduct'),

					$td = $(e.target).closest("tr.jqgrow>td"),
					p = list.jqGrid("getGridParam"),
					//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
					cm = "Item_Code";

				var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


				var selectedRow = list.jqGrid('getGridParam', 'selrow');


				if (isfirst == 0) {

					selectedRow = selectedRow - 1;

				}


				if (selectedRow == null) return;

				var ids = list.getDataIDs();
				var index = list.getInd(selectedRow);

				if (ids.length < 2) return;
				index++;

				list.setSelection(ids[index - 1], false, e);
				currow = index;

				var rows = document.querySelectorAll('#jQGridProduct tr');

				var line = document.querySelector(1);



				rows[line].scrollTop({
					behavior: 'smooth',
					block: 'nearest'
				});

				//                  var w = $(window);

				//                  var row = $('#jQGridProduct').find('tr').eq(line);

				//                  if (row.length) {

				//                      w.scrollTop(row.offset().top - (w.height / 2));
				//}
				e.preventDefault();




			}

			if (e.keyCode == 38) {


				//arrow("prev");
				var list = $('#jQGridProduct'),

					$td = $(e.target).closest("tr.jqgrow>td"),
					p = list.jqGrid("getGridParam"),
					//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
					cm = "Item_Code";

				var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


				var selectedRow = list.jqGrid('getGridParam', 'selrow');


				if (isupfirst == 0) {
					selectedRow = Number(selectedRow) + Number(1);

				}

				if (selectedRow == null) return;
				var ids = list.getDataIDs();

				var index = list.getInd(selectedRow);

				if (ids.length < 2) return;
				index--;

				list.setSelection(ids[index - 1], false, e);
				currow = index;

				var rows = document.querySelectorAll('#jQGridProduct tr');

				var line = document.querySelector(1);



				rows[line].scrollTop({
					behavior: 'smooth',
					block: 'nearest'
				});

				//var w = $(window);
				//var row = $('#jQGridProduct').find('tr').eq(line);

				//                  if (row.length) {
				//                      list.scrollTop(row.offset().top + (12/ 2));
				//}
				e.preventDefault();


			}

			if (e.ctrlKey && e.keyCode == 13) {

				var rowid = currow;
				
				var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');

				var ItemId = $('#ddlProducts' + curcounter + ' option[item_code=' + serviceId + ']').attr("value");

                $('#ddlProducts' + curcounter + '  option[value=' + ItemId + ']').prop('selected', 'selected');
                FillData(curcounter, ItemId);



				
				$("#ItemGrid1").dialog("close");


				$("#txtQty" + curcounter).focus();

				event.stopPropagation();
				isfirst = 1;
				isupfirst = 1;
				selrowf = true;
			}


			// }

		});



        function CommonCalculation() {
            var TotalAmount = 0;
            var ExciseAmount = 0;
            var html = "";
            var SaleRate = 0;
            var Excise = 0;
            var counter_id = "";
            $(".cls_ddlproduct").each(function () {
                 counter_id = $(this).attr('counter');
                 var itemcode = $(this).val();
                 if (itemcode!="") {

               
                if (BillBasicType == "I") {
                    Excise = ProductCollection[i]["Excise"];

                    SaleRate = Number($("#txtMRP" + counter_id + "").val()) - (Number(($("#txtMRP" + counter_id + "").val()) * Number($("#txtTaxPer" + counter_id + "").val())) / (100 + Number($("#txtTaxPer" + counter_id + "").val())));
                    SaleRate = SaleRate - ((Number(SaleRate) * Number(Excise)) / (100 + Number(Excise)));
                    SaleRate = (SaleRate * (100 - Number($('#ddlProducts' + counter_id + ' option[value=' + itemcode + ']').attr("abatement"))) / 100);




                }
                else {

                    SaleRate = (Number($("#txtRate" + counter_id + "").val()) * (100 - Number($('#ddlProducts' + counter_id + ' option[value=' + itemcode + ']').attr("abatement"))) / 100);


                }

                TotalAmount += parseFloat($("#txtAmount" + counter_id + "").val());


                ExciseAmount += parseFloat(((($("#txtExcise" + counter_id + "").val()) * (Number($("#txtRate" + counter_id + "").val()))) / 100))
                 }

            });

            $("#txtBillval").val(TotalAmount.toFixed(2));
            if ($("#rdbnonexcise").prop("checked") == true) {
                $("#txtExcise").val("0");
            }
            else {
                $("#txtExcise").val(ExciseAmount.toFixed(2));
            }
            var DisPer = "0";
            var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
            $("#txtDisPer").val(DisPer);
            $("#txtDisAmt").val(DisAmt);
            $("#txtAdj").val("0");

            $("#txtnetAmt").val(Number(TotalAmount.toFixed(2)) - Number(DisAmt.toFixed(2)) + Number($("#txtExcise" + counter_id + "").val()));
    
           
        }
        $(document).on('click', '.fa-plus', function (e) {
            addTR();
        });

        $("#closetast3").click(function () {

            $("#dvlast3Dialog").hide();


        });
        $(document.body).on('click', '.btn_select', function (e) {
            var currentRow = $(this).closest("tr");
            var mrp = currentRow.find(".cls_mrp").text();
            $("#txtMRP" + global_counterId + "").val(mrp);
            global_counterId = "";
            $("#dvlast3Dialog").hide();
        });
        $(document).on('click', '.cls_remove', function (e) {
            var counter_ = $(this).attr('counter');
            var len = $("input[name='txtServiceId']").length;
            if (len == 1) {
                alert("Row deletion failed. Package must contain atleast one service");
                return;
            }
            var tr = $("#btnRemove" + counter_).closest("tr");
            tr.remove();
            CommonCalculation();
      


        });
        $(".chk_itemtype").change(function () {


            BindProductDropDown($(this).val());

            $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            OnLoadContent();
        });
        $(document).on('change', '.cls_ddlproduct', function (e) {
            var _counter = $(this).attr('counter');
            global_counterId = _counter;
            var ItemId = $("#ddlProducts" + _counter).val();
            FillData(_counter, ItemId);
            CommonCalculation();
        });
        $(document).on('keypress', '.cls_itemcode', function (e) {
            if (e.which == 13) {
                GetfromStock($(this).val());
                var _counter = $(this).attr('counter');

                global_counterId = _counter;
                var ItemId = $('#ddlProducts' + _counter + ' option[item_code=' + $(this).val() + ']').attr("value");
                $('#ddlProducts' + _counter + '  option[value=' + ItemId + ']').prop('selected', 'selected');
                FillData($(this).attr('counter'), ItemId);
                
			
            }

        });
        function FillData(counter_id,item_id) {

            
            var ItemCode = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("item_code");
  
            var Rate = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("sale_rate");
            var MRP = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("mrp");
            var Tax = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("tax_code");

            var StockQty = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("stock_qty");
            var Abatement = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("Abatement");
            var Excise = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("Excise");
            var ExciseId = $('#ddlProducts' + counter_id + ' option[value=' + item_id + ']').attr("ExciseId");


            $("#txtServiceId" + counter_id).val(ItemCode);
            $("#txtRate" + counter_id).val(Rate);
            $("#txtMRP" + counter_id).val(MRP);
            $("#txtstock" + counter_id).val(StockQty);
            $("#txtTaxPer" + counter_id).val(Tax);
            $("#txtFree" + counter_id).val(Excise);
           

        }
        function addTR() {
            var qtyVal = $("#txtQty" + $("#hdnCounter").val()).val();


            var counterId = Number($("#hdnCounter").val()) + 1;
            $("#hdnCounter").val(counterId);
            var tr = "";


            tr = "<tr><td><input type='text'  id='txtServiceId" + counterId + "'  counter='" + counterId + "'  class='form-control input-small cls_itemcode' name='txtServiceId' /></td>" +
				"<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' counter='" + counterId + "'  class='cls_ddlproduct' disabled='disabled'></select></td>" +

                "<td><input type='text' id='txtQty" + counterId + "'  counter='" + counterId + "'  class='form-control input-small cls_qty' name='txtQty'  value = '0'/></td>" +

                 "<td><input type='text' id='txtRate" + counterId + "'  counter='" + counterId + "' readonly=readonly   class='form-control input-small  validate' name='txtRate'   value = '0.0'/></td>" +


                "<td><input type='text' id='txtMRP" + counterId + "'    counter='" + counterId + "' readonly=readonly  class='form-control input-small  validate float' name='txtMRP1'  value = '0.0'/></td>" +

               "<td><input type='text' id='txtAmount" + counterId + "'  counter='" + counterId + "' readonly=readonly   class='form-control input-small' name='txtAmount'  value='0.0' /></td>" +
               "<td><input type='text' id='txtDis3Per" + counterId + "'    counter='" + counterId + "'   readonly=readonly  class='form-control input-small  validate' name='txtDis1Per'  value='0.0' /></td>" +
               "<td><input type='text' id='txtstock" + counterId + "'   counter='" + counterId + "'  readonly=readonly  class='form-control input-small  validate' name='txtstock'  value='0.0' /></td>" +
               "<td><input type='text' id='txtTaxPer" + counterId + "'   counter='" + counterId + "' readonly=readonly  readonly=readonly class='form-control input-small  validate float' name='txtTaxPer'  value = '0.0'/></td>" +
               "<td><input type='text' id='txtExcise" + counterId + "'   counter='" + counterId + "'  readonly=readonly   class='form-control input-small  validate' name='txtExcise'  value ='0.0' /></td>";



            tr = tr + "<td stytle='color:white'><div id='btnAddRow" + counterId + "' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

            tr = tr + "<td><div id='btnRemove" + counterId + "'  style='cursor:pointer' class='cls_remove' counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>";

            $("#tbProducts").append(tr);
      
            $("#txtServiceId" + counterId + "").focus();
        
            $("#ddlProducts" + counterId + "").append(DropDownData);


        }

        $("#btnSave").click(function () {
            var flag = 0;
 
            $(".cls_qty").each(function () {
                var counter_id = $(this).attr('counter');
              var _Itemid = $("#ddlProducts" + counter_id).val();
              if (Number($(this).val()) < 1 && _Itemid != "" && counter_id != undefined)
                {
                    flag = 1;

                }

            });

            if (flag==1) {
                alert("Qty Should not be 0 !")
                flag = 0;
                return false;
            }
            $(".cls_ddlproduct").each(function () {
                counter_id = $(this).attr('counter');
                BindGridData(counter_id);
            });
            InsertUpdate();
        });



        function ResetControls() {

            m_KitID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtddlItems").val("").removeAttr("disabled");
            $("#ddlItems").html("<option value='0'></option>")
            $("#txtGrNo").val("");
            $("#txtVehNo").val("");
            $("#ddlDealer").val("0");

            $("#ddlRefno").val("0");
            $("#txtBillval").val("");
            $("#txtDisAmt").val("");
            $("#txtAdj").val("");
            $("#txtnetAmt").val("");
            $("#txtExcise").val("");
            ProductCollection = [];
           // $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            
       
        }

		function CheckItemCode(ItemCode, counterId) {
			$.ajax({
				type: "POST",
				data: '{"ItemCode": "' + ItemCode + '"}',
				url: "BillScreenOption.aspx/ChkItemCode",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

					var isexist = obj.rtnval;

					if (isexist == 1) {

						BindGridProducts(ItemCode, counterId);
						$("#ItemGrid1").dialog({
							modal: true,
							width: 450
						});

						$("#ItemGrid1").parent().addClass('Itemgrid-pop');


					}
					else {

						alert("Item Does not exists");
						return;
					}

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


					$.uiUnlock();
				}

			});

		}


        function GetfromStock(itemcode) {
            
            $.ajax({
                type: "POST",
				data: '{ "ItemCode": "' + itemcode + '", "billtype": "' + 0 + '"}',
				url: "BillScreenOption.aspx/GetByItemCode",
                contentType: "application/json",

                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    if (obj.productData.Item_Name == "") {



                        CheckItemCode(itemcode, global_counterId);


                        return;

                    }
                    

                    //else {
                    //    ddlServiceVal = itemcode;
                    //    $("#ddlProducts" + global_counterId + " option").removeAttr("selected");
                    //    $('#ddlProducts' + global_counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');

                    //    cost = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("cost");

                    //    amount = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("cost");

                    //    tax = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("tax");

                    //    mrp = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("mrp");

                    //    srate = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("srate");

                    //    unit = $('#ddlProducts' + global_counterId + ' option[value=' + itemcode + ']').attr("unit");
                     
                    //    $("#txtAmount" + global_counterId).val(amount);
                    //    $("#txtRate" + global_counterId).val(cost);
                    //    $("#txtQty" + global_counterId).val("1");
                    //    $("#txtMRP" + global_counterId).val(mrp);
                    //    $("#txtSRate" + global_counterId).val(srate);
                    //    $("#txtTaxPer" + global_counterId).val(tax);
                    //    $("#txtFree" + global_counterId).val("0");
                    //    $("#txtDis1Per" + global_counterId).val("0.0");
                    //    $("#txtDis2Per" + global_counterId).val("0.0");
                    //    $("#txtUnit" + global_counterId).val(unit);

                    //    $("#txtServiceId" + global_counterId).val(ddlServiceVal);

                    //    if ($("#ddlProducts" + global_counterId).val() != "") {
                    //       // $("#txtQty" + global_counterId).focus();
                    //    }
                    //}



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }




            });

        }

        function InsertUpdate() {

            if ($("#ddlBranch").val() == "0") {
                alert("Please Choose Branch");
                $("#ddlBranch").focus();
                return;
            }


            if ($("#ddlToBranch").val() == "0") {
                alert("Please Choose Branch for which Branch you are creating Delivery Note");
                $("#ddlToBranch").focus();
                return;
            }

            if ($("#ddlRefno").val() == "0") {
                alert("Please Choose Form Type");
                $("#ddlRefno").focus();
                return;
            }


            if (ProductCollection.length == 0) {

                alert("Please choose Delivery Items");
                $("#ddlProducts").focus();
                return;
            }




            var objDelivery = {};



            objDelivery.Bill_No = $("#txtBillNo").val();

            if (objDelivery.Bill_No == "Auto") {
                objDelivery.Bill_No = "0"
            }

            objDelivery.ForBranch = $("#ddlToBranch").val();
            objDelivery.Bill_Date = $("#txtBillDate").val();
            objDelivery.GR_No = $("#txtGrNo").val();
            objDelivery.GR_Date = $("#txtDate").val();
            objDelivery.Veh_No = $("#txtVehNo").val();
            objDelivery.Dis1InRs = "false";
            objDelivery.Dis2InRs = "false";
            objDelivery.Dis2AftDedDis1 = "false";
            objDelivery.LocalOut = "Out";
            objDelivery.TaxAfterDis1 = "true";
            objDelivery.TaxAfterDis2 = "true";
            objDelivery.Remarks = $("#txtremarks").val();
            objDelivery.Bill_Value = $("#txtBillval").val();
            objDelivery.Dis1Amt = 0.00;
            objDelivery.Dis2Amt = 0.00;
            objDelivery.Dis3AftDis1PDis2 = "False";
            objDelivery.Dis3P = $("#txtDisPer").val();
            objDelivery.Dis3Amt = $("#txtDisAmt").val();
            objDelivery.TaxP = 0.00;
            objDelivery.TaxAmt = 0.00;
            objDelivery.Total_Amount = $("#txtnetAmt").val();
            objDelivery.ODisP = 0.00;
            objDelivery.ODisAmt = 0.00;
            objDelivery.Display_Amount = 0.00;
            objDelivery.Adjustment = $("#txtAdj").val();
            objDelivery.Net_Amount = $("#txtnetAmt").val();
            objDelivery.CCODE = $("#ddlToBranch").val();

            //objDelivery.CCODE = "0";
            objDelivery.Godown_ID = $("#ddlGodown").val();
            objDelivery.Form_Name = $("#ddlRefno").val();
            objDelivery.Desp_Date = $("#txtDispDate").val();
            objDelivery.Ref_No = 0;
            objDelivery.UpdateMaster = "true";
            objDelivery.PASSING = "false";
            objDelivery.ChkRaw = "GEN";
            objDelivery.dr_grno = 0;
            objDelivery.delchk = "false";
            objDelivery.Excise_Amt = $("#txtExcise").val();

            objDelivery.EduCess = 0.00;
            objDelivery.HSCess = 0.00;
            objDelivery.BillMode = "1";
            var Type = "0";
            if ($("#rdbExcise").prop("checked") == true) {
                Type = "1";

            }
            else if ($("#rdbnonexcise").prop("checked") == true) {
                Type = "0";
            }

            else if ($("#rdballexcise").prop("checked") == true) {
                Type = "2";
            }

            objDelivery.Type = Type;



            var Excise = false;
            if ($("#rdbExcise").prop("checked") == true) {
                Excise = true;
            }
            else if ($("#rdbnonexcise").prop("checked") == true) {
                Excise = false;
            }

            var DTO = { 'objDeliveryDetails': ProductCollection, 'objDeliveryMaster': objDelivery, 'Excise': Excise };



            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/InsertUpdate",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (m_KitID == 0) {
                        alert("Delivery Note Added Successfully");
                    }
                    else {
                        alert("Delivery Note Updated Successfully");
                    }
                    BindGrid();
                    ResetControls();
                

                    $("#KitDialog").dialog("close");


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });

            }
        // register jQuery extension
        jQuery.extend(jQuery.expr[':'], {
            focusable: function (el, index, selector) {
                return $(el).is('a, button, :input, [tabindex]');
            }
        });
        var counter_cant = 0;
        $(document).on('keypress', 'input,select', function (e) {
            var focus_on;
            var counter;
            $(":focus").each(function () {
                focus_on = this.name;
                try {
                    counter = this.attributes.counter.value;
                } catch (e) {
                    counter = "";
                }

            });

			if (focus_on == "txtServiceId") {
				if (e.which == 13) {

                    $("#txtQty" + counter).focus();
                    $("#txtQty" + counter).select();
				}

            }

			
			if (focus_on == "txtQty") {
				if (e.which == 13) {

                    addTR();
				}

			}
            //if (focus_on != "txtServiceId") {
            //    if (e.which == 13) {
            //        e.preventDefault();
            //        // Get all focusable elements on the page
            //        var $canfocus = $(':focusable');
            //        var index = $canfocus.index(document.activeElement) + 1;
            //        if (index >= $canfocus.length) index = 0;
            //        $canfocus.eq(index).focus();
            //        $canfocus.eq(index).select();
            //        if (focus_on == "txtTaxPer") {
            //            if ($("#tbProducts tr").length == counter) {

            //                addTR();
            //            }
            //            $("#txtServiceId" + counter + 1 + "").select();
            //        }
            //    }
            //}
        });
        function BindProductDropDown(_itemtype) {


                 var Godown = 1;
  
                 $.ajax({
                     type: "POST",
                     contentType: "application/json; charset=utf-8",
                     url: "managedeliverynote.aspx/BindProductDropDown",
                     data: '{"GodownId": "' + Godown + '","ItemType": "' + _itemtype + '","Excise": "' + 2 + '"}',
                     dataType: "json",
                     async:false,
                     success: function (msg) {

                         var obj = jQuery.parseJSON(msg.d);

                         DropDownData = obj.Options;

                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                   
                     },
                     complete: function () {


                     }
                 });


             }
        function BindBranches() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/BindBranches",
                data: {},
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html = "<option value = 0>--SELECT--</option>";


                    for (var k = 0; k < obj.BranchLogOptions.length; k++) {

                        html = html + "<option value='" + obj.BranchLogOptions[k]["BranchId"] + "'>" + obj.BranchLogOptions[k]["BranchName"] + "</option>";
                    }

                    $("#ddlToBranch").html(html);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }
            });

        }



        function BindDealer() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/BindDealers",
                data: {},
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html = "<option value = 0>--SELECT--</option>";
                    var html1 = "<option value = 0>--SELECT--</option>";
                    var html2 = "";
                    for (var i = 0; i < obj.DealerOptions.length; i++) {

                        html = html + "<option value='" + obj.DealerOptions[i]["CCODE"] + "' Dis ='" + obj.DealerOptions[i]["DIS_PER"] + "'>" + obj.DealerOptions[i]["CNAME"] + "</option>";
                    }

                    for (var j = 0; j < obj.GodownOptions.length; j++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[j]["Godown_Id"] + "'>" + obj.GodownOptions[j]["Godown_Name"] + "</option>";
                    }

                    for (var k = 0; k < obj.BranchOptions.length; k++) {

                        html2 = html2 + "<option value='" + obj.BranchOptions[k]["BranchId"] + "'>" + obj.BranchOptions[k]["BranchName"] + "</option>";
                    }
                    $("#ddlDealer").html(html);
                    $("#ddlGodown").html(html1);
                    $("#ddlBranch").html(html2);
                    $("#ddlBranch").prop("disabled", true);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {




                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {



                            $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
                            var ItemType = "";

                            if ($("#rdbcfinished").prop("checked") == true) {
                                ItemType = "3";
                            }
                            else if ($("#rdbraw").prop("checked") == true) {
                                ItemType = "1";
                            }
                            else if ($("#rdbsemi").prop("checked") == true) {
                                ItemType = "5";
                            }



                            var Excise = "2";
                            //                            if ($("#rdbExcise").prop("checked") == true) {
                            //                                Excise = "1";

                            //                            }
                            //                            else if ($("#rdbnonexcise").prop("checked") == true) {
                            //                                Excise = "2";
                            //                            }
                            //                            else if ($("#rdballexcise").prop("checked") == true) {
                            //                                Excise = "2";
                            //                            }



                            $("#ddlProducts").supersearch({
                                Type: "Product",
                                Caption: "Please enter Item Name/Code ",
                                AccountType: "",
                                Godown: $("#ddlGodown").val(),
                                ItemType: ItemType,
                                Excise: Excise,
                                Width: 214,
                                DefaultValue: 0
                            });


                        }

                    });



                }
            });

        }


    });
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>



  <div id="ItemGrid1" title="Press CTRL+ENTER To Select Item" style="display:none">
						
							<table id="jQGridProduct">
												</table>
												<div id="jQGridProductPager">
												</div>
						</div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                           <table class="manage_table_top">
                                <tr>
                              
                                <td style="padding:0px;">
                                    <div class="x_panel" >
                                <div class="x_title">
                                  <%--  <h2>Dealer Billing</h2>--%>
                                  <h2>Dealer Billing</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 <div class="x_content">
                                    
                                       <table>

                                     
                                     <tr>
                                        <td><label class="control-label">STN No</label></td>
                                        <td><input type="text" class="form-control" id="txtBillNo" readonly ="readonly" data-bind="value:BillNo" ></td>
                                         <td ><label class="control-label">STN Date</label></td>
                                        <td> <input type="text" class="form-control" id="txtBillDate" data-bind="value:TotalAmount" ></td>
                                        <td> <label class="control-label">Order Date</label></td>
                                        <td><input type="text" class="form-control" id="txtOrderDate" data-bind="value:GrNo" ></td>
                                        <td ><label class="control-label">Disp Date</label></td>
                                        <td> <input type="text" class="form-control" id="txtDispDate" data-bind="value:TotalAmount" ></td>
                                        
                                        
                                        
                                        <td><label class="control-label">Date</label></td>
                                        <td><input type="text" class="form-control" id="txtDate"></td>
                                     </tr>

                                    <tr>
                                        <td> <label class="control-label">Gr No</label></td>
                                        <td><input type="text" class="form-control" id="txtGrNo" data-bind="value:GrNo" class="validate required" ></td>
                                        <td><label class="control-label">Veh No.</label></td>
                                        <td><input type="text" class="form-control" id="txtVehNo"></td>
                                       <%-- <td><label class="control-label">Dispatch Time</label></td>--%>
                                       
                                        <%--<td><cc1:TimeSelector ID="tsDispatch" runat="server"  DisplaySeconds="false"></cc1:TimeSelector></td>--%>
                                    </tr>
                                    </table>
                                   <table id="tblitem_type"> <tr>
                                    <td><input type="radio" id="rdbcfinished"  class="chk_itemtype" value="3" name="cash" /> <label class="control-label" for="rbLocal"> Finished</label></td>
                                        <td><input type="radio" id="rdbraw" class="chk_itemtype" value="1" name="cash" checked="checked"/> <label class="control-label" for="rbLocal"> Raw</label></td>
                                        <td><input type="radio" id="rdbsemi"  class="chk_itemtype" value="5" name="cash" /> <label class="control-label" for="rbLocal"> Semi-Finished</label></td>
                                         <td><input type="radio" id="rdball"  class="chk_itemtype" value="-1" name="cash" /> <label class="control-label" for="rbLocal"> All Items</label></td>
                                    <td><input type="radio" id="rdbExcise" style="visibility:hidden" name="Excise" /> <label class="control-label" style="visibility:hidden" for="rdbExcise" >Excisable Items</label></td>
                                        <td><input type="radio" id="rdbnonexcise" style="visibility:hidden" name="Excise" checked="checked" /> <label class="control-label" style="visibility:hidden" for="rdbnonexcise"> Non-Exciseable Items</label></td>
                                        <td><input type="radio" id="rdballexcise" style="visibility:hidden" name="Excise" checked="checked" /> <label class="control-label" style="visibility:hidden" for="rdballexcise"> All Items</label></td>
                                        <td></td>
                                                <td style="float:right;">   
                                                <label class="control-label">To Branch</label>   
                                                <select class="control-label" id="ddlToBranch">
                                                         <option value="0"></option>
                                                         </select>
                                                </td>
                                    </tr>
                               </table>

                                </div>
                                
                            </div></td>
                                </tr>
                                </table>

                                       <table class="manage_table_top" style="display:none;">
                                            <tr>
                                                <td><label class="control-label" style="display:none">Dealer</label></td>
                                                <td><select id="ddlDealer" style="width:100px;display:none">
                                                     <option value="0"></option>
                                                     </select>
                                               </td>
                                               <td><label class="control-label">Branch</label></td>
                                                <td>      
                                                <select id="ddlBranch">
                                                         <option value="0"></option>
                                                         </select>
                                                </td>
                                               <td> <label class="control-label">Godown</label></td>
                                                <td>      
                                                <select id="ddlGodown">
                                                         <option value="0"></option>
                                                         </select>
                                               </td>
                                              <td> <label class="control-label">Form</label></td>
                                                <td>      
                                                 <select id="ddlRefno">
                                                       <%-- <option value="0">--SELECT--</option>--%>
                                                        <option value="NOT AGAINST FORM">NOT AGAINST FORM</option>
                                                         <option value="C FORM">C FORM</option>
                                                         <option value="F FORM">F FORM</option>
                                                        
                                                         </select>
                                               </td>
                                              
                                          </tr>
                                  </table>

                                                <div class="fix-height-delivery-note">          
                                                    <table class="manage_table_top" style="margin: 0px;" id="tbProducts">
                                                        <thead>
                                                            <tr>
                                                                <th>Code</th>
                                                                <th>Product</th>
                                                                <th>Qty</th>
                                                                <th>Rate</th>
                                                                <th>MRP</th>
                                                                <th>Amount</th>
                                                                <th id="th1">Dis3</th>
                                                                <th>Stock</th>
                                                                <th>Tax</th>
                                                                <th>Excise</th>
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                       <div>
                                                          
                                                            <div id="dvlast3Dialog" style="position: fixed; display: none; background-color: rgb(76, 73, 77); color: white; border: solid 1px silver; border-radius: 10px; padding: 5px;">

                                                                <table class="table">
                                                                    <tr>
                                                                        <td colspan="8" style="text-align: center; font-weight: bold; text-decoration: underline; background-color: #1479b8;border-bottom: 1px solid #ddd; padding-top:10px; padding-bottom:10px;">Select Item From List</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th>Sr.no.</th>
                                                                        <th>Item Code</th>
                                                                        <th>MRP</th>
                                                                          <th>Qty</th>
                                                                        <th>Sale Rate</th>
                                                                              <th>Tax</th>
                                                                              <th>Unit</th>
                                                                      
                                                                      
                                                                    </tr>
                                                                    <tbody id="tbllastrec"></tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <button type="button" id="closetast3" class="btn btn-danger">Close</button>

                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>

                                                        </div>
                    <%--           <table class="manage_table_top">

                                    <thead>
                                    <tr>

                                    <th>Item/Code</th>
                                    <th>Code</th>
                                    <th>Name</th>
                                    <th>Stock</th>
                                    <th>Qty</th>
                                    <th>Rate</th>
                                    <th>MRP</th>
                                    <th>Amount</th>
                                        <th>ADD</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td id="DKID">
                                        <select id="ddlProducts" class="form-control"></select>
                                        </td>
                                        <td>
                                        <input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox" style="width:60px;" />
                                        </td>
                                        <td><input id="txtName" type="text"  class="form-control customTextBox" / style="width:290px;"></td>
                                        <td><input type="text" id="txtStock" class="form-control customTextBox"  readonly="readonly" style="width:65px;" /></td>
                                        <td><input type="number" id="txtQty" class="form-control customTextBox" style="width:65px;"  /></td>
                                        <td><input type="text" id="txtRate"  class="form-control customTextBox"  style="width:100px;" /></td>
                                        <td><input type="text" id="txtMarketPrice" class="form-control customTextBox" style="width:100px;" /></td>
                                        <td>
                                        <input type="text"  class="form-control customTextBox" readonly="readonly" id="txtAmount" style="width:100px;" /></td>
                                        <td>
                                        <button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td>
                                    </tr>--%>
                                    <%--<button id="Button1" class="btn btn-success" style="width: 58px ; margin-right: -5px; background-color: Black; border-color: white; font-weight: bold; height: 33px; margin-bottom: 0px; margin-top: -1px;" type="button">Add</button>--%>
                          <%--          </tbody>
                              </table>  --%>
                   

                    

                               <div class="col-md-12 col-xs-12 col-sm-12">
                   
                                                              

                                                <table class="manage_table_top">
                                            
                                                    <tr>
                                                        <td><label class="control-label">Remarks</label></td>
                                                        <td rowspan="3"><textarea id="txtremarks"></textarea></td>
                                                        <td><label class="control-label">Estimated Value</label></td>
                                                        <td><input type="text" class="form-control" id="txtBillval" readonly="readonly"></td>
                                                        <td><label class="control-label">Excise</label></td>
                                                        <td><input type="text" class="form-control" id="txtExcise" readonly="readonly"></td>
                                                        <td style="display:none;"><label class="control-label" >Discount</label></td>
                                                        <td style="display:none;"><input type="text" class="form-control" id="txtDisAmt" readonly="readonly"></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                                                                      
                                                        <td><label class="control-label">Adj(-)Less/(+)Add</label></td>
                                                        <td><input type="text" class="form-control" id="txtAdj" readonly="readonly"></td>
                                                        <td><label class="control-label">Net Amount</label></td>
                                                        <td><input type="text" class="form-control" id="txtnetAmt" readonly="readonly" data-bind="value:GrNo"></td>
                                                       
                                                        <td><input type="text" class="form-control" id="txtDisPer" readonly="readonly" style="width:40px;display:none"></td>                                                        
                                                    </tr>
                                                </table>

                                                <div class="deliverynot_btns">
                                                    <div id="btnSave" class="btn btn-primary inv_sub_canc"><i class="fa fa-save"></i> Save</div>
                                                    <button id="btnCancelDialog" class="btn btn-danger inv_sub_canc"> <i class="fa fa-mail-reply-all"></i> Cancel</button>
                                                </div>
                                  



                               

                        </div>
                        </div>
  
