﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="DeliveryReceive.ascx.cs" Inherits="Templates_DeliveryReceive" %>
<script type ="text/javascript">


    $(document).ready(
    function () {
        BindDealer();


        function BindDealer() {


            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "managedeliverynote.aspx/BindDealers",
                data: {},
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    var html = "<option value = 0>--SELECT--</option>";
                    var html1 = "";
                    for (var i = 0; i < obj.DealerOptions.length; i++) {

                        html = html + "<option value='" + obj.DealerOptions[i]["CCODE"] + "' Dis ='" + obj.DealerOptions[i]["DIS_PER"] + "'>" + obj.DealerOptions[i]["CNAME"] + "</option>";
                    }

                    for (var i = 0; i < obj.GodownOptions.length; i++) {

                        html1 = html1 + "<option value='" + obj.GodownOptions[i]["Godown_Id"] + "'>" + obj.GodownOptions[i]["Godown_Name"] + "</option>";
                    }
                    $("#ddlDealer").html(html);
                    $("#ddlGodown").html(html1);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {




                    $.ajax({
                        type: "POST",
                        data: '{ }',
                        url: "managedeliverynote.aspx/GetDefaultGodown",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);



                            $("#ddlGodown option[value='" + obj.DefaultGodown + "']").prop("selected", true);
                            $("#ddlGodown").prop("disabled", true);

                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function () {


                        }

                    });





                }
            });

        }


    }
    );
</script>
<style type="text/css">

.table > tbody > tr > td
{
    padding:4px;
    }

.table > thead > tr > td
{
padding:4px;    
}
</style>
                        <div class="inventry_adddialog" id ="dvDialog">
                                <table class="manage_table_top">
                                           <tr>
                                              <td style="padding:0px;">
                                                  <h2 class="manage_title_top">Receive Delivery Note</h2>
                                              </td>
                                          </tr>
                                           <tr>
                                               <td style="padding:0px;" colspan="100%">
                                                   <table>
                                                      <tr>
                                                          <td  style="text-align: left;"><label class="control-label">Bill No</label></td>
                                                        <td><input type="text" class="form-control" 
                                                            id="txtBillNo" style="width:250px;color:#222" />
                                                        </td>
                                                         <td  style="text-align: left;"> <label class="control-label">Godown</label></td>
                                                        <td><select id="ddlGodown" style="width:250px;" >
                                                            <option value="0"></option>
                                                           </select>
                                                          </td>
                                                             <td><div id="btnAddInfo" class="btn btn-primary" style="margin-top:5px">Add Information</div></td>
                                                        </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                         </table>
                  
                                <div class="x_panel delivery-receive-temo-x-panel">
                                    <%--  <div class="x_title">
                                                                <h2>Kit ITEMS</h2>

                                                                <div class="clearfix"></div>
                                                            </div>--%>
                                            <table class="table table-striped manage_table_top deliver-recieive-code-table">
                                                <thead>
                                                    <tr>
                                                        <th>Code</th>
                                                        <th>Name</th>
                                                        <th style="display:none">CaseQty</th>
                                                        <th>Qty</th>
                                                        <th style="display:none">Scheme</th>
                                                        <th>Rate</th>
                                                        <th>SaleRate</th>
                                                        <th>MRP</th>
                                                        <th>Amount</th>
                                                        <th>Dis3</th>
                                                        <th style="display:none">QTy_To_Less</th>
                                                        <th>Stock</th>
                                                        <th>Tax</th>
                                                        <th>Excise</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tbKitProducts">

                                                </tbody>
                                            </table>
                                </div>

                                <table class="manage_table_top">
                                            <tr>
                                                <td style="vertical-align:top;" colspan="100%">
                                                    <table class="delivry-rec-remark-table">
                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Remarks</label>
                                                            </td>
                                                            <td>
                                                                <textarea id="txtremarks" style="width:100%;"></textarea>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>

                                                <td colspan="100%">
                                                    <table>
                                                        <tr>

                                                            <td>
                                                                <label class="control-label">Bill Value</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" id="txtBillval" readonly="readonly">

                                                            </td>
                                                            <td>
                                                                    <label class="control-label">Excise</label>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" id="txtExciseAmt" readonly="readonly"

                                                                </td>

                                                        </tr>
                                                 <!--       <tr>
                                                                <td>
                                                                    <label class="control-label">Excise</label>
                                                                </td>
                                                                <td>
                                                                    <input type="text" class="form-control" id="txtExciseAmt" readonly="readonly"

                                                                </td>
                                                            </tr> -->
                                                        <tr>
                                                            <td>
                                                                 <label class="control-label">Discount</label>
                                                            </td>
                                                            <td>
                                                              <input type="text" class="form-control" id="txtDisPer" readonly="readonly">
                                                               <input type="text" class="form-control" id="Text1" readonly="readonly">
                                                            </td>
                                                           <!-- <td>
                                                               <input type="text" class="form-control" id="txtDisAmt" readonly="readonly">
                                                            </td>-->
                                                            <td>
                                                                <label class="control-label">Adj(-)Less/(+)Add</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" id="txtAdj" readonly="readonly">
                                                            </td>
                                                        </tr>

                                                    <!--    <tr>
                                                            <td>
                                                                <label class="control-label">Adj(-)Less/(+)Add</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" id="txtAdj" readonly="readonly">
                                                            </td>
                                                        </tr> -->

                                                        <tr>
                                                            <td>
                                                                <label class="control-label">Net Amount</label>
                                                            </td>
                                                            <td>
                                                                <input type="text" class="form-control" id="txtnetAmt" readonly="readonly" data-bind="value:GrNo">

                                                            </td>
                                                        </tr>

                                                    </table>
                                                </td>

                                            </tr>

                                        </table>

                                           <div id="btnSave" data-bind="click: $root.PlaceOrder" class="btn btn-primary inv_sub_canc">Transfer</div>
                                           <button id="btnCancelDialog" class="btn   btn-danger inv_sub_canc" data-bind="click: $root.CancelOrder">Cancel</button>


                        </div>