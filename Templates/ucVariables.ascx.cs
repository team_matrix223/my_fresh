﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Templates_ucVariables : System.Web.UI.UserControl
{
    managesqence ms = new managesqence();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bind_dd_dep();
        }

    }

    public void bind_dd_dep()
    {
        ms.req = "bind_dd";
        DataTable dt = ms.bind_item_dd();
        rptr_ddpos.DataSource = dt;
        rptr_ddpos.DataBind();

    }
}