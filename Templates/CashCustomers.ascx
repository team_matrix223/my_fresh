﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CashCustomers.ascx.cs" Inherits="Templates_CashCustomers" %>

  <link href="css/customcss/managecashcustomers.css" rel="stylesheet" /> 
  
<script language="javascript">

    var m_CustomrId = -1;

    $(document).ready(function () {

       // $("#cm_DOB").val($("#<%=hdnDate.ClientID%>").val());
       // $("#cm_DOA").val($("#<%=hdnDate.ClientID%>").val());
      $("#cm_DOB").val("1900-01-01");
      $("#cm_DOA").val("1900-01-01");
      $("#cm_Discount").val("0");
      $("#cm_Tag").val("0");

      $('#cm_DOB').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1"
      }, function (start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
      });

//      $('#cm_DOB').daterangepicker({
//          singleDatePicker: true,
//          calender_style: "picker_1"
//      }, function (start, end, label) {


//          console.log(start.toISOString(), end.toISOString(), label);
//      });


      $('#cm_DOA').daterangepicker({
          singleDatePicker: true,
          calender_style: "picker_1"
      }, function (start, end, label) {


          console.log(start.toISOString(), end.toISOString(), label);
      });



    });


      function BindInformation(CustomerId) {

          $.ajax({
              type: "POST",
              data: '{"CustomerId":"' + CustomerId + '"}',
              url: "managecashcustomers.aspx/GetByCustomerId",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);
                  m_CustomrId = obj.Customer.Customer_ID;
                  $("#cm_ddlPrefix").val(obj.Customer.Prefix);
                  $("#cm_Name").val(obj.Customer.Customer_Name);
                  $("#cm_Address1").val(obj.Customer.Address_1);
                  $("#cm_Address2").val(obj.Customer.Address_2);
                  $("#cm_DOB").val(obj.Customer.strDOB);
                  $("#cm_DOA").val(obj.Customer.strDOA);
                  $("#cm_Discount").val(obj.Customer.Discount);
                  $("#cm_Tag").val(obj.Customer.Tag);
                  $("#cm_ContactNumber").val(obj.Customer.Contact_No);
                  $("#cm_EmailId").val(obj.Customer.EmailId);
                  $("#txtGSTNo").val(obj.Customer.GSTNo);
                  var city = obj.Customer.City_ID;

                  $("#cm_ddlCities").val(city);

                  var state = obj.Customer.State_ID;
                  $("#cm_ddlState").val(state);
                  var area = obj.Customer.Area_ID;
                  $("#cm_ddlArea").val(area);

                  $('#cm_FOC').prop("checked", false);
                  $('#cm_IsActive').prop("checked", false);
                  var iscredit = obj.Customer.CCODE;
                 
                  if (iscredit != "") {

					  $('#cm_Credit').prop("checked", true);
				  }
                  if (obj.Customer.FocBill == true) {
                      $('#cm_FOC').prop("checked", true);
                  }


                  if (obj.Customer.IsActive == true) {
                      $('#cm_IsActive').prop("checked", true);
                  }

                  $("#CustomerDialog").dialog({ autoOpen: true,
					  closeOnEscape: false,
                      width: 800,
                      resizable: false,
                      modal: true
                  });

              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {
                  $.uiUnlock();
              }
          });


      }


      function GetVMResponse(Id, Title, IsActive, Status, Type) {
          $("#dvVMDialog").dialog("close");
		 
          var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
          if (Type == "Area") {

              $("#cm_ddlArea").append(opt);

          }

          if (Type == "City") {

              $("#cm_ddlCities").append(opt);

          }

          if (Type == "State") {

              $("#cm_ddlState").append(opt);

          }



      }

      function OpenVMDialog(Type) {


          $.ajax({
              type: "POST",
              data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
              url: "managearea.aspx/LoadUserControl",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  $("#dvVMDialog").remove();
                  $("body").append("<div id='dvVMDialog'/>");
                  $("#dvVMDialog").html(msg.d).dialog({ modal: true });
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }
          });

      }
      function isNumberKey(evt) {
          var charCode = (evt.which) ? evt.which : evt.keyCode;
          if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
              return false;

          return true;
      }


    function ClearCustomerDialog() {
        validateForm("detach");
        m_CustomrId = -1;

        $("#cm_ddlPrefix").val();
        $("#cm_Name").val("");
        $("#cm_Address1").val("");
        $("#cm_Address2").val("");
       // $("#cm_DOB").val("");
        //$("#cm_DOA").val("");
        $("#cm_DOB").val("1/1/1");
        $("#cm_DOA").val("1/1/1");
        $("#cm_Discount").val("0");
        $("#cm_Tag").val("0");
        $("#cm_ContactNumber").val("");
        $("#cm_EmailId").val("");
        $("#CustomerDialog").dialog("close");
        $("#txtGSTNo").val("");
        $('#cm_IsActive').prop("checked", true);

        BindGrid();
    }

    function InsertUpdateCustomer() {

        if (!validateForm("formCustomer")) {
            return;
        }

        if ($("#txtGSTNo").val()!="0") {

    
        if ($("#txtGSTNo").val().length < 15) {
            alert("InValid GST No.!")
            return;
        }
        }
        var objCustomer = {};
        objCustomer.Customer_ID = m_CustomrId;
        objCustomer.Prefix = $("#cm_ddlPrefix").val();
        objCustomer.Customer_Name = $("#cm_Name").val();
        objCustomer.Address_1 = $("#cm_Address1").val();
        objCustomer.Address_2 = $("#cm_Address2").val();
        objCustomer.Area_ID = $("#cm_ddlArea").val();
        objCustomer.Pincode = $("#cm_Pincode").val();
         
        objCustomer.City_ID = $("#cm_ddlCities").val();
        objCustomer.State_ID = $("#cm_ddlState").val();
        
            objCustomer.Date_Of_Birth = $("#cm_DOB").val();
       

       
            objCustomer.Date_Anniversary = $("#cm_DOA").val();
       

       



        
        objCustomer.Discount = $("#cm_Discount").val();
        objCustomer.Contact_No = $("#cm_ContactNumber").val();

        objCustomer.EmailId = $("#cm_EmailId").val();
        objCustomer.Tag = $("#cm_Tag").val();
        //objCustomer.GSTNo = $("#txtGSTNo").val();
        var Foc = false;

        if ($("#cm_Tag").val() == "") {
          objCustomer.Tag = '0';
        }
        else {
          objCustomer.Tag = $("#cm_Tag").val();
        }

        if ($("#txtGSTNo").val() == "") {
            objCustomer.GSTNo = '0';
        }
        else {
            objCustomer.GSTNo = $("#txtGSTNo").val();
        }
        if ($('#cm_FOC').is(":checked")) {
            Foc = true;
        }

        objCustomer.FocBill = Foc; 
        var IsActive = false;
        if ($('#cm_IsActive').is(":checked")) {
            IsActive = true;
        }
        var IsCredit = 0;
        if ($('#cm_Credit').is(":checked")) {
            IsCredit = 1;
        }
        objCustomer.IsActive = IsActive;
        objCustomer.IsCredit = IsCredit;
        var DTO = { 'objCustomer': objCustomer };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "managecashcustomers.aspx/InsertUpdateCustomer",
            data: JSON.stringify(DTO),
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {

                    alert("Sorry. Contact Number Already Registered with our Database");
                    $("#cm_ContactNumber").focus();
                    return;
                }
                else {
                    alert("CustomerSaved Successfully");
                    BindCreditCst();
                    ClearCustomerDialog();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


            }

        });


    }
</script>


<style>
    .Customer_Information {
    width: 60%;
    margin: 5px auto !important;
    float: unset;
    box-shadow: 0px 0px 5px #f0667194;
    padding: 0px 25px !important;
    border-radius: 5px;
    background-color: #f1f1f1;
    border: 1px solid #f0667159;}
    .form-control {height: 30px; margin: 0 5px 0px; padding: 0px 6px;}
    .form-group textarea#cm_Address1 {height: 50px; padding: 5px 5px;}
    .form-group .form-control {font-size: 12px; line-height: 10px; border-radius: 5px;}
    .form-group .form-control:focus {border-color: #26b99a;}
    .cashcustomer_btns .btn {border-radius: 5px;}
    .ui-dialog {width: 100% !important;left: 0px !important;}
    .Customer_Information .form-group {width: 100%; float: left;}
    .cashcustomer_btns {width: 100%; float: left; margin: 0px; padding: 10px 0 0; border-top: 1px solid #dde2e8; text-align: center;}
    .Customer_Information table#formCustomer label {text-align: left; margin: 0px; font-size: 13px; font-weight: 600;}
    .cashcustomer_panel {padding: 0px;}
    .cashcustomer_panel .x_title {margin: 0px; padding: 0px;}
    .cashcustomer_panel .x_title h2 {margin: 0px; padding: 5px 15px; width: 100%; float: left; text-align: center;}
    
    @media screen and (max-width:820px) and (min-width:768px)
    {
        .Customer_Information {width: 80%;}
        .Customer_Information .form-group {margin-bottom: 5px;}
        .form-control {height: 26px;}
    }

    @media screen and (max-width:767px) and (min-width:580px)
    {
        .Customer_Information table#formCustomer { margin: 0 auto;}
        .Customer_Information #formCustomer label {padding: 0px;}
        .Customer_Information #formCustomer select {margin-top: 0px; margin-bottom: 0px;}
        .Customer_Information #formCustomer input {margin-top: 0px; margin-bottom: 0px;}
        .Customer_Information {width: 78%; padding: 0px 25px !important;}
    }

</style>
  <asp:HiddenField ID="hdnDate" runat="server"/>

 <div class="x_panel cashcustomer_panel">
  <div class="x_title">
                                    <h2 style="text-decoration:  underline;">Customer Information</h2>
                          <%--          <ul class="nav navbar-right panel_toolbox">
                                        
                                        <li class="dropdown">
                                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        
                                    </ul>--%>
                                    <div class="clearfix"></div>
                                </div>
                  <div class="x_content Customer_Information">
                                    <br>
                     <form class="form-horizontal form-label-left"   >

                          <table width="100%" id="formCustomer">
                                <tr>

                                    <td>

                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Prefix <span>*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-9 col-xs-9">

                                                <asp:DropDownList class="form-control" style="width: 60px;float:  left;" ID="cm_ddlPrefix" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                                <input type="text" class="txt form-control col-md-7 col-xs-12 validate required alphanumeric" placeholder="Customer Name" id="cm_Name" style="float:  left;width: 140px;">

                                            </div>
                                        </div>

                                        <%--                            <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Customer Name <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">

                                                                        </div>
                                                                    </div>--%>

                                            <div class="form-group">
                                                <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Address1 <span>*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-9">

                                                    <textarea class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address1"></textarea>
                                                    <%--   <input type="text" class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address1" style="height: 72px;">--%>
                                                </div>
                                            </div>
                                        
                                                <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-9 col-sm-7 col-xs-9">

                                             <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_ddlState" ClientIDMode="Static"  runat="server" style="width: 91%;"></asp:DropDownList>
                                      

											<div class="city">
												<label for="first-name" class="control-label ">City <span class="required">*</span>    </label>
												<asp:DropDownList  class="form-control " ID="cm_ddlCities" ClientIDMode="Static"  runat="server" style="width:  91%;"></asp:DropDownList>
                                                 
                                            <span class="fa fa-plus city-plus" onclick="javascript:OpenVMDialog('City')"></span>
                                       
                                            </div>

                                               
                                         
                                            </div>
                                        </div>
                                                            
                                            <div class="form-group">
                                                <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Area <span class="required">*</span>
                                                </label>
                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                   <div class="area-div">
                                                 <asp:DropDownList class="form-control col-md-7 col-xs-12" ID="cm_ddlArea" ClientIDMode="Static" runat="server" style="width: 71%;">

                                                 </asp:DropDownList>
                                                    <span class="fa fa-plus area-plus" onclick="javascript:OpenVMDialog('Area')"></span>
                                                   
                                                    </div>
                                             
                                                           
                                                 <div class="city">
													
                                                <label for="first-name" class="control-label">Pincode <span class="required">*</span>
                                                </label>
                                                
                                                <input type="number" class="txt form-control validate required alphanumeric" onkeypress="return isNumberKey(event)" placeholder="Pincode" id="cm_Pincode">
                                                          
                                                </div>
                                            
                                                 </div>      
                                                            
                                                               <%-- <span class="fa fa-plus" onclick="javascript:OpenVMDialog('Area')" style="font-size:18px;cursor:pointer;margin-top:-5px"></span>--%>
                                                            

                                               
                                            </div>

                                         


                                                                                   
                                            <%--    <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">State <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                            <table>
                                                                            <tr>
                                                                                <td> </td>
                                                                                  <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                                                    <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                                                </td>
                                                                            </tr>

                                                                            </table>

                                                                        </div>
                                                                    </div>--%>

                                                <div class="form-group">
                                                    <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Contact<span class="required">*</span>
                                                    </label>
                                                    <div class="col-md-9 col-sm-9 col-xs-9">

                                                        <input type="text" class="txt form-control col-md-7 col-xs-12 validate required valNumber" placeholder="Mobil no" id="cm_ContactNumber" onkeypress="return isNumberKey(event)" maxlength= "10" style="width: 33%;">
                                                        <div class="city">
															 <label for="first-name" class="control-label">Email
                                                    </label>
															
															<input type="text" class="txt form-control" placeholder="Email" id="cm_EmailId" style="width: 202px;">
														</div>

													 </div>
                                                </div>

                                                <%--       <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">EmailId<span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">

                                                                        </div>
                                                                    </div>--%>

                                                    <div class="form-group">
                                                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Discount
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                                            <input type="text" class="txt form-control col-md- col-xs-12 validate  valNumber" onkeypress="return isNumberKey(event)" value="0" id="cm_Discount" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Date Of Birth </span>
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                         <input type="text"   class="form-control col-md-7 col-xs-12" style="width:120px;background-color:White;color:black"  id="cm_DOB" aria-describedby="inputSuccess2Status" />
                                                           <%-- <input type="date" class="txt form-control col-md-7 col-xs-12" id="cm_DOB">--%>
															<div class="city">
																<label for="first-name" class="control-label">Anniversary Date</span>
																</label>
															
																 <input type="text"   class="form-control" style="width:120px;background-color:White;color:black"  id="cm_DOA" aria-describedby="inputSuccess2Status" />
																   <%-- <input type="date" class="form-control col-md-7 col-xs-12"  id="cm_DOA">--%>
															
															 </div>
                                                        </div>
														 

                                                    </div>
                                                   

                                                    <div class="form-group">
                                                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">Tag
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                                            <input type="text" class="txt form-control col-md-7 col-xs-12 alphanumeric" id="cm_Tag">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">GST No
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">

                                                            <input type="text" class="txt form-control col-md-7 col-xs-12" required="required" value="0" id="txtGSTNo">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="first-name" class="control-label col-md-3 col-sm-3 col-xs-3">IsActive*
                                                        </label>
                                                        <div class="col-md-9 col-sm-9 col-xs-9">
                                                        <input type="checkbox" checked="checked" id="cm_IsActive" />

															<div class="foc-cnt">
																<label for="first-name" class="control-label ">FOC <span class="required">*</span></label>
                                                                <input type="checkbox" id="cm_FOC"  />
															</div>
															<div class="credit-cnt">
																<label for="first-name" class="control-label ">Credit</label>
                                                                   <input type="checkbox"  id="cm_Credit" />
                                                            
															</div>

                                                        </div>
                                                    </div>

                                                    <div class="form-group cashcustomer_btns">
                                                        <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                                        <button class="btn btn-danger" id="btncashcst_close"  onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                                        <%-- <button class="btn btn-danger"  id="closkey" type="button"><i class="fa fa-mail-reply-all"></i> Close keyboard</button>--%>
                                                    </div>

                                                    <%--                                          
                                                                                                                        <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">FOC <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">

                                                                        </div>
                                                                    </div> --%>
                                    </td>

                                    <td valign="top">

                                        <%--    <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address2 <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">--%>
                                            <%--   <input type="text" class="txt form-control col-md-7 col-xs-12" id="cm_Address2" style="height: 72px;"/>--%>
                                                <%--               <textarea class="txt form-control col-md-7 col-xs-12  validate required" id="cm_Address2"   ></textarea>
                                                                         </div>
                                                                    </div>--%>

                                                    <%--               <div class="form-group">
                                                                        <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">City <span class="required">*</span>
                                                                        </label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-12">
                                                                       <table>
                                                                           <tr>
                                                                               <td>

                                                                               </td>
                                                                                 <td  style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                                                 <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')"  style="font-size:18px;cursor:pointer;margin-top:-5px"></span>

                                                                               </td>
                                                                           </tr>

                                                                       </table>

                                                                        </div>
                                                                    </div>--%>

                                    </td>
                                </tr>
                            </table>                                       
                            <%-- <div class="ln_solid"></div>--%>
                                        
                     </form>

                  </div>

                            </div>
<%--<script>
    $(document).ready(function () {

        $("#closkey").click(function () {
     
            $("#mlkeyboard").css('display','none');
            //$("#closkey").css('display', 'none');
        });

    });
</script>--%>
 
                            
   
 

 
 