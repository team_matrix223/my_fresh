﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="AddOn.ascx.cs" Inherits="Templates_AddOn" %>
<script language="javascript" type="text/javascript">
    function BindAddOnData(data) {

        var DataVal = "";
        var addOnCounter=$("#hdnAddOnCounter").val();
        $("input[name='chkAddOn']").each(
        function () {

            if ($(this).prop("checked")) {
                DataVal = DataVal + $(this).val()+", ";

                

            }

        });

        DataVal = DataVal.substr(0, DataVal.length - 2);
        $("#txtRemarks" + addOnCounter).val(DataVal).change();
        $("#txtRemarks" + addOnCounter).prop("title", DataVal);
    }

</script>
 
<asp:Literal ID="hdnAOCounter" ClientIDMode="Static" runat="server" />
<asp:DataList ID="dlAddOn" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
<ItemTemplate>
<div style="margin:5px">
 <input type="checkbox" name='chkAddOn' id='chkAddOn<%#Eval("AddOnId") %>'
 onchange='javascript:BindAddOnData("<%#Eval("Title") %>")'
  value="<%#Eval("Title") %>" />&nbsp;<label for='chkAddOn<%#Eval("AddOnId") %>'><%#Eval("Title") %></label>
</div>

</ItemTemplate>
</asp:DataList>