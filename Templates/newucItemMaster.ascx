﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="newucItemMaster.ascx.cs" Inherits="Templates_newucItemMaster" %>
<asp:Literal ID="ltContent" runat="server"></asp:Literal>


<script langauage="javascript" type="text/javascript">
    var Purcode = "";
    var salecode = "";
    var codeauto = false;
    var alphanumericcode = false;
    var itemcodelen = 0;
	var grpid = 0;
	var grpname = "";
	var cmpnyid = 0;
	var cmpnyname = "";

    var NutritionFactCollection = [];
    function clsFacts() {
        this.Field1 = 0;
        this.Field2 = "";
        this.Field3 = "";
        this.Bold = 0;
        this.Ingredients = 0;
        this.OtherInformation1 = 0;
        this.OtherInformation2 = 0;

    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function GetVMResponse(Id, Title, IsActive, Status, Type) {
        $("#dvVMDialog").dialog("close");


        var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
        if (Type == "Department") {

            $("#ddlIMDepartment").append(opt);

        }

        if (Type == "ItemType") {

            $("#ddlIMItemType").append(opt);

        }

        if (Type == "Color") {

            $("#ddlIMCategory").append(opt);

        }

        if (Type == "Location") {

            $("#ddlIMRackShelf").append(opt);

        }

    }

    function OpenVMDialog(Type) {


        $.ajax({
            type: "POST",
            data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
            url: "managearea.aspx/LoadUserControl",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvVMDialog").remove();
                $("body").append("<div id='dvVMDialog'/>");
                $("#dvVMDialog").html(msg.d).dialog({ modal: true });
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }
        });

    }




    function BindIMSubGroups() {

        //        var GroupID = $("#ddlIMGroup").val();
        $.ajax({
            type: "POST",
            data: '{ }',
            url: "newmanageitemmaster.aspx/BindSubGroups1",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlIMSubGroup").html(obj.SubGroupOptions);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }
	function BindIMCompanies() {



		$.ajax({
			type: "POST",
			data: '{ }',
			url: "newmanageitemmaster.aspx/BindCompanies",
			contentType: "application/json",
			dataType: "json",
			success: function (msg) {

				var obj = jQuery.parseJSON(msg.d);


				$("#ddlIMCompany").html(obj.GroupOptions);


			}, error: function (xhr, ajaxOptions, thrownError) {

				var obj = jQuery.parseJSON(xhr.responseText);
				alert(obj.Message);
			},
			complete: function (msg) {


			}

		});

	}

    function BindIMGroups() {



        $.ajax({
            type: "POST",
            data: '{ }',
            url: "newmanageitemmaster.aspx/BindGroups",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


                $("#ddlIMGroup").html(obj.GroupOptions);


            }, error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function (msg) {


            }

        });

    }


	function BindPSMGroups() {



		$.ajax({
			type: "POST",
			data: '{ }',
			url: "newmanageitemmaster.aspx/BindPSMGroups",
			contentType: "application/json",
			dataType: "json",
			success: function (msg) {

				var obj = jQuery.parseJSON(msg.d);


				$("#ddlPSMGroup").html(obj.GroupOptions);


			}, error: function (xhr, ajaxOptions, thrownError) {

				var obj = jQuery.parseJSON(xhr.responseText);
				alert(obj.Message);
			},
			complete: function (msg) {


			}

		});

	}

	function BindPSMCompany() {



		$.ajax({
			type: "POST",
			data: '{ }',
			url: "newmanageitemmaster.aspx/BindPSMCompany",
			contentType: "application/json",
			dataType: "json",
			success: function (msg) {

				var obj = jQuery.parseJSON(msg.d);


				$("#ddlPSMCompany").html(obj.GroupOptions);


			}, error: function (xhr, ajaxOptions, thrownError) {

				var obj = jQuery.parseJSON(xhr.responseText);
				alert(obj.Message);
			},
			complete: function (msg) {


			}

		});

	}
    function BindVariableMaster() {


        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "newmanageitemmaster.aspx/GetVariableMasters",
            data: {},
            async: false,
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Departments.length; i++) {

                    html = html + "<option value='" + obj.Departments[i]["Id"] + "'>" + obj.Departments[i]["Title"] + "</option>";
                }

                $("#ddlIMDepartment").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Categories.length; i++) {

                    html = html + "<option value='" + obj.Categories[i]["Id"] + "'>" + obj.Categories[i]["Title"] + "</option>";
                }




                $("#ddlIMCategory").html(html);



                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.ItemTypes.length; i++) {

                    html = html + "<option value='" + obj.ItemTypes[i]["Id"] + "'>" + obj.ItemTypes[i]["Title"] + "</option>";
                }

                $("#ddlIMItemType").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Locations.length; i++) {

                    html = html + "<option value='" + obj.Locations[i]["Id"] + "'>" + obj.Locations[i]["Title"] + "</option>";
                }
                $("#ddlIMRackShelf").html(html);

                var html = "<option value='0'></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.SaleUnits.length; i++) {

                    html = html + "<option value='" + obj.SaleUnits[i]["Id"] + "'>" + obj.SaleUnits[i]["Title"] + "</option>";
                }
                $("#ddlIMSaleUnit").html(html);
                $("#ddlIMWgtLtr").html(html);

                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Companies.length; i++) {

                    html = html + "<option value='" + obj.Companies[i]["Id"] + "'>" + obj.Companies[i]["Title"] + "</option>";
                }
                $("#ddlIMCompany").html(html);

                var html = "<option></option>";

                for (var i = 0; i < obj.TaxRates.length; i++) {

                    html = html + "<option value='" + obj.TaxRates[i]["Id"] + "'>" + obj.TaxRates[i]["Title"] + "</option>";
                }
                $("#ddlIMTax").html(html);


                var html = "<option></option>";
                html = html + "<option value='0'>NONE</option>";

                for (var i = 0; i < obj.Excise.length; i++) {

                    html = html + "<option value='" + obj.Excise[i]["Id"] + "'>" + obj.Excise[i]["Title"] + "</option>";
                }
                $("#ddlIMExicise").html(html);

                var html = "<option></option>";

                for (var i = 0; i < obj.HSN.length; i++) {

                    html = html + "<option value='" + obj.HSN[i]["Title"] + "'>" + obj.HSN[i]["Title"] + "</option>";
                }
                $("#ddlHSNCode").html(html);

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $("#ddlIMTax").val(0);
                $("#ddlIMDepartment").val(0);
                $("#ddlIMCompany").val(0);
                $("#ddlIMCategory").val(0);
                $("#ddlIMItemType").val(0);
                $("#ddlIMExicise").val(0);
                $("#ddlHSNCode").val(0);
                $("#ddlIMRackShelf").val(0.00);
                $("#txtIMPurRate").val("0.00");
                $("#txtIMMaxRetailPrice").val("0.00");
                $("#txtIMFranchiseRate").val("0.00");
                $("#txtIMWholeSalerate").val("0.00");
         
                $("#txtIMMarkup").val("0.00");
                $("#txtIMDeiveryNoteRate").val("0.00");
                $("#txtIMSaleRate").val(0.00);
                $("#ddlIMGroup").val("NONE");
                $("#ddlIMGroup").text("NONE");
                $("#ddlIMTransactionType").val("Sale Only");
                $("#ddlIMSaleUnit").val("4.00");
                $("#ddlIMWgtLtr").val("4.00");

            }
        });

    }


	
    function changeitemcode(ItemCode) {
        var code = ItemCode;

			$.ajax({
				type: "POST",
				data: '{  "ItemCode":"' + code + '"}',
				url: "newmanageitemmaster.aspx/GetByItemCodepsm",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);
					if (obj.Productdata.Item_Name == "") {

						alert("Item Not Found");
						$("#txtIMItemCode").val("");
						$("#txtIMSaleRate").val("");
						$("#txtIMPurRate").val("");
						$("#txtIMMaxRetailPrice").val("");
						$("#txtIMItemName").val("");
						$("#txtIMShortName1").val("");
						$("#txtIMBarCode").val("");
						$("#txtIMWholeSalerate").val("");
						$("#txtIMFranchiseRate").val("");
						$("#txtIMSaleRateExcl").val("");
						$("#ddlIMTax").val(0);
						$("#ddlIMItemType").val(0);

						$("#ddlHSNCode").val(0);
						$("#txtIMItemCode").focus();
					} else {
						$("#txtIMPurRate").val(obj.Productdata.Pur_Rate);
						$("#txtIMSaleRate").val(obj.Productdata.Sale_Rate);
						$("#txtIMMaxRetailPrice").val(obj.Productdata.Max_Retail_Price);
						$("#txtIMItemName").val(obj.Productdata.Item_Name);
						$("#txtIMShortName1").val(obj.Productdata.ShortName);
						$("#txtIMBarCode").val(obj.Productdata.Item_Code);
						$("#txtIMWholeSalerate").val(obj.Productdata.Whole_sale_Rate);


						$("#txtIMFranchiseRate").val(obj.Productdata.Sale_Rate);
						$("#txtIMSaleRateExcl").val(obj.Productdata.Sale_Rate_Excl);
						var tax = obj.Productdata.Tax_ID;

						$("#ddlIMTax option[value='" + tax + "']").prop("selected", true);

						var itemtype = obj.Productdata.itemtype;
						var hsn = obj.Productdata.hsncode;

						$("#ddlIMItemType option[value='" + itemtype + "']").prop("selected", true);

						$("#ddlHSNCode option[value='" + hsn + "']").prop("selected", true);

						var group = obj.Productdata.GroupId;

						$("#ddlPSMGroup option[value='" + group + "']").prop("selected", true);

						var company = obj.Productdata.CompanyId;

						$("#ddlPSMCompany option[value='" + company + "']").prop("selected", true);
						$("#ddlPSMGroup").attr('disabled', 'disabled');
						$("#ddlPSMCompany").attr('disabled', 'disabled');

						var transactionType = obj.Productdata.TransactionMode;


						$("#ddlIMTransactionType option[value='" + transactionType + "']").prop("selected", true);

						grpid = group;
						grpname = $("#ddlPSMGroup option:selected").text();
						cmpnyid = company;
						cmpnyname = $("#ddlPSMCompany option:selected").text();
						$("#ddlIMDepartment option[value=2]").prop("selected", true);

					}


				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {


				}

			});
			
		}
	


    function BindNutritionFacts(objArr) {

        var len = objArr.length;


        var html = "";
        for (var i = 0; i < 14; i++) {

            if (i < len) {
                html = html + "<tr><td>" + (i + 1) + "</td><td><input type='checkbox' name='chkIMNuBold'></td><td><input type='text' name='txtIMField1' value='" + objArr[i]["Title"] + "'/></td><td><input type='text' name='txtIMField2'/></td><td><input type='text' name='txtIMField2'/></td></tr>";
            }
            else {
                html = html + "<tr><td>" + (i + 1) + "</td><td><input type='checkbox' name='chkIMNuBold'></td><td><input type='text' name='txtIMField1'/></td><td><input type='text' name='txtIMField2'/></td><td><input type='text' name='txtIMField2'/></td></tr>";

            }


        }

        $("#tbNutritionInfo").html(html);
    }





    function BindRows() {


        var html = "";
        for (var i = 0; i < NutritionFactCollection.length; i++) {

            html += "<tr><td>" + (i + 1) + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Bold"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field1"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field2"] + "</td>";
            html += "<td>" + NutritionFactCollection[i]["Field3"] + "</td>";

            html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer'   /></td>";
            html += "</tr>";

            $("#txtNutritionRemarks").val(NutritionFactCollection[i]["Ingredients"]);


        }

        $("#tbNutritionInfo").html(html);

    }





    function ClearItemDialog() {
        validateForm("detach");
        //$("#txtIMMasterCode").val("");
        //$("#txtIMItemCode").val("");
        $("#txtIMItemName").val("");
        $("#txtIMShortName1").val("");
        $("#txtIMShortName2").val("");
        $("#txtIMBarCode").val("");
        //$("#ddlIMDepartment").val("0");
        //$("#ddlIMGroup").val("0");
        //$("#ddlIMSubGroup").val("0");
        //$("#ddlIMCompany").val("0");
        //$("#ddlIMCategory").val("0");
        //$("#ddlIMTax").val(1);
        //$("#ddlIMItemType").val("1");
        //$("#ddlIMExicise").val("0");
        //$("#ddlHSNCode").val("0");
        //$("#txtIMPurRate").val("");
        //$("#txtIMMaxRetailPrice").val("");
        //$("#txtIMFranchiseRate").val("");
        //$("#txtIMWholeSalerate").val("");
        //$("#txtIMSaleRate").val("");
        //$("#txtIMDeiveryNoteRate").val("");
        $("#txtIMSaleRateExcl").val("");
        //$("#txtIMMarkup").val("");
        //$("#ddlIMRackShelf").val("0");
        //$("#ddlIMSaleUnit").val("1");
        //$("#txtIMWgtLtr").val("");
        //$("#ddlIMWgtLtr").val("");
        //$("#ddlIMTransactionType").val("");

        //$('#chkInHousePacking').prop('checked', false);
        //$('#chkEditRate').prop('checked', false);
        //$("#chkIMPackedQty").val("");
        //$('#chkIMDis1Per').prop('checked', false);
        //$("#txtIMDis1Per").val("");
        //$('#txtIMDis1Per').prop('checked', false);
        //$("#txtIMDis2Per").val("");
        //$('#chkIMItemDiscontinued').prop('checked', false);
        //$('#chkIMCouponPrinting').prop('checked', false);
        //$("#txtIMDays").val("");
        //$('#rdoIMExpiry').prop('checked', false);
        //$('#rdoIMBestBefore').prop('checked', false);
        //$('#rdoIMVeg').prop('checked', false);
        //$('#rdoIMNonVeg').prop('checked', false);
        //$("#ddlIMSubItem").val("");
        //$('#chkIMSubItem').prop('checked', false);
        //$("#txtIMQtyToLess").val("");

        //$("#txtIMMaxLevel").val("");
        //$("#txtIMMinLevel").val("");
        //$("#txtIMReOrderOty").val("");
        //$("#txtIMLevel2").val("");
        //$("#txtIMLevel3").val("");
        //$("#txtIMLevel4").val("");
        //$("#ddlIMCurQtyRate").val("");

        //$("#txtIMDealerMargin").val("");
        //$("#txtIMQtyInCase").val("");
        //$("#txtIMWithTax").val("");

        $("#txtIMLatestPurRate").val("0");
        $("#txtIMComments").val("");
        //$("#ddlIMTax").val(1);
        //$("#ddlIMDepartment").val(0);
        //$("#ddlIMCompany").val(0);
        //$("#ddlIMCategory").val(0);
        //$("#ddlIMItemType").val(1);
        //$("#ddlIMExicise").val(0);
        //$("#ddlHSNCode").val("0");
        //$("#ddlIMRackShelf").val(0);
        $("#txtIMPurRate").val("0.00");
        $("#txtIMMaxRetailPrice").val("0.00");
        $("#txtIMFranchiseRate").val("0.00");
        $("#txtIMWholeSalerate").val("0.00");
        $("#txtIMSaleRate").val("0.00");
        $("#txtIMMarkup").val("0.00");
        $("#txtIMDeiveryNoteRate").val("0.00");
        $("#txtIMSaleRate").val("0.00");
        //$("#ddlIMGroup").val("NONE");
        //$("#ddlIMGroup").text("NONE");
        //$("#ddlIMTransactionType").val("Sale Only");
        //$("#ddlIMSaleUnit").val(4);
        //$("#ddlIMWgtLtr").val(4);
        //$("#dvIMDialog").dialog("close");



    }


    function ClearItemDialog2() {
        validateForm("detach");
        $("#txtIMMasterCode").val("");
        $("#txtIMItemCode").val("");
        $("#txtIMItemName").val("");
        $("#txtIMShortName1").val("");
        $("#txtIMShortName2").val("");
        $("#txtIMBarCode").val("");
        $("#ddlIMDepartment").val("0");
        $("#ddlIMGroup").val("0");
        $("#ddlIMSubGroup").val("0");
        $("#ddlIMCompany").val("0");
        $("#ddlIMCategory").val("0");
        $("#ddlIMTax").val(1);
        $("#ddlIMItemType").val("1");
        $("#ddlIMExicise").val("0");
        $("#ddlHSNCode").val("0");
        $("#txtIMPurRate").val("");
        $("#txtIMMaxRetailPrice").val("");
        $("#txtIMFranchiseRate").val("");
        $("#txtIMWholeSalerate").val("");
        $("#txtIMSaleRate").val("");
        $("#txtIMDeiveryNoteRate").val("");
        $("#txtIMSaleRateExcl").val("");
        $("#txtIMMarkup").val("");
        $("#ddlIMRackShelf").val("0");
        $("#ddlIMSaleUnit").val("1");
        $("#txtIMWgtLtr").val("");
        $("#ddlIMWgtLtr").val("");
        $("#ddlIMTransactionType").val("");

        $('#chkInHousePacking').prop('checked', false);
        $('#chkEditRate').prop('checked', false);
        $("#chkIMPackedQty").val("");
        $('#chkIMDis1Per').prop('checked', false);
        $("#txtIMDis1Per").val("");
        $('#txtIMDis1Per').prop('checked', false);
        $("#txtIMDis2Per").val("");
        $('#chkIMItemDiscontinued').prop('checked', false);
        $('#chkIMCouponPrinting').prop('checked', false);
        $("#txtIMDays").val("");
        $('#rdoIMExpiry').prop('checked', false);
        $('#rdoIMBestBefore').prop('checked', false);
        $('#rdoIMVeg').prop('checked', false);
        $('#rdoIMNonVeg').prop('checked', false);
        $("#ddlIMSubItem").val("");
        $('#chkIMSubItem').prop('checked', false);
        $("#txtIMQtyToLess").val("");

        $("#txtIMMaxLevel").val("");
        $("#txtIMMinLevel").val("");
        $("#txtIMReOrderOty").val("");
        $("#txtIMLevel2").val("");
        $("#txtIMLevel3").val("");
        $("#txtIMLevel4").val("");
        $("#ddlIMCurQtyRate").val("");

        $("#txtIMDealerMargin").val("");
        $("#txtIMQtyInCase").val("");
        $("#txtIMWithTax").val("");

        $("#txtIMLatestPurRate").val("0");
        $("#txtIMComments").val("");
        $("#ddlIMTax").val(1);
        $("#ddlIMDepartment").val(0);
        $("#ddlIMCompany").val(0);
        $("#ddlIMCategory").val(0);
        $("#ddlIMItemType").val(1);
        $("#ddlIMExicise").val(0);
        $("#ddlHSNCode").val("0");
        $("#ddlIMRackShelf").val(0);
        $("#txtIMPurRate").val("1");
        $("#txtIMMaxRetailPrice").val(1);
        $("#txtIMFranchiseRate").val(1);
        $("#txtIMWholeSalerate").val(1);
        $("#txtIMSaleRate").val(1);
        $("#txtIMMarkup").val(1);
        $("#txtIMDeiveryNoteRate").val(1);
        $("#txtIMSaleRate").val(1);
        $("#ddlIMGroup").val("NONE");
        $("#ddlIMGroup").text("NONE");
        $("#ddlIMTransactionType").val("Sale Only");
        $("#ddlIMSaleUnit").val(4);
        $("#ddlIMWgtLtr").val(4);
        $("#dvIMDialog").dialog("close");
    }
   
    $(document).ready(
    function () {

        BindIMGroups();
            BindIMSubGroups();
            BindPSMGroups();
            BindPSMCompany();

        if ($(".rdo_formtype").val() == "B") {
            $(".FormType").hide();

        }
        $("#chkIMPackedQty").prop('disabled', true);
        $("#txtIMDis1Per").prop('disabled', true);
        $("#txtIMDis2Per").prop('disabled', true);
			
            $("#ddlIMGroup").change(function () {

                grpid = $("#ddlIMGroup").val();
				grpname = $("#ddlIMGroup option:selected").text();
            });


			
			$("#ddlIMCompany").change(function () {

                cmpnyid = $("#ddlIMCompany").val();
				cmpnyname = $("#ddlIMCompany option:selected").text();
			});
        $("#chkInHousePacking").change(function () {

            if ($('#chkInHousePacking').prop('checked') == true) {
                $("#chkIMPackedQty").prop('disabled', false);

            }
            else {
                $("#chkIMPackedQty").prop('disabled', true);

                $("#chkIMPackedQty").val("");
            }
        });


        $("#chkIMDis1Per").change(function () {

            if ($('#chkIMDis1Per').prop('checked') == true) {

                $("#txtIMDis1Per").prop('disabled', false);

            }
            else {
                $("#txtIMDis1Per").prop('disabled', true);

                $("#txtIMDis1Per").val("");
            }
        });

        $("#chkIMDis2Per").change(function () {

            if ($('#chkIMDis2Per').prop('checked') == true) {
                $("#txtIMDis2Per").prop('disabled', false);

            }
            else {
                $("#txtIMDis2Per").prop('disabled', true);

                $("#txtIMDis2Per").val("");
            }
        });


        $("#ddlIMSubItem").supersearch({
            Type: "MasterItem",
            Caption: "Please enter Master Items ",
            AccountType: "",
            Width: 214,
            DefaultValue: 0,
            Godown: 0
        });


        $("#btnDel").click(
      function () {
          var RowIndex = Number($(this).closest('tr').index());


          NutritionFactCollection.splice(RowIndex, 1);
          BindRows();

      }

      );


        $("#txtIMSaleRate").keyup(
        function () {




            if ($("#ddlIMTax").val() == "0") {
                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val());
            }
            else {
                var SR = $('#txtIMSaleRate').val();
                var TR = $("#ddlIMTax option:selected").text();

                var TaxAmt = (Number((SR * TR) / (100 + Number(TR)))).toFixed(2);

                var total = $('#txtIMSaleRate').val() - Number(TaxAmt).toFixed(2);

                $("#txtIMSaleRateExcl").val(total.toFixed(2));
            }


            //            if ($('#txtIMSaleRate').val() != "") {
            //                var TaxAmt = ($('#txtIMSaleRate').val() * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val() - TaxAmt);
            //            }






        }
        );

        $("#txtIMItemName").keyup(
            function () {
                $("#txtIMShortName1").val($("#txtIMItemName").val())

            }
            );

        //        $("#txtIMMasterCode").keyup(
        //           function () {
        //              
        //           }
        //           );

            $("#txtIMItemCode").change(
            function () {
                    var code = $(this).val();

					$.ajax({
                        type: "POST",
                        data: '{  "ItemCode":"' + code + '"}',
						url: "newmanageitemmaster.aspx/getitemcodecheck",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);
                            if (obj.Status == -1) {

                                changeitemcode(code);


                            }
                            else {
                                alert("Item Already Exists");
								$("#txtIMItemCode").val("");
								$("#txtIMSaleRate").val("");
								$("#txtIMPurRate").val("");
								$("#txtIMMaxRetailPrice").val("");
								$("#txtIMItemName").val("");
								$("#txtIMShortName1").val("");
								$("#txtIMBarCode").val("");
								$("#txtIMWholeSalerate").val("");
								$("#txtIMFranchiseRate").val("");
								$("#txtIMSaleRateExcl").val("");
								$("#ddlIMTax").val(0);
								$("#ddlIMItemType").val(0);

								$("#ddlHSNCode").val(0);
								$("#txtIMItemCode").focus();
                                $("#txtIMItemCode").val("");
								$("#ddlPSMGroup").val(0);
								$("#ddlPSMCompany").val(0);
                               
                            }

                                

						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {


						}

					});
                //if (code.length > itemcodelen) {
                //    $(this).val(code.substr(0, itemcodelen));
                //    return;
                //}

                //if (alphanumericcode == false) {
                //    if (isNaN(code)) {
                //        $(this).val(code.replace(/\D/g, ''));
                //    }
                //}

                //$("#txtIMBarCode").val($("#txtIMItemCode").val())
            }
            );


        $.ajax({
            type: "POST",
            data: '{ }',
            url: "newmanageitemmaster.aspx/FillSettings",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                codeauto = obj.setttingData.Auto_GenCode;

                alphanumericcode = obj.setttingData.Alphabet_Code;
                itemcodelen = obj.setttingData.Item_CodeLen;
                if (codeauto == true) {

                    $("#txtIMItemCode").attr('disabled', 'disabled').val("Auto");
                }
                else {

                    $("#txtIMItemCode").removeAttr('disabled');
                }

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


            }

        });



        BindVariableMaster();
        // BindNutritionFacts("");

        $("#ddlIMTax").change(function () {
            var taxrate = $("#ddlIMTax option:selected").text();
            $.ajax({
                type: "POST",
                data: '{  "TaxRate":"' + taxrate + '"}',
                url: "newmanageitemmaster.aspx/GetByTax",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    Purcode = obj.TaxData.Cr_AccCode;
                    salecode = obj.TaxData.Dr_AccCode;


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        });


        function ResetList() {

            $("#txtIMField1").val("");
            $("#txtIMField2").val("");
            $("#txtIMField3").val("");
            $("#chkIMBold").removeAttr("checked");



        }

        $("#btnAddNutrition").click(
           function () {




               TO = new clsFacts();
               TO.Field1 = $("#txtIMField1").val();
               TO.Field2 = $("#txtIMField2").val();
               TO.Field3 = $("#txtIMField3").val();

               if (TO.Field1.trim() == "") {
                   $("#txtIMField1").focus();
                   return;
               }

               var IsBold = false;
               if ($("#chkIMBold").prop("checked")) {
                   IsBold = true;
               }


               TO.Bold = IsBold;


               NutritionFactCollection.push(TO);


               BindRows();
               ResetList();

           });


        if ($("#hdnIMID").val() > 0) {
            $.uiLock('');


            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + $("#hdnIMID").val() + '"}',
                url: "newmanageitemmaster.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#txtIMMasterCode").val(obj.ItemOptions.Master_Code);
                    $("#txtIMItemCode").val(obj.ItemOptions.Item_Code);
                    $("#txtIMItemName").val(obj.ItemOptions.Item_Name);
                    $("#txtIMShortName1").val(obj.ItemOptions.Short_Name1);
                    $("#txtIMShortName2").val(obj.ItemOptions.Short_Name2);
                    $("#txtIMBarCode").val(obj.ItemOptions.Bar_Code);
                    

                    if (obj.ItemOptions.Master_Code != obj.ItemOptions.Item_Code) {

                        $("#chkIMSubItem").prop("checked", true);
                        $("#ddlIMSubItem").html("<option selected=selected value='" + obj.ItemOptions.Master_Code + "'>" + obj.ItemOptions.MasterItemName + "</option>");
                        $("#txtddlIMSubItem").val(obj.ItemOptions.MasterItemName);

                        $("#txtddlIMSubItem").attr("title", obj.ItemOptions.MasterItemName);

                    }
                    else {
                        $("#chkIMSubItem").prop("checked", false);
                    }

      
                
                    if (obj.ItemOptions.TAGGED == true) {
                        $("#chkTag").prop("checked", true);

                    }
                    else {
                        $("#chktag").prop("checked", false);
                    }

                    $("#ddlIMCurQtyRate").val(obj.ItemOptions.Cursor_On);
                    var department = obj.ItemOptions.Department;

                    $("#ddlIMDepartment option[value='" + department + "']").prop("selected", true);

                    $.ajax({
                        type: "POST",
                        data: '{}',
                        url: "newmanageitemmaster.aspx/BindGroups",
                        contentType: "application/json",
                        dataType: "json",
                        success: function (msg) {

                            var obj = jQuery.parseJSON(msg.d);


                            $("#ddlIMGroup").html(obj.GroupOptions);


                        }, error: function (xhr, ajaxOptions, thrownError) {

                            var obj = jQuery.parseJSON(xhr.responseText);
                            alert(obj.Message);
                        },
                        complete: function (msg) {

                            $("#ddlIMGroup").val(obj.ItemOptions.GROUP_ID);
                            var GroupID = obj.ItemOptions.GROUP_ID;

                            $.ajax({
                                type: "POST",
                                data: '{ }',
                                url: "newmanageitemmaster.aspx/BindSubGroups1",
                                contentType: "application/json",
                                dataType: "json",
                                success: function (msg) {

                                    var obj = jQuery.parseJSON(msg.d);


                                    $("#ddlIMSubGroup").html(obj.SubGroupOptions);


                                }, error: function (xhr, ajaxOptions, thrownError) {

                                    var obj = jQuery.parseJSON(xhr.responseText);
                                    alert(obj.Message);
                                },
                                complete: function (msg) {


                                    $.uiUnlock();

                                    $("#ddlIMSubGroup").val(obj.ItemOptions.SGroup_id);
                                }

                            });



                        }

                    });


                    var posid = obj.ItemOptions.posid; 
					$("#ddpos option[value='" + posid + "']").prop("selected", true);

                    var company = obj.ItemOptions.COMPANY_ID;

                    $("#ddlIMCompany option[value='" + company + "']").prop("selected", true);


                    var tax = obj.ItemOptions.Tax_ID;
                    $("#ddlIMTax option[value='" + tax + "']").prop("selected", true);

                    var itemtype = obj.ItemOptions.Item_Type;
                    $("#ddlIMItemType option[value='" + itemtype + "']").prop("selected", true);

                    var excise = obj.ItemOptions.Excise_ID;
                    $("#ddlIMExicise option[value='" + excise + "']").prop("selected", true);
                   

                    var location = obj.ItemOptions.Location;
                    $("#ddlIMRackShelf option[value='" + location + "']").prop("selected", true);

                    var saleunit = obj.ItemOptions.Sales_In_Unit;
                    $("#ddlIMSaleUnit option[value='" + saleunit + "']").prop("selected", true);

                    var saleunit2 = obj.ItemOptions.Sales_In_Unit2;
                    $("#ddlIMWgtLtr option[value='" + saleunit2 + "']").prop("selected", true);


                    var transactionType = obj.ItemOptions.Transaction_Mode;


                    $("#ddlIMTransactionType option[value='" + transactionType + "']").prop("selected", true);


                    var category = obj.ItemOptions.COLOR_ID;
                    $("#ddlIMCategory option[value='" + category + "']").prop("selected", true);

                    $("#txtIMPurRate").val(Number(obj.ItemOptions.Purchase_Rate).toFixed(2));
                    $("#txtIMMaxRetailPrice").val(Number(obj.ItemOptions.Max_Retail_Price).toFixed(2));
                    $("#txtIMSaleRate").val(Number(obj.ItemOptions.Sale_Rate).toFixed(2));
                    $("#txtIMSaleRateExcl").val(Number(obj.ItemOptions.Sale_Rate_Excl).toFixed(2));
                    $("#txtIMDeiveryNoteRate").val(Number(obj.ItemOptions.DeliveryNoteRate).toFixed(2));
                    $("#txtIMMarkup").val(Number(obj.ItemOptions.Mark_Up).toFixed(2));
                    $("#txtIMDis1Per").val(Number(obj.ItemOptions.Dis1Value).toFixed(2));
                    $("#txtIMDis2Per").val(Number(obj.ItemOptions.Dis2Value).toFixed(2));
                    $("#chkIMPackedQty").val(obj.ItemOptions.InHouse_PackedQty);
                    $("#txtIMWholeSalerate").val(Number(obj.ItemOptions.Whole_Sale_Rate).toFixed(2));
                    $("#txtIMFranchiseRate").val(Number(obj.ItemOptions.Franchise_SaleRate).toFixed(2));
                    $("#txtIMWgtLtr").val(Number(obj.ItemOptions.Packing).toFixed(2));

                    var transactiontype = obj.ItemOptions.Transaction_Mode
                    $("#ddlIMTransactionType option[value='" + transactiontype + "']").prop("selected", true);






                    if (obj.ItemOptions.Discontinued == true) {

                        $('#chkIMItemDiscontinued').prop('checked', true);
                    }
                    else {
                        $('#chkIMItemDiscontinued').prop('checked', false);
                    }

                    if (obj.ItemOptions.Dis1InRs == true) {

                        $('#chkIMDis1Per').prop('checked', true);
                    }
                    else {
                        $('#chkIMDis1Per').prop('checked', false);
                    }


                    if (obj.ItemOptions.Dis2InRs == true) {

                        $('#chkIMDis2Per').prop('checked', true);
                    }
                    else {
                        $('#chkIMDis2Per').prop('checked', false);
                    }



                    if (obj.ItemOptions.InHouse_Packing == true) {

                        $('#chkInHousePacking').prop('checked', true);
                    }
                    else {
                        $('#chkInHousePacking').prop('checked', false);
                    }


                    if (obj.ItemOptions.Edit_SaleRate == true) {

                        $('#chkEditRate').prop('checked', true);
                    }
                    else {
                        $('#chkEditRate').prop('checked', false);
                    }


                    $("#txtIMLevel2").val(obj.ItemOptions.Sale_Rate2);
                    $("#txtIMLevel3").val(obj.ItemOptions.Sale_Rate3);
                    $("#txtIMLevel4").val(obj.ItemOptions.Sale_Rate4);

                    $("#txtIMDealerMargin").val(obj.ItemOptions.Delear_Margin);
                    $("#txtIMQtyInCase").val(obj.ItemOptions.Qty_in_Case);

                    // $("#ddlIMCurQtyRate").val();
                    // $("#txtIMWithTax").val();
                    $("#txtIMMaxLevel").val(obj.ItemOptions.Max_Level);
                    $("#txtIMMinLevel").val(obj.ItemOptions.Min_Level);
                    $("#txtIMReOrderOty").val(obj.ItemOptions.Re_Order_Qty);
                    $("#txtIMQtyToLess").val(obj.ItemOptions.Qty_To_Less);

                    if (obj.ItemOptions.VEG_NonVeg == "Veg") {

                        $('#rdoIMVeg').prop('checked', true);
                    }
                    else {
                        $('#rdoIMNonVeg').prop('checked', true);
                    }

                    $("#txtIMDays").val(obj.ItemOptions.Day);
                    if (obj.ItemOptions.BestExp == true) {

                        $('#rdoIMBestBefore').prop('checked', true);
                    }
                    else {
                        $('#rdoIMBestBefore').prop('checked', false);
                    }

                    $("#txtIMComments").val(obj.ItemOptions.Remarks);


                    if (obj.ItemOptions.CpPrinting == "True") {


                        $('#chkIMCouponPrinting').prop('checked', true);
                    }
                    else {
                        $('#chkIMCouponPrinting').prop('checked', false);
                    }
					
                    if (obj.ItemOptions.IsBill == 1) {
                        $("#ddlIMTax").attr('disabled', 'disabled');
                        $("#ddlHSNCode").attr('disabled', 'disabled');
                    }
                    else {
						
						$("#ddlIMTax").removeAttr('disabled');
						$("#ddlHSNCode").removeAttr('disabled');
                    }

                    var HSN = obj.ItemOptions.HSNCode;
                   // $("#ddlHSNCode").val(HSN);
                    $("#ddlHSNCode option:selected").text(HSN);





					//$("#ddlHSNCode option[value='" + HSN + "']").prop("selected", true);

                    NutritionFactCollection = obj.NutritionFact;
                    BindRows();

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });


        }

        $(".rdo_formtype").change(function () {

            var val = $(this).val();

            if (val == "B") {
                $(".FormType").hide();

            }

           else if (val == "A") {
                $(".FormType").show();

            }
          

        });


        $("#btnIMCancel").click(
        function () {

            $("#hdnIMID").val("0");
            ClearItemDialog2();

            });

			$("#btnSMInsert").click(
				function () {
					var Id = 0;
					var Title = $("#txtSMTitle").val();
					if ($.trim(Title) == "") {
						$("#txtSMTitle").focus();

						return;
					}
					
					var IsActive = false;

					if ($('#chkSMIsActive').is(":checked")) {
						IsActive = true;
					}

					var Show = false;

					if ($('#chkSMshowinmenu').is(":checked")) {
						Show = true;
					}
					
					$.uiLock('');

					$.ajax({
						type: "POST",
						data: '{"SGroupId":"' + Id + '", "SGroupName": "' + Title + '","ShowInMenu": "' + Show + '","IsActive": "' + IsActive + '","DepartmentId": "' + 0 + '","GroupId": "' + 0 + '"}',
						url: "Managepropsgroups.aspx/Insert",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {


							var obj = jQuery.parseJSON(msg.d);
							if (obj.Status != -1) {

								$("#txtSMTitle").val("");
								$("#chkSMIsActive").prop("checked", "checked");
								$("#chkSMshowinmenu").prop("checked", "checked");

                                $("#dvsbgrp").hide();
                                BindIMSubGroups();
								alert(obj.Status);
								$("#ddlIMSubGroup").val(obj.Status);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {
							$.uiUnlock();
						}
					});




				});

           
			$("#btnGMInsert").click(
                function () {
                    var Id = 0;
					var Title = $("#txtGMTitle").val();
					if ($.trim(Title) == "") {
						$("#txtGMTitle").focus();

						return;
					}
					var ImageUrl = "";
					var IsActive = false;

					if ($('#chkGMIsActive').is(":checked")) {
						IsActive = true;
					}

					var Show = false;

					if ($('#chkGMshowinmenu').is(":checked")) {
						Show = true;
					}
					var department = 0;


					$.uiLock('');

					$.ajax({
						type: "POST",
						data: '{"GroupId":"' + Id + '", "GroupName": "' + Title + '","ImageUrl": "' + ImageUrl + '","IsActive": "' + IsActive + '","ShowInMenu": "' + Show + '","DepartmentId": "' + department + '"}',
						url: "ManagePropGroups.aspx/Insert",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);
							if (obj.Status != -1) {

                                $("#txtGMTitle").val("");
								$("#chkGMIsActive").prop("checked", "checked");
								$("#chkGMshowinmenu").prop("checked", "checked");

                                $("#dvgrp").hide();
                                BindIMGroups();
								alert(obj.Status);
								$("#ddlIMGroup").val(obj.Status);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {
							$.uiUnlock();
						}
					});




                });
            var f = 0;
			var g = 0;
			$("#editgrp").click(
				function () {
                    
                    if (f==0) {
                        $("#ddlIMGroup").show();
                        $("#ddlPSMGroup").hide();
                        f = 1;
                    }
					else if (f==1) {
						$("#ddlIMGroup").hide();
						$("#ddlPSMGroup").show();
						f = 0;
					}
                });
			$("#editCompny").click(
				function () {
                    if (g == 0) {
                        $("#ddlIMCompany").show();
                        $("#ddlPSMCompany").hide();
						g = 1;
                    }
                    else if (g == 1) {
                        $("#ddlIMCompany").hide();
                        $("#ddlPSMCompany").show();
						g = 0;
					}

				});
            
			$("#closedvgrp").click(
				function () {

                    $("#dvgrp").hide();

				});
            $("#btngrpadd").click(
                function () {

                    $("#dvgrp").show();

                });
            
			$("#closesbgrp").click(
				function () {

                    $("#dvsbgrp").hide();

				});
			$("#btnsbgrpadd").click(
				function () {

					$("#dvsbgrp").show();

                });
            
			$("#closecgrp").click(
				function () {

                    $("#dvcgrp").hide();

				});
			$("#btncompadd").click(
				function () {

					$("#dvcgrp").show();

                });
			$("#btnCMInsert").click(
				function () {
					var Id = 0;
					var Title = $("#txtCMTitle").val();
					if ($.trim(Title) == "") {
						$("#txtCMTitle").focus();

						return;
					}


					var IsActive = false;

					if ($('#chkCMIsActive').is(":checked")) {
						IsActive = true;
					}
                    var Suppliers = "";



					$.ajax({
						type: "POST",
						data: '{"CompanyId":"' + Id + '", "Title": "' + Title + '","IsActive": "' + IsActive + '","Suppliers": "' + Suppliers + '"}',
						url: "managecompanies.aspx/Insert",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);
							if (obj.Status != -1) {

								$("#txtCMTitle").val("");
								$("#chkCMIsActive").prop("checked", "checked");
								
                                $("#dvcgrp").hide();
                                BindIMCompanies();
								alert(obj.Status);
								$("#ddlIMCompany").val(obj.Status);
							}
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {
							$.uiUnlock();
						}
					});
					

				});

        $("#btnIMAdd").click(
        function () {




            if (!validateForm("dvDialog")) {
                return;
            }


            //Rate Validation

            if (parseFloat($("#txtIMMaxRetailPrice").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("MRP should be greater than Purchase Rate.");
                $('#txtIMMaxRetailPrice').focus();
                return;
            }


            if (parseFloat($("#txtIMFranchiseRate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert(" Franchise SaleRate should be greater than Purchase Rate.");
                $('#txtIMFranchiseRate').focus();
                return;
            }


            if (parseFloat($("#txtIMFranchiseRate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert(" Franchise SaleRate should be lesser than MRP.");
                $('#txtIMFranchiseRate').focus();
                return;
            }

            if (parseFloat($("#txtIMWholeSalerate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("Whole SaleRate should be greater than Purchase Rate.");
                $('#txtIMWholeSalerate').focus();
                return;
            }

            if (parseFloat($("#txtIMWholeSalerate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert(" Whole SaleRate should be lesser than MRP.");
                $('#txtIMWholeSalerate').focus();
                return;
            }



            if (parseFloat($("#txtIMSaleRate").val()) < parseFloat($("#txtIMPurRate").val())) {
                alert("Sale Rate should be greater than Purchase Rate.");
                $('#txtIMSaleRate').focus();
                $('#txtIMSaleRateExcl').val("")
                return;
            }


            if (parseFloat($("#txtIMSaleRate").val()) > parseFloat($("#txtIMMaxRetailPrice").val())) {
                alert("Sale Rate should be lesser than MRP.");
                $('#txtIMSaleRate').focus();
                $('#txtIMSaleRateExcl').val("")
                return;
            }




            //            if ($("#ddlIMTax").val() == "0") {
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val());
            //            }
            //            else {
            //                var TaxAmt = (float.Parse($('#txtIMSaleRate').val()) * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val(float.Parse($('#txtIMSaleRate').val()) - TaxAmt);
            //            }


            //            if ($('#txtIMSaleRate').val() != "") {
            //                var TaxAmt = ($('#txtIMSaleRate').val() * $("#ddlIMTax option:selected").text()) / 100;
            //                $("#txtIMSaleRateExcl").val($('#txtIMSaleRate').val() - TaxAmt);
            //            }

            //-----------

                var objItemMaster = {};
				objItemMaster.CompnyName = cmpnyname;
				objItemMaster.GroupName = grpname;
                objItemMaster.ItemId = $("#hdnIMID").val();
                objItemMaster.COMPANY_ID = cmpnyid;
                objItemMaster.GROUP_ID = grpid;
            //objItemMaster.Master_Code = $("#txtIMMasterCode").val();
            objItemMaster.Item_Code = $("#txtIMItemCode").val();
            objItemMaster.posid = $(".dd_pos option:selected").val();

            objItemMaster.Bar_Code = $("#txtIMBarCode").val();

            objItemMaster.Item_Name = $("#txtIMItemName").val();
            objItemMaster.Packing = $("#txtIMWgtLtr").val();
            objItemMaster.Short_Name1 = $("#txtIMShortName1").val();
            objItemMaster.Short_Name2 = $("#txtIMShortName2").val();
            objItemMaster.Sales_In_Unit = $("#ddlIMSaleUnit").val();
            objItemMaster.Sales_In_Unit2 = $("#ddlIMWgtLtr").val();

            objItemMaster.Qty_To_Less = $("#txtIMQtyToLess").val();
            objItemMaster.Sale_Rate2 = $("#txtIMLevel2").val();
            objItemMaster.Sale_Rate3 = $("#txtIMLevel3").val();
            objItemMaster.Sale_Rate4 = $("#txtIMLevel4").val();
            objItemMaster.Purchase_Rate = $("#txtIMPurRate").val();


            objItemMaster.Max_Retail_Price = $("#txtIMMaxRetailPrice").val();
            objItemMaster.Mark_Up = $("#txtIMMarkup").val();
            objItemMaster.Qty_in_Case = $("#txtIMQtyInCase").val();
            objItemMaster.Max_Level = $("#txtIMMaxLevel").val();
            objItemMaster.Min_Level = $("#txtIMMinLevel").val();
            objItemMaster.Re_Order_Qty = $("#txtIMReOrderOty").val();

            objItemMaster.Tax_Code = $("#ddlIMTax option:selected").text();
            objItemMaster.Pur_Code = Purcode;
            objItemMaster.Sale_Code = salecode;
            objItemMaster.IsSaleable = true;
            objItemMaster.Delear_Margin = $("#txtIMDealerMargin").val();
            objItemMaster.Tax_AfterBefore = "1";
            objItemMaster.Location = $("#ddlIMRackShelf").val();
            objItemMaster.Discount = 0;
            objItemMaster.Department = $("#ddlIMDepartment").val();
            objItemMaster.Sale_Rate = $("#txtIMSaleRate").val();
            objItemMaster.Sale_Rate_Excl = $("#txtIMSaleRateExcl").val();
            objItemMaster.Whole_Sale_Rate = $("#txtIMWholeSalerate").val();
            objItemMaster.DeliveryNoteRate = $("#txtIMDeiveryNoteRate").val();
            objItemMaster.Delivery_No = 0;
            objItemMaster.SGroup_id = $("#ddlIMSubGroup").val();
        
            objItemMaster.Franchise_SaleRate = $("#txtIMFranchiseRate").val();
            objItemMaster.Transaction_Mode = $("#ddlIMTransactionType option:selected").text();
            objItemMaster.Item_Type = $("#ddlIMItemType").val();
            var cursoron = $("#ddlIMCurQtyRate").val();


            if (cursoron == "(NONE)") {
               
                cursoron = "QTY";
            }
            objItemMaster.Cursor_On = cursoron;


            objItemMaster.Ingredients = $("#txtNutritionRemarks").val();
            objItemMaster.Discontinued = false;
            if ($('#chkIMItemDiscontinued').is(":checked")) {
                objItemMaster.Discontinued = true;
            }


            objItemMaster.InHouse_Packing = false;
            objItemMaster.InHouse_PackedQty = 0;
            if ($('#chkInHousePacking').is(":checked")) {
                objItemMaster.InHouse_Packing = true;
                objItemMaster.InHouse_PackedQty = $("#chkIMPackedQty").val();
            }

            objItemMaster.Dis1InRs = false;
            objItemMaster.Dis1Value = 0;
            if ($('#chkIMDis1Per').is(":checked")) {
                objItemMaster.Dis1InRs = true;
                objItemMaster.Dis1Value = $("#txtIMDis1Per").val();
            }

            objItemMaster.Dis2InRs = false;
            objItemMaster.Dis2Value = 0;
            if ($('#chkIMDis2Per').is(":checked")) {
                objItemMaster.Dis2InRs = true;
                objItemMaster.Dis2Value = $("#txtIMDis2Per").val();
            }
            objItemMaster.IsCompBarCode = false;
            objItemMaster.Remarks = $("#txtIMComments").val();
            objItemMaster.AllowPoint = false;
            objItemMaster.Tax_ID = $("#ddlIMTax").val();

            objItemMaster.CpPrinting = false;
            if ($('#chkIMCouponPrinting').is(":checked")) {
                objItemMaster.CpPrinting = true;
            }
            objItemMaster.Excise_ID = 0;
            objItemMaster.Excise_Code = 0;
            if ($("#ddlIMExicise").val() != 0) {
                objItemMaster.Excise_ID = $("#ddlIMExicise").val();
                objItemMaster.Excise_Code = $("#ddlIMExicise option:selected").val();
            }

                objItemMaster.HSNCode = 0;
                
            if ($("#ddlHSNCode").val() != 0) {
                objItemMaster.HSNCode = $("#ddlHSNCode option:selected").text();

            }
            objItemMaster.ImageUrl = "";
            objItemMaster.SubGroupId = $("#ddlIMSubGroup").val();
            objItemMaster.COLOR_ID = 0;
            if ($("#ddlIMCategory").val() != 0) {
                objItemMaster.COLOR_ID = $("#ddlIMCategory").val();
            }

            objItemMaster.Likes = 0;
            objItemMaster.Day = $("#txtIMDays").val();

            objItemMaster.BestExp = false;
            if ($('#rdoIMBestBefore').is(":checked")) {
                objItemMaster.BestExp = true;
            }

            objItemMaster.VEG_NonVeg = "";
            if ($('#rdoIMVeg').is(":checked")) {
                objItemMaster.VEG_NonVeg = "Veg";
            }
            else if ($('#rdoIMNonVeg').is(":checked")) {
                objItemMaster.VEG_NonVeg = "NonVeg";
            }

            //            objItemMaster.IsSubItem = false;
            //            if ($('#chkIMSubItem').is(":checked")) {
            //                objItemMaster.IsSubItem = true;
            //            }



            objItemMaster.IsSubItem = false;
            if ($('#chkIMSubItem').is(":checked")) {
                objItemMaster.IsSubItem = true

                if ($("#ddlIMSubItem").val() == null || $("#ddlIMSubItem").val() == "") {

                    $("#txtddlIMSubItem").focus();
                    alert("Please choose Sub Item First");
                    return;

                }


            }
            objItemMaster.TAGGED = false;
            if ($('#chkTag').is(":checked")) {
            
                objItemMaster.TAGGED = true
            
            
            }
            objItemMaster.ParentItemCode = $("#ddlIMSubItem").val();




            //            objItemMaster.DealerQtyRate = $("#ddlIMCurQtyRate").val();
            //            objItemMaster.PurRateWithTax = $("#txtIMWithTax").val();
            //            objItemMaster.LatestPurRate = $("#txtIMLatestPurRate").val();

            objItemMaster.Expiry = false;
            if ($('#rdoIMExpiry').is(":checked")) {
                objItemMaster.Expiry = true;
            }

            objItemMaster.Edit_SaleRate = false;
            if ($('#chkEditRate').is(":checked")) {
                objItemMaster.Edit_SaleRate = true;
            }
            var DTO = { 'objItemMaster': objItemMaster, 'objNutritionFacts': NutritionFactCollection };

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "newmanageitemmaster.aspx/InsertUpdate",
                data: JSON.stringify(DTO),
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -1) {

                        alert("Insertion Failed.Item already exists.");
                        return;
                    }

                    else if (obj.Status == -3) {
                        alert("Insertion Failed.Item already exists against same company");
                        return;

                    }
                    else if (obj.Status == -2) {

                        alert("Insertion Failed.Item already exists against same Code.");
                        return;
                    }
                    var Id = $("#hdnIMID").val();

                    if (Id == "0") {

                        alert("Item is added successfully.");
                        ClearItemDialog();

                    }
                    else {

                        alert("Item is Updated successfully.");
                        ClearItemDialog();
                    }
                    BindGrid();


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });


        }

        );



    }

    );

    function showHide(val) {

        var divP = document.getElementById("view1");
        var divD = document.getElementById("view2");

        if (val == 1) {
            document.getElementById("ancV1").style.borderBottom = "solid 1px white";
            document.getElementById("ancV1").style.backgroundColor = "white";

            document.getElementById("ancV2").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV2").style.backgroundColor = "";

            divP.style.display = "block";
            divD.style.display = "none";

        }
        else {
            document.getElementById("ancV1").style.borderBottom = "solid 1px #B7B7B7";
            document.getElementById("ancV1").style.backgroundColor = "";

            document.getElementById("ancV2").style.borderBottom = "solid 1px white";
            document.getElementById("ancV2").style.backgroundColor = "white";

            divP.style.display = "none";
            divD.style.display = "block";

        }
    }

    $(".advance_rbtn").click(function () {
        $(".master_info-tbl").removeClass("basic_table_height");
        $(".master_info_table").addClass("master_info_table_height");
        $(".rate_info_table").addClass("rate_info_table_height");
        $(".pak_info_table").addClass("pak_info_table_height");
        $(".stock_leveling_table").addClass("stock_leveling_table_height");
        $(".basic-mark-up").removeClass("rate_info_table_height");
        $('.basic-pak-info-tbl').removeClass('bsic-pkinfo-tbl-cls');
        $('.basic-pack-info-table-tr').removeClass('bsic-pkinfo-tbl-tr-cls');
        $(".master_info-tbl").removeClass("basic_mster-info-tbl");
        $('.basic-qty-infor').removeClass('bsic-height-qty-intro');
        $(".pak_info_table").removeClass("bsic_pak_info_table_height");
        $(".basic-other-info-tble").removeClass("basic-other-info-tble-height");
    });

    $(".basic_rbtn").click(function () {
        /*$(".master_info-tbl").addClass("basic_table_height");*/
        $(".master_info-tbl").addClass("basic_mster-info-tbl");
        $(".master_info_table").removeClass("master_info_table_height");
        $(".rate_info_table").removeClass("rate_info_table_height");
        $(".pak_info_table").removeClass("pak_info_table_height");
        $(".stock_leveling_table").removeClass("stock_leveling_table_height");
        $(".basic-mark-up").addClass("rate_info_table_height");
        $('.basic-pak-info-tbl').addClass('bsic-pkinfo-tbl-cls');
        $('.basic-pack-info-table-tr').addClass('bsic-pkinfo-tbl-tr-cls');
        $('.basic-qty-infor').addClass('bsic-height-qty-intro');
        $(".pak_info_table").addClass("bsic_pak_info_table_height");
        $(".basic-other-info-tble").addClass("basic-other-info-tble-height");
    });
    $(function () {
        $('.basic-mark-up').addClass('rate_info_table_height');
        $('.basic-pak-info-tbl').addClass('bsic-pkinfo-tbl-cls');
        $('.basic-pack-info-table-tr').addClass('bsic-pkinfo-tbl-tr-cls');
        $(".master_info-tbl").addClass("basic_mster-info-tbl");
        $('.basic-qty-infor').addClass('bsic-height-qty-intro');
        $(".pak_info_table").addClass("bsic_pak_info_table_height");
        $(".basic-other-info-tble").addClass("basic-other-info-tble-height");
      });
</script>
<div id="dvDialog" class="manage_item_master">
    <ul data-persist="true" class="tabs manage_master_tabs">


        <li><a href="javascript:showHide(1,'ancV1');" id="ancV1">Item Information</a></li>
        <li><a href="javascript:showHide(2,'ancV2');" id="ancV2">
            Nutritution Facts</a></li>
          
    </ul>
    <div class="tabcontents" style="background:none;">
        <div id="view1" style="display: block;">
            <table style="display:none;width: 20%;background-color: #46c9ac;font-size: 17px;" class="form-type-basic-adv">
                <tr>
                    
                    <td style="padding-left: 15px;">

                        Form Type:
                    </td>
                 <!--   <td>Basic</td> -->
                        <td>
                        
                      <input type="radio" value="B" name="formtype" class="rdo_formtype basic_rbtn" checked="checked"/> Basic
                    </td>
                       <!--<td>Advance</td>-->
                        <td>
                            
                        <input type="radio" value="A" name="formtype" class="rdo_formtype advance_rbtn"/> Advance
                    </td>
                </tr>
                </table>
            <table style="width: 100%">

                <tr>
                    <td>
                        <table class="tables basic_advance_tables" cellspacing="5" cellpadding="5">
                            <tr>
                                 <th class="tables_headings" colspan="100%">
                                    BASIC INFORMATION
                                </th>
                              
                            </tr>
                           <%-- <tr  style="display:none"> <td colspan="2">
                                   Master Code:
                                </td>
                                <td colspan="100%">
                                    
                                    <input type="text" id="txtIMMasterCode" style="width: 80px;"   />
                                </td></tr> 
                            <tr>--%>
                            <tr class="tables_contant"> 
                                <td>
                                    Code:
                                </td>
                                <td>
                                    
                                    <input type="text" id="txtIMItemCode" data-index="1"  class="cls validate required"  />
                                </td>
                                <td>
                                    Name:
                                </td>
                                <td>
                                    <input type="text" id="txtIMItemName" data-index="2" class="cls" maxlength="50" />
                                </td>
                                <td>
                                    Short Name1:
                                </td>
                                <td>
                                    <input type="text" id="txtIMShortName1"  data-index="3" data-index="3" maxlength="20" />
                                </td>
                                <td style="display:none;">
                                    Short Name2:
                                </td>
                                <td  style="display:none;">
                                    <input type="text" id="txtIMShortName2"  data-index="4" data-index="4" maxlength="20" />
                                </td>
                                <td>
                                    Barcode:
                                </td>
                                <td>
                                    <input type="text" id="txtIMBarCode" data-index="5" data-index="5"   />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td valign="top" style="padding: 5px">
                                    <table class="all_tables master_info_table" cellspacing="5" cellpadding="5" style="  border: solid 1px silver;
                                        width: 100%; border-collapse: separate; height:321px">
                                            <tr>
                                                <th class="tables_headings basic_table_heading" colspan="100%">
                                                    MASTER INFORMATION
                                                </th>
                                            </tr>
                                            <tr>
                                                     <td>
                                                POS:
                                            </td>
                                                    <td>
                 <select id="ddpos" class="dd_pos form-control">
    <asp:Repeater ID="rptr_ddpos" runat="server">
        <ItemTemplate>
 
    <option value='<%#Eval("posid") %>' ><%#Eval("title") %></option>
                   </ItemTemplate> 

    </asp:Repeater>                                 

                 </select>

                                                    </td>
                                                </tr>
                                        <tr>
                                            <td>
                                                Department:
                                            </td>
                                            <td>
                                            <table class="inner_table">
   
                                            <tr>
                                            <td>
                                            
                                               <select id="ddlIMDepartment" data-index="6" class="validate ddlrequired" onchange="javascript:BindIMGroups();">
                                                </select>
                                            </td>
                                            <td>
                                            <span onclick="javascript:OpenVMDialog('Department')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>


                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Group:
                                            </td>
                                            <td>
												 <table class="inner_table">
													 <tr>
														 <td>
															<select id="ddlIMGroup" data-index="7" style="display:none" >
															</select>
															 <select id="ddlPSMGroup" data-index="7" >
															</select>
														</td>
														<td><div id="btngrpadd"><i class="fa fa-plus"></i></div></td>
                                               <td> <button id="editgrp">Edit</button></td>
													</tr>
												</table>
                                            </td>
                                         
                                            
                                        </tr>
                                        <tr>
                                            <td>
                                                Subgroup:
                                            </td>
                                            <td>
												 <table class="inner_table">
													 <tr>
														 <td><select id="ddlIMSubGroup" data-index="8"></select></td>
														 <td><div id="btnsbgrpadd"><i class="fa fa-plus"></i></div></td>
													</tr>
													</table>
                                            </td>
                                             
                                        </tr>
                                        <tr>
                                            <td>
                                                Company:
                                            </td>
                                            <td>
												<table class="inner_table">
													<tr>
														<td>
															<select id="ddlIMCompany" data-index="9" style="display:none" >
															</select>
															 <select id="ddlPSMCompany" data-index="9" >
															</select>
														</td>
														 <td><div id="btncompadd"><i class="fa fa-plus"></i></div></td>
														<td><button id="editCompny">Edit</button></td>
													</tr>
												</table>
                                            </td>
                                            
                                        </tr>
                                        <tr class="FormType">
                                            <td>
                                                Category:
                                            </td>
                                            <td>

                                             <table class="inner_table">
                                            <tr>
                                            <td>
                                            
                                                <select id="ddlIMCategory" data-index="10">
                                                </select>
                                            </td>
                                            <td>
                                            <span onclick="javascript:OpenVMDialog('Color')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>



                                               
                                              
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Tax:
                                            </td>
                                            <td>
                                                <select id="ddlIMTax" data-index="11" class="validate ddlrequired">
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Item Type:
                                            </td>
                                            <td>


                                            
                                             <table class="inner_table">
                                            <tr>
                                            <td>
                                            
                                                <select id="ddlIMItemType" data-index="12"  class="validate ddlrequired">
                                                </select>
                                            </td>
                                            <td>
                                            <span onclick="javascript:OpenVMDialog('ItemType')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>

                                               
                                              
                                            </td>
                                        </tr>
                                        <tr class="FormType">
                                            <td>
                                                Excise/Terrif:
                                            </td>
                                            <td>
                                                <select id="ddlIMExicise" data-index="13">
                                                </select>
                                            </td>
                                        </tr>
										<tr>
                                            <td>
                                                HSN Code:
                                            </td>
                                            <td>
                                                <select id="ddlHSNCode" data-index="14">
                                                </select>
                                            </td>
                                           
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table class="all_tables basic_table_height rate_info_table master_info-tbl" cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate;" >
                                        <tr>
                                            <th class="tables_headings basic_table_heading"  colspan="100%">
                                                RATE INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pur. Rate/Unit
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMPurRate" onkeypress="return isNumberKey(event)" data-index="14" class="validate required float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Max. Retail Price
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMaxRetailPrice" onkeypress="return isNumberKey(event)" data-index="15" class="validate  required float" />
                                            </td>
                                        </tr>

                                           <tr>
                                            <td>
                                                Sale Rate/Unit
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMSaleRate" onkeypress="return isNumberKey(event)" data-index="18"  class="validate  required float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sale Rate(Excl.)
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMSaleRateExcl" onkeypress="return isNumberKey(event)" data-index="19" style="background: lightgrey;" readonly="readonly" class="validate  required float" />
                                            </td>
                                        </tr>

                                          <tr class="FormType basic-mark-up">
                                            <td>
                                                Franchise Rate
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMFranchiseRate" onkeypress="return isNumberKey(event)" data-index="16" class="validate  required float" />
                                            </td>
                                        </tr>
                                          <tr>
                                            <td>
                                                Whole Sale Rate
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMWholeSalerate" onkeypress="return isNumberKey(event)" data-index="17" class="validate  required float" />
                                            </td>
                                        </tr>
                                     
                                        <tr class="FormType basic-mark-up">
                                            <td>
                                                Mark Up(%)
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMarkup" onkeypress="return isNumberKey(event)" data-index="20" class="validate  required float" />
                                            </td>
                                        </tr>
                                        <tr class="FormType">
                                            <td>
                                                Rack & Shelf
                                            </td>
                                            <td>

                                            
                                             <table class="inner_table">
                                            <tr>
                                            <td>
                                            
                                             
                                                <select data-index="21"  id="ddlIMRackShelf">
                                                </select>
                                            </td>
                                            <td>
                                            <span onclick="javascript:OpenVMDialog('Location')" class="fa fa-plus"></span>
                                            </td>
                                            </tr>
                                            </table>


                                             
                                            </td>
                                        </tr>

                                         <tr class="FormType">
                                            <td>
                                                DeliveryNote Rate
                                            </td>
                                            <td>
                                                <input type="text" data-index="22" id="txtIMDeiveryNoteRate" onkeypress="return isNumberKey(event)" class="validate  required float" />
                                            </td>
                                        </tr>
                                       
                                        <tr class="FormType">
                                           
                                            <td><label style="width:auto;">Edit Sale/MRP Rate</label></td>
                                            <td>
                                                 <input type="checkbox" data-index="27" id="chkEditRate" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table class="all_tables basic_table_height master_info-tbl basic-qty-infor" cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate; height: 106px;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                QTY INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Sales Unit
                                            </td>
                                            <td>
                                                <select data-index="23" id="ddlIMSaleUnit">
                                                </select>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Wgt/Ltr/Pcs
                                            </td>
                                            <td>
                                                <input type="text" data-index="24" id="txtIMWgtLtr" />
                                            </td>
                                            <td>
                                                <select id="ddlIMWgtLtr" data-index="25"></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Transaction Type
                                            </td>
                                            <td>
                                                <select data-index="26" id="ddlIMTransactionType">
                                                <option value="Purchase Only">Purchase Only</option>
                                                <option value="Sale Only">Sale Only</option>
                                                <option value="Purchase & Sale Both">Purchase & Sale Both</option>


                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="all_tables FormType pak_info_table basic-pak-info-tbl" width="100%" cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                        width: 100%;     border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                PACKING INFORMATION
                                            </th>
                                         
                                        </tr>
                                        <tr>
                                            <td > <label style="width:auto;">InHouse Packing</label>  
                                                   <input type="checkbox" data-index="27" id="chkInHousePacking" /> </td>
                                         <!--   <td colspan="100%">
                                                <input type="checkbox" data-index="27" id="chkInHousePacking" />
                                            </td>-->
                                          <td > 
                                              <label style="width:auto; padding-right:10px;"> Packed Qty. </label>
                                              <input type="text" data-index="28" id="Text1" />
                                            </td>
                                        </tr>
                                       <!--<tr>
                                            <td>
                                                Packed Qty.
                                            </td>
                                            <td>
                                                <input type="text" data-index="28" id="chkIMPackedQty" />
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td>
                                                <label> Dis1(%)</label>
                                                <input type="checkbox" data-index="29" id="chkIMDis1Per" />
                                           </td>
                                            <td>
                                                <input type="text" data-index="30" onkeypress="return isNumberKey(event)" id="txtIMDis1Per" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            <label> Dis2(%)</label>
                                                <input type="checkbox" data-index="31" id="chkIMDis2Per" />
                                              <!--  Dis2(%)-->
                                            </td>
                                            <td>
                                                <input type="text" data-index=32" onkeypress="return isNumberKey(event)" id="txtIMDis2Per" />
                                            </td>
                                        </tr>

                                         <tr class="basic-pack-info-table-tr">
                                            <td colspan="100%">
                                                <label style="width:auto;">Item Discontinued</label> <input type="checkbox" data-index="33" id="chkIMItemDiscontinued" />
                                            </td>
                                        </tr>
                                        <tr class="basic-pack-info-table-tr">
                                            <td colspan="100%">
                                                <label style="width:auto;">Coupon Printing</label> <input type="checkbox" data-index="34" id="chkIMCouponPrinting" />                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top" style="padding: 5px">
                                    <table class="all_tables basic_table_height master_info-tbl basic-other-info-tble" cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%; border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                OTHER INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="padding:0px;">
                                                <table class="day_table_info"cellspacing="5" cellpadding="5" style="width: 100%;color:Black; border-collapse: separate;">
                                                    <tr>
                                                        <td>
                                                            Days:
                                                        </td>
                                                        <td>
                                                            <input type="text" id="txtIMDays" onkeypress="return isNumberKey(event)" data-index="35" value="0" />
                                                        </td>
                                                        <td>
                                                            <label class="rdo_info">Expiry</label>
                                                            <input type="radio" checked="checked" data-index="36" name="expiry" id="rdoIMExpiry" />
                                                        </td>
                                                        <td>
                                                            <label class="rdo_info">Best Before</label>
                                                            <input type="radio"  name="expiry" data-index="37" id="rdoIMBestBefore" />
                                                        </td>
                                                    </tr>
                                                </table>
                                                <table class="veg_non_table" cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;">
                                                    <tr>
                                                        <td>
                                                            <label>Veg.</label>
                                                            <input type="radio" checked="checked" data-index="38"  name="veg" id="rdoIMVeg" />                                                        </td>
                                                        <td>
                                                            <label>Non Veg.</label>
                                                            <input type="radio" name="veg" data-index="39"  id="rdoIMNonVeg" />
                                                         </td>
                                                    </tr>
                                                </table>
                                                <table class="sub_item_info" cellspacing="5" cellpadding="5" style="width: 100%; border-collapse: separate;">
                                                    <tr>
                                                        <td>
                                                            <input type="checkbox" id="chkIMSubItem" data-index="40" /><label>Sub-Item</label>
                                                        </td>
                                                        <td>
                                                            <select id="ddlIMSubItem" data-index="41"   ></select>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Qty To Less
                                                        </td>
                                                        <td>
                                                            <input type="text" value="1" data-index="42" onkeypress="return isNumberKey(event)" class="validate required  float"  onkeypress="return isNumberKey(event)"  id="txtIMQtyToLess" style="color:black"/>
                                                        </td>
                                                    </tr>
                                                    <tr>

                                                          <td>
                                                            <input type="checkbox" id="chkTag" data-index="40" /><label>Tag</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="all_tables FormType stock_leveling_table" cellspacing="5" cellpadding="5" style="margin-top: 5px; border: solid 1px silver;
                                        width: 100%; border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                STOCK LEVELING
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Max Level
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMaxLevel" data-index="43" value="0" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                            <td>
                                                Min Level
                                            </td>
                                            <td>
                                                <input type="text" id="txtIMMinLevel"  value="0" data-index="44" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="100%">
                                                <table class="inner_table">
                                                    <tr>
                                                        <td>
                                                            Re. Order Qty.
                                                        </td>
                                                        <td colspan="100%">
                                                            <input type="text" value="0" data-index="45" onkeypress="return isNumberKey(event)" class="validate required  float"   id="txtIMReOrderOty"style="color:Black;" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table style="width:100%;">
                            <tr>
                                <td style="padding: 5px; vertical-align: top;" class="FormType">
                                    <table class="all_tables" cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                RATE LEVELING
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 2
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="46"  id="txtIMLevel2" onkeypress="return isNumberKey(event)" class="validate required float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 3
                                            </td>
                                            <td>
                                                <input type="text"  value="0" data-index="47" id="txtIMLevel3" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Level 4
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="48"  id="txtIMLevel4" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 5px; vertical-align: top;" class="FormType">
                                    <table class="all_tables" cellspacing="5" cellpadding="5" style="border: solid 1px silver; width: 100%;
                                        border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                DEALER INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                Cursor On(Qty/Rate)
                                            </td>
                                            <td>
                                                <select id="ddlIMCurQtyRate" data-index="49">
                                                <option value="(NONE)">(NONE)</option>
                                                <option value="NAME">Name</option>
                                                <option  value="QTY">Qty</option>
                                                <option  value="RATE">Rate</option>
                                                <option  value="AMOUNT">Amount</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Dealer Margin (%)
                                            </td>
                                            <td>
                                                <input type="text" data-index="50" value="0"  id="txtIMDealerMargin" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Qty In Case
                                            </td>
                                            <td>
                                                <input type="text" value="1" data-index="51"  id="txtIMQtyInCase"  onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding: 5px; vertical-align: top;" class="FormType">
                                    <table class="all_tables" cellspacing="5" cellpadding="5" style="border: solid 1px silver; height: auto; width: 100%;
                                        border-collapse: separate;">
                                        <tr>
                                            <th class="tables_headings basic_table_heading" colspan="100%">
                                                PURCHASE RATE INFORMATION
                                            </th>
                                        </tr>
                                        <tr>
                                            <td>
                                                With Tax
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="52"  id="txtIMWithTax" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                            <td>
                                                Latest Pur. Rate
                                            </td>
                                            <td>
                                                <input type="text" value="0" data-index="53"  id="txtIMLatestPurRate" onkeypress="return isNumberKey(event)" class="validate required  float" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Comments
                                            </td>
                                            <td colspan="100%">
                                                <textarea data-index="54" id="txtIMComments" ></textarea>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="middle" style="padding: 5px;vertical-align: top;">
                                    <table class="manage_item_btn basic_table_btn">
                                        <tr>
                                            <td>
                                                 <button class="btn btn-primary" data-index="55" id="btnIMAdd">
                                                  <i class="fa fa-save"></i>Save</button>
                                                  <button class="btn btn-danger" id="btnIMCancel">
                                                  <i class="fa fa-mail-reply-all"></i>  Cancel</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="display: none;" id="view2">
            <div style="height: 300px; overflow-y: scroll;">
                <table class="table" style="width: 100%">
                   <thead>
                    <tr>
                        <th>
                            Sr
                        </th>
                        <th>
                            Bold
                        </th>
                        <th>
                            Field1
                        </th>
                        <th>
                            Field2
                        </th>
                        <th>
                            Field3
                        </th>
                        <th>
                        </th>
                    </tr>

                    <tr><td></td><td style="position: relative;"><input type="checkbox" id="chkIMBold"/></td><td><input type="text" id="txtIMField1"/></td><td><input type="text" id="txtIMField2"/></td><td><input type="text" id="txtIMField3"/></td>
                    
                    <td>
                    <button type="button" class="btn btn-success"  id="btnAddNutrition">Add</button>
                    </td></tr>
                    </thead>
                   
                      <tbody id="tbNutritionInfo">
              
              
                      </tbody>
                </table>
            </div>
            <br />
            <div class="nutritoin-rmark-div">
                <span>Ingredients:</span> <textarea id="txtNutritionRemarks"></textarea>
            </div>
        </div>
        <div id="dvgrp" style="display:none">
				<div class="cross-td" id="closedvgrp"><i class="fa fa-times" aria-hidden="true"></i></div>
			<div class="dvgrp-main-div">
			
				<div class="title-div">
					<div class="head-div">Title</div>
					<div class="input-div"><input type="text" class="txt form-control"  id="txtGMTitle" ></div>
				</div>
				<div class="title-div">
					<div class="head-div">IsActive</div>
					<div  class="input-div"><input type="checkbox" id="chkGMIsActive" /></div>
				</div>
				<div class="title-div">
					<div class="head-div">ShowInMenu</div>
					<div  class="input-div"><input type="checkbox" id="chkGMshowinmenu" /></div>
				</div>
				 <div id="btnGMInsert" class="btn btn-primary btn-small" ><i class="fa fa-save"></i> Add</div>
			</div>
           
        </div>

         <div id="dvsbgrp" style="display:none">
			 <div class="cross-td" id="closesbgrp"><i class="fa fa-times" aria-hidden="true"></i></div>
			<div class="dvgrp-main-div">
			
				<div class="title-div">
					<div class="head-div">Title</div>
					<div class="input-div"><input type="text" class="txt form-control"  id="txtSMTitle" ></div>
				</div>
				<div class="title-div">
					<div class="head-div">IsActive</div>
					<div  class="input-div"><input type="checkbox" id="chkSMIsActive" /></div>
				</div>
				<div class="title-div">
					<div class="head-div">ShowInMenu</div>
					<div  class="input-div"><input type="checkbox" id="chkSMshowinmenu" /></div>
				</div>
				  <div id="btnSMInsert" class="btn btn-primary btn-small" ><i class="fa fa-save"></i> Add</div>
			</div>
           
        </div>

         <div id="dvcgrp" style="display:none">
			  <div class="cross-td" id="closecgrp"><i class="fa fa-times" aria-hidden="true"></i></div>
			<div class="dvgrp-main-div">
			
				<div class="title-div">
					<div class="head-div">Title</div>
					<div class="input-div"><input type="text" class="txt form-control"  id="txtCMTitle" ></div>
				</div>
				<div class="title-div">
					<div class="head-div">IsActive</div>
					<div  class="input-div"><input type="checkbox" id="chkCMIsActive" /></div>
				</div>
			
				    <div id="btnCMInsert" class="btn btn-primary btn-small" ><i class="fa fa-save"></i> Add</div>
			</div>
           
        </div>
    </div>
</div>
