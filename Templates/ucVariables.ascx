﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucVariables.ascx.cs" Inherits="Templates_ucVariables" %>

<script language="javascript" type="text/javascript">
    var VMID =0;
    var VMType = 0;
    $(document).ready(
    function () {
         VMID = $("#hdnVMID").val();
         VMType = $("#hdnVMType").val();
       

        if (VMType == 'Department')
        {
            $("#tdpos").css('display', 'block');
            var depid = $(".dd_pos option:selected").val();
        

        }
        if (VMID >=0) {

             $("#btnVMInsert").html("<i class='fa fa-save'></i> Update");
            $.ajax({
                type: "POST",
                data: '{"Id":"' + VMID + '","Type":"' + VMType + '"}',
                url: "VariableService.asmx/GetById",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);
                   

                    $("#txtVMTitle").val(obj.Variable.Title);
                    if (obj.Variable.IsActive == true) {
                        $('#chkVMIsActive').prop('checked', true);
                    }
                    else {
                        $('#chkVMIsActive').prop('checked', false);

                    }

                    if (obj.Variable.ShowInMenu == true)
                    {

                        $("#chkVMshowinmenu").prop('checked', true);

                    }
                    else {
                        $('#chkVMshowinmenu').prop('checked', false);

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }



    }

    );

    function InsertUpdateVariableMaster() { 
        VMID = $("#hdnVMID").val();
        VMType = $("#hdnVMType").val();
        var VMTitle = $("#txtVMTitle").val();

        if (VMType == 'Department') {
            var VMIsActive = false; 
            if ($("#chkVMIsActive").prop("checked")) {
                VMIsActive = true;
            }

            if ($("#chkVMshowinmenu").prop("checked")) {
                VMshowinmenu = true;
            }
            var depid = $(".dd_pos option:selected").val();
           

            $.ajax({
                type: "POST",
                data: '{"Id":"' + VMID + '","Title":"' + VMTitle + '","IsActive":"' + VMIsActive + '","Type":"' + VMType + '","posid":"' + depid + '","showinmenu":"' + VMshowinmenu + '"}',
                url: "VariableService.asmx/InsertUpdate_department",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);


                    if (obj.Variable.Id == -1) {
                        if (VMType == 'Color') {

                            VMType = 'Category';
                        }

                        alert(VMType + " " + "with duplicate name already exists");
                        return;
                    }



                    GetVMResponse(obj.Variable.Id, obj.Variable.Title, obj.Variable.IsActive, VMStatus, VMType)



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }
        else {


            var VMID = $("#hdnVMID").val();
            var VMTitle = $("#txtVMTitle").val();
            if (VMTitle.trim() == "") {
                $("#txtVMTitle").focus();
                return;
            }
            var VMIsActive = false;
            if ($("#chkVMIsActive").prop("checked")) {
                VMIsActive = true;
            }



            var VMType = $("#hdnVMType").val();
            var VMStatus = "U";
            if (VMID == -1) {
                VMStatus = "I";
            }

            $.ajax({
                type: "POST",
                data: '{"Id":"' + VMID + '","Title":"' + VMTitle + '","IsActive":"' + VMIsActive + '","Type":"' + VMType + '"}',
                url: "VariableService.asmx/InsertUpdate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);


                    if (obj.Variable.Id == -1) {
                        if (VMType == 'Color') {

                            VMType = 'Category';
                        }

                        alert(VMType + " " + "with duplicate name already exists");
                        return;
                    }



                    GetVMResponse(obj.Variable.Id, obj.Variable.Title, obj.Variable.IsActive, VMStatus, VMType)



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }
    
    }

</script>

<div id="divdefault">
<table class="table-condensed" style="width: 100%;">
    
<tr id="tdpos" style="display:none;width: 0;"><td  style="padding-left: 21px;">POS</td><td style="padding-left: 20px;"> <select id="ddpos" class="dd_pos form-control" style="width: 10em;height: 32px;">
    <asp:Repeater ID="rptr_ddpos" runat="server">
        <ItemTemplate>
 
    <option value='<%#Eval("posid") %>' ><%#Eval("title") %></option>
                   </ItemTemplate>   </asp:Repeater>                                   </select></td></tr>
<tr>
<td class="headings" style="text-align:left">Title</td>
<td colspan="3" style="text-align:left"> <input type="text" class="txt form-control" id="txtVMTitle" style="width: 100%;"/></td>
</tr>
<tr>
    <td></td>
    <td class="headings" style="text-align:left">IsActive <input type="checkbox" id="chkVMIsActive" /></td>
   <!-- <td style="text-align:left">

    ShowInMenu<input type="checkbox" id="chkVMshowinmenu"  style="margin-left: 5px;"/>

                     </td>-->
     <td class="headings" style="text-align:left">ShowInMenu <input type="checkbox" id="chkVMshowinmenu" /></td>

    
</tr>

                                            <tr>
                                             <td></td>
                                            <td    style="text-align:left">
                                          
                                            
                                             <div id="btnVMInsert" onclick="javascript:InsertUpdateVariableMaster();"  class="btn btn-primary btn-small" ><i class="fa fa-save"></i> Add</div>
                                             
                                             
                                             </td>
                                       
                                     
                                         
                                            </tr>

</table></div>



    <%--                            <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
             <script>

                 $(function () {
                     $('input[type="text"]').keyboard();

                     $('textarea').keyboard();
                    

                 });
                  </script>  --%>
  
    <script>  
        $(".txt").click(function () {
            $.ajax({
                type: "POST",
                //data: '{"Keyword":"' + Keyword + '"}',
                url: "billscreen.aspx/Keyboard",
                contentType: "application/json",
                dataType: "json",
                
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {


                }

            });  
        });
    </script>  

 


<asp:Literal ID="ltContent" runat="server"></asp:Literal>