﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="NewOrderDispatch.ascx.cs" Inherits="Templates_OrderTemplates_OrderDispatch" %>
   <script langauage="javascript" type="text/javascript">


      function BindPaymodes() {


          $.ajax({
              type: "POST",
              contentType: "application/json; charset=utf-8",
              url: "placeorder.aspx/GetPayModes",
              data: {},
              async: false,
              dataType: "json",
              success: function (msg) {


                  var obj = jQuery.parseJSON(msg.d);

				  var html = "<option value='0'>Cash</option><option value='0'>Credit</option>";
                  //html = html + "<option value='0'>Credit</option>";
                  for (var i = 0; i < obj.APaymodes.length; i++) {

                      html = html + "<option value='" + obj.APaymodes[i]["Id"] + "'>" + obj.APaymodes[i]["Name"] + "</option>";
                  }
                  $("#ddlPrefix").html(html);

              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {

              }
          });

      }
  
   $(document).ready(
    function () {

        BindPaymodes();

    }
);
  
  
  </script>

 
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="dispatch-oder-div">
		<div class="dispatch-order-field oder-no-f">
			<div class="dispatch-order-label">Order No </div>
			<div class="dispatch-order-input">
				<input type="text" id="txtOrderNo" readonly = "readonly"  class="form-control"  >
			</div>
		</div>
		<div class="dispatch-order-field oder-no-f">
			<div class="dispatch-order-label">Dated</div>
			<div class="dispatch-order-input">
				<input type="text" id="" readonly = "readonly"  class="form-control"  >
			</div>
		</div>
		<div class="dispatch-order-field cust-add">
			<div class="dispatch-order-label">Customer Name</div>
			<div class="dispatch-order-input">
				<textarea type="text"  id="txtCname" readonly = "readonly" class="form-control"  ></textarea>
			</div>
		</div>
		
	</div>
	                                   
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="dispatch-oder-div wht-div">
		<div class="dispatch-order-field wht-field">
			<div class="dispatch-order-label">Weigth</div>
			<div class="dispatch-order-input">
				<input type="text" id="txtWeightV" readonly = "readonly"  class="form-control"  >
			</div>
		</div>
		<div class="dispatch-order-field qty-field">
			<div class="dispatch-order-label">Qty</div>
			<div class="dispatch-order-input">
				<input type="text" id="txtQtyV" readonly = "readonly"  class="form-control"  >
			</div>
		</div>
		<div class="dispatch-order-field disp-field">
			<div class="dispatch-order-label">Dispatched</div>
			<div class="dispatch-order-input">
				<input type="text"  id="txtDispatchedV" readonly = "readonly" class="form-control"  >
			</div>
		</div>
		
	</div>
	                                   
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="dispatch-div" >
                               <%-- <div class="x_title">
                                    <h2>ORDER ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                      <div class="x_panel" style="max-height:300px;overflow-y:scroll;min-height:215px">
                               
                                <div class="x_content">--%>

                                    <table class="table table-striped" id ="tbDispatchProducts">
                                         <thead>
<tr><th style='display:none;'>Code</th><th>Name</th><th style='display:none;'>Weight</th><th style='display:none;'>Qty</th><th style='display:none;'>Dispatched</th><th>Rate</th><th>DispQty</th><th>Amount</th><th>Tax</th></tr>
</thead>

                                    </table>


                                 
                              <%--  </div>
                            </div>


                                </div>--%>
                              
                            </div>

</div>
<div class="ordre-info-div">
	<%--  <div class="x_title">
            <h2>ORDER INFO</h2>
                                   
            <div class="clearfix"></div>
        </div>--%>
	<div class="col-md-6 col-sm-6 col-xs-6">
		   
				 <form class="form-horizontal form-label-left input_mask dispatch-frm-place-odr">
					   <table id="tbCal" class="oder-adv-tbl">
						   <tbody>
							   <tr>
								      <td>Order Val</td>
								   <td><input type="text" id="txtOValue" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
								</tr>
							   <tr>
								  <td>Adv</td>
									<td>
									<input type="text" class="form-control input-small"  readonly=readonly style="width:70px;background-color:White" 
									id="txtAdv" />
									</td>
								</tr>

						   </tbody>
						</table>
				 </form>
		

	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<form class="form-horizontal form-label-left input_mask dispatch-frm-place-odr">
			<table class="bill-vat-tbl">
				<tr>
					  <td>Bill Val</td><td ><input type="text" id="txtBillValue" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
				</tr>
				<tr>	
					  <td>Gst Amt</td><td ><input type="text"  readonly=readonly id="txtTotalVat"  style="width:70px;background-color:White" class="form-control input-small"></td>
				</tr>
			</table>
		</form>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<form class="form-horizontal form-label-left input_mask dispatch-frm-place-odr">
			<table class="dis-net-tbl">
				<tr>
						<td>Dis</td>
     <td><input type="text" id="txtDiscountPer" readonly="readonly" style="width:32.5px;background-color:White;margin-top:2px;" class="form-control input-small"> <input type="text" id="txtDiscountAmt"  readonly=readonly  style="width:32.5px;background-color:White" class="form-control input-small"></td></td>
					</tr>
				<tr> 
					 <td>Net Amt</td><td ><input type="text" readonly=readonly  id="txtTotalNetAmount"  style="width:70px;background-color:White" class="form-control input-small"></td>
				</tr>
				<tr>
				<td >Last Bal</td><td ><input type="text"  readonly=readonly  id="txtLastBalance" style="width:70px;background-color:White" class="form-control input-small"></td>
			</tr>
			</table>
		</form>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-6">
		<table class="damt-tbl">
			<tr>
				 <td>D Amt</td><td ><input type="text" id="txtDispatchedAmt" readonly=readonly style="width:70px;background-color:White" class="form-control input-small"></td>
			</tr>
			<tr>	
				 <td>Bill Mode</td><td > <select id="ddlPrefix" style="width:70px;height:34px" class="form-control">
			</tr>
			<tr>
				<td>Rec Amt</td><td ><input type="text"  id="txtCashRecvd" style="width:70px;background-color:White" class="form-control input-small"></td>
			</tr>
		</table>
	</div>

</div>
<div class="damt-div">
	
	<%--<div class="col-md-6 col-sm-6 col-xs-6">
		<table class="last-bal-tbl">
			
			
		</table>
	</div>--%>
	<div class="col-md-12 col-sm-12 col-xs-12">
	    <table width="100%" class="order_dispatch_btns">
			<tr>
				<td>
		<button id="btnBillSave"  Style="padding:15px;font-size:20px;width:130px"  class="btn btn-primary">
                                       <i class="fa fa-save"></i>
                                       Bill&Save</button>
                                        <button id="Button1"  class="btn btn-danger"  Style="padding:15px;font-size:20px;width:130px"  >
                                       <i class="fa fa-mail-reply-all"></i>
                                       Cancel</button>
					</td>
				</tr>
			</table>
	</div>
	  
                                   
                          

                               
</div>