﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OrderBooking.ascx.cs" Inherits="Templates_OrderTemplates_OrderBooking" %>


<style type="text/css">

.table > tbody > tr > td
{
    padding:4px;
    }

.table > thead > tr > td
{
padding:4px;    
}


</style>

              <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel order_xpanel mobile_order_xpanel">
                             
                                <div class="x_content order_xcontant">
                                
                                   <table>
                                   <tr>
                                 <%--  <td>
                                   <label class="control-label">Payment Mode <span class="required">*</span>   
                                            </label>
                                   </td>
                                   <td>
                                    <select class="form-control"  data-bind="options:PaymentModes, 
    optionsText: 'Text', optionsValue: 'Value', value: PaymentMode, optionsCaption: 'Choose..'" style="width:100px">
                                                </select>
                                               
                                   </td>--%>

                                   
                                   <td><label  > Manual Order No</label></td>
                                   <td><input type="text"  data-bind="value:ManualOrderNo"  class="form-control" /></td>
                                   



                                   <td>
									  
                                      <input type="radio"  id="rdoCakeOrder" name="rdoCake" value="1" data-bind="checked:OrderType">
                    <label for="rdoCakeOrder">Order For Cake</label>
                                   </td>
                                   <td>
                                   <input type="radio"    id="rdoNonCake" name="rdoCake" value="2"  data-bind="checked:OrderType">
                      <label for="rdoNonCake">Order For Non Cake Items</label>
                                   </td>
<td> <asp:DropDownList ID="ddpos" runat="server" ClientIDMode="Static">
                                          
									   </asp:DropDownList></td>
                                       <%-- <td><select class="form-control" id="ddlPos" data-bind="options:Posids, 
                                                optionsText: 'Name', optionsValue: 'Id', value: PosMode, optionsCaption: 'PosId'" style="width:53px;color:#333;">
                                                    
                                                </select></td>--%>
                                   
                                   </tr>
                                   
                                   </table>
                                </div>

                                <div class="row">
                                <div class="col-md-12 col-sm-12">
                                <table style="width:100%"   >
                                <tr>
                                <td>
                                    <div class="x_panel xpanel_information">
                                        <div class="x_title">
                                            <h2>ORDER INFO</h2>
                                   
                                            <div class="clearfix"></div>
                                        </div>
                                <div class="x_content xpanel_information_contant">
             
                                        <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Order No</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <input type="text"  data-bind="value:OrderNo"readonly=readonly  class="form-control">
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Order Date</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <input type="text"  class="form-control" id="txtOrderDate" readonly data-bind="value:OrderDate" aria-describedby="inputSuccess2Status">
                                             
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Delivery Mode</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              <select class="form-control"  data-bind="options:DeliveryModes, 
                                                  optionsText: 'Text', optionsValue: 'Value', value: DeliveryType, optionsCaption: 'Choose..'">
                                                </select>
                                            </div>
                                        </div>

                                        
                                       

                                   <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Delivery Date</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <input type="text"  class="form-control" id="txtDeliveryDate" data-bind="value:DeliveryTime" aria-describedby="inputSuccess2Status">
                                       
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12" >Delivery Time</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                      
                                                                               <select class="form-control"  data-bind="options:DeliveryTimes,optionsText: 'Text', optionsValue: 'Value', value: DelTime, optionsCaption: 'Choose..'"></select>
                                                  <input type="text" id="txtMobile" style="display:none"  class="form-control" readonly="readonly"  data-bind="value:MobileNo">

                                                  
                                            </div>
                                        </div>

                                   

                                </div>
                            </div></td>
                             <!--   <td>
                                    <div class="x_panel xpanel_information">
                                        <div class="x_title">
                                            <h2>CUSTOMER INFO</h2>
                                   
                                            <div class="clearfix"></div>
                                        </div>
                                <div class="x_content xpanel_information_contant">
                                     
                                    <table style="width:100%">
                                    
                                    <tr><td>      
                        
                        <table cellspacing="0" cellpadding="0">
                                    <tr>
                                    <td><b>Search</b></td>
                                    <td style="padding-left:5px">

                                  

                                    <div class="input-group" style="margin-right:10px;color:#333" >
                                                           <input type="text" id="txtCustomerSearch" placeholder="Enter Name/Phone" class="form-control"  >
        
                                                 
                                                    <span class="input-group-btn" style="font-size:14px;color:#333">
                                                     
                                                  
                                           <%--<button type="button" class="btn btn-primary" data-bind="click: $root.findCustomerDetail">Go</button>--%>
                                        </span>




                                                </div>
                                    
                                

 <div id="dvCustSearchResult"    style="margin-top:-15px;display: none; width: 400px;color:#333; max-height: 400px; overflow-y: scroll; background: none repeat scroll 0% 0% white; position: absolute; z-index: 999;border:solid 1px silver;border-top:transparent">
 
<ul id="tbCustSearchResult">

</ul>

</div>  
                             </td>
                                  
                                    <td>
                                     
                                  <img src="images/search.png"   onclick="javascript:GetCustomerSearch()" style="width:25px;cursor:pointer"/>
  </td>    
                                    <td style="padding-left:10px">
                                    
                                          <img id="btnAddCustomer"  style="cursor:pointer;width:25px"  src="images/adduser.png"    />
                                    </td>
                                    </tr>
                                    
                                    </table>
                        
                                    
                                <%--     <table cellspacing="0" cellpadding="0">
                                    <tr><td><input type="text" id="txtMobile" class="form-control"  data-bind="value:MobileNo"></td>
                                    <td><button type="button" class="btn btn-success" data-bind="click: $root.findCustomerDetail">Search</button></td>
                                    <td>
                                    <img src="images/search.png" data-bind="click: $root.SearchCustomer"/>
 </td>    
                                    <td><button type="button" id="btnAddCustomer" class="btn btn-success" >Add</button></td>
                                    </tr>
                                    
                                    </table>--%>
                                    
                                    </td></tr>
                                        <tr>
                                        <td>    
                                        
      
        <input type="hidden" class="form-control" id="txtId" data-bind="value:Customer_ID" ></td>
        
        </tr>
                                    <tr>
                                    <td colspan="100%">
                                    <table style="width:100%">
                                    <tr>
                                    <td>        
                                     <label class="control-label">Name</label></td>
                                     
                                    <td style="padding-left:5px">     
                                     <input type="text" class="form-control" 
                                    readonly="readonly"
                                    id="txtCustomerName" data-bind="value:CustomerName";style="color:#333;" ></td>
                                    </tr>
                                    </table>

                                    
                                    </td>

                                    
                                    </tr>


                                   
                                    <tr><td>         <label class="control-label">Customer Address</label></td></tr>
                                    <tr><td> <textarea readonly="readonly"
 id="txtAddress" data-bind="value:Address" class="form-control" style="height:66px;color:#333">
                                             
                                             </textarea></td></tr>




                                   
                                    </table>

                                    
                                </div>
                            </div>
                                </td>-->

                                <td>
                                      <div class="x_panel xpanel_information">
                                            <div class="x_title">
                                                 <h2>CUSTOMER INFO</h2>
                                                 <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content xpanel_information_contant">
                                                <div class="x_content xpanel_information_contant">
                                                     <table style="width:100%" class="customer_info_order_table">
                                                        <tr>
                                                            <td class="srch-td"><b>Search</b></td>
                                                            <td>
                                                                <div class="input-group search_input_grp" style="margin-right:10px;color:#333" >
                                                                    <input type="text" id="txtCustomerSearch" maxlength="50" placeholder="Enter Name/Phone" class="form-control"  >
                                                                    <span class="input-group-btn" style="font-size:14px;color:#333"></span> <img class="src-img" src="images/search.png"   onclick="javascript:GetCustomerSearch()" style="width:23px;cursor:pointer"/>
                                                                 <img id="btnAddCustomer"  style="cursor:pointer;width:23px"  src="images/adduser.png"    />
                                                                </div>
                                                                <div id="dvCustSearchResult"    style="margin-top:-15px;display: none; width: 400px;color:#333; max-height: 400px; overflow-y: scroll; background: none repeat scroll 0% 0% white; position: absolute; z-index: 999;border:solid 1px silver;border-top:transparent">
                                                                    <ul id="tbCustSearchResult">

                                                                    </ul>

                                                                 </div> 
                                                             </td>
                                                         </tr> 
                                                         <tr>
                                                            <td>
                                                                <input type="hidden" class="form-control" id="txtId" data-bind="value:Customer_ID" >
                                                            </td>
                                                            
                                                        </tr>
                                                        <tr>
                                                            <td>        
                                                                <label class="control-label">Name</label>
                                                            </td>
                                                            <td>     
                                                                 <input type="text" class="form-control" readonly="readonly" id="txtCustomerName" data-bind="value:CustomerName";style="color:#333;" >
                                                            </td>
                                 
                                                         </tr>
                                                         <tr>
                                                            <td>
                                                                 <label class="control-label">Customer Address</label>
                                                             </td>
                                                            <td>
                                                                 <textarea readonly="readonly" id="txtAddress" data-bind="value:Address" class="form-control" style="height:66px;color:#333">
                                             
                                                                </textarea>
                                                             </td>
                                                          </tr>
                                                     </table>
                                                </div>

                                            </div>

                                        </div>
                                
                                </td>



                                <td>
                                    <div class="x_panel xpanel_information">
                                <div class="x_title">
                                    <h2>EMPLOYEE INFO</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content xpanel_information_contant">
                                    <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Choose Employee</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <select class="form-control"  data-bind="options:Employees,optionsText: 'Name', optionsValue: 'Code', value: Employee, optionsCaption: 'Choose..'"style="color:#333">
                                                </select>
                                            </div>
                                        </div>
                                    <div class="form-group">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Venue</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <textarea  class="form-control"  data-bind="value:DeliveryAddress" style="height:80px;color:#333">
                                                </textarea>
                                            </div>
                                        </div>

                                </div>
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>

 



                         
                    </div>


                <div class="row">
                  <div class="col-md-12 col-xs-12">
                            <div class="x_panel xpanel_information_middle">
                               
                                <div class="x_content xpanel_contant_middle">

                               <table>
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Weight</th>
                                        <th>Qty</th>
                                        <th>MRP</th>
                                        <th>Rate</th>
                                        <th>Amount</th>
                                        <th>TaxAmount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    <tr>
                                        <td><input type="text" id="txtSearch" class="form-control customTextBox" />
                                            <div id="dvSearchResult" style="display: none; width: 250px; max-height: 400px; overflow-y: scroll; background: none repeat scroll 0% 0% white; position: absolute; z-index: 999;border:solid 1px silver;border-top:transparent">
                                            <ul id="tbSearchResult">

                                            </ul>
                                            </div>
                                           <select style="width:154px;display:none" id="ddlItem"   class="form-control"></select>
                                        </td>
                                        <td>
                                          <input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox" data-bind="value:ItemCode"/>
                                        </td>
                                        <td>
                                            <input id="txtName" type="text"  class="form-control customTextBox" data-bind="value:ItemName"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtWeight"  class="form-control customTextBox" data-bind="value:Weight"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtQty" class="form-control customTextBox" data-bind="value:Qty"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtMrp" class="form-control customTextBox"  data-bind="value:MRP"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtRate"  class="form-control customTextBox" data-bind="value:Rate"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtAmount" class="form-control customTextBox"  readonly="readonly" data-bind="value:Amount"/>
                                        </td>
                                        <td>
                                          <input type="text"  class="form-control customTextBox" readonly="readonly" data-bind="value:TotalTax"/>
                                        </td>
                                        <td>
                                           <button type="button" id="addproduct" class="btn btn-success" data-bind="click: $root.AddProductToList">
                                          <i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                    </tbody>

</table>
                                 
                                </div>
                            </div>

                      
                        </div>
                    

                    

                          <div class="col-md-8 col-sm-8 col-xs-12">
                            <div class="x_panel xpanel_bottom">
                              <%--  <div class="x_title">
                                    <h2>ORDER ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content xpanel_contant_bottom">
                                    <table class="table table-striped detail-tble-oder-booking-temp">
                                         <thead>
                                            <tr><th>Name</th><th>Wt.</th><th>Qty</th><th>MRP</th><th>Rate</th><th>Amt</th><th>Delete</th></tr>
                                            </thead>
                                            <tbody data-bind="foreach:OrderProducts" >
                                            <tr  data-bind="click: $root.EditProduct" style="cursor:pointer" ><td data-bind="text:Name"> </td><td data-bind="text:Weight"> </td>
                                            <td data-bind="text:Qty"> </td><td data-bind="text:MRP"> </td><td data-bind="text:Rate"> </td><td data-bind="text:Amount"> </td>
                                            <%--<td data-bind="text:TaxPer"></td>
                                            <td data-bind="text:Surcharge"></td>--%>

                                            <td> 
                                            <img src="images/trashico.png"  data-bind="click: $root.RemoveProduct" style="cursor:pointer"  />

 
                                            </td>
                                            </tr>
                                            </tbody>
                                      </table>
                                </div>
                            </div>
                              <textarea type="text" class="form-control" id="txtRemarks" data-bind="value:Remarks" placeholder="Order Remarks"></textarea>
                      </div>

                               
                     

                           <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="x_panel xpanel_bottom">
                              <%--  <div class="x_title">
                                    <h2>ORDER INFO</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">
                                    
                                    <form class="form-horizontal form-label-left input_mask">
                                     <table id="tbCal">
                                         <tbody>
                                             <tr>
                                                <td style="width:100px">Order Value</td><td colspan="2"><input type="text" id="txtOrderValue" readonly=readonly data-bind="value:GrossAmount" style="width:100px;color:#333" class="form-control input-small"></td>
                                             </tr>
                                             <tr>  
                                                 <td>Discount</td>
                                                 <td><input type="text" data-bind="value:DisPer" style="width:45px;color:#333;background-color:White" class="form-control input-small"> <input type="text" id="txtDiscount" data-bind="value:DisAmt" readonly=readonly  style="width:51px;color:#333;background-color:White" class="form-control input-small"></td>
                                                 <td></td>
                                             </tr>
                                             <tr>
                                                <td class="after-dis-order-booking-temp">After Discount</td>
                                                 <td colspan="2"><input type="text" readonly=readonly  id="txtAfterDis" data-bind="value:AfterDisAmt" style="width:100px;color:#333;" class="form-control input-small"></td>
                                             </tr>
                                             <tr id="trVatAmt">
                                                 <td>Vat Amount</td>
                                                 <td colspan="2"><input type="text"  readonly=readonly id="txtVat" data-bind="value:VatAmount" style="width:100px;color:#333;" class="form-control input-small"></td>
                                             </tr>
                                             <tr>
                                                 <td>Net Amount</td>
                                                 <td colspan="2"><input type="text" readonly=readonly  id="txtNetAmount" data-bind="value:NetAmount" style="width:100px;color:#333;" class="form-control input-small"></td>
                                            </tr>
                                             <tr>
                                               <td>Advance</td>
                                               <td><input type="text" class="form-control input-small" data-bind="value:Advance" style="width:40px;color:#333;background-color:White" 
                                                  id="txtAdvance" /> 
                                                  <%-- <select class="form-control" id="ddlOrderPaymentModes" data-bind="value:PaymentMode", style="width:53px;color:#333;">
                                                    
                                                </select>--%>


<select class="form-control" id="ddlOrderPaymentModes" data-bind="options:PaymentModes, 
    optionsText: 'Name', optionsValue: 'Id', value: PaymentMode" style="width:60px">
                                                </select>


                                               </td>
                                             
                                               
                                                 
                                                 
                                                 <td>
                                               <%--      <select class="form-control" id="ddlOrderPaymentModes" data-bind="options:PaymentModes, 
                                                optionsText: 'Name', optionsValue: 'Id', value: PaymentMode, optionsCaption: 'Mode'" style="width:53px;color:#333;">
                                                    
                                                </select>--%>
                                                          

                                                 </td>

                                               <%--   <input type="text" class="form-control input-small" data-bind="value:Advance" style="width:100px;background-color:White" 
                                                  id="txtAdvance" />--%>
                                             </tr>

                                            <tr id="trCreditCardNo" style="display:none">
                                                <td>Card Number</td>
                                                <td colspan="2"><input type="text" id="txtCreditCardNumber" data-bind="value:CreditCardNumber" style="width:100px;color:#333;"/>
                                                </td>
                                            </tr> 
                                            <tr>
                                              <td>Balance</td>
                                              <td colspan="2"><input type="text"  readonly=readonly data-bind="value:LeftPayRecd" id="txtBalance" style="width:100px;color:#333;" class="form-control input-small"></td>
                                                
                                            </tr>
                                     </tbody>
                                 </table>
                                 </form>
                                </div>
                            </div>
                               <div class="order_panal_buttons place_order_btn">
                                    <div id="btnSave" data-bind="click: $root.PlaceOrder" class="btn btn-primary"><i class="fa fa-save"></i> Save</div>
                                    <button id="Button1" data-bind="click: $root.CancelOrder" class="btn btn-danger"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                </div>
                        </div>

                                   



                    </div>

              </div>
          </div>

<script>

    $(document).ready(function () {
        $("#addproduct").click(function () {

            if ($("#txtQty").val() == " ")
            {
                
                alert("Please enter the Qty.");
                return;
            }

        });
      

    });

</script>