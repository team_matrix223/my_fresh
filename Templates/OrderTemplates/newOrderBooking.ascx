﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="newOrderBooking.ascx.cs" Inherits="Templates_OrderTemplates_newOrderBooking" %>
<script language="javascript">

	function ClearCustomerDialog() {
		alert("hello");
	}
	



</script>
<div class="col-md-12 col-sm-12 col-xs-12">
	<div class="x_panel order_xpanel mobile_order_xpanel">

		<div class="x_content order_xcontant">
			<div class="manal-tbl">
				<div class="manual-order-field order-fiedl">
					<%--<div class="manual-order-label"><label>Manual Order No</label> </div>--%>
					<div class="manual-order-input">
						<input type="text" data-bind="value: ManualOrderNo" class="form-control" placeholder="Manual Order No" /></div>
				</div>
			
				<div class="manual-order-field drop-field">
					<div class="manual-input">
						<div class="customer-label">POS</div>
						<asp:DropDownList ID="ddpos" runat="server" ClientIDMode="Static">
						</asp:DropDownList>
					</div>
				</div>
				
			</div>
			<div class="oder-non-cake-div">
					<div class="manual-order-field cake-field">
					<div class="manual-input">

						<input type="radio" id="rdoCakeOrder" name="rdoCake" value="1" data-bind="checked: OrderType">
						<label for="rdoCakeOrder">Order For Cake</label>
					</div>
					
				</div>
				<div class="manual-order-field non-cake-field">
					<div class="manual-input">
						<input type="radio" id="rdoNonCake" name="rdoCake" value="2" data-bind="checked: OrderType">
						<label for="rdoNonCake">Order For Non Cake Items</label>

					</div>
				</div>
			</div>
 
			<div class="col-md-6 col-sm-6 col-xs-6">
				 <div class="customer-form order-no-field">
							<%--<div class="customer-label">Order No</div>--%>
							 <div class="customer-field order-no-input">
								  <input type="text"  data-bind="value: OrderNo"readonly=readonly  class="form-control">
							 </div>
				 <div class="customer-field oder-date-filed">
								   <input type="text"  class="form-control" id="txtOrderDate" readonly data-bind="value: OrderDate" aria-describedby="inputSuccess2Status">
							 </div>
				</div>
				<div class="customer-info-div delivery-info">
							
						
				  <div class="customer-form deli-field">
							<%--<div class="customer-label">Delivery Mode</div>--%>
							 <div class="customer-field mode-filed">
								  <select class="form-control"  data-bind="options: DeliveryModes,
	optionsText: 'Text', optionsValue: 'Value', value: DeliveryType, optionsCaption: 'Dlv Mode..'">
                                                </select>
							 </div>
						 </div>
				  <div class="customer-form deli-field delivery-date-fled">
							<%--<div class="customer-label">Delivery Date</div>--%>
							 <div class="customer-field">
								   <input type="text"  class="form-control" placeholder="Dlv Date" id="txtDeliveryDate" data-bind="value: DeliveryTime" aria-describedby="inputSuccess2Status">
							 </div>
						 </div>
				 
				  </div>
				<div class="customer-info-div employee-info-div">
								
                               
                               
                                  <%--	 <div class="customer-form ">
                                            <label class="control-label col-md-5 col-sm-5 col-xs-12">Choose Employee</label>
                                           <div class="customer-field">
                                                <select class="form-control"  data-bind="options: Employees, optionsText: 'Name', optionsValue: 'Code', value: Employee, optionsCaption: 'Choose Employee..'"style="color:#333">
                                                </select>
                                            </div>
                                        </div>--%>
								 <div class="customer-form">
							<%--<div class="customer-label">Delivery Time</div>--%>
							 <div class="customer-field time-div">
								  <select class="form-control"  data-bind="options: DeliveryTimes, optionsText: 'Text', optionsValue: 'Value', value: DelTime, optionsCaption: 'Dlv Time..'"></select>
                                                  <input type="text" id="txtMobile" style="display:none"  class="form-control" readonly="readonly"  data-bind="value: MobileNo">

							 </div>
						 </div>
                                   	 <div class="customer-form ">
                                            <%--<label class="control-label col-md-5 col-sm-5 col-xs-12">Venue</label>--%>
                                          <div class="customer-field">
                                                <textarea  class="form-control" placeholder="Venu" data-bind="value:DeliveryAddress" style="color:#333">
                                                </textarea>
                                            </div>
                                        </div>

                            
                         
					 </div>
			</div>

			<div class="col-md-6 col-sm-6 col-xs-6">
				<div class="search-part-div">
				 <div class="customer-form">
						<%--	<div class="customer-label">Search</div>--%>
							 <div class="customer-field ">
								 <div class="input-group search_input_grp" style="margin-right:10px;color:#333" >
                                    <input type="text" id="txtCustomerSearch" maxlength="50" placeholder="Search Name/Phone" class="form-control" data-bind="value: CustomerName";  >
                                    <span class="input-group-btn" style="font-size:14px;color:#333"></span> <img class="src-img" src="images/search.png"   onclick="javascript:GetCustomerSearch()" style="width:23px;cursor:pointer;display:none"/>
                                    <img id="btnAddCustomer"  style="cursor:pointer;width:23px"  src="images/adduser.png"    />
                                 </div>
                                 <div id="dvCustSearchResult"    style="margin-top:-15px;display: none; width: 400px;color:#333; max-height: 400px; overflow-y: scroll; background: none repeat scroll 0% 0% white; position: absolute; z-index: 999;border:solid 1px silver;border-top:transparent">
                                    <ul id="tbCustSearchResult">

                                    </ul>

								  </div> 
								  <input type="hidden" class="form-control" id="txtId" data-bind="value: Customer_ID" >
							 </div>
						 </div>
			</div>
				<div class="customer-info-div cust-info">
						 
						
						 <div class="customer-form " style="display:none;">
							<%--<div class="customer-label">Name</div>--%>
							 <div class="customer-field">
								<input type="text" class="form-control" readonly="readonly" placeholder="Name" id="txtCustomerName" data-bind="value: CustomerName";style="color:#333;" >
							 </div>
						 </div>
					 <%--<div class="customer-form ">
							<div class="customer-label">Name</div>
							 <div class="customer-field">
								<input type="text" class="form-control" readonly="readonly" placeholder="Phone no" id="" data-bind="";style="color:#333;" >
							 </div>
						 </div>--%>
						  <div class="customer-form ">
							<%--<div class="customer-label">Customer Address</div>--%>
							 <div class="customer-field">
								<textarea readonly="readonly" placeholder="Customer Address" id="txtAddress" data-bind="value: Address" class="form-control" style="color:#333">
                                             
                                                                </textarea>
							 </div>
						 </div>
					 </div>
			</div>

		</div>



	</div>
</div>



<div class="item-info" id="itemdet" >
	  <div class="row">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel xpanel_information_middle ">
                               
                                <div class="x_content xpanel_contant_middle">

                               <table>
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th style="display:none;">Code</th>
                                        <th style="display:none;">Name</th>
                                        <th>Weight</th>
                                        <th>Qty</th>
                                        <th style="display:none;">MRP</th>
                                      
                                    </tr>
                                </thead>
                                    <tbody>
                                    <tr>
                                        <td><input type="text" id="txtSearch" class="form-control customTextBox" data-bind="value:ItemName" />
                                            <div id="dvSearchResult" style="display: none; width: 250px; max-height: 400px; overflow-y: scroll; background: none repeat scroll 0% 0% white; position: absolute; z-index: 999;border:solid 1px silver;border-top:transparent">
                                            <ul id="tbSearchResult">

                                            </ul>
                                            </div>
                                           <select style="width:154px;display:none" id="ddlItem"   class="form-control"></select>
                                        </td>
                                        <td style="display:none;">
                                          <input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox" data-bind="value:ItemCode"/>
                                        </td>
                                        <td style="display:none;">
                                            <input id="txtName" type="text"  class="form-control customTextBox" data-bind="value:ItemName"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtWeight"  class="form-control customTextBox" data-bind="value:Weight"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtQty" class="form-control customTextBox" data-bind="value:Qty"/>
                                        </td>
                                        <td style="display:none;">
                                            <input type="text" id="txtMrp" class="form-control customTextBox"  data-bind="value:MRP"/>
                                        </td>
                                     
                                    </tr>
                                    </tbody>

</table>
                                 
									<table>
									  <thead>
											<tr>
												
												<th>Rate</th>
												<th>Amount</th>
												<th>TaxAmount</th>
												<th></th>
											</tr>
										</thead>
										 <tbody>
											<tr>
												   <td>
                                            <input type="text" id="txtRate"  class="form-control customTextBox" data-bind="value:Rate"/>
                                        </td>
                                        <td>
                                            <input type="text" id="txtAmount" class="form-control customTextBox"  readonly="readonly" data-bind="value:Amount"/>
                                        </td>
                                        <td>
                                          <input type="text"  class="form-control customTextBox" readonly="readonly" data-bind="value:TotalTax"/>
                                        </td>
                                        <td>
                                           <button type="button" id="addproduct" class="btn btn-success" data-bind="click: $root.AddProductToList">
                                          <i class="fa fa-plus"></i></button>
                                        </td>
											</tr>
										</tbody>
									</table>
                                </div>
                            </div>

                      
                        </div>
                    

                    

                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel xpanel_bottom">
                              <%--  <div class="x_title">
                                    <h2>ORDER ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content xpanel_contant_bottom">
                                    <table class="table table-striped detail-tble-oder-booking-temp">
                                         <thead>
                                            <tr><th>Name</th><th>Wt.</th><th>Qty</th><th style="display:none;">MRP</th><th>Rate</th><th>Amt</th><th>Tax</th><th>Delete</th></tr>
                                            </thead>
                                            <tbody data-bind="foreach:OrderProducts" >
                                            <tr  data-bind="click: $root.EditProduct" style="cursor:pointer" ><td data-bind="text:Name"> </td><td data-bind="text:Weight"> </td>
                                            <td data-bind="text:Qty"> </td><td style="display:none;" data-bind="text:MRP"> </td><td data-bind="text:Rate"> </td><td data-bind="text:Amount"> </td>
                                            <td data-bind="text:Vat"></td>
                                           <%-- <td data-bind="text:Surcharge"></td>--%>

                                            <td> 
                                            <img src="images/trashico.png"  data-bind="click: $root.RemoveProduct" style="cursor:pointer"  />

 
                                            </td>
                                            </tr>
                                            </tbody>
                                      </table>
                                </div>
                            </div>
                              
                      </div>

                               <div class="col-md-3-col-sm-3 col-xs-3">
								   <textarea type="text" class="form-control" id="txtRemarks" data-bind="value:Remarks" placeholder="Order Remarks"></textarea>
                               </div>
						<div  class="col-md-4 col-sm-4 col-xs-4">
							<div class="x_panel xpanel_bottom">
							<div class="x_content">
                                    
                                    <form class="form-horizontal form-label-left input_mask">
										<table class="adv-tbl">
											 <tr>
                                               <td>Adv</td>
                                               <td><input type="text" class="form-control input-small" data-bind="value:Advance" style="width:40px;color:#333;background-color:White" 
                                                  id="txtAdvance" />  <select class="form-control" id="ddlOrderPaymentModes" data-bind="value:PaymentMode", style="width:53px;color:#333;">
                                                    
                                                </select></td>
                                             
                                               
                                                 
                                                 
                                                 <td>
                                               <%--      <select class="form-control" id="ddlOrderPaymentModes" data-bind="options:PaymentModes, 
                                                optionsText: 'Name', optionsValue: 'Id', value: PaymentMode, optionsCaption: 'Mode'" style="width:53px;color:#333;">
                                                    
                                                </select>--%>
                                                          

                                                 </td>

                                               <%--   <input type="text" class="form-control input-small" data-bind="value:Advance" style="width:100px;background-color:White" 
                                                  id="txtAdvance" />--%>
                                             </tr>
											    <tr>
                                              <td>Bal</td>
                                              <td colspan="2"><input type="text"  readonly=readonly data-bind="value:LeftPayRecd" id="txtBalance" style="width:100px;color:#333;" class="form-control input-small"></td>
                                                
                                            </tr>
										</table>
									</form>
								</div>
								 <div class="order_panal_buttons place_order_btn">
                                    <div id="btnSave" data-bind="click: $root.PlaceOrder" class="btn btn-primary"><i class="fa fa-save"></i> Save</div>
                                    <button id="Button1" data-bind="click: $root.CancelOrder" class="btn btn-danger"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                </div>
								</div>
						</div>

                           <div class="col-md-5 col-sm-5 col-xs-5">
                            <div class="x_panel xpanel_bottom">
                              <%--  <div class="x_title">
                                    <h2>ORDER INFO</h2>
                                   
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">
                                    
                                    <form class="form-horizontal form-label-left input_mask">
                                     <table id="tbCal">
                                         <tbody>
                                             <tr>
                                                <td style="width:100px">Value</td><td colspan="2"><input type="text" id="txtOrderValue" readonly=readonly data-bind="value:GrossAmount" style="width:100px;color:#333" class="form-control input-small"></td>
                                             </tr>
                                             <tr>  
                                                 <td>Dis</td>
                                                 <td><input type="text" data-bind="value:DisPer" style="width:45px;color:#333;background-color:White" class="dis-cls form-control input-small dis-td"> <input type="text" id="txtDiscount" data-bind="value:DisAmt" readonly=readonly  style="width:51px;color:#333;background-color:White" class="form-control input-small"></td>
                                                 <td></td>
                                             </tr>
                                             <tr style="display:none;">
                                                <td class="after-dis-order-booking-temp">After Discount</td>
                                                 <td colspan="2"><input type="text" readonly=readonly  id="txtAfterDis" data-bind="value:AfterDisAmt" style="width:100px;color:#333;" class="form-control input-small"></td>
                                             </tr>
                                             <tr id="trVatAmt" style="display:none;">
                                                 <td>Vat Amount</td>
                                                 <td colspan="2"><input type="text"  readonly=readonly id="txtVat" data-bind="value:VatAmount" style="width:100px;color:#333;" class="form-control input-small"></td>
                                             </tr>
                                             <tr>
                                                 <td>Net</td>
                                                 <td colspan="2"><input type="text" readonly=readonly  id="txtNetAmount" data-bind="value:NetAmount" style="width:100px;color:#333;" class="form-control input-small"></td>
                                            </tr>
                                            

                                            <tr id="trCreditCardNo" style="display:none">
                                                <td>Card Number</td>
                                                <td colspan="2"><input type="text" id="txtCreditCardNumber" data-bind="value:CreditCardNumber" style="width:100px;color:#333;"/>
                                                </td>
                                            </tr> 
                                        
                                     </tbody>
                                 </table>
                                 </form>
                                </div>
                            </div>
                              

                        </div>

                                   



                    </div>
</div>


<%--<div class="col-md-6 col-sm-6 col-xs-12 delivery-info">
	 <div class="x_panel xpanel_information">
			
                                <div class="x_content xpanel_information_contant">
              
                                     

                                   

                                </div>
                            </div>
</div>--%>
