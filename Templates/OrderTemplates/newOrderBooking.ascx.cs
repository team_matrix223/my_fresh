﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Templates_OrderTemplates_newOrderBooking : System.Web.UI.UserControl
{
	protected void Page_Load(object sender, EventArgs e)
	{
		ddcustomertype();
	}

	public void ddcustomertype()
	{
		pos objpos = new pos();
		objpos.req = "bindgrid";
		DataTable dt = objpos.bindgride();
		ddpos.DataSource = dt;
		ddpos.DataTextField = "title";
		ddpos.DataValueField = "posid";
		ddpos.DataBind();
	}
}