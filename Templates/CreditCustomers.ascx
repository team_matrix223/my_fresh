﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CreditCustomers.ascx.cs" Inherits="Templates_CreditCustomers" %>
 <style type="text/css">
     .form-control {
         margin-bottom: 10px;
     }
 </style>
<script language="javascript">

    var m_CustomrId = -1;

    $(document).ready(


      function () {


          $('#cm_cr_CSTDate').daterangepicker({
              singleDatePicker: true,
              calender_style: "picker_1"
          }, function (start, end, label) {


              console.log(start.toISOString(), end.toISOString(), label);
          });





      });



    function BindInformation(CustomerId) {




        $.ajax({
            type: "POST",
            data: '{"AccountId":"' + CustomerId + '"}',
            url: "managecreditcustomers.aspx/GetByCustomerId",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                m_CustomrId = obj.Customer.AccountId;
                $("#cm_cr_ddlPrefix").val(obj.Customer.Prefix);
                $("#cm_cr_Name").val(obj.Customer.CNAME);
                $("#cm_cr_ShortName").val(obj.Customer.CNAME);
                $("#cm_cr_Address").val(obj.Customer.CADD1);
                $("#cm_cr_Address1").val(obj.Customer.CADD2);
                var area = obj.Customer.AREA_ID;
                $("#cm_cr_ddlArea").val(area);
                var city = obj.Customer.CITY_ID;
                $("#cm_cr_ddlCity").val(city);

                var state = obj.Customer.STATE_ID;
                $("#cm_cr_ddlState").val(state);

                $("#cm_cr_CreditLimit").val(obj.Customer.CR_LIMIT);
                $("#cm_cr_CreditDays").val(obj.Customer.CR_DAYS);
                $("#cm_cr_OpBal").val(obj.Customer.OP_BAL);

                $("#cm_cr_ddlOption").val(obj.Customer.DR_CR);
                $("#cm_cr_CSTNo").val(obj.Customer.CST_NO);
                $("#cm_cr_CSTDate").val(obj.Customer.strCSTDate);
                $("#cm_cr_TinNo").val(obj.Customer.TINNO);
                $("#cm_cr_TOTNo").val(obj.Customer.TOTNO);
                $("#cm_cr_ContactPerson").val(obj.Customer.CONT_PER);
                $("#cm_cr_ContactNumber").val(obj.Customer.CONT_NO);
                $("#cm_cr_Discount").val(obj.Customer.DIS_PER);
                $("#cm_cr_ddlAccSGroup").val(obj.Customer.SS_CODE);


                $('#cm_cr_DeliveryNote').prop("checked", false);


                if (obj.Customer.ACC_ZONE == "DELIVERY NOT") {
                    $('#cm_cr_DeliveryNote').prop("checked", true);
                }


                $("#CustomerDialog").dialog({
                    autoOpen: true,
					closeOnEscape:false,
                    width: 800,
                    resizable: false,
                    modal: true
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {
                $.uiUnlock();
            }
        });


    }


    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }


    function GetVMResponse(Id, Title, IsActive, Status, Type) {
        $("#dvVMDialog").dialog("close");

        var opt = "<option value='" + Id + "' selected=selected>" + Title + "</option>";
        if (Type == "Area") {

            $("#cm_cr_ddlArea").append(opt);

        }

        if (Type == "City") {

            $("#cm_cr_ddlCities").append(opt);

        }

        if (Type == "State") {

            $("#cm_cr_ddlState").append(opt);

        }



    }

    function OpenVMDialog(Type) {


        $.ajax({
            type: "POST",
            data: '{"Id":"' + -1 + '","Type": "' + Type + '"}',
            url: "managearea.aspx/LoadUserControl",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                $("#dvVMDialog").remove();
                $("body").append("<div id='dvVMDialog'/>");
                $("#dvVMDialog").html(msg.d).dialog({ modal: true, closeOnEscape:false });
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }
        });

    }

    function ClearCustomerDialog() {
        validateForm("detach");

        $("#cm_cr_ShortName").val();
        $("#cm_cr_Name").val("");
        $("#cm_cr_Address").val("");
        $("#cm_cr_Address1").val("");
        $("#cm_cr_CreditLimit").val("");
        $("#cm_cr_CSTDate").val("");
        $("#cm_cr_CSTNo").val("");
        $("#cm_cr_CreditDays").val("");
        $("#cm_cr_OpBal").val("");
        $("#cm_cr_TinNo").val("");
        $("#cm_cr_TOTNo").val("");
        $("#cm_cr_ContactPerson").val("");
        $("#cm_cr_ContactNumber").val("");
        $("#cm_cr_Discount").val("");
        $("#CustomerDialog").dialog("close");
        m_CustomrId = 0;
        $('#cm_cr_DeliveryNote').prop("checked", false);
    }
    $(document).ready(function () {
        $("#cm_cr_Name").keyup(function () {
            $("#cm_cr_Name").css('border', '1px solid #DDE2E8');
        })
        $("#cm_cr_ContactNumber").keyup(function () {
            $("#cm_cr_ContactNumber").css('border', '1px solid #DDE2E8');
        })
        $("#cm_cr_ShortName").keyup(function () {
            $("#cm_cr_ShortName").css('border', '1px solid #DDE2E8'); 
        })
    });
    function InsertUpdateCreditCustomer() {

        //if (!validateForm("myFormID")) {
        //    return;
        //}
        var Validation = true;
        if ($("#cm_cr_Name").val() == "") {
            Validation = false;
            $("#cm_cr_Name").css('border-color', '#b94a48');
        }
        else {
            $("#cm_cr_Name").css('border', '1px solid #DDE2E8');
        }
        if ($("#cm_cr_ContactNumber").val() == "") {
            Validation = false;
            $("#cm_cr_ContactNumber").css('border-color', '#b94a48');
        }
        else {
            $("#cm_cr_ContactNumber").css('border', '1px solid #DDE2E8');
        }
        if ($("#cm_cr_ShortName").val() == "")
        {
            Validation = false;
            $("#cm_cr_ShortName").css('border-color', '#b94a48');
        }
        else
        {
            $("#cm_cr_ShortName").css('border', '#1px solid #DDE2E8');
        }
        if (Validation == false) {
            return false;
        }
        var objAccLedger = {};
        objAccLedger.AccountID = m_CustomrId;
        objAccLedger.CODE = "D";
        objAccLedger.SS_CODE = $("#cm_cr_ddlAccSGroup").val();
        objAccLedger.CNAME = $("#cm_cr_Name").val();
        objAccLedger.CADD1 = $("#cm_cr_Address").val();
        objAccLedger.CADD2 = $("#cm_cr_Address1").val();

        if ($("#cm_cr_ddlArea").val() == 0) {
            objAccLedger.Area_ID = 0;
        }
        else {
            objAccLedger.Area_ID = $("#cm_cr_ddlArea").val();
        }
        objAccLedger.CITY_ID = $("#cm_cr_ddlCities").val();
        objAccLedger.State_ID = $("#cm_cr_ddlState").val();

        objAccLedger.CST_NO = "";
        objAccLedger.CST_DATE = "";
        objAccLedger.TINNO = $("#cm_cr_TinNo").val();
        objAccLedger.TOTNO = "";
        objAccLedger.CR_LIMIT = $("#cm_cr_CreditLimit").val();
        objAccLedger.CR_DAYS = $("#cm_cr_CreditDays").val();

        objAccLedger.CONT_PER = $("#cm_cr_ContactPerson").val();
        objAccLedger.CONT_NO = $("#cm_cr_ContactNumber").val();
        objAccLedger.OP_BAL = $("#cm_cr_OpBal").val();
        objAccLedger.DR_CR = $("#cm_cr_ddlOption").val();

        objAccLedger.DIS_PER = $("#cm_cr_Discount").val();
        objAccLedger.PREFIX = $("#cm_cr_ddlPrefix").val();



        var AccZone = "";
        if ($('#cm_cr_DeliveryNote').is(":checked")) {
            AccZone = "DELIVERY NOT";
        }
        objAccLedger.ACC_ZONE = AccZone;

        var DTO = { 'objAccLedger': objAccLedger };

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "managecreditcustomers.aspx/InsertUpdateCustomer",
            data: JSON.stringify(DTO),
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                if (obj.Status == -1) {


                    alert("An error occured during transaction. Please try again.");
                    return;
                }
                else {
                    alert("Customer Saved Successfully");
                    ClearCustomerDialog();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


            }

        });


    }
       
          </script>

 <div class="x_panel" id="myFormID">
                                <div class="x_title">
                                    <h2>Credit Customer Information</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                        </li>
                                        <li class="dropdown">
                                            <a aria-expanded="false" role="button" data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-wrench"></i></a>
                                            <ul role="menu" class="dropdown-menu">
                                                <li><a href="#">Settings 1</a>
                                                </li>
                                                <li><a href="#">Settings 2</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <br>
                                    <form class="form-horizontal form-label-left" data-parsley-validate="" id="formID" novalidate="">

                             <table width="100%">
                             <tr><td valign="top">
                            <%--  
                               <div class="form-group">
                                          <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Prefix <span class="required">*</span>
                                            <</label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                           
                                       
                                         
                                            </div>
                                        </div>--%>
     


                              <div class="form-group">
                                            <label for="first-name" class="control-label  col-md-3 col-sm-5 col-xs-3">Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9 cls-name-credit-cust">
                                             <asp:DropDownList  class="form-control col-md-3 col-xs-12" ID="cm_cr_ddlPrefix" ClientIDMode="Static" runat="server" style="width: 92px; margin-right:18px;">
                                             </asp:DropDownList>
                                              <input type="text"   class="form-control col-md-4 col-xs-12" id="cm_cr_Name" style="width: 143px; margin-right:13px;" placeholder="Customer Name"> 
                                                   <input type="text"   class="form-control col-md-7 col-xs-12" style="width: 159px;" id="cm_cr_ShortName" placeholder="Short Name"> 
                                            </div>
                                        </div>
     
                            <%--<div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Short Name <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                            </div>
                                        </div>--%>
                                  
                             <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Address1 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12  validate required" id="cm_cr_Address"  required></textarea>
                                             </div>
                                        </div>

                                       <%--   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">Address2 <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                              <textarea class="form-control col-md-7 col-xs-12  validate required" id="cm_cr_Address1"  required></textarea>
                                             </div>
                                        </div>--%>


                                      
                                     <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Area <span class="required">*</span>
                                            </label>

                                         <div class="col-md-7 col-sm-7 col-xs-9">
                                          <table>

                                              <tr>
                                                  <td> <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_cr_ddlArea" ClientIDMode="Static"  runat="server"></asp:DropDownList></td>
                                                  <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                    <span class="fa fa-plus btn-add-plus" onclick="javascript:OpenVMDialog('Area')"  style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                  </td>
                                                   <td>
                                                      <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_cr_ddlCities" ClientIDMode="Static" style="width:168px"  runat="server"></asp:DropDownList>
                                                 </td>
                                                 <td  style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                     <span class="fa fa-plus" onclick="javascript:OpenVMDialog('City')"  style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>
                                                 </td>
                                                   <td>
                                                     <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_cr_ddlState" ClientIDMode="Static"  runat="server"></asp:DropDownList>

                                                        </td>
                                                        <td style="vertical-align:middle;margin:0px;padding:0px;padding:3px">
                                                        <span class="fa fa-plus" onclick="javascript:OpenVMDialog('State')" style="font-size:18px; ;cursor:pointer;margin-top:-5px"></span>

                                                        </td>
                                              </tr>
                                          </table>
                                            </div>
                                        </div>

                                       <%-- <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">City <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                         <table>
                                             <tr>
                                                
                                             </tr>
                                         </table>
                                            </div>
                                        </div>


                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">State <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                                <table>
                                                    <tr>
                                                       
                                                    </tr>

                                                </table>
                                          
                                      
                                         
                                            </div>
                                        </div>--%>


                                  <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Op. Bal <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">

                                           <table>
                                            <tr><td>
                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required float" value="0.00" style="width:100px"  id="cm_cr_OpBal" onkeypress="return isNumberKey(event)"> 

                                                </td>
                                            <td style="padding-left:10px">
                                        
                                            <select id="cm_cr_ddlOption" style="width:50px;height:30px;margin-top:0px" class="validate ddlrequired" >
                                            <option value="DR">DR</option>
                                            <option value="CR">CR</option></select>
                                                </td>

                                                </tr>

                                                </table>
                                         
                                            </div>
                                        </div>


                                 <%--Second div %>
                                               </div>--%>
                                   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">TIN No <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">
                                            <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber" value="0"   id="cm_cr_TinNo" onkeypress="return isNumberKey(event)" style="width:38%"> 
                                            </div>
                                        </div>

                                                               <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Contact Person <span class="required" style="width:38%">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">

                                            <input type="text" class="form-control col-md-7 col-xs-12"  required="required" id="cm_cr_ContactPerson" style="width:38%"> 
                                           
                                            </div>
                                        </div>

                                          <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Contact Number 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">

                                              <input type="text" class="form-control col-md-7 col-xs-12 validate required valNumber"  required="required" id="cm_cr_ContactNumber" onkeypress="return isNumberKey(event)" maxlength = "10" style="width:38%"> 
                                           
                                            </div>
                                        </div>



                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Discount(%) 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">


                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required float" value="0.00"  id="cm_cr_Discount" onkeypress="return isNumberKey(event)" style="width:38%" /> 
                                            </div>
                                        </div>


                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Acc SubGroup <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">

                                           <asp:DropDownList  class="form-control col-md-7 col-xs-12" ID="cm_cr_ddlAccSGroup" ClientIDMode="Static"  runat="server" style="width:38%"></asp:DropDownList>
                                      
                                         
                                            </div>
                                        </div>


                                           <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Credit Limit <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">


                                                <input type="text"   class="form-control col-md-7 col-xs-12 validate required float" value="0.00" style="width:38%" id="cm_cr_CreditLimit" onkeypress="return isNumberKey(event)"> 
                                            </div> 
                                        </div>


                                         <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">Credit Days <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">
                                                <input type="text"   class="form-control col-md-7 col-xs-12 validate required valNumber" style="width:38%" value="0" id="cm_cr_CreditDays" onkeypress="return isNumberKey(event)"> 
                                            </div> 
                                        </div>
                                        <div class="form-group">
                                            <label for="first-name" class="control-label col-md-3 col-sm-5 col-xs-3">DeliveryNote 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-9">

                                            <input type="checkbox" id="cm_cr_DeliveryNote"  />
                                           
                                            </div>
                                        </div>
                                            
                                          

                                         
                             </td>
                             
                            <%-- <td valign="top">--%>
                                        <%--<div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">CST No 
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">

                                                <input type="text" class="form-control col-md-7 col-xs-12 validate required"    id="cm_cr_CSTNo"> 
                                            </div>
                                        </div>--%>
                                       

                                       <%--   <div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">CST Date <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">
                                              
                                             <input type="text" class="form-control col-md-7 col-xs-12 validate required"    id="cm_cr_CSTDate"> 
                                             </div>
                                        </div>--%>


                                       

                 

                                                            <%--<div class="form-group">
                                            <label for="first-name" class="control-label col-md-5 col-sm-5 col-xs-12">TOT No <span class="required">*</span>
                                            </label>
                                            <div class="col-md-7 col-sm-7 col-xs-12">


                                                <input type="text" class="form-control col-md-7 col-xs-12"  required="required" id="cm_cr_TOTNo"> 
                                            </div>
                               
                                                           
                                       
                                               
                                       
                                            
                                          <%--  </td>--%>
                                            
                                            </tr>
                                            </table>
                                            
                                           

                                            
                                        </div>

                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                                
                                                <button class="btn btn-primary" type="button" onclick="javascript:InsertUpdateCreditCustomer()"><i class="fa fa-external-link"></i> Submit</button>
                                           <button class="btn btn-danger" onclick="javascript:ClearCustomerDialog()" type="button"><i class="fa fa-mail-reply-all"></i> Cancel</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>