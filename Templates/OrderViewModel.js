﻿
/// <reference path="../Scripts/knockout-3.3.0.js" />
/// <reference path="../Scripts/jquery-2.0.3.min.js" />
/// <reference path="../Scripts/knockout.validation.js" />


function DeliveryMode(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}

function PayMode(data) {
    this.Name = ko.observable(data.Name);
    this.Id = ko.observable(data.Id);


}

function PaymentMode(data) {
    this.Text = ko.observable(data.Id);
    this.Value = ko.observable(data.Name);


}

function Prefix(data) {
    this.Text = ko.observable(data.Text);
    this.Value = ko.observable(data.Value);


}

function Employee(data) {
    this.Name = ko.observable(data.Name);
    this.Code = ko.observable(data.Code);

}

function Customer(data) {
    this.Customer_ID = ko.observable(data.Customer_ID);
    this.Customer_Name = ko.observable(data.Customer_Name);
    this.Address_1 = ko.observable(data.Address_1);


}


function Product(data) {

    this.Code = ko.observable(data.Code);
    this.Name = ko.observable(data.Name);
    this.Weight = ko.observable(data.Weight);
    this.Qty = ko.observable(data.Qty);
    this.MRP = ko.observable(data.MRP);
    this.Rate = ko.observable(data.Rate);
    this.Amount = ko.observable(data.Amount);
    this.Vat = ko.observable(data.Vat);
    this.TaxPer = ko.observable(data.TaxPer);
    this.Surcharge = ko.observable(data.Surcharge);
}




function OrderViewModel() {
    var self = this;



    self.DispatchedQty = ko.observable();

    self.OrderNo = ko.observable();
    self.OrderDate = ko.observable().extend({ required: true }); ;
    self.Customer_ID = ko.observable();
    self.CustomerName = ko.observable().extend({ required: true }); ;
    self.Address = ko.observable();
    self.MobileNo = ko.observable();
    self.Employee = ko.observable();
    self.DeliveryType = ko.observable();
    self.DeliveryTime = ko.observable();
    self.DeliveryAddress = ko.observable();
    self.Advance = ko.observable(0);
   
    self.Remarks = ko.observable();
    self.OrderValue = ko.observable();
 

    self.OrderType = ko.observable("2");
  

    self.PaymentMode = ko.observable("").extend({
        required: { message: "*" }
    }); ;
    self.Employees = ko.observableArray([]);
    self.PayModes = ko.observableArray([]);
    self.DeliveryModes = ko.observableArray([]);
    self.DeliveryModes.push(new DeliveryMode({
        Text: "By Hand",
        Value: "ByHand"

    }));
    self.DeliveryModes.push(new DeliveryMode({
        Text: "By Delivery",
        Value: "ByDelivery"

    }));

    self.PaymentModes = ko.observableArray([]);
    //self.PaymentModes = ko.observableArray([]);
    //self.PaymentModes.push(new PaymentMode({
    //    Text: "Cash",
    //    Value: "Cash"

    //}));
    //self.PaymentModes.push(new PaymentMode({
    //    Text: "Credit",
    //    Value: "Credit"

    //}));

    self.OrderProducts = ko.observableArray([]);
    self.ItemCode = ko.observable();
    self.ItemName = ko.observable();
    self.Weight = ko.observable();
    self.Qty = ko.observable(0);
    self.Rate = ko.observable(0);
    self.MRP = ko.observable(0);
    self.TaxAmount = ko.observable(0);

    self.TaxPer = ko.observable(0);
    self.Surcharge = ko.observable(0);
    self.BillAmount = ko.observable(0);

   

   
    self.Amount = ko.computed(function () {
        var total = 0;
        total = self.Qty() * self.Rate();

        return total.toFixed(2);
    }, self);




    self.TotalTax = ko.computed(function () {
        var total = 0;


        var TaxAmt = (self.Qty() * self.Rate()) * self.TaxPer() / 100;
        
              var SurchargeAmt = (TaxAmt * self.Surcharge()) / 100;



             total = TaxAmt + SurchargeAmt;

          

        return total.toFixed(2);
    }, self);



    self.GrossAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.OrderProducts(), function (item) {

            var value = parseFloat(item.Rate()) * parseFloat(item.Qty());
            if (!isNaN(value)) {
                total += value;
            }
        });
        return total.toFixed(2);
    }, self);




    self.DisPer = ko.observable(0);


    self.DisAmt = ko.computed(function () {
        var total = 0;
        total = self.GrossAmount() * self.DisPer() / 100;
     
        return total.toFixed(2);
    }, self);

    self.AfterDisAmt = ko.computed(function () {
        var total = 0;
        total = self.GrossAmount() - self.DisAmt();
        return total.toFixed(2);
    }, self);


    self.VatAmount = ko.computed(function () {
        var total = 0;
        ko.utils.arrayForEach(self.OrderProducts(), function (item) {

            var DisAmt = (item.Qty() * item.Rate()) * self.DisPer() / 100;
            var TotalAmt = (item.Qty() * item.Rate()) - parseFloat(DisAmt);

            var TaxAmt = TotalAmt * item.TaxPer() / 100;

            var SurchargeAmt = (TaxAmt * item.Surcharge()) / 100;

       

            total += TaxAmt + SurchargeAmt;



            //var value =  parseFloat(item.Vat());
            //if (!isNaN(value)) {
            //    total += value;
            //}
        });
        return total.toFixed(2);
    }, self);


    self.NetAmount = ko.computed(function () {
        var total = 0;
        total = parseFloat(self.AfterDisAmt()) + parseFloat(self.VatAmount());
        return total.toFixed(2);
    }, self);

    self.LeftPayRecd = ko.computed(function () {
        var total = 0;
        total = self.NetAmount() - self.Advance();
        return total.toFixed(2);
    }, self);

    self.BillValue = ko.observable(0);


   

//    self.SearchCustomer = function (product) {



//        $.ajax({
//            type: "POST",
//            url: "placeorder.aspx/LoadUserControl",
//            data: "{message: '" + '' + "'}",
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (r) {

//                $("#dvCustomerSearch").remove();
//                $('body').append("<div id='dvCustomerSearch'></div>");
//                $("#dvCustomerSearch").html(r.d);


//                $('#dvCustomerSearch').dialog({ autoOpen: true,

//                    width: 800,
//                    resizable: false,
//                    modal: true
//                });

//            }
//        });





//    };


    self.CancelOrder = function (product) {

        alert(self.MobileNo());
        self.MobileNo("");
        alert(self.MobileNo());
        ClearData();

    }

    //self.DispatchOrder = function () {

    //    self.OrderNo(m_OrderNo);
    //    if (self.OrderNo() == "0") {
    //        alert("No Order is selected for Dispatch");
    //        return;
    //    }


    //    $.ajax({
    //        type: "POST",
    //        url: 'placeorder.aspx/GetByOrderNo',
    //        data: ko.toJSON({ OrderNo: m_OrderNo }),
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json",
    //        success: function (results) {
    //            var obj = jQuery.parseJSON(results.d);
    //            self.OrderNo(m_OrderNo);
    //            self.OrderDate(obj.Booking.strOD);
    //            self.CustomerName(obj.Booking.CustomerName);

    //            self.Advance(obj.Booking.Advance);
    //            self.PaymentMode(obj.Booking.PaymentMode);

    //            var products = $.map(obj.BookingData, function (item) {
    //                return new Product(item)
    //            });
    //            alert(products);
    //            self.OrderProducts(products);

    //            var DisPerc = self.DisAmt() * 100 / self.GrossAmount();
    //            self.DisPer(DisPerc);
    //        },
    //        error: function (err) {
    //            alert(err.status + " - " + err.statusText);
    //        }
    //    });

    //    //Ajax Function Get By Order No..Return-- ObjBooking and ProductData On Success Show Dialog


    //    $('#dispatchDialog').dialog({ autoOpen: true,

    //        width: 1100,
    //        resizable: false,
    //        modal: true
    //    });



    //}

    self.EditOrder = function () {

   

       
        if (self.OrderNo() == "0") {
            alert("No Order is selected for editing");
            return;
        }

        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/GetByOrderNo',
            data: ko.toJSON({ OrderNo: m_OrderNo }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var obj = jQuery.parseJSON(results.d);
                self.OrderNo(m_OrderNo);
                self.OrderDate(obj.Booking.strOD);
                self.Customer_ID(obj.Booking.Customer_ID);
                self.CustomerName(obj.Booking.CustomerName);
                self.MobileNo(obj.Booking.MobileNo);
       
                self.DeliveryType(obj.Booking.DeliveryType);
                self.DeliveryTime(obj.Booking.strDD);
                self.DeliveryAddress(obj.Booking.DeliveryAddress);
                self.Address(obj.Booking.Address);
                self.Advance(obj.Booking.Advance);
                self.PaymentMode(obj.Booking.PaymentMode);



              

                var products = $.map(obj.BookingData, function (item) {
                    return new Product(item)
                });
            
                self.OrderProducts(products);
              
               var DisPerc = self.DisAmt() * 100 / self.GrossAmount();
               self.DisPer(DisPerc);
            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });

        //Ajax Function Get By Order No..Return-- ObjBooking and ProductData On Success Show Dialog


        $('#orderDialog').dialog({ autoOpen: true,

            width: 1100,
            resizable: false,
            modal: true
        });

    };


    self.PlaceOrder = function (product) {



        self.removeErrors();
        if (self.hasError()) {
            self.showErrors();
            return;
        }
        if (product.Advance != 0) {
            if (product.PaymentMode == 0) {
                alert("choose Advance Mode");
            }

        }

        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/InsertUpdate',
            data: ko.toJSON({ booking: product, OrderProducts: self.OrderProducts }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (results) {
                var obj = jQuery.parseJSON(results.d);

       

             
                BindGrid();
              
                $('#orderDialog').dialog("close");

                if (product.OrderNo() == "0") {
                    alert("Thanks for placing order with NikBakers. Your Order Number Is:" + obj.Booking.OrderNo + ".");
                   
                }
                else {

                    alert("Order Updated Successfully");
                }
                ClearData();
            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }

  


      

    function ClearData() {
        self.OrderProducts([]);
        self.OrderNo("");
        self.OrderDate("");
        self.Customer_ID("");
        self.CustomerName("");
        self.Address("");
        self.MobileNo("")
        self.Employee (0);
        self.DeliveryMode("");
        self.DeliveryType("");
        self.DeliveryTime("");
        self.DeliveryAddress("");
        self.Advance(0);
        self.Remarks("");
        self.OrderValue(0);
        self.OrderType(0);
        self.PaymentMode("")
        self.Employees([]);
        self.PayModes([]);
        self.DeliveryModes([]);
       
       



    
        self.ItemCode("");
        self.ItemName("");
        self.Weight(0);
        self.Qty(0);
        self.Rate(0);
        self.MRP(0);
        self.TaxAmount(0);
        self.TaxPer(0);
        self.Surcharge(0);
        self.BillAmount(0);
        self.Amount(0);
        self.TotalTax(0);
        self.GrossAmount(0);
        self.DisPer(0);
        self.DisAmt(0);
        self.AfterDisAmt(0);
        self.VatAmount(0);
        self.NetAmount(0);
        self.LeftPayRecd(0);

        $('#orderDialog').dialog("close");


    }



    self.RemoveProduct = function (product) {
        self.OrderProducts.remove(product);

      

        //        $.notifyBar({ cssClass: "success", html: "" + product.Item_Name() + " REMOVED FROM YOUR ORDER !" });


    }



    self.AddProductToList = function (product) {




        var match = ko.utils.arrayFirst(self.OrderProducts(), function (line) {
            return line.Code() === product.ItemCode();

        });

        if (!match) {




            self.OrderProducts.push(new Product({
                Code: product.ItemCode(),
                Name: product.ItemName(),
                Weight: product.Weight(),
                Qty: product.Qty(),
                MRP: product.MRP(),
                Rate: product.Rate(),
                Amount: product.Amount(),
                Vat: product.TotalTax(),
                TaxPer: product.TaxPer(),
                Surcharge:product.Surcharge()

            }));


            self.ItemCode("");
            self.ItemName("");
            self.Weight(1);
            self.Qty(0);
            self.MRP(0);
            self.Rate(0);
            self.TotalTax(0);

        }
        else {

            alert("Item Code Already Added to List");
        }






    };



    self.errors = ko.validation.group([self.CustomerName, self.Address, self.MobileNo]);
    self.hasError = function () {
        return self.errors().length > 0;
    };


    self.showErrors = function () {
        self.errors.showAllMessages();
    };
    self.removeErrors = function () {
        self.errors.showAllMessages(false);
    };




    $.ajax({
        type: "POST",
        url: 'placeorder.aspx/FetchPayModes',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var paymodes = $.map(results.d, function (item) {



                return new PaymentMode(item)
            });


            self.PaymentModes(paymodes);



        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });


    $.ajax({
        type: "POST",
        url: 'placeorder.aspx/FetchEmployees',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (results) {
            var employees = $.map(results.d, function (item) {



                return new Employee(item)
            });


            self.Employees(employees);



        },
        error: function (err) {
            alert(err.status + " - " + err.statusText);
        }
    });


    $.ajax({
        type: "POST",
        data: '{}',
        url: "placeorder.aspx/GetByItemType",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

            var obj = jQuery.parseJSON(msg.d);

            $("#ddlItem").html(obj.ProductOptions);

        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {


        }


    });


    $("#ddlItem").change(
    function () {
        self.ItemCode($("#ddlItem option:selected").attr("value"));
        self.ItemName($("#ddlItem option:selected").attr("Name"));
        self.Qty($("#ddlItem option:selected").attr("Qty"));
        self.MRP($("#ddlItem option:selected").attr("MRP"));
        self.Rate($("#ddlItem option:selected").attr("Rate"));
       self.TaxPer($("#ddlItem option:selected").attr("TaxPer"));
       self.Surcharge($("#ddlItem option:selected").attr("SurValue"));

        self.Weight(1);
     


    });

    self.findCustomerDetail = function () {

        alert(self.MobileNo());
        $.ajax({
            type: "POST",
            url: 'placeorder.aspx/GetByMobileNo',
            data: '{ "MobileNo": "' + self.MobileNo() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {


                var obj = jQuery.parseJSON(msg.d);

                if (obj.Customer.Customer_ID == 0) {
                    alert("Sorry, Mobile No is Not Registered with Our Database");
                    return;
                }


                self.Customer_ID(obj.Customer.Customer_ID);
                self.CustomerName(obj.Customer.Customer_Name);
                self.Address(obj.Customer.Address_1);


            },
            error: function (err) {
                alert(err.status + " - " + err.statusText);
            }
        });



    }






}



$(document).ready(function () {
    ko.applyBindingsWithValidation(new OrderViewModel());





});