﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Kit.ascx.cs" Inherits="Templates_Kit" %>
<style type="text/css">

.table > tbody > tr > td
{
    padding:4px;
    }

.table > thead > tr > td
{
padding:4px;    
}
</style>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel kit_information">
                             
                              

                                <div class="row">
                                <div class="col-md-12">
                                <table style="width:100%"   >
                                <tr>
                              
                                <td>
                                    <div class="x_panel choos_kit">
                                <div class="x_title">
                                    <h2>KIT INFO</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 <div class="x_content">
                                    
                                 <table class="kit_top_table">
                                    <tr>
                                        <td><label class="control-label">Choose Kit</label></td>
                                        <td colspan = "5">                                    
                                        <select id="ddlItems">
                                        <option value="0"></option>
                                        </select>
                                        </td>
                                    </tr>
                                   
                                    <tr>
                                        <td ><label class="control-label">Total Amount</label></td>
                                        <td> <input type="text" class="form-control" readonly="readonly" id="txtTotalAmount data-bind="value:TotalAmount" ></td>
                                        <td><label class="control-label">MRP</label></td><td><input type="text" class="form-control" id="txtMRP" readonly="readonly"></td>
                                        <td><label class="control-label">SALE RATE</label></td>
                                        <td><input readonly="readonly" type="text" class="form-control" id="txtSaleRate" ></td>
                                   </tr>
                                </table>

                                </div>
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>


                    <div class="row">
                     <div class="col-md-12">
                        <div class="x_panel item_code_table">
                           <div class="x_content">

                                 <table class="table item_code_midletable">
                                    <thead>
                                        <tr>
                                        <th>Item/Code</th>
                                        <th>Code</th>
                                        <th>Name</th>
                                        <th>Qty</th>
                                        <th>Rate</th>
                                        <th>MRP</th>
                                        <th>Amount</th>
                                        <th>TaxAmount</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                    <td><select id="ddlProducts" class="form-control"></select></td>
                                    <td><input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox" /></td>
                                    <td><input id="txtName" type="text"  class="form-control customTextBox"/></td>
                                    <td><input type="text" id="txtQty" class="form-control customTextBox" /></td>
                                    <td><input type="text" id="txtRate"  class="form-control customTextBox" /></td>
                                    <td><input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>
                                    <td><input type="text" id="txtAmount" class="form-control customTextBox"  readonly="readonly" /></td>
                                    <td><input type="text"  class="form-control customTextBox" readonly="readonly" id="txtTax"/></td>
                                    <td><button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td>
                                    </tr>
                                    </tbody>
                                  </table>

                         </div>
                      </div>
                    </div>
                    

                    

                   <div class="col-md-12">
                       <div class="x_panel bottom_table">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                       <div class="x_content">
                          <table class="table table-striped kit_product">
                          <thead>
                            <tr>
                            <th>Name</th>
                            <th>Qty</th>
                            <th>Rate</th>
                            <th>MRP</th>
                            <th>Amount</th>
                            <th>Tax</th><th></th>
                            </tr>
                        </thead>
                        <tbody id="tbKitProducts">

                        </tbody>
                       </table>
                    </div>
                 </div>

                        <table class="kit_btns">
                            <tr>
                                <td><div id="btnSave" class="btn btn-success"><i class="fa fa-save"></i> Save</div></td>
                                <td><button id="btnCancelDialog" class="btn btn-danger">
                                    <i class="fa fa-mail-reply-all"></i> Cancel</button></td>
                            </tr>
                        </table>
             </div>


         </div>

        </div>
     </div>
  