﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class Templates_newucItemMaster : System.Web.UI.UserControl
{
    managesqence ms = new managesqence();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false) {

            bind_dd_dep();
        }
    }

    public void bind_dd_dep()
    {
        ms.req = "bind_dd";
        DataTable dt = ms.bind_item_dd();
        rptr_ddpos.DataSource = dt;
        rptr_ddpos.DataBind();

    }
}