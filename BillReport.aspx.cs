﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraPrinting;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using DevExpress.XtraReports.UI;

using Spire.Pdf;


public partial class Report : System.Web.UI.Page
{


    public string BillNowPrefix { get { return Request.QueryString["BillNowPrefix"] != null ? Convert.ToString(Request.QueryString["BillNowPrefix"]) : string.Empty; } }


    protected void Page_Load(object sender, EventArgs e)
    {
        //string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
        //string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");

        //if (Directory.Exists(filepath))
        //{
        //    PdfDocument doc = new PdfDocument();
        //    doc.LoadFromFile(filepath + "/" + BillNowPrefix + ".pdf");
        //    doc.Print();
        //}

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        bool Barcode = false;
        bool PrintSign = false;
        Connection con1 = new Connection();
        SqlConnection con = new SqlConnection(con1.sqlDataString);
        SqlCommand cmd = new SqlCommand("select BarcodePrintOnBill,Print_Sign from mastersetting_Basic ", con);
        DataTable dt = new DataTable();
        dt.Clear();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            Barcode = Convert.ToBoolean(dt.Rows[0]["BarcodePrintOnBill"].ToString());
            PrintSign = Convert.ToBoolean(dt.Rows[0]["Print_Sign"].ToString());
        }

        using (MemoryStream ms = new MemoryStream())
        {

           
            ARetailBill r = new ARetailBill(BillNowPrefix, Barcode, PrintSign);
            SqlCommand _Cmd = null;
            _Cmd = new SqlCommand();
            _Cmd.Connection = con;
            _Cmd.CommandText = "Report_sp_ExciseChallanReportSubBillReport";
            _Cmd.Parameters.AddWithValue("@BranchId", Convert.ToInt32(Branch));
            _Cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
            _Cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter adpex = new SqlDataAdapter(_Cmd);
            DataSet dse = new DataSet();
            adpex.Fill(dse);
            var subReport = r.FindControl("xrSubreport1", true) as XRSubreport;
            if (subReport != null)
            {
                subReport.ReportSource.DataSource = dse;
                //subReport.ReportSource.DataMember = _DataSet.Tables[1].TableName; //Here

                //  subReport.ReportSource.Parameters.Add();
                subReport.ReportSource.CreateDocument(true);
            }

            r.PrintingSystem.ShowMarginsWarning = true;

            r.CreateDocument();




            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;


            r.ExportToPdf(ms, opts);

            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);


        }


    }
}