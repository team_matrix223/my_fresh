﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managebarcodes.aspx.cs" Inherits="managebarcodes" %>
<%@ Register src="~/Templates/barcode.ascx" tagname="PrintBarcode" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <link href="css/tabcontent.css" rel="stylesheet" type="text/css" />
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <style>
            table.ui-jqgrid-htable th{
                text-align: left !important;
            }
            table#jQGridDemo td {
                text-align: left !important;
            }
     </style>
    <script language="javascript" type="text/javascript">
        var m_ItemId = 0;
		var ItemCode = 0;
        var BestExp = 0;
        var Day = 0;
        var SaleRate = 0;
        var Branch = 0;
        function BarcodeWithWeight(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                async: false,
                success: function (msg) {
                   
                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;

                    var GetBranchdropID = localStorage.getItem("Branchddl");
                    $("#ddlbranch_wht").val(GetBranchdropID)
                    $("#txtItemCOde_wht").val(obj.ItemOptions.Item_Code);
                    $("#txtItemName_wht").val(obj.ItemOptions.Item_Name);
                    $("#txtMRP_wht").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtSaleRate_wht").val(obj.ItemOptions.Sale_Rate);
                    $("#txtExpDate_wht").val(obj.ItemOptions.strExpDate);
                    $("#txtweight_wht").val(obj.ItemOptions.weight);
                    BestExp = obj.ItemOptions.BestExp;
                    Day = obj.ItemOptions.Day;
                    SaleRate = obj.ItemOptions.Sale_Rate;
                    $("#txtDay_wht").val(obj.ItemOptions.Day);
                    if (BestExp == true) {
                        $("#<%=lbltype.ClientID %>").html("Consumed Day");
                        $("#txtDay_wht").css("display", "block");
                        $("#txtExpDate_wht").css("display", "none");
                    }
                    else {
                        $("#<%=lbltype.ClientID %>").html("Choose Date");
                        $("#txtDay_wht").css("display", "none");
                        $("#txtExpDate_wht").css("display", "block");
                    }

                   
                    $("#DvBarCodeWeight").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        modal: true
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                  
                    $.uiUnlock();
                }
            });


        }

        function BindInformation(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;

                    var GetBranchdropID = localStorage.getItem("Branchddl");
                   
                    $("#ddlbranch").val(GetBranchdropID)
                    $("#txtItemCOde").val(obj.ItemOptions.Item_Code);
                    $("#txtItemName").val(obj.ItemOptions.Item_Name);
                    $("#txtMRP").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtSaleRate").val(obj.ItemOptions.Sale_Rate);
                    $("#txtExpDate").val(obj.ItemOptions.strExpDate);
                    BestExp = obj.ItemOptions.BestExp;
                    Day = obj.ItemOptions.Day;
                    $("#txtDay").val(obj.ItemOptions.Day);
                    if (BestExp == true) {
                        $("#<%=lbltype.ClientID %>").html("Consumed Day");
                        $("#txtDay").css("display", "block");
                        $("#txtExpDate").css("display", "none");
                    }
                    else {
                        $("#<%=lbltype.ClientID %>").html("Choose Date");
                        $("#txtDay").css("display", "none");
                        $("#txtExpDate").css("display", "block");
                    }
					$("#txtQty").focus();
                    
                    $("#ProductDialog").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        closeOnEscape: true,
                        modal: true
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                    
                }
            });


        }



        function BindBatchInformation(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;
                 
                    $("#txtbatchCode").val(obj.ItemOptions.Item_Code);
                    $("#txtbatchName").val(obj.ItemOptions.Item_Name);
                    $("#txtbatchMrp").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtbatchSaleRate").val(obj.ItemOptions.Sale_Rate);
        
                    $("#DvBatch").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        modal: true
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }



        function BindIngridientInformation(ItemId) {

            $.ajax({
                type: "POST",
                data: '{"ItemId":"' + ItemId + '"}',
                url: "managebarcodes.aspx/BindIemDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    m_ItemId = obj.ItemOptions.ItemID;

                    var GetBranchdropID = localStorage.getItem("Branchddl");
                    $("#ddlibranch").val(GetBranchdropID)
                    $("#txticode").val(obj.ItemOptions.Item_Code);
                    $("#txtiname").val(obj.ItemOptions.Item_Name);
                    $("#txtimrp").val(obj.ItemOptions.Max_Retail_Price);
                    $("#txtisrate").val(obj.ItemOptions.Sale_Rate);
                    $("#txtiexpdate").val(obj.ItemOptions.strExpDate);
                    BestExp = obj.ItemOptions.BestExp;
                    Day = obj.ItemOptions.Day;
                    $("#txtiday").val(obj.ItemOptions.Day);
                    if (BestExp == true) {
                        $("#<%=lbltype.ClientID %>").html("Consumed Day");
                        $("#txtiday").css("display", "block");
                        $("#txtiexpdate").css("display", "none");
                    }
                    else {
                        $("#<%=lbltype.ClientID %>").html("Choose Date");
                        $("#txtiday").css("display", "none");
                        $("#txtiexpdate").css("display", "block");
                    }

                    for (var i = 0; i < obj.NutritionFact.length; i++) {
                        $("#txtifield1").val(obj.NutritionFact[0]["Field1"]);
                        $("#txtifield2").val(obj.NutritionFact[0]["Field2"]);
                        $("#txtifield3").val(obj.NutritionFact[0]["Field3"]);
                        $("#txtingridients").val(obj.NutritionFact[0]["Ingredients"]);
                    }
                    $("#Dvingredient").dialog({
                        autoOpen: true,
                        left: 367,
                        width: 350,
                        resizable: false,
                        modal: true
                    });



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });


        }

        var bcodetype = "1";

        $(document).ready(
    function () {



       Bindbranch();
                $("#<%=rdb1.ClientID %>").change(
                    function () {


                        if ($("#<%=rdb1.ClientID %>").prop('checked') == true) {
                            bcodetype = "1";
                            $("#<%=rdb2.ClientID %>").prop('checked', false);
                            $("#<%=rdb3.ClientID %>").prop('checked', false);
                            $("#<%=rdb4.ClientID %>").prop('checked', false);

                        }


                    });

				$("#<%=rdb2.ClientID %>").change(
					function () {


						if ($("#<%=rdb2.ClientID %>").prop('checked') == true) {
							 bcodetype = "2";
							 $("#<%=rdb1.ClientID %>").prop('checked', false);
							$("#<%=rdb3.ClientID %>").prop('checked', false);
							$("#<%=rdb4.ClientID %>").prop('checked', false);

						 }


                    });

				$("#<%=rdb3.ClientID %>").change(
					function () {


						if ($("#<%=rdb3.ClientID %>").prop('checked') == true) {
							 bcodetype = "3";
							 $("#<%=rdb2.ClientID %>").prop('checked', false);
							$("#<%=rdb1.ClientID %>").prop('checked', false);
							$("#<%=rdb4.ClientID %>").prop('checked', false);

						 }


                    });

				$("#<%=rdb4.ClientID %>").change(
					function () {


						if ($("#<%=rdb4.ClientID %>").prop('checked') == true) {
							 bcodetype = "4";
							 $("#<%=rdb2.ClientID %>").prop('checked', false);
							$("#<%=rdb3.ClientID %>").prop('checked', false);
							$("#<%=rdb1.ClientID %>").prop('checked', false);

						 }


					 });

        function Bindbranch() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "managebarcodes.aspx/Bindbranches",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlbranch").html(obj.BranchOptions);
                    $("#ddlbranch_wht").html(obj.BranchOptions);
                    $("#ddlibranch").html(obj.BranchOptions);
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }

                $("#txtQty").keyup(function () {
                    
                    Printty = "Bar";
                   
				});
				
        $("#ddlbranch").change(function () {

            //var SaleRate = $("#ddlbranch option:selected").attr('salerate');

         GetSaleRate($(this).val());
            getbranch($(this).val());
            $("#txtQty").focus();
        }
        );
        $("#ddlbranch").change(function () {
            var BranchId = $("#ddlbranch").val();
            localStorage.setItem("Branchddl", BranchId);
			$("#txtQty").focus();
        });

        $("#ddlibranch").change(function () {

            //var SaleRate = $("#ddlbranch option:selected").attr('salerate');

            GetiSaleRate($(this).val());
            getibranch($(this).val());
        }
        );

        $("#ddlibranch").change(function () {
            var BranchId = $("#ddlibranch").val();
            localStorage.setItem("Branchddl", BranchId);
        });
        $("#ddlbranch_wht").change(function () {

            //var SaleRate = $("#ddlbranch option:selected").attr('salerate');

            GetSaleRate($(this).val());
            getbranch($(this).val());
        }
        );
        $("#ddlbranch_wht").change(function () {
            var BranchId = $("#ddlbranch_wht").val();
            localStorage.setItem("Branchddl", BranchId);
        });

        function getbranch(BranchId) {
            $.ajax({
                type: "POST",
                data: '{BranchId:"' + BranchId + '","ItemCode":"' + ItemCode + '"}',
                url: "managebarcodes.aspx/getbranch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlbranch").val(obj.mRP);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });

        }

        function getbranch_wht(BranchId) {
            $.ajax({
                type: "POST",
                data: '{BranchId:"' + BranchId + '","ItemCode":"' + ItemCode + '"}',
                url: "managebarcodes.aspx/getbranch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlbranch_wht").val(obj.mRP);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });

        }
        function GetSaleRate(BranchId) {

            var ItemCode = $("#txtItemCOde").val();

            $.ajax({
                type: "POST",
                data: '{BranchId:"' + BranchId + '","ItemCode":"' + ItemCode + '"}',
                url: "managebarcodes.aspx/GetSaleRate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#txtSaleRate").val(obj.saleRate);
                    $("#txtMRP").val(obj.mRP);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }


        function getibranch(BranchId) {
            $.ajax({
                type: "POST",
                data: '{BranchId:"' + BranchId + '","ItemCode":"' + ItemCode + '"}',
                url: "managebarcodes.aspx/getbranch",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlibranch").val(obj.mRP);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }
            });

        }
        function GetiSaleRate(BranchId) {

            var ItemCode = $("#txticode").val();

            $.ajax({
                type: "POST",
                data: '{BranchId:"' + BranchId + '","ItemCode":"' + ItemCode + '"}',
                url: "managebarcodes.aspx/GetSaleRate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#txtisrate").val(obj.saleRate);
                    $("#txtimrp").val(obj.mRP);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }




        $('#txtiexpdate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#txtbatchDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $("#txtbatchDate").val($("#<%=hdnDate.ClientID%>").val());

        $('#txtExpDate').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
        $('#txtExpDate_wht').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        BindGrid();
        $("#btnBarcodeWeight").click(
                                        function () {

                                            if (m_ItemId == 0) {
                                                alert("No Product is selected for Barcode Printing");
                                                return;
                                            }


                                            var ItemId = m_ItemId;


                                            BarcodeWithWeight(ItemId);
                                            var branch = $("#ddlbranch_wht").val();
                                      
                                            if (branch != "") {
                                                //GetSaleRate(branch);
                                            }
                                            wht_cal();
                                        }
                );

                var Printty = "";

        $("#btnBarcode").click(
                                        function () {

                                            if (m_ItemId == 0) {
                                                alert("No Product is selected for Barcode Printing");
                                                return;
                                            }


                                            var ItemId = m_ItemId;


                BindInformation(ItemId);
                
                                            var branch = $("#ddlbranch").val();
				$("#txtQty").focus();
                                            //if (branch != "") {
                                            //    GetSaleRate(branch);
                                            //}

                                        }
                                        );



        $("#btnBatch").click(
                                        function () {

                                            if (m_ItemId == 0) {
                                                alert("No Product is selected for Barcode Printing");
                                                return;
                                            }


                                            var ItemId = m_ItemId;


                                            BindBatchInformation(ItemId);


                                        }
                                        );

        $("#btningrdients").click(function () {

            if (m_ItemId == 0) {
                alert("No Product is selected for Barcode Printing");
                return;
            }


            var ItemId = m_ItemId;


            BindIngridientInformation(ItemId);
            var branch = $("#ddlibranch").val();

            if (branch != "") {
                GetiSaleRate(branch);
            }
        }
                                        );


        $("#btnPrint").click(
            function () {
				Printty = "";
                var Qty = 0;
                var txQty = $("#txtQty").val();
                if (txQty > 2) {
                    Qty = Math.round((txQty / 2),0);

                }
                else {

                    Qty = 1;
				}
                 
				
                if (Qty == "") {
					alert("Enter Qty To Print");
					return;

                }
                
                var Date = $("#txtExpDate").val();
                var Day1 = $("#txtDay").val();
                var Branch = $("#ddlbranch").val();

                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

				iframe = document.createElement("iframe"); 
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);

				window.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1 + '&Branch=' + Branch + '&bcodetype=' + bcodetype;
                //document.getElementById('reportout').contentWindow.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId;
                //alert("hi");
                $("#ProductDialog").dialog("close");
                m_ItemId = 0;
            });
  
        function wht_cal() {
            var weight=0;
             weight = Number($("#txtweight_wht").val());
             if (weight == 0)
             {
                 weight = 1;
             
             }
            total = Number(SaleRate * weight).toFixed(2)
            $("#txtSaleRate_wht").val(total);
        }
        $("#txtweight_wht").keyup(function () {
            wht_cal();
        });
        $("#btnBar_wht_Print").click(
            function () {


                var Qty = $("#txtQty_wht").val();
                var Date = $("#txtExpDate_wht").val();
                var Day1 = $("#txtDay_wht").val();
                var Branch = $("#ddlbranch_wht").val();
                var wht_itemcode = $("#txtItemCOde_wht").val();
                var wht_rate = $("#txtSaleRate_wht").val();
                var wht_weight = $("#txtweight_wht").val();



                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

                iframe = document.createElement("iframe");
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);
               

                $.ajax({
                    type: "POST",
                    url: "managebarcodes.aspx/insertupdate_whtbarcode",
                    contentType: "application/json",
                    data: '{"wht_itemcode":"' + wht_itemcode + '","wht_rate":"' + wht_rate + '","wht_weight":"' + wht_weight + '",wht_Qty:"' + Qty + '",wht_ItemId:"' + m_ItemId + '",wht_Date:"' + Date + '",wht_BestExp:"' + BestExp + '",wht_Day:"' + Day1 + '",wht_Branch:"' + Branch + '"}',
                   
                    dataType: "json",
                    success: function () {
                      
                        //$(this).attr('Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1 + '&Branch=' + Branch  + '&wt='+wht_weight+'', '_blank');
                        //window.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1 + '&Branch=' + Branch  + '&wt='+wht_weight;
                        window.open('Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1 + '&Branch=' + Branch + '&wt=' + wht_weight, '_blank');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                    }
                });

            });
                $(window).keydown(function (e) {

                    switch (e.keyCode) {

                        case 13:
                            if (Printty == "Bar") {
                                $("#btnPrint").click();
                                
                                Printty = "";
                            }
                            else {

        //                        var rowid = currow;
                               
								//m_ItemId = 0;

								//m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');
								//var m_itemcode = $('#jQGridDemo').jqGrid('getCell', rowid, 'Item_Code');
        //                        //alert(m_itemcode);
        //                        selrowf = true;

								//$("#txtItemCOde").val(m_itemcode);
                                
        //                        $("#btnBarcode").click();
								
                            }
							
                            return false;
                        case 40:
							
							currow = 0;

                            //arrow("next");
							
							//if (selrowf == true) {

							//	$("#jQGridDemo").setSelection(1);
							//	selrowf = false;

							//}
							//var list = $('#jQGridDemo'),

							//	$td = $(e.target).closest("tr.jqgrow>td"),
							//	p = list.jqGrid("getGridParam"),
							//	//cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
							//	cm = "Item_Code";

							//var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


       //                     var selectedRow = list.jqGrid('getGridParam', 'selrow');
                            

							////if (isfirst == 0) {

							////	selectedRow = selectedRow - 1;

							////}


       //                     if (selectedRow == null) {
       //                         selectedRow = 1;
       //                     }

							//var ids = list.getDataIDs();
							//var index = list.getInd(selectedRow);

							//if (ids.length < 2) return;
							//index++;

							//list.setSelection(ids[index - 1], false, e);
       //                     currow = selectedRow;
                           
							//var rows = document.querySelectorAll('#jQGridDemo tr');

							//var line = document.querySelector(1);



							//rows[line].scrollTop({
							//	behavior: 'smooth',
							//	block: 'nearest'
							//});

							//e.preventDefault();

                            return false;


                        case 38:
                           // currow = 0;
                           // //arrow("prev");
                           // var list = $('#jQGridDemo'),

                           //     $td = $(e.target).closest("tr.jqgrow>td"),
                           //     p = list.jqGrid("getGridParam"),
                           //     //cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
                           //     cm = "Item_Code";

                           // var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


                           // var selectedRow = list.jqGrid('getGridParam', 'selrow');


                           // //if (isupfirst == 0) {
                           // //	selectedRow = Number(selectedRow) + Number(1);

                           // //}

                           // if (selectedRow == null) return;
                           // var ids = list.getDataIDs();

                           // var index = list.getInd(selectedRow);

                           // if (ids.length < 2) return;
                           // index--;

                           // list.setSelection(ids[index - 1], false, e);

                           // currow = selectedRow ;
                            
                           

                           // var rows = document.querySelectorAll('#jQGridDemo tr');

                           // var line = document.querySelector(1);



                           // rows[line].scrollTop({
                           //     behavior: 'smooth',
                           //     block: 'nearest'
                           // });

                           // //var w = $(window);
                           // //var row = $('#jQGridProduct').find('tr').eq(line);

                           // //                  if (row.length) {
                           // //                      list.scrollTop(row.offset().top + (12/ 2));
                           // //}
                           // e.preventDefault();
                          return false;



                    }
                });

        $("#btningrdntprint").click(
            function () {


                var Qty = $("#txtiqty").val();
                var Date = $("#txtiexpdate").val();
                var Day1 = $("#txtiday").val();
                var Branch = $("#ddlibranch").val();
                var field1 = $("#txtifield1").val();
                var field2 = $("#txtifield2").val();
                var field3 = $("#txtifield3").val();
                var ingrdient = $("#txtifield3").val();

                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

                iframe = document.createElement("iframe");
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);

                window.location = 'ReportIngridient.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&Date=' + Date + '&BestExp=' + BestExp + '&Day=' + Day1 + '&Branch=' + Branch + '&field1=' + field1 + '&field2=' + field2 + '&field3=' + field3 + '&ingrdient=' + ingrdient;
                //document.getElementById('reportout').contentWindow.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId;
                //alert("hi");

            });



        $("#btnBatchPrint").click(
            function () {

                var Qty = $("#txtbatchqty").val();
                var Name = $("#txtbatchName").val();
                var MRP = $("#txtbatchMrp").val();
                var Date = $("#txtbatchDate").val();
                var BatchNo = $("#txtbatchNo").val();

                var iframe = document.getElementById('reportout');

                //                if (iframe != null) {
                //                    document.body.removeChild(iframe);
                //                } 

                iframe = document.createElement("iframe");
                iframe.setAttribute("id", "reportout");
                iframe.style.width = 0 + "px";
                iframe.style.height = 0 + "px";
                document.body.appendChild(iframe);

                window.location = 'ReportBatch.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId + '&MRP=' + MRP + '&MfdDate=' + Date + '&BatchNo=' + BatchNo + '&Name=' + Name;
                //document.getElementById('reportout').contentWindow.location = 'Report.aspx?Qty=' + Qty + '&ItemId=' + m_ItemId;
                //alert("hi");

            });
				var selrowf = true;

				var isfirst = 0;
				var isupfirst = 0;
				var currow = 0;
				

    });
    
    </script>


      <style type="text/css">
        .table
        {
            margin: 5px;
        }
         .page-title .title_left{
            width: 100%;
            padding-left: 15px;
            background: #1479B8;
            color: #ffffff;
        }
        .ui-jqgrid-titlebar.ui-widget-header
        {
               background: #1479B8 !important;
        }
        /*.right_col
        {
           min-height:unset;
        }
        body
        {
            background: lavender !important;
        }*/
        .right_col {
            min-height: unset;
            float: left;
            width:100%;
        }
        .nav_menu {
            margin-bottom: 0;
        }
        .page-title {
            height: 44px;
            padding:0;
            width:82% !important;
        }
        .x_panel {
            float: left;
        }
    </style>
<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:HiddenField ID="hdnDate" runat="server"/>
 <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>BARCODES</h3>
                        </div>
                       
      
                    <div class="x_panel">
                         <div class="form-group">
                                
                              

                <div class="youhave"  >
                     <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnBarcode" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Barcode</div></td>
                                       <td style="padding-top:5px"> <div id="btnBatch" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Batch Printing</div></td>
                                       <td style="padding-top:5px"> <div id="btningrdients" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Ingridients Printing</div></td>     
                                        <td style="padding-top:5px"> <div id="btnBarcodeWeight"  class="btn btn-primary" ><i class="fa fa-external-link"></i> Barcode With Weight</div></td>     
                                            
                                            </tr>

                                         
                                            </table>
    <div id="jQGridDemoPager">
    </div>

    
                </div>

<div class="row" id="DvBatch" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="tblbatch" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchCode" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchName" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchMrp" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtbatchSaleRate" disabled="disabled"> </td>
                                    
                                    </tr>

                                   <%-- <tr>
                                    <td style="width:60px"><asp:Label ID="Label1" runat="server"></asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="Text5" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="Text6" >
                                    </td>
                                  
                                    </tr>--%>

                                      <tr>
                                    <td style="width:60px"> <label>BatchNo 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchNo" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>


                                     <tr>
                                    <td style="width:60px"> <label>Mfd Date: 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchDate" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>


                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtbatchqty" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btnBatchPrint" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>


<div class="row" id="Dvingredient" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="Table1" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                    <tr>
                                    <td style="width:60px"> <label>Branch 
                                            </label></td>
                                      <td><select id = "ddlibranch" style="height:30px"  class ="form-control"></select></td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txticode" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtiname" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtimrp" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtisrate" disabled="disabled"> </td>
                                    
                                    </tr>

                                    <tr>
                                    <td style="width:60px"><asp:Label ID="Label1" runat="server"></asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtiexpdate" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtiday" >
                                    </td>
                                  
                                    </tr>





                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtiqty" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>
                                     <tr>
                                    <td style="width:120px"> <label>Field1 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtifield1" >   </td>
                                    
                                    </tr>

                                     <tr>
                                    <td style="width:120px"> <label>Field2 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtifield2" >   </td>
                                    
                                    </tr>
                                     <tr>
                                    <td style="width:120px"> <label>Field3 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtifield3" >   </td>
                                    
                                    </tr>

                                    <tr>
                                    <td style="width:120px"> <label>Ingridients 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtingridients" multiline="multline">   </td>
                                    
                                    </tr>
                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btningrdntprint" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>
                            
<div class="row" id="ProductDialog" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                             <table width="100%" id="formbarcode" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                     <tr>
                                    <td style="width:60px"> <label>Branch 
                                            </label></td>
                                      <td>
                                          
                                          <select id = "ddlbranch" disabled="disabled" style="height:30px"  class ="form-control"></select>

                                      </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemCOde" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemName" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtMRP" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtSaleRate" disabled="disabled"> </td>
                                    
                                    </tr>

                                    <tr>
                                    <td style="width:60px"><asp:Label ID="lbltype" runat="server"></asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtExpDate" disabled="disabled" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtDay" >
                                    </td>
                                  
                                    </tr>





                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtQty" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>

<tr><td> <div class="radio-section">
                  <asp:RadioButton ID="rdb1" Checked="true" name ="all" Visible="false"   runat="server" Text="1" />
						<asp:RadioButton ID="rdb2" name ="all" Visible="false"  runat="server" Text="2" />
    <asp:RadioButton ID="rdb3" name ="all"   runat="server" Visible="false" Text="3" />
    <asp:RadioButton ID="rdb4" name ="all"   runat="server" Visible="false" Text="4" />
					 
                </div></td></tr>

                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btnPrint" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>


<div class="row" id="DvBarCodeWeight" style="display:none">

<div class="x_panel">
  <div class="x_title">
                                    <h2>Product Detail</h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        
                                    
                                        
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
   <div class="x_content">
                                  
                                    <form class="form-horizontal form-label-left"   >

                           <table width="100%" id="formbarcode_wht" >
                             <tr><td>
                                  
                             <div class="form-group">

                                    <table>
                                     <tr>
                                    <td style="width:60px"> <label>Branch 
                                            </label></td>
                                      <td><select id = "ddlbranch_wht" style="height:30px"  class ="form-control"></select></td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Code 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemCOde_wht" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Name 
                                            </label></td><td>     <input type="text"   class="form-control col-md-7 col-xs-12" id="txtItemName_wht" disabled="disabled">   </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>MRP 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtMRP_wht" disabled="disabled"> </td>
                                    
                                    </tr>
                                    <tr>
                                    <td style="width:60px"> <label>Sale Rate 
                                            </label></td><td>  <input type="text"   class="form-control col-md-7 col-xs-12" id="txtSaleRate_wht" disabled="disabled"> 
                                              
                                                         </td>
                                    
                                    </tr>

                                    <tr>
                                    <td style="width:60px"><asp:Label ID="Label2" runat="server">Choose Date</asp:Label> </td>
                                    <td>
                                    <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtExpDate_wht" >
                                     <input type="text"   class="form-control col-md-7 col-xs-12" style="display:none" id="txtDay_wht" >
                                    </td>
                                  
                                    </tr>





                                     <tr>
                                    <td style="width:60px"> <label>Qty 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtQty_wht" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>

                                    
                                     <tr>
                                    <td style="width:60px"> <label>Weight 
                                            </label></td><td>
                                           
                                             <input type="text"  id="txtweight_wht" class="form-control col-md-7 col-xs-12 validate required valNumber" >  </td>
                                    
                                    </tr>

                                    </table>


                                        </div>

                                        

                             </td>
                             
                      
                             </tr>
                             </table>
                             
                             
                                    
                                        <div class="ln_solid"></div>
                                        <div class="form-group">
                                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                            <%-- <asp:button id="btnPrint" class="btn btn-primary" OnClick="btnPrint_Click" runat="server"  Text="Print" />--%> 
                                             <div id="btnBar_wht_Print" style="display:block;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Print</div>
                                          
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
</div>

                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>

</div>
  <iframe id="reportout" width="0" height="0"></iframe>
</form>



     <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageProducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ItemId', 'ItemCode', 'ItemName', 'SaleRate', 'MRP'],
                           
                        colModel: [
                            { name: 'ItemID', key: true, index: 'ItemID', width: 50, stype: 'text', sorttype: 'int', hidden: true },
                           
                                    { name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Name', index: 'Item_Name', width: 300, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                    { name: 'Sale_Rate', index: 'Sale_Rate', width: 80, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 80, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'ItemID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Items List",

                        editurl: 'handlers/ManageProducts.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                        .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                    $("#globalSearchText").keydown(function (e) {
                var key = e.charCode || e.keyCode || 0;
              
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: true
            }).click(function () {
               
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                
                //for (i = 0; i < l; i++) {
                //    cm = colModel[i];
                //    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                //        rules.push({
                //            field: cm.name,
                //            op: "bw",
                //            data: searchText
                //        });
                //    }
                //}

				rules.push({
                            field:"Item_Code",
                            op: "bw",
                            data: searchText
                });
				rules.push({
					field: "Item_Name",
					op: "cn",
					data: searchText
				});
    //            rules: [
    //                { "field": "Item_Code", "op": "bw", "data": searchText },
    //                { "field": "Item_Name", "op": "bw", "data": searchText }
				//],
                postData.filters = JSON.stringify({
				
                    groupOp: "OR",
   
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {

                onSelectRow: function (rowid, iRow, iCol, e) {
                   
                    m_ItemId = 0;

                           m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');
                        var m_itemcode= $('#jQGridDemo').jqGrid('getCell', rowid, 'Item_Code');
                      //alert(m_itemcode);
                      $("#txtItemCOde").val(m_itemcode);

                           
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>

