﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managedeliverynote.aspx.cs" Inherits="managedeliverynote" %>

    <%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
  

<%@ Register Src="~/Templates/DeliveryNote.ascx" TagName="AddKit" TagPrefix="uc1" %>
<asp:Content ID="Content2" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/Inventory.css" rel="stylesheet" />
    <script type="text/javascript" src="js/SearchPluginforDN.js"></script>
    <script language="javascript" type="text/javascript">

        var m_KitID = 0;
        var ProductCollection = [];
        var mrpeditable = false;
        var saleRateeditable = false;
        var BillBasicType = "";
        var ItemType = "";
        var DropDownData = "";
        var global_counterId = "";
      
        function clsProduct() {
            this.Item_ID = 0;
            this.Item_Code = "";
            this.Item_Name = "";
            this.Qty_In_Case = 0;
            this.Qty = 0;
            this.Rate = 0;
            // this.Sale_Rate = 0;
            this.MRP = 0;
            this.Amount = 0;
            this.QTY_TO_LESS = 0;
            this.Tax_ID = 0;
            this.TaxP = 0;
            this.Stock = 0;
            this.Dis = 0;
            this.Excise = 0;
            this.Abatment = 0;
        }

        var m_KitID = 0;




        $(document).on("click", "#btnDel", function (event) {

            var RowIndex = Number($(this).closest('tr').index());

            ProductCollection.splice(RowIndex, 1);
            BindRows();


        });



        function BindRows() {


            var TotalAmount = 0;
            var ExciseAmount = 0;
            var html = "";
            var SaleRate = 0;
            var Excise = 0;

            for (var i = 0; i < ProductCollection.length; i++) {



                html += "<tr>";
                html += "<td>" + ProductCollection[i]["Item_Code"] + "</td>";
                html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["Qty_In_Case"] + "</td>";
                html += "<td>" + ProductCollection[i]["Qty"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["Scheme"] + "</td>";
                html += "<td>" + ProductCollection[i]["Rate"] + "</td>";

                if (BillBasicType == "I") {
                    Excise = ProductCollection[i]["Excise"];

                    SaleRate = ProductCollection[i]["MRP"] - ((ProductCollection[i]["MRP"] * ProductCollection[i]["TaxP"]) / (100 + Number(ProductCollection[i]["TaxP"])));
                    SaleRate = SaleRate - ((Number(SaleRate) * Number(Excise)) / (100 + Number(Excise)));
                    SaleRate = (SaleRate * (100 - Number(ProductCollection[i]["Abatement"])) / 100);




                }
                else {

                    SaleRate = (Number(ProductCollection[i]["Sale_Rate"]) * (100 - Number(ProductCollection[i]["Abatement"])) / 100);


                }

                if ($("#rdbExcise").prop("checked") == true) {

                    //html += "<td>" + SaleRate.toFixed(2) + "</td>";
                }
                else {
                    // html += "<td style='display:none'>" + ProductCollection[i]["Sale_Rate"] + "</td>";
                }
                html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
                html += "<td>" + ProductCollection[i]["Amount"] + "</td>";
                html += "<td>" + ProductCollection[i]["Dis3P"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["QTY_TO_LESS"] + "</td>";
                html += "<td >" + ProductCollection[i]["Stock"] + "</td>";
                html += "<td>" + ProductCollection[i]["TaxP"] + "</td>";
                html += "<td>" + ProductCollection[i]["Excise"] + "</td>";
                html += "<td style='display:none'>" + ProductCollection[i]["Abatement"] + "</td>";
                html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer;width:20px'   /></td>";
                html += "</tr>";

                TotalAmount += parseFloat(ProductCollection[i]["Amount"])


                ExciseAmount += parseFloat((((ProductCollection[i]["Excise"]) * (ProductCollection[i]["Rate"])) / 100))


            }

            $("#txtBillval").val(TotalAmount.toFixed(2));
            if ($("#rdbnonexcise").prop("checked") == true) {
                $("#txtExcise").val("0");
            }
            else {
                $("#txtExcise").val(ExciseAmount.toFixed(2));
            }

            //var DisPer = $("#ddlDealer option:selected").attr("dis");
            var DisPer = "0";
            var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
            $("#txtDisPer").val(DisPer);
            $("#txtDisAmt").val(DisAmt);
            $("#txtAdj").val("0");

            $("#txtnetAmt").val(Number(TotalAmount.toFixed(2)) - Number(DisAmt.toFixed(2)) + Number($("#txtExcise").val()));
            $("#tbKitProducts").html(html);



        }


        function Settings() {


            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managedeliverynote.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);




                    if (obj.DRate == "0") {
                        $("#txtRate").attr('disabled', 'disabled');
                    }
                    else {
                        $("#txtRate").removeAttr('disabled');
                    }

                    if (obj.DMRP == "0") {
                        $("#txtMarketPrice").attr('disabled', 'disabled');
                    }
                    else {
                        $("#txtMarketPrice").removeAttr('disabled');
                    }





                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });

        }


        function ResetControls() {

            m_KitID = 0;
            $("#tbKitProducts").html("");
            $("#txtTotalAmount").val("");
            $("#txtMRP").val("");
            $("#txtSaleRate").val("");
            $("#txtddlItems").val("").removeAttr("disabled");
            $("#ddlItems").html("<option value='0'></option>")
            $("#txtGrNo").val("");
            $("#txtVehNo").val("");
            $("#ddlDealer").val("0");

            $("#ddlRefno").val("0");
            $("#txtBillval").val("");
            $("#txtDisAmt").val("");
            $("#txtAdj").val("");
            $("#txtnetAmt").val("");
            $("#txtExcise").val("");
            ProductCollection = [];
            ResetList();
        }

        function ResetList() {


            $("#txtCode").val("");
            $("#txtName").val("");
            $("#txtQty").val("");
            $("#txtRate").val("");
            $("#txtMarketPrice").val("");
            $("#txtAmount").val("");
            $("#txtTax").val("");
            $("#ddlProducts").html("<option value='0'></option>");
            $("#txtddlProducts").val("").focus();
            $("#txtStock").val("");



        }






        function GetPluginData(Type) {

            if (Type == "Kit") {

                var IsExist = $("#ddlItems option:selected").attr("is_exist");

                if (IsExist != 0) {

                    alert("Kit Already Created");
                    $("#txtddlItems").val("").focus();
                    $("#ddlItems").html("<option value='0'></option>")

                    return;

                }



                $("#txtMRP").val($("#ddlItems option:selected").attr("mrp"));
                $("#txtSaleRate").val($("#ddlItems option:selected").attr("sale_rate"));



                $("#txtTotalAmount").val("0");



            }
            else if (Type == "Product") {


                if ($("#ddlBranch").val() == "0") {
                    alert("Please choose Branch");
                    return;
                }

//                if ($("#ddlGodown").val() == "0") {
//                    alert("Please choose Godown");
//                    return;
//                }
                var m_ItemCode = $("#ddlProducts option:selected").attr("item_code");
                var Rate = 0;
                var MRP = 0;
                var Abatement = 0;
                var TaxAmt = 0;
                var ExciseId = 0;

                
                //Rate = $("#ddlProducts option:selected").attr("DnRate");
                Rate = $("#ddlProducts option:selected").attr("sale_rate");
                MRP = $("#ddlProducts option:selected").attr("mrp");
                TaxAmt = $("#ddlProducts option:selected").attr("tax_code");
                Abatement = $("#ddlProducts option:selected").attr("Abatement");
                Excise = $("#ddlProducts option:selected").attr("Excise");
                ExciseId = $("#ddlProducts option:selected").attr("ExciseId");



                if (BillBasicType == "I") {



                    Rate = MRP - ((Number(MRP) * Number(TaxAmt)) / (100 + Number(TaxAmt)));
                    Rate = Rate - ((Number(Rate) * Number(Excise)) / (100 + Number(Excise)));

                    Rate = (Rate * (100 - Number(Abatement)) / 100);





                }
                else {

                    Rate = (Rate * (100 - (Abatement)) / 100);

                }


                $("#txtCode").val(m_ItemCode);
                $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
                $("#txtQty").val("").focus();
                if ($("#rdbExcise").prop("checked") == true || $("#rdballexcise").prop("checked") == true) {
                    if (ExciseId == 0) {
                        $("#txtRate").val($("#ddlProducts option:selected").attr("DnRate"));
                    }
                    else {
                        $("#txtRate").val(Rate.toFixed(2));
                    }
                }
                else {
                    $("#txtRate").val($("#ddlProducts option:selected").attr("DnRate"));
                }
                $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));
                $("#txtAmount").val(0);



                var CurrentStock = 0;
                for (var i = 0; i < ProductCollection.length; i++) {

                    if (m_ItemCode == ProductCollection[i]["Item_Code"]) {

                        CurrentStock = parseFloat(CurrentStock) + parseFloat(ProductCollection[i]["Qty"]);
                    }

                }

                $("#txtStock").val(parseFloat($("#ddlProducts option:selected").attr("stock_qty")) - parseFloat(CurrentStock));
            }
        }





        $(document).ready(function () {
 
            $("#btnReprint").click(function () {

         var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if ($.trim(SelectedRow) == "") {
             alert("No Record is selected to Reprint");
             return;
         }

         var m_BillNowPrefix = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillPrefix')

         $.uiLock('');

         var PrintType = "";
         if ($("#rdbExcise").prop("checked") == true) {

             PrintType = "DNEx";

         }
         else if ($("#rdbnonexcise").prop("checked") == true) {

             PrintType = "DN";
         }

         else if ($("#rdballexcise").prop("checked") == true) {

             PrintType = "DNEx";
         }


         $.ajax({
             type: "POST",
             data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
             url: "BillScreen.aspx/Reprint",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);


             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {

                 $.uiUnlock();

             }

         });

     });






            $('#txtDateFrom').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtDateTo').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            Settings();


            $("#btnGo").click(
                   function () {

                       BindGrid();

                   }
                    );

          //  $("#rdbcfinished").change(
          //function () {

          //    $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
      

          //    if ($("#rdbcfinished").prop("checked") == true) {
          //        ItemType = "3";
          //    }
          //    else if ($("#rdbraw").prop("checked") == true) {
          //        ItemType = "1";
          //    }
          //    else if ($("#rdbsemi").prop("checked") == true) {
          //        ItemType = "5";
          //    }


          //    var Excise = "0";
          //    if ($("#rdbExcise").prop("checked") == true) {
          //        Excise = "1";

          //    }
          //    else if ($("#rdbnonexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }

          //    else if ($("#rdballexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }



          //    $("#ddlProducts").supersearch({
          //        Type: "Product",
          //        Caption: "Please enter Item Name/Code ",
          //        AccountType: "",
          //        Godown: $("#ddlGodown").val(),
          //        ItemType: ItemType,
          //        Excise: Excise,
          //        Width: 214,
          //        DefaultValue: 0
          //    });
          //}
          //);


          //  $("#rdbraw").change(
          //function () {

          //    $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
          //    var ItemType = "";

          //    if ($("#rdbcfinished").prop("checked") == true) {
          //        ItemType = "3";
          //    }
          //    else if ($("#rdbraw").prop("checked") == true) {
          //        ItemType = "1";
          //    }
          //    else if ($("#rdbsemi").prop("checked") == true) {
          //        ItemType = "5";
          //    }


          //    var Excise = "0";
          //    if ($("#rdbExcise").prop("checked") == true) {
          //        Excise = "1";

          //    }
          //    else if ($("#rdbnonexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }
          //    else if ($("#rdballexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }


          //    $("#ddlProducts").supersearch({
          //        Type: "Product",
          //        Caption: "Please enter Item Name/Code ",
          //        AccountType: "",
          //        Godown: $("#ddlGodown").val(),
          //        ItemType: ItemType,
          //        Excise: Excise,
          //        Width: 214,
          //        DefaultValue: 0
          //    });
          //}
          //);

          //  $("#rdbsemi").change(
          //function () {

          //    $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
          //    var ItemType = "";

          //    if ($("#rdbcfinished").prop("checked") == true) {
          //        ItemType = "3";
          //    }
          //    else if ($("#rdbraw").prop("checked") == true) {
          //        ItemType = "1";
          //    }
          //    else if ($("#rdbsemi").prop("checked") == true) {
          //        ItemType = "5";
          //    }

          //    var Excise = "0";
          //    if ($("#rdbExcise").prop("checked") == true) {
          //        Excise = "1";

          //    }
          //    else if ($("#rdbnonexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }

          //    else if ($("#rdballexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }



          //    $("#ddlProducts").supersearch({
          //        Type: "Product",
          //        Caption: "Please enter Item Name/Code ",
          //        AccountType: "",
          //        Godown: $("#ddlGodown").val(),
          //        ItemType: ItemType,
          //        Excise: Excise,
          //        Width: 214,
          //        DefaultValue: 0
          //    });
          //}
          //);



          //  $("#rdball").change(
          //function () {

          //    $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
          //    var ItemType = "";

          //    if ($("#rdbcfinished").prop("checked") == true) {
          //        ItemType = "3";
          //    }
          //    else if ($("#rdbraw").prop("checked") == true) {
          //        ItemType = "1";
          //    }
          //    else if ($("#rdbsemi").prop("checked") == true) {
          //        ItemType = "5";
          //    }
          //    else if ($("#rdball").prop("checked") == true) {
          //        ItemType = "-1";
          //    }
          //    var Excise = "0";
          //    if ($("#rdbExcise").prop("checked") == true) {
          //        Excise = "1";

          //    }
          //    else if ($("#rdbnonexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }

          //    else if ($("#rdballexcise").prop("checked") == true) {
          //        Excise = "2";
          //    }



          //    $("#ddlProducts").supersearch({
          //        Type: "Product",
          //        Caption: "Please enter Item Name/Code ",
          //        AccountType: "",
          //        Godown: $("#ddlGodown").val(),
          //        ItemType: ItemType,
          //        Excise: Excise,
          //        Width: 214,
          //        DefaultValue: 0
          //    });
          //}
          //);


            $("#rdbExcise").change(
          function () {


              ResetControls();

              $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
              var ItemType = "";

              if ($("#rdbcfinished").prop("checked") == true) {
                  ItemType = "3";
              }
              else if ($("#rdbraw").prop("checked") == true) {
                  ItemType = "1";
              }
              else if ($("#rdbsemi").prop("checked") == true) {
                  ItemType = "5";
              }

              var Excise = "0";
              if ($("#rdbExcise").prop("checked") == true) {
                  Excise = "1";

              }
              else if ($("#rdbnonexcise").prop("checked") == true) {
                  Excise = "2";
              }

              else if ($("#rdballexcise").prop("checked") == true) {
                  Excise = "2";
              }


              $("#ddlProducts").supersearch({
                  Type: "Product",
                  Caption: "Please enter Item Name/Code ",
                  AccountType: "",
                  Godown: $("#ddlGodown").val(),
                  ItemType: ItemType,
                  Excise: Excise,
                  Width: 214,
                  DefaultValue: 0
              });
          }
          );




            $("#rdbnonexcise").change(
          function () {

              ResetControls();

              $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
             

              if ($("#rdbcfinished").prop("checked") == true) {
                  ItemType = "3";
              }
              else if ($("#rdbraw").prop("checked") == true) {
                  ItemType = "1";
              }
              else if ($("#rdbsemi").prop("checked") == true) {
                  ItemType = "5";
              }

              var Excise = "0";
              if ($("#rdbExcise").prop("checked") == true) {
                  Excise = "1";

              }
              else if ($("#rdbnonexcise").prop("checked") == true) {
                  Excise = "2";
              }

              else if ($("#rdballexcise").prop("checked") == true) {
                  Excise = "2";
              }



              $("#ddlProducts").supersearch({
                  Type: "Product",
                  Caption: "Please enter Item Name/Code ",
                  AccountType: "",
                  Godown: $("#ddlGodown").val(),
                  ItemType: ItemType,
                  Excise: Excise,
                  Width: 214,
                  DefaultValue: 0
              });
          }
          );



            $("#rdballexcise").change(
          function () {

              ResetControls();

              $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");
              if ($("#rdbcfinished").prop("checked") == true) {
                  ItemType = "3";
              }
              else if ($("#rdbraw").prop("checked") == true) {
                  ItemType = "1";
              }
              else if ($("#rdbsemi").prop("checked") == true) {
                  ItemType = "5";
              }

              var Excise = "0";
              if ($("#rdbExcise").prop("checked") == true) {
                  Excise = "1";

              }
              else if ($("#rdbnonexcise").prop("checked") == true) {
                  Excise = "2";
              }

              else if ($("#rdballexcise").prop("checked") == true) {
                  Excise = "2";
              }



              $("#ddlProducts").supersearch({
                  Type: "Product",
                  Caption: "Please enter Item Name/Code ",
                  AccountType: "",
                  Godown: $("#ddlGodown").val(),
                  ItemType: ItemType,
                  Excise: Excise,
                  Width: 214,
                  DefaultValue: 0
              });
          });





            $("#txtOrderDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtBillDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDispDate").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());
            $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());

            $('#txtOrderDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtBillDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });



            $('#txtDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });


            $('#txtDispDate').daterangepicker({
                singleDatePicker: true,
                calender_style: "picker_1"
            }, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });






            $.ajax({
                type: "POST",
                data: '{ }',
                url: "managekits.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    mrpeditable = obj.Mrp;
                    saleRateeditable = obj.Sale;


                    if (saleRateeditable == "0") {

                        $("#txtRate").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtRate").removeAttr('disabled');
                    }

                    if (mrpeditable == "0") {
                        $("#txtMarketPrice").attr('disabled', 'disabled');

                    }
                    else {

                        $("#txtMarketPrice").removeAttr('disabled');
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });


            ResetList();

            $("#btnCancelDialog").click(
           function () {
               //BindGrid();
               //$("#KitDialog").dialog("close");
               //$("#btnSave").removeAttr('disabled');
               window.location = "managedeliverynote.aspx";

           }
           );









           // $("#txtQty").keyup(
           //function () {

           //    CommonCalculation();

           //}
           //);

           // $("#txtRate").keyup(
           //function () {
           //    CommonCalculation();


           //});





            $("#ddlGodown").change(
            function () {




                $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");


                if ($("#rdbcfinished").prop("checked") == true) {
                    ItemType = "3";
                }
                else if ($("#rdbraw").prop("checked") == true) {
                    ItemType = "1";
                }
                else if ($("#rdbsemi").prop("checked") == true) {
                    ItemType = "5";
                }

                var Excise = "0";
                if ($("#rdbExcise").prop("checked") == true) {
                    Excise = "1";

                }
                else if ($("#rdbnonexcise").prop("checked") == true) {
                    Excise = "2";
                }

                else if ($("#rdballexcise").prop("checked") == true) {
                    Excise = "2";
                }



                $("#ddlProducts").supersearch({
                    Type: "Product",
                    Caption: "Please enter Item Name/Code ",
                    AccountType: "",
                    Godown: $("#ddlGodown").val(),
                    ItemType: ItemType,
                    Excise: Excise,
                    Width: 214,
                    DefaultValue: 0
                });

            }
            );



            BindGrid();


            $("#btnDelete").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Kit is selected to Delete");
               return;
           }

           var BillNo = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Bill_No')


           var Prefix = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'Prefix')

           if (confirm("Are You sure to delete this record")) {
               $.uiLock('');


               $.ajax({
                   type: "POST",
                   data: '{"BillNo":"' + BillNo + '","Prefix":"' + Prefix + '"}',
                   url: "managedeliverynote.aspx/Delete",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);


                       BindGrid();
                       alert("Delivery Note is Deleted successfully.");

                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $.uiUnlock();
                   }
               });

           }


       }
       );


        });
   
    </script>
    
    <script>
          $( function() {
              $("#dvlast3Dialog").draggable();
          } );
     </script>

    <style>
        
        textarea#txtremarks {
            width: 100%;
            border: 1px solid #c2c2c2;
        }
        td#DKID div#dvOuter_ddlProducts {
            width: 320px !important;
        }
        tbody#tbKitProducts td {
            text-align: center !important;
        }
        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable {
            min-height: 656px;
            background:#fff;
        }
        div#dvDialog{
            background:#fff;
        }
        button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
            display: none;
        }
        .manage_table_top td {
            padding: 3px 10px;
        }
       .manage_table_top select, .manage_table_top input {
            height: 24px;
        }
       div#dvlast3Dialog {
            left: 400px;
            top: 120px;
            z-index:4;
        }
        [name="ddlProducts"] {
            width: 322px;
        }
        [name="txtstock"],[name="txtExcise"] {
            width: 70px;
        }
        [name="txtQty"] {
            width: 60px !important;
        }
        [name="txtRate"],[name="txtMRP1"],[name="txtAmount"] {
            width: 120px !important;
        }
        .fix-height-delivery-note {
            float: left;
            width: 100%;
            min-height: 267px;
            overflow: auto;
            max-height: 267px;
        }
        .page-title
        {
            padding:0;
        }
        @media(max-width:480px) 
        {
            div#dvlast3Dialog {
                left: 0;
                top: 70px;
                overflow: auto;
                height: 200px;
                width: 100%;
            }
        }
        @media (max-width:767px) {
            div#dvlast3Dialog {
                left: 0;
                top: 70px;
            }
        }
        @media (min-width:768px) and (max-width:991px) {
            div#dvlast3Dialog {
                left: 16%;
            }
        }
        @media (min-width:992px) and (max-width:1024px)
        {
            [name="ddlProducts"] {
	            width: 200px;
            }
            [name="txtQty"] {
	            width: 50px !important;
            }
            [name="txtRate"], [name="txtMRP1"], [name="txtAmount"] {
	            width: 70px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 40px !important;
            }
            [name="txtstock"], [name="txtExcise"] {
	            width: 60px;
            }

        }
        @media (min-width:1025px) and (max-width:1100px)
        {
            [name="ddlProducts"] {
	            width: 215px;
            }
            [name="txtQty"] {
	            width: 50px !important;
            }
            [name="txtRate"], [name="txtMRP1"], [name="txtAmount"] {
	            width: 75px !important;
            }
            [name="txtDis1Per"], [name="txtDis2Per"] {
	            width: 40px !important;
            }
            [name="txtstock"], [name="txtExcise"] {
	            width: 60px;
            }

        }
        @media (min-width:1101px) and (max-width:1200px)
        {
            [name="ddlProducts"] {
	            width: 235px;
            }
            [name="txtRate"], [name="txtMRP1"], [name="txtAmount"] {
	            width: 80px !important;
            }
            
        }
        @media (min-width:1201px) and (max-width:1305px)
        {
            [name="ddlProducts"] {
	            width: 270px;
            }
            [name="txtRate"], [name="txtMRP1"], [name="txtAmount"] {
	            width: 100px !important;
            }
            
        }
        @media (min-width:992px) and (max-width:1300px) {
            div#dvlast3Dialog {
                left: 23%;
             }
        }
    </style>


    <form runat="server" id="formID" method="post">
    <asp:HiddenField ID="hdnRoles" runat="server" />
      <input type="hidden" id="hdntodaydate" runat="server" value="0"/>
      <input type="hidden" id="hdnpassing" clientidmode="Static" runat="server" value="0"/>
    <asp:HiddenField ID="hdnDate" runat="server" />
                <input type="hidden" id="hdnCounter" value="1" />
          <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Delivery Note</h3>
                </div>
                <div class="x_panel">
                    <div class="form-group">
                        <div class="youhave" style="padding-left: 1px">
                           <table width="100%">
                                <tr>
                                    <td align="left">
                                        <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>
                                                    Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                </td>
                                                <td>
                                                    Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px;
                                                        background-color: White" id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="Div1">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <table cellspacing="0" cellpadding="0" style="margin-top: 5px">
                                <tr>
                                    <td>
                                        <!--&nbsp;-->
                                    </td>
                                    <td>
                                        <div id="btnNew" class="btn btn-primary" style="margin-right:8px;">
                                        <i class="fa fa-external-link"></i>
                                            New</div>
                                    </td>
                                    <td>
                                        <!--&nbsp;-->
                                    </td>
                                    <td>
                                        <div id="btnEdit" class="btn btn-success" style="margin-right:8px;">
                                        <i class="fa fa-edit m-right-xs"></i>
                                            Edit</div>
                                    </td>
                                    <td>
                                         <!--&nbsp;-->
                                    </td>
                                    <td>
                                        <div id="btnDelete" class="btn btn-danger" style="margin-right:8px;">
                                        <i class="fa fa-trash m-right-xs"></i>
                                            Delete</div>
                                    </td>

                                       <td><div class="btn btn-danger" style="display: block;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> Reprint</div></td>
                                </tr>
                            </table>
                            <div id="jQGridDemoPager">
                            </div>
                        </div>
                        <div class="inventry_adddialog" id="KitDialog" style="display: none;" title="<%= Request.Cookies[Constants.BranchName].Value %>">
                            <uc1:AddKit ID="ucAddKit" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <!-- /page content -->

            
       
        </div>
    </form>
    <script type="text/javascript">

        

                function BindGrid() {


                    var DateFrom=$("#txtDateFrom").val();
     var DateTo=$("#txtDateTo").val();
     jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/managedelivery.ashx?dateFrom='+DateFrom+'&dateTo='+DateTo,
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['RefNo','BillPrefix','Prefix','BillNo','Date','Customer','GrNo','GrDate','VehNo','Value','ForBranch','Dis3P','Dis3Amt','TaxP','TaxAmt','TotalAmt','Adjustment','NetAmount','CCode','GodownID','FormName','DespDate','Passing','DeliveryReceived'],
  


                        colModel: [
                            { name: 'BillPrefix', index: 'BillPrefix', width: 100, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                            { name: 'BillPrefix', index: 'BillPrefix', width: 100, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                            { name: 'Prefix', index: 'Prefix', width: 100, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                            { name: 'Bill_No', index: 'Bill_No', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                    { name: 'strBillDate', index: 'strBillDate', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                     { name: 'Dealer', index: 'Dealer', width: 400, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'GR_No', index: 'GR_No', width:400, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'strBillDate', index: 'strBillDate', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Veh_No', index: 'Veh_No', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
							{ name: 'BranchName', index: 'BranchName', width: 100, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                            { name: 'Dis3P', index: 'Dis3P', width: 100, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                                    { name: 'Dis3Amt', index: 'Dis3Amt', width:400, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'TaxP', index: 'TaxP', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'TaxAmt', index: 'TaxAmt', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Total_Amount', index: 'Total_Amount', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                            { name: 'Adjustment', index: 'Adjustment', width: 100, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                            { name: 'Net_Amount', index: 'Net_Amount', width: 100, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: true, editrules: { required: true } },
                                    { name: 'CCODE', index: 'CCODE', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Godown_ID', index: 'Godown_ID', width:150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'Form_Name', index: 'Form_Name', width: 150, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },

                                     { name: 'strDespDate', index: 'strDespDate', width:100, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                   
                                    { name: 'PASSING', index: 'PASSING', width:100, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'delchk', index: 'delchk', width: 100, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                   

                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'Kit_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Delivery Note List",

                        editurl: 'handlers/managedelivery.ashx',
                           ignoreCase: true,
                         toolbar: [true, "top"],


                    });

                     var $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                return false;
            });



                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                  ResetControls();
            m_KitID = $('#jQGridDemo').jqGrid('getCell', rowid, 'Bill_No');
 
             
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


    </script>
</asp:Content>

