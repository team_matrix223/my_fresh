﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;

public partial class assign_homedelvry : System.Web.UI.Page
{
  assign_homedel ah = new assign_homedel();
  string ddlval;
  protected void Page_Load(object sender, EventArgs e)
  {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

        if (strDate == "")
        {

            Response.Redirect("index.aspx?DayOpen=Close");
        }
        if (IsPostBack == false)
    {
      getsethomedel();
    }


  }

  void getsethomedel()
  {
    ah.req = "getrecords";
    DataTable dt = ah.gethomedel();
    gv_display.DataSource = dt;
    gv_display.DataBind();
        ViewState["dirState"] = dt;
        ViewState["sortdr"] = "Asc";
    }

  protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
  {
    if (e.Row.RowType == DataControlRowType.DataRow && gv_display.EditIndex == e.Row.RowIndex)
    {
            Connection connn = new Connection();
            SqlConnection con = new SqlConnection(connn.sqlDataString);
            // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
            //DropDownList ddlempo = (e.Row.FindControl("dd_employee") as DropDownList);
            DropDownList ddlempo = (DropDownList)e.Row.FindControl("dd_employee");
      con.Open();
      SqlCommand cmd = new SqlCommand("select *,case when Name='Packing' then 1 else 0 end as Names from dbo.employinfo where isdeliveryboy='1'  order by Names desc,Name ", con);
      cmd.CommandType = CommandType.Text;
      DataTable dt = new DataTable();
      SqlDataAdapter adp = new SqlDataAdapter(cmd);
      adp.Fill(dt);
      ddlempo.DataSource = dt;
      ddlempo.DataTextField = "Name";
      ddlempo.DataValueField = "code";
      ddlempo.DataBind();
      System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select Employee-", "0");
      ddlempo.Items.Insert(0, listItem1);
      //ddlempo.Items.FindByValue((e.Row.FindControl("dd_employee") as Label).Text).Selected = true;
      con.Close();

    }
  }

  protected void EditCustomer(object sender, GridViewEditEventArgs e)
  {
    gv_display.EditIndex = e.NewEditIndex;
    getsethomedel();
  }

  protected void CancelEdit(object sender, GridViewCancelEditEventArgs e)
  {
    gv_display.EditIndex = -1;
    getsethomedel();
  }
  protected void UpdateCustomer(object sender, GridViewUpdateEventArgs e)
  {

    ah.req = "insert";
    string billnowprefix = (gv_display.Rows[e.RowIndex].FindControl("lblbillnowprefix_edit") as Label).Text;
    ah.billnowprefix = billnowprefix;
    string ddlempo = (gv_display.Rows[e.RowIndex].FindControl("dd_employee") as DropDownList).SelectedItem.Value;
    string Customer_name = (gv_display.Rows[e.RowIndex].FindControl("lblcstname") as Label).Text;
    string employe_Name = (gv_display.Rows[e.RowIndex].FindControl("dd_employee") as DropDownList).SelectedItem.Text;
    string Amount = (gv_display.Rows[e.RowIndex].FindControl("lblcsamount") as Label).Text;
    string CustomerContact = (gv_display.Rows[e.RowIndex].FindControl("lblcstcontactno") as Label).Text;
    //DropDownList ddlempo = gv_display.FindControl("dd_employee") as DropDownList;
    ah.emp_id = Convert.ToInt32(ddlempo);
    ah.insert_update_homedel();
    gv_display.EditIndex = -1;
    getsethomedel();
    if (Customer_name != "Cash")
    {
      SendToMeassage(Customer_name, employe_Name, ddlempo, billnowprefix, CustomerContact);
    }
  }

  public void SendToMeassage(string Customer_name, string employe_Name, string EmpID, string billno,string ConatactNo)
  {
    Connection con = new Connection();
    SqlParameter[] Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@MessageText", "Order_Dispatched_Msg");
    var Message = SqlHelper.ExecuteScalar(con.sqlDataString, CommandType.StoredProcedure, "Proc_getbillingsms", Param);
    if (Message != null)
    {
      string qry = "select contactno from [dbo].[EmployInfo] where code=" + EmpID;
            Connection connn = new Connection();
            SqlConnection cons = new SqlConnection(connn.sqlDataString);
            // SqlConnection cons = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
            cons.Open();
      SqlCommand cmd = new SqlCommand(qry, cons);
      SqlDataReader rd = cmd.ExecuteReader();
      if (rd.HasRows)
      {
        rd.Read();
        var Employe_Contact = rd["ContactNo"].ToString();
        var CustomerName = Customer_name;
        Message = Message.ToString().Replace("$CN$", CustomerName).Replace("$EN$", employe_Name).Replace("$PN$", Employe_Contact).Replace("$No$", billno);
        //SendSms SendMessage = new SendSms();
        //SendMessage.SMSSend(ConatactNo, Message.ToString());
      }

    }
  }

    protected void gv_display_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Asc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            gv_display.DataSource = dtrslt;
            gv_display.DataBind();


        }

    }

    protected void gv_display_viewall_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Asc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            gv_display_viewall.DataSource = dtrslt;
            gv_display_viewall.DataBind();


        }

    }

    protected void btnsubmit_Click(object sender, EventArgs e)
  {


    if (btnsubmit.Text == "View All")
    {
      gv_display.Visible = false;
      gv_display_viewall.Visible = true;
      ah.req = "gethomedel_detail";
      DataTable dt = ah.gethomedel();
      gv_display_viewall.DataSource = dt;
      gv_display_viewall.DataBind();
      ViewState["dirState"] = dt;
      ViewState["sortdr"] = "Asc";
      btnsubmit.Text = "Back";
  


    }
    else
    {
      Response.Redirect("assign_homedelvry.aspx");
      btnsubmit.Text = "View All";

    }
  }

    protected void btnraisebill_Click(object sender, EventArgs e)
    {
        Response.Redirect("BillScreen.aspx");
    }

    protected void btnkot_Click(object sender, EventArgs e)
    {
  
        Response.Redirect("manageKotscreen.aspx");
    }
}