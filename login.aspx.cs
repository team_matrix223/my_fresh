﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class login : System.Web.UI.Page
{
    public string LogBG_IMG;
    string DeviceType = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        //DirectoryInfo di = new DirectoryInfo(Server.MapPath("/images/DashImg/"));
        //FileInfo fi = di.GetFiles()[0];
        //this.LogBG_IMG = "/images/DashImg/" + fi;
        try
        {
            
            
            DeviceType = Request.QueryString["dt"].ToString();
        }
        catch 
        {
            DeviceType = "D";

        }

        try
        {
            Response.Cookies[Constants.AdminId].Value = "0";
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies[Constants.dbname].Value))
            {
                dvlocation.Visible = true;
                dvmasterkey.Visible = false;

            } 
         
        }
        catch
        {
            dvlocation.Visible = false;
            dvmasterkey.Visible = true;
            

        }

        if (!IsPostBack)
        {
            try
            {
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["DCookie"].Value))
                {
                    string mstrkey = HttpContext.Current.Request.Cookies["DCookie"].Value.ToString();

                    SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
                    SqlCommand cmd = new SqlCommand("strp_getdb", con);
                    cmd.Parameters.AddWithValue("@mater_key", mstrkey);
                    cmd.CommandType = CommandType.StoredProcedure;
                    DataTable dt = new DataTable();
                    SqlDataAdapter adb = new SqlDataAdapter(cmd);
                    adb.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        ddlLocation.DataSource = dt;
                        ddlLocation.DataBind();
                        dvlocation.Visible = true;
                        dvmasterkey.Visible = false;
                    }

                    //   BindBranches();
                }
            }
            catch
            {


            }
        }


    }

    void BindBranches()
    {


        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }
    
    protected void itemSelected(object sender, EventArgs e)
    {

        var locid = ddlLocation.SelectedValue.ToString();
        
      // var locid = ddlLocation.SelectedItem.Text;
        if (locid != "")
        {
            string dbname = "";
           SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
            SqlDataAdapter adp = new SqlDataAdapter("select db_name from master_dbcon where Id = " + locid + " ", con);
            DataTable dt = new DataTable();
            dt.Clear();
            adp.Fill(dt);
            if(dt.Rows.Count > 0)
            {
                 dbname = dt.Rows[0]["db_name"].ToString();

            }
            if (dbname != "")
            {
                EncDyc ED = new EncDyc();
                //string constring = "server =ms4; database = " + locid + "; uid = sa; pwd =matrix; Connect Timeout=10000";
                string constring = "server =49.50.124.155,1443; database = " + dbname + "; uid = ms; pwd =ZXD#%@SD54g; Connect Timeout=10000";
                Response.Cookies[Constants.DataBase].Value = dbname;
                Response.Cookies[Constants.dbname].Value = ED.Encrypt(constring);
                //Response.Cookies[Constants.abc].Value = "hello";
                //Response.Cookies[Constants.posid].Value = "3";
                HttpCookie cookie = new HttpCookie(Constants.dbname);
                cookie.Value = ED.Encrypt(constring);
                cookie.Expires = DateTime.Now.AddYears(100);
                Response.SetCookie(cookie);
                BindBranches();
                dvmasterkey.Visible = false;
                dvlocation.Visible = true;
            }
        }


        //      if (locid == "1")
        //      {
        //          Response.Cookies[Constants.DataBase].Value = "1";
        //          BindBranches();
        //      }
        //      else if (locid == "2")
        //      {
        //          Response.Cookies[Constants.DataBase].Value = "6";


        //          BindBranches();
        //      }
        // else if (locid == "3")
        //      {
        //          Response.Cookies[Constants.DataBase].Value = "7";
        //          BindBranches();
        //      }
        // else if (locid == "4")
        //      {
        //          Response.Cookies[Constants.DataBase].Value = "8";
        //          BindBranches();
        //}
        else
        {

            Response.Write("<script>alert('Choose Location First')</script>");
            return;
        }


    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Int32 pos = 0;
        Int32 screen = 0;
       
        
        
       
        pos = Convert.ToInt32((HttpContext.Current.Request.Cookies["DCookie1"].Value));


        Connection con1 = new Connection();
         SqlConnection con = new SqlConnection(con1.sqlDataString);
        SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + pos + "", con);
        DataTable dt = new DataTable();
        dt.Clear();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());
            Response.Cookies[Constants.ScreenNo].Value = screen.ToString();
        }
        else
        {
            Response.Write("<script>alert('PosId doesn't Exists')</script>");
            return;
        }


        if (ddlBranch.SelectedValue == "0")
        {

            Response.Write("<script>alert('Choose Branch First')</script>");
            return;
        }

        User objuser = new User()
        {
            User_ID = txtUserName.Value,
            UserPWD = txtPassword.Value

        };

        int Branchvalue = Convert.ToInt32(ddlBranch.SelectedValue);

        Int32 status = new UserBLL().SuperUserLoginCheck(objuser,Branchvalue);


        if (status.ToString() == "-1")
        {
            Response.Write("<script>alert('Invalid User Name');</script>");
        }
        else if (status.ToString() == "-2")
        {
            Response.Write("<script>alert('Invalid Password');</script>");
        }
        else if (status.ToString() == "-3")
        {
            Response.Write("<script>alert('Invalid Branch');</script>");
        }
        else
        {


            Response.Redirect("SuperWelcome.aspx?bid=" + ddlBranch.SelectedValue + "&bname=" + ddlBranch.SelectedItem.Text + "&UserId=" + status + "&UserName=" + txtUserName.Value.Trim() + "&Device=" +DeviceType + "&Screen="+screen);
        }


    }

    protected void btnchkkey_Click(object sender, EventArgs e)
    {
        if (txtPos.Text == string.Empty)
        {
            Response.Write("<script>alert('Enter PosId')</script>");
            txtPos.Focus();
            return;
        }
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        SqlCommand cmd = new SqlCommand("strp_getdb", con);
        cmd.Parameters.AddWithValue("@mater_key", tbmasterkey.Text.Trim());
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count > 0)
        {
           
            ddlLocation.DataSource = dt;

            
            ddlLocation.DataBind();
           
            HttpCookie aCookie = Request.Cookies["DCookie"];
            Response.Cookies["DCookie"].Value = tbmasterkey.Text.Trim();
            Response.Cookies["DCookie"].Expires = DateTime.Now.AddDays(100);
            Response.Cookies[Constants.keyName].Value = tbmasterkey.Text.Trim();
            //if (txtPos.Text == "")
            //{
            //    Response.Cookies[Constants.posid].Value = "0";
            //}
            //else
            //{

            HttpCookie aCookie1 = Request.Cookies["DCookie1"];
            Response.Cookies["DCookie1"].Value = txtPos.Text.Trim();
            Response.Cookies["DCookie1"].Expires = DateTime.Now.AddDays(100);
            Response.Cookies[Constants.posid].Value = txtPos.Text.Trim();
            //}

            //EncDyc ED = new EncDyc();
            //string constring = "server =" + dt.Rows[0][1].ToString() + "; database = " + dt.Rows[0][2].ToString() + "; uid = " + dt.Rows[0][3].ToString() + "; pwd =" + dt.Rows[0][4].ToString() + "";

            //Response.Cookies[Constants.dbname].Value = ED.Encrypt(constring);

            //HttpCookie cookie = new HttpCookie(Constants.dbname);
            //cookie.Value = ED.Encrypt(constring);
            //cookie.Expires = DateTime.Now.AddYears(100);
            //Response.SetCookie(cookie);
            dvmasterkey.Visible = false;
            dvlocation.Visible = true;

            //if (Map_logpath == "1")
            //{
            //    Response.Redirect("Login.aspx?q=1");
            //}
            //else if (Map_logpath == "2")
            //{
            //    Response.Redirect("StewardLogin.aspx?q=1");
            //}

        }
        else
        {
            dvmasterkey.Visible = true;
            dvlocation.Visible = false;
         
            Response.Write("<script>alert('Invalid Key')</script>");
        }
    }
}