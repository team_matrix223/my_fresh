﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevExpress.XtraPrinting;

public partial class Reports_PrintKOT : System.Web.UI.Page
{
    public string BillNowPrefix { get { return Request.QueryString["BillNowPrefix"] != null ? Convert.ToString(Request.QueryString["BillNowPrefix"]) : string.Empty; } }
    public string DepartmentName { get { return Request.QueryString["DepartmentName"] != null ? Convert.ToString(Request.QueryString["DepartmentName"]) : string.Empty; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            RptPrintKot r = new RptPrintKot(BillNowPrefix, DepartmentName);
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);
            Page.Response.End();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Response.Redirect("rptKOTPrint.aspx?billnowprefix=" + TextBox1.Text + "&departmentname=" + TextBox2.Text);
    }
}