﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="changePaymode.aspx.cs" Inherits="changePaymode" %>

<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/changepaymode.css" rel="stylesheet" />
    <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
    <link href="css/css.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">



        function BindBanks() {
            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindBanks",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlBank").html(obj.BankOptions);
                    $("#ddlBank option[value='" + DefaultBank + "']").prop("selected", true);
                }


            });
        }








        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }



            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }



        function GetBillByBNF() {
            var Mode = "";

            var BNF = $("#txtBillNumber").val();


            $.ajax({
                type: "POST",
                data: '{ "BNF": "' + BNF + '"}',
                url: "changePaymode.aspx/GetBillByBNF",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                    $("#<%=ddlOtherPayment.ClientID %>").val(obj.OtherPaymode);
                    $("#ddlbillttype").val(obj.BillData.BillMode);
                    $("#txtFinalBillAmount").val(obj.BillData.Net_Amount);

                    if ($("#txtFinalBillAmount").val() != 0) {

                        $("#btnBillWindowOk").removeAttr("disabled");

                    }
                    else {
                        alert("BillNo Does Not Exists");
                        return;

                    }

                    $("#Text13").val(obj.BillData.CrCard_Amount);
                    $("#ddlBank").val(obj.BillData.CreditBank);
                    if (obj.BillData.BillMode == "Credit") {
                        Mode = "Credit";
                        CheckMode(Mode)
                        $("#ddlChosseCredit").val(obj.BillData.Customer_ID);
                        $("#lblCreditCustomerName").html(obj.BillData.Customer_Name);
                        $("#creditCustomer").css("display", "block");
                        $("#dvOuter_ddlCreditCustomers").show();
                        $("#ddlChosseCredit").show();
                        $("#txtCashReceived").val(obj.BillData.Credit_Amount);
                        $("#lblCreditCustomerName").show();
                        $("#lblBank").hide();
                        $("#lblCustomer").show();
                    }
                    else if (obj.BillData.BillMode == "OnlinePayment") {
                        // $("#txtCashReceived").val(obj.BillData.OnlinePayment);
                        Mode = "OnlinePayment";
                        CheckMode(Mode)
                        if (obj.Mode == 'OnlinePayment') {
                            $("#OnlineChk").prop('checked', true);
                            $("#txtpaymentAmount").val(obj.Amount);
                            $("#txtpaymentAmount").attr('readonly', 'readonly');
                            $("#txtCODAmount").attr('readonly', 'readonly')
                            if ($("#txtpaymentAmount").val() != "0" || $("#txtCODAmount").val() != "0") {
                                $("#txtBalanceReturn").val(0);
                            }
                               
                        }
                        else if (obj.Mode == 'COD') {
                            $("#codChk").prop('checked', true);
                            $("#txtCODAmount").val(obj.Amount);
                            $("#txtCODAmount").attr('readonly', 'readonly');
                            $("#txtpaymentAmount").attr('readonly', 'readonly');
                            if ($("#txtpaymentAmount").val() != "0" || $("#txtCODAmount").val() != "0") {
                                $("#txtBalanceReturn").val(0);
                            }
                        }
                        $("#lblBank").hide();
                        $("#lblCustomer").hide();
                    }
                    else if (obj.BillData.BillMode == "Cash") {
                        Mode = "Cash";
                        CheckMode(Mode)
                        $("#txtCashReceived").val(obj.BillData.Cash_Amount);
                        $("#lblBank").hide();
                        $("#lblCustomer").hide();
                       
                    }
                    else if (obj.BillData.BillMode == "CreditCard") {
                        Mode = "CreditCard";
                        CheckMode(Mode)
                        $("#txtCashReceived").val(obj.BillData.Cash_Amount);
                        $("#lblBank").show();
                        $("#lblCustomer").hide();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    if ($("#ddlbillttype").val() == "Cash" || $("#ddlbillttype").val() == "OnlinePayment") {
                        $("#Text13").attr("disabled", "disabled");
                        $("#ddlBank").attr("disabled", "disabled");
                        $("#txtCashReceived").removeAttr("disabled");
                        $("#dvOuter_ddlCreditCustomers").hide();
                        $("#ddlChosseCredit").hide();
                        $("#ddlChosseCredit").val(0);


                    }
                    else if ($("#ddlbillttype").val() == "CreditCard") {

                        $("#Text13").removeAttr("disabled");
                        $("#ddlBank").removeAttr("disabled");
                        $("#txtCashReceived").attr("disabled", "disabled");
                        $("#dvOuter_ddlCreditCustomers").hide();
                        $("#ddlChosseCredit").hide();
                        $("#ddlChosseCredit").val(0);

                    }
                    else if ($("#ddlbillttype").val() == "Credit") {
                        $("#Text13").attr("disabled", "disabled");
                        $("#ddlBank").attr("disabled", "disabled");
                        $("#txtCashReceived").removeAttr("disabled");
                        $("#dvOuter_ddlCreditCustomers").show();
                        $("#ddlChosseCredit").show();

                    }

                    DEVBalanceCalculation();
                    if ($("#txtpaymentAmount").val() != "0" || $("#txtCODAmount").val() != "0") {
                        $("#txtBalanceReturn").val(0);
                    }
                   
                    $.uiUnlock();

                }

            });

        }

        function Reset() {
            $("#txtCashReceived").val("0.00");
            $("#Text13").val("0.00");
            $("#ddlBank").val("");
            $("#dvOuter_ddlCreditCustomers").hide();

            $("#ddlChosseCredit").hide();
            $("#ddlChosseCredit").val(0);
        }


        function ChangePaymode() {
            var Mode = "";
            var BNF = $("#txtBillNumber").val();
            var OTPVal = $("#OTPVal").val();
            if (BNF == "") {
                alert("Please Enter Bill Number");
                $("#txtBillNumber").focus();
                return;
            }
            var BillType = $("#ddlbillttype").val();
            var CashAmount = $("#txtCashReceived").val();
            var CreditCustId = $("#ddlChosseCredit").val();


            if (BillType == "Cash" || BillType == "CreditCard" || BillType == "OnlinePayment") {

                if (Number($("#txtBalanceReturn").val()) < 0) {

                    $.uiUnlock();
                    alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                    $("#btnBillWindowOk").removeAttr('disabled');
                    return;
                }
                else {
                    CashAmount = CashAmount - Number($("#txtBalanceReturn").val());
                }

            }


            if (BillType == "CreditCard") {
                if ($("#ddlBank").val() == "") {
                    alert("Choose Bank");
                    $("#ddlBank").focus();
                    return;
                }
            }

            if (BillType == "Credit") {

                if (CreditCustId == 0) {

                    alert("Please select Credit Customer");
                    $("#txtddlCreditCustomers").focus();

                    return;
                }


            }


            if (Number(CashAmount) < 0) {
                $.uiUnlock();
                alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
                $("#btnBillWindowOk").removeAttr('disabled');
                return;
            }



            var OtherPaymentID = $("#<%=ddlOtherPayment.ClientID %>").val();
            if ($("#codChk").prop('checked') == true) {
                Mode = "COD"
                CashAmount = parseFloat($("#txtCODAmount").val()).toFixed(2);
            }
            else if ($("#OnlineChk").prop('checked') == true) {
                Mode = "OnlinePayment";
                CashAmount = parseFloat($("#txtpaymentAmount").val()).toFixed(2);
            }
            var CrCardAmount = $("#Text13").val();
            var Bank = $("#ddlBank").val();
            var BankName = $("#ddlBank").text();

            var CreditAmount = $("#txtCashReceived").val();

            var CreditCustName = $("#ddlChosseCredit option:selected").text();
            var OnlinePayment = $("#txtCashReceived").val();
            if ($("#txtFinalBillAmount").val() != "") {
                FinalAmount = parseFloat($("#txtFinalBillAmount").val()).toFixed(2);
            }

            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BNF + '","BillType": "' + BillType + '","CashAmount": "' + CashAmount + '","CrCardAmount": "' + CrCardAmount + '","Bank": "' + Bank + '","BankName": "' + BankName + '","CreditAmount": "' + CreditAmount + '","CreditCustId": "' + CreditCustId + '","CreditCustName": "' + CreditCustName + '","OnlinePayment":"' + OnlinePayment + '","OtherPaymentID":"' + OtherPaymentID + '","Mode":"' + Mode + '","FinalAmount":"' + FinalAmount + '","OTPVal":"' + OTPVal + '"}',
                url: "changePaymode.aspx/ChangePaymode",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    Mode = "";
                    $("#<%=ddlOtherPayment.ClientID %>").val(0);
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                    $("#txtCODAmount").val(0);
                    $("#txtpaymentAmount").val(0);
                    alert("PayMode Changed Successfully");

                    return;

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#btnBillWindowOk").attr("disabled", "disabled");
                    Reset();
                    $("#ddlbillttype").val("Cash");
                    $("#txtFinalBillAmount").val("0.00");
                    $("#txtBillNumber").val("");
                   
                    $.uiUnlock();

                }

            });

        }





        $(document).ready(
        function () {

            $("#btnBillWindowOk").attr("disabled", "disabled");

            $("#ddlChosseCredit").change(
                     function () {


                         if ($("#ddlChosseCredit").val() != "0") {


                             CrdCustSelId = $("#ddlChosseCredit").val();
                             $("#hdnCreditCustomerId").val(CrdCustSelId);
                             CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                             $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())

                             $("#creditCustomer").css("display", "block");

                         }




                     });



            $("#ddlCreditCustomers").supersearch({
                Type: "Accounts",
                Caption: "Please enter Customer Name/Code ",
                AccountType: "D",
                Width: 100,
                DefaultValue: 0,
                Godown: 0
            });


            $("#txtCashReceived").keyup(
         function () {
             var regex = /^[0-9\.]*$/;


             var value = jQuery.trim($(this).val());
             var count = value.split('.');


             if (value.length >= 1) {
                 if (!regex.test(value) || value <= 0 || count.length > 2) {

                     $(this).val(0);


                 }
             }


             DEVBalanceCalculation();

         }
         );

            $("#Text13").keyup(
                 function () {
                     var regex = /^[0-9\.]*$/;


                     var value = jQuery.trim($(this).val());
                     var count = value.split('.');


                     if (value.length >= 1) {
                         if (!regex.test(value) || value <= 0 || count.length > 2) {

                             $(this).val(0);


                         }
                     }


                     DEVBalanceCalculation();

                 }
                 );
            $("#Text15").keyup(
                 function () {
                     var regex = /^[0-9\.]*$/;


                     var value = jQuery.trim($(this).val());
                     var count = value.split('.');


                     if (value.length >= 1) {
                         if (!regex.test(value) || value <= 0 || count.length > 2) {

                             $(this).val(0);


                         }
                     }

                     DEVBalanceCalculation();

                 }
                 );







            $("#ddlbillttype").change(
            function () {

                Reset();

                if ($(this).val() == "Credit") {

                    //New Changes //

                    $("#CrCardAmt").hide();
                    $("#txtddlCreditCustomers").show();
                    $("#ddlChosseCredit").show();

                    var FinalAmount = $("#txtFinalBillAmount").val();
                    $("#txtCashReceived").val(parseFloat(FinalAmount).toFixed(2));
                    $("#dvOuter_ddlCreditCustomers").show();
                    $("#lblCashHeading").text("Receipt Amt:");
                    $("#ddlbillttype option[value='Credit']").prop("selected", true);
                    $("#txtCashReceived").val("0").prop("readonly", false);
                    $("#txtCashReceived").removeAttr('disabled', 'disabled');
                    $("#txtCashReceived").show();
                    $("#Text13").val("0").hide();
                    $("#Text14").val("").hide();
                    $("#Text15").val("0").hide();
                    $("#Text16").val("").hide();
                    $("#ddlType").hide();
                    $("#ddlOtherPayment").hide();
                    $("#rdoCOD").hide();
                    $("#rdoOnline").hide();
                    $("#Td1").hide();
                    $("#txtCODAmount").hide();
                    $("#COD").hide();
                    $("#Online").hide();
                    $("#txtpaymentAmount").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#Coupans").hide();
                    $("#OP").hide();
                    $("#coupan").hide();
                    $("#holder input").hide();
                    $("#holder2 input").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#lblCardAmount").hide();
                    $("#lblCreaditType").hide();
                    $("#lblCCNO").hide();
                    $("#lblBank").hide();
                    $("#dropOtherPayemode").hide();
                    $("#OtherPaymodes").hide();
                    //$("#ddlBank").prop("disabled", true);
                    $("#ddlBank").hide();
                    $("#lblBank").hide();
                    $("#CrCardAmt").hide();
                    $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                    $("#creditCustomer").show();
                    $("#lblCustomer").show();
                  

                }
                else if ($(this).val() == "CreditCard") {
                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    //New Changes//
                    $("#lblBank").show();
                    $("#ddlChosseCredit").hide();
                    $("#ddlType").show();
                    $("#Text14").show();
                    $("#ddlBank").show();
                    $("#lblBank").show();
                    $("#lblCardAmount").show();
                    $("#lblCreaditType").show();
                    $("#lblCCNO").show();
                    $("#Text13").show();
                    $("#CrCardAmt").show();
                    $("#lblCustomer").hide();
                    //Hide All Properties//

                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    $("#lblCreditCustomerName").hide();
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#ddlCreditCustomers").hide();
                    $("#txtddlCreditCustomers").hide();
                    $("#txtddlCreditCustomers").val("");
                    $("#Coupans").hide();
                    $("#OP").hide();
                    $("#coupan").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#COD").hide();
                    $("#Online").hide();
                    $("#rdoOnline").hide();
                    $("#lblCashHeading").text("Cash Rec:");
                    $("#lblCashHeading").hide();
                    $("#creditCustomer").css("display", "none");
                    //$("#ddlType").prop("disabled", true);
                    // $("#ddlType").hide();
                    $("#ddlOtherPayment").hide();
                    $("#txtpaymentAmount").hide();
                    $("#rdoCOD").hide();
                    $("#Td1").hide();
                    $("#txtCODAmount").hide();
                    $("#dvOuter_ddlCreditCustomers").hide();
                    // $("#ddlChosseCredit").hide();
                    $("#holder input").hide(); $("#holder2 input").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#dropOtherPayemode").hide();
                    $("#OtherPaymodes").hide();

                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#txtCashReceived").val("0").prop("readonly", true);
                    $("#Text13").val("0").prop("readonly", false);
                    $("#Text14").val("").prop("readonly", false);
                    $("#Text15").val("0").prop("readonly", false);
                    $("#Text16").val("").prop("readonly", false);
                    $("#ddlType").prop("disabled", false);
                    $("#ddlBank").prop("disabled", false);
                    $("#Text13").val($("#txtFinalBillAmount").val());
                    $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());

                    $("#txtCashReceived").hide();
                    // $("#Text13").hide();
                    //  $("#Text14").hide();
                    $("#Text15").hide();
                    $("#Text16").hide();
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                   
                }

                else if ($(this).val() == "Cash") {


                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    $("#txtCashReceived").show();
                    $("#lblCashHeading").show();
                    $("#lblCreaditType").hide();
                    //Hide All Properties//

                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers").val("");
                    $("#ddlCreditCustomers").hide();
                    $("#txtddlCreditCustomers").hide();
                    $("#Coupans").hide();
                    $("#OP").hide();
                    $("#lblCashHeading").text("Cash Rec:");
                    // $("#lblCashHeading").hide();
                    $("#creditCustomer").css("display", "none");
                    $("#coupan").hide();
                    $("#dvOuter_ddlCreditCustomers").hide();
                    $("#ddlChosseCredit").hide();
                    $("#ddlType").prop("disabled", true);
                    $("#ddlType").hide();
                    $("#ddlOtherPayment").hide();
                    $("#COD").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#Online").hide();
                    $("#txtpaymentAmount").hide();
                    $("#rdoCOD").hide();
                    $("#Td1").hide();
                    $("#txtCODAmount").hide();
                    $("#dropOtherPayemode").hide();
                    $("#OtherPaymodes").hide();
                    $("#holder input").hide();
                    $("#holder2 input").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#lblCardAmount").hide();
                    $("#lblCCNO").hide();
                    $("#rdoOnline").hide();
                    $("#lblBank").hide();
                    $("#lblCustomer").hide();
                    $("#CrCardAmt").hide();
                    $("#txtCashReceived").val("0").prop("readonly", false);
                    $("#Text13").val("0").prop("readonly", true);
                    $("#Text14").val("").prop("readonly", true);
                    $("#Text15").val("0").prop("readonly", true);
                    $("#Text16").val("").prop("readonly", true);
                    $("#ddlType").prop("disabled", true);
                    $("#ddlBank").prop("disabled", true);
                    $("#txtCashReceived").val($("#txtFinalBillAmount").val());
                    $("#txtBalanceReturn").val(" ");

                    // $("#txtCashReceived").hide();
                    $("#Text13").val("0").hide();
                    $("#Text14").val("").hide();
                    $("#Text15").val("0").hide();
                    $("#Text16").val("").hide();
                    $("#ddlType").hide();
                    $("#ddlBank").hide();
                    // $("#txtCashReceived").hide();
                    $("#txtBalanceReturn").val(" ");
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#lblBank").hide();
                    $("#lblCustomer").hide();
                   
                }
                else if ($(this).val() == "OnlinePayment") {

                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    //New Changes//
                    $("#ddlOtherPayment").show();
                    $("#txtCODAmount").show();
                    $("#txtCODAmount").val("");
                    $("#ddlOtherPayment").val("");
                    $("#txtpaymentAmount").show();
                    $("#Online").show();
                    $("#OnlineChk").show();
                    $("#codChk").show();
                    $("#COD").show();
                    $("#OP").show();
                    $("#coupan").show();
                    $("#Coupans").show();
                    $("#OnlineChk").prop('checked', false);
                    $("#codChk").prop('checked', false);
                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers").val("");

                    $("#lblCashHeading").text("Cash Rec:");
                    $("#creditCustomer").css("display", "none");
                    $("#coupan").hide();
                    $("#dvOuter_ddlCreditCustomers").hide();
                    $("#ddlChosseCredit").hide();
                    $("#lblCashHeading").hide();
                    $("#txtCashReceived").hide();
                    $("#lblCardAmount").hide();
                    $("#Text13").hide();
                    $("#lblCreaditType").hide();
                    $("#lblCCNO").hide();
                    $("#Text14").hide();
                    $("#ddlBank").hide();
                    $("#ddlType").hide();
                    $("#No").show();
                    $("#Amt").show();
                    $("#CrCardAmt").hide();
                    $("#dropOtherPayemode").show();
                    $("#OtherPaymodes").show();
                    //                            $("#rdoOnline").show();
                    //                               $("#rdoCOD").show();
                    if ($("#txtCODAmount").val() != 0) {
                        onlineoption = "COD"


                    }
                    $("#Coupans").val("0").prop("readonly", true);
                    $("#Td1").show();
                    $("#txtCODAmount").show();
                    $("#holder2").hide();
                    $("#holder").hide();
                    $("#No").hide();
                    $("#Amt").hide();


                    $("#Coupans").show();
                    $("#coupan").show();
                    $("#OP").show();
                    $("#txtCashReceived").val("0").prop("readonly", true);
                    $("#Text13").val("0").prop("readonly", true);
                    $("#Text14").val("").prop("readonly", true);
                    $("#Text15").val("0").prop("readonly", false);
                    $("#Text16").val("").prop("readonly", false);
                    $("#ddlType").prop("disabled", true);
                    $("#ddlBank").prop("disabled", true);
                    $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                    $("#lblBank").hide();
                    $("#lblCustomer").hide();
                  
                }
                DEVBalanceCalculation();

            });

            BindBanks();
            $("#dvShow").click(
        function () {


            GetBillByBNF();

        }
        );


            $("#btnBillWindowOk").click(
        function () {
            var CheckBillType = $("#ddlbillttype").val();
            if (CheckBillType == 'OnlinePayment') {
                if ($("#<%=ddlOtherPayment.ClientID %>").val() == "--Choose Other--") {
                    alert("Please Select Other Paymentmode");
                    return false;
                }
                else {
                    ChangePaymode();
                }
            }
            else {
                ChangePaymode();
            }

        }
            );


            $("#dvOuter_ddlCreditCustomers").hide();
            $("ddlCreditCustomers").hide();
            $("#ddlChosseCredit").hide();
            $("#ddlChosseCredit").val(0);

        }
        );


    </script>
    <script type="text/javascript">
        function CheckMode(Mode) {
            if (Mode == "Credit") {

                //New Changes //

                $("#CrCardAmt").hide();
                $("#txtddlCreditCustomers").hide();
                $("#ddlChosseCredit").show();
                $("#lblCashHeading").show();

                $("#dvOuter_ddlCreditCustomers").show();
                $("#lblCashHeading").text("Receipt Amt:");
                $("#ddlbillttype option[value='Credit']").prop("selected", true);
                $("#txtCashReceived").val("0").prop("readonly", false);
                $("#txtCashReceived").show();
                $("#Text13").val("0").hide();
                $("#Text14").val("").hide();
                $("#Text15").val("0").hide();
                $("#Text16").val("").hide();
                $("#ddlType").hide();
                $("#ddlOtherPayment").hide();
                $("#rdoCOD").hide();
                $("#rdoOnline").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                $("#COD").hide();
                $("#Online").hide();
                $("#txtpaymentAmount").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#Coupans").hide();
                $("#OP").hide();
                $("#coupan").hide();
                $("#holder input").hide();
                $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#lblCardAmount").hide();
                $("#lblCreaditType").hide();
                $("#lblCCNO").hide();
                $("#lblBank").hide();
                $("#dropOtherPayemode").hide();
                $("#OtherPaymodes").hide();
                //$("#ddlBank").prop("disabled", true);
                $("#ddlBank").hide();
                $("#CrCardAmt").hide();
                $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                $("#lblCustomer").show();

            }
            else if (Mode == "CreditCard") {
                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                //New Changes//

                $("#ddlChosseCredit").show();
                $("#ddlType").show();
                $("#Text14").show();
                $("#ddlBank").show();
                $("#lblBank").show();
                $("#lblCardAmount").show();
                $("#lblCreaditType").show();
                $("#lblCCNO").show();
                $("#Text13").show();
                $("#CrCardAmt").show();
                $("#ddlBank").show();
                //Hide All Properties//

                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                $("#lblCreditCustomerName").hide();
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#ddlCreditCustomers").hide();
                $("#txtddlCreditCustomers").hide();
                $("#txtddlCreditCustomers").val("");
                $("#Coupans").hide();
                $("#OP").hide();
                $("#coupan").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#COD").hide();
                $("#Online").hide();
                $("#rdoOnline").hide();
                $("#lblCashHeading").text("Cash Rec:");
                $("#lblCashHeading").hide();
                $("#creditCustomer").css("display", "none");
                //$("#ddlType").prop("disabled", true);
                // $("#ddlType").hide();
                $("#ddlOtherPayment").hide();
                $("#txtpaymentAmount").hide();
                $("#rdoCOD").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                // $("#ddlChosseCredit").hide();
                $("#holder input").hide(); $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#dropOtherPayemode").hide();
                $("#OtherPaymodes").hide();

                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#txtCashReceived").val("0").prop("readonly", true);
                $("#Text13").val("0").prop("readonly", false);
                $("#Text14").val("").prop("readonly", false);
                $("#Text15").val("0").prop("readonly", false);
                $("#Text16").val("").prop("readonly", false);
                $("#ddlType").prop("disabled", false);
                $("#ddlBank").prop("disabled", false);
                $("#Text13").val($("#txtFinalBillAmount").val());
                $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());

                $("#txtCashReceived").hide();
                // $("#Text13").hide();
                //  $("#Text14").hide();
                $("#Text15").hide();
                $("#Text16").hide();
                $("#lblCustomer").hide();
            }

            else if (Mode == "Cash") {


                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#txtCashReceived").show();
                $("#lblCashHeading").show();
                $("#lblCreaditType").hide();
                //Hide All Properties//

                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");
                $("#ddlCreditCustomers").hide();
                $("#txtddlCreditCustomers").hide();
                $("#Coupans").hide();
                $("#OP").hide();
                $("#lblCashHeading").text("Cash Rec:");
                // $("#lblCashHeading").hide();
                $("#creditCustomer").css("display", "none");
                $("#coupan").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                $("#ddlChosseCredit").hide();
                $("#ddlType").prop("disabled", true);
                $("#ddlType").hide();
                $("#ddlOtherPayment").hide();
                $("#COD").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#Online").hide();
                $("#txtpaymentAmount").hide();
                $("#rdoCOD").hide();
                $("#Td1").hide();
                $("#txtCODAmount").hide();
                $("#dropOtherPayemode").hide();
                $("#OtherPaymodes").hide();
                $("#holder input").hide();
                $("#holder2 input").hide();
                $("#No").hide();
                $("#Amt").hide();
                $("#lblCardAmount").hide();
                $("#lblCCNO").hide();
                $("#rdoOnline").hide();
                $("#lblBank").hide();
                $("#CrCardAmt").hide();
                $("#txtCashReceived").val("0").prop("readonly", false);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", true);
                $("#Text16").val("").prop("readonly", true);
                $("#ddlType").prop("disabled", true);
                $("#ddlBank").prop("disabled", true);
                $("#txtCashReceived").val($("#txtFinalBillAmount").val());
                $("#txtBalanceReturn").val(" ");

                // $("#txtCashReceived").hide();
                $("#Text13").val("0").hide();
                $("#Text14").val("").hide();
                $("#Text15").val("0").hide();
                $("#Text16").val("").hide();
                $("#ddlType").hide();
                $("#ddlBank").hide();
                // $("#txtCashReceived").hide();
                $("#txtBalanceReturn").val(" ");

                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#lblCustomer").hide();
            }
            else if (Mode == "OnlinePayment") {

                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                //New Changes//
                $("#ddlOtherPayment").show();
                $("#txtCODAmount").show();
                $("#txtpaymentAmount").show();
                $("#Online").show();
                $("#OnlineChk").show();
                $("#codChk").show();
                $("#COD").show();
                $("#OP").show();
                $("#coupan").show();
                $("#Coupans").show();

                $("#hdnCreditCustomerId").val(0);
                $("#lblCreditCustomerName").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers").html("");
                $("#txtddlCreditCustomers").val("");

                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");
                $("#coupan").hide();
                $("#dvOuter_ddlCreditCustomers").hide();
                $("#ddlChosseCredit").hide();
                $("#lblCashHeading").hide();
                $("#txtCashReceived").hide();
                $("#lblCardAmount").hide();
                $("#Text13").hide();
                $("#lblCreaditType").hide();
                $("#lblCCNO").hide();
                $("#Text14").hide();
                $("#lblBank").hide();
                $("#ddlBank").hide();
                $("#ddlType").hide();
                $("#No").show();
                $("#Amt").show();
                $("#CrCardAmt").hide();
                $("#dropOtherPayemode").show();
                $("#OtherPaymodes").show();
                //                            $("#rdoOnline").show();
                //                               $("#rdoCOD").show();
                if ($("#txtCODAmount").val() != 0) {
                    onlineoption = "COD"


                }
                $("#Coupans").val("0").prop("readonly", true);
                $("#Td1").show();
                $("#txtCODAmount").show();
                $("#holder2").hide();
                $("#holder").hide();
                $("#No").hide();
                $("#Amt").hide();


                $("#Coupans").show();
                $("#coupan").show();
                $("#OP").show();
                $("#txtCashReceived").val("0").prop("readonly", true);
                $("#Text13").val("0").prop("readonly", true);
                $("#Text14").val("").prop("readonly", true);
                $("#Text15").val("0").prop("readonly", false);
                $("#Text16").val("").prop("readonly", false);
                $("#ddlType").prop("disabled", true);
                $("#ddlBank").prop("disabled", true);
                $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                $("#lblCustomer").hide();

            }
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#OnlineChk").change(function () {
                if ($("#OnlineChk").prop('checked')) {
                    var FinalAmount = $("#txtFinalBillAmount").val();
                    $("#txtpaymentAmount").attr('readonly', 'readonly');
                    $("#txtCODAmount").attr('readonly', 'readonly');
                    $("#txtBalanceReturn").val(0)
                    $("#txtpaymentAmount").val(FinalAmount);
                    $("#txtCODAmount").val(0);
                    $("#codChk").prop('checked', false);
                }
            })
            $("#codChk").change(function () {
                if ($("#codChk").prop('checked')) {
                    var FinalAmount = $("#txtFinalBillAmount").val();
                    $("#txtpaymentAmount").attr('readonly', 'readonly');
                    $("#txtCODAmount").attr('readonly', 'readonly');
                    $("#txtpaymentAmount").val(0);
                    $("#txtBalanceReturn").val(0)
                    $("#txtCODAmount").val(FinalAmount);
                    $("#OnlineChk").prop('checked', false);
                }
            })
        })
    </script>
    <form runat="server" id="formID" method="post">
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <input type="hidden" id="hdnCreditCustomerId" value="0" />
        <div class="right_col change-paymode-right-col" role="main">


            <div class="">

                <!--  <div class="page-title">
                    <div class="title_left">
                        <h3>Change Paymode</h3>
                    </div>
                  <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">Go!</button>
                                </span>
                            </div>
                        </div>
                    </div> -->
                </div>
                <div class="clearfix"></div>


                <div class="x_panel paymode_panel">
                    <div class="x_title">
                        <h2>Change Paymode</h2>

                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td align="center" width="100%" colspan="100%">
                                        <div class="bill_number">
                                           <table>
                                                <tbody>
                                                    <tr>
                                                        <td>Bill Number:</td>
                                                        <td><input type="text" id="txtBillNumber" class="form-control input-small"></td>
                                                        <td><div id="dvShow" class="btn btn-primary btn-small">Show</div></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                                        <div class="cate paymode_cate">

                                                        <table class="table_one">
                                                            <tr>
                                                                <%-- <td>Bill Type:
                                                                </td>--%>
                                                                <td>
                                                                    <select id="ddlbillttype" class="form-control" multiple="multiple">
                                                                        <option value="Cash">Cash</option>
                                                                        <option value="Credit">Credit</option>
                                                                        <option value="CreditCard">Credit Card</option>
                                                                        <option value="OnlinePayment">Online Payment</option>
                                                                    </select>
                                                                </td>
                                                                <td>
                                                                    <%--<select id="ddlCreditCustomers" class="form-control" style="display: inline-block; float: left; font-size: 11px; min-width: 0; padding: 0; width: 166px;">
                                                                        <option value="0"></option>
                                                                    </select>--%>
                                                                    <span id="lblCustomer" style="display:none;">Customer Name</span>
                                                                </td>
                                                                <td><asp:DropDownList ID="ddlChosseCredit" ClientIDMode="Static" runat="server" class="form-control"></asp:DropDownList></td>
                                                            </tr>
                                                        </table>
                                            <table  class="table_two">
                                                <tr>
                                                    <td class="table-two-td">Amount:</td>
                                                    <td colspan="100%">
                                                        <table>
                                                            <tr>
                                                                <td><input type="text" class="form-control input-small" id="txtFinalBillAmount" disabled="disabled" /></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td id="OtherPaymodes" class="table-two-td"><span>Other Paymentmode</span></td>
                                                    <td id="dropOtherPayemode"><asp:DropDownList ID="ddlOtherPayment" class="form-control" runat="server"></asp:DropDownList></td>
                                                </tr>

                                                <tr>
                                                    <td class="table-two-td"><input type="checkbox" disabled="disabled" id="codChk" /> <label class="headings" id="COD" for="rdoCOD">COD</label></td>
                                                     <td><input class="form-control input-small" disabled="disabled" id="txtCODAmount" type="text" value="0" /></td>
                                                </tr>

                                                <tr>      
                                                    <td class="table-two-td">
                                                        <input type="checkbox" id="OnlineChk"/>
                                                        <label class="headings" id="Online" for="rdoOnline">Online</label>
                                                    </td>
                                                    <td><input type="text" class="form-control input-small" value="0" id="txtpaymentAmount" /></td>
                                                </tr>

                                                 <tr>
                                                    <td class="table-two-td"><label id="lblCashHeading">Cash Rec:</label></td>
                                                     <td colspan="100%">
                                                         <table>
                                                           <tr>
                                                              <td><input type="text" class="form-control input-small" value="0" id="txtCashReceived" /></td>
                                                              <td>
                                                                <table width="100%" id="creditCustomer" style="padding: 0px 5px 0px 5px; background: #2a3f54; color: white; display: none; border: dashed 1px silver; border-collapse: separate; border-spacing: 1"
                                                                   cellpadding="2">
                                                                 <tr><td><label id="lblCreditCustomerName"></label></td></tr>
                                                               </table>
                                                             </td>
                                                           </tr>
                                                        </table>
                                                     </td>
                                                 </tr>

                                                 <tr>
                                                     <td id="CrCardAmt" class="table-two-td">Cr.Card Amount</td>
                                                     <td colspan="100%">  
                                                         <table>
                                                             <tr>
                                                                 <td><input type="text" class="form-control input-small" value="0"id="Text13" /></td>
                                                            </tr>
                                                         </table>
                                                     </td>
                                                 </tr>

                                                <tr>
                                                    <td class="table-two-td"><span id="lblBank">Bank:</span></td>
                                                    <td><select id="ddlBank" class="form-control"></select>
                                                        <input type="text" class="form-control input-small" style="width: 70px; display: none" id="Text15" value="0" />
                                                    </td>
                                                </tr>

                                                <tr style="display: none">
                                                    <td class="table-two-td">Cheque No:
                                                    </td>
                                                    <td colspan="100%">
                                                        <input type="text" class="form-control input-small" id="Text16" />
                                                    </td>
                                                </tr>

                                                 <tr>
                                                    <td class="table-two-td">Balance:</td>
                                                    <td><input type="text" class="form-control input-small" id="txtBalanceReturn"readonly="readonly" /></td>
                                                 </tr>
            
                                           </table>

                                            <div class="paymode_btns">
                                                 <div id="btnBillWindowOk" class="btn btn-primary btn-small">Change Paymode</div>
                                                 <div id="btnBillWindowClose" class="btn btn-primary btn-small">Close</div>
                                           </div>
                                        </div>
         
                          

                    </div>

                </div>

            </div>





        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
        <!-- /footer content -->

        </div>



    </form>

    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>

    <script type="text/javascript" src="js/jquery.uilock.js"></script>

    <script type="text/javascript" src="js/SearchPlugin.js"></script>









</asp:Content>

