﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageitemsequence.aspx.cs" Inherits="manageitemsequence" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="css/customcss/setup.css" rel="stylesheet" />
  <style>
    tbody {
      display: block;
      max-height: 700px;
      overflow: auto;
    }

      thead, tbody tr {
        display: table;
        width: 100%;
        table-layout: fixed; /* even columns width , fix width of table too*/
      }

    thead {
      width: calc( 100% - 1em ); /* scrollbar is average 1em/16px width, remove it from thead width */
    }

    table {
      width: 400px;
    }
    .setup_title
    {
        background :#ffffff;
        color:#73879C;
    }
    .manage-itm-seq-x-panel
    {
        min-height:484px;
        max-height:484px;
        overflow:auto;
    }
    .frm-cntrl-manage-item-seque{
        padding:3px 2px;
    }
    @media(max-width:767px)
    {
        .x_panel.manage-itm-seq-x-panel select {
	        width: 200px !important;
        }
		#dvProducts input[type="text"] {
			width: 50px !important;
		}
    }
    @media(min-width:992px) and (max-width:1200px)
    {
        .manage-itm-seq-x-panel {
	        min-height: 405px;
	        max-height: 405px;
	        overflow: auto;
        }
    }
  </style>
  <form runat="server" id="formID" method="post">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
        <asp:UpdateProgress ID="updateProgress" runat="server">
          <ProgressTemplate>
            <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
              <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
            </div>
          </ProgressTemplate>
        </asp:UpdateProgress>
        <asp:HiddenField ID="hdnRoles" runat="server" />

        <div class="right_col" role="main">
          <div class="">

            <div class="page-title">
              <div class="title_left">
                <h3>Manage Group Sequence</h3>
              </div>
              <%--          <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>--%>
            </div>
            <div class="clearfix"></div>
              <div class="x_title setup_title">
                <h2>Add/Edit Group Sequence</h2>

                <div class="clearfix"></div>
              </div>
            <div class="x_panel manage-itm-seq-x-panel">
              
              <asp:DropDownList ID="dd_pos" runat="server" CssClass="form-control frm-cntrl-manage-item-seque" AutoPostBack="true" Style="height: 25px; width: 20%;" OnSelectedIndexChanged="dd_pos_SelectedIndexChanged"></asp:DropDownList>

              <div class="x_content">



                <table class="table">
                  <tr>
                    <th>Departments Id</th>
                    <th>Departments Name</th>
                    <th>Sequence</th>

                    <asp:Button ID="btnsave" class="btn btn-primary btn-small" runat="server" Text="Submit" OnClick="btnsave_Click" Visible="false" />
                  </tr>
                  <tbody id="dvProducts">
                    <asp:Repeater ID="rpt_itemlist" runat="server">
                      <ItemTemplate>
                        <tr>
                          <td>
                            <asp:Label ID="lblproductid" runat="server" Text='<%#Eval("PROP_ID") %>'></asp:Label></td>
                          <td><%#Eval("PROP_NAME") %></td>
                          <td>
                            <input type="text" runat="server" class="form-control" style="width: 20%;" id="tblvl" value='<%#Eval("lvl") %>' /></td>

                        </tr>
                      </ItemTemplate>
                    </asp:Repeater>
                  </tbody>

                </table>


              </div>

            </div>



          </div>
          <!-- /page content -->

         

        </div>
           <!-- footer content -->
               <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
          <!-- /footer content -->

      </ContentTemplate>
    </asp:UpdatePanel>
  </form>
</asp:Content>

