﻿<%@ Page Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="BillingSms.aspx.cs" Inherits="BillingSms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
  <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
  <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
  <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
  <script src="js/grid.locale-en.js" type="text/javascript"></script>
  <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
  <script src="js/jquery-ui.js"></script>
  <link href="css/tabcontent.css" rel="stylesheet" type="text/css" />
  <script src="js/customValidation.js" type="text/javascript"></script>
  <script type="text/javascript" src="js/jquery.uilock.js"></script>
  <link href="semantic.css" rel="stylesheet" type="text/css" />
  <form action="#" runat="server" id="formID" method="post">
    <div class="right_col" role="main">
      <h2 style="color: white">Message Settings</h2>
      <div>
        <table width="100%" style="border: solid 1px silver; background-color: white">
          <tr>
            <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">KEYWORDS FOR MESSAGE</td>
          </tr>
          <tr>
              <td style="width: 300px">For Customer Name With Prefix:</td>
              <td>
                <asp:Label ID="lblname" runat="server" Text="$CN$"></asp:Label></td>
            </tr>
            <tr>
              <td style="width: 300px">For Employee Name:</td>
              <td>
                <asp:Label ID="Label1" runat="server" Text="$EN$"></asp:Label></td>
            </tr>
            <%-- <tr><td style="width:300px">For Ser/Pkg/MEM:</td><td><asp:Label ID = "Label1" runat="server" Text ="$SE$"></asp:Label></td></tr>--%>
            <tr>
              <td style="width: 300px">For Bill Amount:</td>
              <td>
                <asp:Label ID="Label2" runat="server" Text="$AM$"></asp:Label></td>
            </tr>
            <tr>
              <td style="width: 300px">For Date:</td>
              <td>
                <asp:Label ID="Label3" runat="server" Text="$DT$"></asp:Label></td>
            </tr>
          <tr>
              <td style="width: 300px">For Phone NO:</td>
              <td>
                <asp:Label ID="Label4" runat="server" Text="$PN$"></asp:Label></td>
            </tr>
        
        </table>
        <table width="100%" style="border: solid 1px silver; background-color: white">

          <tr>
            <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">ORDER MESSAGE SETTINGS</td>
          </tr>
          <tr>
            <td style="width: 100px">Message:</td>
            <td>
              <asp:TextBox runat="server" Width="100%" ID="txtOrder"
                TextMode="MultiLine"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Is Active</td>
            <td>

              <asp:CheckBox ID="chkOrder" runat="server" /></td>
          </tr>

          <tr>
            <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">ORDER BILL MESSAGE SETTINGS</td>
          </tr>
          <tr>
            <td style="width: 100px">Message:</td>
            <td>
              <asp:TextBox runat="server" Width="100%" ID="txtOrderBill"
                TextMode="MultiLine"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Is Active</td>
            <td>

              <asp:CheckBox ID="chkOrderBill" runat="server" /></td>
          </tr>

          <tr>
            <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">ORDER DESPATCHED MESSAGE SETTINGS</td>
          </tr>
          <tr>
            <td style="width: 100px">Message:</td>
            <td>
              <asp:TextBox runat="server" Width="100%" ID="txtOrderDespatched"
                TextMode="MultiLine"></asp:TextBox></td>
          </tr>
          <tr>
            <td>Is Active</td>
            <td>

              <asp:CheckBox ID="chkOrderDespatched" runat="server" /></td>
          </tr>

          <%-- <tr>
                                  <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">SIMPLE SERVICE MESSAGE SETTINGS</td>
                                </tr>--%>
          <%-- <tr>
                    <td style="width: 100px">Message:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtSmSerMsg"
                        TextMode="MultiLine"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Is Active</td>
                    <td>

                      <asp:CheckBox ID="chkSmSerActive" runat="server" /></td>
                  </tr>--%>

          <%-- <tr>
                                  <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">MEMBER SERVICE MESSAGE SETTINGS</td>
                                </tr>--%>
          <%--  <tr>
                    <td style="width: 100px">Message:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtMemSerMsg"
                        TextMode="MultiLine"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Is Active</td>
                    <td>

                      <asp:CheckBox ID="chkMemSerActive" runat="server" /></td>
                  </tr>--%>

          <%--<tr>
                                  <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">PACKAGE SERVICE MESSAGE SETTINGS</td>
                                </tr>--%>
          <%--<tr>
                    <td style="width: 100px">Message:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtPkgSerMsg"
                        TextMode="MultiLine"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Is Active</td>
                    <td>

                      <asp:CheckBox ID="chkPkgSerActive" runat="server" /></td>
                  </tr>--%>


          <%--<tr>
                                  <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">PAYMENT RECEIPT MESSAGE SETTINGS</td>
                                </tr>--%>
          <%--<tr>
                    <td style="width: 100px">Message:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtPayRecMsg"
                        TextMode="MultiLine"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Is Active</td>
                    <td>

                      <asp:CheckBox ID="chkPayRecActive" runat="server" /></td>
                  </tr>--%>





          <%--<tr>
                    <td style="background-color: Black; font-weight: bold; text-align: center; color: White" colspan="100%">APPOINTMENT REMINDER MESSAGE SETTINGS</td>
                  </tr>--%>
          <%--<tr>
                    <td style="width: 100px">Message:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtAppRem"
                        TextMode="MultiLine"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Time:</td>
                    <td>
                      <asp:TextBox runat="server" Width="550px" ID="txtTime"></asp:TextBox></td>
                  </tr>
                  <tr>
                    <td>Is Active</td>
                    <td>

                      <asp:CheckBox ID="chkAppRemActive" runat="server" /></td>
                  </tr>--%>
            <tr>
              <td>
          <asp:Button ID="btnSubmitChanges"
            runat="server" CssClass="btn btn-primary" Text="Save Settings"  style="margin-top:20px;"
            OnClick="btnSubmitChanges_Click" /></td>

        </tr>
        </table>
      
      </div>
    </div>
  </form>

</asp:Content>
