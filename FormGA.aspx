﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="FormGA.aspx.cs" Inherits="FormGA" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
	<link href="css/customcss/formga.css" rel="stylesheet" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script>

        $(document).ready(function () {

            $("#btngetdata").click(function () {
                var date = $("#txtfrmdate").val();
            $.ajax({
                type: "POST",
                data: '{"date": "' + date + '"}',
                url: "FormGa.aspx/Getdata",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                	if (obj.option.length>0) {
                    	$("#tbl_group").show();

                    }
                    else {
                		$("#tbl_group").hide();
						alert("No Record(s) Found!")
                    }
                    $("#tblgroup").html(obj.option);
                    var add_total_actual = 0;
                    $(".cls_actual").each(function () {
                        var val = 0;
                        val = $(this).val();
                        add_total_actual = Number(add_total_actual) + Number(val);

                    })

                    $("#total_actual").text(add_total_actual);
                    add_total = 0;
                    add_total_actual = 0;
                    add_total_less = 0;
                    $("#total_less").text(0);
                    $("#total_add").text(0);
                  

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            });

            })
            var add_total = 0;
            $(document.body).on('keyup', '.cls_add', function (e) {
       
                $(".cls_less").attr('disabled', 'disabled');
     
                $(".cls_add").each(function () {
                    var val = 0;
                    val = $(this).val();
                    add_total = Number(add_total) + Number(val);

                })
          
                $("#total_add").text(add_total);
                add_total = 0;
            })
            var add_total_less = 0;
            $(document.body).on('keyup', '.cls_less', function (e) {

                $(".cls_add").attr('disabled', 'disabled');

                $(".cls_less").each(function () {
                    var val = 0;
                    val = $(this).val();
                    add_total_less = Number(add_total_less) + Number(val);

                })

                $("#total_less").text(add_total_less);
                add_total_less = 0;
            })
            $("#btnreset").click(function () {

                $("#btngetdata").click();
                Reset();
        
            });
            function Reset() {
            	add_total = 0;
            	add_total_actual = 0;
            	add_total_less = 0;
            	$("#total_less").text(0);
            	$("#total_add").text(0);
            	$("#total_actual").text(0);

            }
        });
    </script>
	  <div class="right_col" role="main">
		  <div class="">
				<div class="page-title">
					<div class="title_left">
						<h3>Groupwise Alteration</h3>
					</div>
              
				</div>
				<div class="clearfix">
				</div>
			</div>
		   <div class="col-md-12 col-sm-12 col-xs-12">
			 <div class="x_panel">
				   <div class="x_title">
						<h2>Add/Edit Groupwise Alteration</h2>
						<div class="clearfix">
						</div>
					</div>
					<div class="x_content">
                           <div class="frm-GA-date-section">
                        <input type="date" class="form-control" id="txtfrmdate"/>  <button type="button" id="btngetdata"  class="btn btn-danger">Search</button>
                               </div>
                       <!--  <div class="col-md-4 col-sm-4 col-xs-4">
                       
                               </div>-->
						<table class="table form-ga-table-section"  id="tbl_group" style="display:none">
							<thead>
								<th>Groups</th>
								<th>Actual Sale</th>
								<th>Add</th>
								<th>+/-</th>
							</thead>
                            <tbody id="tblgroup">

                            </tbody>
						<tr class="total-tr-ga-frm"><td><label>Total:</label></td><td><label id="total_actual"></label></td><td><label id="total_add">0</label></td><td style="display:none"><label id="total_less">0</label></td></tr>
							<tr>
								<td colspan="4" class="submit-btn-formga"><button class="btn btn-primary btn-small">Submit</button><button type="button" id="btnreset" class="btn btn-danger btn-small">Reset</button></td>
                                <!--<td colspan="4" class="submit-btn-formga"></td>-->
							</tr>
						</table>
					</div>
			  </div>

		 </div>
	  </div>
	

</asp:Content>

