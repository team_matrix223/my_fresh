﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="placeordernew.aspx.cs" Inherits="placeordernew" %>
<%@ Register src="Templates/OrderTemplates/newOrderBooking.ascx" tagname="OrderBooking" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
	<link href="css/customcss/placeordernew.css" rel="stylesheet" />
	<div class="customer-info-div">
		<div class="form-heading">
			<h2>CUSTOMER INFO</h2>
		</div>
		<form>
			<div class="customer-form">
				<div class="customer-label">Search</div>
				<div class="customer-field">
					<input type="text" maxlength="50" placeholder="Enter Name/Phone" class="form-control">
				</div>
			</div>
			<div class="customer-form">
				<div class="customer-label">Name</div>
				<div class="customer-field">
					<input type="text" class="form-control" readonly="readonly" />
				</div>
			</div>
			<div class="customer-form">
				<div class="customer-label">Customer Address</div>
				<div class="customer-field">
					<textarea readonly="readonly"class="form-control">                                             
                                                                </textarea>
				</div>
			</div>
		</form>
		</div>
	<div class="customer-info-div">
		<div class="form-heading">
			<h2>DELIVERY INFO</h2>
		</div>
		<form>
			<div class="customer-form">
				<div class="customer-label">Delivery Mode</div>
				<div class="customer-field">
					<select class="form-control">
						<option value="">Choose..</option>
						<option value="ByHand">By Hand</option>
						<option value="ByDelivery">By Delivery</option>
                    </select>
				</div>
			</div>
			<div class="customer-form">
				<div class="customer-label">Delivery Date</div>
				<div class="customer-field">
					<input type="text" class="form-control" >
				</div>
			</div>
			<div class="customer-form">
				<div class="customer-label">Delivery Time</div>
				<div class="customer-field">
					<select class="form-control">
						<option value="">Choose..</option>
						<option value="09:00">09:00</option>
						<option value="09:30">09:30</option>
						<option value="10:00">10:00</option>
						<option value="10:30">10:30</option>
						<option value="11:00">11:00</option>
						<option value="11:30">11:30</option>
						<option value="12:00">12:00</option>
						<option value="12:30">12:30</option>
						<option value="13:00">13:00</option>
						<option value="13:30">13:30</option>
						<option value="14:00">14:00</option>
						<option value="14:30">14:30</option>
						<option value="15:00">15:00</option>
						<option value="15:30">15:30</option>
						<option value="16:00">16:00</option>
						<option value="16:30">16:30</option>
						<option value="17:00">17:00</option>
						<option value="17:30">17:30</option>
						<option value="18:00">18:00</option>
						<option value="18:30">18:30</option>
						<option value="19:00">19:00</option>
						<option value="19:30">19:30</option>
						<option value="20:00">20:00</option>
						<option value="20:30">20:30</option>
						<option value="21:00">21:00</option>
						<option value="21:30">21:30</option>
						<option value="22:00">22:00</option>
						<option value="22:30">22:30</option>

					</select>
				</div>
			</div>
			<div class="customer-form">
				<div class="customer-label">Venue</div>
				<div class="customer-field">
					<textarea class="form-control"></textarea>
				</div>
			</div>
		</form>
		</div>
</asp:Content>

