﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Globalization;

using Microsoft.Reporting.WebForms;
using System.Collections;
using System.Data.OleDb;
using System.Web.Security;
using System.Drawing;
using System.Drawing.Printing;

//using Aspose.Pdf.Facades;
using Spire.Pdf;
using System.Xml;



public partial class BillScreen : System.Web.UI.Page
{
   

    public string m_getdate { get; set; }
    mst_customer_rate msr = new mst_customer_rate();
    Connection con = new Connection();
   
    public string RouteFrom { get { return Request.QueryString["RouteFrom"] != null ? Convert.ToString(Request.QueryString["RouteFrom"]) : string.Empty; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        string Touch = "0";
        clearcombodetail();
        Int32 Screen = 0;
        //Response.Cookies[Constants.posid].Value = "2";
        Int32 pos = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        if (RouteFrom == "Kot")
        {
            Response.Cookies[Constants.posid].Value = "2";
            hdnroute.Value = "Kot";

        }
        else
        {
            Response.Cookies[Constants.posid].Value = pos.ToString();
            hdnroute.Value = "";
        }
        try
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies[Constants.Touch].Value))
            {

                Touch = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.Touch].Value);
            }
            else
            {

                Touch = "0";
            }
        }
        catch { Touch = "0"; }
       

        if (pos == 20)
        {
            if (Touch == "1")
            {
                Response.Redirect("BillScreenSlip.aspx");
                // Response.Redirect("TouchBilling.aspx");
            }
            else
            {
                Response.Redirect("BillScreenOption.aspx");
            }
        }
        try
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies[Constants.ScreenNo].Value))
            {

                Screen = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.ScreenNo].Value);
            }
            else
            {
                Connection con1 = new Connection();
                SqlConnection con = new SqlConnection(con1.sqlDataString);
                SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + pos + "", con);
                DataTable dt = new DataTable();
                dt.Clear();
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                if (dt.Rows.Count > 0)
                {
                    Screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());


                    Response.Cookies[Constants.ScreenNo].Value = Screen.ToString();
                }
                else
                {
                    Response.Write("<script>alert('PosId doesn't Exists')</script>");
                    return;
                }
                


            }
        }
        catch
        {
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + pos + "", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());


                Response.Cookies[Constants.ScreenNo].Value = Screen.ToString();
            }
            else
            {
                Response.Write("<script>alert('PosId doesn't Exists')</script>");
                return;
            }
        }

        if (Screen == 2)
        {
            Response.Redirect("managekotscreen.aspx");
        }
       
        //testtest

        if (!IsPostBack)
        {
           // lblpos.Text = pos.ToString();
            hdnScreen.Value = Screen.ToString();
           // hdnScreen.Value = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.ScreenNo].Value);
            hdnDate.Value = DateTime.Now.ToShortDateString();
            ltDateTime.Text = "<span style='font-weight:bold;'>Today - " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now) + "</span>";
            m_getdate = DateTime.Now.ToString("ddddd,MMMM-dd-yyyy");
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();

            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

            //else {
            //    Process.Start("osk");
            //}
            BindCreditCustomers();
            BindOtherPayment();
            ddcustomertype();
            BindOtherPayment_pm();
            GetTodayData();
            BindDropDownList();

        }

        CheckRole();
    }

    public void GetTodayData()
    
    {
        Int32 desiid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value);
        hdndesg.Value = desiid.ToString();
        
        if (desiid == 1)
        {
            LblTodayCash.Visible = true;
            LblTodayTotalSale.Visible = true;
            LblTodayOnlineSale.Visible = true;
            LblTodayCrCardSale.Visible = true;
            lblTodayGST.Visible = true;
            lblTodayCredit.Visible = true;
            lbltotl.Visible = true;
            lblcash.Visible = true;
            lblcrcrd.Visible = true;
            lblonline.Visible = true;
            lblCredit.Visible = true;
            lblGST.Visible = true;
            Connection con = new Connection();
            using (SqlConnection conn = new SqlConnection(con.sqlDataString))
            {
                Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
                Int32 PosId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.posid].Value);
                conn.Open();
                SqlCommand cmd = new SqlCommand("strp_Billtiles", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BranchId", Branch);
                cmd.Parameters.AddWithValue("@pos_id", PosId);
                SqlDataReader rd = cmd.ExecuteReader();
                if (rd.Read())
                {
                    LblTodayCash.Text = rd["TodayCashSale"].ToString();
                    LblTodayTotalSale.Text = rd["TodayTotalSale"].ToString();
                     lblTodayCredit.Text = rd["TodayCreditSale"].ToString();
                    LblTodayOnlineSale.Text = rd["TodayOnlineSale"].ToString();
                    //LblTodayPendingSettelment.Text = rd["TodayPendingSettelment"].ToString();
                    //LblTodayOpenTables.Text = rd["TodayOpenTables"].ToString();
                    //LblTodayOpenKot.Text = rd["TodayOpenTables"].ToString();
                    LblTodayCrCardSale.Text = rd["TodayCrCardSale"].ToString();
                    lblTodayGST.Text = rd["GSTSale"].ToString();

                }



            }
        }
        else
        {
            LblTodayCash.Visible = false;
            LblTodayTotalSale.Visible = false;
            LblTodayOnlineSale.Visible = false;
            LblTodayCrCardSale.Visible = false;
            lblTodayCredit.Visible = false;
            lblTodayGST.Visible = false;
            lbltotl.Visible = false;
            lblcash.Visible = false;
            lblcrcrd.Visible = false;
            lblonline.Visible = false;
            lblCredit.Visible = false;
            lblGST.Visible = false;

        }
    }
    public void clearcombodetail()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("delete from combo_detail where billnowprefix is null ", con);
        cmd.CommandType = CommandType.Text;
        cmd.ExecuteNonQuery();
        con.Close();

    }


    public void BindDropDownList()
    {

        DataSet ds = new CommonMasterDAL().GetAll();

        cm_ddlCities.DataSource = ds.Tables[0];
        cm_ddlCities.DataValueField = "CIty_ID";
        cm_ddlCities.DataTextField = "City_Name";
        cm_ddlCities.DataBind();

        cm_ddlArea.DataSource = ds.Tables[1];
        cm_ddlArea.DataTextField = "Area_Name";
        cm_ddlArea.DataValueField = "Area_ID";
        cm_ddlArea.DataBind();


        cm_ddlState.DataSource = ds.Tables[2];
        cm_ddlState.DataTextField = "State_Name";
        cm_ddlState.DataValueField = "STATE_ID";
        cm_ddlState.DataBind();



        cm_ddlPrefix.DataSource = ds.Tables[3];
        cm_ddlPrefix.DataTextField = "PROP_NAME";
        cm_ddlPrefix.DataValueField = "PROP_NAME";
        cm_ddlPrefix.DataBind();


    }

public void ddcustomertype()
    {
        msr.req = "bind_ddcustomer";
        DataTable dt = msr.bind_item_dd();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "customer_name";
        dd_customername.DataValueField = "cst_id";
        dd_customername.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Shop", "0");
        dd_customername.Items.Insert(0, listItem1);
        dd_customername1.Items.Insert(0, listItem1);


    }
    public void OnSelectedIndexChanged(EventArgs arg)
    {

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString() | m == Convert.ToInt16(Enums.Roles.RETURN).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    [WebMethod]
    public static string ChkUserLogin()
    {
        string rtnval = "1";
        try
        {
            Connection connn = new Connection();
            SqlConnection con = new SqlConnection(connn.sqlDataString);
            con.Open();
            SqlCommand cmd = new SqlCommand("Select 1 from userinfo where IsLogin='false' and User_ID=@Username", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@Username", HttpContext.Current.Request.Cookies[Constants.EmployeeName].Value);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);

            if (dt.Rows.Count == 1)
            {

                rtnval = "0";
            }
            con.Close();


        }
        catch
        {


        }
        return rtnval;
    }

    
    [WebMethod]
    public static string GeneratePDF(string BillNowPrefix,string sbc,string kkc)
    {
        string rtnval = "1";
       
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            bool Barcode = false;
            bool PrintSign = false;
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select BarcodePrintOnBill,Print_Sign from mastersetting_Basic ", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                Barcode = Convert.ToBoolean(dt.Rows[0]["BarcodePrintOnBill"].ToString());
                PrintSign = Convert.ToBoolean(dt.Rows[0]["Print_Sign"].ToString());
            }
        string flpath = "";
            using (MemoryStream ms = new MemoryStream())
            {

                // rptBarcode r = new rptBarcode(ItemId, Qty, Date, BestExp, Day, wt);

                ARetailBill r = new ARetailBill(BillNowPrefix, Barcode, PrintSign);
                SqlCommand _Cmd = null;
                _Cmd = new SqlCommand();
                _Cmd.Connection = con;
                _Cmd.CommandText = "Report_sp_ExciseChallanReportSubBillReport";
                _Cmd.Parameters.AddWithValue("@BranchId", Convert.ToInt32(Branch));
                _Cmd.Parameters.AddWithValue("@BillNowPrefix", BillNowPrefix);
                _Cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adpex = new SqlDataAdapter(_Cmd);
                DataSet dse = new DataSet();
                adpex.Fill(dse);
                var subReport = r.FindControl("xrSubreport1", true) as DevExpress.XtraReports.UI.XRSubreport;
                if (subReport != null)
                {
                    subReport.ReportSource.DataSource = dse;
                    //subReport.ReportSource.DataMember = _DataSet.Tables[1].TableName; //Here

                    //  subReport.ReportSource.Parameters.Add();
                    subReport.ReportSource.CreateDocument(true);
                }

                r.PrintingSystem.ShowMarginsWarning = true;

                r.CreateDocument();

                string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
                string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");



                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);
                }

                //if (!Directory.Exists(@"E:/pdf/"+dbname+""))
                //{
                //    Directory.CreateDirectory(@"E:/pdf/" + dbname + "");
                //}
                // SavePDF(r, "C:/inifdpdf/" + BillNowPrefix + ".pdf", "data");
                DevExpress.XtraPrinting.PdfExportOptions opts = new DevExpress.XtraPrinting.PdfExportOptions();
                opts.ShowPrintDialogOnOpen = true;
                // r.ExportToPdf(@"F:/pdf/" + dbname + "/" + BillNowPrefix + ".pdf");
                if ((System.IO.File.Exists(filepath + "/" + BillNowPrefix + ".pdf")))
                {
                    System.IO.File.Delete(filepath + "/" + BillNowPrefix + ".pdf");
                    r.ExportToPdf(filepath + "/" + BillNowPrefix + ".pdf");

                }
                else
                {
                    r.ExportToPdf(filepath + "/" + BillNowPrefix + ".pdf");

                flpath = filepath;
            }


            }



        
        return rtnval;
    }


    [WebMethod]
    public static string BillPrint(string BillNowPrefix)
    {
        

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 noofcopy = 1;
        string rtnval = "1";
        Connection con1 = new Connection();
        SqlConnection con = new SqlConnection(con1.sqlDataString);
        SqlCommand cmd = new SqlCommand("select pc.no_of_copy from mst_printcopies as pc inner join bill_master as bm on pc.bill_type=bm.billmode where pc.branchid="+ Branch + " and bm.billnowprefix='"+BillNowPrefix+"'", con);
        DataTable dt = new DataTable();
        dt.Clear();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count > 0)
        {
           noofcopy =  Convert.ToInt32(dt.Rows[0]["no_of_copy"].ToString());
        } 

            string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
            string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");


        if (Directory.Exists(filepath))
        {



            for (int i = 1; i <= Convert.ToInt32(noofcopy); i++)
            {
                PdfDocument doc = new PdfDocument();
                doc.LoadFromFile(filepath + "/" + BillNowPrefix + ".pdf");
                doc.Print();




            }
        }




        return rtnval;
    }

    [WebMethod]
    public static string DeletePdf(string BillNowPrefix)
    {
        string rtnval = "1";
        try
        {
            string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
            string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");

            if (Directory.Exists(filepath))
            {
                if ((System.IO.File.Exists(filepath + "/" + BillNowPrefix + ".pdf")))
                {
                    System.IO.File.Delete(filepath + "/" + BillNowPrefix + ".pdf");
                }
                
            }


        }
        catch
        {


        }
        return rtnval;
    }


    [WebMethod]
    public static string GenerateCoupon(string BillNowPrefix)
    {
        string rtnval = "1";
       
            Connection con1 = new Connection();
            SqlConnection con = new SqlConnection(con1.sqlDataString);
            SqlCommand cmd = new SqlCommand("select distinct Prop_name as DepartmentName,Print_Com   from dbo.Bill_detail BD inner join packing_belongs P on BD.Item_Code = P.Item_Code inner join departments D on D.Prop_id = P.Department inner join settingexprinting S on S.Dep_Name = D.prop_Name where P.CPPRINTING = 'True' AND  BillNowPrefix = '" + BillNowPrefix + "'", con);
            DataTable dt = new DataTable();
            dt.Clear();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                    string PrinterName = dt.Rows[i]["Print_Com"].ToString();
                    string Department = dt.Rows[i]["DepartmentName"].ToString();

                        // rptBarcode r = new rptBarcode(ItemId, Qty, Date, BestExp, Day, wt);

                        rptCoupon r = new rptCoupon(BillNowPrefix,Department );

                        r.PrintingSystem.ShowMarginsWarning = true;

                        r.CreateDocument();

                        string dbname = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.DataBase].Value);
                        string filepath = System.Web.HttpContext.Current.Server.MapPath("~/PdFFile/" + dbname + "");



                        if (!Directory.Exists(filepath))
                        {
                            Directory.CreateDirectory(filepath);
                        }


                        DevExpress.XtraPrinting.PdfExportOptions opts = new DevExpress.XtraPrinting.PdfExportOptions();
                        opts.ShowPrintDialogOnOpen = true;
                        // r.ExportToPdf(@"F:/pdf/" + dbname + "/" + BillNowPrefix + ".pdf");

                        r.ExportToPdf(filepath + "/" + BillNowPrefix+','+Department + ".pdf");

                        PdfDocument doc = new PdfDocument();
                        doc.LoadFromFile(filepath + "/" + BillNowPrefix+',' + Department + ".pdf");

                        doc.PrintDocument.PrinterSettings.PrinterName = PrinterName;

                   
                        doc.PrintDocument.Print();
                   

                        if ((System.IO.File.Exists(filepath + "/" + BillNowPrefix+',' + Department + ".pdf")))
                        {
                            System.IO.File.Delete(filepath + "/" + BillNowPrefix+',' + Department + ".pdf");
                        }

                    }
                }

            }



       
      
        return rtnval;
    }



    [WebMethod]
    public static string UpdateGSTBill(string LocalOut, string BillNowPrefix, Boolean BillType, string CustomerId, string CustomerName)
    {

        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new BillDAL().UpdateGSTBill(LocalOut, BillNowPrefix, BillType, CustomerId, CustomerName, UserNo);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {


        };
        return ser.Serialize(JsonData);

    }
    [WebMethod]
    public static List<PaymentMode> BindDropOnlinePayemntmode(string PaymentModeID)
    {
        string SqlQuery = string.Empty;
        if (PaymentModeID == "0")
        {
            SqlQuery = "select paymentmodeid as OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode";
        }
        else
            SqlQuery = "select paymentmodeid as OtherPayment_ID,OtherPayment_Name from prop_otherpaymentmode where PaymentModeID=" + PaymentModeID;
        DataTable dt = new DataTable();
        Connection con = new Connection();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        List<PaymentMode> OtherPaymentModeList = new List<PaymentMode>();
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (i == 0 && PaymentModeID == "0")
            {
              //  OtherPaymentModeList.Add(new PaymentMode() { OtherPaymentModeID = "0", OtherPaymentName = "--Choose Other--" });
            }
            OtherPaymentModeList.Add(new PaymentMode() { OtherPaymentModeID = dt.Rows[i].ItemArray[0].ToString(), OtherPaymentName = dt.Rows[i].ItemArray[1].ToString() });
        }

        return OtherPaymentModeList.ToList();
    }

    [WebMethod]
    public static void insertupdate_combo(string comboid, string itemid, decimal amount, string itemqty, decimal comborate, string combosess_id, int NoItem)
    {
        if (comboid != itemid)
        {
            combo cmb = new combo();
            cmb.req = "insert";
            cmb.comboid = comboid;
            cmb.itemid = itemid;
            cmb.amount = amount;
            cmb.itemqty = Convert.ToDecimal(itemqty);
            cmb.No_Ofitem = NoItem;
            cmb.comborate = comborate;
            cmb.combosess_id = combosess_id;
            cmb.insert_update_combo();
        }

    }

    [WebMethod]
    public static string GetCstDis(string PaymentModeID)
    {
        Connection con = new Connection();
        string cst_discount = "";
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("select discount from mst_customer where cst_id=@cst_id", conn);
            cmd.Parameters.AddWithValue("@cst_id", PaymentModeID);
            DataTable dt = new DataTable();
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            adp.Fill(dt);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                cst_discount = rd["discount"].ToString();

            }

            return cst_discount;

        }
    }
    [WebMethod]
    public static string update_comboWtQty(string ComboSesID, decimal QtyWt, decimal NoItem, decimal ComboRate)
    {
        SqlParameter[] Param = new SqlParameter[3];
        Connection con = new Connection();
        Param[0] = new SqlParameter("@ComboSesID", ComboSesID);
        Param[1] = new SqlParameter("@QtyWt", QtyWt);
        Param[2] = new SqlParameter("@NoItem", NoItem);
        SqlHelper.ExecuteNonQuery(con.sqlDataString, CommandType.StoredProcedure, "Proc_ComboUpdate", Param);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            Status = "Saved Combo Calculation"

        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static void update_combo(string comboid, string itemid, string amount, string itemqty, string comborate, string combosess_id, string noItem)
    {

        combo cmb = new combo();
        cmb.req = "delete";
        cmb.update_combo(combosess_id);

        string[] Qty = itemqty.Split(',');
        string[] ComboId = comboid.Split(',');
        string[] ComboItemId = itemid.Split(',');
        string[] Comboamount = amount.Split(',');
        string[] ItemID = itemid.Split(',');
        string[] Comborate = comborate.Split(',');
        string[] NoItem = noItem.Split(',');
        int i = 0;
        foreach (var item in ComboId)
        {
            cmb.req = "insert";
            cmb.comboid = ComboId[i];
            cmb.itemid = ItemID[i];
            cmb.amount = Convert.ToDecimal(Comboamount[i]);
            cmb.itemqty = Convert.ToDecimal(Qty[i]);
            cmb.comborate = Convert.ToDecimal(Comborate[i]);
            //cmb.No_Ofitem = Convert.ToInt32(NoItem[i]);
            cmb.No_Ofitem = 0;
            cmb.combosess_id = combosess_id;
            cmb.insert_update_combo();
            i++;
        }

    }


    [WebMethod]
    public static List<combo> getcombodetails(string combosess_id)
    {
        combo cmb = new combo();
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("select b.item_name,comboid,a.itemid,rate,qty,combo_price,combo_sess_id from combo_detail as a inner join packing_belongs as b on a.itemid = b.item_code  where combo_sess_id  = @combo_sess_id  order by cid asc", con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@combo_sess_id", combosess_id);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        SqlDataReader rd = cmd.ExecuteReader();
        List<combo> CbList = new List<combo>();

        while (rd.Read())
        {
            CbList.Add(new combo()
            {
                comboid = rd["comboid"].ToString(),
                ItemName = rd["item_name"].ToString(),
                itemid = rd["itemid"].ToString(),
                amount = Convert.ToDecimal(rd["rate"]),
                itemqty = Convert.ToDecimal(rd["qty"]),
                combosess_id = rd["combo_sess_id"].ToString(),
                comborate = Convert.ToDecimal(rd["combo_price"].ToString())
            });
        }
        rd.Close();
        con.Close();

        return CbList.ToList();
    }


    [WebMethod]
    public static void delcombodetails(string combosess_id)
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
        // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("delete from combo_detail where combo_sess_id =@combosess_id ", con);
        cmd.Parameters.AddWithValue("@combosess_id", combosess_id);
        cmd.CommandType = CommandType.Text;
        cmd.ExecuteNonQuery();
        con.Close();
    }



    //[WebMethod]
    //public static void keyboard()
    //{

    //      try
    //      {
    //          Process[] pname = Process.GetProcessesByName("osk");
    //          if (pname.Length == 0)
    //          {
    //              Process.Start("osk");
    //              //Process process = Process.Start(new ProcessStartInfo(((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
    //          }
    //          else
    //          {


    //          }
    //      }
    //      catch (Exception)
    //      {


    //      }
    //  }

    [WebMethod]
    public static string InsertOnlineOtherPayment(int ID, int OtherPayment_ID, string Bill_No, string CoupanNo, string CoupanAmt, string Mode, decimal CashAmt, decimal OnlineAmt, string OTPVal)

    {
        string[] ItemCode = CoupanNo.Split(',');
        string[] Amount = CoupanAmt.Split(',');
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        if (ID == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Bill objOnlineOtherPayment = new Bill();
        if (CoupanNo != string.Empty)
        {


            for (int i = 0; i < ItemCode.Length; i++)
            {
                objOnlineOtherPayment = new Bill()
                {
                    ID = ID,
                    OtherPayment_ID = Convert.ToInt32(OtherPayment_ID),

                    BillNowPrefix = Bill_No,
                    CoupanNo = Convert.ToString(ItemCode[i]),
                    CouponAmt = Convert.ToDecimal(Amount[i]),
                    Mode = Mode,
                    CashAmt = CashAmt,
                    OnlineAmt = OnlineAmt,

                };
                status = new BillBLL().InsertOnlineOtherPayment(objOnlineOtherPayment);
            }
        }
        else
        {
            objOnlineOtherPayment = new Bill()
            {
                ID = ID,
                OtherPayment_ID = Convert.ToInt32(OtherPayment_ID),

                BillNowPrefix = Bill_No,
                CoupanNo = CoupanNo == "" ? OTPVal : "",
                CouponAmt = 0,
                Mode = Mode,
                CashAmt = CashAmt,
                OnlineAmt = OnlineAmt,

            };
            status = new BillBLL().InsertOnlineOtherPayment(objOnlineOtherPayment);
        }

        var JsonData = new
        {
            bill = objOnlineOtherPayment,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string CheckReturn()
    {
        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.RETURN).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -12;
        }
        else
        {
            status = 1;
        }


        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindEmployees()
    {

        string EmployeesData = new EmployeeBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            EmployeeOptions = EmployeesData

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindCreditCst()
    {

        List<AccLedger> list = new CustomerBLL().GetAllCreditCustomer();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        StringBuilder str = new StringBuilder();
        str.Append(string.Format("<option value='0'>-Select-</option>"));
        foreach (var item in list)
        {
            str.Append(string.Format("<option value='{0}' CADD1= '{2}' CONT_NO = '{3}' >{1}</option>", item.CCODE, item.CNAME, item.CADD1, item.CONT_NO));
           // str.Append(string.Format("<option  value=" + item.CCODE + ">" + item.CNAME + "</option>"));
        }


        var JsonData = new
        {
            Options = str.ToString()

        };
        return ser.Serialize(JsonData);
    }

    void BindCreditCustomers()
    {

        //ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        //ddlChosseCredit.DataValueField = "CCODE";
        //ddlChosseCredit.DataTextField = "CNAME";
        //ddlChosseCredit.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Customer--";
        //li1.Value = "0";
        //ddlChosseCredit.Items.Insert(0, li1);



        ddlChosseCredit_pm.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit_pm.DataValueField = "CCODE";
        ddlChosseCredit_pm.DataTextField = "CNAME";
        ddlChosseCredit_pm.DataBind();
        ListItem li2 = new ListItem();
        li2.Text = "--Choose Customer--";
        li2.Value = "0";
        ddlChosseCredit_pm.Items.Insert(0, li2);



    }

    [WebMethod]
    public static string LoadUserControl(string counter)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
            (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }


    void BindOtherPayment()
    {

        //ddlOtherPayment.DataSource = new OtherPaymentModeBLL().GetAll();
        //ddlOtherPayment.DataValueField = "OtherPayment_ID";
        //ddlOtherPayment.DataTextField = "OtherPayment_Name";
        //ddlOtherPayment.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Other--";
        //li1.Value = "0";
        //ddlOtherPayment.Items.Insert(0, li1);

        msr.req = "bind_ddcustomer_onlinepay";
        DataTable dt = msr.bind_item_dd();
        ddlOtherPayment.DataSource = dt;
        ddlOtherPayment.DataTextField = "customer_name";
        ddlOtherPayment.DataValueField = "cst_id";
        ddlOtherPayment.DataBind();
        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose Paymode", "0");
        //ddlOtherPayment.Items.Insert(0, listItem1);

    }


    void BindOtherPayment_pm()
    {

        //ddlOtherPayment_pm.DataSource = new OtherPaymentModeBLL().GetAll();
        //ddlOtherPayment_pm.DataValueField = "OtherPayment_ID";
        //ddlOtherPayment_pm.DataTextField = "OtherPayment_Name";
        //ddlOtherPayment_pm.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Other--";
        //li1.Value = "0";
        //ddlOtherPayment_pm.Items.Insert(0, li1);


        msr.req = "bind_ddcustomer_onlinepay";
        DataTable dt = msr.bind_item_dd();
        ddlOtherPayment_pm.DataSource = dt;
        ddlOtherPayment_pm.DataTextField = "customer_name";
        ddlOtherPayment_pm.DataValueField = "cst_id";
        ddlOtherPayment_pm.DataBind();
        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose Paymode", "0");
        //ddlOtherPayment_pm.Items.Insert(0, listItem1);

    }

    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string Reprint(string PrintType, string BillNowPrefix)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new CommonMasterDAL().Reprint(UserNo, PrintType, Branch, BillNowPrefix);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {


        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetBranchState()
    {
        Branches objBranch = new Branches();
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        objBranch.BranchId = Branch;
        new BranchBLL().GetByStateName(objBranch);
        var JsonData = new
        {

            BranchState = objBranch,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetLastBill()
    {
        Bill objBill = new Bill();
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        objBill.UserNO = UserNo;
        new BillBLL().GetLastBill(objBill);
        var JsonData = new
        {

            lastrecord = objBill,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string NewAdvancedSearch(int CategoryId, string Keyword,Int32 posid)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        if (posid == 0)
        {
             posid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.posid].Value);
        }
        string prodData = new ProductBLL().AdvancedSearchforBill(CategoryId, Keyword.Trim(), Branch, posid);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string AdvancedSearch(int CategoryId, string Keyword)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 PosId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.posid].Value);
        string prodData = new ProductBLL().AdvancedSearchforBill(CategoryId, Keyword.Trim(), Branch,PosId);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(string BillNowPrefix)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;



        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        if (roles.Count() == 0)
        {
            Status = -10;
        }
        else
        {
            Status = new BillBLL().DeleteBill(objBill);
        }
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    //[WebMethod]
    //public static string InsertKOT(int TableID, int PaxNo, decimal Value, decimal DisPercentage, decimal DisAmount, decimal ServiceCharges,decimal TaxAmount, decimal TotalAmount, int EmpCode, string itemcodeArr, string priceArr, string qtyArr, string AmountArr, string taxArr,string TaxAmountArr)
    //{

    //    int status = 0;
    //    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

    //    string[] arrRoles = sesRoles.Split(',');



    //    var roles = from m in arrRoles
    //                where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
    //                select m;


    //    if (roles.Count() == 0)
    //    {
    //        status = -11;
    //    }

    //    int Status = 0;
    //    Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
    //    Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    //    //User objuser = new User()
    //    //{
    //    //    UserNo = UserNo

    //    //};

    //    //Int32 status2 = new UserBLL().chkIsLogin(objuser);

    //    KOT objKOT = new KOT()
    //    {
    //        MKOTNo = 0,
    //        TableID = TableID,
    //        PaxNo = PaxNo,
    //        R_Code = 1,
    //        M_Code = UserNo,
    //        Value = Value,
    //        DisAmount = DisAmount,
    //        DisPercentage = DisPercentage,
    //        ServiceCharges = ServiceCharges,
    //        TaxAmount = TaxAmount,
    //        TotalAmount = TotalAmount,
    //        Complementary = false,
    //        Happy = false,
    //        EmpCode = EmpCode,
    //        BillNoWPrefix = "",
    //        Pass = false,
    //        BranchId = Branch,



    //    };
    //    //if (status2 != 0)
    //    //{
    //    string[] ItemCode = itemcodeArr.Split(',');
    //    string[] Price = priceArr.Split(',');
    //    string[] Qty = qtyArr.Split(',');            
    //    string[] Amount = AmountArr.Split(',');
    //    string[] Tax = taxArr.Split(',');    
    //    string[] Tax_Amount = TaxAmountArr.Split(',');


    //    DataTable dt = new DataTable();
    //    dt.Columns.Add("ItemCode");
    //    dt.Columns.Add("Rate");
    //    dt.Columns.Add("Qty");
    //    dt.Columns.Add("Amount");
    //    dt.Columns.Add("Tax");
    //    dt.Columns.Add("Tax_Amount");



    //    for (int i = 0; i < ItemCode.Length; i++)
    //    {
    //        DataRow dr = dt.NewRow();
    //        dr["ItemCode"] = ItemCode[i];
    //        dr["Rate"] = Convert.ToDecimal(Price[i]);
    //        dr["Qty"] = Convert.ToDecimal(Qty[i]);
    //        dr["Amount"] = Convert.ToDecimal(Amount[i]);
    //        dr["Tax"] = Convert.ToDecimal(Tax[i]);
    //        dr["Tax_Amount"] = Convert.ToDecimal(Tax_Amount[i]);

    //        dt.Rows.Add(dr);
    //    }




    //        Status = new kotBLL().Insert(objKOT, dt);



    //    JavaScriptSerializer ser = new JavaScriptSerializer();


    //    var JsonData = new
    //    {
    //        kot = objKOT,
    //        BNF = objKOT.KOTNo,
    //        Status = Status
    //    };
    //    return ser.Serialize(JsonData);
    //}


    [WebMethod]
    public static string KOTprint()
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        List<KotPrint> objkot = new KotPrintBLL().GetDataByUserNo(UserNo);

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            kotprint = objkot

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string DeletePrinter(string BillNowPrefix)
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new BillBLL().DeletePrinter(objBill, UserNo);
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string DeletePrinterUserWise()
    {
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new PrinterDAL().DeletePrinter(UserNo);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }


    protected void btnRefreshPrinter_Click(object sender, EventArgs e)
    {
        Connection con = new Connection();

        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {

            conn.Open();
            SqlCommand cmd = new SqlCommand("truncate table printer", conn);
            cmd.ExecuteNonQuery();

        }
    }

    //protected void btnDetail_Click(object sender, EventArgs e)
    //{
    //    Int32 desiid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value);
    //    if (desiid == 1)
    //    {
    //        Response.Redirect("ViewBillDetail.aspx");
    //    }
    //}

}