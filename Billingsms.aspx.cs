﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class BillingSms : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    if (!IsPostBack)
    {
      GetAll();
    }

  }

  public void GetAll()
  {
    Connection con = new Connection();
    SqlParameter[] Param = new SqlParameter[1];
    Param[0] = new SqlParameter("@Req", "GetData");
    SqlDataReader reader = SqlHelper.ExecuteReader(con.sqlDataString, System.Data.CommandType.StoredProcedure, "Pro_billingsms", Param);
    if (reader.HasRows)
    {
        reader.Read();
        txtOrder.Text = reader["Order_Msg"].ToString();
        chkOrder.Checked = Convert.ToBoolean(reader["Order_IsActive"]);
        txtOrderBill.Text = reader["Order_Bill_Msg"].ToString();
        chkOrderBill.Checked = Convert.ToBoolean(reader["Order_Bill_IsActive"]);
        txtOrderDespatched.Text = reader["Order_Dispatched_Msg"].ToString();
        chkOrderDespatched.Checked = Convert.ToBoolean(reader["Order_Dispatched_IsActive"]);
    }
  }

  protected void btnSubmitChanges_Click(object sender, EventArgs e)
  {
    try
    {
      Connection con = new Connection();
      SqlParameter[] Param = new SqlParameter[7];
      Param[0] = new SqlParameter("@OrderMSg", txtOrder.Text);
      Param[1] = new SqlParameter("@Order_IsActive", chkOrder.Checked);
      Param[2] = new SqlParameter("@Order_Bill_Msg", txtOrderBill.Text);
      Param[3] = new SqlParameter("@Order_Bill_IsActive", chkOrderBill.Checked);
      Param[4] = new SqlParameter("@Order_Despatched_Msg", txtOrderDespatched.Text);
      Param[5] = new SqlParameter("@Order_Despatched_IsActive", chkOrderDespatched.Checked);
      Param[6] = new SqlParameter("@Req", "Insert");
      SqlHelper.ExecuteNonQuery(con.sqlDataString, System.Data.CommandType.StoredProcedure, "Pro_billingsms", Param);
      Response.Write("<script>alert('Settings saved Successfully');</script>");
    }
    catch (Exception)
    {
      throw;
    }

  }
}