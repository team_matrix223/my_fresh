﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageaccounts.aspx.cs" Inherits="manageaccounts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
	 <link href="css/customcss/manageaccounts.css" rel="stylesheet" />
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
 

 
     
   
<script language="javascript" type="text/javascript">

    var m_AccountId = 0;
	var hcode = 0;
    var scode = 0;

	function ApplyRoles(Roles) {


		$("#<%=hdnRoles.ClientID%>").val(Roles);
    }


    function ResetControls() {
        m_AccountId = 0;
		var txtTitle = $("#txtTitle");
		var btnAdd = $("#btnAdd");
		var btnUpdate = $("#btnUpdate");
		txtTitle.focus();
		txtTitle.val("");
		txtTitle.focus();
		$("#ddlAcount").val("");
		btnAdd.css({ "display": "block" });
		btnAdd.html("Add Account");

		btnUpdate.css({ "display": "none" });
		btnUpdate.html("Update Account");

        $("#txtOpBal").val("0");
		$("#btnReset").css({ "display": "none" });
		$("#hdnId").val("0");
		validateForm("detach");
    }

	function TakeMeTop() {
		$("html, body").animate({ scrollTop: 0 }, 500);
	}

	function RefreshGrid() {
		$('#jQGridDemo').trigger('reloadGrid');

    }

    function InsertUpdate() {
        
		if (!validateForm("frmCity")) {
			return;
        }
        var Id = m_AccountId;
		
		var Title = $("#txtTitle").val();
		if ($.trim(Title) == "") {
			$("#txtTitle").focus();

			return;
		}

		var SSCODE = $("#ddlAccount").val();
		var opbal = $("#txtOpBal").val();
        var DRCR = $("#ddlDrCr").val();
		var IsFlag = false;

        if ($('#chkFlag').is(":checked")) {
            IsFlag = true;
		}
		$.ajax({
            type: "POST",
            data: '{"AccountId":"' + Id + '", "AccountName": "' + Title + '","SSCODE": "' + SSCODE + '","SCODE": "' + scode + '","HCODE": "' + hcode + '","OpBal": "' + opbal + '","DRCR": "' + DRCR + '","IsFlag": "' + IsFlag + '"}',
			url: "manageaccounts.aspx/Insert",
			contentType: "application/json",
			dataType: "json",
			success: function (msg) {

				var obj = jQuery.parseJSON(msg.d);

				if (obj.Status == -11) {
					alert("You don't have permission to perform this action..Consult Admin Department.");
					return;
				}

				if (obj.Status == 0) {

					alert("Insertion Failed.Account with duplicate name already exists.");
					return;
				}

				if (Id == "0") {
					ResetControls();
					BindGrid();
					alert("Account is added successfully.");
				}
				else {
					ResetControls();
					BindGrid();
					alert("Account is Updated successfully.");
				}


			},
			error: function (xhr, ajaxOptions, thrownError) {

				var obj = jQuery.parseJSON(xhr.responseText);
				alert(obj.Message);
			},
			complete: function () {
				$.uiUnlock();
			}
		});

	}
    $(document).ready(
    function () {

            BindGrid();


			ValidateRoles();

			function ValidateRoles() {

				var arrRole = [];
				arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

			for (var i = 0; i < arrRole.length; i++) {
				if (arrRole[i] == "1") {

					$("#btnAdd").click(
						function () {


							m_AccountId = 0;
							InsertUpdate();
						}
					);

				}

				else if (arrRole[i] == "3") {

					$("#btnUpdate").click(
						function () {
							InsertUpdate();
						}
					);
				}
			}

		}

        $("#ddlAccount").change(
        function () {

            var BalInc = $("#ddlAccount").val();
            $.ajax({
                type: "POST",
                data: '{ "BALINC": "' + BalInc + '"}',
                url: "manageaccounts.aspx/BindAccGroups",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                   
					$("#ddlAcount").html(obj.AccGroupOptions);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            }
        );


            });
           

			$("#ddlAcount").change(
                function () {
                   
                    scode = $("#ddlAcount").val();
                   
					$.ajax({
                        type: "POST",
                        data: '{ "scode": "' + scode + '"}',
						url: "manageaccounts.aspx/Getbyscode",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);

                            hcode = obj.acchead.H_CODE;

                           
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {

						}

					}
					);


                });

			$("#btnReset").click(
				function () {

					ResetControls();

				}
			);

    });


</script>
    



<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Accounts</h3>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                     

             

                     


                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Add/Edit Account</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                     <tr><td class="headings">Account Master:</td><td colspan = "3">    <select id="ddlAccount" style="width:300px" class="form-control validate ddlrequired" >
                     <option value="B">BALANCE SHEET</option>
                     <option value="I">INCOME/EXPENDITURE</option></select>
                         </td></tr>                               
                    
                     <tr><td class="headings" >Account Name:</td><td colspan = "3">  <input type="text"  name="txtTitle" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 300px"/></td></tr>
                      <tr><td class="headings">Group:</td><td colspan = "3">
                           <select id="ddlAcount" style="width:215px;" class="form-control validate ddlrequired" ></select>
                          <%--<asp:DropDownList style="width:215px;height:35px"  ID ="ddlAccGroup"  runat="server" ></asp:DropDownList>--%>
                    </td></tr>   

                        <tr><td class="headings">Op. Balance</td><td><input type="text"  name="txtOpBal" class="form-control validate required valnumber"   data-index="1" id="txtOpBal" style="width: 150px"/></td><td class="headings">Debit/Credit:</td><td>  
                            <select id="ddlDrCr" style="width:300px" class="form-control validate ddlrequired" >
                         <option value="DR">DR</option>
                     <option value="CR">CR</option></td></tr>      
                         <tr><td class="headings">Flag:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkFlag" checked="checked" data-index="2"  name="chkFlag" /></td>     
                       </tr> 
                                 </select>
                    </td></tr>
               
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           <td>&nbsp;</td><td> <div id="btnAdd"  class="btn btn-primary btn-small" >Add Account</div></td>
                                            <td><div id="btnUpdate"  class="btn btn-primary btn-small" style="display:none;" >Update Account</div></td>
                                            <td><div id="btnReset"  class="btn btn-primary btn-small" style="display:none;" >Cancel</div></td>
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


     <div class="x_panel">
                        <div class="x_title">
                            <h2>Manage Accounts</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">

                               <div class="youhave" style="padding-left:30px">
                    
      	          <table id="jQGridDemo">
    </table>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                        <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


 
</form>


  <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageAccounts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

						colNames: ['Account Id', 'CODE', 'CCODE', 'H_CODE', 'S_CODE', 'SS_CODE', 'CNAME', 'OP. BAL', 'DR/CR','IsFlag'],
  


                        colModel: [
                                    { name: 'AccountId', key: true, index: 'AccountId', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                  
                                    { name: 'CODE', index: 'CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'CCODE', index: 'CCODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'H_CODE', index: 'H_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'S_CODE', index: 'S_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'SS_CODE', index: 'SS_CODE', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
                                    { name: 'CNAME', index: 'CNAME', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                   { name: 'OP_BAL', index: 'OP_BAL', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
 
                                    { name: 'DR_CR', index: 'DR_CR', width: 200, stype: 'text', sortable: true, editable: true,hidden:true, editrules: { required: true } },
							{ name: 'IsFlag', index: 'IsFlag', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'AccountId',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Accounts List",

                        editurl: 'handlers/ManageAccounts.ashx',



                    });




                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
                    m_AccountId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    m_AccountId = $('#jQGridDemo').jqGrid('getCell', rowid, 'AccountId');
                  
                    
                     txtTitle.val($('#jQGridDemo').jqGrid('getCell', rowid, 'CNAME'));
                     txtTitle.focus();

					if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsFlag') == "true") {
						$('#chkFlag').prop('checked', true);
					}
					else {
						$('#chkFlag').prop('checked', false);

					}

                    $("#txtOpBal").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OP_BAL'));
					var sscode = $('#jQGridDemo').jqGrid('getCell', rowid, 'SS_CODE');
					
                    var accgroup = $('#jQGridDemo').jqGrid('getCell', rowid, 'S_CODE');
                    $('#ddlAccount option[value=' + sscode + ']').prop('selected', 'selected');
                    $("#ddlAccount").change();

                    
                   
                     var div = $('#jQGridDemo').jqGrid('getCell', rowid, 'DR_CR')
                     $('#ddlDrCr option[value=' + div + ']').prop('selected', 'selected');
					$('#ddlAcount option[value=' + accgroup + ']').prop('selected', 'selected');
                    $("#btnAdd").css({ "display": "none" });
                    $("#btnUpdate").css({ "display": "block" });
                    $("#btnReset").css({ "display": "block" });
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }


            </script>

</asp:Content>

