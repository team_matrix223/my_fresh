﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
public partial class SuperWelcome : System.Web.UI.Page
{
    public Int32 BranchId { get { return Request.QueryString["bid"] != null ? Convert.ToInt32(Request.QueryString["bid"]) : 0; } }
    public Int32 screenNo { get { return Request.QueryString["Screen"] != null ? Convert.ToInt32(Request.QueryString["Screen"]) : 0; } }
    public string BranchName { get { return Request.QueryString["bname"] != null ? Convert.ToString(Request.QueryString["bname"]) : ""; } }
    public Int32 UserId { get { return Request.QueryString["UserId"] != null ? Convert.ToInt32(Request.QueryString["UserId"]) : 0; } }
    public string UserName { get { return Request.QueryString["UserName"] != null ? Convert.ToString(Request.QueryString["UserName"]) : ""; } }
    public string Device { get { return Request.QueryString["Device"] != null ? Convert.ToString(Request.QueryString["Device"]) : ""; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        Int32 PosId = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);
        User objUser = new User
        {
            UserNo = UserId
        };
        new UserBLL().GetByUserId(objUser);

        Response.Cookies[Constants.AdminId].Value = UserId.ToString();
        Response.Cookies[Constants.DesignationId].Value = objUser.Counter_NO.ToString(); 
        Response.Cookies[Constants.BranchId].Value = BranchId.ToString();
        Response.Cookies[Constants.BranchName].Value = BranchName;
        Response.Cookies[Constants.EmployeeName].Value = UserName;
        Response.Cookies[Constants.DisplayCompanyName_].Value = objUser.DisplayCompanyName;
       CheckUserSession("Login.aspx", UserId.ToString());
  
        //if (objUser.sms==1)
        //{
        //SendSms SendMessage = new SendSms();
        //SendMessage.SMSSend("7508634720","User:-'"+ UserName + "' Is Logged In.");
        //}
        if (Device == "D")
        {   
            if (screenNo == 2)
            {
                Response.Redirect("managekotscreen.aspx");
            }
            else
            {
                if (PosId < 20)
                {
                    Response.Redirect("BillScreen.aspx");

                }
                else
                {
                    Response.Redirect("BillScreenOption.aspx");
                }
            }
        }
       else if (Device == "M")
        {
            Response.Redirect("Reports/Default2.aspx");
        }


    }

    public void CheckUserSession(string pagename,string user)
    {

        UserActivityLog.SetActivityLog("IN",pagename, user);

    }
}