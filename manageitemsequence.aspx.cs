﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class manageitemsequence : System.Web.UI.Page
{
    managesqence ms = new managesqence();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {

        
            bind_dd_dep();

        }
    }

    protected void bindlist(int pos_id)
    {
        ms.req = "bindlist";
        ms.posid = pos_id;
        DataTable dt = ms.bind_item_dd();
        rpt_itemlist.DataSource = dt;
        rpt_itemlist.DataBind();
        if (dt.Rows.Count > 0)
        {

            btnsave.Visible = true;
        }

    }

    public void bind_dd_dep()
    {
        ms.req = "bind_dd";
        DataTable dt = ms.bind_item_dd();
        dd_pos.DataSource = dt;
        dd_pos.DataTextField = "title";
        dd_pos.DataValueField = "posid";
        dd_pos.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select POS-", "0");
        dd_pos.Items.Insert(0, listItem1);

    }

    protected void btnsave_Click(object sender, EventArgs e)
    {

        foreach (RepeaterItem item in rpt_itemlist.Items)
        {
            int prodctid = Convert.ToInt32(((Label)item.FindControl("lblproductid")).Text);
            int lvl = Convert.ToInt32(((System.Web.UI.HtmlControls.HtmlInputControl)item.FindControl("tblvl")).Value);
                ms.req = "update";
                ms.PROP_ID = prodctid;
                ms.lvl = lvl;
                ms.insert_update_sequence();
         

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "key", "alert('Record Updated Successfully!');", true);
    }
    protected void dd_pos_SelectedIndexChanged(object sender, EventArgs e)
    { string ddval = dd_pos.SelectedItem.Value;
        if (ddval == "0")
        {
            btnsave.Visible = false;

        }
        else
        {

            bindlist(Convert.ToInt32(dd_pos.SelectedItem.Value));
        }
    }
}