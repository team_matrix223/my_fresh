﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

public partial class test : System.Web.UI.Page
{
    protected string m_sMessage = "";
    protected string m_sSubMessage = "";

    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {
            BinDDept();
            BindGroups();
            ddlgrp.Visible = false;
            ddlsbgrp.Visible = false;
            ddlCat2.Visible = true;
            rdbDepartment.Checked = true;
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            lbltxt.Text = "Choose Department";
        }
        CheckRole();
       

    }


    public void BinDDept()
    {
        ddlCat2.DataSource = new DepartmentBLL().GetAll();
        ddlCat2.DataValueField = "Prop_ID";
        ddlCat2.DataTextField = "Prop_Name";
        ddlCat2.DataBind();
        ListItem li1 = new ListItem();
        ListItem li2 = new ListItem();
        li1.Text = "--Choose Department--";
        li1.Value = "-1";
        li2.Text = "All";
        li2.Value = "0";
        ddlCat2.Items.Insert(0, li1);
        ddlCat2.Items.Insert(1, li2);


        ddlsbgrp.DataSource = new SubGroupBLL().GetAll();
        ddlsbgrp.DataValueField = "SGroup_Id";
        ddlsbgrp.DataTextField = "SGroup_Name";
        ddlsbgrp.DataBind();
        ListItem li3 = new ListItem();
        ListItem li4 = new ListItem();
        li3.Text = "--Choose SubGroup--";
        li3.Value = "-1";
        li4.Text = "All";
        li4.Value = "0";
        ddlsbgrp.Items.Insert(0, li3);
        ddlsbgrp.Items.Insert(1, li4);
    }

    public void BindGroups()
    {
        ddlGrpFrm.DataSource = new GroupBLL().GetAll();
        ddlGrpFrm.DataValueField = "Group_Id";
        ddlGrpFrm.DataTextField = "Group_Name";
        ddlGrpFrm.DataBind();
        ListItem li1 = new ListItem();
       
        li1.Text = "--Choose Group--";
        li1.Value = "-1";
       
        ddlGrpFrm.Items.Insert(0, li1);


        ddlGrpTo.DataSource = new GroupBLL().GetAll();
        ddlGrpTo.DataValueField = "Group_Id";
        ddlGrpTo.DataTextField = "Group_Name";
        ddlGrpTo.DataBind();
        ListItem li11 = new ListItem();

        li11.Text = "--Choose Group--";
        li11.Value = "-1";

        ddlGrpTo.Items.Insert(0, li11);

        ddlgrp.DataSource = new GroupBLL().GetAll();
        ddlgrp.DataValueField = "Group_Id";
        ddlgrp.DataTextField = "Group_Name";
        ddlgrp.DataBind();
        ListItem li3 = new ListItem();
        ListItem li4 = new ListItem();
        li3.Text = "--Choose Group--";
        li3.Value = "-1";
        li4.Text = "All";
        li4.Value = "0";
        ddlgrp.Items.Insert(0, li3);
        ddlgrp.Items.Insert(1, li4);

    }


    protected void rdbsubgroup_CheckedChanged(object sender, EventArgs e)
    {

        if (rdbSubGroup.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbDepartment.Checked = false; ;
            ddlsbgrp.Visible = true;
            ddlCat2.Visible = false;
            ddlgrp.Visible = false;
            lbltxt.Text = "Choose SubGroup";
           
        }

    }

    protected void rdbgroup_changed(object sender, EventArgs e)
    {

        if (rdbGroup.Checked == true)
        {
            rdbSubGroup.Checked = false;
            rdbDepartment.Checked = false;
            ddlsbgrp.Visible = false;
            ddlCat2.Visible = false;
            ddlgrp.Visible = true;
            lbltxt.Text = "Choose Group";
        }

    }

    protected void rdbdepartment_changed(object sender, EventArgs e)
    {

        if (rdbDepartment.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            ddlsbgrp.Visible = false;
            ddlCat2.Visible = true;
            ddlgrp.Visible = false;
            lbltxt.Text = "Choose Department";


        }

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.UPDATEITEMPRICE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.SAVE).ToString() 
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    //[WebMethod]
    //public static string GetByItemCode(string ItemCode)
    //{
    //    Products objProduct = new Products() { ItemCode = ItemCode };

    //    new ProductsBLL().GetProductDetail(objProduct);

    //    var JsonData = new
    //    {
    //        productData = objProduct
    //    };
    //    JavaScriptSerializer ser = new JavaScriptSerializer();
    //    ser.MaxJsonLength = int.MaxValue;
    //    return ser.Serialize(JsonData);
    //}





    //[WebMethod]
    //public static string BindBrand(int SubCat)
    //{

    //    string Cat2 = new BrandBLL().GetAllBySubCat(SubCat);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Brand = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}

    //[WebMethod]
    //public static string BindAllBrand()
    //{

    //    string Cat2 = new BrandBLL().GetOptions();
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Brand = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}



    //[WebMethod]
    //public static string BindCat1()
    //{

    //    string Cat1 = new CategoryBLL().GetLevel2Options(1);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category1 = Cat1,
    //    };
    //    return ser.Serialize(JsonData);
    //}



    //[WebMethod]
    //public static string BindCat2ByCat1(int CatId)
    //{

    //    string Cat1 = new CategoryBLL().GetOptions(CatId);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category1 = Cat1,
    //    };
    //    return ser.Serialize(JsonData);
    //}


    //[WebMethod]
    //public static string BindCat2()
    //{

    //    string Cat2 = new CategoryBLL().GetLevel2Options(2);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Category2 = Cat2,
    //    };
    //    return ser.Serialize(JsonData);
    //}


    [WebMethod]
    public static string GetData(int Dept,string ItemWise)
    {
        int TotalRows = 0;
        string html = new DepartmentBLL().GetProductByItemWise(Dept,ItemWise, out TotalRows);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            HTML = html,
            TR = TotalRows
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetDataforHSN(int Dept,string HSNCode, string ItemWise)
    {
        int TotalRows = 0;
        string html = new DepartmentBLL().GetProductByDeptforHSN(Dept,HSNCode, ItemWise, out TotalRows);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            HTML = html,
            TR = TotalRows
        };
        return ser.Serialize(JsonData);
    }

    //[WebMethod]
    //public static string GetDataforHSN(int Dept,string HSNCode,string ItemWIse)
    //{
    //    int TotalRows = 0;
    //    string html = new DepartmentBLL().GetProductByDeptforHSN(Dept,HSNCode,ItemWIse, out TotalRows);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {
    //        HTML = html,
    //        TR = TotalRows
    //    };
    //    return ser.Serialize(JsonData);
    //}
    [WebMethod]
    public static string dept()
    {
 
        StringBuilder str = new StringBuilder();
        List<Departments> lst= new DepartmentBLL().GetAll();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        foreach (var item in lst)
        {
            str.Append(string.Format("<option value="+item.Prop_ID+">"+item.Prop_Name+"</option>"));
        }
        var JsonData = new
        {
            HTML = str.ToString()

        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Group()
    {

        StringBuilder str = new StringBuilder();
        List<Group> lst = new GroupBLL().GetAll();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        foreach (var item in lst)
        {
            str.Append(string.Format("<option value=" + item.Group_Id + ">" + item.Group_Name + "</option>"));
        }
        var JsonData = new
        {
            HTML = str.ToString()

        };
        return ser.Serialize(JsonData);
    }


    //[WebMethod]
    //public static string GetProductDetail(string ItemCode, int VariationId)
    //{
    //    int VId = 0;
    //    string HtmlLocal = "";
    //    int OStatus = 0;
    //    int LStatus = 0;
    //    string html = new ProductsBLL().GetProductDetail(ItemCode, VariationId, out VId, out HtmlLocal, out OStatus, out LStatus);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {
    //        HL = HtmlLocal,
    //        HTML = html,
    //        VariationId = VId,
    //        OStatus = OStatus,
    //        LStatus = LStatus
    //    };
    //    return ser.Serialize(JsonData);
    //}

    [WebMethod]
    public static string UpdateVariationDetail(Int64 ItemId, decimal Price, decimal Mrp, decimal Tax, string HSNCode, int DepttId,int Grpid)
    {

        int status = new DepartmentDAL().UpdatePackingbelongsDetail(ItemId, Price, Mrp,Tax,HSNCode, DepttId, Grpid);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string UpdateByHsncode(Int32 Dept, string  FromHsn, string ToHsn,string ItemWise)
    {

        int status = new DepartmentDAL().UpdateByHsnCode(Dept ,FromHsn ,ToHsn,ItemWise);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string UpdateByGroup(Int32 Dept, string FromGrp, string ToGrp)
    {

        int status = new DepartmentDAL().UpdateByGroup(Dept, FromGrp, ToGrp);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        var JsonData = new
        {
            Status = status,

        };
        return ser.Serialize(JsonData);
    }

    //[WebMethod]
    //public static string UpdateCategories(string ItemCode, int Cat1, int Cat2, int BrandId)
    //{

    //    int status = new ProductsDAL().UpdateCategories(ItemCode, Cat1, Cat2, BrandId);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Status = status,

    //    };
    //    return ser.Serialize(JsonData);
    //}


    //[WebMethod]
    //public static string DeActiveVariationDetail(string ItemCode, string VariationId,bool IsActive)
    //{

    //    int status = new ProductsDAL().DeActiveVariationDetail(ItemCode, VariationId,IsActive);
    //    JavaScriptSerializer ser = new JavaScriptSerializer();

    //    var JsonData = new
    //    {

    //        Status = status,

    //    };
    //    return ser.Serialize(JsonData);
    //}








}