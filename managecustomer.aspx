﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managecustomer.aspx.cs" Inherits="managecustomer" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    
<link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <link href="css/customcss/setup.css" rel="stylesheet" />

     <script src="js/jquery-ui.js"></script>

    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>


     <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <style type="text/css">
 #tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
 #tblist tr td{text-align:left;padding:2px}
  #tblist tr:nth-child(even){background:#F7F7F7}
  .setup_title
{
    background :#ffffff;
    color:#73879C;
}
table#frmCity td {
    text-align: left;
}
.x_panel.manage-customer-x-panel-second {
    max-height: 273px;
    min-height: 273px;
    overflow: auto;
}
.form-cntl-cstm-manage-customer
{
    padding:3px 2px;
}
@media(max-width:480px)
{
    #frmCity input,#frmCity select {
	    width: 100% !important;
    }
}
@media(min-width:992px) and (max-width:1200px)
{
    .x_panel.manage-customer-x-panel-second {
	    max-height: 193px;
	    min-height: 193px;
    }
}
 </style>

 <form   runat="server" id="formID" method="post">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="updateProgress" runat="server">
                            <ProgressTemplate>
                                <div style="position: fixed; text-align: center; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999;">
                                    <asp:Image ID="imgUpdateProgress" runat="server" ImageUrl="images/loading.gif" AlternateText="Loading ..."
                                        ToolTip="Loading ..." Style="border-width: 0px; position: fixed; top: 45%; width: 5%;" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Delivery Company</h3>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                     <div class="x_title setup_title">
                            <h2>Add/Edit Delivery Company</h2>
                             
                            <div class="clearfix"></div>
                        </div>

                    <div class="x_panel">
                        
                        <div class="x_content">

                               <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                              
                             <tr>
                  <td class="headings">Name:</td>
                                 <td> <input type="text"  name="tbName" maxlength="50" class="form-control validate required alphanumeric" runat="server"  data-index="1" id="tbName" style="width: 213px"/></td>

                             </tr>
                     <tr><td class="headings">Show In Shop:</td><td>



                         <asp:DropDownList ID="ddstatus" class="form-control form-cntl-cstm-manage-customer" runat="server" style="height: 25px;">

                             <asp:ListItem Value="Y">Yes</asp:ListItem>
                               <asp:ListItem Value="N">No</asp:ListItem>
                         </asp:DropDownList>
                                                          </td></tr>

                                                <tr>
                  <td class="headings">Discount:</td>
                                 <td> <input type="text" maxlength="50" name="tbName" class="form-control validate required alphanumeric" runat="server"  data-index="1" id="tbdiscount" style="width: 213px"/></td>

                             </tr>


                         
                         
                         
                        
      
                     
                     
                     <tr style="display:none">    <td class="headings">Remark:</td>
                         <td>
                             <asp:TextBox ID="tbremark" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox></td></tr>  
                                   <tr>
                                       <td>
                                           <asp:Button ID="btnsave"  class="btn btn-primary btn-small setup_btn" runat="server" Text="Submit" OnClick="btnsave_Click" />

                                       </td>
                                   </tr>
             

                     </table>  
                       
                 
                          
                             

                        </div>
           
                    </div>


     <div class="x_panel manage-customer-x-panel-second">

                        <div class="x_content">

                               <div class="youhave"  >

             <div class="col-md-4">
                        <asp:GridView ID="gv_display" runat="server" OnPageIndexChanging="OnPaging"
                                EnableModelValidation="True" AllowPaging="True"
                                PagerSettings-PageButtonCount="10" PagerStyle-HorizontalAlign="Right" OnRowCommand="gv_display_OnRowCommand" AutoGenerateColumns="false" 
                            class="table_class table table-striped table-bordered table-hover" PageSize="13">

                                <Columns>
                                    <asp:BoundField DataField="customer_name" HeaderText="Customer Name" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                                          <asp:BoundField DataField="status" HeaderText="Is Active" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>
                               <%--           <asp:BoundField DataField="remark" HeaderText="Remark" HeaderStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:BoundField>--%>
                                       <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lnkedit" runat="server" CommandName="select" CommandArgument='<%#Eval("cst_id") %>'><i class="fa fa-edit" style="font-size:20px;color:red"></i></asp:LinkButton>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField>
                                        <ItemTemplate>

                                            <asp:LinkButton ID="lnkdel" runat="server" OnClientClick=" return confirm('Do you want to delete this record ?');" CommandName="del" CommandArgument='<%#Eval("cst_id") %>'><i class="material-icons">delete_forever</i></asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    </Columns>


                        </asp:GridView></div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

                

            </div>
            <!-- footer content -->
                <footer>
                      <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
            <!-- /footer content -->
</ContentTemplate>
                     </asp:UpdatePanel>
 
</form>


</asp:Content>

