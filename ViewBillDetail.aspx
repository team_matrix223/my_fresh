﻿

  <%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ViewBillDetail.aspx.cs" Inherits="BillScreen" %>
  <%@ Register src="~/Templates/CashCustomers.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>
<%@ Register Src="~/Templates/CreditCustomers.ascx" TagPrefix="uc3" TagName="CreditCustomers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

    <form id="form1" runat="server">
        
        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>

     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
      <script src="Scripts/CursorEnd.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <link href="css/customcss/billscreen.css" rel="stylesheet" type="text/css" />
	<link href="css/customcss/viewbilldetail.css" rel="stylesheet" />
     <%--       <link href="css/bootstrap.css" rel="stylesheet" />--%>

<%--<link href="css/bootstrap-select.css" rel="stylesheet" />--%>
<link href="css/css.css" rel="stylesheet" />
      
    <style type="text/css">
                    div#screenDialog1 {
    height: auto !important;
}
					
        .nav1 ul li {
    float: left;
    background: #f4f3ef;
    width: 83px;
    height: 50px;
     display: table; 
}

        #tbliteminfo td {
            padding: 0;
        }

        #tbamountinfo td {
            padding: 0;
        }
      
        .ui-widget-header button {
    position: relative;
    top: 10px !important;
    z-index: 1;
    right: 0px !important;
}

        .button3 {
    height: 40px;
    box-shadow: inset -2px -5px 5px #8d3c42;
    font-size:10px;
}

        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }

        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable {
            height: auto !important;
            /*width: 100% !important;*/
            /*top: 0px !important;*/
        }


            .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable > #getcombodetail {
                z-index: 1000 !important;
            }

       .button1 {
    box-shadow: inset -2px -5px 5px #8d3c42;
    font-size: 10px;
    height: 40px;
    margin: 2px;
    padding: 10px 5px 5px;
    text-align: center;
    vertical-align: middle;
    width: 95%;
    cursor: pointer;
    color: #fff !important;
    text-transform: uppercase;
    border-radius: 5px;
    display: inline-block;
    border: none;
}


        .box h3 {
            font-weight: bold;
            margin: 0px;
            padding: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            height: 45px;
            background: #887d73;
            color: #FFFFFF;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        #txtddlMobSearchBox {
            padding-left: 29px;
        }

        #dvOuter_ddlMobSearchBox {
            height: 50px;
        }

        #mlkeyboard {
            z-index: 100 !important;
        }
.leftside {
    margin: 0px;
    border: 1px solid #46c9ac;
    background: -webkit-linear-gradient(#46c9ac, #46c9ac);
        border-radius: 0px;
}
        .ui-keyboard-keyset {
            text-align: center;
            white-space: nowrap;
            float: left;
        }
        .ui-dialog
        {
            min-height: 657px;
            padding-bottom:0 !important;
        }
        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.billscreen-gst-bill {
            width: 40% !important;
            margin: 0 auto;
            text-align: center;
            display: block;
            left: 240px !important;
            margin-top: 66px !important;
            min-height:290px !important;
        }
        table.cstmer-table-bill-screen div#dvOuter_ddlMobSearchBox1 {
            width: 100% !important;
        }
        div#btnsa 
        {
            float:left;
            margin: 10px auto;
            text-align: center;
            position: relative;
            left: 50%;
        }
		button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
			display: block !important;
		}
        @media (max-width:991px)
        {
            .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.billscreen-gst-bill {
	            width: 100% !important;
	            left: 0px !important;
	        }
	        div#btnsa {
	            left: 50%;
            }
        }
        @media(min-width:668px) and (max-width:767px)
        {
            .ui-dialog {
	            min-height: 767px;
	        }
        }
        @media(min-width:768px) and (max-width:991px)
        {
            .ui-dialog {
	            min-height: 991px;
	        }
        }
        @media(min-width:992px) and (max-width:1024px)

        {
            .bill_detail_screen div#colDvProductList {
	            height: 347px !important;
				border-bottom:unset;
            }
            .product_items .cate {
	            height: 276px !important;
            }
            .bill_detail_screen div#products {
	            height: 448px !important;
            }
        }
        
        @media(min-width:1025px) and (max-width:1100px){
            .product_items .cate {
	            height: 257px !important;
            }
        }
        @media(min-width:1101px) and (max-width:1200px){
            .product_items .cate {
	            height: 250px !important;
            }
        }
        @media (min-width:1201px) and (max-width:1310px)
        {
            .product_items .cate {
	            height: 245px;
            }
        }

    </style>
    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
    
    <script src="js/ShortcutKey.js" type="text/javascript"></script>
     
    <script language="javscript" type="text/javascript">
        //$(function () {
        //    $('input[type="text"]').keyboard();

        //    $('textarea').keyboard();
        //    $('.txt').keyboard();

        //});

        $(document).ready(function () {
           
   

    Mousetrap.bind('alt+i', function (e) {
    	$("#btnView").click();


    });
   
   
});
        
        var MachineNo = 0;
        function ApplyRoles(Roles) {
            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }
        var con_getitemcode = 0;
        var delarrayid = 0;
        var subcm_amt = 0;
        var subcm_Qtyamt = 0;
        var ComboCalCount = 0;
        var combosubcnt = 0;
        var combosess_id = "";
        var combosesqty_id = "";
        var countecombo = 0;
        var ComboTotalAmount = 0;
        var dirval = 0;
        var itemnametype = "";
        var con_itemnametype = "";
        var con_QtyCombo = "";
        var Storeval = ""
        var comborate = 0;
        var ClickEditQtyCombo = false;
        var con_getcomboprice = 0;
        var con_getcomboid = 0;
        var getcomboprice = 0;
        var getcomboid = 0;
        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
		var DefaultBankName = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode = "";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var OpenDiv = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var AllowKKC = false;
        var AllowSBC = false;
        var KKC = 0;
        var SBC = 0;
        var DefaultService = "";
        var NoOfCopy = 1;
        var PrintTime = false;
        var StartCursor = "";
        var Printkot_Reprint = false;
        var DiscountValues = [];
        var cst_id = 0;
        var getdel_cust_id = 0;
        var pos_edit = 0;
        var ComPrice = 0;
        var CountWight = 0;
        var PriceCount = 0;
        var FlagEdit = 0;
        var KotAmt = 0;
        var FlagPaymode = 0;
     
       
       

       


       
        function processingComplete() {
            $.uiUnlock();

        }

        var onlineoption = "COD";
        var CshCustSelId = 0;
        var CshCustSelName = "";
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var m_BillNowPrefix = "";
        var m_DiscountType = "";
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var billingmode = ""
        var OrderId = "";
        var Type = "";
        var TotalItems = 0;
        //.....................................




        function viewBill(BillNowPrefix) {



        	var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
        	if ($.trim(SelectedRow) == "") {
        		alert("No Product is selected to add");
        		return;
        	}

        	var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');
        	var option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');



        	//	var getdel_cust_id = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'cst_id');



        	if (option == "Dine") {
        		$("#ddlTable1").removeAttr("disabled");

        		$("#ddlTable1 option").removeAttr("selected");

        		$('#ddlTable1 option[value=' + TableNo + ']').prop('selected', 'selected');
        	}

        	else if (option == "HomeDelivery") {
        		$("#ddlTable1").hide();


        		$("#ddlTable1 option").removeAttr("selected");

        		$("#DelCharges1").show();
        		$("#dvDeliveryChrg1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DeliveryCharges'));
        		//$("#dvDeliveryChrg").attr("disabled", "disabled");

        	}



        	BillNowPrefix = m_BillNowPrefix;
        	$.ajax({
        		type: "POST",
        		data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
        		url: "ViewBillDetail.aspx/GetBillDetailByBillNowPrefix",
        		contentType: "application/json",
        		dataType: "json",
        		success: function (msg) {

        			var obj = jQuery.parseJSON(msg.d);
        			var prevmode = $("#txtRemarks1").val();
        			$("#dvDeliveryChrg1").val(obj.productLists[0]["DeliveryCharges"]);
        			var billmode = obj.productLists[0]["BillMode"];

        			$("#txtcustomer1").val(obj.productLists[0]["CustomerName"]);

        			$("#txtcst").val(obj.productLists[0]["CSTName"]);

        			$("#txtRemarks1").val(billmode);



        			for (var i = 0; i < obj.productLists.length; i++) {
        				addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["CursorOn"], 0, 0, obj.productLists[i]["TaxType"]);


        			}
        			$("#txtcustomer1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name'));
        			//$("#txtcst").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Cst_Name'));
        			$("#dvsbtotal1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Bill_Value'));

        			$("#dvdisper1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));
        			$("#dvdiscount1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Less_Dis_Amount'));
        			$("#dvVat1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Add_Tax_Amount'));

        			$("#dvnetAmount1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Net_Amount'));
        		},
        		error: function (xhr, ajaxOptions, thrownError) {

        			var obj = jQuery.parseJSON(xhr.responseText);
        			alert(obj.Message);
        		},
        		complete: function () {



        			$.uiUnlock();
        		}

        	});





        }



        function EditBill(BillNowPrefix) {


            BillNowPrefix = m_BillNowPrefix;
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);
                    $("#txtRemarks").val(obj.productLists[0]["BillRemarks"]);

                    var billoption = obj.productLists[0]["Remarks"];


                   

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["CursorOn"], 0, 0, obj.productLists[i]["TaxType"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


      
       

        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");
            var txtOnlineAmount = $("#txtpaymentAmount");
            var txtCoupanAmount = $("#Coupans");
            var txtCODAmount = $("#txtCODAmount");
            var txtCredit = $("#txtCreditAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }

            else if ((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCreditCard.val(0);
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }
            else if ((Number(txtOnlineAmount.val()) + Number(txtCoupanAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtCoupanAmount.val(Number(txtFinalBillAmount.val()) - Number(txtOnlineAmount.val()));
                txtCheque.val(0);

            }


            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) + Number(txtCoupanAmount.val()) + Number(txtCODAmount.val()) + Number(txtCredit.val()) - Number(txtFinalBillAmount.val())));
            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }




        //...................................

        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;
        var KKCTAmt = 0;
        var SBCTAmt = 0;

      


      
       
        $(document).ready(function () {
           
        })

      


        var option = "";



     
    function Scrolldown() {
        setTimeout(function () {
            var elem = document.getElementById('ScrollHgt');
            elem.scrollTop = elem.scrollHeight;
        }, 100);
    }

   

    $(document).ready(
     function () {

         var count = 0;


                     $("#btnView").click(
                         function () {

                             //ProductCollection = [];
                             //$('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                             //$("#txtRemarks1").val("");
                             //$("#txtcst").val("");


                             //viewBill(m_BillNowPrefix);


                            
                         });
   

      



     });

 var m_ItemId = 0;
 var m_ItemCode = "";
 var m_ItemName = "";
 var m_Qty = 0.00;
 var m_Price = 0;
 var m_TaxRate = 0;
 var m_Surval = 0;
 var m_ItemDiscount = 0;
 var m_EditSaleRate = 0;
 var m_CursorOn = "";
 var m_EditVal = 0;

 var ProductCollection = [];

 function clsproduct() {
     this.ItemId = 0;
     this.ItemCode = "";
     this.ItemName = "";
     this.Qty = 0;
     this.Price = 0;
     this.TaxCode = 0;
     this.SurVal = 0;
     this.ProductAmt = 0;
     this.Producttax = 0;
     this.ProductSurchrg = 0;
     this.TaxId = 0;
     this.ItemRemarks = "";
     this.ItemDiscount = 0;
     this.EditSaleRate = false;
     this.CursorOn = "";
     this.EditButton = false;
     this.EditQtyButton = false;
     this.Combo_Sessionid = 0;
     this.ComboQty_Sessionid = 0;
     this.EditVal = 0;
     this.NoItem = 0;
     this.Tax_type = 0;
 }
 var m_Total = 0;
 
		

function Bindtr(btntext) {

     DiscountAmt = 0;
     VatAmt = 0;
     Total = 0;
     var fPrice = 0;
     TotalItems = 0;
     $("div[name='vat']").html("0.00");
     $("div[name='amt']").html("0.00");
     $("div[name='sur']").html("0.00");

     $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
	 $('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
     var counterId = 1;
     BEndDiscountAmt = 0;
     var Count = 0;

     var _ComboCount = 1;
     for (var i = 0; i < ProductCollection.length; i++) {

         BillBasicType = ProductCollection[i].Tax_type;


         //if (BillBasicType == "I") {

         //    $("#vatIncOrExc").hide();
         //}
         //else {
         //    $("#vatIncOrExc").show();
         //}


         if (ProductCollection[i].EditButton == true) {
             if (delarrayid != ProductCollection[i].Combo_Sessionid)
                 var TotalAmount = Number((ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]) * (ProductCollection[i]["NoItem"])).toFixed(2);
             var tr = "<tr  style='background-color:darkorange;' data-item='" + ProductCollection[i].Combo_Sessionid + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center'  value='" + TotalAmount + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'  id='dvEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             //delarrayid = 0;
         }
         if (ProductCollection[i].EditQtyButton == true) {
             if (delarrayid != ProductCollection[i].ComboQty_Sessionid)
                 var tr = "<tr  style='background-color:turquoise;' data-item='" + ProductCollection[i].ComboQty_Sessionid + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;'  value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center'  value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'  id='dvQtyEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
         }
         else if (_ComboCount == ProductCollection.length && con_itemnametype == "COMBO") {
             var tr = "<tr class='SumMainCombo' style='background-color:darkorange;'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;'  disabled='disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' class='comboamt' name='txtBillAmount' style='width:70px;text-align:center'  value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'  id='dvsavecombo'><i  style='cursor:pointer'><img src='images/save.png'/></i></td><td style='width:50px;text-align:center'  id='dvEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             Storeval = ProductCollection.length;
             countecombo++;
         }
         else if (_ComboCount == ProductCollection.length && con_QtyCombo == "QTY_COMBO") {
             var tr = "<tr class='SumMainQtyCombo' style='background-color:turquoise'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;' disabled='disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' class='comboQtyamt' name='txtBillAmount' style='width:70px;text-align:center' disabled='disabled' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'  id='dvsavecombo'><i  style='cursor:pointer'><img src='images/save.png'/></i></td><td style='width:50px;text-align:center'  id='dvQtyEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             Storeval = ProductCollection.length;
             countecombo++;
         }
         else {
             if (ProductCollection[i]["EditSaleRate"] == true) {
                 if (delarrayid != ProductCollection[i].Combo_Sessionid) {
                     if (ProductCollection[i]["Qty"] < 0) {
                         var tr = "<tr style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + Number(ProductCollection[i]["Qty"]) + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";

                     }
                     else {
                         var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + Number(ProductCollection[i]["Qty"]) + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                     }
                 }
                 else {
                     var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + Number(ProductCollection[i]["Qty"]) + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                 }
             }
             else {
                 if (ProductCollection[i]["EditSaleRate"] == false && ProductCollection[i].EditButton == false) {

                     if (ProductCollection[i]["Qty"] < 0) {
                         var tr = "<tr  style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                     }

                     else {
                         if (ProductCollection[i]["EditVal"] == 1)
                         {
                             var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input  disabled='disabled' class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input disabled='disabled' type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input disabled='disabled' type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                         }
                         else
                         {
                             var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                             var amt = 0;
                             amt =  Number(ProductCollection[i]["Price"]);
                             KotAmt = KotAmt + amt;
                         }
                         


                     }
                 }
                 else if (ProductCollection[i].EditButton == false) {
                     if (ProductCollection[i]["Qty"] < 0) {
                         var tr = "<tr  style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
                       
                     }
                     else {
                         var tr = "<tr ><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";


                     }
                 }


             }

         }

        
		 $("#tbProductInfo1").append(tr);
         

         counterId = counterId + 1;

         Count = Count + 1;


        


        

        
         if (ProductCollection[i]["Qty"] < 1) {

             TotalItems = TotalItems + Number(1);


         }
         else {

             TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
         }


         _ComboCount++;
     };

    
    


     
	 
     if (ProductCollection.length == 0) {
         var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
        
		 $("#tbProductInfo1").append(tr);
     }

    
	 $("#lblNoItems1").val(TotalItems);

 }
 
 
 
        var OnLoad = 0;

		


        function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount, B_Qty, EditSaleRate, CursorOn, btntext, EditVal, TaxType) {
			
            var ExistedKot = 0;
 <%--         
            if ($("#<%=ddloption.ClientID%>").val() == 'Dine' && OnLoad==1) {


                ExistedKot = 1;
                OnLoad == 1;
            }
            else {

                ExistedKot = 0;
            }--%>
    m_ItemId = ProductId;
    m_ItemCode = Code;
    m_Price = Price;
    m_ItemName = Name;
    m_EditSaleRate = EditSaleRate;
    m_CursorOn = CursorOn;
    m_EditVal = EditVal;

    if (B_Qty == 0) {
        if (modeRet == "Return") {
            m_Qty = -1;
        }
        else {
            m_Qty = 1;
        }
    }
    else {
        m_Qty = B_Qty;
    }
    m_TaxRate = TaxCode;
    m_Surval = SurVal;
    m_Tax_Id = Tax_Id;
    m_ItemDiscount = Discount;

    TO = new clsproduct();
    var ExistItem = "";
    for (var i = 0; i < ProductCollection.length; i++) {
        if (m_ItemName === ProductCollection[i].ItemName) {
            ExistItem = "Exist";
        }
    }
    var Amount = 0;
    var Qty = 0;
            if (ProductCollection.length == 0) {
        
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;
        TO.CursorOn = m_CursorOn;
        TO.EditVal = m_EditVal;
        TO.Tax_type = TaxType;
                
                ProductCollection.push(TO);
				

    }
    else if (ExistItem != "" && EditVal==1) {
        for (var i = 0; i < ProductCollection.length; i++) {
            if (m_ItemName === ProductCollection[i].ItemName) {
                Amount = (parseFloat(Amount) + parseFloat(ProductCollection[i].Price));
                Qty = parseFloat(ProductCollection[i].Qty)
                ProductCollection[i].Qty = parseFloat(Qty) + parseFloat(m_Qty);
            }
        }

    }
    else {
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;
        TO.CursorOn = m_CursorOn;
        TO.EditVal = m_EditVal;
        TO.Tax_type = TaxType;
        ProductCollection.push(TO);

    }
    Bindtr(btntext);
    
    if (StartCursor=="ItemCodeQty") {
    	$("#tbProductInfo tbody tr").last().children().find('input').eq(0).select();
    }
  
    var textControlID = $("#tbProductInfo tbody tr").last().children().find('input').eq(0).attr('id')

    // $("#" + textControlID).caretToEnd();

}

       




//............................................





    </script>
   
       
 
                <div class="right_col billing_screen_right_col">

                
                    <div class="clearfix"></div>

                    <div class="row bill-screen-row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="page-title bill-screen-page-title">
                        <div class="title_left">
                            <h3><label>Retail Billing        </label> 

                            <asp:Label ID="lbltotl" runat="server" Text="Total Sale = " Font-Size="Medium"></asp:Label> <asp:Label ID="LblTodayTotalSale" Font-Size ="Medium" runat="server"></asp:Label> 
                             <asp:Label ID="lblcash" runat="server" Text ="Cash Sale = " Font-Size="Medium"></asp:Label>   <asp:Label ID="LblTodayCash" Font-Size="Medium" runat="server"></asp:Label>
                             <asp:Label ID="lblcrcrd" runat="server" Text ="CrCard Sale =  " Font-Size="Medium"> </asp:Label><asp:Label ID="LblTodayCrCardSale" Font-Size="Medium" runat="server"></asp:Label>
                              <asp:Label ID="lblonline" runat="server" Text ="Online Sale =  " Font-Size="Medium"></asp:Label> <asp:Label ID="LblTodayOnlineSale" Font-Size="Medium" runat="server"></asp:Label>
                                <%--<label>Cash Sale = </label><asp:Label ID="Label4" runat="server"></asp:Label>
                                <label>Cash Sale = </label><asp:Label ID="Label5" runat="server"></asp:Label>
                                <label>Cash Sale = </label><asp:Label ID="Label6" runat="server"></asp:Label>--%>


                            </h3>
                            
                        </div>
                     </div>
						

     <div class="view-btn-div ">
		 <asp:LinkButton runat="server" class="btn btn-danger view-btn-close" OnClick="btnDetail_Click">
     <i class="fa fa-times" aria-hidden="true"></i>
</asp:LinkButton>
		<%-- <asp:Button id="btnDetail" class="btn btn-success view-btn" Text="Back Button" OnClick="btnDetail_Click" runat="server"  />--%>

     </div>
                                         
						</div>

                       <div class="col-md-6 col-sm-6 col-xs-12">
                         
                            <div class="x_panel view-bill-xpanel">

                                <div class="x_title" style="display:none;">
                                    <h2>Bills List</h2>
                                 
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group date-tbl-frm-grp" style="margin-bottom:0 !important;">
                                 <div class="form-group betlist_form_table" style="margin-bottom:0 !important;">
                                
                                <table class="view-date-tbl">
                                <tr><td >Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:80px;background-color:White;"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td>
                                <td >Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:80px;background-color:White;font-size:medium;"   id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                  
            




                               
                                </tr>
                                    <tr>
                                        <td >User:</td>
                                    <td><div class="col-md-4 col-sm-4 col-xs-12">
             <asp:DropDownList class="form-control" id="ddlUser" ClientIDMode="Static" runat="server" placeholder="Choose Branch"></asp:DropDownList>
           </div></td> 
                                   <td ></td>
                                    <td><div class="col-md-4 col-sm-4 col-xs-12">
            <%-- <asp:DropDownList class="form-control" id="ddlBranch" AutoPostBack="true" ClientIDMode="Static" runat="server" placeholder="Choose Branch" OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"></asp:DropDownList>--%>
           </div></td>     
                                        
                                       
 
                                    </tr>
									<tr>
										<td>Pos:</td><td><div class="col-md-4 col-sm-4 col-xs-12">
				<asp:DropDownList ID="ddPos" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div></td>



                                         <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
									</tr>
                                </table>

                <table id="jQGridDemoBill">

                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                     
                                </div>


                                   
                                        <div class="form-group bill_list_btns">
                                            <div class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table>
                                                    <tr>

                                                        <td><div class="btn btn-success" " id="btnView" style ="display:none">
                                           <i class="fa fa-edit m-right-xs"></i> View<u>Bi</u>ll</div></td>
                                           
                                          
                                   
                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <div class="col-md-12 col-div-product-lilst" >
            <div class="" id="colDvProductList1">
                <div id="dvProductList1" class="product_items" style="display: block;">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                               <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                    Code
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>
                                 
                                <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    Wt/Qty
                                </th>
                                  
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                 <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amount
                                </th>
                                
                                
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" id="ScrollHgt1" style="overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo1">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                                
                            </tr>

                         <%--      <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                  <table id="tbcomboInfo"></table>
                                </td>
                                
                            </tr>--%>
                       
                       
                          
                           
                        </table>
                      
                    </div>
                   
                </div>
               
              
                </div>
         </div>
                               <div class="col-md-6 col-sm-6 col-xs-12 no-of-itm">
                    <table class="table reciept_detail" id="tbliteminfo1" style="float:left;font-size:12px; border: 1px solid #eeeeee;">
                                 <tr>           <td>No Of Items: </td><td style="border-right: 1px solid #eeeeee;">
                            
                              <div style="text-align:left;width:60%;">
                                 
								   <input type="text" class="txt form-control" id="lblNoItems1" style="width: 132px;" disabled="disabled"/>
								  
                                  </div></td></tr>
                                  <tr>
                                        <td style="font-weight: bold;width:80px; text-align: left">
                                            BillMode:
                                        </td>
                                        <td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtRemarks1" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td>
                                    </tr>
                                    <tr style="
    height: 20px;"> <td style="font-weight: bold;width:80px; text-align: left">
                                            CustomerName:
                                        </td>
                                        <td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtcustomer1" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td> 
                                        <%--<td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtcst" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td> --%>

                                    </tr>
                        <tr style="
    height: 20px;
">
                           <%-- <td > LastBillAmount:</td>
                            <td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt1" style="font-weight:bold;font-size:13px;color:Red;"></div></td>--%>

                        </tr>
                                        <%--    <tr style="border-bottom: 1px solid #eeeeee;border-right: 1px solid #eeeeee;"><td style="font-weight: bold;width:150px; text-align: left;font-weight:bold;border-bottom: 1px solid #eeeeee;"></td>--%>
                                               <%--<td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt" style="font-weight:bold;width: 146px;font-size:13px;color:Red;text-align:center"></div></td>--%>
                            
                    </table>

                      <%-- <p class="show_date"><%=DateTime.Now.DayOfWeek %>,<%=DateTime.Now %></p>--%>

                </div>

                <div style="float:right;" class="col-md-6 col-sm-6 col-xs-12 amt-details-tbl">
                    <table id="tbamountinfo1" class="table reciept_detail" style="font-size: 12px;float: right;">
                        <tr>
                          
							<asp:Literal id="Literal1" runat="Server" Visible="false"/></tr>

                         
                           
                           

                                    <tr>

                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Amount:
                                        </td>
                                         <td style="width: 100px; text-align: left;border-right: 1px solid #eeeeee;" colspan="100%">
                                              <input type="text" class="txt form-control" id="dvsbtotal1" style="width: 132px;" disabled="disabled"/>
                                            
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Discount:
                                        </td>
                                        <td style="border-right: 1px solid #eeeeee;padding: 0px !important;">
                                            <table>
                                                <tr>

<td style="width: 30px; text-align: right;">
                                        <input type="text" class="form-control input-small" style="width:45px;height:19px;color:black;" 
                                                    id="dvdisper1" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="3">
                                           <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;"  
                                                    id="dvdiscount1" value ="0" />
                                        </td>
                                                </tr>


                                            </table>

                                        </td>


                                        
                                    </tr>
                                   
                                    <tr id ="vatIncOrExc1" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   text-align: left">
                                            GST Amount:
                                        </td>
                                        <td style="width: 100px; text-align: left;"  colspan="100%">
                                            <input type="text" class="txt form-control" id="dvVat1" style="width: 132px;" disabled="disabled"/>
											
											 
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges1" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="width:80px;padding-right: 10px;height: 25px; text-align: left;border-left: 1px solid #eeeeee;">
                                            DeliveryCharges:
                                        </td>
                                        <td style="width: 100px; text-align: left;border-right: 1px solid #eeeeee;padding-bottom: 0px;"  colspan="100%">
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;" id="dvDeliveryChrg1" value ="0" />

                                                    </td>
                                                    </tr>
                                           
                                     
                                     <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;display:none">
                                        <td style="padding-right: 10px;height: 25px;  text-align: left;">
                                            Round Off:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvRound1">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: left;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                            Net Amt:
                                        </td>
                                        <td style="width: 100px; text-align: left;"  colspan="100%">
                                            <input type="text" class="txt form-control" id="dvnetAmount1" style="width: 132px;" disabled="disabled"/>
											
											 
                                        </td>
                                    </tr>
                            
                                           
                                </table>
                            </div>
                     
						</div>
                    </div>

                    
                    <script type="text/javascript">

                        jQuery.fn.putCursorAtEnd = function () {

                            return this.each(function () {

                                // Cache references
                                var $el = $(this),el = this;

                                // Only focus if input isn't already
                                if (!$el.is(":focus")) {
                                    $el.focus();
                                }

                                // If this function exists... (IE 9+)
                                if (el.setSelectionRange) {

                                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                                    var len = $el.val().length * 2;

                                    // Timeout seems to be required for Blink
                                    setTimeout(function () {
                                        el.setSelectionRange(len, len);
                                    }, 1);

                                } else {

                                    // As a fallback, replace the contents with itself
                                    // Doesn't work in Chrome, but Chrome supports setSelectionRange
                                    $el.val($el.val());

                                }

                                // Scroll to the bottom, in case we're in a tall textarea
                                // (Necessary for Firefox and Chrome)
                                this.scrollTop = 999999;

                            });

                        };

                        (function () {

                            var searchInput = $("#search");

                            searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });

                        })();


                        $(document).ready(function () {


                            $("#txtDateFrom,#txtDateTo").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();







                            $("#btnGo").click(function () { BindGrid(); });

                            $(".txt").click(function () {
                                $.ajax({
                                    type: "POST",
                                    url: "billscreen.aspx/Keyboard",
                                    contentType: "application/json",
                                    dataType: "json",

                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function (msg) {


                                    }

                                });
                            });


                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>

                   

                    

 
                </div>
                <!-- /page content -->

               

            </div>






 
           
            
     
    </form>
    
 
   
  <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          
          var UserId = $("#<%=ddlUser.ClientID %>").val();
         
          var posId = $("#<%=ddPos.ClientID %>").val();
		
          jQuery("#jQGridDemoBill").GridUnload();

          var $grid = jQuery("#jQGridDemoBill").jqGrid({
			  url: 'handlers/ManageBillsByPos.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo + '&UserId=' + UserId + '&PosId=' + posId ,
              //url: 'handlers/ManageBillsByPos.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo + '&UserId=' + UserId + '&PosId=' + posId + '&BranchId=' + BranchId,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CstName', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer', 'CashCustAddr', 'CreditCustAddr', 'DeliveryCharges', 'KKCPer', 'KKCAmt', 'SBCPer', 'SBCAmt', 'User', 'Time', 'posid','cst_id'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 300, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                            { name: 'cst_name', index: 'cst_name', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCPer', index: 'KKCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCAmt', index: 'KKCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCPer', index: 'SBCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCAmt', index: 'SBCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'UserName', index: 'UserName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                            { name: 'Time', index: 'Time', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                              { name: 'posid', index: 'posid', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                             { name: 'cst_id', index: 'cst_id', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },


              ],

			  rowNum: 100000000,
              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'Bill_No',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'asc',
              ignoreCase: true,
              caption: "Bills List",
              rowList: [10, 20, 30, 100000000],
              loadComplete: function () {
                  $("option[value=100000000]").text('All');
              },
          })
          $('#t_' + $.jgrid.jqID($grid[0].id))
            .append($("<div><label for=\"globalSearchText\">Global search:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $(document).ready(function () {
              $("#gview_jQGridDemoBill").removeAttr('style');
              $("#gview_jQGridDemoBill").attr('style', 'width: 716px;');
              //$('#jQGridDemoBill').closest(".ui-jqgrid-bdiv").css({ "max-height: 364px;overflow-y": "scroll" });
              $(".ui-jqgrid-bdiv").remove('style');
              $(".ui-jqgrid-bdiv").attr('style', 'height: 100%;width: 716px;overflow-y": "scroll');

          });
          $(function () {
              $('#globalSearchText').keyboard();
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



                  m_BillNowPrefix = 0;
                  m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');
              


              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
              //  $("#txtddlMobSearchBox").val(CashCustName);

              //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");


               ProductCollection = [];
                             $('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                             $("#txtRemarks1").val("");
                             $("#txtcst").val("");


                             viewBill(m_BillNowPrefix);


          }
      });





      $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
              {
                  refresh: false,
                  edit: false,
                  add: false,
                  del: false,
                  search: false,
                  searchtext: "Search",
                  addtext: "Add",
              },

              {//SEARCH
                  closeOnEscape: true

              }

                );


      var DataGrid = jQuery('#jQGridDemoBill');
      DataGrid.jqGrid('setGridWidth', '716');


  }

  $(".getprice").click(function () {

      //var prc = $(this).attr('class');
      alert("ok");
  });
  </script>
<%--     <link href="virtualKeyboard/jquery.ml-keyboard.css" rel="stylesheet" />

    <script src="virtualKeyboard/jquery.ml-keyboard.min.js"></script>--%>
    <script type="text/javascript">



    </script>
   
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
               <iframe id="reportkot" width="0" height="0" onload="processingComplete()"></iframe>
</asp:Content>

