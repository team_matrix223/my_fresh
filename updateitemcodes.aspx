﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="updateitemcodes.aspx.cs" EnableEventValidation="false"   Inherits="test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 

<head runat="server">
    <title></title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/jquery-1.10.2.js"  type="text/javascript"></script>
    <script src="js/jquery-ui.js"  type="text/javascript"></script>
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="js/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/updateitemcode.css" rel="stylesheet" />
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <style type="text/css">
    .active
    {
         background:orange;
    border:solid 1px gray;
    padding:3px;
    margin:2px;
   cursor:pointer;
    text-decoration:none;
     margin-bottom:15px;
    
    }
        
    .paging
    {
        background:silver;
        border:solid 1px gray;
        padding:3px;
        margin:2px;
        cursor:pointer;
        text-decoration:none;
        margin-bottom:15px;
    }
   
    </style>




    <script language="javascript">

        var PageSize = 25;
        var PageNo = 1;
        var VID = 0;
        var UpdateStatus = 0;

        var DeptForall = "";
        var GrpForall = "";
        var ItemWise = "";
        $(document).ready(function () {


            Getdept();
            GetGrp();
        function Printt(PageNumber, PageSize, Cat2,BrandId) {
       

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
            //document.getElementById('reportout').contentWindow.location = "Reports/RptBilling.aspx?BillNowPrefix=" + celValue;
            //document.getElementById('reportout').contentWindow.location = "Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize;
            document.getElementById('reportout').contentWindow.location = "Backoffice/Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize + "&Cat2=" + Cat2 + "&BrandId="+BrandId;
            //window.location = "Backoffice/Reports/rptproducts.aspx?PageNumber=" + PageNumber + "&PageSize=" + PageSize + "&Cat2=" + Cat2;
            $.uiUnlock();

        }

       



        function DeActiveRecord(Id) {



          

            var VariationId = Id;


            var Code = $("#txtItemCode_" + VariationId).val();
            var ISActive = 'false';


          
          
            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + Code + '","VariationId":"' + VariationId + '","IsActive":"' + ISActive + '"}',
                url: "updateitemcodes.aspx/DeActiveVariationDetail",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                  
                    var obj = jQuery.parseJSON(msg.d);

                   
                        alert("Information is Deleted");
                        return;
                    
             

                },
                complete: function (msg) {
                    var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                    var BrandId = "";

                    if ($.trim(Cat2) != "") {
                        GetData(16);
                    }

                }

            });



        }



        function UpdateCategories() {





            var Code = $("#txtItemCode").val();
            if (Code == "") {
                alert("Please Enter Item Code");
                $("#txtItemCode").focus();
                return;
            }
            var Cat1 = "";
           
            var Cat2 = "";
            
            var BrandId = "";
           



            $.ajax({
                type: "POST",
                data: '{ "ItemCode": "' + Code + '","Cat1":"' + Cat1 + '","Cat2":"' + Cat2 + '","BrandId":"' + BrandId + '"}',
                url: "updateitemcodes.aspx/UpdateCategories",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    alert("Information is updated");
                    return;

                },
                complete: function (msg) {

                }

            });
        }



 
        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '27') {
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
               
            }

        });

        $(document).on("keypress", "input[name='pitemcode']", function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                var pid = $(this).attr("id");
                var arrPid = pid.split('_');

                var VariationId = arrPid[1];
                VID = VariationId;
              

                return;


            }
        });


			function GetDataforHSN(Deptid,HSNCode,ItemWise) {

               
                
				$.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

               
				$.ajax({
                    type: "POST",
					data: '{ "Dept": "' + Deptid + '","HSNCode": "' + HSNCode + '","ItemWise": "' + ItemWise + '"}',
					url: "updateitemcodes.aspx/GetDataforHSN",
					contentType: "application/json",
					async: false,
					dataType: "json",
					success: function (msg) {
						var obj = jQuery.parseJSON(msg.d);
                        $("#dvProducts").html(obj.HTML);
                        
					},
					complete: function (msg) {


						$(".cls_dpt").html(DeptForall);
						$(".cls_grp").html(GrpForall);


						$.uiUnlock();
					}

				});


			}

        function GetData(Deptid,ItemWise) {
         
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


            $.ajax({
                type: "POST",
                data: '{ "Dept": "' + Deptid + '","ItemWise": "' + ItemWise + '"}',
                url: "updateitemcodes.aspx/GetData",
                contentType: "application/json",
                async:false,
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    $("#dvProducts").html(obj.HTML);
                 
                },
                complete: function (msg) {
            

                    $(".cls_dpt").html(DeptForall);
                    $(".cls_grp").html(GrpForall);

                  
                    $.uiUnlock();
                }

            });


        }

        function Getdept()
        {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "updateitemcodes.aspx/dept",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {


                    var obj = jQuery.parseJSON(msg.d);


                    DeptForall=obj.HTML;
                },
                complete: function (msg) {
                    $.uiUnlock();
                }

            });


        }

        function GetGrp() {
            $.ajax({
                type: "POST",
                data: '{}',
                url: "updateitemcodes.aspx/Group",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    GrpForall = obj.HTML;
                },
                complete: function (msg) {
                    $.uiUnlock();
                }

            });


        }
			$("#dvUpdateHsn").click(
				function () {

					UpdateCategories();
				});

            $("#btnUpdate").click(
                function () {
                    if (ItemWise == "Department") {
                        updatehsn($("#<%=ddlCat2.ClientID %>").val(), $("#<%=txtfromHSN.ClientID %>").val(), $("#<%=txttoHSN.ClientID %>").val(), ItemWise);;
                    }
                    if (ItemWise == "Group") {
                        updatehsn($("#<%=ddlgrp.ClientID %>").val(), $("#<%=txtfromHSN.ClientID %>").val(), $("#<%=txttoHSN.ClientID %>").val(), ItemWise);;

                    }
                    if (ItemWise == "SubGroup") {
                        updatehsn($("#<%=ddlsbgrp.ClientID %>").val(), $("#<%=txtfromHSN.ClientID %>").val(), $("#<%=txttoHSN.ClientID %>").val(), ItemWise);;

					}
            });
			$("#btnUpdategrp").click(
                function () {
                   
                    updategrp($("#<%=ddlCat2.ClientID %>").val(), $("#<%=ddlGrpFrm.ClientID %>").val(), $("#<%=ddlGrpTo.ClientID %>").val());;

                });

			function updategrp(DeptId, FromGrp, ToGrp) {
				
                var Dept = DeptId;
                var FromGrp = FromGrp;
                var ToGrp = ToGrp;

                alert(ToGrp);

				$.ajax({
                    type: "POST",
					data: '{ "Dept": "' + Dept + '","FromGrp":"' + FromGrp + '","ToGrp":"' + ToGrp + '"}',
					url: "updateitemcodes.aspx/UpdateByGroup",
					contentType: "application/json",
					dataType: "json",
					success: function (msg) {

						var obj = jQuery.parseJSON(msg.d);

						alert("Information is updated");
						//GetData($("#ddlCat2").val())

					},
					complete: function (msg) {

					}

				});

			}

            function updatehsn(DeptId, FromHsn, ToHsn,ItemWise) {
                var Dept = DeptId;
                var FromHsn = FromHsn;
                var ToHsn = ToHsn;



				$.ajax({
                    type: "POST",
                    data: '{ "Dept": "' + Dept + '","FromHsn":"' + FromHsn + '","ToHsn":"' + ToHsn + '","ItemWise":"' + ItemWise + '"}',
					url: "updateitemcodes.aspx/UpdateByHsncode",
					contentType: "application/json",
					dataType: "json",
					success: function (msg) {

						var obj = jQuery.parseJSON(msg.d);

						alert("Information is updated");
						//GetData($("#ddlCat2").val())

					},
					complete: function (msg) {

					}

				});

            }
            $(document.body).on('click', '.btn-small', function (e) {

                var currentRow = $(this).closest("tr");
                var itemcode = currentRow.find(".td_itemcode").text();

                UpdateRecord($(this).attr('data'));

            });
            function UpdateRecord(Id) {



                var ItemID = Id;



                var Price = $("#txtPrice_" + Id).val();
                var Mrp = $("#txtMrp_" + Id).val();
                var tax = $("#txttax_" + Id).val();
                var HSNCode = $("#txthsncode_" + Id).val();
                var DepttId = $("#dd_dpt_" + Id).val();
                var Grpid = $("#dd_grp_" + Id).val();



                $.ajax({
                    type: "POST",
                    data: '{ "ItemId": "' + ItemID + '","Price":"' + Price + '","Mrp":"' + Mrp + '","Tax":"' + tax + '","HSNCode":"' + HSNCode + '","DepttId":"' + DepttId + '","Grpid":"' + Grpid + '"}',
                    url: "updateitemcodes.aspx/UpdateVariationDetail",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        alert("Information is updated");
                        //GetData($("#ddlCat2").val())

                    },
                    complete: function (msg) {

                    }

                });







            }
            $("#txtItemCode").keyup(
     function () {


         GetByItemCode();
     });


            $("#<%=txtfromHSN.ClientID %>").change(
                function () {
                    if (ItemWise == "Department") {
                       
                        GetDataforHSN($("#<%=ddlCat2.ClientID %>").val(), $(this).val(), ItemWise);
                    }
					if (ItemWise == "Group") {
						GetDataforHSN($("#<%=ddlgrp.ClientID %>").val(), $(this).val(), ItemWise);
                    }
					if (ItemWise == "SubGroup") {
						GetDataforHSN($("#<%=ddlsbgrp.ClientID %>").val(), $(this).val(), ItemWise);
					}
					$('#dvProducts tr').each(function () {
						var $tds = $(this).find('td');
						if ($tds.length != 0) {
							var $ItemID = $tds.eq(1).text();
                            var $GroupID = $tds.eq(10).text();
							var $DeptID = $tds.eq(11).text();
                            $("#dd_grp_" + $ItemID).val($GroupID);
							$("#dd_dpt_" + $ItemID).val($DeptID);
							//alert('Curr Source Language: ' + $currText);
						}
					});

                });


            $("#<%=ddlCat2.ClientID %>").change(
                function () {
                    ItemWise = "Department";
                    GetData($(this).val(), ItemWise);

        $(".cls_dpt").val($(this).val());

        $('#dvProducts tr').each(function () {
            var $tds = $(this).find('td');
            if ($tds.length != 0) {
                var $ItemID = $tds.eq(1).text();
                var $GroupID = $tds.eq(10).text();
                $("#dd_grp_" + $ItemID).val($GroupID);
                //alert('Curr Source Language: ' + $currText);
            }
        });
       
    });



			$("#<%=ddlgrp.ClientID %>").change(
				function () {
					ItemWise = "Group";
					GetData($(this).val(), ItemWise);

					//$(".cls_dpt").val($(this).val());

					$('#dvProducts tr').each(function () {
						var $tds = $(this).find('td');
						if ($tds.length != 0) {
							var $ItemID = $tds.eq(1).text();
                            var $GroupID = $tds.eq(10).text();
                            var $DeptID = $tds.eq(11).text();
                            
                            $("#dd_grp_" + $ItemID).val($GroupID);
							$("#dd_dpt_" + $ItemID).val($DeptID);
							//alert('Curr Source Language: ' + $currText);
						}
					});

				});



			$("#<%=ddlsbgrp.ClientID %>").change(
				function () {
					ItemWise = "SubGroup";
					GetData($(this).val(), ItemWise);

					//$(".cls_dpt").val($(this).val());

					$('#dvProducts tr').each(function () {
						var $tds = $(this).find('td');
						if ($tds.length != 0) {
							var $ItemID = $tds.eq(1).text();
							var $GroupID = $tds.eq(10).text();
							var $DeptID = $tds.eq(11).text();
							$("#dd_grp_" + $ItemID).val($GroupID);
							$("#dd_dpt_" + $ItemID).val($DeptID);
							//alert('Curr Source Language: ' + $currText);
						}
					});

				});



            //GetData(1, PageSize);
            $("#dvclose").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });


            $("#dvPrint").click(function () {
                var Cat2 = $("#<%=ddlCat2.ClientID%>").val();
                var BrandId = "";
                Printt(PageNo, PageSize, Cat2, BrandId);
            });







            $("#btnOk").click(function () {

                UpdateRecord(VID);
                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
            $("#btnCancel").click(function () {

                var dialogDiv1 = $('#dvPopup');
                dialogDiv1.dialog("option", "position", [500, 200]);
                dialogDiv1.dialog('close');
            });
        });
       
    </script>
</head>
<body>
<iframe id="reportout" width="0" height="0"   onload="processingComplete()"></iframe>
    <form id="form1" runat="server">
    <div class="update_itemcode_table">
        
    <table>
        <tr class="branch-name-udate-item-code-table"><td><%= Request.Cookies[Constants.BranchName].Value %></td></tr>
      <tr><td> <div class="tax-wise-sale-radio-section">
						
						<asp:RadioButton ID="rdbDepartment" Checked="true" name ="all"   runat="server" AutoPostBack="true" Text="Department" OnCheckedChanged="rdbdepartment_changed" />
						<asp:RadioButton ID="rdbGroup" name ="all"   runat="server" Text="Group" AutoPostBack="true" OnCheckedChanged="rdbgroup_changed" />
					 <asp:RadioButton ID="rdbSubGroup" name ="all"   runat="server" Text="SubGroup" AutoPostBack="true" OnCheckedChanged="rdbsubgroup_CheckedChanged" />
				</div></td></tr>
        <tr>
          <td class="headings" colspan="100%">
             <table class="heading_table">
                    <tr>
                        <td ><asp:label ID="lbltxt" runat="server"></asp:label></td>
                        <td >  
                         <asp:DropDownList ID="ddlCat2" runat="server"></asp:DropDownList>
                         <asp:DropDownList ID="ddlgrp" runat="server"></asp:DropDownList>
                         <asp:DropDownList ID="ddlsbgrp" runat="server"></asp:DropDownList>
                       </td>

                         <td ><b>Change HSNCode From:</b></td>
                        <td > 
                            <asp:TextBox ID="txtfromHSN" runat="server"></asp:TextBox>
                           
                         
                       </td>
                        <td ><b>HSNCode To:</b></td>
                        <td >  
                              <asp:TextBox ID="txttoHSN" runat="server"></asp:TextBox>
                        
                       </td>


                        <td> <div id="btnUpdate" class="btn btn-primary btn-small" style="width:80px">Update</div></td>

                    </tr>
                 <tr>
                      <td ><b>Change Group From:</b></td>
                        <td > 
                            <asp:DropDownList ID="ddlGrpFrm" runat="server"></asp:DropDownList>
                       </td>
                        <td ><b>Group To:</b></td>
                        <td >  
                             <asp:DropDownList ID="ddlGrpTo" runat="server"></asp:DropDownList>
                        
                       </td>


                        <td> <div id="btnUpdategrp" class="btn btn-primary btn-small" >Update Group</div></td>

                 </tr>
             </table>
          </td>
      </tr>



    <tr>
        <td colspan="100%">
            <table class="table updateitemcode_products">
             <tbody  id="dvProducts"></tbody>
           </table>
       </td>
    </tr>

   <tr><td colspan="100%"><div id="dvPages"></div></td></tr>
   <tr><td colspan="100%" align="center"><div id="dvPrint" style="cursor:pointer;display:none" class="btn btn-primary btn-small"><b>Print</b></div>
  
   </td>
   </tr>

   <tr><td colspan="100%">
      <div id="dvPopup" style="display:none;background-color:Black;color:White" >
  <table width="100%" cellpadding="3" >
                    
                  
                     <tr>
                     <td valign="top" style="text-align:center " colspan="100%" >
                     
                    <h3 style="padding-bottom: 10px">Product Detail</h3>
                  
                    </td>
                    <td valign="top">  <div id="dvclose" style="cursor:pointer"><b>Close</b></div></td>
                    </tr>
                  
                    <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetail" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
               <tr>
                    <td style="padding-bottom:10px" align="center">
                   <span id="sp_ProductDetailLocal" style="float:left;padding-left:30px"></span>
                  
                    </td></tr>
                    <tr><td colspan="100%" style="float:right;padding-left:20px"><table><tr>
                    <td style="padding-right:20px">
                      <div id="btnOk" class="btn btn-primary btn-small" style="width:80px;display:none">Update</div>
                    </td>
                    <td>
                  
                    <div id="btnCancel" class="btn btn-primary btn-small" style="width:80px">Cancel</div>
                    </td>
                    </tr>
                    </table></td></tr>
                    </table>
                    </div>
   </td></tr>
    
    </table>
 
    <a href="/index.aspx" class="update-price-exit-btn">Exit</a>

    </div>
    </form>
</body>
</html>
