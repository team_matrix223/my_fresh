﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PreLogin : System.Web.UI.Page
{
    string Map_logpath = "";
    protected void Page_Load(object sender, EventArgs e)
    {
       
        try
        {

            Map_logpath = Request.QueryString["maplogpath"].ToString();
        }
        catch (Exception)
        {

            Response.Redirect("Default.aspx");
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        SqlCommand cmd = new SqlCommand("strp_getdb", con);
        cmd.Parameters.AddWithValue("@mater_key", txtPassword.Value.Trim());
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        if (dt.Rows.Count>0)
        {
            EncDyc ED = new EncDyc();
            string constring= "server =" + dt.Rows[0][1].ToString() + "; database = " + dt.Rows[0][2].ToString() + "; uid = " + dt.Rows[0][3].ToString() + "; pwd =" + dt.Rows[0][4].ToString() + "";
            Response.Cookies[Constants.dbname].Value = ED.Encrypt(constring);
            if (Map_logpath=="1")
            {
                Response.Redirect("Login.aspx?q=1");
            }
            else if (Map_logpath == "2")
            {
                Response.Redirect("StewardLogin.aspx?q=1");
            }

            //dt.Rows[0][1].ToString()
        }
        else
        {
            Response.Write("<script>alert('Invalid Key')</script>");
        }

    }
}