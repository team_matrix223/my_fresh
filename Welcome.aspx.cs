﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Welcome : System.Web.UI.Page
{
    Int32 Screen = 0;
    Int32 PosId = 0;
    public Int32 UserId { get { return Request.QueryString["Id"] != null ? Convert.ToInt32(Request.QueryString["Id"]) : 0; } }
    public Int32 db { get { return Request.QueryString["db"] != null ? Convert.ToInt32(Request.QueryString["db"]) : 0; } }
    public string dbase { get { return Request.QueryString["dbase"] != null ? Convert.ToString(Request.QueryString["dbase"]) : "0"; } }

    public string Touch { get { return Request.QueryString["Touch"] != null ? Convert.ToString(Request.QueryString["Touch"]) : "0"; } }

    protected void Page_Load(object sender, EventArgs e)
    {
        EncDyc ED = new EncDyc();
        Response.Cookies[Constants.AdminId].Value = UserId.ToString();
        Response.Cookies[Constants.DataBase].Value = dbase.ToString();
        Response.Cookies[Constants.Touch].Value = Touch.ToString();
        string constring = "server =49.50.124.155,1443; database = " + dbase + "; uid = ms; pwd =ZXD#%@SD54g; Connect Timeout=5000";

        Response.Cookies[Constants.dbname].Value = ED.Encrypt(constring);
       
        HttpCookie cookie = new HttpCookie(Constants.dbname);
        cookie.Value = ED.Encrypt(constring);
        cookie.Expires = DateTime.Now.AddYears(100);
        Response.SetCookie(cookie);

        try
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["DCookie1"].Value))
            {
                 PosId = Convert.ToInt32(HttpContext.Current.Request.Cookies["DCookie1"].Value);

                string SessionId = Session.SessionID;
                User objuser = new User()
                {
                    UserNo = (UserId),
                    SessionId = SessionId

                };

                Int32 status = new UserBLL().UserLoginCheck(objuser);
                if (status == -1)
                {
                    Response.Write("<script>alert('Invalid User');</script>");

                }
                if (status == -3)

                {
                    Response.Write("<script>alert('Please First  Login From Window EXE');</script>");

                }
                if (status == -2)
                {
                    Response.Write("<script>alert('Already Login on some other machine');</script>");

                }
                if (status > 0)
                {

                    User objUser = new User
                    {
                        UserNo = UserId
                    };
                    new UserBLL().GetByUserId(objuser);
                    Response.Cookies[Constants.AdminId].Value = UserId.ToString();
                    Response.Cookies[Constants.DesignationId].Value = objuser.Counter_NO.ToString();
                    Response.Cookies[Constants.BranchId].Value = objuser.BranchId.ToString();
                    Response.Cookies[Constants.BranchName].Value = objuser.BranchName;
                    Response.Cookies[Constants.EmployeeName].Value = objuser.User_ID;
                    Response.Cookies[Constants.DisplayCompanyName_].Value = objuser.DisplayCompanyName;

                    Connection con1 = new Connection();
                    SqlConnection con = new SqlConnection(con1.sqlDataString);
                    SqlCommand cmd = new SqlCommand("select * from pos_detail where posid = " + PosId + "", con);
                    DataTable dt = new DataTable();
                    dt.Clear();
                    SqlDataAdapter adb = new SqlDataAdapter(cmd);
                    adb.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        Screen = Convert.ToInt32(dt.Rows[0]["screen"].ToString());


                        Response.Cookies[Constants.ScreenNo].Value = Screen.ToString();
                    }
                    else
                    {
                        Response.Write("<script>alert('PosId doesn't Exists')</script>");
                        return;
                    }





                    if (Screen == 2)
                    {

                        Response.Redirect("managekotscreen.aspx");
                    }
                    else
                    {
                        if (PosId < 20)
                        {
                            Response.Redirect("BillScreen.aspx");

                        }
                        else
                        {
                            if (Touch == "1")
                            {
                                Response.Redirect("BillScreenSlip.aspx");
                                //  Response.Redirect("TouchBilling.aspx");
                            }
                            else
                            {
                                Response.Redirect("BillScreenOption.aspx");
                            }
                        }

                    }

                }
            }
        }
        catch
        {
            if (PosId != 0)
            {
                if (Screen == 2)
                {

                    Response.Redirect("managekotscreen.aspx");
                }
                else
                {
                    if (PosId < 20)
                    {
                        Response.Redirect("BillScreen.aspx");

                    }
                    else
                    {
                        if (Touch == "1")
                        {
                            Response.Redirect("BillScreenSlip.aspx");
                            // Response.Redirect("TouchBilling.aspx");
                        }
                        else
                        {
                            Response.Redirect("BillScreenOption.aspx");
                        }
                    }

                }

            }
            else
            {
                Response.Redirect("login.aspx");

            }
        }

    }
}