﻿

  <%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" EnableEventValidation="false" CodeFile="GstBillScreen.aspx.cs" Inherits="BillScreen" %>
  <%@ Register src="~/Templates/CashCustomers.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>
<%@ Register Src="~/Templates/CreditCustomers.ascx" TagPrefix="uc3" TagName="CreditCustomers" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
<link href="css/customcss/GstBillScreen.css" rel="stylesheet" />
    <form id="form1" runat="server">
        
        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>

     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>

    <link href="css/style.css" rel="stylesheet" type="text/css" />
      <script src="Scripts/CursorEnd.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <link href="css/customcss/billscreen.css" rel="stylesheet" type="text/css" />
     <%--       <link href="css/bootstrap.css" rel="stylesheet" />--%>

<%--<link href="css/bootstrap-select.css" rel="stylesheet" />--%>
<link href="css/css.css" rel="stylesheet" />
      
    <style type="text/css">
                    div#screenDialog1 {
    height: auto !important;
}
					div#screenDialog {
    height: auto !important;
}
        .nav1 ul li {
    float: left;
    background: #f4f3ef;
    width: 83px;
    height: 50px;
     display: table; 
}

        #tbliteminfo td {
            padding: 0;
        }

        #tbamountinfo td {
            padding: 0;
        }
      
        .ui-widget-header button {
    position: relative;
    top: 10px !important;
    z-index: 1;
    right: 0px !important;
}

        .button3 {
    height: 40px;
    box-shadow: inset -2px -5px 5px #8d3c42;
    font-size:10px;
}

        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }

        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable {
            height: auto !important;
            /*width: 100% !important;*/
            /*top: 0px !important;*/
        }


            .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable > #getcombodetail {
                z-index: 1000 !important;
            }

       .button1 {
    box-shadow: inset -2px -5px 5px #8d3c42;
    font-size: 10px;
    height: 40px;
    margin: 2px;
    padding: 10px 5px 5px;
    text-align: center;
    vertical-align: middle;
    width: 95%;
    cursor: pointer;
    color: #fff !important;
    text-transform: uppercase;
    border-radius: 5px;
    display: inline-block;
    border: none;
}


        .box h3 {
            font-weight: bold;
            margin: 0px;
            padding: 5px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            height: 45px;
            background: #887d73;
            color: #FFFFFF;
            border-top-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-right-radius: 3px;
            border-bottom-left-radius: 3px;
        }

        #txtddlMobSearchBox {
            padding-left: 29px;
        }

        #dvOuter_ddlMobSearchBox {
            height: 50px;
        }

        #mlkeyboard {
            z-index: 100 !important;
        }
.leftside {
    margin: 0px;
    border: 1px solid #46c9ac;
    background: -webkit-linear-gradient(#46c9ac, #46c9ac);
        border-radius: 0px;
}
        .ui-keyboard-keyset {
            text-align: center;
            white-space: nowrap;
            float: left;
        }
        .ui-dialog
        {
            min-height: 657px;
            padding-bottom:0 !important;
        }
        .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.billscreen-gst-bill {
            width: 40% !important;
            margin: 0 auto;
            text-align: center;
            display: block;
            left: 240px !important;
            margin-top: 66px !important;
            min-height:290px !important;
        }
        table.cstmer-table-bill-screen div#dvOuter_ddlMobSearchBox1 {
            width: 100% !important;
        }
        div#btnsa 
        {
            float:left;
            margin: 10px auto;
            text-align: center;
            position: relative;
            left: 50%;
        }
		button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
			display: block !important;
		}
        @media (max-width:991px)
        {
            .ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable.billscreen-gst-bill {
	            width: 100% !important;
	            left: 0px !important;
	        }
	        div#btnsa {
	            left: 50%;
            }
        }
        @media(min-width:668px) and (max-width:767px)
        {
            .ui-dialog {
	            min-height: 767px;
	        }
        }
        @media(min-width:768px) and (max-width:991px)
        {
            .ui-dialog {
	            min-height: 991px;
	        }
        }
        @media(min-width:992px) and (max-width:1024px)

        {
            .bill_detail_screen div#colDvProductList {
	            height: 347px !important;
            }
            .product_items .cate {
	            height: 130px !important;
            }
            .bill_detail_screen div#products {
	            height: 335px !important;
            }
			.button_options td#tdsavebill {
				width: 20% !important;
			}
        }
        
        @media(min-width:1025px) and (max-width:1100px){
            .product_items .cate {
	            height: 257px !important;
            }
			.button_options td#tdsavebill {
				width: 20% !important;
			}
			.bill_detail_screen div#products {
				height: 457px !important;
			}
        }
        @media(min-width:1101px) and (max-width:1200px){
            .product_items .cate {
	            height: 250px !important;
            }
			.button_options td#tdsavebill {
				width: 20% !important;
			}
			.bill_detail_screen div#products {
				height: 448px !important;
			}
        }
        @media (min-width:1201px) and (max-width:1320px)
        {
            .product_items .cate {
	            height: 245px;
            }
			.button_options td#tdsavebill {
				width: 20% !important;
			}
			.bill_detail_screen div#products {
				height: 442px !important;
			}
        }
		/*@media(min-width:1321px){
			.product_items .cate {
				height: 209px;
			}
		}*/

    </style>
    <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
  <script src="js/Jquery.numpad.js" type="text/javascript"></script>
    <link href="css/Jquery.numpad.css" rel="stylesheet" />
    
    <script src="js/ShortcutKey.js" type="text/javascript"></script>
     
    <script language="javscript" type="text/javascript">
        //$(function () {
        //    $('input[type="text"]').keyboard();

        //    $('textarea').keyboard();
        //    $('.txt').keyboard();

        //});

        $(document).ready(function () {
            var LocalOut = "";
            BindCreditCst();
    Mousetrap.bind('alt+n', function (e) {
        $("#btnShowScreen").click();
 

    });

    Mousetrap.bind('alt+i', function (e) {
        $("#btnEdit").click();


    });
    Mousetrap.bind('alt+r', function (e) {
        $("#btnReprint").click();


    });

    Mousetrap.bind('alt+c', function (e) {
        $("#btnDelete").click();


    });
    Mousetrap.bind('alt+g', function (e) {
        $("#btnGSTBill").click();


    });
});
        
        var MachineNo = 0;
        function ApplyRoles(Roles) {
            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }
        var con_getitemcode = 0;
        var delarrayid = 0;
        var subcm_amt = 0;
        var subcm_Qtyamt = 0;
        var ComboCalCount = 0;
        var combosubcnt = 0;
        var combosess_id = "";
        var combosesqty_id = "";
        var countecombo = 0;
        var ComboTotalAmount = 0;
        var dirval = 0;
        var itemnametype = "";
        var con_itemnametype = "";
        var con_QtyCombo = "";
        var Storeval = ""
        var comborate = 0;
        var ClickEditQtyCombo = false;
        var con_getcomboprice = 0;
        var con_getcomboid = 0;
        var getcomboprice = 0;
        var getcomboid = 0;
        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
        var BillOption = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode = "";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var AllowKKC = false;
        var AllowSBC = false;
        var KKC = 0;
        var SBC = 0;
        var DefaultService = "";
        var NoOfCopy = 1;
        var PrintTime = false;
        var StartCursor = "";
        var Printkot_Reprint = false;
        var DiscountValues = [];
        var cst_id = 0;
        var getdel_cust_id = 0;
        var pos_edit = 0;
        var ComPrice = 0;
        var CountWight = 0;
        var PriceCount = 0;
        var FlagEdit = 0;
        var KotAmt = 0;
        var FlagPaymode = 0;
     
        $("#CreaditCustomerPart").hide();
        $("#CreditSearchBtn").hide();
        function clsDiscount() {
            this.StartValue = 0;
            this.EndValue = 0;
            this.DisPer = 0;

        }
       

        function ResetCashCustmr() {

            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");

        }


        function BindAddOn(counter) {

            $.ajax({
                type: "POST",
                data: '{"counter": "' + counter + '"}',
                url: "BillScreen.aspx/LoadUserControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    $("#dvAddOn").html(msg.d);

                    $("#dvAddOn").dialog({
                        autoOpen: true,

                        width: 500,
                        resizable: false,
                        modal: false
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            });
        }

        function processingComplete() {
            $.uiUnlock();

        }

        var onlineoption = "COD";
        var CshCustSelId = 0;
        var CshCustSelName = "";
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var m_BillNowPrefix = "";
        var m_DiscountType = "";
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var billingmode = ""
        var OrderId = "";
        var Type = "";
        var TotalItems = 0;
        //.....................................


        function BindTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlTable").html(obj.TableOptions);
					$("#ddlTable1").html(obj.TableOptions);
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }

        function BindEmployees() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "BillScreen.aspx/BindEmployees",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlEmployees").html(obj.EmployeeOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }


        function BindKOTTables() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "screen.aspx/BindTables",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlKotTable").html(obj.TableOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }

        function BindKOTEmployees() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "BillScreen.aspx/BindEmployees",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlKotSteward").html(obj.EmployeeOptions);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }


            });


        }


        function OpenBillWindow() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").hide();
            $("#dvBillWindow").show();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
           
        }

        function OpenProductWindow() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            //$("#tbliteminfo").removeAttr('style');
            //$("#tbliteminfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;');
            //$("#tbamountinfo").removeAttr('style');
            //$("#tbamountinfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;');
        }

        function UnHoldBill(HoldNo) {
            RestControls();

            $.ajax({
                type: "POST",
                data: '{ "HoldNo": "' + HoldNo + '"}',
                url: "screen.aspx/GetByHoldNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["CursorOn"], 0, 0, '', obj.productLists[i]["TaxType"]);

                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


        function viewBill(BillNowPrefix) {
           
            
			$('#<%=dd_customername1.ClientID %>').attr("disabled", "disabled");
			$('#<%=ddloption1.ClientID %>').attr("disabled", "disabled");
			var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
			if ($.trim(SelectedRow) == "") {
				alert("No Product is selected to add");
				return;
			}

			var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');
			var option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');



		//	var getdel_cust_id = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'cst_id');


			$("#<%=ddloption1.ClientID%>").val(option);
            $("#<%=dd_customername1.ClientID%>").val(getdel_cust_id);
			if (option == "Dine") {
				$("#ddlTable1").removeAttr("disabled");

				$("#ddlTable1 option").removeAttr("selected");

				$('#ddlTable1 option[value=' + TableNo + ']').prop('selected', 'selected');
			}

			else if (option == "HomeDelivery") {
				$("#ddlTable1").hide();

				
				$("#ddlTable1 option").removeAttr("selected");
				
				$("#DelCharges1").show();
				$("#dvDeliveryChrg1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DeliveryCharges'));
				//$("#dvDeliveryChrg").attr("disabled", "disabled");

			}


            $("#dvdisper1").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));
			
			BillNowPrefix = m_BillNowPrefix;
			$.ajax({
				type: "POST",
				data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
				url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
				contentType: "application/json",
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);
					var prevmode =  $("#txtRemarks1").val();
                    $("#dvDeliveryChrg1").val(obj.productLists[0]["DeliveryCharges"]);
                    var billmode = obj.productLists[0]["BillMode"];
					
                    $("#txtcustomer1").val(obj.productLists[0]["CustomerName"]);
					
                    $("#txtcst").val(obj.productLists[0]["CSTName"]);
					
                    $("#txtRemarks1").val(billmode);
                    //alert(obj.productLists[0]["CSTName"]);
					
					//var billoption = obj.productLists[0]["Remarks"];


                    //                    var tableno = obj.productLists[0]["Tableno"] ;
                    //                    var Empcode = obj.productLists[0]["EmpCode"] ;
					//                    $("#<%=ddloption.ClientID%> option[value='" + billoption + "']").prop("selected", true);
					//                    if(billoption == "Dine")
					//                    {
					//                      $("#ddlTable option[value='" + tableno + "']").prop("selected", true);  
					//                    }
					//                     if(billoption == "HomeDelivery")
					//                    {
					//                      $("#ddlEmployees option[value='" + Empcode + "']").prop("selected", true);  
					//                    }


					for (var i = 0; i < obj.productLists.length; i++) {
						addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["CursorOn"], 0, 0, obj.productLists[i]["TaxType"]);


					}

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {
					
					$("#dvProductList1").show();
					
					$.uiUnlock();
				}

			});


           

			
		}


        function EditBill(BillNowPrefix) {


            BillNowPrefix = m_BillNowPrefix;
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

					
					
					
                    $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);
                    $("#txtRemarks").val(obj.productLists[0]["BillRemarks"]);

                    var billoption = obj.productLists[0]["Remarks"];


                    //                    var tableno = obj.productLists[0]["Tableno"] ;
                    //                    var Empcode = obj.productLists[0]["EmpCode"] ;
                    //                    $("#<%=ddloption.ClientID%> option[value='" + billoption + "']").prop("selected", true);  
                    //                    if(billoption == "Dine")
                    //                    {
                    //                      $("#ddlTable option[value='" + tableno + "']").prop("selected", true);  
                    //                    }
                    //                     if(billoption == "HomeDelivery")
                    //                    {
                    //                      $("#ddlEmployees option[value='" + Empcode + "']").prop("selected", true);  
                    //                    }


                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"], obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["CursorOn"], 0, 0, obj.productLists[i]["TaxType"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


        function GenerateBill(OrderNo) {
            RestControls();
            OrderId = OrderNo;

            $.ajax({
                type: "POST",
                data: '{ "OrderNo": "' + OrderNo + '"}',
                url: "screen.aspx/GetByOrderNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["ItemDiscount"], 1, obj.productLists[i]["Edit_SaleRate"], obj.productLists[i]["Cursor_On"],0,0);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


        function BackToList() {
            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();

        }




        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");
            var txtOnlineAmount = $("#txtpaymentAmount");
            var txtCoupanAmount = $("#Coupans");
            var txtCODAmount = $("#txtCODAmount");
            var txtCredit = $("#txtCreditAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }

            else if ((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCreditCard.val(0);
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }
            else if ((Number(txtOnlineAmount.val()) + Number(txtCoupanAmount.val())) >= Number(txtFinalBillAmount.val())) {
                txtCoupanAmount.val(Number(txtFinalBillAmount.val()) - Number(txtOnlineAmount.val()));
                txtCheque.val(0);

            }


            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) + Number(txtCoupanAmount.val()) + Number(txtCODAmount.val()) + Number(txtCredit.val()) - Number(txtFinalBillAmount.val())));
            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }




        //...................................

        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;
        var KKCTAmt = 0;
        var SBCTAmt = 0;

        function bindGrid2() {

            var searchon = $("input[name='searchon1']:checked").val();
            var criteria = $("input[name='searchcriteria1']:checked").val();
            var stext = $("#Txtsrchcredit").val();


            jQuery("#jQGridDemoCredit").GridUnload();

            jQuery("#jQGridDemoCredit").jqGrid({
                url: 'handlers/CreditCustomerSearch.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '&cType=' + "Customer",
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['Code', 'Name', 'CSTNO', 'TINNO'],
                colModel: [
                            { name: 'CCODE', key: true, index: 'CCODE', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                            { name: 'CNAME', index: 'CNAME', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                            { name: 'CST_NO', index: 'CST_NO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                            { name: 'TINNO', index: 'TINNO', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true } },




                ],
                rowNum: 10,
                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPagerCredit',
                sortname: 'CCODE',
                viewrecords: true,
                height: "100%",
                width: "800px",
                sortorder: 'desc',
                caption: "Customers List"


            });


            $('#jQGridDemoCredit').jqGrid('navGrid', '#jQGridDemoPagerCredit',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }

                         );

            var Datad = jQuery('#jQGridDemoCredit');
            Datad.jqGrid('setGridWidth', '240');

            $("#jQGridDemoCredit").jqGrid('setGridParam',
             {
                 onSelectRow: function (rowid, iRow, iCol, e) {

                     if (billingmode == "direct") {
                         $("#btnOk").css("display", "block");
                         $("#btnCancel").css("display", "block");
                     }
                     else {
                         $("#CustomerSearchWindow").hide();
                         $("#dvProductList").hide();
                         $("#dvBillWindow").show();
                         $("#dvCreditCustomerSearch").hide();
                         $("#dvHoldList").hide();
                         $("#dvOrderList").hide();
                         var customerId = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CCODE');
                         CrdCustSelId = customerId;
                         $("#hdnCreditCustomerId").val(customerId);
                         $("#lblCreditCustomerName").text($('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME'));
                         CrdCustSelName = $('#jQGridDemoCredit').jqGrid('getCell', rowid, 'CNAME')
                         //$('#dvCreditCustomerSearch').dialog('close');
                         $("#creditCustomer").css("display", "block");
                         $("#ddlbilltype option[value='Credit']").prop("selected", true);

                     }



                 }
             });

        }




        function bindGrid(searchon, mobile) {


            //var searchon=$("input[name='searchon']:checked").val();
            var criteria = $("input[name='searchcriteria']:checked").val();
            var stext = $("#txtSearch1").val();
            if (searchon == "M") {
                stext = mobile;

            }


            jQuery("#jQGridDemo").GridUnload();


            jQuery("#jQGridDemo").jqGrid({
                url: 'handlers/CustomersList.ashx?searchon=' + searchon + '&criteria=' + criteria + '&stext=' + stext + '',
                ajaxGridOptions: { contentType: "application/json" },
                datatype: "json",

                colNames: ['ID', 'Name', 'Address1', 'Address2', 'Area', 'City', 'State', 'DateOfBirth', 'AnniversaryDate', 'Discount', 'ContactNo', 'Tag', 'FocBill', 'Group'],
                colModel: [
                            { name: 'Customer_ID', key: true, index: 'Customer_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                            { name: 'Customer_Name', index: 'Customer_Name', width: 100, stype: 'text', sorttype: 'int', hidden: false, editable: false },

                            { name: 'Address_1', index: 'Address_1', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: false },
                             { name: 'Address_2', index: 'Address_2', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Area_ID', index: 'Area_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },

                             { name: 'City_ID', index: 'City_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'State_ID', index: 'State_ID', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Date_Of_Birth', index: 'Date_Of_Birth', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Date_Anniversary', index: 'Date_Anniversary', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                             { name: 'Discount', index: 'Discount', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                             { name: 'Contact_No', index: 'Contact_No', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },

                            { name: 'Tag', index: 'Tag', width: 150, stype: 'text', sortable: true, editable: true, editrules: { required: true }, hidden: true },
                            { name: 'FocBill', index: 'FocBill', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                           { name: 'grpid', index: 'grpid', width: 150, stype: 'text', hidden: true, sortable: true, editable: true, editrules: { required: true } },
                ],
                rowNum: 10,
                mtype: 'GET',
                loadonce: true,
                rowList: [10, 20, 30],
                pager: '#jQGridDemoPager',
                sortname: 'Code',
                viewrecords: true,
                height: "100%",
                width: "800px",
                sortorder: 'desc',
                caption: "Customers List"


            });


            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                       {
                           refresh: false,
                           edit: false,
                           add: false,
                           del: false,
                           search: true,
                           searchtext: "Search",
                           addtext: "Add",
                       },

                       {//SEARCH
                           closeOnEscape: true

                       }
                         );

            var Datad = jQuery('#jQGridDemo');
            Datad.jqGrid('setGridWidth', '290');



            $("#jQGridDemo").jqGrid('setGridParam',
                {
                    onSelectRow: function (rowid, iRow, iCol, e) {


                        var Discount = $('#jQGridDemo').jqGrid('getCell', rowid, 'Discount');
                        $("#lblCashCustomerName").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));

                        CshCustSelId = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_ID');
                        CshCustSelName = $('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name');
                        //                 $("#lblCashCustomerAddress").text($('#jQGridDemo').jqGrid('getCell', rowid, 'Customer_Name'));
                        // $('#dvSearch').dialog('close');
                        $("#CashCustomer").css("display", "block");

                        if (EnableCustomerDiscount == 1) {
                            $("#dvdisper").val(Discount.toFixed(2));

                        }

                        $("#CustomerSearchWindow").hide();
                        $("#dvProductList").show();
                        $("#dvBillWindow").hide();
                        $("#dvHoldList").hide();
                        $("#dvOrderList").hide();
                        CommonCalculation();
                    }
                });




        }

        function RestControls() {

            //if (DefaultService == "Dine") {
            //    $("#ddlTable").removeAttr("disabled");
            //    $("#ddlTable").show();
            //    $("#ddlEmployees").hide();
            //}
            FlagPaymode = 0;
            $("#ddlTable").prop("disabled", true);
            $("#ddlEmployees").hide();
            $("#ddlEmployees").val(0);
            ExistedKot = 0;
            $("#ddlTable").val(0);
            option = DefaultService;
            $("#<%=ddloption.ClientID%> option[value='" + DefaultService + "']").prop("selected", true);

            $("#dv_lblcustmobno").hide();
            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            $("#lblCreditCustomerName").text("");
            $("#ddlEmployees").val(0);
            $("ddlCreditCustomers").hide();
          //  $("ddlChosseCredit").hide();
          //$("#btnAddCreditCustomer").css("display", "none");
			$("#ddlTable1").val(0);
          $("#ddlTable").val(0);
            $("ddlChosseCredit").val(0);
            $("#OTPVal").val("");
            $('#<%=dd_customername.ClientID %>').removeAttr("disabled");
            BillNowPrefix1 = "";
            Type = "";
            $("#CashCustomer").css("display", "none");
            ProductCollection = [];
			$('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            $("#dvdisper").val(0);
            $("#dvdiscount").val(0);
            $("#dvnetAmount").html("0");
            $("#dvRound").html("0");
            $("#dvTax").html("0");
            $("#dvVat").html("0");
            $("#dvDeliveryChrg").val("0");
            $("#dvsertaxper").html("0");
            $("#dvKKCPer").html("0");
            $("#dvKKCAmt").html("0");
            $("#dvSBCPer").html("0");
            $("#dvSBCAmt").html("0");
            $("#dvsbtotal").html("0");
            $("#dvsbtotal1").html("0");
            $("#dvnetAmount1").html("0");
			$("#dvdisper1").val(0);
			$("#dvdiscount1").val(0);
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;


            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#txtCreditAmount").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            KotAmt = 0;
            $("#ddlTable option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvProductList").show();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            $("#txtRemarks").val("");
            $("#txtCashReceived").val("0").prop("readonly", false);
            CrdCustSelId = 0;
            CshCustSelId = 0;
            CshCustSelName = "";
            CrdCustSelName = "";
            $("#txtpaymentAmount").val("0");
            $("#Coupans").val("0");
            $("#ddlOtherPayment").val("0");
            //$("#ddlOtherPayment").hide();
            //$("#rdoCOD").hide();
            //$("#rdoOnline").hide();
            //$("#Coupans").hide();
            //$("#txtpaymentAmount").hide();
            //$("#OP").hide();
            $("#coupan").hide();
            $("#holder input").hide();
            $("#holder input").val("");
            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            $("#btnEdit").css({ "display": "none" });
            $("#btnShowScreen").css({ "display": "none" });

            $("#btnDelete").css({ "display": "none" });

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "9") {

                    $("#btnShowScreen").css({ "display": "block" });
                }

                if (arrRole[i] == "3") {

                    $("#btnEdit").css({ "display": "block" });
                }

                if (arrRole[i] == "2") {

                    $("#btnDelete").css({ "display": "block" });
                }

               
            }
            $("#No").hide();
            $("#Amt").hide();
            $("#rdoOnline").prop('checked');
            $("#rdoCOD").hide();
            $("#rdoOnline").hide();
            $("#txtCODAmount").val("0").prop("readonly", false);
            $("#Coupans").val("0").prop("readonly", false);
            $("#txtpaymentAmount").val("0").prop("readonly", false);
            $("#holder").val("");
            $("#holder2").val("");
            $("#holder").hide()
            $("#holder2").hide()
            $("#Coupans").hide();
            $("#coupan").hide();
            //$("#OP").hide();
            $("#txtCoupan").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#COD").hide();
            $("#Online").hide();

            if (StartCursor == "ItemName") {

                $("#txtSearch").focus();

            }
            else {


                $("#txtItemCode").focus();

            }
        }
        $(document).ready(function () {
            $('#<%=dd_customername.ClientID %>').change(function () {

                var CheckSelectVal = $('#<%=dd_customername.ClientID %>').val();
                if (CheckSelectVal > 0) {
                    $('#tdCash').css('display', 'none');
                    $('#tdCreaditCard').css('display', 'none');
                    $('#tdsavebill').css('display', 'none');
                    $("#OnlinePaymode").removeAttr('style');

                }
                else {
                    $('#tdCash').removeAttr('style');
                    $('#tdCreaditCard').removeAttr('style');
                    $('#tdsavebill').removeAttr('style');
                    $("#OnlinePaymode").removeAttr('style');
                    $("#OnlinePaymode").attr('style', 'display:none');

                }
            })
        })

        function InsertHoldBill() {

            if (m_BillMode != "") {
                alert("Sorry, You cannot Hold Bill in Edit Mode");
                return;
            }


            var custcode = 0;
            var custName = "";
            if (CshCustSelId == "0") {
                custcode = 0;
                custName = "CASH";

            }
            else {
                custcode = CshCustSelId;
                custName = CshCustSelName;

            }


            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();
            var addtaxamt = $("#dvVat").html();
            var NetAmt = 0;
            if (con_getcomboprice == 'COMBO') {
                NetAmt = comborate;
            }
            else {
                NetAmt = $("#dvnetAmount").html();
            }

            var billmode = "0";

            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var PAmt = [];
            var Tax = [];

            var Ptax = [];
            var ItemRemarks = [];

            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {

                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Price[i] = ProductCollection[i]["Price"];
                Qty[i] = ProductCollection[i]["Qty"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                //ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                ItemRemarks[i] = ProductCollection[i]["Tax_type"];
            }


            $.ajax({
                type: "POST",
                data: '{ "CustomerId": "' + custcode + '","CustomerName": "' + custName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","ItemRemarksArr": "' + ItemRemarks + '"}',
                url: "screen.aspx/InsertHoldBill",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {
                        alert("Bill Holded Successfully");
                        RestControls();
                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }

        function InsertOnlineOtherPayment_pm(billno) {

            var Id = 0;
            var OtherPayment_ID = $("#ddlOtherPayment_pm").val();
            //var OtherPayment_ID = OtherPayment_ID;

            var BillNo = billno;
            var CashAmt = $("#txtCODAmount_pm").val();
            if (CashAmt == 0) {
                CashAmt = 0;
            }
            if (CashAmt > 0) {
                Billmode = "COD"
            }
            else {
                Billmode = "OnlinePayment"
            }


            var OnlineAmt = $("#txtpaymentAmount_pm").val();
            //var Coupan= Coupan;
            if (onlineoption == "COD") {
                var Coupan = [];
                var CoupanAmt = [];
            }
            else {

                var Coupan = [];
                var CoupanAmt = [];

                for (var i = 0; i < $("#holder_pm input").length; i++) {
                    if ($("#txtCoupan_pm" + i).val() > "0") {
                        Coupan[i] = $("#txtCoupan_pm" + i).val();
                    }
                }
                // var Coupan= $("#holder input").val();

                for (var i = 0; i < $("#holder2_pm input").length; i++) {
                    if ($("#txtCoupanAmt_pm" + i).val() > "0") {
                        CoupanAmt[i] = $("#txtCoupanAmt_pm" + i).val();
                    }
                }
            }

            $.ajax({

                type: "POST",
                data: '{ "ID": "' + Id + '","OtherPayment_ID": "' + OtherPayment_ID + '","Bill_No": "' + BillNo + '","CoupanNo": "' + Coupan + '","CoupanAmt": "' + CoupanAmt + '","Mode": "' + Billmode + '","CashAmt": "' + CashAmt + '","OnlineAmt": "' + OnlineAmt + '"}',
                url: "BillScreen.aspx/InsertOnlineOtherPayment",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }





        function InsertOnlineOtherPayment(billno) {

            var Id = 0;
            var OtherPayment_ID = $("#ddlOtherPayment").val();
            //var OtherPayment_ID = OtherPayment_ID;
            var OTPVal = $("#OTPVal").val();
            var BillNo = billno;
            var CashAmt = $("#txtCODAmount").val();
            if (CashAmt == 0) {
                CashAmt = 0;
            }
            if (CashAmt > 0) {
                Billmode = "COD"
            }
            else {
                Billmode = "OnlinePayment"
            }


            var OnlineAmt = $("#txtpaymentAmount").val();
            //var Coupan= Coupan;
            if (onlineoption == "COD") {
                var Coupan = [];
                var CoupanAmt = [];
            }
            else {

                var Coupan = [];
                var CoupanAmt = [];

                for (var i = 0; i < $("#holder input").length; i++) {
                    if ($("#txtCoupan" + i).val() > "0") {
                        Coupan[i] = $("#txtCoupan" + i).val();
                    }
                }
                // var Coupan= $("#holder input").val();

                for (var i = 0; i < $("#holder2 input").length; i++) {
                    if ($("#txtCoupanAmt" + i).val() > "0") {
                        CoupanAmt[i] = $("#txtCoupanAmt" + i).val();
                    }
                }
            }

            $.ajax({

                type: "POST",
                data: '{ "ID": "' + Id + '","OtherPayment_ID": "' + OtherPayment_ID + '","Bill_No": "' + BillNo + '","CoupanNo": "' + Coupan + '","CoupanAmt": "' + CoupanAmt + '","Mode": "' + Billmode + '","CashAmt": "' + CashAmt + '","OnlineAmt": "' + OnlineAmt + '","OTPVal": "' + OTPVal + '"}',
                url: "BillScreen.aspx/InsertOnlineOtherPayment",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -12) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }
                    $("#codChk").prop('checked', false);
                    $("#OnlineChk").prop('checked', false);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    RestControls();
                    $.uiUnlock();
                }

            });

        }



        function AddRemarks(counterId) {

            var DRemarks = $("#txtRemarks" + counterId).val();
            ProductCollection[counterId]["ItemRemarks"] = DRemarks;


        }




        function InsertKOT() {

            var Tableno = $("#ddlKotTable").val();
            if (Tableno == "") {
                alert("Choose table No");
                $("#ddlKotTable").focus();
                return;
            }
           
            var Empcode = $("#ddlKotSteward").val();
            if (Empcode == "") {
                alert("Choose Employee");
                $("#ddlKotSteward").focus();
                return;
            }


            var PaxNo = $("#ddlPax").val();
            var Value = $("#dvsbtotal").html();

            var DisPercentage = $("#dvdisper").val();

            var DisAmount = $("#dvdiscount").val();

            var TaxAmount = $("#dvVat").html();

            var TotalAmount = $("#hdnnetamt").val();
            var ServiceCharges = $("#dvTax").html();



            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];

            var PAmt = [];
            var Ptax = [];

            if (ProductCollection.length == 0) {
                alert("Please first Select Products");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {

                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];

            }

            $.uiLock();
            //         TableID, PaxNo, Value, DisPercentage, DisAmount, ServiceCharges, TaxAmount, TotalAmount, EmpCode
            $.ajax({
                type: "POST",
                data: '{ "TableID": "' + Tableno + '","PaxNo": "' + PaxNo + '","Value": "' + Value + '","DisPercentage": "' + DisPercentage + '","DisAmount": "' + DisAmount + '","ServiceCharges": "' + DisAmount + '","TaxAmount": "' + TaxAmount + '","TotalAmount": "' + TotalAmount + '","EmpCode": "' + Empcode + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '"}',
                url: "BillScreen.aspx/InsertKOT",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == -5) {
                        alert("Please Login Again and Try Again..");
                        return;
                    }

                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                    $.uiUnlock();


                }

            });

        }

        function TenderAmount() {

            var NetAmount = parseFloat($("#dvnetAmount").html());
            $("#TotalNetAmount").val(NetAmount);
            $("#TenderAmounts").dialog({
                autoOpen: true,
                top: 300,
                width: 356,
                resizable: false,
                modal: false,
                open: function (event, ui) {
                    $('.ui-dialog').css('z-index', 100);
                    $('.ui-widget-overlay').css('z-index', 100);
                },
            });

            //$('#TenderAmount').val("");
            $(function () {
                $("#txtDate").focus();
            });

            //$("#TenderAmounts").attr('style', 'margin-top:261px!important');
        }
        $("#TenderAmount").keyup(function () {
            var TenderVal = parseFloat($("#TenderAmount").val());
            var TotalNetAmountval = parseFloat($("#TotalNetAmount").val());
            var MinusTenderAmount = TenderVal - TotalNetAmountval;
            $("#TotalTenderAmount").html(MinusTenderAmount);
        });

        function Printt(celValue, DeptValue, PType) {


            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);

            document.getElementById('reportout').contentWindow.location = "Reports/rptRetailBill.aspx?BillNowPrefix=" + celValue;


        }



        function Printtkot(celValue, DeptValue, PType, max, i) {


            // $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            // var iframe = document.getElementById('reportkot');
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "reportkot" + i);
            iframe.style.width = 0 + "px";
            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
            document.getElementById('reportkot' + i).contentWindow.location = "Reports/rptKOTPrint.aspx?BillNowPrefix=" + celValue + "&DepartmentName=" + DeptValue;

            if (max == i + 1) {
                //DeletePrinter(celValue);
            }


        }



        function DeletePrinter(m_BillNowPrefix) {

            $.ajax({
                type: "POST",
                data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
                url: "BillScreen.aspx/DeletePrinter",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }




        function DeletePrinterUserWise() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "BillScreen.aspx/DeletePrinterUserWise",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


        function BindCreditCst() {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "BillScreen.aspx/BindCreditCst",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlChosseCredit").html(obj.Options);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        }



        function kotPrint(BillNowPrefix) {

            $.ajax({
                type: "POST",
                data: '{}',
                url: "BillScreen.aspx/KOTprint",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i <= obj.kotprint.length; i++) {

                        // Printtkot(BillNowPrefix,obj.kotprint[i]["DepartmentName"],'Kot',obj.kotprint.length,i); 

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                }

            });
        }




        function InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment)
        {
            var cst_id = 0;
            if (MachineNo == 0) {

                MachineNo = 1;
                var OrderNo = OrderId;
                if (OrderNo == "") {
                    OrderNo = "0";
                }
                if ($("#ddlOtherPayment").val() != 0) {

                    cst_id = $("#ddlOtherPayment").val();


                }
                else {

                    cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();

                }
           
                var BIllValue = $("#dvsbtotal").html();

                var DisPer = $("#dvdisper").val();

                var lessdisamt = $("#dvdiscount").val();

                var addtaxamt = (Number($("#dvVat").html())).toFixed(2);
                
                var RoundAmt = 0;
                var hdnNetamt = $("#hdnnetamt").val();
                var Order_No = $("#txtorderno").val();
                RoundAmt = (Number(NetAmt) - Number(hdnNetamt)).toFixed(2);
				
                var DeliveryCharges = 0;
                var EmpCode = 0;
                if (Order_No == "") {

                    Order_No = 0;



                }



                if (option == "HomeDelivery") {

                    DeliveryCharges = $("#dvDeliveryChrg").val();
                    EmpCode = $("#ddlEmployees").val();
                    //if (cashcustcode == " " || cashcustcode == "0") {
                    //    alert("Please enter customer name/ mobile number");
                    //  //$.uiUnlock();

                    //}

                }

                var remarks = option;

                var Tableno = $("#ddlTable").val();
                if (Tableno == "") {
                    Tableno = "0";
                }

                if (option == "Dine") {


                    if (Tableno == "0") {
                        alert("Please choose Table Number");
                        $("#ddlTable").focus();
                        $.uiUnlock();
                        return;
                    }
                }

                var setatx = $("#dvTax").html();
                var ServiceTax = $("#dvsertaxper").html();
                var KKCPer = $("#dvKKCPer").html();
                var KKCAmt = $("#dvKKCAmt").html();
                var SBCPer = $("#dvSBCPer").html();
                var SBCAmt = $("#dvSBCAmt").html();
                var BillRemarks = $("#txtRemarks").val();


                var ItemCode = [];
                var Price = [];
                var Qty = [];
                var Tax = [];
                var OrgSaleRate = [];
                var PAmt = [];
                var Ptax = [];
                var PSurChrg = [];
                var ItemRemarks = [];
                var SurPer = [];
      
                if (ProductCollection.length == 0) {
                    alert("Please first Select ProductsFor Billing");

                    return;
                }

                for (var i = 0; i < ProductCollection.length; i++) {

                    ItemCode[i] = ProductCollection[i]["ItemCode"]; 
                    Qty[i] = ProductCollection[i]["Qty"];
                    Price[i] = (Number(ProductCollection[i]["Price"])).toFixed(2);
					Tax[i] = (Number(ProductCollection[i]["TaxCode"])).toFixed(2);
					OrgSaleRate[i] = (Number(ProductCollection[i]["Price"])).toFixed(2);
					PAmt[i] = (Number(ProductCollection[i]["ProductAmt"])).toFixed(2);
					Ptax[i] = (Number(ProductCollection[i]["Producttax"])).toFixed(2);
                    PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                    //ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                    ItemRemarks[i] = ProductCollection[i]["Tax_type"];
					SurPer[i] = (Number(ProductCollection[i]["SurVal"])).toFixed(2);
                 

                }
                
                TaxDen = [];
                VatAmtDen = [];
                VatDen = [];
                SurDen = [];

                $("div[name='tax']").each(
               function (y) {
                   TaxDen[y] = $(this).html();
               }
               );
                $("div[name='amt']").each(
               function (z) {
                   VatAmtDen[z] = $(this).html();
               }
               );
                $("div[name='vat']").each(
               function (a) {
                   VatDen[a] = $(this).html();
               }
               );
                $("div[name='sur']").each(
               function (a) {
                   SurDen[a] = $(this).html();
               }
               );
                var BillNowPrefix = "";
				var BillDate = $("#txtBillDate").val();

                $.uiLock();

                $.ajax({
                    type: "POST",
                    data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '","cst_id": "' + cst_id + '","Order_No": "' + Order_No + '","BillDate": "' + BillDate + '"}',
					url: "VatBilling.aspx/InsertUpdate",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        
                        var obj = jQuery.parseJSON(msg.d);
                        BillNowPrefix = obj.BNF;
                         if (BillOption == "Direct") {

                          
                         var iframe = document.getElementById('reportout');

                         //                if (iframe != null) {
                         //                    document.body.removeChild(iframe);
                         //                } 

                         iframe = document.createElement("iframe");
                         iframe.setAttribute("id", "reportout");
                         iframe.style.width = 0 + "px";
                         iframe.style.height = 0 + "px";
                         document.body.appendChild(iframe);

                         window.location = 'GSTBillReport.aspx?BillNowPrefix=' + BillNowPrefix;

                     }
                        if (obj.Status == -11) {
                            alert("You don't have permission to perform this action..Consult Admin Department.");
                            return;
                        }


                        if (obj.Status == -5) {
                            alert("Please Login Again and Try Again..");
                            return;
                        }

                        if (obj.Status == 0) {
                            alert("An Error Occured. Please try again Later");
                            return;

                        }


                        else {
                            $("#lblAmmmt").html(NetAmt);
                            //   $("#dvbillSave").toggle();
                            //  Printt(BillNowPrefix,'','Bill');
                            $.uiUnlock();

                            GetLastBill();
                            BindGrid();
                            count = 0;

                        }

                        RestControls(); 
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        $("#btnBillWindowOk").removeAttr('disabled');
                        if (billmode == "OnlinePayment") {

                           // InsertOnlineOtherPayment(BillNowPrefix);


                        }
                        //kotPrint(BillNowPrefix);
                        var netamt = parseFloat($("#dvnetAmount").html())
                        if (netamt != 0) {
                            //TenderAmount();
                        }
                        RestControls();
                        $.uiUnlock();
                        MachineNo = 0;
                       

                    }

                });
            }

        }


        var option = "";



        function GetByItemCode(div, ItemCode) {

			if (CshCustSelName == "") {

				alert("Choose Party First.");
				return;
            }
           
            if (option == "HomeDelivery") {

                //if (CshCustSelId == "0" || CshCustSelId == " ") {
                //  alert("Please enter customer name/ mobile number");
                //  //$.uiUnlock();
                //  return false;
                //}

            }
            if ($(div).children().children().eq(6).text().trim() == "COMBO".trim()) {
                for (var i = 0; i < ProductCollection.length; i++) {
                    var ExistCode = ProductCollection[i].ItemCode;
                    if (ItemCode == ExistCode) {
                        alert("This combo already exist!")
                        return false;
                    }
                }
            }
            else if ($(div).children().children().eq(6).text().trim() == "QTY_COMBO".trim()) {
                for (var i = 0; i < ProductCollection.length; i++) {
                    var ExistCode = ProductCollection[i].ItemCode;
                    if (ItemCode == ExistCode) {
                        alert("This combo already exist!")
                        return false;
                    }
                }
            }
            var CheckComboExistVal = true;
            var buttontext = $(div).children().find('span').eq(3).text();
            var comboname = $(div).children().find('span').eq(0).text();
            var tablerow = $("#tbProductInfo").find("tr").length;
            getcomboprice = $(div).children().find('span').eq(1).text();
            getcomboid = $(div).children().find('span').eq(2).text();
            itemnametype = $(div).children().find('span').eq(3).text();


            //alert(itemnametype);
            if ($('#getcombodetail').css('display') == 'block') {
                var Target = $("#tbComboInfo");
                Target.append("<tr style='background-color:darkorange;'><td style=width:30px;text-align:center;font-size:13px;font-weight: 600;>" + getcomboid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + comboname + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td class='subcomboqty" + combosubcnt + "' style='width:25px;text-align:center;font-weight: 600;font-size: 17px;'><input type='text' value='1' style='width: 38px;height: 30px;font-size: 13px;text-align: center;font-weight: 600;' id='txtBillQty'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='text' value=" + getcomboprice + " style='width: 70px;height: 30px;font-size: 13px;text-align: center;font-weight: 600;'></input></td><td  style='width:70px;text-align:center;font-size:17px;font-weight: 600;' class='combosubprice" + combosubcnt + "'><input type='text' value=" + getcomboprice + " style='width: 70px;height: 30px;font-size: 13px;text-align: center;font-weight: 600;'></input></td><td  style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>")
            }

            else if (con_itemnametype != 'COMBO' && con_QtyCombo != "QTY_COMBO") {

                if (itemnametype == 'COMBO') {

                    $("#tbProductInfo tr").css('display', 'none');
                    con_getcomboprice = getcomboprice;
                    con_getcomboid = getcomboid;
                    con_itemnametype = itemnametype;

                }
                else if (itemnametype == "QTY_COMBO") {
                    con_getcomboprice = getcomboprice;
                    con_getcomboid = getcomboid;
                    con_QtyCombo = itemnametype;
                }


                $.uiLock('');

                var rows = $('#tbProductInfo tr').length;
                var billtype = $('#<%=dd_customername.ClientID %> option:selected').val();
                $.ajax({
                    type: "POST",
                    data: '{ "ItemCode": "' + ItemCode + '","billtype": "' + CshCustSelId + '"}',
                    url: "vatbilling.aspx/GetByItemCode",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {
                        var $result = $(msg).find(this);
                        var obj = jQuery.parseJSON(msg.d);


                        if (obj.productData.ItemID == 0) {
                            alert("No Item Found Corresponding to Code " + obj.productData.Item_Code);
                            $("#txtItemCode").val("");

                        }
                        else {

							addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate_Excl, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, "", obj.productData.Discount, 0, obj.productData.Edit_SaleRate, obj.productData.CursorOn, buttontext, 0, obj.productData.TaxType);

                        }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {


                        $.uiUnlock();
                    }

                });
                if (rows > 0) {

                    $('#<%=dd_customername.ClientID %>').attr("disabled", "disabled");


                }

            }

            else if (con_QtyCombo == 'QTY_COMBO') {
                if ($(".SumMainQtyCombo").nextAll().length == 10) {
                    alert("More than 10 item not allow in combo");
                    return false;
                }
                var tr = "<tr class='subqtycombo'  style='background-color:turquoise;'><td style=width:30px;text-align:center;font-size:13px;font-weight: 600;>" + getcomboid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + comboname + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td class='subcomboqty" + combosubcnt + "' style='width:25px;text-align:center;font-weight: 600;font-size: 17px;'><input type='text' Value='1' onkeyup='CalculateComboQty(this)' style='width:41px'></input></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'>" + getcomboprice + "</td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;' class='subQtycombo_amt comboqtysubprice'><input type='text' Value=" + getcomboprice + " style='width:68px'></input></td><td  style='width:50px;text-align:center'><i id='dvComboQtyItemRemove' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                $("#tbProductInfo").append(tr);
                // var sum = 0;
                // sum += parseFloat($(".comboQtyamt").val());
                subcm_Qtyamt = 0;
                $('.subQtycombo_amt').each(function (i, obj) {
                    subcm_Qtyamt += parseFloat($(obj).find('input').val());
                    //alert(subcm_amt);
                })
                //$(".comboQtyamt").val(subcm_Qtyamt);
                //  var SumCombo = $(".comboQtyamt").val();
                var classlnt = $('.SumMainQtyCombo').nextAll().length;

                //var ComboCalculation = subcm_Qtyamt / parseFloat(classlnt);

                var ComboCalculation = subcm_Qtyamt;
                if (ComboCalculation == 'Infinity') {

                    ComboCalculation = subcm_amt / 1;
                }


                if (ComboCalCount == 0) {
                    if ($(".comboQtyamt").val() != "") {
                        $(".comboQtyamt").val(0);
                    }
                    $(".comboQtyamt").val(parseFloat(ComboCalculation).toFixed(2));
                }
                else {
                    $(".comboQtyamt").val(parseFloat(ComboCalculation).toFixed(2));
                }
            }

            else if (con_itemnametype == 'COMBO') {

                //var TableLength = $("#tbProductInfo tr").length
                //var TableLength = $(".SumMainCombo").nextAll("tr").length
                if ($(".SumMainCombo").nextAll().length == 10) {
                    alert("More than 10 item not allow in combo");
                    return false;
                }

                var sum = 0;

                var tr = "<tr class='subcombo'  style='background-color:darkorange;'><td style=width:30px;text-align:center;font-size:13px;font-weight: 600;>" + getcomboid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + comboname + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td class='subcomboqty" + combosubcnt + "' style='width:25px;text-align:center;font-weight: 600;font-size: 17px;'>1</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'>" + getcomboprice + "</td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;' class='subcombo_amt combosubprice" + combosubcnt + "'>" + getcomboprice + "</td><td  style='width:50px;text-align:center'><i id='dvComboItemRemove' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                $("#tbProductInfo").append(tr);
                combosubcnt++;
                var Comboc = combosubcnt - 1;
                sum += parseFloat($(".combosubprice" + Comboc).text());
                // $(".comboamt").val(sum);
                var SumCombo = $(".comboamt").val();
                var classlnt = $('.subcombo').length;


                // subcm_amt += parseInt($(".subcombo_amt").text());
                //console.log(obj);
                $('.subcombo_amt').each(function (i, obj) {
                    subcm_amt += parseFloat($(obj).text());
                    //alert(subcm_amt);
                })


                var ComboCalculation = subcm_amt / parseFloat(classlnt);
                if (ComboCalculation == 'Infinity') {


                    ComboCalculation = subcm_amt / 1;

                }


                if (ComboCalCount == 0) {
                    if ($(".comboamt").val() != "") {
                        $(".comboamt").val(0);
                    }
                    $(".comboamt").val(parseFloat(ComboCalculation).toFixed(2));
                }
                else {
                    $(".comboamt").val(parseFloat(ComboCalculation).toFixed(2))
                }
                // alert(sum);
                ComboCalCount++;
            }
            //else if (con_itemnametype == 'COMBO') {

            //    //var TableLength = $("#tbProductInfo tr").length
            //    //var TableLength = $(".SumMainCombo").nextAll("tr").length
            //    if ($(".SumMainCombo").nextAll().length == 10) {
            //        alert("More than 10 item not allow in combo");
            //        return false;
            //    }

            //    var sum = 0;

            //    var tr = "<tr class='subcombo'  style='background-color:darkorange;'><td style=width:30px;text-align:center;font-size:13px;font-weight: 600;>" + getcomboid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + comboname + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td class='subcomboqty" + combosubcnt + "' style='width:25px;text-align:center;font-weight: 600;font-size: 17px;'>1</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'>" + getcomboprice + "</td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;' class='subcombo_amt combosubprice" + combosubcnt + "'>" + getcomboprice + "</td><td  style='width:50px;text-align:center'><i id='dvComboItemRemove' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
            //    $("#tbProductInfo").append(tr);
            //    combosubcnt++;
            //    var Comboc = combosubcnt - 1;
            //    sum += parseFloat($(".combosubprice" + Comboc).text());
            //    // $(".comboamt").val(sum);
            //    var SumCombo = $(".comboamt").val();
            //    var classlnt = $('.subcombo').length;


            //    // subcm_amt += parseInt($(".subcombo_amt").text());
            //    //console.log(obj);
            //    $('.subcombo_amt').each(function (i, obj) {
            //        subcm_amt += parseFloat($(obj).text());
            //        //alert(subcm_amt);
            //    })


            //    var ComboCalculation = subcm_amt;
            //    if (ComboCalculation == 'Infinity') {


            //        ComboCalculation = subcm_amt / 1;

            //    }


            //    if (ComboCalCount == 0) {
            //        if ($(".comboamt").val() != "") {
            //            $(".comboamt").val(0);
            //        }
            //        $(".comboamt").val(parseFloat(ComboCalculation).toFixed(2));
            //    }
            //    else {
            //        $(".comboamt").val(parseFloat(ComboCalculation).toFixed(2))
            //    }
            //    // alert(sum);
            //    ComboCalCount++;
            //}

        subcm_amt = 0;
        con_getitemcode = $(div).children().find('span').eq(2).text();


        Scrolldown();
        var Amount = 0;
        $("#tbProductInfo tr").each(function (i, obj) {
            if ($(obj).find('input').eq(3).val() == "") {
                Amount += parseFloat($(obj).find('input').eq(2).val());
            }
            else {
                Amount += parseFloat($(obj).find('input').eq(3).val());
            }
        });
        $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
        $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
            //var CheckModelOpen = localStorage.getItem("ComboSaveOpen");
            //if (CheckModelOpen == "Open") {
            //    var Amount = 0;
            //    var TotalAmount = 0;
            //    $("#tbComboInfo tr").each(function (i, obj) {
            //        Amount += parseFloat(($(obj).find('input').eq(2).val()));
            //    });
            //    var divno = $("#tbComboInfo tr").length;
            //    $("#tbProductInfo tr").filter(function (i, item) {
            //        if ($(item).attr("data-item") === $("#tbComboInfo tr").attr("data-item")) {
            //            var DivideAmountbyItem = (parseFloat(Amount) / parseFloat(divno));
            //            $(item).children().eq(6).find('input').val(DivideAmountbyItem)
            //            var ComboWt = $(item).children().eq(3).find('input').val();
            //            var ItemNo = $(item).children().eq(4).find('input').val();
            //            var Price = $(item).children().eq(6).find('input').val();
            //            var FinalWeightRate = ((parseFloat(Price)).toFixed(2) * (parseFloat(ComboWt)) * (parseFloat(ItemNo)).toFixed(2));
            //            $(item).children().eq(7).find('input').val(parseFloat(FinalWeightRate).toFixed(2))
            //        }
            //    });
            //}

    }
    function Scrolldown() {
        setTimeout(function () {
            var elem = document.getElementById('ScrollHgt');
            elem.scrollTop = elem.scrollHeight;
        }, 100);
    }

    $(document).on('click', '#dvComboItemRemove', function () {
        $(this).parent().parent().remove();
        var length = parseFloat($('.subcombo').length);
        subcm_Qtyamt = 0;
        $('.subcombo').each(function (i, obj) {
            subcm_Qtyamt += parseFloat($(obj).find('td').eq(6).text());

            //alert(subcm_amt);
        })
        var divno = parseFloat($("#tbProductInfo tr.subcombo").length);
        var TotalAmount = parseFloat(subcm_Qtyamt) / divno;
        $(".comboamt").val(TotalAmount);
        $(this).parent().parent().remove();
    });
    $(document).on('click', '#dvComboQtyItemRemove', function () {
        $(this).parent().parent().remove();
        var length = parseFloat($('.subqtycombo').length);
        subcm_Qtyamt = 0;
        $('.subQtycombo_amt').each(function (i, obj) {
            subcm_Qtyamt += parseFloat($(obj).find('input').val());
            //alert(subcm_amt);
        })
        var TotalAmount = parseFloat(subcm_Qtyamt)
        $(".comboQtyamt").val(TotalAmount);
    });

    function GetDiscountType() {
        $.ajax({
            type: "POST",
            data: '{}',
            url: "screen.aspx/GetDiscountType",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

                m_DiscountType = obj.Discount.DiscountType;
                if (m_DiscountType == "DisPerAmount") {
                    $("#dvdiscount").removeAttr("disabled");
                    $("#dvdisper").removeAttr("disabled");
                }
                else if (m_DiscountType == "BackEndDiscount") {


                    $("#dvdiscount").prop("disabled", "disabled").val("0");
                    $("#dvdisper").prop("disabled", "disabled").val("0");
                }


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                $.uiUnlock();
            }

        });

    }

    function GetPluginData(Type) {

        if (Type == "CashCustomer") {


            var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
            //$("#lblCashCustomerName").html($("#ddlMobSearchBox option:selected").attr("phone") + "<br>" + $("#ddlMobSearchBox option:selected").attr("address"));

            $("#lblcustmobno").text($("#ddlMobSearchBox option:selected").attr("phone"));

            if( $("#lblcustmobno").text()=="")
            {
            $("#dv_lblcustmobno").hide();
            
            
            }
            else
            {
                     $("#lblcustmobno").css("padding", "0 5px");
      $("#dv_lblcustmobno").show();
            }

            var GSTNo = "";

			
			

			GSTNo = $("#ddlMobSearchBox option:selected").attr("GST");

            
			if (GSTNo == "0") {
				alert("Party should have GST No for GST Billing.");
				return;
			}
            CshCustSelId = $("#ddlMobSearchBox option:selected").val();
            CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

			var CustState = $("#
 option:selected").attr("State");
			if (BranchState == CustState) {
				LocalOut = "Local";
			}
			else {
				LocalOut = "Out"
			}

            $("#CashCustomer").css("display", "block");

            if (EnableCustomerDiscount == 1) {
                $("#dvdisper").val(Discount.toFixed(2));

            }

            $("#CustomerSearchWindow").hide();
            $("#dvProductList").show();
            $("#dvBillWindow").hide();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            CommonCalculation();





        }
        else if (Type == "Accounts") {
            var customerId = $("#ddlCreditCustomers").val();
            CrdCustSelId = customerId;
            $("#hdnCreditCustomerId").val(customerId);


            $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
            CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



            $("#creditCustomer").css("display", "block");
        }
    }


    function GetLastBill() {

        $.ajax({
            type: "POST",
            data: '{ }',
            url: "BillScreen.aspx/GetLastBill",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                $("#lstBillNo").html(obj.lastrecord.BillNowPrefix);
				
                $("#lstBillAmt").html(obj.lastrecord.Net_Amount);
				
                $("#dvShow_pm").attr('dir', obj.lastrecord.BillNowPrefix);

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();

            }

        });

    }

    function Reprints(m_BillNowPrefix) {
        var PrintType = "RetailBill";
        $.ajax({
            type: "POST",
            data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
            url: "BillScreen.aspx/Reprint",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {


                //Printt(m_BillNowPrefix,'','Bill');

                if (Printkot_Reprint == true) {

                    //kotPrint(m_BillNowPrefix);
                }
                else {

                    //DeletePrinter(m_BillNowPrefix);
                }
                $.uiUnlock();

            }

        });
    }

    function GetBranchState() {

        $.ajax({
            type: "POST",
            data: '{ }',
            url: "BillScreen.aspx/GetBranchState",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);
                BranchState = obj.BranchState.StateName;



            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

                $.uiUnlock();

            }

        });

    }





    $(document).ready(
     function () {


        $('#<%=dd_customername.ClientID %>').change(function () {
            PaymentModeID = $('#<%=dd_customername.ClientID %> option:selected').val();
            //$.ajax({
            //    type: "POST",
            //    data: "{'PaymentModeID':" + PaymentModeID + "}",
            //    url: "BillScreen.aspx/BindDropOnlinePayemntmode",
            //    contentType: "application/json",
            //    async: false,
            //    dataType: "json",
            //    success: function (msg) {
            //        if (msg.d.length != 0) {
            //            var Target = $("#ddlOtherPayment");
            //            Target.empty();
            //            $.each(msg.d, function (i, item) {
            //                Target.append("<option value=" + item.OtherPaymentModeID + ">" + item.OtherPaymentName + "</option>")
            //            });
            //        }

            //        //if (PaymentModeID == "10062") {

            //        //    $("#dvdisper").val(10);
            //        //}
            //        getcst_dis();
            //    },
            //});

            $("#ddlOtherPayment").val(PaymentModeID);
        });


        function getcst_dis() {
            PaymentModeID = $('#<%=dd_customername.ClientID %> option:selected').val();
            $.ajax({
                type: "POST",
                data: "{'PaymentModeID':" + PaymentModeID + "}",
                url: "BillScreen.aspx/GetCstDis",
                contentType: "application/json",
                async: false,
                dataType: "json",
                success: function (msg) {
                    if (msg.d.length != 0) {
                        if (PaymentModeID != 0) {
                            $("#dvdisper").val(msg.d);
                            $("#dvdisper").attr('disabled', 'disabled');
                        }
                        else {

                            $("#dvdisper").val(0);
                            $("#dvdisper").removeAttr('disabled');

                        }
                    }
                    else {

                        $("#dvdisper").val(0);
                        $("#dvdisper").removeAttr('disabled');
                    }

                 
                    
                    
                },
            });
        }

         $("#ddlTable").change(
      function () {
          
     
          var Tableval = $("#ddlTable").val();

          GetKotDetail(Tableval);
          KotAmt = 0;
          if (StartCursor == "ItemName") {

              $("#txtSearch").focus();

          }
          else {


              $("#txtItemCode").focus();

          }

      });

         function GetKotDetail(KotNo) {


             $.uiLock('');

             $.ajax({
                 type: "POST",
                 data: '{ "KotNo": "' + KotNo + '"}',
                 url: "manageKotscreen.aspx/GetKotDetail",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     ProductCollection = [];

                     if (obj.productLists.length != "0") {
               
                         for (var i = 0; i < obj.productLists.length; i++) {


                             var takeway = obj.productLists[i].TakeAway;
                             if (takeway == true) {

                                 $('#chktakeway').prop('checked', true);

                             }
                             else {
                                 $('#chktakeway').prop('checked', false);

                             }


                             $("#ddlpax option[value='" + obj.productLists[i]["PaxNo"] + "']").prop("selected", true);
                             $("#ddlEmployees option[value='" + obj.productLists[i]["EmpCode"] + "']").prop("selected", true);
                             //addToList(obj.productData.ItemID, obj.productData.Item_Name, obj.productData.Sale_Rate, obj.productData.Tax_Code, obj.productData.SurVal, obj.productData.Item_Code, obj.productData.Tax_ID, 0, obj.productData.AddOn)
                       
                             addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"],"","0", obj.productLists[i]["Qty"],0,  obj.productLists[i]["CursorOn"],0,1);


                         }
                     }
                     else {
                         if ($("#ddlEmployees").val() == 0)
                         {

                             alert("Please Select Employee First!");
                             $("#ddlTable").val(0);

                         }
                         ProductCollection = [];
                         $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                         //$("#ddlEmployees").val(0);
                         $("#ddlEmployees").focus();
                         ExistedKot = 0;
                     }




                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
                 }

             });





         }



            function InsertUpdateKot() {


        if ($('#ChkComplementary').is(":checked")) {
            var Total = $("#dvsbtotal").html();

            var Balance = $("#lblcombal").html();

            if (Number(Total) > Number(Balance)) {
                alert("Your Balance is too low for this billing");
                return;
            }


        }
        var Countprod = 0;

        for (var i = 0; i < ProductCollection.length; i++) {
            if (ProductCollection[i]["EditVal"] == "0") {
                Countprod = Countprod + 1;
            }
        }


        if (Countprod == "0") {

            alert("No New Item Is Added To KOT");
            $.uiUnlock();
            return;

        }
        else {


            var MKotNo = "1";
            Tableno = $("#ddlTable").val();
            var TakeAway = false;
            if ($('#chktakeway').is(":checked")) {
                TakeAway = true;
            }



            var pax = $("#ddlpax").val();
            if (pax == "") {
                pax = "1";
            }
            var R_Code = "1";
            var M_Code = "1";
           
            var Value = KotAmt;
            var DisPer = 0;
            var DisAmt = 0;
            var ServiceCharges = 0;
            var TaxAmt = 0;
            var TotalAmt = KotAmt;
            var cst_id = 0;


            //           var DisPer = $("#dvdisper").html();   
            //           var DisAmt = $("#dvdiscount").html();  
            //           var ServiceCharges =0;     
            //           var TaxAmt = $("#dvTax").html();
            //           var TotalAmt = $("#dvnetAmount").html(); 



            var Complementary = false;
            if ($('#ChkComplementary').is(":checked")) {
                Complementary = true;
            }
            var Happy = false;
            if ($("#chkhappyhours").prop('checked') == true) {
                Happy = true;
            }

            var Empcode = $("#ddlEmployees").val();

            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];
            var PAmt = [];
            var Ptax = [];
            var AddOn = [];
            var EditVal = [];


            if (ProductCollection.length == 0) {
                alert("Please first Select Products For Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {


                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                AddOn[i] = ProductCollection[i]["Addon"];
                EditVal[i] = ProductCollection[i]["EditVal"];

            }

            if ($('#chkhomedelivery').is(":checked")) {

                subtotal = parseFloat($("#dvsbtotal").text());
                Tableno = -1;
                if (subtotal > minamt) {

                    $("#spn_delcharge").text(0);
                    TotalAmt = TotalAmt + 0;


                }
                else {

                    TotalAmt = TotalAmt + parseFloat($("#spn_delcharge").text());

                }

                //TotalAmt = TotalAmt + parseFloat(minamt);

            }
            else { Tableno = $("#ddlTable").val(); }
            var SendMessageCustomerName = $("#SendMessageCustomerName").val();
            var SendMessagePhoneNumber = $("#SendMessagePhoneNumber").val();
         
            cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();
            //if (SendMessageCustomerName == "")
            //{
            //  alert("Please enter customer name/ phone number");
            //  return false;
            //}

          $.ajax({
              type: "POST",
              data: '{ "MKOTNo": "' + MKotNo + '","TableID": "' + Tableno + '","PaxNo": "' + 0 + '","R_Code": "' + R_Code + '","M_Code": "' + M_Code + '","Value": "' + Value + '","DisPercentage": "' + DisPer + '","DisAmount": "' + DisAmt + '","ServiceCharges": "' + ServiceCharges + '","TaxAmt": "' + TaxAmt + '","TotalAmount": "' + TotalAmt + '","Complementary": "' + Complementary + '","Happy": "' + Happy + '","EmpCode": "' + Empcode + '","itemcodeArr": "' + ItemCode + '","priceArr": "' + Price + '","qtyArr": "' + Qty + '","AmountArr": "' + PAmt + '","taxArr": "' + Tax + '","TaxAmountArr": "' + Ptax + '","AddOnArr": "' + AddOn + '","EditValArr": "' + EditVal + '","TakeAway": "' + TakeAway + '","SendMessageCustomerName":"-","SendMessagePhoneNumber":"0","cst_id": "' + cst_id + '"}',
              url: "manageKotscreen.aspx/InsertUpdate",
              contentType: "application/json",
              dataType: "json",
              async:false,
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);
                  BillNowPrefix = obj.BNF;

                  if (obj.Status == 0) {
                      alert("An Error Occured. Please try again Later");
                      return;

                  }

                  else {
                      //alert("KOT Saved Successfully");
                    // BindSourceTables();
                      //$("#ddlEmployees").val(0);
                      $("#ddlpax").val(0);
                      KotAmt = 0;
                      $('#chktakeway').prop('checked', false);
                   
                  }
                  //.getElementById('ddlTable').focus();
                  //if (StartCursor == "ItemName") {

                  //    $("#txtSearch").focus();

                  //}
                  //else {


                  //    $("#txtItemCode").focus();

                  //}
                  RestControls();
              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {


                  //kotPrint(BillNowPrefix);


                  RestControls();
                  $.uiUnlock();
              }

          });





      }

  }
         $("#btnGSTBill").click(
         function () {

             $("#GSTDialog").dialog({
                 autoOpen: true,
                 width: 400,
                 resizable: false,
                 modal: false
             });
             $("#GSTDialog").parent().removeAttr('style')
             $("#GSTDialog").parent().attr('style', 'width:400px;left:426px;margin-top:192px');
             $.ajax({
                 type: "POST",
                 url: "billscreen.aspx/Keyboard",
                 contentType: "application/json",
                 dataType: "json",

                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function (msg) {
                 }

             });
             $("#chkGst").prop("checked", true);
         }
         );


         function updateGSTBill() {

             LocalOut = "";


             //           if ($("#rdblocal").prop("checked") == true) {
             //             LocalOut = "Local";
             //             }
             //             else
             //             {
             //              LocalOut = "Out";
             //             }

             var BillType = false;
             if ($('#chkGst').prop('checked') == true) {
                 BillType = true;
             }


             var CshCustSelId = 0;
             var CshCustSelName = "";
             var GSTNo = "";

             if ($("#ddlMobSearchBox1 option:selected").text() == "") {

                 alert("First Choose Party. Only then You can convert It to GST Bill.");
                 return;
             }
             else {
                 GSTNo = $("#ddlMobSearchBox1 option:selected").attr("GST");
                 if (GSTNo == "") {
                     alert("Party should have GST No for GST Billing.");
                     return;
                 }

                 CshCustSelId = $("#ddlMobSearchBox1 option:selected").val();
                 CshCustSelName = $("#ddlMobSearchBox1 option:selected").attr("name");
                 var CustState = $("#ddlMobSearchBox1 option:selected").attr("State");
                 if (BranchState == CustState) {
                     LocalOut = "Local";
                 }
                 else {
                     LocalOut = "Out"
                 }

             }


             $.ajax({
                 type: "POST",
                 data: '{"LocalOut": "' + LocalOut + '","BillNowPrefix": "' + m_BillNowPrefix + '","BillType": "' + BillType + '",CustomerId : "' + CshCustSelId + '",CustomerName : "' + CshCustSelName + '"}',
                 url: "BillScreen.aspx/UpdateGSTBill",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     alert("Data Saved Successfully");


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {
                     ResetBillCntrols();
                     m_BillNowPrefix = 0;
                     $("#GSTDialog").dialog('close');
                     BindGrid();

                     $.uiUnlock();

                 }

             });


         }
         $("#btn1").click(
				 function () {
				 	for (var i = 0; i < ProductCollection.length; i++) {
				 		var ItemCode = ProductCollection[i].ItemCode;
				 		if (m_ItemCode == ItemCode) {

				 			ProductCollection = $.grep(ProductCollection, function (value) {
				 				return value != m_ItemCode;
				 			});
				 			// ProductCollection.splice($.inArray(ProductCollection[i], ProductCollection[i]));
				 		}
				 	}
				 	var rows = $('#tbProductInfo tr').length;
				 	// var RowIndex = Number($(this).last('tr').index());
				 	var RowIndex = ((ProductCollection.length) - 1);
				 	var m_oldQty = m_Qty;
				 	m_oldQty = m_Qty + Number(.500);
				 	ProductCollection[RowIndex]["Qty"] = m_oldQty;
				 	Bindtr();

				 });


         $("#btn2").click(
			 function () {

			 	for (var i = 0; i < ProductCollection.length; i++) {
			 		var ItemCode = ProductCollection[i].ItemCode;
			 		if (m_ItemCode == ItemCode) {

			 			ProductCollection = $.grep(ProductCollection, function (value) {
			 				return value != m_ItemCode;
			 			});
			 			// ProductCollection.splice($.inArray(ProductCollection[i], ProductCollection[i]));
			 		}
			 	}
			 	var rows = $('#tbProductInfo tr').length;
			 	// var RowIndex = Number($(this).last('tr').index());
			 	var RowIndex = ((ProductCollection.length) - 1);
			 	var m_oldQty = m_Qty;
			 	m_oldQty = m_Qty + Number(.250);
			 	ProductCollection[RowIndex]["Qty"] = m_oldQty;
			 	Bindtr();


			 });

         $("#btn3").click(
			 function () {


			 	for (var i = 0; i < ProductCollection.length; i++) {
			 		var ItemCode = ProductCollection[i].ItemCode;
			 		if (m_ItemCode == ItemCode) {

			 			ProductCollection = $.grep(ProductCollection, function (value) {
			 				return value != m_ItemCode;
			 			});
			 			// ProductCollection.splice($.inArray(ProductCollection[i], ProductCollection[i]));
			 		}
			 	}
			 	var rows = $('#tbProductInfo tr').length;
			 	// var RowIndex = Number($(this).last('tr').index());
			 	var RowIndex = ((ProductCollection.length) - 1);
			 	var m_oldQty = m_Qty;
			 	m_oldQty = m_Qty - Number(.500);
			 	ProductCollection[RowIndex]["Qty"] = m_oldQty;
			 	Bindtr();


			 });
         $("#btn4").click(
			 function () {


			 	for (var i = 0; i < ProductCollection.length; i++) {
			 		var ItemCode = ProductCollection[i].ItemCode;
			 		if (m_ItemCode == ItemCode) {

			 			ProductCollection = $.grep(ProductCollection, function (value) {
			 				return value != m_ItemCode;
			 			});
			 			// ProductCollection.splice($.inArray(ProductCollection[i], ProductCollection[i]));
			 		}
			 	}
			 	var rows = $('#tbProductInfo tr').length;
			 	// var RowIndex = Number($(this).last('tr').index());
			 	var RowIndex = ((ProductCollection.length) - 1);
			 	var m_oldQty = m_Qty;
			 	m_oldQty = m_Qty - Number(.250);
			 	ProductCollection[RowIndex]["Qty"] = m_oldQty;
			 	Bindtr();


			 });

         $("#btnsa").click(
       function () {


           updateGSTBill();


       }
       );

         $("#Coupans_pm").change(function () {

             // The easiest way is of course to delete all textboxes before adding new ones
             //$("#holder").html("");
             $("#holder_pm input").val("");
             $("#holder2_pm input").val("");
             if ($("#Coupans_pm").val() > 10) {
                 alert("Coupans more than 10 cannot be accepted...");
                 $("#Coupans_pm").focus();
                 $("#Coupans_pm").val(" ");
                 return;
             }
             $("#No_pm").show();
             $("#Amt_pm").show();
             $("#holder2_pm").show();
             $("#holder_pm").show();
             var count = $("#holder_pm input").size();
             var requested = parseInt($("#Coupans_pm").val(), 10);

             if (requested > count) {
                 for (i = count; i < requested; i++) {
                     var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupan_pm' + [i], name: 'text', value: '0' });
                     $("#holder_pm").append($ctrl);
                 }
             }
             else if (requested < count) {
                 var x = requested - 1;
                 $("#holder_pm input:gt(" + x + ")").remove();
             }

             var count1 = $("#holder2_pm input").size();
             var requested = parseInt($("#Coupans_pm").val(), 10);

             if (requested > count1) {
                 for (i = count1; i < requested; i++) {
                     var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupanAmt_pm' + [i], name: 'text', value: '0' });
                     $("#holder2_pm").append($ctrl);
                 }
             }
             else if (requested < count1) {
                 var x = requested - 1;
                 $("#holder2_pm input:gt(" + x + ")").remove();
             }
         });


         $("#btnAddCreditCustomer").click(function () {

             $("#CreditCustomerDialog").dialog({
                 autoOpen: true,

                 width: 800,
                 resizable: false,
                 modal: true
             });



         });


         $("#btncashcst_close").click(function () {

             $("#CreditCustomerDialog").dialog("close");

         })

         $("#Coupans").change(function () {

             // The easiest way is of course to delete all textboxes before adding new ones
             //$("#holder").html("");
             $("#holder input").val("");
             $("#holder2 input").val("");
             if ($("#Coupans").val() > 10) {
                 alert("Coupans more than 10 cannot be accepted...");
                 $("#Coupans").focus();
                 $("#Coupans").val(" ");
                 return;
             }
             $("#No").show();
             $("#Amt").show();
             $("#holder2").show();
             $("#holder").show();
             var count = $("#holder input").size();
             var requested = parseInt($("#Coupans").val(), 10);

             if (requested > count) {
                 for (i = count; i < requested; i++) {
                     var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupan' + [i], name: 'text', value: '0' });
                     $("#holder").append($ctrl);
                 }
             }
             else if (requested < count) {
                 var x = requested - 1;
                 $("#holder input:gt(" + x + ")").remove();
             }

             var count1 = $("#holder2 input").size();
             var requested = parseInt($("#Coupans").val(), 10);

             if (requested > count1) {
                 for (i = count1; i < requested; i++) {
                     var $ctrl = $('<input/>').attr({ type: 'text', id: 'txtCoupanAmt' + [i], name: 'text', value: '0' });
                     $("#holder2").append($ctrl);
                 }
             }
             else if (requested < count1) {
                 var x = requested - 1;
                 $("#holder2 input:gt(" + x + ")").remove();
             }
         });




         GetLastBill();
         //$("#ddlOtherPayment").hide();
         //$("#Coupans").hide();
         //$("#coupan").hide();
         //$("#OP").hide();
         //$("#txtCoupan").hide();
         //$("#txtpaymentAmount").hide();
         //         BindKOTTables();
         //        BindKOTEmployees();
         //      
         //        $("#btnKOTSave").attr('disabled','disabled');

         //         $(document).keydown(function(e) {
         //    // ESCAPE key pressed
         //    if (e.keyCode == 27) {

         //          $("#colProducts").slideUp(200);
         //    }
         //});

         $("#DelCharges").hide();
         $("#btnAddCustomer").click(
         function () {

             $("#CreditCustomerDialog").dialog({
                 autoOpen: true,

                 width: 800,
                 resizable: false,
                 modal: true
             });

         });

         $("#Img2").click(
         function () {

             $("#CustomerDialog").dialog({
                 autoOpen: true,

                 width: 800,
                 resizable: false,
                 modal: true
             });
         }
         );





         $("#ddlChosseCredit").change(
             function () {


                 if ($("#ddlChosseCredit").val() != "0") {


                     CrdCustSelId = $("#ddlChosseCredit").val();
                     $("#hdnCreditCustomerId").val(CrdCustSelId);
                     CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                     $("#lblCreditCustomerName").text($("#ddlChosseCredit option:selected").text())

                     $("#creditCustomer").css("display", "block");

                 }




             });

         var count = 0;

         $(window).keydown(function (e) {
             $.uiLock();

             switch (e.keyCode) {
                 case 112:   // F1 key is left or up
                     count = count + 1

                     if (count == 1 && FlagPaymode==0) {
                        
                         if ($("#tdCash").css('display') == 'none') {

                             $("#OnlinePaymodes").click();
                         }
                         else {

                             $("#btnCash").click();

                         }
                       
                         
                            // CashSave();
                             $.uiUnlock();
                         count = 0;
                     }

                     else if (count == 1 && FlagPaymode == 1) {

                         $("#btnBillWindowOk").click();
                         $.uiUnlock();
                         count = 0;
                     }

                     else if (count == 1 && FlagPaymode == 2) {

                         $("#btnCreditCardBilling").click();
                         $.uiUnlock();
                         count = 0;
                     }
                     else {

                         alert("Bill Is Under Process Already...");
                         $.uiUnlock();

                         return;
                     }
                     $.uiUnlock();
                     return false;
                 case 113: //F2 key is left or down

                     ClearData();
                     $.uiUnlock();
                     return false; //"return false" will avoid further events
                 case 114: //F3 key is left or down
                     ReturnItem();
                     $.uiUnlock();
                     return false; 
                 case 115: //F4 key is left or down
                     
                     $("#btnsavebill").click();
                     $.uiUnlock();
                     return false; 

                 case 117: //F6 key is left or down

                     $("#btnCal").click();
                     $.uiUnlock();
                     return false;

                 case 118: //F7 key is left or down

                     $("#dvShow_pm").click();
                     $.uiUnlock();
                     return false;
                 case 119: //F8 key is left or down

                     CreditCardSave();
                     $.uiUnlock();

                     return false;

                 case 120: //F9 key is left or down

                     window.location.href = 'manageKotscreen.aspx';
                  
                     $.uiUnlock();

                     return false;

                 case 122: //F11 key is left or down
                     BillUnhold();

                     $.uiUnlock();
                     return false;

                 case 123: //F12 key is left or down
                     InsertHoldBill();
                     $.uiUnlock();
                     return false;


             }
             $.uiUnlock();
             return; //using "return" other attached events will execute
         });




         $("#chkDelivery").change(function () {

             //                if($('#chkDelivery').prop('checked') == true)

             //                {
             //                $("#DelCharges").show();
             //                }
             //                else{

             //                $("#DelCharges").hide();
             //                }

         });



         $("#btnMsgClose").click(
        function () {
            $("#dvbillSave").css("display", "none");
        }
        );


       



         //$("#ddlCreditCustomers").val("").removeAttr("disabled");
         //$("#ddlCreditCustomers").css({ "display": "none" });

       //  $("#ddlChosseCredit").css({ "display": "none" });
       //$("#btnAddCreditCustomer").css("display", "none");
         $("#ddlChosseCredit").val(0);

         $.ajax({
             type: "POST",
             data: '{ }',
			 url: "VatBilling.aspx/GetAllBillSetting",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);

                 BillOption = obj.setttingData.BillOption;
                 
				 BillBasicType = obj.setttingData.vat_bill;

                 TakeAwayDine = obj.setttingData.TakeAwayDine;
                 AllowServicetax = obj.setttingData.ServiceTax;
                 StartCursor = obj.setttingData.StartCursor;


                 if (StartCursor == "ItemName") {

                     $("#txtSearch").focus();

                 }
                 else {


                     $("#txtItemCode").focus();

                 }
                 if (AllowServicetax == 1) {
                     Sertax = obj.setttingData.SerTax_Per;
                     AllowKKC = obj.setttingData.AllowKKC;
                     AllowSBC = obj.setttingData.AllowSBC;
                     KKC = obj.setttingData.KKC;
                     SBC = obj.setttingData.SBC;
                     $("#trSBC").show();
                     $("#trKKC").show();
                     $("#trServicetax").show();
                 }
                 else {
                     $("#trServicetax").hide();
                     Sertax = 0;
                     AllowKKC = false;
                     AllowSBC = false;
                     KKC = 0;
                     SBC = 0;
                     $("#trSBC").hide();
                     $("#trKKC").hide();
                 }

                 Takeaway = obj.setttingData.AlloServicetax_TakeAway;
                 HomeDelCharges = obj.setttingData.homedel_charges;

                 minbillvalue = obj.setttingData.min_bill_value;
                 DeliveryCharges = obj.setttingData.del_charges;
                 EnableCashCustomer = obj.setttingData.CashCustomer;
                 DefaultBank = obj.setttingData.defaultBankID;
                 RoundBillAmount = obj.setttingData.roundamt;

                 PrintShortName = obj.setttingData.shortname;

                 FocAffect = obj.setttingData.focaffect;
                 NEgativeStock = obj.setttingData.NegtiveStock;

                 DefaultPaymode = obj.setttingData.defaultpaymodeID;
                 AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;
                 DefaultService = obj.setttingData.DefaultService;
                 NoOfCopy = obj.setttingData.NoOfCopy;
                 PrintTime = obj.setttingData.PrintTime;
                 Printkot_Reprint = obj.setttingData.PrintKot_Reprint;


                 if (DefaultService == "Dine") {
                     $("#ddlTable").removeAttr("disabled");
                 }

                 option = DefaultService;
                 $("#<%=ddloption.ClientID%> option[value='" + DefaultService + "']").prop("selected", true);

                 if (AllowDiscountOnBilling == 1) {
                     EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                     EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                     DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                     BackEndDiscount = obj.setttingData.Back_End_Discount;

                 }


                 if (HomeDelCharges == 1) {

                     if (minbillvalue == 0 || DeliveryCharges == 0) {
                         $("#DelCharges").show();
                         $("#dvDeliveryChrg").removeAttr("disabled");
                         $("#chkDelivery").show();
                         $("#lbldelcharges").show();

                     }
                     else {

                         $("#dvDeliveryChrg").val(DeliveryCharges);
                      //   $("#dvDeliveryChrg").attr("disabled", "disabled");
                         $("#chkDelivery").hide();
                         $("#lbldelcharges").hide();
                         $("#DelCharges").show();

                     }

                 }
                 else {
                     $("#DelCharges").hide();
                     $("#chkDelivery").hide();
                     $("#lbldelcharges").hide();
                     $("#dvDeliveryChrg").val(0);
                 }


                 if (EnableCashCustomer == 1) {

                     $("#btnCash").removeAttr('disabled')
                     //$("#txtMobSearchBox,#imgSrchCashCust").show();
                     $("#txtddlMobSearchBox").removeAttr('disabled');

                 }
                 else {

                     $("#btnCash").prop("disabled", true).css("background", "#f06671");
                     //$("#txtMobSearchBox,#imgSrchCashCust").hide();

                     $("#txtddlMobSearchBox").attr('disabled', 'disabled');
                 }


                 //if (BillBasicType == "I") {

                 //    $("#vatIncOrExc").hide();
                 //}
                 //else {
                 //    $("#vatIncOrExc").show();
                 //}



                 if (DiscountOnBillValue == 1) {

                     DiscountValues = obj.DiscountDetail;



                 }

                 if (EnableDiscountAmount == 1) {

                     $("#dvdisper").removeAttr('disabled');
                     $("#dvdiscount").removeAttr('disabled');
                     $("#dvdisper").val("0");
                     $("#dvdiscount").val("0");
                 }
                 else {

                     $("#dvdisper").attr('disabled', 'disabled');
                     $("#dvdiscount").attr('disabled', 'disabled');
                     $("#dvdisper").val("0");
                     $("#dvdiscount").val("0");
                 }

             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {



             }

         });



         ValidateRoles();

         function ValidateRoles() {

             var arrRole = [];
             arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
             
             for (var i = 0; i < arrRole.length; i++) {

                 if (arrRole[i] == "18") {
                     RoleForEditRate = true;


                 }
                 else if (arrRole[i] == "9") {
                     $("#btnShowScreen").click(
                         function () {


                             FlagEdit = 0;

                             $("#<%=dd_customername.ClientID%>").val(0);
                             $('#<%=dd_customername.ClientID %>').removeAttr("disabled");
                             $("#btnsavebill").text("(F4) Other Pay Mode");
                            
                             $("#btnCash").show();
                             $("#btnCreditCard").show();
                             $("#btnClear").show();
                             $("#btnreturn").show();
                             $("#btnhold").show();
                             $("#btnUnHold").show();
                             $("#dvShow_pm").show();
                             $("#OnEditHide").show();
                             $("#<%=dd_customername.ClientID%>").removeAttr('disabled');
                             $("#<%=ddloption.ClientID %>").removeAttr('disabled');
                             RestControls();
                             m_BillNowPrefix = "";
                             m_BillMode = "";
                             $("#ddlbillttype").removeAttr("disabled");
                             $("#btnCreditCard").css("background", "#f06671");
                             $("#ddlCreditCustomers").html("");
                             $("#txtddlCreditCustomers").val("");
                             $("#btnCash").css("background", "#f06671");
                             $("#btnhold").css("background", "#f06671");
                             $("#screenDialog").dialog({
                                 autoOpen: true,
                                 closeOnEscape: false,
                                 width: 800,
                                 resizable: false,
                                 modal: false
                             });

                             $("#screenDialog").parent().removeAttr('style');
                             $("#screenDialog").parent().attr('style', 'width:100%;top:0px!important;');
                             if (StartCursor == "ItemName") {

                                 $("#txtSearch").focus();

                             }
                             else {


                                 $("#txtItemCode").focus();

                             }
                         }

                     );

                 }
                

                 else if (arrRole[i] == "2") {

                    
                     $("#btnDelete").show();

                     $("#btnDelete").click(
                         function () {

                             var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');

                             if ($.trim(SelectedRow) == "") {
                                 alert("No Bill is selected to Cancel");
                                 return;
                             }

                             var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
                             if (confirm("Are You sure to delete this record")) {
                                 $.uiLock('');


                                 $.ajax({
                                     type: "POST",
                                     data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
									 url: "VatBilling.aspx/Delete",
                                     contentType: "application/json",
                                     dataType: "json",
                                     success: function (msg) {

                                         var obj = jQuery.parseJSON(msg.d);


                                         if (obj.status == -10) {
                                             alert("You don't have permission to perform this action..Consult Admin Department.");
                                             return;
                                         }
                                         else {
                                             BindGrid();
                                             alert("Bill is Canceled successfully.");
                                         }

                                     },
                                     error: function (xhr, ajaxOptions, thrownError) {

                                         var obj = jQuery.parseJSON(xhr.responseText);
                                         alert(obj.Message);
                                     },
                                     complete: function () {
                                         $.uiUnlock();
                                     }
                                 });

                             }


                         }
                     );

                 }




                 else if (arrRole[i] == "3") {
                     $("#btnEdit").show();
                     $("#btnEdit").click(
                         function () {

                             FlagEdit = 1;

                             $('#<%=dd_customername.ClientID %>').attr("disabled", "disabled");
                             $("#btnsavebill").text("Save");
                             
                             $("#btnCash").hide();
                             $("#btnCreditCard").hide();

                             $("#btnClear").hide();
                             $("#btnreturn").hide();
                             $("#btnhold").hide();

                             $("#btnUnHold").hide();
                             $("#dvShow_pm").hide();

                             RestControls();
                             //$("#txtCashReceived").removeAttr('readonly');
                             $("#ddlbillttype").removeAttr('disabled');
                             var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
                             if ($.trim(SelectedRow) == "") {
                                 alert("No Product is selected to add");
                                 return;
                             }
                             CshCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code');
                             CshCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Name');
                             CrdCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_ID');
                             CrdCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name');

                             m_BillMode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillMode');

                             pos_edit = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'posid');


                             $("#btnhold").css("background", "gray");

           <%-- if (pos_edit == 3) {
                $("#<%=dd_customername.ClientID%>").attr('disabled', 'disabled');

                $("#<%=ddloption.ClientID %>").attr('disabled', 'disabled');

            }
            else {

                $("#<%=dd_customername.ClientID%>").removeAttr('disabled');

                $("#<%=ddloption.ClientID %>").removeAttr('disabled');

            }--%>


                             $("#<%=dd_customername.ClientID%>").attr('disabled', 'disabled');

                             $("#<%=ddloption.ClientID %>").attr('disabled', 'disabled');

                             if (CshCustSelId != 0) {

                                 $("#ddlMobSearchBox").html("<option value='" + CshCustSelId + "'>" + CshCustSelName + "</option>");
                                 $("#txtddlMobSearchBox").val(CshCustSelName);
                                 $("#CashCustomer").show();
                                 $("#lblCashCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCustAddress'));
                             }


                             $("#ddlbillttype").prop("disabled", true);


                             if (m_BillMode == "Credit") {

                                 $("#btnCash").css("background", "Black");
                                 $("#btnCreditCard").css("background", "Black");

                                 $("#lblCreditCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CreditCustAddress'));

                             }
                             else if (m_BillMode == "CreditCard") {
                                 $("#btnCash").css("background", "Black");
                                 $("#btnCreditCard").css("background", "Black");

                             }
                             else if (m_BillMode == "Cash") {
                                 $("#btnCash").css("background", "Black");
                                 $("#btnCreditCard").css("background", "Black");
                             }

                             var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');
                             option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');



                             getdel_cust_id = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'cst_id');


                             $("#<%=ddloption.ClientID%>").val(option);
                             $("#<%=dd_customername.ClientID%>").val(getdel_cust_id);
                             if (option == "Dine") {
                                 $("#ddlTable").removeAttr("disabled");

                                 $("#ddlTable option").removeAttr("selected");

                                 $('#ddlTable option[value=' + TableNo + ']').prop('selected', 'selected');
                             }

                             else if (option == "HomeDelivery") {
                                 $("#ddlTable").hide();

                                 $("#ddlEmployees").show();
                                 $("#ddlTable option").removeAttr("selected");
                                 var empcode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'EmpCode');

                                 $('#ddlEmployees option[value=' + empcode + ']').prop('selected', 'selected');

                                 $("#DelCharges").show();
                                 $("#dvDeliveryChrg").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DeliveryCharges'));
                                 //$("#dvDeliveryChrg").attr("disabled", "disabled");

                             }


                             $("#dvdisper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));

							 $("#txtBillDate").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'strBD'));
							




                             EditBill(m_BillNowPrefix);
                             $("#dvsertaxper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'servalue'));

                             $("#screenDialog").dialog({
                                 autoOpen: true,
                                 height: 800,
                                 width: 1000,
                                 resizable: false,
                                 modal: false,
                                 top: 10
                             });

                             //getcst_dis();
                         }

                     );

                 }
                 else if (arrRole[i] == "10") {
                     $("#btnhold").click(
                         function () {

                             InsertHoldBill();
                         });

                 }

                 else if (arrRole[i] == "23") {

                     $("#btnView").click(
                         function () {

                             ProductCollection = [];
                             $('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                             $("#txtRemarks1").val("");
                             $("#txtcst").val("");


                             viewBill(m_BillNowPrefix);


                             $("#screenDialog1").dialog({
                                 autoOpen: true,
                                 height: 1000,
                                 width: 1000,
                                 resizable: false,
                                 modal: false

                             });
                         });


                 }
                 else if (arrRole[i] == "24") {
                    
					 $("#btnReprint").click(
						 function () {
                         
							 var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
							 if ($.trim(SelectedRow) == "") {
								 alert("No Bill is selected to Reprint");
								 return;
							 }

							 m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
							 //window.open('PrintBill.aspx?q=' + m_BillNowPrefix, '_blank');
                            
                           if (BillOption == "Direct") {

                          
                         var iframe = document.getElementById('reportout');

                         //                if (iframe != null) {
                         //                    document.body.removeChild(iframe);
                         //                } 

                         iframe = document.createElement("iframe");
                         iframe.setAttribute("id", "reportout");
                         iframe.style.width = 0 + "px";
                         iframe.style.height = 0 + "px";
                         document.body.appendChild(iframe);

                         window.location = 'GSTBillReport.aspx?BillNowPrefix=' + m_BillNowPrefix;

                     }
                     else {
                         
                           
                           
                             $.uiLock('');
                            
							 var PrintType = "RetailBill";

                             $.ajax({

								 type: "POST",
								 data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
								 url: "VatBilling.aspx/Reprint",
								 contentType: "application/json",
								 dataType: "json",
								 success: function (msg) {
									 var obj = jQuery.parseJSON(msg.d);
								 },
								 error: function (xhr, ajaxOptions, thrownError) {

									 var obj = jQuery.parseJSON(xhr.responseText);
									 alert(obj.Message);
								 },
								 complete: function () {


									 //Printt(m_BillNowPrefix,'','Bill');

									 if (Printkot_Reprint == true) {

										 kotPrint(m_BillNowPrefix);
									 }
									 else {

										 //DeletePrinter(m_BillNowPrefix);
									 }
									 $.uiUnlock();

								 }

							});

                            }
                             





						 }
					 );




                 }



 }

}

			

         $("#ddlTable").prop("disabled", true);
         BindTables();
         BindEmployees();
         GetDiscountType();

         $("#btnGetByItemCode").click(
         function () {


             var ItemCode = $("#txtItemCode");

             if (ItemCode.val().trim() == "") {
                 ItemCode.focus();
                 return;
             }


             GetByItemCode(this, ItemCode.val());
             $("#txtItemCode").val("");
         }

         );




         $("#ddlMobSearchBox").supersearch({
             Type: "CashCustomer",
             Caption: "CustName/Mobile ",
             AccountType: "D",
             Width: 167,
             DefaultValue: 0,
             Godown: 0,
             height: 50
         });


         $("#ddlMobSearchBox1").supersearch({
             Type: "CashCustomer",
             Caption: "CustName/Mobile ",
             AccountType: "D",
             Width: 50,
             DefaultValue: 0,
             Godown: 0
         });



         $("#ddlCreditCustomers").supersearch({
             Type: "CashCustomer",
             Caption: "Please enter Customer Name/Code ",
             AccountType: "D",
             Width: 100,
             DefaultValue: 0,
             Godown: 0
         });


         $("#dvdisper").keyup(function EnterEvent(e) {
             if (e.keyCode == 13) {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }

                 if (value > 100) {
                     $(this).val(100);

                 }



                 BindtrDiscount($(this).val());
                 CommonCalculation();

             }

         });



         $("#dvdiscount").keyup(
       function () {
           var regex = /^[0-9]*$/;


           var value = jQuery.trim($(this).val());
           var ttlAmt = $("#dvsbtotal").html();

           if (value > Number(ttlAmt)) {
               $(this).val(ttlAmt);

           }
           $("#dvdisper").val(($("#dvdiscount").val() * 100 / $("#dvsbtotal").html()).toFixed(2));
           CommonCalculation();

       });



         $("#dvDeliveryChrg").keyup(
        function () {
            //var regex = /^[0-9]*$/;


            //var value = jQuery.trim($(this).val());

            //if (value.length >= 1) {
            //    if (!regex.test(value) || value <= 0) {

            //        $(this).val(0);


            //    }
            //}

            CommonCalculation();

        });



         $("#btnCash").click(
             function () {
                 if (CshCustSelName == "") {

					 alert("Choose Party First.");
					 return;
				 }


           $("#btnCash").attr('disabled', 'disabled');
           var netamt = parseFloat($("#dvnetAmount").html())
           if (netamt != 0) {
               //TenderAmount();
           }
           var cnt = 0;
           cnt = cnt + 1

           if (cnt == 1) {
               if ($("#<%=ddloption.ClientID%>").val() == "Dine") {

                   InsertUpdateKot();
               }
               else
               {
                   CashSave();
               }
           
       




               if (con_itemnametype == 'COMBO') {

                   ComboSave();
               }

               if (con_QtyCombo == 'QTY_COMBO') {

               }
               var ConactBillNum = $("#lstBillNo").text().split('-')
               var Increment = parseFloat(ConactBillNum[1]);
               var BillNumPlusValue = ConactBillNum[0] + "-" + parseFloat(Increment + 1);
              // Reprints(BillNumPlusValue);

               cnt = 0;
           }
           else {
               alert("Bill Is Under Process Already...");
               return;
           }

       });

         function updatecombobill() {




         }
         function ComboQtySave() {
             var n = $(".SumMainQtyCombo").nextAll("tr").length;
             var comboid = con_getcomboid;
             var itemid = 0;
             var itemqty = 0;
             var itemprice = 0;
             var amount = 0;
             var comboprice = 0;
             var count = 1;
             var countamt = 1;
             var stramt = 0;

             var a = Math.floor((Math.random() * 10000) + 1);
             var b = Math.floor((Math.random() * 1000) + 1);
             var c = Math.floor((Math.random() * 10000) + 1);
             var d = Math.floor((Math.random() * 1000) + 1);

             combosesqty_id = a + '' + b + '' + c;
             var divno = n;
             var adj_amt = $(".comboQtyamt").val();
             //comborate = adj_amt / divno;
             comborate = adj_amt;
             var CheckClass = false;
             var RepeatCount = 0;
             for (var i = 0; i < ProductCollection.length; i++) {
                 if (ProductCollection[i].ItemCode == $(".SumMainQtyCombo").children().eq(0).text()) {
                     // ProductCollection.splice(i, 1);
                     ProductCollection.splice($.inArray(i, ProductCollection), 1);
                 }
             }
             var Price = parseFloat($(".SumMainQtyCombo input").eq(0).val()) * (parseFloat($(".SumMainQtyCombo input").eq(2).val()));
             TO = new clsproduct();
             TO.ItemCode = $(".SumMainQtyCombo").children().eq(0).text();
             TO.ItemName = $(".SumMainQtyCombo").children().eq(1).text();
             TO.Qty = $(".SumMainQtyCombo input").eq(0).val();
             TO.Price = parseFloat(Price).toFixed(2);
             TO.EditQtyButton = true;
             TO.ComboQty_Sessionid = combosesqty_id;
             ProductCollection.push(TO);
             var _countqtysave = 0;
             $("#tbProductInfo tr").each(function (i, obj) {
                 if ($(obj).hasClass("SumMainQtyCombo") == true) {
                     CheckClass = true;
                     $(obj).attr('data-item', combosesqty_id)
                 }
                 if (CheckClass == true) {

                     if (RepeatCount == 1) {
                         $(obj).remove();
                     }
                     //itemprice = $("#tbProductInfo").find("tr").eq(i).find("#txtBillQty1").eq(3).val();
                     itemid = $(obj).children().eq(0).text();
                     amount = $(obj).children().eq(6).find('input').val();
                     itemqty = $(obj).children().eq(3).find('input').val();
                     comborate = $(obj).children().eq(5).text()
                     //amount = $("#combosubprice" + count).text();
                     //alert(amount);
                     if (_countqtysave != 0)
                         $.ajax({
                             type: "POST",
                             data: '{ "comboid": "' + comboid + '","itemid": "' + itemid + '","amount": "' + amount + '","itemqty": "' + itemqty + '","comborate": "' + comborate + '","combosess_id": "' + combosesqty_id + '","NoItem": "' + 0 + '"}',
                             url: "billscreen.aspx/insertupdate_combo",
                             contentType: "application/json",
                             dataType: "json",
                             success: function (msg) {
                                 subcm_amt = 0;

                             },
                             error: function (xhr, ajaxOptions, thrownError) {

                                 var obj = jQuery.parseJSON(xhr.responseText);
                                 // alert(obj.Message);

                             },
                             complete: function () {

                             }
                         });

                     RepeatCount = 1;
                 }
                 _countqtysave++;
             })
             var Amount = 0;
             $("#tbProductInfo tr").each(function (i, obj) {
                 if ($(obj).find('input').eq(3).val() == "") {
                     Amount += parseFloat($(obj).find('input').eq(2).val());
                 }
                 else {
                     Amount += parseFloat($(obj).find('input').eq(3).val());
                 }

             });
             $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
             $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
             $("#dvProductList tr").removeClass('SumMainQtyCombo');
             $("#dvProductList tr td input").removeClass('comboQtyamt');
             con_itemnametype = "";
             con_QtyCombo = "";
         }
         function ComboSave() {

             var n = $(".SumMainCombo").nextAll("tr").length;

             var comboid = con_getcomboid;
             var itemid = 0;
             var itemqty = 0;
             var itemprice = 0;
             var amount = 0;
             var comboprice = 0;
             var count = 1;
             var countamt = 1;
             // var stramt = 0;

             var a = Math.floor((Math.random() * 10000) + 1);
             var b = Math.floor((Math.random() * 1000) + 1);
             var c = Math.floor((Math.random() * 10000) + 1);
             var d = Math.floor((Math.random() * 1000) + 1);

             combosess_id = a + '' + b + '' + c;
             //for (var i = 0; i < n; i++) {
             //  stramt += parseFloat($("#txtBillAmount" + countamt).val());
             //  stramt++;
             //  countamt++;
             //}
             var divno = n;
             var adj_amt = $(".comboamt").val();
             //comborate = adj_amt / divno;
             comborate = adj_amt;
             var CheckClass = false;
             var RepeatCount = 0;
             for (var i = 0; i < ProductCollection.length; i++) {
                 if (ProductCollection[i].ItemCode == $(".SumMainCombo").children().eq(0).text()) {
                     // ProductCollection.splice(i, 1);
                     ProductCollection.splice($.inArray(i, ProductCollection), 1);
                 }
             }
             var Price = parseFloat($(".SumMainCombo input").eq(0).val()) * (parseFloat($(".SumMainCombo input").eq(2).val()));
             TO = new clsproduct();
             TO.ItemCode = $(".SumMainCombo").children().eq(0).text();
             TO.ItemName = $(".SumMainCombo").children().eq(1).text();
             TO.Qty = $(".SumMainCombo").children().eq(3).find('input').val();
             if (parseFloat($(".SumMainCombo").children().eq(7).find('input').val()).toFixed(2) > parseFloat($(".SumMainCombo").children().eq(6).find('input').val())) {
                 TO.Price = $(".SumMainCombo").children().eq(7).find('input').val();
             }
             else {
                 TO.Price = $(".SumMainCombo").children().eq(6).find('input').val();
             }
             TO.EditButton = true;
             TO.NoItem = $(".SumMainCombo").children().eq(4).find('input').val();
             TO.Combo_Sessionid = combosess_id;
             ProductCollection.push(TO);


             $("#tbProductInfo tr").each(function (i, obj) {
                 if ($(obj).hasClass("SumMainCombo") == true) {
                     CheckClass = true;
                     $(obj).attr('data-item', combosess_id)
                 }
                 if (CheckClass == true) {

                     if (RepeatCount == 1) {
                         $(obj).remove();
                     }
                     //itemprice = $("#tbProductInfo").find("tr").eq(i).find("#txtBillQty1").eq(3).val();
                     itemid = $(obj).children().eq(0).text();
                     amount = $(obj).children().eq(5).text();
                     comborate = amount;
                     if (parseFloat($(".SumMainCombo").children().eq(4).find('input').val()) > 0 || parseFloat($(".SumMainCombo").children().eq(4).find('input').val()) > 1) {
                         itemqty = $(".SumMainCombo").children().eq(3).find('input').val();
                     }
                     else {
                         itemqty = $(obj).children().eq(3).text();
                     }
                     if ($(".SumMainCombo").children().eq(4).find('input').val() == 0) {
                         NoItem = 0;
                     }
                     else {
                         NoItem = $(".SumMainCombo").children().eq(4).find('input').val();
                     }
                     //amount = $("#combosubprice" + count).text();
                     //alert(amount);
                     $.ajax({
                         type: "POST",
                         data: '{ "comboid": "' + comboid + '","itemid": "' + itemid + '","amount": "' + amount + '","itemqty": "' + itemqty.toString() + '","comborate": "' + comborate + '","combosess_id": "' + combosess_id + '","NoItem": "' + NoItem + '"}',
                         url: "billscreen.aspx/insertupdate_combo",
                         contentType: "application/json",
                         dataType: "json",
                         success: function (msg) {
                             subcm_amt = 0;

                         },
                         error: function (xhr, ajaxOptions, thrownError) {



                         },
                         complete: function () {

                         }
                     });

                     RepeatCount = 1;
                 }

             })

             //var TotalAmt = parseFloat($("#dvsbtotal").text());
             //var ComboAmt = $(".comboamt").val();
             //var SubAmount = parseFloat(TotalAmt) + parseFloat(ComboAmt);
             //$("#dvsbtotal").html(SubAmount);
             //$("#dvnetAmount").html(SubAmount)
             //var NetAmount = parseFloat($("#dvnetAmount").text())

             //var TotalNetAmount = parseFloat(tbProductInfoSubAmount);

             //$("#dvnetAmount").html(TotalNetAmount);
             var Amount = 0;
             $("#tbProductInfo tr").each(function (i, obj) {
                 if ($(obj).find('input').eq(3).val() == "") {
                     Amount += parseFloat($(obj).find('input').eq(2).val());
                 }
                 else {
                     Amount += parseFloat($(obj).find('input').eq(3).val());
                 }

             });
             $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
             $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
             //$("#dvnetAmount").html(dvnetAmount);
             $("#dvProductList tr").removeClass('SumMainCombo');
             $("#dvProductList tr td input").removeClass('comboamt');
             con_itemnametype = "";
             con_QtyCombo = "";
         }


         function CashSave() {
           
             if (m_BillMode == "Credit" || m_BillMode == "CreditCard") {
                 //alert("Bill Mode is "+m_BillMode+" and cannot be changed to Cash.");
                 //return;
             }

             var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
             var CustomerId = "0";
             var CustomerName = "CASH";
             var billmode = "Cash";
             var NetAmt = $("#dvnetAmount").html();
             var CreditBank = "";
             var CashAmt = $("#dvnetAmount").html();
             var creditAmt = 0;
             var CreditCardAmt = 0;
             var cashcustcode = 0;
             var cashcustName = "";



             if (CshCustSelId == "0") {
                 cashcustcode = 0;
                 cashcustName = "CASH"
             }
             else {
                 cashcustcode = CshCustSelId;
                 cashcustName = CshCustSelName;
                 CustomerId = "0";
                 CustomerName = "CASH";
             }
             var OnlinePayment = 0

             //alert(con_itemnametype);

             if (con_itemnametype == 'COMBO') {

                 InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, comborate, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
                 // InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
             }
             else {
                 InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
             }

             $("#btnCash").removeAttr('disabled');
             //$("#btnClear").click();
             ClearData();


         }

         $("#btnClear").click(
         function () {
             $('#<%=dd_customername.ClientID %>').removeAttr("disabled");
             ClearData();
             DeletePrinterUserWise();


         });


         function ClearData() {
         if(FlagEdit==0)
         {  m_BillNowPrefix = "";}
           
             m_BillMode = "";
             $("#ddlbillttype").removeAttr("disabled");
             $("#btnCreditCard").css("background", "black");
             $("#ddlCreditCustomers").html("");
             $("#txtddlCreditCustomers").val("");
             $("#ddlChosseCredit").val(0);
             $("#btnCash").css("background", "black");
             $("#btnhold").css("background", "black");
             con_getcomboid = 0;
             con_getcomboprice = 0;
             getcomboid = 0;
             getcomboprice = 0;
             Storeval = 0;
             con_itemnametype = "";
             RestControls();
         }

         $("#btnOk").click(
     function () {
         var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
         var CustomerId = 0;
         var CustomerName = "";
         if (CrdCustSelId == "0") {
             CustomerId = 0;
             CustomerName = "";
         }
         else {
             CustomerId = CrdCustSelId;
             CustomerName = CrdCustSelName;
         }
         var billmode = "Credit";
         var NetAmt = $("#dvnetAmount").html();
         var CreditBank = "";
         var CashAmt = 0
         var creditAmt = $("#dvnetAmount").html();
         var CreditCardAmt = 0;
         var cashcustcode = 0;
         var cashcustName = "";


         var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
         if (CshCustSelId == "0") {
             cashcustcode = 0;
             cashcustName = "";
         }
         else {
             cashcustcode = CshCustSelId;
             cashcustName = CshCustSelName;
         }
         var OnlinePayment = 0;
         if (con_itemnametype == 'COMBO') {

             InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, comborate, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);

         }
         else if (con_QtyCombo == "QTY_COMBO") {
             InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, comborate, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
         }
         else {
             InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
         }

     });



         $("#btnCredit").click(
        function () {


            if (ProductCollection.length == 0) {
                alert("First Choose Products for billing");
                return;
            }



            billingmode = "direct";
            $("#dvCreditCustomerSearch").show();
            $("#dvProductList").hide();

        });

         $("#btnUnHold").click(
        function () {


            BillUnhold();



        });

         function BillUnhold() {
             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/GetHoldList",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     $("#tbHoldList").html(obj.holdList);

                     $("#CustomerSearchWindow").hide();
                     $("#dvProductList").hide();
                     $("#dvBillWindow").hide();

                     $("#dvCreditCustomerSearch").hide();
                     $("#dvHoldList").show();
                     $("#dvOrderList").hide();

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
          );

         }


         $("#btnOrdersList").click(
           function () {


               $.ajax({
                   type: "POST",
                   data: '{}',
                   url: "screen.aspx/GetOrdersList",
                   contentType: "application/json",
                   dataType: "json",
                   success: function (msg) {

                       var obj = jQuery.parseJSON(msg.d);

                       $("#tbOrderList").html(obj.OrderList);

                       $("#CustomerSearchWindow").hide();
                       $("#dvProductList").hide();
                       $("#dvBillWindow").hide();

                       $("#dvCreditCustomerSearch").hide();
                       $("#dvHoldList").hide();
                       $("#dvOrderList").show();
                       $("#btnreturn").attr('disabled', true);
                       $("#btnsavebill").attr('disabled', true);



                       $("#btnUnHold").attr('disabled', true);
                       $("#btnCash").attr('disabled', true);
                       $("#btnCredit").attr('disabled', true);


                   },
                   error: function (xhr, ajaxOptions, thrownError) {

                       var obj = jQuery.parseJSON(xhr.responseText);
                       alert(obj.Message);
                   },
                   complete: function () {
                       $("#btnhold").attr('disabled', 'disabled');
                   }

               }
         );

           });







         $("#btnreturn").click(
            function () {


                ReturnItem();


                //count = count+1;
            }
            );



         function ReturnItem() {




             $.ajax({
                 type: "POST",
                 data: '{ }',
                 url: "Billscreen.aspx/CheckReturn",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -12) {
                         alert("You don't have permission to perform this action..Consult Admin Department.");
                         return;
                     }




                     else {

                         if (modeRet == "Return") {
                             $("#btnreturn").css("background", "#f06671");
                             modeRet = "";
                         }
                         else {

                             $("#btnreturn").css("background", "red");
                             modeRet = "Return";

                         }
                     }


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                     $.uiUnlock();
                 }

             });



         }




         $("#dvdisper").val("0");
         $("#dvdiscount").val("0");

         $("#btnSearch").click(
       function () {

           var Keyword = $("#txtSearch");
           if (Keyword.val().trim() != "") {
               $("#colProducts").slideDown(200);
               Search(0, Keyword.val());
           }
           else {
               Keyword.focus();
           }
           $("#txtSearch").val("");

       });



         $("#txtSearch").keyup(
         function (event) {

             var keycode = (event.keyCode ? event.keyCode : event.which);

             if (keycode == '13') {


                 var Keyword = $("#txtSearch");
                 if (Keyword.val().trim() != "") {
                     $("#colProducts").slideDown(200);
                     Search(0, Keyword.val());
                     $("#txtSearch").val("");
                 }
                 else {
                     Keyword.focus();
                 }


             }


         }

         );





         $("#txtItemCode").keyup(
         function (event) {

             var keycode = (event.keyCode ? event.keyCode : event.which);

             if (keycode == '13') {



                 if ($(this).val().trim() != "") {

                     GetByItemCode(this, $(this).val());
                     $(this).val("").focus();
                 }
                 else {
                     $(this).focus().val("");
                 }


                 if ($("#dvdisper").val() != "0") {

                     BindtrDiscount($("#dvdisper").val());
                 }

             }

         }

         );

         $("#btnBillWindowClose_pm").click(function () {


             $("#dvpaymode").hide();

             ResetBillCntrols();
             Reset();


         });


         $("#btnBillWindowClose").click(
         function () {

             $("#CustomerSearchWindow").hide();
             $("#dvProductList").show();
             $("#dvBillWindow").hide();

             $("#dvCreditCustomerSearch").hide();
             $("#dvHoldList").hide();
             $("#dvOrderList").hide();
             // $("#dvBillWindow").dialog('close');
             //$("#tbliteminfo").removeAttr('style');
             //$("#tbliteminfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;')
             //$("#tbamountinfo").removeAttr('style');
             //$("#tbamountinfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;')

         }

         );

         $("#dvGetCreditCustomers,#dvGetCreditCustomersMember,#dvGetCreditCustomersPackage").click(
        function () {


            var DataGrid = jQuery('#jQGridDemo1');
            DataGrid.jqGrid('setGridWidth', '680');

            jQuery('#jQGridDemo1').GridUnload();


            $("#CustomerSearchWindow").hide();
            $("#dvProductList").hide();
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").show();
            $("#dvHoldList").hide();

            $("#dvOrderList").hide();

            //            $('#dvCreditCustomerSearch').dialog(
            //        {
            //            autoOpen: false,

            //            width: 720,
            //            resizable: false,
            //            modal: false

            //        });

            // 
            //            linkObj = $(this);
            //            var dialogDiv = $('#dvCreditCustomerSearch');
            //            dialogDiv.dialog("option", "position", [233, 48]);
            //            dialogDiv.dialog('open');
            //            return false;


        });



         //            $("#rdoOnline").change(
         //             function () {
         //                if( $("#rdoOnline").prop('checked')== true)
         //                {
         //                    onlineoption = "Onlinepayment"
         //                      $("#Td1").hide();
         //                            $("#txtCODAmount").hide();
         //                             $("#txtPaymentAmount").show();
         //                            $("#txtCODAmount").val("");
         //                             $("#txtPaymentAmount").val("");
         //                             $("#Coupans").val("0").prop("readonly", false);
         //                }
         //                
         //             });


         //               $("#rdoCOD").change(
         //             function () {
         //                if( $("#rdoCOD").prop('checked')== true)
         //                {
         //                    onlineoption = "COD"
         //                      $("#Td1").show();
         //                            $("#txtCODAmount").show();
         //                             $("#txtCODAmount").val("");
         //                            $("#txtPaymentAmount").val("");
         //                            $("#txtPaymentAmount").hide();
         //                             $("#Coupans").val("0").prop("readonly", true);
         //                             $("#No").hide();
         //                              $("#Amt").hide();
         //                               $("#holder").hide();
         //                                  $("#holder2").hide();
         //                              
         //                }
         //                
         //             });

         //               $("#txtCODAmount").change(

         //               function() {

         //                var txtOnlineAmount = $("#txtpaymentAmount");
         //                  var txtFinalBillAmount = $("#txtFinalBillAmount");
         //             
         //             var txtCODAmount = $("#txtCODAmount");
         //              txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
         //             
         //           
         //                }

         //               );


         $("#txtCODAmount_pm").keyup(
      function () {
          var regex = /^[0-9\.]*$/;

          onlineoption = "COD"
          $("#Td1_pm").show();

          $("#txtPaymentAmount_pm").val("");
          $("#txtPaymentAmount_pm").hide();
          //$("#Coupans_pm").val("0").prop("readonly", true);
          $("#No_pm").hide();
          $("#Amt_pm").hide();
          $("#holder_pm").hide();
          $("#holder2_pm").hide();
          var value = jQuery.trim($(this).val());
          var count = value.split('.');


          if (value.length >= 1) {
              if (!regex.test(value) || value <= 0 || count.length > 2) {

                  $(this).val(0);


              }
          }
          var txtOnlineAmount = $("#txtpaymentAmount_pm");
          var txtFinalBillAmount = $("#txtFinalBillAmount_pm");

          var txtCODAmount = $("#txtCODAmount_pm");
          txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
          DEVBalanceCalculation_pm();


      }
      );








         $("#txtCODAmount").keyup(
      function () {
          var regex = /^[0-9\.]*$/;

          onlineoption = "COD"
          $("#Td1").show();

          $("#txtPaymentAmount").val("");
          $("#txtPaymentAmount").hide();
          $("#Coupans").val("0").prop("readonly", true);
          $("#No").hide();
          $("#Amt").hide();
          $("#holder").hide();
          $("#holder2").hide();
          var value = jQuery.trim($(this).val());
          var count = value.split('.');


          if (value.length >= 1) {
              if (!regex.test(value) || value <= 0 || count.length > 2) {

                  $(this).val(0);


              }
          }
          var txtOnlineAmount = $("#txtpaymentAmount");
          var txtFinalBillAmount = $("#txtFinalBillAmount");

          var txtCODAmount = $("#txtCODAmount");
          txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
          DEVBalanceCalculation();


      }
      );







         $("#ddlbillttype").change(function () {
        if ($(this).val() == "Credit") {

            //New Changes //
           // $("#txtddlCreditCustomers").show();
          //  $("#ddlChosseCredit").show();
     //  $("#btnAddCreditCustomer").css("display", "block");
  
            $("#lblOTP").hide();
            $("#OTPVal").hide();
            $("#txtCreditAmount").show();
            $("#ddlcustName").show();
          //  $("#dvOuter_ddlCreditCustomers").show();
            $("#lblCashHeading").text("Receipt Amt:");
            $("#ddlbillttype option[value='Credit']").prop("selected", true);
            $("#Text13").val("0").hide();
            $("#Text14").val("").hide();
            $("#Text15").val("0").hide();
            $("#Text16").val("").hide();
            $("#ddlType").hide();
            $("#ddlOtherPayment").hide();
            $("#rdoCOD").hide();
            $("#rdoOnline").hide();
            $("#Td1").hide();
            $("#txtCODAmount").hide();
            $("#COD").hide();
            $("#Online").hide();
            $("#txtpaymentAmount").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#Coupans").hide();
            $("#OP").hide();
            $("#coupan").hide();
            $("#holder input").hide();
            $("#holder2 input").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#codChk").hide();
            $("#OnlineChk").hide();
            $("#lblCardAmount").hide();
            $("#lblCreaditType").hide();
            $("#lblCCNO").hide();
            $("#lblBank").hide();
            //$("#ddlBank").prop("disabled", true);
            $("#lblCashHeading").hide();
            $("#ddlBank").hide();
            var FinalAmount = $("#txtFinalBillAmount").val();
            $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
            $("#txtCashReceived").val(FinalAmount);
            $("#txtCashReceived").hide();
            $("#txtBalanceReturn").val(0);
            $("#CreditSearchBtn").show();
        }
        else if ($(this).val() == "CreditCard") {
            var customerId = 0;
            CrdCustSelId = 0;
            CrdCustSelName = "";
            //New Changes//
            $("#ddlcustName").show();
            $("#ddlChosseCredit").show();
          //  $("#btnAddCreditCustomer").css("display", "block");
            $("#ddlType").show();
            $("#Text14").show();
            $("#ddlBank").show();
            $("#lblBank").show();
            $("#lblCardAmount").show();
            $("#lblCreaditType").show();
            $("#lblCCNO").show();
            $("#Text13").show();
            $("#lblOTP").hide();
            $("#OTPVal").hide();
            $("#txtCreditAmount").hide();
            //Hide All Properties//

            $("#hdnCreditCustomerId").val(0);
            $("#lblCreditCustomerName").text("");
            $("#lblCreditCustomerName").hide();
            CrdCustSelName = "";
            $("#ddlCreditCustomers").html("");
            $("#ddlCreditCustomers").hide();
            $("#txtddlCreditCustomers").hide();
            $("#txtddlCreditCustomers").val("");
            $("#Coupans").hide();
            $("#OP").hide();
            $("#coupan").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#COD").hide();
            $("#Online").hide();
            $("#rdoOnline").hide();
            $("#lblCashHeading").text("Cash Rec:");
            $("#lblCashHeading").hide();
            $("#creditCustomer").css("display", "none");
            //$("#ddlType").prop("disabled", true);
            // $("#ddlType").hide();
            $("#ddlOtherPayment").hide();
            $("#txtpaymentAmount").hide();
            $("#rdoCOD").hide();
            $("#Td1").hide();
            $("#txtCODAmount").hide();
            $("#dvOuter_ddlCreditCustomers").hide();
            // $("#ddlChosseCredit").hide();
            $("#holder input").hide(); $("#holder2 input").hide();
            $("#No").hide();
            $("#Amt").hide();

            //                   $("#ddlOtherPayment").hide();
            $("#codChk").hide();
            $("#OnlineChk").hide();
            $("#txtCashReceived").val("0").prop("readonly", true);
            $("#Text13").val("0").prop("readonly", false);
            $("#Text14").val("").prop("readonly", false);
            $("#Text15").val("0").prop("readonly", false);
            $("#Text16").val("").prop("readonly", false);
            $("#ddlType").prop("disabled", false);
            $("#ddlBank").prop("disabled", false);
            $("#Text13").val($("#txtFinalBillAmount").val());
            $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());

            $("#txtCashReceived").hide(); 
            
            $("#Text15").hide();
            $("#Text16").hide();
            $("#ddlChosseCredit").hide();
        //  $("#btnAddCreditCustomer").css("display", "none");
            $("#ddlcustName").hide();
            $("#CreaditCustomerPart").hide();
            $("#CreditSearchBtn").hide();
        }

        else if ($(this).val() == "Cash") {

            $("#ddlcustName").hide();
            var customerId = 0;
            CrdCustSelId = 0;
            CrdCustSelName = "";
            $("#txtCashReceived").show();
            $("#lblCashHeading").show();
            $("#lblCreaditType").hide();
            //Hide All Properties//
            $("#txtCreditAmount").hide();
            $("#hdnCreditCustomerId").val(0);
            $("#lblCreditCustomerName").text("");
            CrdCustSelName = "";
            $("#ddlCreditCustomers").html("");
            $("#txtddlCreditCustomers").val("");
            $("#ddlCreditCustomers").hide();
            $("#txtddlCreditCustomers").hide();
            $("#Coupans").hide();
            $("#OP").hide();
            $("#lblCashHeading").text("Cash Rec:");
            // $("#lblCashHeading").hide();
            $("#creditCustomer").css("display", "none");
            $("#coupan").hide();
          //  $("#dvOuter_ddlCreditCustomers").hide();
         //   $("#ddlChosseCredit").hide();
        //  $("#btnAddCreditCustomer").css("display", "none");
            $("#ddlType").prop("disabled", true);
            $("#ddlType").hide();
            $("#ddlOtherPayment").hide();
            $("#COD").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#Online").hide();
            $("#txtpaymentAmount").hide();
            $("#rdoCOD").hide();
            $("#Td1").hide();
            $("#txtCODAmount").hide();
            //                   $("#ddlOtherPayment").hide();
            $("#holder input").hide();
            $("#holder2 input").hide();
            $("#No").hide();
            $("#Amt").hide();
            $("#lblCardAmount").hide();
            $("#lblCCNO").hide();
            $("#rdoOnline").hide();
            $("#lblBank").hide();

            $("#txtCashReceived").val("0").prop("readonly", false);
            $("#Text13").val("0").prop("readonly", true);
            $("#Text14").val("").prop("readonly", true);
            $("#Text15").val("0").prop("readonly", true);
            $("#Text16").val("").prop("readonly", true);
            $("#ddlType").prop("disabled", true);
            $("#ddlBank").prop("disabled", true);
            $("#txtCashReceived").val($("#txtFinalBillAmount").val());
            $("#txtBalanceReturn").val(" ");

            // $("#txtCashReceived").hide();
            $("#Text13").val("0").hide();
            $("#Text14").val("").hide();
            $("#Text15").val("0").hide();
            $("#Text16").val("").hide();
            $("#ddlType").hide();
            $("#ddlBank").hide();
            // $("#txtCashReceived").hide();
            $("#txtBalanceReturn").val(" ");

            $("#codChk").hide();
            $("#OnlineChk").hide();
            $("#CreaditCustomerPart").hide();
            $("#CreditSearchBtn").hide();
            $("#lblOTP").hide();
            $("#OTPVal").hide();
        }
        else if ($(this).val() == "OnlinePayment") {

            var customerId = 0;
            CrdCustSelId = 0;
            CrdCustSelName = "";
            //New Changes//
            $("#ddlOtherPayment").show();
            $("#txtCODAmount").show();
            $("#txtpaymentAmount").show();
            $("#Online").show();
            $("#OnlineChk").show();
            $("#codChk").show();
            $("#COD").show();
            $("#OP").show();
            $("#coupan").show();
            $("#Coupans").show();
            $("#ddlcustName").hide();
            $("#hdnCreditCustomerId").val(0);
            $("#lblCreditCustomerName").text("");
            CrdCustSelName = "";
            $("#ddlCreditCustomers").html("");
            $("#txtddlCreditCustomers").val("");
            $("#txtCreditAmount").hide();
            $("#lblCashHeading").text("Cash Rec:");
            $("#creditCustomer").css("display", "none");
            $("#coupan").hide();
            $("#dvOuter_ddlCreditCustomers").hide();
          //  $("#ddlChosseCredit").hide();
         // $("#btnAddCreditCustomer").css("display", "none");
            $("#lblCashHeading").hide();
            $("#txtCashReceived").hide();
            $("#lblCardAmount").hide();
            $("#Text13").hide();
            $("#lblCreaditType").hide();
            $("#lblCCNO").hide();
            $("#Text14").hide();
            $("#lblBank").hide();
            $("#ddlBank").hide();
            $("#ddlType").hide();
            $("#No").show();
            $("#Amt").show();


            //                            $("#rdoOnline").show();
            //                               $("#rdoCOD").show();
            if ($("#txtCODAmount").val() != 0) {
                onlineoption = "COD"


            }
            $("#Coupans").val("0").prop("readonly", true);
            $("#Td1").show();
            $("#txtCODAmount").show();
            $("#holder2").hide();
            $("#holder").hide();
            $("#No").hide();
            $("#Amt").hide();


            $("#Coupans").show();
            $("#coupan").show();
            $("#OP").show();
            $("#txtCashReceived").val("0").prop("readonly", true);
            $("#Text13").val("0").prop("readonly", true);
            $("#Text14").val("").prop("readonly", true);
            $("#Text15").val("0").prop("readonly", false);
            $("#Text16").val("").prop("readonly", false);
            $("#ddlType").prop("disabled", true);
            $("#ddlBank").prop("disabled", true);
            //$("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
            $("#txtBalanceReturn").val("0");
            $("#CreaditCustomerPart").hide();
            $("#CreditSearchBtn").hide();
            if ($('#<%=dd_customername.ClientID %>').val() != "0") {
                $("#lblOTP").show();
                $("#OTPVal").show();
            }
            else
            {
                $("#lblOTP").hide();
                $("#OTPVal").hide();
            }
        }

    }
    );

         option = $("#<%=ddloption.ClientID%>").val();

         $("#<%=ddloption.ClientID%>").change(
          function () {
            
              option = $("#<%=ddloption.ClientID%>").val();
           
              if (option == "TakeAway") {
                  $("#ddlEmployees").css("display", "none");
                  $("#ddlTable").hide();
                  $("#ddlTable").prop("disabled", true);
                  $("#ddlTable option").removeAttr("selected");
                  $("#DelCharges").hide();

              }
              else if (option == "HomeDelivery") {
                  $("#ddlTable").hide();
                  $("#ddlEmployees").css("display", "block");


                  if (HomeDelCharges == 1) {

                      if (minbillvalue == 0 || DeliveryCharges == 0) {
                          $("#DelCharges").show();
                          $("#dvDeliveryChrg").removeAttr("disabled");
                          $("#chkDelivery").show();
                          $("#lbldelcharges").show();
                      }
                      else {
                          if (cst_id == 0) {
                              $("#dvDeliveryChrg").val(DeliveryCharges);
                              //$("#dvDeliveryChrg").attr("disabled", "disabled");
                              $("#chkDelivery").hide();
                              $("#lbldelcharges").hide();
                              $("#DelCharges").show();
                          }

                      }

                  }
                  else {
                      $("#DelCharges").hide();
                      $("#chkDelivery").hide();
                      $("#lbldelcharges").hide();
                      $("#dvDeliveryChrg").val(0);
                  }

              }
              else {
                  $("#ddlEmployees").css("display", "block");

                  $("#ddlTable").show();
                  $("#ddlTable").prop("disabled", false).focus();
                  $("#DelCharges").hide();
                  // alert("Choose Table No");
              }

              Bindtr();

          }
       );




         $.ajax({
             type: "POST",
             data: '{}',
             url: "screen.aspx/BindCategories",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);


                 $("#categories").html(obj.categoryData);
                 AdjustMinues();
                 //                     if (AllowServicetax == 1) {
                 //                         Sertax = obj.setttingData.SerTax;
                 //                     }
                 //                     else {
                 //                         Sertax = 0;
                 //                     }
                 //  Takeaway = obj.setttingData.TakeAway;

                 Takeawaydefault = obj.setttingData.TakeAwayDefault;

                 var CatId = obj.CategoryId;

                 Search(CatId, "");
             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {

             }

         }
     );



            $("#btnBillWindowOk").click(
                function () {
                    billtype = $("#ddlbillttype").val();

                   
				 if (billtype == "Cash" || billtype == "CreditCard" || billtype == "OnlinePayment") {
					 if (CshCustSelName == "") {

						 alert("Choose Party First.");
						 return;
					 }
                 }
        var maximum = null;
        var billtype = "";
        var txtval;

        $('.cls_mmp').each(function () {
            var value = parseFloat($(this).val());
            txtval = value;
            maximum = (value > maximum) ? value : maximum;
        });

        //$('.cls_mmp').each(function () {

        //    var value = parseFloat($(this).val());
        //    if (value == maximum)
        //    {
        //        billtype=$(this).attr('data');
            
        //    }


        //});
        billtype = $("#ddlbillttype").val();
        if ($("#txtBalanceReturn").val() < 0) {
            alert("Balance Remaining!");
            return

        }
        if ($("#ddlOtherPayment").val() === "0" && $("#ddlbillttype").val() === "OnlinePayment") {
            alert("Please select online payment");
            return;
        }
        $("#btnBillWindowOk").attr('disabled', 'disabled');

       
        if (billtype == "") {
            alert("Select BillType First");
            $("#ddlbillttype").focus();
            return;

        }


                    if (billtype == "Credit") {


            //if (CrdCustSelId == 0) {

            //    alert("Please select Credit Customer");
            //    $("#txtddlCreditCustomers").focus();
            //    $("#btnBillWindowOk").removeAttr('disabled');
            //    return;
            //}


        }


        $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');




        DEVBalanceCalculation();



        var txtcreditcardcheck = $("#Text13").val();

        if (txtcreditcardcheck != "0") {
            if ($("#ddlType").val() == "") {
                $.uiUnlock();
                alert("Please Select Credit Card Type");
                $("#ddlType").focus();
                $("#btnBillWindowOk").removeAttr('disabled');
                return;
            }

        }

        var txtchequecheck = $("#Text15").val();
        if (txtchequecheck != "0") {
            if ($("#ddlBank").val() == "") {
                $.uiUnlock();
                alert("Please Select Bank");
                $("#ddlBank").focus();
                $("#btnBillWindowOk").removeAttr('disabled');
                return;
            }
            if ($("#Text16").val() == "") {
                $.uiUnlock();
                alert("Please Enter Cheque No");
                $("#Text16").focus();
                $("#btnBillWindowOk").removeAttr('disabled');
                return;
            }
        }


        var cashamount = $("#txtCashReceived").val();


        if (billtype == "Cash" || billtype == "CreditCard" || billtype == "OnlinePayment") {
            if (Number($("#txtBalanceReturn").val()) < 0) {
                $.uiUnlock();
                alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                $("#btnBillWindowOk").removeAttr('disabled');
                return;
            }
            else {
                cashamount = cashamount - Number($("#txtBalanceReturn").val());
            }

        }

        if (Number(cashamount) < 0) {
            $.uiUnlock();
            alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
            $("#btnBillWindowOk").removeAttr('disabled');
            return;
        }


        if (billtype == "CreditCard") {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            //var CustomerId = $("#ddlBank option:selected").val();
            //var CustomerName = $("#ddlBank option:selected").text();


			var CustomerId = CshCustSelId
			var CustomerName = CshCustSelName
            
            var billmode = billtype;
            var NetAmt = $("#txtFinalBillAmount").val();

            var CashAmt = 0
            var creditAmt = 0;
            var OnlinePayment = 0;
            var BIllValue = $("#dvsbtotal").html();


            var CreditBank = $("#ddlBank").val();
            var CreditCardAmt = $("#Text13").val();


            var cashcustcode = 0;
            var cashcustName = "";
            if (CshCustSelId == "0") {
                cashcustcode = 0;
                cashcustName = "CASH"
            }
            else {
				cashcustcode = $("#ddlBank option:selected").val();
				cashcustName = $("#ddlBank option:selected").text();
            }

        }
        if (billtype == "Cash") {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            var CustomerId = "CASH";
            var CustomerName = "CASH";
            var billmode = billtype;
            var NetAmt = $("#txtFinalBillAmount").val();
            var CashAmt = $("#txtCashReceived").val();
            var creditAmt = 0;
            //var OnlinePayment = 0;
            var BIllValue = $("#dvsbtotal").html();


            var CreditBank = $("#ddlBank").val();
            var CreditCardAmt = $("#Text13").val();


            var cashcustcode = 0;
            var cashcustName = "";
            if (CshCustSelId == "0") {
                cashcustcode = 0;
                cashcustName = "CASH"
            }
            else {
                cashcustcode = CshCustSelId;
                cashcustName = CshCustSelName;
            }

        }


        if (billtype == "OnlinePayment") {

            var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            var CustomerId = CshCustSelId;
            var CustomerName = CshCustSelName;
            var billmode = billtype;
            var NetAmt = $("#txtFinalBillAmount").val();
            

            var CashAmt = 0;
            if (onlineoption == "COD") {
                CashAmt = $("#txtCODAmount").val();
            }
            var creditAmt = 0;

            var OnlinePayment = $("#txtpaymentAmount").val();
            var BIllValue = $("#dvsbtotal").html();


            var CreditBank = "";
            var CreditCardAmt = 0;


            var cashcustcode = 0;
            var cashcustName = "";
            if (CshCustSelId == "0") {
                cashcustcode = 0;
                cashcustName = "CASH"
            }
            else {
                cashcustcode = CshCustSelId;
                cashcustName = CshCustSelName;
            }

        }

        else if (billtype == "Credit") {

            var SelectedRow = jQuery('#jQGridDemoCredit').jqGrid('getGridParam', 'selrow');
            var CustomerId = 0;
            var CustomerName = "";
            if (CrdCustSelId == "0") {
                CustomerId = CshCustSelId;
                CustomerName = CshCustSelName;
            }
            else {
				CustomerId = CshCustSelId;
				CustomerName = CshCustSelName;
            }

            var NetAmt = $("#txtFinalBillAmount").val();

            var billmode = billtype;

            var CreditBank = $("#ddlBank").val();
            var OnlinePayment = 0;
            var CashAmt = $("#txtCashReceived").val();

            var BIllValue = $("#dvsbtotal").html();
            var creditAmt = 0;
            if (billmode == "Credit") {
                creditAmt = Number(BIllValue) - Number(CashAmt);


            }

            var CreditCardAmt = $("#Text13").val();


            var cashcustcode = 0;
            var cashcustName = "";


            var SelectedRow2 = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
            if (CshCustSelId == "0") {
                cashcustcode = 0;
                cashcustName = "";
            }
            else {
                cashcustcode = CshCustSelId;
                cashcustName = CshCustSelName;
            }

        }

                  
        if (con_itemnametype == 'COMBO') {
            ComboSave();
            //InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
            InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, $("#txtCreditAmount").val(), CreditCardAmt, cashcustcode, cashcustName, $("#txtpaymentAmount").val());
           
        }
        else if (con_QtyCombo == "QTY_COMBO") {
            ComboSave();
			InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, $("#txtCreditAmount").val(), CreditCardAmt, cashcustcode, cashcustName, $("#txtpaymentAmount").val());
           // InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
        }
        else {
			InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, $("#txtCreditAmount").val(), CreditCardAmt, cashcustcode, cashcustName, $("#txtpaymentAmount").val());
           // InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
        }
        $('#<%=dd_customername.ClientID %>').val(0);
        $('#tdCash').removeAttr('style');
        $('#tdCreaditCard').removeAttr('style');
        $('#tdsavebill').removeAttr('style');
        $("#OnlinePaymode").removeAttr('style');
        $("#OnlinePaymode").attr('style', 'display:none');
    }
);


         $("#txtCreditAmount").keyup(
function () {
    var regex = /^[0-9\.]*$/;


    var value = jQuery.trim($(this).val());
    var count = value.split('.');


    if (value.length >= 1) {
        if (!regex.test(value) || value <= 0 || count.length > 2) {
            $(this).val(0);
        }
    }

    DEVBalanceCalculation();

}
);




         $("#txtCashReceived").keyup(
    function () {
        var regex = /^[0-9\.]*$/;


        var value = jQuery.trim($(this).val());
        var count = value.split('.');


        if (value.length >= 1) {
            if (!regex.test(value) || value <= 0 || count.length > 2) {
                $(this).val(0);
            }
        }

        DEVBalanceCalculation();

    }
    );

         $("#txtpaymentAmount_pm").keyup(
                function () {
                    var regex = /^[0-9\.]*$/;
                    if ($("#txtCODAmount_pm").val() != 0) {
                        onlineoption = "COD"

                    }
                    else {
                        onlineoption = "Onlinepayment"
                        $("#Td1_pm").hide();
                        //  $("#txtCODAmount").hide();
                        // $("#txtPaymentAmount").show();
                        //  $("#txtCODAmount").val("");
                        $("#txtPaymentAmount_pm").val("");
                        $("#Coupans_pm").val("0").prop("readonly", false);
                    }
                    if ($("#txtCODAmount_pm").val() == "") {
                        $("#txtCODAmount_pm").val(0);
                    }

                    var value = jQuery.trim($(this).val());
                    var count = value.split('.');


                    if (value.length >= 1) {
                        if (!regex.test(value) || value <= 0 || count.length > 2) {

                            $(this).val(0);


                        }
                    }

                    DEVBalanceCalculation_pm();


                }
                );
         $("#txtpaymentAmount").keyup(
        function () {
            var regex = /^[0-9\.]*$/;
            if ($("#txtCODAmount").val() != 0) {
                onlineoption = "COD"

            }
            else {
                onlineoption = "Onlinepayment"
                $("#Td1").hide();
                //  $("#txtCODAmount").hide();
                // $("#txtPaymentAmount").show();
                //  $("#txtCODAmount").val("");
                $("#txtPaymentAmount").val("");
                $("#Coupans").val("0").prop("readonly", false);
            }
            if ($("#txtCODAmount").val() == "") {
                $("#txtCODAmount").val(0);
            }

            var value = jQuery.trim($(this).val());
            var count = value.split('.');


            if (value.length >= 1) {
                if (!regex.test(value) || value <= 0 || count.length > 2) {

                    $(this).val(0);


                }
            }
            var txtcodAmount = $("#txtCODAmount").val();
            var txtFinalBillAmount = $("#txtFinalBillAmount").val();
            var txtPaymentAmount = $("#txtpaymentAmount").val();
            //var txtCODAmount = $("#txtCODAmount");
            //txtOnlineAmount.val(Number(txtFinalBillAmount.val()) - Number(txtCODAmount.val()));
            if (Number(txtPaymentAmount) > 0) {
                $("#txtCODAmount").val(0);
            }


            DEVBalanceCalculation();


        }
        );

         $(document).ready(function () {
             $("#txtCODAmount").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;

                 onlineoption = "COD"
                 $("#Td1").show();
                 $("#txtPaymentAmount").val("");
                 $("#txtPaymentAmount").hide();
                 $("#Coupans").val("0").prop("readonly", true);
                 $("#No").hide();
                 $("#Amt").hide();
                 $("#holder").hide();
                 $("#holder2").hide();
                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');
                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {
                         $(this).val(0);
                     }
                 }
                 var txtcodAmount = $("#txtCODAmount").val();
                 var txtFinalBillAmount = $("#txtFinalBillAmount").val();
                 var txtPaymentAmount = $("#txtpaymentAmount").val();
                 if (Number(txtcodAmount) > 0) {
                     $("#txtpaymentAmount").val(0);
                 }
                 DEVBalanceCalculation();
             }
             )
         })





         $("#Text13").keyup(
         function () {
             //var regex = /^[0-9\.]*$/;


             //var value = jQuery.trim($(this).val());
             //var count = value.split('.');


             //if (value.length >= 1) {
             //    if (!regex.test(value) || value <= 0 || count.length > 2) {

             //        $(this).val(0);


             //    }
             //}


             DEVBalanceCalculation();

         }
         );
         $("#Text15").keyup(
         function () {
             var regex = /^[0-9\.]*$/;


             var value = jQuery.trim($(this).val());
             var count = value.split('.');


             if (value.length >= 1) {
                 if (!regex.test(value) || value <= 0 || count.length > 2) {

                     $(this).val(0);


                 }
             }

             DEVBalanceCalculation();

         }
         );

         function ResetBillCntrols() {

             $("#txtFinalBillAmount").val("");
             $("#ddlbillttype").val("");
             $("#txtCashReceived").val("0");
             $("#txtCreditAmount").val("0");
             $("#Text13").val("0");
             $("#Text15").val("0");
             $("#Text14").val("");
             $("#ddlType").val("");
             $("#ddlBank").val("");

             $("#Text16").val("");
             $("#txtRemarks").val("");
             $("#txtBalanceReturn").val("0");
             modeRet = "";
             $("#CustomerSearchWindow").hide();
             $("#dvProductList").show();
             $("#dvBillWindow").hide();
             $("#dvCreditCustomerSearch").hide();
             $("#dvHoldList").hide();
             $("#dvOrderList").hide();



             $("#hdnCreditCustomerId").val("0");
             $("#ddlbillttype option[value='" + DefaultPaymode + "']").prop("selected", true);
             $("#creditCustomer").css("display", "none");


             if (m_BillNowPrefix != "") {

                 if (m_BillMode == "Credit") {


                  //   $("#dvOuter_ddlCreditCustomers").show();
                 //  $("#ddlChosseCredit").show() ;$("#btnAddCreditCustomer").css("display", "block");

                     $("#lblCashHeading").text("Receipt Amt:");
                     $("#ddlbillttype option[value='Credit']").prop("selected", true);
                     $("#txtCashReceived").val("0").prop("readonly", false);
                     //$("#Text13").val("0").prop("readonly", true);
                     //$("#Text14").val("").prop("readonly", true);
                     $("#Text15").val("0").prop("readonly", true);
                     $("#Text16").val("").prop("readonly", true);
                     //$("#ddlType").prop("disabled", true);
                     //$("#ddlBank").prop("disabled", true);

                 }
                 else if (m_BillMode == "CreditCard") {


                     $("#hdnCreditCustomerId").val(0);
                     $("#lblCreditCustomerName").text("");

                     $("#ddlCreditCustomers").html("");
                     $("#txtddlCreditCustomers").val("");

                     $("#lblCashHeading").text("Cash Rec:");
                     $("#creditCustomer").css("display", "none");

                     $("#dvOuter_ddlCreditCustomers").hide();
                   //  $("#ddlChosseCredit").hide();
                   //$("#btnAddCreditCustomer").css("display", "none");


                     $("#txtCashReceived").val("0").prop("readonly", true);
                     $("#Text13").val("0").prop("readonly", false);
                     $("#Text14").val("").prop("readonly", false);
                     $("#Text15").val("0").prop("readonly", false);
                     $("#Text16").val("").prop("readonly", false);
                     $("#ddlType").prop("disabled", false);
                     $("#ddlBank").prop("disabled", false);

                 }
                 else if (m_BillMode == "Cash") {



                     CrdCustSelId = 0;
                     CrdCustSelName = "";
                     $("#hdnCreditCustomerId").val(0);
                     $("#lblCreditCustomerName").text("");
                     CrdCustSelName = "";
                     $("#ddlCreditCustomers").html("");
                     $("#txtddlCreditCustomers").val("");

                     $("#lblCashHeading").text("Cash Rec:");
                     $("#creditCustomer").css("display", "none");

                   //  $("#dvOuter_ddlCreditCustomers").hide();
                   //  $("#ddlChosseCredit").hide();
                   //$("#btnAddCreditCustomer").css("display", "none");



                     $("#txtCashReceived").val("0").prop("readonly", false);
                     //$("#Text13").val("0").prop("readonly", true);
                     //$("#Text14").val("").prop("readonly", true);
                     $("#Text15").val("0").prop("readonly", true);
                     $("#Text16").val("").prop("readonly", true);
                     $("#ddlType").prop("disabled", false);
                     $("#ddlBank").prop("disabled", false);


                 }
             }

             else {


                 if (DefaultPaymode == "Credit") {


                  //   $("#dvOuter_ddlCreditCustomers").show();
                //   $("#ddlChosseCredit").show() ;$("#btnAddCreditCustomer").css("display", "block");

                     $("#lblCashHeading").text("Receipt Amt:");
                     $("#ddlbillttype option[value='Credit']").prop("selected", true);
                     $("#txtCashReceived").val("0").prop("readonly", false);
                     //$("#Text13").val("0").prop("readonly", true);
                     //$("#Text14").val("").prop("readonly", true);
                     $("#Text15").val("0").prop("readonly", true);
                     $("#Text16").val("").prop("readonly", true);
                     //$("#ddlType").prop("disabled", true);
                     //$("#ddlBank").prop("disabled", true);

                 }
                 else if (DefaultPaymode == "CreditCard") {


                     $("#hdnCreditCustomerId").val(0);
                     $("#lblCreditCustomerName").text("");

                     $("#ddlCreditCustomers").html("");
                     $("#txtddlCreditCustomers").val("");

                     $("#lblCashHeading").text("Cash Rec:");
                     $("#creditCustomer").css("display", "none");

                  //   $("#dvOuter_ddlCreditCustomers").hide();
                   //  $("#ddlChosseCredit").hide();
                   //$("#btnAddCreditCustomer").css("display", "none");




                     $("#txtCashReceived").val("0").prop("readonly", true);
                     $("#Text13").val("0").prop("readonly", false);
                     $("#Text14").val("").prop("readonly", false);
                     $("#Text15").val("0").prop("readonly", false);
                     $("#Text16").val("").prop("readonly", false);
                     $("#ddlType").prop("disabled", false);
                     $("#ddlBank").prop("disabled", false);

                 }
                 else if (DefaultPaymode == "Cash") {



                     CrdCustSelId = 0;
                     CrdCustSelName = "";
                     $("#hdnCreditCustomerId").val(0);
                     $("#lblCreditCustomerName").text("");
                     CrdCustSelName = "";
                     $("#ddlCreditCustomers").html("");
                     $("#txtddlCreditCustomers").val("");

                     $("#lblCashHeading").text("Cash Rec:");
                     $("#creditCustomer").css("display", "none");

                     $("#dvOuter_ddlCreditCustomers").hide();
                   //  $("#ddlChosseCredit").hide();
                   //$("#btnAddCreditCustomer").css("display", "none");



                     $("#txtCashReceived").val("0").prop("readonly", false);
                     //$("#Text13").val("0").prop("readonly", true);
                     //$("#Text14").val("").prop("readonly", true);
                     $("#Text15").val("0").prop("readonly", true);
                     $("#Text16").val("").prop("readonly", true);
                     //$("#ddlType").prop("disabled", true);
                     //$("#ddlBank").prop("disabled", true);


                 }



             }



             if (m_BillNowPrefix != "") {
                 $("#ddlbillttype option[value='" + m_BillMode + "']").prop("selected", true);
             }
             if (m_BillMode == "Credit") {

                 $("#ddlCreditCustomers").html("<option value='" + CrdCustSelId + "'>" + CrdCustSelName + "</option>");
                 $("#txtddlCreditCustomers").val(CrdCustSelName);
                 $("#creditCustomer").show();

                 if ($("#ddlChosseCredit").val() != "0") {
                     CrdCustSelId = $("#ddlChosseCredit").val();
                     CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                 }


             }
             $.uiUnlock();
         }


         $("#btnCreditCardBilling").click(
    function () {

        try {
            //TenderAmount();
            var ConactBillNum = $("#lstBillNo").text().split('-')
            var Increment = parseFloat(ConactBillNum[1]);
            var BillNumPlusValue = ConactBillNum[0] + "-" + parseFloat(Increment + 1);
           // Reprints(BillNumPlusValue);
        } catch (e) {
              
        }

        //var CustomerId = $("#ddlCreditCardBank").val()
                 //var CustomerName = $("#ddlCreditCardBank option:selected").text();
                 var CustomerId = CshCustSelId;
                 var CustomerName = CshCustSelName;
        var billmode = "CreditCard";
        var NetAmt = $("#dvnetAmount").html();
        var CreditBank = $("#ddlCreditCardBank").val()
        var CashAmt = 0;
        var creditAmt = 0;
        var CreditCardAmt = $("#dvnetAmount").html();
        var cashcustcode = 0;
        var cashcustName = "";
        if (CshCustSelId == "0") {
            cashcustcode = 0;
            cashcustName = "CASH";
        }
        else {
            cashcustcode = $("#ddlCreditCardBank").val();
			cashcustName = $("#ddlCreditCardBank option:selected").text();
           
        }

        var OnlinePayment = 0;

        if (con_itemnametype == 'COMBO') {
            ComboSave();
            InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, creditAmt, comborate, cashcustcode, cashcustName, OnlinePayment);
            // InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);
        }
        else if (con_QtyCombo == "QTY_COMBO") {
            ComboSave();
            InsertUpdate(CustomerId, CustomerName, billmode, comborate, CreditBank, CashAmt, creditAmt, comborate, cashcustcode, cashcustName, OnlinePayment);
        }
        else {

                InsertUpdate(CustomerId, CustomerName, billmode, NetAmt, CreditBank, CashAmt, creditAmt, CreditCardAmt, cashcustcode, cashcustName, OnlinePayment);

        }
        $("#dvCreditCardWindow").dialog('close');

        ClearData();



    });

         $("#btnCreditCard").click(
             function () {
				 if (CshCustSelName == "") {

					 alert("Choose Party First.");
					 return;
				 }

             $("#btnCreditCard").attr('disabled', 'disabled');
				
             CreditCardSave();

         }
         );



         function CreditCardSave() {
             FlagPaymode = 2;
             if (m_BillMode == "Credit" || m_BillMode == "Cash") {
                 $("#btnCreditCard").removeAttr('disabled');
                 // alert("Bill Mode is "+m_BillMode+" and cannot be changed to Credit Card.");
                 // return;
             }

             $("#dvCreditCardWindow").dialog({

                 modal: true


             });

             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindBanks",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     $("#ddlCreditCardBank").html(obj.BankOptions);
                     $("#ddlCreditCardBank option[value='" + DefaultBank + "']").prop("selected", true);
                 }


             });
         }



         $("#btnsavebill").click(
         function ShowPaymentmode() {
             BindtrDiscount($("#dvdisper").val());
             $("#lblOTP").hide();
             $("#OTPVal").hide();
             $("#ddlbillttype").removeAttr('readonly');
             $("#ddlbillttype").removeAttr('disabled');
           
             if (ProductCollection.length == 0) {
                 alert("Please first Select ProductsFor Billing");
                 $.uiUnlock();
                 return false;
             }


             
           ResetBillCntrols();



             $("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly", true);


             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindBanks",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     $("#ddlBank").html(obj.BankOptions);
                     $("#ddlBank option[value='" + DefaultBank + "']").prop("selected", true);
                 }


             });


             $("#CustomerSearchWindow").hide();
             $("#dvProductList").hide();
             $("#dvBillWindow").show();
             $("#dvCreditCustomerSearch").hide();
             $("#dvHoldList").hide();
             $("#dvOrderList").hide();
             $("#codChk").hide();
             $("#OnlineChk").hide();
             Cash();
             $("#CreaditCustomerPart").hide();
             $("#CreditSearchBtn").hide();
             FlagPaymode = 1;
     
             if (FlagEdit == 1)
             {

                 if (getdel_cust_id != 0)
                 {
                     $("#CustomerSearchWindow").hide();
                     $("#dvProductList").hide();
                     $("#dvBillWindow").show();
                     $("#dvCreditCustomerSearch").hide();
                     $("#dvHoldList").hide();
                     $("#ddlcustName").hide();
                     $("#dvOrderList").hide();
                     $('#ddlbillttype').val(['OnlinePayment']);
                     Onlinepaymet();

                 }
                 GetBillByBNFForEdit();
             }
             
             //$("#tbliteminfo").removeAttr('style');
             //$("#tbliteminfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;background-color:aquamarine!important;')
             //$("#tbamountinfo").removeAttr('style');
             //$("#tbamountinfo").attr('style', 'float:left;font-size:13px;width: 42%;background: #F5FFFA; border: 1px solid #eeeeee;background-color:aquamarine!important;')
            
         });
         function Cash() {
             $("#ddlbillttype").val(["Cash"]);
             //$("select#ddlbillttype option").filter(function () {
             //    return $(this).val() == "Cash";
             //}).prop('selected', true);
             var customerId = 0;
             CrdCustSelId = 0;
             CrdCustSelName = "";
             $("#ddlcustName").hide();
             $("#txtCashReceived").show();
             $("#lblCashHeading").show();
             $("#lblCreaditType").hide();
             //Hide All Properties//

             $("#hdnCreditCustomerId").val(0);
             $("#lblCreditCustomerName").text("");
             CrdCustSelName = "";
             $("#ddlCreditCustomers").html("");
             $("#txtddlCreditCustomers").val("");
             $("#ddlCreditCustomers").hide();
             $("#txtddlCreditCustomers").hide();
             $("#Coupans").hide();
             $("#OP").hide();
             $("#lblCashHeading").text("Cash Rec:");
             // $("#lblCashHeading").hide();
             $("#creditCustomer").css("display", "none");
             $("#coupan").hide();
            // $("#dvOuter_ddlCreditCustomers").hide();
            // $("#ddlChosseCredit").hide();
            // $("#btnAddCreditCustomer").css("display", "none");
             $("#ddlType").prop("disabled", true);
             $("#ddlType").hide();
             $("#ddlOtherPayment").hide();
             $("#COD").hide();
             $("#No").hide();
             $("#Amt").hide();
             $("#Online").hide();
             $("#txtpaymentAmount").hide();
             $("#rdoCOD").hide();
             $("#Td1").hide();
             $("#txtCODAmount").hide();
             //                   $("#ddlOtherPayment").hide();
             $("#holder input").hide();
             $("#holder2 input").hide();
             $("#No").hide();
             $("#Amt").hide();
             $("#lblCardAmount").hide();
             $("#lblCCNO").hide();
             $("#rdoOnline").hide();
             $("#lblBank").hide();

             $("#txtCashReceived").val("0").prop("readonly", false);
             $("#Text13").val("0").prop("readonly", true);
             $("#Text14").val("").prop("readonly", true);
             $("#Text15").val("0").prop("readonly", true);
             $("#Text16").val("").prop("readonly", true);
             $("#ddlType").prop("disabled", true);
             $("#ddlBank").prop("disabled", true);
             $("#txtCashReceived").val($("#txtFinalBillAmount").val());
             $("#txtBalanceReturn").val(" ");

             // $("#txtCashReceived").hide();
             $("#Text13").val("0").hide();
             $("#Text14").val("").hide();
             $("#Text15").val("0").hide();
             $("#Text16").val("").hide();
             $("#ddlType").hide();
             $("#ddlBank").hide();
             // $("#txtCashReceived").hide();
             $("#txtBalanceReturn").val(" ");

             $("#codChk").hide();
             $("#OnlineChk").hide();

         }
         $("#OnlinePaymodes").click(function ShowPaymentmode() {

             if (ProductCollection.length == 0) {
                 alert("Please first Select ProductsFor Billing");
                 return;
             }



            // ResetBillCntrols();
             localStorage.setItem('Pay', 1);


             $("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly", true);


             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindBanks",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     $("#ddlBank").html(obj.BankOptions);
                     $("#ddlBank option[value='" + DefaultBank + "']").prop("selected", true);
                 }


             });


             $("#CustomerSearchWindow").hide();
             $("#dvProductList").hide();
             $("#dvBillWindow").show();
             $("#dvCreditCustomerSearch").hide();
             $("#dvHoldList").hide();
             $("#ddlcustName").hide(); 
             $("#dvOrderList").hide();

             $("#txtCreditAmount").hide(); 
           //  $("#ddlChosseCredit").hide(); 
             $("#CreditSearchBtn").hide(); 
     
             
           //  $("#btnAddCreditCustomer").hide();
             
             $('#ddlbillttype').val(['OnlinePayment']);
             Onlinepaymet();
         });
         function Onlinepaymet() {
             // $("#ddlbillttype").removeAttr('style');
             if ($("#ddlbillttype").attr('disabled') != "disabled") {
                 $("#ddlbillttype").attr('disabled', 'disabled');
             }
             var customerId = 0;
             CrdCustSelId = 0;
             CrdCustSelName = "";
             //New Changes//
             $("#ddlOtherPayment").show();


             $("#ddlOtherPayment option:selected").val(PaymentModeID);
         
             $("#txtCODAmount").show();
             $("#txtpaymentAmount").show();
             $("#Online").show();
             $("#OnlineChk").show();
             $("#codChk").show();
             $("#COD").show();
             $("#OP").show();
             //$("#coupan").show();
             //$("#Coupans").show();
             $("#ddlcustName").hide();
             $("#OTPVal").hide();
             $("#lblOTP").hide();
             
             $("#hdnCreditCustomerId").val(0);
             $("#lblCreditCustomerName").text("");
             CrdCustSelName = "";
             $("#ddlCreditCustomers").html("");
             $("#txtddlCreditCustomers").val("");

             $("#lblCashHeading").text("Cash Rec:");
             $("#creditCustomer").css("display", "none");
             $("#coupan").hide();
             $("#dvOuter_ddlCreditCustomers").hide();
           //  $("#ddlChosseCredit").hide();
           //$("#btnAddCreditCustomer").css("display", "none");
             $("#lblCashHeading").hide();
             $("#txtCashReceived").hide();
             $("#lblCardAmount").hide();
             $("#Text13").hide();
             $("#lblCreaditType").hide();
             $("#lblCCNO").hide();
             $("#Text14").hide();
             $("#lblBank").hide();
             $("#ddlBank").hide();
             $("#ddlType").hide();
             $("#No").show();
             $("#Amt").show();
             if ($("#txtCODAmount").val() != 0) {
                 onlineoption = "COD"
             }
             $("#Coupans").val("0").prop("readonly", true);
             $("#Td1").show();
             $("#txtCODAmount").show();
             $("#holder2").hide();
             $("#holder").hide();
             $("#No").hide();
             $("#Amt").hide();


         
             $("#OP").show();
             $("#txtCashReceived").val("0").prop("readonly", true);
             //$("#Text13").val("0").prop("readonly", true);
             //$("#Text14").val("").prop("readonly", true);
             $("#Text15").val("0").prop("readonly", false);
             $("#Text16").val("").prop("readonly", false);
             //$("#ddlType").prop("disabled", true);
             //$("#ddlBank").prop("disabled", true);
             $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
             //$("#lblOTP").show();
             //$("#OTPVal").show();
         }
         $(document).ready(function () {
             $("#CreditSearchBtn").click(function () {
                 $("#CreaditCustomerPart").show();
             })
         });

         $("#btncreditsrch").click(
    function () {
        bindGrid2();
    }
    );




         $("#btnCustomer").click(
     function () {

         if (ProductCollection.length == 0) {
             alert("Please first Select ProductsFor Billing");

             return;
         }
         else {
             $("#CustomerSearchWindow").show();
             $("#dvProductList").hide();
             $("#dvBillWindow").hide();
             $("#dvCreditCustomerSearch").hide();
             $("#dvHoldList").hide();
             $("#dvOrderList").hide();
             bindGrid("M", $("#txtMobSearchBox").val());

         }



         //             $('#dvSearch').dialog(
         //            {
         //            autoOpen: false,

         //            width:720,
         //     
         //          
         //            resizable: false,
         //            modal: true,
         //                  
         //            });
         //            linkObj = $(this);
         //            var dialogDiv = $('#dvSearch');
         //            dialogDiv.dialog("option", "position", [238, 36]);
         //            dialogDiv.dialog('open');
         //            return false;



     });
         $(document).on("click", "#dvsavecombo", function (event) {
             if (con_itemnametype != "") {
                 con_itemnametype = "";
                 ComPrice = 0;
                 CountWight = 0;
                 PriceCount = 0;
                 localStorage.removeItem("SavePrice");
                 PriceCount++;
                 ComboSave();
                 $(this).parent().children().eq(3).find("input").removeAttr("disabled");
                 $(this).parent().children().eq(4).find("input").removeAttr("disabled")

             }
             else if (con_QtyCombo == "QTY_COMBO") {
                 con_QtyCombo = "";
                 ComboQtySave();
             }

         });
         $(document).on("click", "#dvDelete", function (event) {

             $(this).parent().parent().remove();

         });


         $(document).on("click", "#dvClosecombo", function () {
             var DeleteItemCode = $(this).parent().find('td').first().text()
             for (var i = 0; i < ProductCollection.length; i++) {
                 var ItemCode = ProductCollection[i].ItemCode;
                 if (DeleteItemCode == ItemCode) {
                     ProductCollection.splice($.inArray(i, ProductCollection), 1);
                 }
             }
             var SessionID = $(this).parent().attr('data-item');
             con_itemnametype = "";
             con_QtyCombo = ""
             delarrayid = SessionID;
             //$("dvsavecombo").css('display', 'none');

             $.ajax({
                 type: "POST",
                 data: '{"combosess_id": "' + SessionID + '"}',
                 url: "BillScreen.aspx/delcombodetails",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             });

             $("#dvClosecombo").parent().hide();
             $("#dvClosecombo").parent().remove();
             //$("dvClosecombo").click();
             //alert(SessionID);
             var Amount = 0;
             $("#tbProductInfo tr").each(function (i, obj) {
                 Amount += parseFloat($(obj).find('input').eq(2).val());
             });
             $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
             $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
         });

         $(document).on("click", "#dvClose", function (event) {
             var DeleteItemCode = $(this).parent().parent().find('td').first().text()
             for (var i = 0; i < ProductCollection.length; i++) {
                 var ItemCode = ProductCollection[i].ItemCode;
                 if (DeleteItemCode == ItemCode) {
                     ProductCollection = $.grep(ProductCollection, function (value) {
                         return value != DeleteItemCode;
                     });
                     // ProductCollection.splice($.inArray(ProductCollection[i], ProductCollection[i]));
                 }
             }
             var rows = $('#tbProductInfo tr').length;
             var RowIndex = Number($(this).closest('tr').index());
             var tr = $(this).closest("tr");
             tr.remove();
             if (rows == 1) {

                 $('#<%=dd_customername.ClientID %>').removeAttr("disabled");


         }
             ProductCollection.splice(RowIndex, 1);

             if (ProductCollection.length == 0) {


                 //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");
                 ClearData();


                 RestControls();



             }
             Bindtr();

         });


      

         $(document).on("click", "a[name='categories']", function (event) {

             $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
             $(this).addClass("ancSelected").removeClass("ancBasic");



         });
         $(document).on("click", "#DialogClose", function (event) {
             $("#getcombodetail").hide();

         });
         $(document).on("click", "#SaveItemCode", function (event) {

             var ItemIdArray = [];
             var AmountArray = [];
             var ItemQtyArray = [];
             var ItemPrice = [];
             var NoItem = [];
             var noitem = 0;
             var ComboValid = localStorage.getItem("CheckCoupon");

             if (ComboValid === "CouponQty") {
                 $("#tbComboInfo tr").each(function (i, obj) {
                     ComboTotalAmount += parseFloat($(obj).find('td').eq(6).find('input').val())
                     if (i === 0)
                         combosess_id = $(obj).attr('data-item')
                 })
                 var TableLength = $("#tbComboInfo tr").length
                 var divno = TableLength;
                 var adj_amt = ComboTotalAmount;
                 //comborate = adj_amt / TableLength;
                 comborate = adj_amt;
                 $("#tbProductInfo tr").each(function (i, obj) {
                     var getSessionId = $(obj).attr('data-item');
                     if (combosess_id == getSessionId) {
                         $(obj).find('input').eq(2).val(comborate);
                     }
                 });
                 for (var i = 0; i < ProductCollection.length; i++) {
                     var ItemCode = ProductCollection[i].ItemCode;
                     var EditListItemCode = $("#tbComboInfo tr").find('td').first().text();
                     if (ItemCode.trim() === EditListItemCode.trim()) {
                         ProductCollection[i].Price = comborate;
                     }
                 }


                 ComboTotalAmount = 0;
                 $("#tbComboInfo tr").each(function (i, obj) {

                     itemid = $(obj).children().eq(0).text();
                     amount = $(obj).children().find('input').eq(2).val();
                     itemqty = $(obj).children().find('input').eq(0).val();
                     itemprice = $(obj).children().find('input').eq(1).val()

                     ItemIdArray.push(itemid);
                     AmountArray.push(amount);
                     ItemQtyArray.push(itemqty);
                     ItemPrice.push(itemprice);
                 });
                 $.ajax({
                     type: "POST",
                     data: '{ "comboid": "' + ItemIdArray.toString() + '","itemid": "' + ItemIdArray.toString() + '","amount": "' + AmountArray.toString() + '","itemqty": "' + ItemQtyArray.toString() + '","comborate": "' + ItemPrice.toString() + '","combosess_id": "' + combosess_id + '","noItem": "' + "0" + '"}',
                     url: "billscreen.aspx/update_combo",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                     },
                     complete: function () {

                     }
                 });
             }
             else if (ComboValid === "CouponSave") {

                 $("#tbComboInfo tr").each(function (i, obj) {
                     ComboTotalAmount += parseFloat($(obj).find('td').eq(6).find('input').val())
                     if (i === 0)
                         combosess_id = $(obj).attr('data-item')
                 })

                 var TableLength = $("#tbComboInfo tr").length
                 var divno = TableLength;
                 var adj_amt = ComboTotalAmount;
                 comborate = adj_amt / TableLength;
                 // comborate = adj_amt;
                 $("#tbProductInfo tr").each(function (i, obj) {
                     var getSessionId = $(obj).attr('data-item');
                     if (combosess_id == getSessionId) {
                         $(obj).find('input').eq(2).val(comborate);
                     }
                 });
                 for (var i = 0; i < ProductCollection.length; i++) {
                     var ItemCode = ProductCollection[i].ItemCode;
                     var EditListItemCode = $("#tbComboInfo tr").find('td').first().text();
                     if (ItemCode.trim() === EditListItemCode.trim()) {
                         ProductCollection[i].Price = comborate;
                     }
                 }


                 ComboTotalAmount = 0;
                 $("#tbComboInfo tr").each(function (i, obj) {

                     $("#tbProductInfo tr").filter(function (i, item) {
                         if ($(item).attr("data-item") === $("#tbComboInfo tr").eq(0).attr("data-item")) {

                             itemid = $(obj).children().eq(0).text();
                             amount = $(obj).children().find('input').eq(2).val();
                             itemqty = $(item).children().eq(3).find('input').val();
                             itemprice = $(obj).children().find('input').eq(1).val();
                             noitem = $(item).children().eq(4).find('input').val();
                             var TotalAmount = (parseFloat(itemqty).toFixed(2) * parseFloat(NoItem).toFixed(2) * parseFloat(comborate).toFixed(2));
                             $(item).children().eq(7).find('input').val(TotalAmount);
                         }
                     });
                     ItemIdArray.push(itemid);
                     AmountArray.push(amount);
                     ItemQtyArray.push(itemqty);
                     ItemPrice.push(itemprice);
                     NoItem.push(noitem);
                 });

                 $.ajax({
                     type: "POST",
                     data: '{ "comboid": "' + ItemIdArray.toString() + '","itemid": "' + ItemIdArray.toString() + '","amount": "' + AmountArray.toString() + '","itemqty": "' + ItemQtyArray.toString() + '","comborate": "' + ItemPrice.toString() + '","combosess_id": "' + combosess_id + '","noItem": "' + NoItem.toString() + '"}',
                     url: "billscreen.aspx/update_combo",
                     contentType: "application/json",
                     dataType: "json",
                     success: function (msg) {

                     },
                     error: function (xhr, ajaxOptions, thrownError) {

                     },
                     complete: function () {

                     }
                 });
             }
             var Amount = 0;
             $("#tbProductInfo tr").each(function (i, obj) {
                 if ($(obj).find('input').eq(3).val() == "") {
                     Amount += parseFloat($(obj).find('input').eq(2).val());
                 }
                 else {
                     Amount += parseFloat($(obj).find('input').eq(3).val());
                 }
             });
             $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
             $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
             $("#DialogClose").click();
             //}
             // ClickEditQtyCombo = false;
         });


         $(document).on("click", "#dvQtyEditcombo", function () {
             var SessionID = $(this).parent().attr('data-item')
             localStorage.setItem("CheckCoupon", "CouponQtySave");
             //ClickEditQtyCombo = true;
             localStorage.setItem("CheckCoupon", "CouponQty");
             $("#getcombodetail").show();
             $.ajax({
                 type: "POST",
                 data: '{"combosess_id": "' + SessionID + '"}',
                 url: "BillScreen.aspx/getcombodetails",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {
                     //var obj = jQuery.parseJSON(msg.d);
                     $("#getcombodetail").css("background-color", "")
                     $("#getcombodetail").css("background-color", "turquoise")
                     var Target = $("#tbComboInfo");
                     Target.empty();
                     $.each(msg.d, function (i, item) {
                         Target.append("<tr  data-item='" + item.itemid + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + item.itemid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + item.ItemName + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;'  value='" + item.itemqty + "' id='txtBillQty'  /></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center' disabled='disabled' value='" + item.comborate + "' id='txtBillPrice'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' disabled='disabled' value='" + item.amount + "' id='txtBillAmount'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + item.amount + "' id='txtRemarks' onkeyup='javascript:AddRemarks();' value='" + item.ItemRemarks + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn()'/></td><td style='width:50px;text-align:center'><i id='dvDelete' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>")
                     })
                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
    );

         });
         $(document).on("change", "#txtBillQty", function () {
             var Qty = $(this).val();
             var Price = $(this).parent().parent().children().eq(5).find('input').val();
             var TotalPrice = parseFloat(Qty) * parseFloat(Price);
             $(this).parent().parent().children().eq(6).find('input').val(TotalPrice)
    
         });
         $(document).on("click", "#dvEditcombo", function () {
             var SessionID = $(this).parent().attr('data-item')
             localStorage.setItem("CheckCoupon", "CouponSave");
             // localStorage.setItem("ComboSaveOpen", "Open");
             //ClickEditQtyCombo = false;
             $("#getcombodetail").show();
             $("#getcombodetail").css("background-color", "")
             $("#getcombodetail").css("background-color", "darkorange")
             $.ajax({
                 type: "POST",
                 data: '{"combosess_id": "' + SessionID + '"}',
                 url: "BillScreen.aspx/getcombodetails",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var Target = $("#tbComboInfo");
                     Target.empty();
                     $.each(msg.d, function (i, item) {
                         Target.append("<tr  data-item='" + item.combosess_id + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + item.comboid + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + item.ItemName + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;' disabled='disabled' value='1' id='txtBillQty'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center' disabled='disabled' value='" + item.amount + "' id='txtBillPrice'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' disabled='disabled' value='" + Number(item.amount) + "' id='txtBillAmount'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;' style='disable:none'  data-toggle='tooltip' title='" + Number(item.amount) + "' id='txtRemarks' onkeyup='javascript:AddRemarks();' value='" + item.ItemRemarks + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn()'/></td><td style='width:50px;text-align:center'><i id='dvDelete' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>")
                     })
                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
    );

         });

         var sum = 0;
         $("#dvcountcombo").click(function () {
             var total = 0;
             $(".combo_cal").each(function () {
                 sum += $(this).attr('dir');

             });
             alert(sum)

         });


         $("#btnSearch1").click(
     function () {

         bindGrid("N", "");

     }
     );



     });

 var m_ItemId = 0;
 var m_ItemCode = "";
 var m_ItemName = "";
 var m_Qty = 0;
 var m_Price = 0;
 var m_TaxRate = 0;
 var m_Surval = 0;
 var m_ItemDiscount = 0;
 var m_EditSaleRate = 0;
 var m_CursorOn = "";
 var m_EditVal = 0;

 var ProductCollection = [];

 function clsproduct() {
     this.ItemId = 0;
     this.ItemCode = "";
     this.ItemName = "";
     this.Qty = 0;
     this.Price = 0;
     this.TaxCode = 0;
     this.SurVal = 0;
     this.ProductAmt = 0;
     this.Producttax = 0;
     this.ProductSurchrg = 0;
     this.TaxId = 0;
     this.ItemRemarks = "";
     this.ItemDiscount = 0;
     this.EditSaleRate = false;
     this.CursorOn = "";
     this.EditButton = false;
     this.EditQtyButton = false;
     this.Combo_Sessionid = 0;
     this.ComboQty_Sessionid = 0;
     this.EditVal = 0;
     this.NoItem = 0;
     this.Tax_type = 0;
 }
 var m_Total = 0;
 function Bindtr(btntext) {

     DiscountAmt = 0;
     VatAmt = 0;
     Total = 0;
     var fPrice = 0;
     TotalItems = 0;
     $("div[name='vat']").html("0.00");
     $("div[name='amt']").html("0.00");
     $("div[name='sur']").html("0.00");

     $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
	 $('#tbProductInfo1 tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
     var counterId = 1;
     BEndDiscountAmt = 0;
     var Count = 0;
     var TaxValue = 0;
     var _ComboCount = 1;
     for (var i = 0; i < ProductCollection.length; i++) {

         BillBasicType = ProductCollection[i].Tax_type;

         var Pricev = ProductCollection[i]["Price"];
         
         var TaxV = ProductCollection[i]["TaxCode"];
         
         var TaxAmtv = (Pricev * TaxV) / 100;

         TaxValue = (Number(Pricev) + Number(TaxAmtv)).toFixed(2);
		 
         //TaxValue = Number(ProductCollection[i]["Price"] + ((ProductCollection[i]["Price"] * ProductCollection[i]["TaxCode"]) / 100)).toFixed(2);
         
         //if (BillBasicType == "I") {

         //    $("#vatIncOrExc").hide();
         //}
         //else {
         //    $("#vatIncOrExc").show();
         //}


         if (ProductCollection[i].EditButton == true) {
             if (delarrayid != ProductCollection[i].Combo_Sessionid)
                 var TotalAmount = Number((ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]) * (ProductCollection[i]["NoItem"])).toFixed(2);
			 var tr = "<tr  style='background-color:darkorange;' data-item='" + ProductCollection[i].Combo_Sessionid + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:20px;text-align:center;font-size:13px;font-weight: 600;'><input type='text' class='CoQty' value=" + ProductCollection[i]["NoItem"] + " id='CoQty" + counterId + "'  style='width: 49px;'  placeholder='No.Item'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center'  value='" + TotalAmount + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'  id='dvEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             //delarrayid = 0;
         }
         if (ProductCollection[i].EditQtyButton == true) {
             if (delarrayid != ProductCollection[i].ComboQty_Sessionid)
				 var tr = "<tr  style='background-color:turquoise;' data-item='" + ProductCollection[i].ComboQty_Sessionid + "'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;'  value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input  type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center'  value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'  id='dvQtyEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
         }
         else if (_ComboCount == ProductCollection.length && con_itemnametype == "COMBO") {
			 var tr = "<tr class='SumMainCombo' style='background-color:darkorange;'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQtynew' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;'  disabled='disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:20px;text-align:center;font-size:13px;font-weight: 600;'><input type='text'  disabled='disabled' class='CoQty' value='1' style='width: 49px;'  placeholder='No.Item'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' class='comboamt' name='txtBillAmount' style='width:70px;text-align:center'  value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td  id='dvClosecombo' style='width:50px;text-align:center'><i style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'  id='dvsavecombo'><i  style='cursor:pointer'><img src='images/save.png'/></i></td><td style='width:50px;text-align:center'  id='dvEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             Storeval = ProductCollection.length;
             countecombo++;
         }
         else if (_ComboCount == ProductCollection.length && con_QtyCombo == "QTY_COMBO") {
			 var tr = "<tr class='SumMainQtyCombo' style='background-color:turquoise'><td style='width:30px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px;font-weight: 600;'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align:center;display:none;font-weight: 600;'>-</div></td><td style='width:25px;text-align:center;font-weight: 600;'><input type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center;font-weight: 600;' disabled='disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center;font-weight: 600;'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center;display:none;'>+</div></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillPrice'  name='txtBillPrice' style='width:70px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtBillAmount' class='comboQtyamt' name='txtBillAmount' style='width:70px;text-align:center' disabled='disabled' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none;font-weight: 600;'><input type='text'  style ='width:70px;padding:0px;font-weight: 600;'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td  id='dvClosecombo' style='width:50px;text-align:center'><i style='cursor:pointer'><img src='images/trash.png'/></i></td><td style='width:50px;text-align:center'  id='dvsavecombo'><i  style='cursor:pointer'><img src='images/save.png'/></i></td><td style='width:50px;text-align:center'  id='dvQtyEditcombo'><i  style='cursor:pointer'><img src='images/edit.png'/></i></td></tr>";
             Storeval = ProductCollection.length;
             countecombo++;
         }
         else {
             if (ProductCollection[i]["EditSaleRate"] == true) {
                 if (delarrayid != ProductCollection[i].Combo_Sessionid) {
                     if (ProductCollection[i]["Qty"] < 0) {
						 var tr = "<tr style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

                     }
                     else {
						 var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                     }
                 }
                 else {
					 var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text' style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                 }
             }
             else {
                 if (ProductCollection[i]["EditSaleRate"] == false && ProductCollection[i].EditButton == false) {

                     if (ProductCollection[i]["Qty"] < 0) {
						 var tr = "<tr  style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                     }

                     else {
                         if (ProductCollection[i]["EditVal"] == 1)
                         {
							 var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' disabled='disabled' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input  disabled='disabled' class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus' disabled='disabled' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input disabled='disabled' type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input disabled='disabled' type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer;display:none'><img src='images/trash.png'/></i></td></tr>";
                         }
                         else
                         {
							 var tr = "<tr><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center'  value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                             var amt = 0;
                             amt =  Number(ProductCollection[i]["Price"]);
                             KotAmt = KotAmt + amt;
                         }
                         


                     }
                 }
                 else if (ProductCollection[i].EditButton == false) {
                     if (ProductCollection[i]["Qty"] < 0) {
						 var tr = "<tr  style='background: red;'><td style='width:30px;text-align:center;font-size:13px;'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                       
                     }
                     else {
						 var tr = "<tr ><td style='width:30px;text-align:center;font-size:13px;background-color:#ffffff'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:200px;text-align:center;font-size:13px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:25px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>-</div></td><td style='width:25px;text-align:center'><input class='qty' type='txtBillQty' name='txtBillQty' style='width:38px;height:30px;font-size:13px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:25px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:25px;text-align: center'>+</div></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillPrice' name='txtBillPrice' style='width:70px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input disabled = 'disabled' type='txtTaxRate'  name='txtTaxRate' style='width:70px;text-align:center'  value='" + ProductCollection[i]["TaxCode"] + "' id='txtTaxRate" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px;font-weight: 600;'><input type='txtTaxValue'  name='txtTaxValue' style='width:70px;text-align:center'  value='" + TaxValue + "' id='txtTaxValue" + counterId + "'/></td><td style='width:70px;text-align:center;font-size:17px'><input type='txtBillAmount' name='txtBillAmount' style='width:70px;text-align:center' value='" + Number(ProductCollection[i]["Price"] * ProductCollection[i]["Qty"]).toFixed(2) + "' id='txtBillAmount" + counterId + "'/></td><td style='width:150px;text-align:center;display:none'><input type='text'  style ='width:70px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";


                     }
                 }


             }

         }

         $("#tbProductInfo").append(tr);
		 $("#tbProductInfo1").append(tr);
         if (ProductCollection[i]["CursorOn"] == "QTY") {
             var searchInput = $("#txtBillQty" + counterId + "");
         }
         else if (ProductCollection[i]["CursorOn"] == "RATE") {
             var searchInput = $("#txtBillPrice" + counterId + "");
         }
         else if (ProductCollection[i]["CursorOn"] == "AMOUNT") {
             var searchInput = $("#txtBillAmount" + counterId + "");
         }

         counterId = counterId + 1;

         Count = Count + 1;

         fPrice = 0;
         fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
         var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);
         var Peritemdiscount = (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
         //if (BillBasicType == "I") {


         //    if (ProductCollection[i]["SurVal"] == "0") {
         //        var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
         //    }
         //    else {

         //        var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
         //       // var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"] - ProductCollection[i]["SurVal"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
         //        var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));

         //    }
         //}
         //else
         //{

             var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);


        // }


         var surchrg = ((Number(fPrice) - Number(Peritemdiscount)) * Number(ProductCollection[i]["SurVal"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
         var tottax = Number(TAx);

         
       //  if (BillBasicType == "E") {

             VatAmt = VatAmt + tottax;
       //  }




         Total = Total + fPrice;
         ProductCollection[i]["ProductAmt"] = fPrice;
         ProductCollection[i]["Producttax"] = TAx;
         ProductCollection[i]["ProductSurchrg"] = surchrg;


         var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
         var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
         var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
         $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
         $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
         $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


         BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);

         if (ProductCollection[i]["Qty"] < 1) {

             TotalItems = TotalItems + Number(1);


         }
         else {

             TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
         }


         _ComboCount++;
     };

     
     CommonCalculation();




     if (ProductCollection.length == 0) {
         var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
         $("#tbProductInfo").append(tr);
		 $("#tbProductInfo1").append(tr);
     }

     $("#CustomerSearchWindow").hide();
	 $("#dvProductList").show();
     $("#dvBillWindow").hide();
     $("#dvCreditCustomerSearch").hide();
     $("#dvHoldList").hide();
     $("#dvOrderList").hide();
	 $("#dvProductList1").show();
     //var Amount = 0;
     //$("#tbProductInfo tr").each(function (i, obj) {
     //    if ($(obj).find('input').eq(3).val() == "") {
     //        Amount += parseFloat($(obj).find('input').eq(2).val());
     //    }
     //    else {
     //        Amount += parseFloat($(obj).find('input').eq(3).val()); 
     //    }
     //});
     //var Round_Amt = $("#dvRound").html();
     //var NetAmount = (parseFloat(Amount).toFixed(2) - parseFloat(Round_Amt).toFixed(2))
     //$("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
     //$("#dvnetAmount").html(parseFloat(NetAmount).toFixed(2));

 }
 //$(function () {
 //    function ChangeCombo(td) {
 //        var NoOFItem = $(td).val()
 //        var WigthtAmount = $(td).parent().nextAll().eq(3).find('input').val();
 //        var Calculation = (parseFloat(NoOFItem) * parseFloat(WigthtAmount));
 //        $(td).parent().nextAll().eq(3).find('input').val(Calculation);
 //    }
 //});

 var FinalWeightRate = 0;

 $(document).on('keyup', "input[class='CoQty']", function () {
     var NoOFItem = $(this).val()
     var ItemWeight = $(this).parent().parent().children().eq(3).find('input').val();
     if (CountWight == 0) {
         ComPrice = $(this).parent().parent().children().eq(6).find('input').val();
         //localStorage.setItem('Weight', Price);
     }
     // var Wt = localStorage.getItem('Weight');
     FinalWeightRate = ((parseFloat(ComPrice)).toFixed(2) * (parseFloat(ItemWeight)) * (parseFloat(NoOFItem)).toFixed(2));
     $(this).parent().nextAll().eq(2).find('input').val(parseFloat(FinalWeightRate).toFixed(2));
     CountWight++;
     ComboSaveWithWeight(this);
 })
 function check() {
     var count = 0
     $("#tbProductInfo tr").each(function (i, obj) {
         console.log(obj);
         if ($(obj).attr('style') != undefined && count == 0) {
             var style = $(obj).attr('style');
             count = 1;
         }
         else if (count == 1) {
             $(obj).attr("style", "background-color:darkorange");
             $(obj).attr('class', 'combo_cal');

         }
     })
 }

 function BindtrDiscount(disc)
 {
     
     DiscountAmt = 0;
     VatAmt = 0;
     Total = 0;
     var fPrice = 0;
     TotalItems = 0;
     $("div[name='vat']").html("0.00");
     $("div[name='amt']").html("0.00");
     $("div[name='sur']").html("0.00");

     $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
     var counterId = 0;
     BEndDiscountAmt = 0;
     var Count = 0;

     for (var i = 0; i < ProductCollection.length; i++) {

         ProductCollection[i]["ItemDiscount"] = disc;

         var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);

         if (ProductCollection[i]["ChallanNo"] == "") {

             if (RoleForEditRate == true) {

                 var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

             }
             else {

                 if (ProductCollection[i]["EditSaleRate"] == false) {

                     var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                 }
                 else {
                     var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                 }

             }
         }
         else {

             if (RoleForEditRate == true) {

                 var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' disabled = 'disabled' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center;' disabled = 'disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus' disabled = 'disabled'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' disabled = 'disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center;display:none'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";

             }
             else {

                 if (ProductCollection[i]["EditSaleRate"] == false) {

                     var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' disabled = 'disabled' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center;' disabled = 'disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus' disabled = 'disabled'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' disabled = 'disabled' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center;display:none'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                 }
                 else {
                     var tr = "<tr><td style='width:80px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:180px;text-align:center;font-size:17px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><div id='btnMinus' disabled = 'disabled' class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>-</div></td><td style='width:50px;text-align:center'><input type='txtBillQty' style='width:40px;text-align:center;' disabled = 'disabled' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty'/></td><td style='width:50px;text-align:center'><div id='btnPlus' disabled = 'disabled'  class='btn btn-primary btn-small' style='height:30px;width:30px;text-align: center'>+</div></td><td style='width:100px;text-align:center;font-size:17px'><input type='txtBillPrice' disabled = 'disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + "' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td><td style='width:50px;text-align:center;display:none'><i id='dvClose' style='cursor:pointer'><img src='images/trash.png'/></i></td></tr>";
                 }

             }

         }

         counterId = counterId + 1;

         Count = Count + 1;
         $("#tbProductInfo").append(tr);

         fPrice = 0;
         fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);

         //if (BillBasicType == "I") {


         //    if (ProductCollection[i]["SurVal"] == "0") {
         //        var TAx = ((Number(fPrice) - ((parseFloat(fPrice) * parseFloat(itemDiscount) / 100))) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
         //    }
         //    else {

         //        var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
         //      // var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"]) + Survalll));
         //       var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));



         //    }
         //}
         //else {

             var TAx = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["TaxCode"]) / 100);


      //   }


         var surchrg = ((Number(fPrice) - (parseFloat(fPrice) * parseFloat(itemDiscount) / 100)) * Number(ProductCollection[i]["SurVal"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
         //var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
         var tottax = Number(TAx) + Number(surchrg);


         var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);
         
         VatAmt = VatAmt + tottax;
         Total = Total + fPrice;
         ProductCollection[i]["ProductAmt"] = fPrice;
         ProductCollection[i]["Producttax"] = TAx;
         ProductCollection[i]["ProductSurchrg"] = surchrg;


         var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
         var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
         var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
         $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
         $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
         $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


         BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);

         if (ProductCollection[i]["Qty"] < 1) {

             TotalItems = TotalItems + Number(1);


         }
         else {

             TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
         }




     }


     CommonCalculation();

     if (ProductCollection.length == 0) {
         var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
         $("#tbProductInfo").append(tr);
     }

     $("#CustomerSearchWindow").hide();
     $("#dvProductList").show();
     $("#dvBillWindow").hide();
     $("#dvCreditCustomerSearch").hide();
     $("#dvHoldList").hide();
     $("#dvOrderList").hide();


        }

		function CommonCalculation() {

			var m_Total = Total.toFixed(2);
			$("div[id='dvsbtotal']").html(m_Total);



			$("div[id='dvVat']").html(VatAmt.toFixed(2));
			TaxAmt = GetServiceTax();


			//if (BillBasicType == "E") {

				if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
				$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
				$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
			}
			else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

				if (Takeaway == 1) {
					$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
					$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
				}
				else {

					$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
					$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));


				}

			}
		//}
		<%--else {

			if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

				$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
				$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

			}
			else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
					 if (Takeaway == 1) {
						 $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
						 $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					 }
					 else {
						 $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
						 $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					 }


				 }

			 }--%>
			 var BEndDiscountPer = 0;


			 if (BackEndDiscount == 1) {



				 BEndDiscountPer = BEndDiscountAmt * 100 / m_Total;

				 $("#dvdisper").val(BEndDiscountPer.toFixed(2));
			 }


			 if (DiscountOnBillValue == 1) {

				 for (var i = 0; i < DiscountValues.length; i++) {

					 var StartValue = DiscountValues[i]["StartValue"];
					 var EndValue = DiscountValues[i]["EndValue"];
					 var DisPer = DiscountValues[i]["DisPer"];


					 if (parseFloat(m_Total) >= parseFloat(StartValue) && parseFloat(m_Total) <= parseFloat(EndValue)) {

						 $("#dvdisper").val(DisPer.toFixed(2));
						 break;

					 }
					 else {
						 $("#dvdisper").val(0);

					 }



				 }
			 }



			 DiscountAmt = (Number(Total) * Number($("#dvdisper").val())) / 100;


			 $("#dvdiscount").val(DiscountAmt.toFixed(2));


			 if (RoundBillAmount == 1) {
				// if (BillBasicType == "E") {

					 if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

					$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
				}
				else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
					if (Takeaway == 1) {
						$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));

					}
					else {
						$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
					}
				}

			//}
			<%--else {

				if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

					$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
				}
				else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

					if (Takeaway == 1) {
						$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
					}
					else {
						$("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
					}
				}


			}--%>
		}
		else {
			//if (BillBasicType == "E") {

				if ($("#<%=ddloption.ClientID %>").val() == "Dine") {

					$("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
				}
				else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
					if (Takeaway == 1) {
						$("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					} else {

						$("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					}


				}
			//}
			<%--else {
				if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
					$("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
				} else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

						 if (Takeaway == 1) {
							 $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
						 } else {
							 $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
						 }
					 }


				 }--%>


			 }


			// if (BillBasicType == "E") {

				 if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
				$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
			}
			else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {
				if (Takeaway == 1) {
					$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
				} else {
					$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
				}

			}
	//	}
		<%--else {

			if ($("#<%=ddloption.ClientID %>").val() == "Dine") {
				$("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
			}
			else if ($("#<%=ddloption.ClientID %>").val() == "TakeAway") {

					 if (Takeaway == 1) {
						 $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					 } else {
						 $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
					 }
				 }

			 }--%>


			 var netamount = $("div[id='dvnetAmount']").html();
			 var hdnNetamt = $("#hdnnetamt").val();
			 var Round = Number(netamount) - Number(hdnNetamt)



			 $("div[id='dvRound']").html(Round.toFixed(2));


			 $("#lblNoItems").html(TotalItems);

			 //alert(Total);
			 //alert(TaxAmt);
			 //alert(VatAmt);
			 //alert(DiscountAmt);

			 //alert($("#hdnnetamt").val());
			 //alert($("div[id='dvnetAmount']").html());


		 }

        var OnLoad = 0;

		


        function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount, B_Qty, EditSaleRate, CursorOn, btntext, EditVal,TaxType) {

            var ExistedKot = 0;
 <%--         
            if ($("#<%=ddloption.ClientID%>").val() == 'Dine' && OnLoad==1) {


                ExistedKot = 1;
                OnLoad == 1;
            }
            else {

                ExistedKot = 0;
            }--%>
    m_ItemId = ProductId;
    m_ItemCode = Code;
    m_Price = Price;
    m_ItemName = Name;
    m_EditSaleRate = EditSaleRate;
    m_CursorOn = CursorOn;
    m_EditVal = EditVal;

    if (B_Qty == 0) {
        if (modeRet == "Return") {
            m_Qty = -1;
        }
        else {
            m_Qty = 1;
        }
    }
    else {
        m_Qty = B_Qty;
    }
    m_TaxRate = TaxCode;
    m_Surval = SurVal;
    m_Tax_Id = Tax_Id;
    m_ItemDiscount = Discount;

    TO = new clsproduct();
    var ExistItem = "";
    for (var i = 0; i < ProductCollection.length; i++) {
        if (m_ItemName === ProductCollection[i].ItemName) {
            ExistItem = "Exist";
        }
    }
    var Amount = 0;
    var Qty = 0;
    if (ProductCollection.length == 0) {
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;
        TO.CursorOn = m_CursorOn;
        TO.EditVal = m_EditVal;
        TO.Tax_type = TaxType;
        ProductCollection.push(TO);

    }
    else if (ExistItem != "" && EditVal==1) {
        for (var i = 0; i < ProductCollection.length; i++) {
            if (m_ItemName === ProductCollection[i].ItemName) {
                Amount = (parseFloat(Amount) + parseFloat(ProductCollection[i].Price));
                Qty = parseFloat(ProductCollection[i].Qty)
                ProductCollection[i].Qty = parseFloat(Qty) + parseFloat(m_Qty);
            }
        }

    }
    else {
        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;
        TO.CursorOn = m_CursorOn;
        TO.EditVal = m_EditVal;
        TO.Tax_type = TaxType;
        ProductCollection.push(TO);

    }
    Bindtr(btntext);
    
    if (StartCursor=="ItemCodeQty") {
    	$("#tbProductInfo tbody tr").last().children().find('input').eq(0).select();
    }
  
    var textControlID = $("#tbProductInfo tbody tr").last().children().find('input').eq(0).attr('id')

    // $("#" + textControlID).caretToEnd();

}


function GetServiceTax() {


    var TaxAmt = 0;


    $("#dvsertaxper").html(Sertax);
    var Dis = $("#dvdiscount").val();
    if (option == "TakeAway") {
        if (Takeaway == "1") {

            TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
            $("#dvTax").html(TaxAmt.toFixed(2));


        }
        else {
            $("#dvsertaxper").html("0");
            TaxAmt == "0";
            $("#dvTax").html(TaxAmt.toFixed(2));
        }
    }
    else if (option == "Dine") {
        TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
        $("#dvTax").html(TaxAmt.toFixed(2));


    }


    return TaxAmt;

}


function GetKKC() {


    $("#dvKKCPer").html(KKC);

    if (option == "TakeAway") {
        if (Takeaway == "1") {

            KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
            $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


        }
        else {
            $("#dvKKCPer").html("0");
            KKCTAmt == "0";
            $("#dvKKCAmt").html(KKCTAmt.toFixed(2));
        }
    }
    else if (option == "Dine") {
        KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
        $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


    }


    return KKCTAmt;

}


function GetSBC() {


    $("#dvSBCPer").html(SBC);

    if (option == "TakeAway") {
        if (Takeaway == "1") {

            SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
            $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



        }
        else {
            $("#dvSBCPer").html("0");
            SBCTAmt == "0";
            $("#dvSbCAmt").html(SBCTAmt.toFixed(2));

        }
    }
    else if (option == "Dine") {
        SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
        $("#dvSBCAmt").html(SBCTAmt.toFixed(2));



    }


    return SBCTAmt;

}


function Search(CatId, Keyword) {



    $.ajax({
        type: "POST",
        data: '{"CategoryId": "' + CatId + '","Keyword": "' + Keyword + '"}',
        url: "screen.aspx/AdvancedSearchForGST",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

            var obj = jQuery.parseJSON(msg.d);

            $("#products").html(obj.productData);



        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {

        }

    }
   );


}

//........................................


        //$(document).on("keyup", "input[name='txtBillQty']", function (event) {

$(document).on("keydown", "input[name='txtBillQty']", function (event) {

    var keycode = (event.keyCode ? event.keyCode : event.which);

    if (keycode == '13') {
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];



        var fQty = $(this).val();
        if (isNaN(fQty)) {
            fQty = 1;

        }
        ProductCollection[RowIndex]["Qty"] = fQty;

        Bindtr();
        if (StartCursor == "ItemName") {

            $("#txtSearch").focus();

        }
        else {


            $("#txtItemCode").focus();

        }
    }

    else if (keycode == '9')
    {


        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];



        var fQty = $(this).val();
        if (isNaN(fQty)) {
            fQty = 1;

        }
        ProductCollection[RowIndex]["Qty"] = fQty;
}

});

$(document).on("keyup", "input[name='txtBillQtynew']", function (event) {
    // var keycode = (event.keyCode ? event.keyCode : event.which);
    // if (keycode == '13') {
    var Calculation = 0;
    var NOWt = $(this).val();
    var getprice = 0;
    var CalAmount = $(this).parent().nextAll().eq(3).find('input').val();
    var NoItem = $(this).parent().nextAll().eq(0).find('input').val();
    // var Item = localStorage.getItem("ComboSaveOpen")
    getprice = localStorage.getItem('SavePrice');
    if (NOWt != ".") {
        if (PriceCount == 0) {
            Calculation = (parseFloat(CalAmount) * (parseFloat(NOWt)).toFixed(2)) * (parseFloat(NoItem)).toFixed(2);
            $(this).parent().nextAll().eq(3).find('input').val(parseFloat(Calculation).toFixed(2));
            $(this).parent().nextAll().eq(2).find('input').val(parseFloat(CalAmount).toFixed(2));

        }
        else if (getprice != 0) {
            var SumAmount = $(this).parent().nextAll().eq(3).find('input').val();
            getprice = $(this).parent().nextAll().eq(2).find('input').val();
            if (parseFloat(SumAmount) > parseFloat(getprice) && parseFloat(getprice) > 1) {
                Calculation = (parseFloat(getprice) * (parseFloat(NOWt)).toFixed(2)) * (parseFloat(NoItem)).toFixed(2);
                $(this).parent().nextAll().eq(3).find('input').val(parseFloat(Calculation).toFixed(2));
                $(this).parent().nextAll().eq(2).find('input').val(parseFloat(getprice).toFixed(2));
            }
            else {
                Calculation = ((parseFloat(SumAmount)).toFixed(2) * (parseFloat(NOWt)) * (parseFloat(NoItem)).toFixed(2));
                $(this).parent().nextAll().eq(2).find('input').val(parseFloat(SumAmount).toFixed(2));
                $(this).parent().nextAll().eq(3).find('input').val(parseFloat(Calculation).toFixed(2));
            }
        }
        if (PriceCount == 0) {
            var RealAmount = $(this).parent().nextAll().eq(2).find('input').val();
            localStorage.setItem('SavePrice', RealAmount);
        }
        PriceCount++;
        ComboSaveWithWeight(this);
    }

});
function ComboSaveWithWeight(div) {
    var ComboSesID = $(div).parent().parent().attr('data-item');
    var QtyWt = $(div).parent().parent().children().eq(3).find('input').val();
    var NoItem = $(div).parent().parent().children().eq(4).find('input').val();
    var ComboRate = $(div).parent().parent().children().eq(6).find('input').val();

    for (var i = 0; i < ProductCollection.length; i++) {
        var ItemCode = ProductCollection[i].ItemCode;
        var EditListItemCode = $(div).parent().parent().children().eq(0).text();
        if (ItemCode.trim() === EditListItemCode.trim()) {
            ProductCollection[i].Price = ComboRate;
            ProductCollection[i].Qty = QtyWt;
            ProductCollection[i].NoItem = NoItem;
        }
    }

    $.ajax({
        type: "POST",
        data: '{"ComboSesID": "' + ComboSesID + '","QtyWt": "' + QtyWt + '","NoItem": "' + NoItem + '","ComboRate": "' + ComboRate + '"}',
        url: "BillScreen.aspx/update_comboWtQty",
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {

        },
        error: function (xhr, ajaxOptions, thrownError) {

            var obj = jQuery.parseJSON(xhr.responseText);
            alert(obj.Message);
        },
        complete: function () {
            var Amount = 0;
            $("#tbProductInfo tr").each(function (i, obj) {
                if ($(obj).find('input').eq(3).val() == "") {
                    Amount += parseFloat($(obj).find('input').eq(2).val());
                }
                else {
                    Amount += parseFloat($(obj).find('input').eq(3).val());
                }
            });
            $("#dvsbtotal").html(parseFloat(Amount).toFixed(2));
            $("#dvnetAmount").html(parseFloat(Amount).toFixed(2));
        }
    });
}
$(document).on("keyup", "input[name='txtBillPrice']", function (event) {

    var keycode = (event.keyCode ? event.keyCode : event.which);

    if (keycode == '13') {

        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = $(this).closest('tr').find('qty').text();
        var Price = ProductCollection[RowIndex]["Price"];



        var fPrice = $(this).val();
       

        ProductCollection[RowIndex]["Price"] = fPrice;

        Bindtr();
        //$("#txtSearch").focus();

        if (StartCursor == "ItemName") {

            $("#txtSearch").focus();

        }
        else {


            $("#txtItemCode").focus();

        }

    }

});



$(document).on("keyup", "input[name='txtBillAmount']", function (event) {

    var keycode = (event.keyCode ? event.keyCode : event.which);

    if (keycode == '13') {

        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];



        var fAmount = $(this).val();
        var fqty = (Number(fAmount) / Number(Price));

        fqty = parseFloat(parseFloat(fqty).toFixed(3))

        ProductCollection[RowIndex]["Qty"] = fqty;

        Bindtr();
       // $("#txtSearch").focus();

    }

});

		$(document).on("keyup", "input[name='txtTaxValue']", function (event) {

			var keycode = (event.keyCode ? event.keyCode : event.which);

			if (keycode == '13') {

				var RowIndex = Number($(this).closest('tr').index());
				var PId = ProductCollection[RowIndex]["ItemId"];

				var Mode = "Plus";

				var Qty = ProductCollection[RowIndex]["Qty"];
				var Price = ProductCollection[RowIndex]["Price"];
				var TaxCode = ProductCollection[RowIndex]["TaxCode"];


                var ftaxValue = $(this).val();
                var fprice = Number(ftaxValue - ((ftaxValue * TaxCode) / (100 + TaxCode)));
                
                fprice = parseFloat(parseFloat(fprice).toFixed(2))

                ProductCollection[RowIndex]["Price"] = fprice;

				Bindtr();
				// $("#txtSearch").focus();

			}

		});


$(document).on("click", "#btnPlus", function (event) {


    var RowIndex = Number($(this).closest('tr').index());
    var PId = ProductCollection[RowIndex]["ItemId"];

    var Mode = "Plus";

    var Qty = ProductCollection[RowIndex]["Qty"];
    var Price = ProductCollection[RowIndex]["Price"];
    var fQty = 0;
    if (Qty < 0) {
        fQty = Number(Qty) + (-1);
    }
    else {
        fQty = Number(Qty) + 1;
    }




    ProductCollection[RowIndex]["Qty"] = fQty;

    Bindtr();

});


$(document).on("click", "#btnMinus", function (event) {

    var RowIndex = Number($(this).closest('tr').index());
    var PId = ProductCollection[RowIndex]["ItemId"];
    var Mode = "Minus";

    var Qty = ProductCollection[RowIndex]["Qty"];
    var Price = ProductCollection[RowIndex]["Price"];

    var fQty = 0;
    if (Qty < 0) {
        fQty = Number(Qty) - (-1);
    }
    else {
        fQty = Number(Qty) - 1;
    }

    ProductCollection[RowIndex]["Qty"] = fQty;
    if (fQty == "0") {
        ProductCollection.splice(RowIndex, 1);
    }

    Bindtr();


});



//............................................





    </script>
   
        <script>


            function BindBanks() {
                $.ajax({
                    type: "POST",
                    data: '{}',
                    url: "screen.aspx/BindBanks",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#ddlBank_pm").html(obj.BankOptions);
                        $("#ddlBank_pm option[value='" + DefaultBank + "']").prop("selected", true);
                    }


                });
            }








            function DEVBalanceCalculation_pm() {


                //var txtCashReceived = $("#txtCashReceived_pm");
                //var txtCreditCard = $("#Text13_pm");
                //var txtCheque = $("#Text15_pm");
                //var txtFinalBillAmount = $("#txtFinalBillAmount_pm");


                var txtCashReceived = $("#txtCashReceived_pm");
                var txtCreditCard = $("#Text13_pm");
                var txtCheque = $("#Text15_pm");
                var txtFinalBillAmount = $("#txtFinalBillAmount_pm");
                var txtOnlineAmount = $("#txtpaymentAmount_pm");
                var txtCoupanAmount = $("#Coupans_pm");
                var txtCODAmount = $("#txtCODAmount_pm");


                if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                    txtCreditCard.val(0);
                    txtCheque.val(0);
                }
                else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                    txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                    txtCheque.val(0);

                }
                else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                    txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

                }
                var balReturn = Number((Number(txtCashReceived.val()) + Number(txtOnlineAmount.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) + Number(txtCoupanAmount.val()) + Number(txtCODAmount.val()) - Number(txtFinalBillAmount.val())));

                $("#txtBalanceReturn_pm").val(balReturn.toFixed(2));


            }


            ////////////getbillnoedit//////////

            

            function GetBillByBNFForEdit()
            {

                var Mode = "";
                //var BNF = $("#dvShow").attr('dir');
                //var BNF = $("#txtBillNumber").val();
                //if (BNF == "") {

                //    var PreBNF = $("#dvShow").attr('dir');
                //    $("#txtBillNumber").val(PreBNF);
                //    BNF = PreBNF;
                //    //$("#btnBillWindowOk").removeAttr("disabled");

                //}


                $.ajax({
                    type: "POST",
                    data: '{ "BNF": "' + m_BillNowPrefix + '"}',
                    url: "changePaymode.aspx/GetBillByBNF",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#rdoOnline").prop('checked', false);
                        $("#rdoCOD").prop('checked', false);
                        $("#<%=ddlOtherPayment.ClientID %>").val(obj.OtherPaymode);

                    $("#ddlbillttype").val(obj.BillData.BillMode);
                    $("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html());
                    if ($("#txtFinalBillAmount").val() != 0) {

                        $("#btnBillWindowOk").removeAttr("disabled");

                    }
                    else {

                        alert("BillNo Does Not Exists");
                        return;

                    }

                    $("#Text13").val(obj.BillData.CrCard_Amount);
                    $("#ddlBank").val(obj.BillData.CreditBank);
                    if (obj.BillData.BillMode == "Credit") {

                        $("#ddlChosseCredit").val(obj.BillData.Customer_ID);
                        $("#lblCreditCustomerName").html(obj.BillData.Customer_Name);
                        $("#creditCustomer").css("display", "block");
                      //  $("#dvOuter_ddlCreditCustomers").show();
                    //  $("#ddlChosseCredit").show() ;$("#btnAddCreditCustomer").css("display", "block");
                        $("#ddlcustName").show();
                        $("#txtCashReceived").val($("div[id='dvnetAmount']").html());
                        Mode = "Credit";
                        CheckModeForedit(Mode)
                    }
                    else if (obj.BillData.BillMode == "OnlinePayment") {
                        Mode = "OnlinePayment";
                        CheckModeForedit(Mode)
                        $("#txtCashReceived").val($("div[id='dvnetAmount']").html());
                        if (obj.Mode == 'OnlinePayment') {
                            $("#rdoOnline").prop('checked', true);
                            $("#txtCODAmount").prop('checked', false);
                            $("#txtpaymentAmount").val($("div[id='dvnetAmount']").html());
                            $("#txtpaymentAmount").attr('readonly', 'readonly');
                            $("#txtCODAmount").attr('readonly', 'readonly');
                            $("#ddlcustName").hide();
                            $("#OTPVal").val(obj.OTPVal);
                            if (obj.Amount != 0)
                            {
                                $("#txtBalanceReturn").val(0);
                            }
                        }
                        else if (obj.Mode == 'COD') {
                            $("#rdoCOD").prop('checked', true);
                            $("#rdoOnline").prop('checked', false);
                            $("#txtCODAmount").val($("div[id='dvnetAmount']").html());
                            $("#txtCODAmount").attr('readonly', 'readonly');
                            $("#txtpaymentAmount").attr('readonly', 'readonly');
                            if (obj.Amount != 0) {
                                $("#txtBalanceReturn").val(0);
                            }
                            $("#ddlcustName").hide();
                            $("#OTPVal").val(obj.OTPVal);
                        }
                    }
                    else if (obj.BillData.BillMode == "Cash") {
                        Mode = "Cash";
                        CheckModeForedit(Mode)
                        $("#ddlcustName").hide();
                       // $("#txtFinalBillAmount").val($("div[id='dvnetAmount']").html()).prop("readonly", true);
                        //$("#txtCashReceived").val(obj.BillData.Cash_Amount);
                        $("#txtCashReceived").val($("div[id='dvnetAmount']").html());
                    }
                    else if (obj.BillData.BillMode == "CreditCard") {
                        Mode = "CreditCard";
                        CheckModeForedit(Mode)
                        $("#txtCashReceived").val($("div[id='dvnetAmount']").html());
                    }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        if ($("#ddlbillttype").val() == "Cash" || $("#ddlbillttype").val() == "OnlinePayment") {
                            $("#Text13").attr("disabled", "disabled");
                            $("#ddlBank").attr("disabled", "disabled");
                            $("#txtCashReceived").removeAttr("disabled");
                            $("#dvOuter_ddlCreditCustomers").hide();
                        //  $("#ddlChosseCredit").hide();
                        //  $("#btnAddCreditCustomer").css("display", "none");
                          //  $("#ddlChosseCredit").val(0);
                        }
                        else if ($("#ddlbillttype").val() == "CreditCard") {

                            $("#Text13").removeAttr("disabled");
                            $("#ddlBank").removeAttr("disabled");
                            $("#txtCashReceived").attr("disabled", "disabled");
                            $("#dvOuter_ddlCreditCustomers").hide();
                        //  $("#ddlChosseCredit").hide();
                        //  $("#btnAddCreditCustomer").css("display", "none");
                          //  $("#ddlChosseCredit").val(0);
                            $("#ddlcustName").hide();
                        }
                        else if ($("#ddlbillttype").val() == "Credit") {
                            $("#Text13").attr("disabled", "disabled");
                            $("#ddlBank").attr("disabled", "disabled");
                            $("#txtCashReceived").removeAttr("disabled");
                          //  $("#dvOuter_ddlCreditCustomers").show();
                        //  $("#ddlChosseCredit").show() ;$("#btnAddCreditCustomer").css("display", "block");
                          ///  $("#ddlcustName").show();
                        }


                        DEVBalanceCalculation();

                        if ($("#txtCODAmount").val() != "0" || $("#txtpaymentAmount").val() != "0")
                        {
                            $("#txtBalanceReturn").val(0);
                        }


                        if ($("#ddlbillttype").val() == "Credit") {
							$("#txtBalanceReturn").val(0);
                        }
                        $.uiUnlock();

                    }

                });

        };




            //////////////////////////////////
            function GetBillByBNF() {

                var Mode = "";
                //var BNF = $("#dvShow_pm").attr('dir');
                var BNF = $("#txtBillNumber_pm").val();
                if (BNF == "") {

                    var PreBNF = $("#dvShow_pm").attr('dir');
                    $("#txtBillNumber_pm").val(PreBNF);
                    BNF = PreBNF;
                    //$("#btnBillWindowOk_pm").removeAttr("disabled");

                }


                $.ajax({
                    type: "POST",
                    data: '{ "BNF": "' + BNF + '"}',
                    url: "changePaymode.aspx/GetBillByBNF",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#rdoOnline_pm").prop('checked', false);
                        $("#rdoCOD_pm").prop('checked', false);
                        $("#<%=ddlOtherPayment_pm.ClientID %>").val(obj.OtherPaymode);

                    $("#ddlbillttype_pm").val(obj.BillData.BillMode);
                    $("#txtFinalBillAmount_pm").val(obj.BillData.Net_Amount);
                    if ($("#txtFinalBillAmount_pm").val() != 0) {

                        $("#btnBillWindowOk_pm").removeAttr("disabled");

                    }
                    else {

                        alert("BillNo Does Not Exists");
                        return;

                    }

                    $("#Text13_pm").val(obj.BillData.CrCard_Amount);
                    $("#ddlBank_pm").val(obj.BillData.CreditBank);
                    if (obj.BillData.BillMode == "Credit") {

                        $("#ddlChosseCredit_pm").val(obj.BillData.Customer_ID);
                        $("#lblCreditCustomerName_pm").html(obj.BillData.Customer_Name);
                        $("#creditCustomer_pm").css("display", "block");
                        $("#dvOuter_ddlCreditCustomers_pm").show();
                        $("#ddlChosseCredit_pm").show();
                        $("#ddlcustName_pm").show();
                        $("#txtCashReceived_pm").val(obj.BillData.Credit_Amount);
                        Mode = "Credit";
                        CheckMode(Mode)
                    }
                    else if (obj.BillData.BillMode == "OnlinePayment") {
                        Mode = "OnlinePayment";
                        CheckMode(Mode)
                        $("#txtCashReceived_pm").val(obj.BillData.OnlinePayment);
                        if (obj.Mode == 'OnlinePayment') {
                            $("#rdoOnline_pm").prop('checked', true);
                            $("#txtCODAmount_pm").prop('checked', false);
                            $("#txtpaymentAmount_pm").val(obj.Amount);
                            $("#txtpaymentAmount_pm").attr('readonly', 'readonly');
                            $("#txtCODAmount_pm").attr('readonly', 'readonly');
                            $("#ddlcustName_pm").hide();
                            if (obj.Amount != 0)
                            {
                                $("#txtBalanceReturn_pm").val(0);
                            }
                        }
                        else if (obj.Mode == 'COD') {
                            $("#rdoCOD_pm").prop('checked', true);
                            $("#rdoOnline_pm").prop('checked', false);
                            $("#txtCODAmount_pm").val(obj.Amount);
                            $("#txtCODAmount_pm").attr('readonly', 'readonly');
                            $("#txtpaymentAmount_pm").attr('readonly', 'readonly');
                            if (obj.Amount != 0) {
                                $("#txtBalanceReturn_pm").val(0);
                            }
                            $("#ddlcustName_pm").hide();
                        }
                    }
                    else if (obj.BillData.BillMode == "Cash") {
                        Mode = "Cash";
                        CheckMode(Mode)
                        $("#ddlcustName_pm").hide();
                        $("#txtCashReceived_pm").val(obj.BillData.Cash_Amount);
                    }
                    else if (obj.BillData.BillMode == "CreditCard") {
                        Mode = "CreditCard";
                        CheckMode(Mode)
                        $("#txtCashReceived_pm").val(obj.BillData.Cash_Amount);
                    }


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {

                        if ($("#ddlbillttype_pm").val() == "Cash" || $("#ddlbillttype_pm").val() == "OnlinePayment") {
                            $("#Text13_pm").attr("disabled", "disabled");
                            $("#ddlBank_pm").attr("disabled", "disabled");
                            $("#txtCashReceived_pm").removeAttr("disabled");
                            $("#dvOuter_ddlCreditCustomers_pm").hide();
                            $("#ddlChosseCredit_pm").hide();
                            $("#ddlChosseCredit_pm").val(0);
                        }
                        else if ($("#ddlbillttype_pm").val() == "CreditCard") {

                            $("#Text13_pm").removeAttr("disabled");
                            $("#ddlBank_pm").removeAttr("disabled");
                            $("#txtCashReceived_pm").attr("disabled", "disabled");
                            $("#dvOuter_ddlCreditCustomers_pm").hide();
                            $("#ddlChosseCredit_pm").hide();
                            $("#ddlChosseCredit_pm").val(0);
                            $("#ddlcustName_pm").hide();
                        }
                        else if ($("#ddlbillttype_pm").val() == "Credit") {
                            $("#Text13_pm").attr("disabled", "disabled");
                            $("#ddlBank_pm").attr("disabled", "disabled");
                            $("#txtCashReceived_pm").removeAttr("disabled");
                            $("#dvOuter_ddlCreditCustomers_pm").show();
                            $("#ddlChosseCredit_pm").show();
                            $("#ddlcustName_pm").show();
                        }


                        DEVBalanceCalculation_pm();

                        if ($("#txtCODAmount_pm").val() != "0" || $("#txtpaymentAmount_pm").val() != "0")
                        {
                            $("#txtBalanceReturn_pm").val(0);
                        }
                        $.uiUnlock();

                    }

                });

            };

            function CheckModeForedit(Mode) {
                if (Mode == "Credit") {

                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#dvOuter_ddlCreditCustomers").show();
                    $("#lblCashHeading").text("Receipt Amt:");
                    $("#ddlbillttype option[value='Credit']").prop("selected", true);
                    $("#txtCashReceived").val("0").prop("readonly", false);
                    $("#txtCashReceived").removeAttr("disabled");
                    //$("#Text13").val("0").prop("readonly", true);
                    //$("#Text14").val("").prop("readonly", true);
                    $("#Text15").val("0").prop("readonly", true);
                    $("#Text16").val("").prop("readonly", true);
                    //$("#ddlType").prop("disabled", true);
                    //$("#ddlOtherPayment").hide();
                    //$("#rdoCOD").hide();
                

                   //$("#ddlBank").pr  //$("#txtpaymentAmount").hide();op("disabled", true);

                    $("#No").hide();
                    $("#Amt").hide();
                    $("#Coupans").hide();
                    //$("#OP").hide();
                    $("#coupan").hide();
                    $("#holder  input").hide();
                    $("#holder2  input").hide();
                    $("#No").hide();
                    $("#Amt").hide();

                    //$("#ddlBank").prop("disabled", true);
                //  $("#ddlChosseCredit").show() ;$("#btnAddCreditCustomer").css("display", "block");
                    $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                    $("#Text13").hide();
                    $("#ddlType").hide();
                    $("#ddlBank").hide();
                    $("#crcredit").hide();
                    $("#Type").hide();
                    $("#Bank").hide();
                    $("#lblCashHeading").hide();
                    $("#txtCashReceived").hide();
                    $("#rdoCOD").hide();
                    $("#rdoOnline").hide();
                    $("#lblCashHeading").show();
                    $("#txtCashReceived").show();

                }
                else if (Mode == "CreditCard") {
                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers ").val("");
                    $("#Coupans").hide();
                    //$("#OP").hide();
                    $("#coupan").hide();
                    $("#No").hide();
                    $("#Amt").hide();
                    $("#COD").hide();
                    $("#Online").hide();
                    $("#rdoOnline").hide();
                    $("#lblCashHeading").text("Cash Rec:");
                    $("#creditCustomer").css("display", "none");
                    //$("#ddlType").prop("disabled", true);
                    //$("#ddlOtherPayment").hide();
                   //$("#ddlBank").pr  //$("#txtpaymentAmount").hide();op("disabled", true);
                    $("#rdoCOD").hide();
                    $("#Td1").hide();
                    $("#txtCODAmount").hide();
                    $("#dvOuter_ddlCreditCustomers ").hide();
                  //  $("#ddlChosseCredit").hide();
                  //$("#btnAddCreditCustomer").css("display", "none");
                    $("#holder  input").hide(); $("#holder2  input").hide();
                    $("#No").hide();
                    $("#Amt").hide();

                    //                   //$("#ddlOtherPayment").hide();

                    $("#txtCashReceived ").val("0").prop("readonly", true);
                    $("#Text13").removeAttr("disabled");
                    $("#Text13").removeAttr("readonly");
                    //$("#Text13").val("0").prop("readonly", false);
                    $("#Text14").val("").prop("readonly", false);
                    $("#Text15").val("0").prop("readonly", false);
                    $("#Text16").val("").prop("readonly", false);
                    $("#ddlType").prop("disabled", false);
                    $("#ddlBank").prop("disabled", false);
                    $("#Text13").val($("#txtFinalBillAmount ").val());
                    $("#txtBalanceReturn ").val(" ");
                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#Text13").show();
                    $("#ddlType").show();
                    $("#ddlBank").show();
                    $("#crcredit").show();
                    $("#Type").show();
                    $("#Bank").show();
                    $("#lblCashHeading").hide();
                    $("#txtCashReceived").hide();
                    $("#rdoCOD").hide();
                    $("#rdoOnline").hide();
                    $("#lblCashHeading").hide();
                    $("#txtCashReceived").hide();
                }

                else if (Mode == "Cash") {


                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers").val("");
                    $("#Coupans").hide();
                    //$("#OP").hide();
                    $("#lblCashHeading").text("Cash Rec:");
                    $("#creditCustomer").css("display", "none");
                    $("#coupan").hide();
                    $("#dvOuter_ddlCreditCustomers").hide();
                  //  $("#ddlChosseCredit").hide();
                  //$("#btnAddCreditCustomer").css("display", "none");
                    //$("#ddlType").prop("disabled", true);
                    //$("#ddlOtherPayment").hide();
                    //$("#COD").hide();
                    //$("#No").hide();
                    //$("#Amt").hide();
                    //$("#Online").hide();
                   //$("#ddlBank").pr  //$("#txtpaymentAmount").hide();op("disabled", true);
                    $("#rdoCOD").hide();
                    $("#Td1").hide();
                    $("#txtCODAmount").hide();
                    //                   //$("#ddlOtherPayment").hide();
                    $("#holder input").hide();
                    $("#holder2 input").hide();
                    $("#No").hide();
                    $("#Amt").hide();

                    $("#rdoOnline").hide();
                    $("#txtCashReceived").val("0").prop("readonly", true);
                   // $("#txtCashReceived").removeAttr("disabled");
                    //$("#Text13").val("0").prop("readonly", true);
                    //$("#Text14").val("").prop("readonly", true);
                    $("#Text15").val("0").prop("readonly", true);
                    $("#Text16").val("").prop("readonly", true);
                    //$("#ddlType").prop("disabled", true);
                    //$("#ddlBank").prop("disabled", true); 
                    $("#txtCashReceived").val($("#txtFinalBillAmount").val());
                    
                    $("#txtBalanceReturn").val(" ");
                    $("#codChk").hide();
                    $("#OnlineChk").hide();
                    $("#Text13").hide();
                    $("#ddlType").hide();
                    $("#ddlBank").hide();
                    $("#crcredit").hide();
                    $("#Type").hide();
                    $("#Bank").hide();
                    $("#txtCashReceived").show();
                    $("#lblCashHeading").show();
                    $("#rdoCOD").hide();
                    $("#rdoOnline").hide();

                }
                else if (Mode == "OnlinePayment") {

                    var customerId = 0;
                    CrdCustSelId = 0;
                    CrdCustSelName = "";
                    $("#hdnCreditCustomerId").val(0);
                    $("#lblCreditCustomerName").text("");
                    CrdCustSelName = "";
                    $("#ddlCreditCustomers").html("");
                    $("#txtddlCreditCustomers").val("");

                    $("#lblCashHeading").text("Cash Rec:");
                    $("#creditCustomer").css("display", "none");
                    $("#coupan").hide();
                    $("#dvOuter_ddlCreditCustomers").hide();
                    $("#ddlChosseCredit").hide();
                  $("#btnAddCreditCustomer").css("display", "none");
                    $("#COD").show();
                    $("#Online").show();
                    $("#No").show();
                    $("#Amt").show();
                    $("#ddlOtherPayment").show();
                    $("#txtpaymentAmount").show();
                    //                            $("#rdoOnline").show();
                    //                               $("#rdoCOD").show();
                    if ($("#txtCODAmount").val() != 0) {
                        onlineoption = "COD"


                    }
                    //$("#Coupans").val("0").prop("readonly", true);
                    $("#Td1").show();
                    $("#txtCODAmount").show();
                    $("#holder2").hide();
                    $("#holder").hide();
                    $("#No").hide();
                    $("#Amt").hide();


                    $("#Coupans").show();
                    $("#coupan").show();
                    $("#OP").show();
                    $("#txtCashReceived").val("0").prop("readonly", true);
                    //$("#Text13").val("0").prop("readonly", true);
                    //$("#Text14").val("").prop("readonly", true);
                    $("#Text15").val("0").prop("readonly", false);
                    $("#Text16").val("").prop("readonly", false);
                    //$("#ddlType").prop("disabled", true);
                    //$("#ddlBank").prop("disabled", true);
                    $("#txtBalanceReturn").val($("#txtFinalBillAmount").val());
                    $("#codChk").show();
                    $("#OnlineChk").show();
                    //$("#codChk").hide();
                 
                    //$("#OnlineChk").hide();
                    $("#Text13").hide();
                    $("#ddlType").hide();
                    $("#ddlBank").hide();
                    $("#crcredit").hide();
                    $("#Type").hide();
                    $("#Bank").hide();
                    $("#lblCashHeading").hide();
                    $("#txtCashReceived").hide();
                    $("#rdoCOD").hide();
                    $("#rdoOnline").hide();
                }
            }
        function CheckMode(Mode) {
            if (Mode == "Credit") {

                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#dvOuter_ddlCreditCustomers_pm").show();
                $("#lblCashHeading_pm").text("Receipt Amt:");
                $("#ddlbillttype_pm option[value='Credit']").prop("selected", true);
                $("#txtCashReceived_pm").val("0").prop("readonly", false);
                $("#txtCashReceived_pm").removeAttr("disabled");
                $("#Text13_pm").val("0").prop("readonly", true);
                $("#Text14_pm").val("").prop("readonly", true);
                $("#Text15_pm").val("0").prop("readonly", true);
                $("#Text16_pm").val("").prop("readonly", true);
                $("#ddlType_pm").prop("disabled", true);
                $("#ddlOtherPayment_pm").hide();
                $("#rdoCOD_pm").hide();
                $("#rdoOnline_pm").hide();
                $("#Td1_pm").hide();
                $("#txtCODAmount_pm").hide();
                $("#COD_pm").hide();
                $("#Online_pm").hide();

                $("#txtpaymentAmount_pm").hide();

                $("#No_pm").hide();
                $("#Amt_pm").hide();
                $("#Coupans_pm").hide();
                $("#OP_pm").hide();
                $("#coupan_pm").hide();
                $("#holder_pm  input").hide();
                $("#holder2_pm  input").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();

                $("#ddlBank_pm").prop("disabled", true);
                $("#ddlChosseCredit_pm").show();
                $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
                $("#Text13_pm").hide();
                $("#ddlType_pm").hide();
                $("#ddlBank_pm").hide();
                $("#crcredit_pm").hide();
                $("#Type_pm").hide();
                $("#Bank_pm").hide();
                $("#lblCashHeading_pm").hide();
                $("#txtCashReceived_pm").hide();
                $("#rdoCOD_pm").hide();
                $("#rdoOnline_pm").hide();
                $("#lblCashHeading_pm").show();
                $("#txtCashReceived_pm").show();

            }
            else if (Mode == "CreditCard") {
                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId_pm").val(0);
                $("#lblCreditCustomerName_pm").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers_pm").html("");
                $("#txtddlCreditCustomers_pm ").val("");
                $("#Coupans_pm").hide();
                $("#OP_pm").hide();
                $("#coupan_pm").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();
                $("#COD_pm").hide();
                $("#Online_pm").hide();
                $("#rdoOnline_pm").hide();
                $("#lblCashHeading_pm").text("Cash Rec:");
                $("#creditCustomer_pm").css("display", "none");
                $("#ddlType_pm").prop("disabled", true);
                $("#ddlOtherPayment_pm").hide();
                $("#txtpaymentAmount_pm").hide();
                $("#rdoCOD_pm").hide();
                $("#Td1_pm").hide();
                $("#txtCODAmount_pm").hide();
                $("#dvOuter_ddlCreditCustomers_pm ").hide();
                $("#ddlChosseCredit_pm").hide();
                $("#holder_pm  input").hide(); $("#holder2_pm  input").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();

                //                   //$("#ddlOtherPayment").hide();

                $("#txtCashReceived_pm ").val("0").prop("readonly", true);
                $("#Text13_pm").removeAttr("disabled");
                $("#Text13_pm").removeAttr("readonly");
                //$("#Text13_pm").val("0").prop("readonly", false);
                $("#Text14_pm").val("").prop("readonly", false);
                $("#Text15_pm").val("0").prop("readonly", false);
                $("#Text16_pm").val("").prop("readonly", false);
                $("#ddlType_pm").prop("disabled", false);
                $("#ddlBank_pm").prop("disabled", false);
                $("#Text13_pm").val($("#txtFinalBillAmount_pm ").val());
                $("#txtBalanceReturn_pm ").val(" ");
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#Text13_pm").show();
                $("#ddlType_pm").show();
                $("#ddlBank_pm").show();
                $("#crcredit_pm").show();
                $("#Type_pm").show();
                $("#Bank_pm").show();
                $("#lblCashHeading_pm").hide();
                $("#txtCashReceived_pm").hide();
                $("#rdoCOD_pm").hide();
                $("#rdoOnline_pm").hide();
                $("#lblCashHeading_pm").hide();
                $("#txtCashReceived_pm").hide();
            }

            else if (Mode == "Cash") {


                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId_pm").val(0);
                $("#lblCreditCustomerName_pm").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers_pm").html("");
                $("#txtddlCreditCustomers_pm").val("");
                $("#Coupans_pm").hide();
                $("#OP_pm").hide();
                $("#lblCashHeading_pm").text("Cash Rec:");
                $("#creditCustomer_pm").css("display", "none");
                $("#coupan_pm").hide();
                $("#dvOuter_ddlCreditCustomers_pm").hide();
                $("#ddlChosseCredit_pm").hide();
                $("#ddlType_pm").prop("disabled", true);
                $("#ddlOtherPayment_pm").hide();
                $("#COD_pm").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();
                $("#Online_pm").hide();
                $("#txtpaymentAmount_pm").hide();
                $("#rdoCOD_pm").hide();
                $("#Td1_pm").hide();
                $("#txtCODAmount_pm").hide();
                //                   //$("#ddlOtherPayment").hide();
                $("#holder_pm input").hide();
                $("#holder2_pm input").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();

                $("#rdoOnline_pm").hide();
                //$("#txtCashReceived_pm").val("0").prop("disabled", false);
                $("#txtCashReceived_pm").removeAttr("disabled");
                $("#Text13_pm").val("0").prop("readonly", true);
                $("#Text14_pm").val("").prop("readonly", true);
                $("#Text15_pm").val("0").prop("readonly", true);
                $("#Text16_pm").val("").prop("readonly", true);
                $("#ddlType_pm").prop("disabled", true);
                $("#ddlBank_pm").prop("disabled", true);
                $("#txtCashReceived_pm").val($("#txtFinalBillAmount_pm").val());
                $("#txtBalanceReturn_pm").val(" ");
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#Text13_pm").hide();
                $("#ddlType_pm").hide();
                $("#ddlBank_pm").hide();
                $("#crcredit_pm").hide();
                $("#Type_pm").hide();
                $("#Bank_pm").hide();
                $("#txtCashReceived_pm").show();
                $("#lblCashHeading_pm").show();
                $("#rdoCOD_pm").hide();
                $("#rdoOnline_pm").hide();

            }
            else if (Mode == "OnlinePayment") {

                var customerId = 0;
                CrdCustSelId = 0;
                CrdCustSelName = "";
                $("#hdnCreditCustomerId_pm").val(0);
                $("#lblCreditCustomerName_pm").text("");
                CrdCustSelName = "";
                $("#ddlCreditCustomers_pm").html("");
                $("#txtddlCreditCustomers_pm").val("");

                $("#lblCashHeading").text("Cash Rec:");
                $("#creditCustomer").css("display", "none");
                $("#coupan_pm").hide();
                $("#dvOuter_ddlCreditCustomers_pm").hide();
                $("#ddlChosseCredit_pm").hide();
                $("#COD_pm").show();
                $("#Online_pm").show();
                $("#No_pm").show();
                $("#Amt_pm").show();
                $("#ddlOtherPayment_pm").show();
                $("#txtpaymentAmount_pm").show();
                //                            $("#rdoOnline").show();
                //                               $("#rdoCOD").show();
                if ($("#txtCODAmount_pm").val() != 0) {
                    onlineoption = "COD"


                }
                //$("#Coupans_pm").val("0").prop("readonly", true);
                $("#Td1_pm").show();
                $("#txtCODAmount_pm").show();
                $("#holder2_pm").hide();
                $("#holder_pm").hide();
                $("#No_pm").hide();
                $("#Amt_pm").hide();


                $("#Coupans_pm").show();
                $("#coupan_pm").show();
                $("#OP_pm").show();
                $("#txtCashReceived_pm").val("0").prop("readonly", true);
                $("#Text13_pm").val("0").prop("readonly", true);
                $("#Text14_pm").val("").prop("readonly", true);
                $("#Text15_pm").val("0").prop("readonly", false);
                $("#Text16_pm").val("").prop("readonly", false);
                $("#ddlType_pm").prop("disabled", true);
                $("#ddlBank_pm").prop("disabled", true);
                $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
                $("#codChk").show();
                $("#OnlineChk").show();
                $("#codChk").hide();
                $("#OnlineChk").hide();
                $("#Text13_pm").hide();
                $("#ddlType_pm").hide();
                $("#ddlBank_pm").hide();
                $("#crcredit_pm").hide();
                $("#Type_pm").hide();
                $("#Bank_pm").hide();
                $("#lblCashHeading_pm").hide();
                $("#txtCashReceived_pm").hide();
                $("#rdoCOD_pm").show();
                $("#rdoOnline_pm").show();
            }
        }
        function Reset() {
            $("#txtCashReceived_pm").val("0.00");
            $("#Text13_pm").val("0.00");
            $("#ddlBank_pm").val("");
            $("#dvOuter_ddlCreditCustomers_pm").hide();

            $("#ddlChosseCredit_pm").hide();
            $("#ddlChosseCredit_pm").val(0);
        }


        function ChangePaymode() {
            var Mode = "";
            var FinalAmount = 0;
            //var BNF = $("#dvShow_pm").attr('dir'); 
            var BNF = $("#txtBillNumber_pm").val();
            if (BNF == "") {
                alert("Please Enter Bill Number");
                //$("#txtBillNumber").focus();
                return;
            }
            var BillType = $("#ddlbillttype_pm").val();
            var CashAmount = $("#txtCashReceived_pm").val();
            var CreditCustId = $("#ddlChosseCredit_pm").val();


            if (BillType == "Cash" || BillType == "CreditCard" || BillType == "OnlinePayment") {

                if (Number($("#txtBalanceReturn_pm").val()) < 0) {

                    $.uiUnlock();
                    alert("Total amount is not equal to Bill Amount....Please first tally amount.");
                    $("#btnBillWindowOk_pm").removeAttr('disabled');
                    return;
                }
                else {



                    CashAmount = CashAmount - Number($("#txtBalanceReturn_pm").val());
                }

            }


            if (BillType == "CreditCard") {
                if ($("#ddlBank_pm").val() == "") {
                    alert("Choose Bank");
                    $("#ddlBank_pm").focus();
                    return;
                }
            }

            if (BillType == "Credit") {

                if (CreditCustId == 0) {

                    alert("Please select Credit Customer");
                    $("#txtddlCreditCustomers_pm").focus();

                    return;
                }


            }


            if (Number(CashAmount) < 0) {
                $.uiUnlock();
                alert("Invalid Cash Amount. Return amount cannot be greater than Cash Amount.");
                $("#btnBillWindowOk_pm").removeAttr('disabled');
                return;
            }





            var CrCardAmount = $("#Text13_pm").val();
            var Bank = $("#ddlBank_pm").val();
            var BankName = $("#ddlBank_pm option:selected").text();
            var OnlinePayment = 0;
            var CreditAmount = $("#txtCashReceived_pm").val();

            var CreditCustName = $("#ddlChosseCredit_pm option:selected").text();
            var amt_cod = parseFloat($("#txtCODAmount_pm").val());
            var amt_online = parseFloat($("#txtpaymentAmount_pm").val());
            var total_amt = parseFloat(amt_cod) + parseFloat(amt_online)
            if (amt_cod <= 0 || amt_cod == "") {
                OnlinePayment = parseFloat(total_amt);
            }

            else if (amt_cod > 0 && amt_online <= 0 || amt_cod > 0 && amt_online == "") {

                CashAmount = amt_cod;

            }
            var OtherPaymentID = $("#<%=ddlOtherPayment_pm.ClientID %>").val();
                if ($("#rdoCOD_pm").prop('checked') == true) {
                    Mode = "COD"
                    CashAmount = parseFloat($("#txtCODAmount_pm").val()).toFixed(2);
                }
                else if ($("#rdoOnline_pm").prop('checked') == true) {
                    Mode = "OnlinePayment";
                    CashAmount = parseFloat($("#txtpaymentAmount_pm").val()).toFixed(2);
                }
                if ($("#txtFinalBillAmount_pm").val() != "")
                {
                    FinalAmount = parseFloat($("#txtFinalBillAmount_pm").val()).toFixed(2);
                }
                $.ajax({
                    type: "POST",
                    data: '{ "BillNowPrefix": "' + BNF + '","BillType": "' + BillType + '","CashAmount": "' + CashAmount + '","CrCardAmount": "' + CrCardAmount + '","Bank": "' + Bank + '","BankName": "' + BankName + '","CreditAmount": "' + CreditAmount + '","CreditCustId": "' + CreditCustId + '","CreditCustName": "' + CreditCustName + '","OnlinePayment":"' + OnlinePayment + '","OtherPaymentID":"' + OtherPaymentID + '","Mode":"' + Mode + '","FinalAmount":"' + FinalAmount + '"}',
                    url: "changePaymode.aspx/ChangePaymode",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);
                        $("#<%=ddlOtherPayment_pm.ClientID %>").val("0");
                        $("#rdoCOD_pm").prop('checked', false);
                        $("#rdoOnline_pm").prop('checked', false);
                        $("#txtCODAmount_pm").val("");
                        $("#txtpaymentAmount_pm").val("");

                        //InsertOnlineOtherPayment_pm(BNF);
                        alert("PayMode Changed Successfully");

                        return;

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $("#btnBillWindowOk_pm").attr("disabled", "disabled");
                        Reset();
                        $("#ddlbillttype_pm").val("Cash");
                        $("#txtFinalBillAmount_pm").val("0.00");
                        $("#txtBillNumber_pm").val("");

                        $.uiUnlock();

                    }

                });



            }





            $(document).ready(
            function () {

                $("#btnBillWindowOk_pm").attr("disabled", "disabled");

                $("#ddlChosseCredit_pm").change(
                         function () {


                             if ($("#ddlChosseCredit_pm").val() != "0") {


                                 CrdCustSelId = $("#ddlChosseCredit_pm").val();
                                 $("#hdnCreditCustomerId_pm").val(CrdCustSelId);
                                 CrdCustSelName = $("#ddlChosseCredit_pm option:selected").text();
                                 $("#lblCreditCustomerName_pm").text($("#ddlChosseCredit_pm option:selected").text())

                                 $("#creditCustomer_pm").css("display", "block");

                             }




                         });



                $("#ddlCreditCustomers_pm").supersearch({
                    Type: "CashCustomer",
                    Caption: "Please enter Customer Name/Code ",
                    AccountType: "D",
                    Width: 100,
                    DefaultValue: 0,
                    Godown: 0
                });


                $("#txtCashReceived_pm").keyup(
             function () {
                 var regex = /^[0-9\.]*$/;


                 var value = jQuery.trim($(this).val());
                 var count = value.split('.');


                 if (value.length >= 1) {
                     if (!regex.test(value) || value <= 0 || count.length > 2) {

                         $(this).val(0);


                     }
                 }


                 DEVBalanceCalculation_pm();

             }
             );

                $("#Text13_pm").keyup(
                     function () {
                         var regex = /^[0-9\.]*$/;


                         var value = jQuery.trim($(this).val());
                         var count = value.split('.');


                         if (value.length >= 1) {
                             if (!regex.test(value) || value <= 0 || count.length > 2) {

                                 $(this).val(0);


                             }
                         }


                         DEVBalanceCalculation_pm();

                     }
                     );
                $("#Text15_pm").keyup(
                     function () {
                         var regex = /^[0-9\.]*$/;


                         var value = jQuery.trim($(this).val());
                         var count = value.split('.');


                         if (value.length >= 1) {
                             if (!regex.test(value) || value <= 0 || count.length > 2) {

                                 $(this).val(0);


                             }
                         }

                         DEVBalanceCalculation_pm();

                     }
                     );



                $("#ddlbillttype_pm").change(
           function () {



               if ($(this).val() == "Credit") {
                   $("#ddlcustName_pm").show();
                   $("#codChk").hide();
                   $("#OnlineChk").hide();
                   $("#dvOuter_ddlCreditCustomers_pm").show();
                   $("#lblCashHeading_pm").text("Receipt Amt:");
                   $("#ddlbillttype_pm option[value='Credit']").prop("selected", true);
                   $("#txtCashReceived_pm").val("0").prop("readonly", false);
                   $("#txtCashReceived_pm").removeAttr("disabled");
                   $("#Text13_pm").val("0").prop("readonly", true);
                   $("#Text14_pm").val("").prop("readonly", true);
                   $("#Text15_pm").val("0").prop("readonly", true);
                   $("#Text16_pm").val("").prop("readonly", true);
                   $("#ddlType_pm").prop("disabled", true);
                   $("#ddlOtherPayment_pm").hide();
                   $("#rdoCOD_pm").hide();
                   $("#rdoOnline_pm").hide();
                   $("#Td1_pm").hide();
                   $("#txtCODAmount_pm").hide();
                   $("#COD_pm").hide();
                   $("#Online_pm").hide();

                   $("#txtpaymentAmount_pm").hide();

                   $("#No_pm").hide();
                   $("#Amt_pm").hide();
                   $("#Coupans_pm").hide();
                   $("#OP_pm").hide();
                   $("#coupan_pm").hide();
                   $("#holder_pm  input").hide();
                   $("#holder2_pm  input").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();

                   $("#ddlBank_pm").prop("disabled", true);
                   $("#ddlChosseCredit_pm").show();
                   $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
                   $("#Text13_pm").hide();
                   $("#ddlType_pm").hide();
                   $("#ddlBank_pm").hide();
                   $("#crcredit_pm").hide();
                   $("#Type_pm").hide();
                   $("#Bank_pm").hide();
                   $("#lblCashHeading_pm").hide();
                   $("#txtCashReceived_pm").hide();
                   $("#rdoCOD_pm").hide();
                   $("#rdoOnline_pm").hide();
                   $("#lblCashHeading_pm").show();
                   $("#txtCashReceived_pm").show();
                   $("#lblCashHeading_pm").hide();
                   $("#txtCashReceived_pm").hide();
                   var FinalAmt = $("#txtFinalBillAmount_pm").val(); 
                   $("#txtCashReceived_pm").val(parseFloat(FinalAmt).toFixed(2));
                   $("#txtBalanceReturn_pm").val(0);
               }
               else if ($(this).val() == "CreditCard") {
                   var customerId = 0;
                   CrdCustSelId = 0;
                   CrdCustSelName = "";
                   $("#hdnCreditCustomerId_pm").val(0);
                   $("#lblCreditCustomerName_pm").text("");
                   CrdCustSelName = "";
                   $("#ddlCreditCustomers_pm").html("");
                   $("#txtddlCreditCustomers_pm ").val("");
                   $("#Coupans_pm").hide();
                   $("#OP_pm").hide();
                   $("#coupan_pm").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();
                   $("#COD_pm").hide();
                   $("#Online_pm").hide();
                   $("#rdoOnline_pm").hide();
                   $("#lblCashHeading_pm").text("Cash Rec:");
                   $("#creditCustomer_pm").css("display", "none");
                   $("#ddlType_pm").prop("disabled", true);
                   $("#ddlOtherPayment_pm").hide();
                   $("#txtpaymentAmount_pm").hide();
                   $("#rdoCOD_pm").hide();
                   $("#Td1_pm").hide();
                   $("#txtCODAmount_pm").hide();
                   $("#dvOuter_ddlCreditCustomers_pm ").hide();
                   $("#ddlChosseCredit_pm").hide();
                   $("#holder_pm  input").hide(); $("#holder2_pm  input").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();

                   //                   //$("#ddlOtherPayment").hide();

                   $("#txtCashReceived_pm ").val("0").prop("readonly", true);
                   $("#Text13_pm").removeAttr("disabled");
                   $("#Text13_pm").removeAttr("readonly");
                   //$("#Text13_pm").val("0").prop("readonly", false);
                   $("#Text14_pm").val("").prop("readonly", false);
                   $("#Text15_pm").val("0").prop("readonly", false);
                   $("#Text16_pm").val("").prop("readonly", false);
                   $("#ddlType_pm").prop("disabled", false);
                   $("#ddlBank_pm").prop("disabled", false);
                   $("#Text13_pm").val($("#txtFinalBillAmount_pm ").val());
                   $("#txtBalanceReturn_pm ").val(" ");
                   $("#codChk").hide();
                   $("#OnlineChk").hide();
                   $("#Text13_pm").show();
                   $("#ddlType_pm").show();
                   $("#ddlBank_pm").show();
                   $("#crcredit_pm").show();
                   $("#Type_pm").show();
                   $("#Bank_pm").show();
                   $("#lblCashHeading_pm").hide();
                   $("#txtCashReceived_pm").hide();
                   $("#rdoCOD_pm").hide();
                   $("#rdoOnline_pm").hide();
                   $("#lblCashHeading_pm").hide();
                   $("#txtCashReceived_pm").hide();
                   $("#ddlcustName_pm").hide();
               }

               else if ($(this).val() == "Cash") {

                   $("#ddlcustName_pm").hide();
                   var customerId = 0;
                   CrdCustSelId = 0;
                   CrdCustSelName = "";
                   $("#hdnCreditCustomerId_pm").val(0);
                   $("#lblCreditCustomerName_pm").text("");
                   CrdCustSelName = "";
                   $("#ddlCreditCustomers_pm").html("");
                   $("#txtddlCreditCustomers_pm").val("");
                   $("#Coupans_pm").hide();
                   $("#OP_pm").hide();
                   $("#lblCashHeading_pm").text("Cash Rec:");
                   $("#creditCustomer_pm").css("display", "none");
                   $("#coupan_pm").hide();
                   $("#dvOuter_ddlCreditCustomers_pm").hide();
                   $("#ddlChosseCredit_pm").hide();
                   $("#ddlType_pm").prop("disabled", true);
                   $("#ddlOtherPayment_pm").hide();
                   $("#COD_pm").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();
                   $("#Online_pm").hide();
                   $("#txtpaymentAmount_pm").hide();
                   $("#rdoCOD_pm").hide();
                   $("#Td1_pm").hide();
                   $("#txtCODAmount_pm").hide();
                   //                   //$("#ddlOtherPayment").hide();
                   $("#holder_pm input").hide();
                   $("#holder2_pm input").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();

                   $("#rdoOnline_pm").hide();
                   //$("#txtCashReceived_pm").val("0").prop("disabled", false);
                   $("#txtCashReceived_pm").removeAttr("disabled");
                   $("#Text13_pm").val("0").prop("readonly", true);
                   $("#Text14_pm").val("").prop("readonly", true);
                   $("#Text15_pm").val("0").prop("readonly", true);
                   $("#Text16_pm").val("").prop("readonly", true);
                   $("#ddlType_pm").prop("disabled", true);
                   $("#ddlBank_pm").prop("disabled", true);
                   $("#txtCashReceived_pm").val($("#txtFinalBillAmount_pm").val());
                   $("#txtBalanceReturn_pm").val(" ");
                   $("#codChk").hide();
                   $("#OnlineChk").hide();
                   $("#Text13_pm").hide();
                   $("#ddlType_pm").hide();
                   $("#ddlBank_pm").hide();
                   $("#crcredit_pm").hide();
                   $("#Type_pm").hide();
                   $("#Bank_pm").hide();
                   $("#txtCashReceived_pm").show();
                   $("#lblCashHeading_pm").show();
                   $("#rdoCOD_pm").hide();
                   $("#rdoOnline_pm").hide();

               }
               else if ($(this).val() == "OnlinePayment") {

                   var customerId = 0;
                   CrdCustSelId = 0;
                   CrdCustSelName = "";
                   $("#hdnCreditCustomerId_pm").val(0);
                   $("#lblCreditCustomerName_pm").text("");
                   CrdCustSelName = "";
                   $("#ddlCreditCustomers_pm").html("");
                   $("#txtddlCreditCustomers_pm").val("");
                   $("#ddlcustName_pm").hide();
                   $("#lblCashHeading").text("Cash Rec:");
                   $("#creditCustomer").css("display", "none");
                   $("#coupan_pm").hide();
                   $("#dvOuter_ddlCreditCustomers_pm").hide();
                   $("#ddlChosseCredit_pm").hide();
                   $("#COD_pm").show();
                   $("#Online_pm").show();
                   $("#No_pm").show();
                   $("#Amt_pm").show();
                   $("#ddlOtherPayment_pm").show();
                   $("#txtpaymentAmount_pm").show();
                   //                            $("#rdoOnline").show();
                   //                               $("#rdoCOD").show();
                   if ($("#txtCODAmount_pm").val() != 0) {
                       onlineoption = "COD"


                   }
                   //$("#Coupans_pm").val("0").prop("readonly", true);
                   $("#Td1_pm").show();
                   $("#txtCODAmount_pm").show();
                   $("#holder2_pm").hide();
                   $("#holder_pm").hide();
                   $("#No_pm").hide();
                   $("#Amt_pm").hide();


                   $("#Coupans_pm").show();
                   $("#coupan_pm").show();
                   $("#OP_pm").show();
                   $("#txtCashReceived_pm").val("0").prop("readonly", true);
                   $("#Text13_pm").val("0").prop("readonly", true);
                   $("#Text14_pm").val("").prop("readonly", true);
                   $("#Text15_pm").val("0").prop("readonly", false);
                   $("#Text16_pm").val("").prop("readonly", false);
                   $("#ddlType_pm").prop("disabled", true);
                   $("#ddlBank_pm").prop("disabled", true);
                   $("#txtBalanceReturn_pm").val($("#txtFinalBillAmount_pm").val());
                   $("#codChk").show();
                   $("#OnlineChk").show();
                   $("#codChk").hide();
                   $("#OnlineChk").hide();
                   $("#Text13_pm").hide();
                   $("#ddlType_pm").hide();
                   $("#ddlBank_pm").hide();
                   $("#crcredit_pm").hide();
                   $("#Type_pm").hide();
                   $("#Bank_pm").hide();
                   $("#lblCashHeading_pm").hide();
                   $("#txtCashReceived_pm").hide();
                   $("#rdoCOD_pm").show();
                   $("#rdoOnline_pm").show();
               }

           }
           );




                //$("#ddlbillttype_pm").change(
                //function () {

                //  Reset();
                //  if ($(this).val() == "Cash" || $(this).val() == "OnlinePayment") {
                //    $("#Text13_pm").attr("disabled", "disabled");
                //    $("#ddlBank_pm").attr("disabled", "disabled");
                //    $("#txtCashReceived_pm").removeAttr("disabled");
                //    $("#dvOuter_ddlCreditCustomers_pm").hide();
                //    $("#ddlChosseCredit_pm").hide();
                //    $("#ddlChosseCredit_pm").val(0);
                //    $("#creditCustomer_pm").css("display", "none");



                //  }



                //  else if ($(this).val() == "CreditCard") {

                //    $("#Text13_pm").removeAttr("disabled");
                //    $("#ddlBank_pm").removeAttr("disabled");
                //    $("#txtCashReceived_pm").attr("disabled", "disabled");
                //    $("#dvOuter_ddlCreditCustomers_pm").hide();
                //    $("#ddlChosseCredit_pm").hide();
                //    $("#ddlChosseCredit_pm").val(0);
                //    $("#creditCustomer_pm").css("display", "none");

                //  }
                //  else if ($("#ddlbillttype_pm").val() == "Credit") {
                //    $("#Text13_pm").attr("disabled", "disabled");
                //    $("#ddlBank_pm").attr("disabled", "disabled");
                //    $("#txtCashReceived_pm").removeAttr("disabled");
                //    $("#dvOuter_ddlCreditCustomers_pm").show();
                //    $("#ddlChosseCredit_pm").show();
                //    $("#ddlChosseCredit_pm").val(0);
                //  }
                //  DEVBalanceCalculation_pm();

                //});

                BindBanks();
                $("#dvShow_pm").click(
            function () {
                $("#dvpaymode").show();
                GetBillByBNF();
            }
            );

                $("#dvShow").click(
         function () {
             GetBillByBNF();
         }
         );


                $("#btnBillWindowOk_pm").click(function () {

                    var CheckBillType = $("#ddlbillttype_pm").val();
                    if (CheckBillType == 'OnlinePayment') {
                        if ($("#<%=ddlOtherPayment_pm.ClientID %>").val() == "0") {
                            alert("Please Select Other Paymentmode");
                            return false;
                        }
                        else {
                            ChangePaymode();
                        }
                    }
                    else {
                        ChangePaymode();
                    }
                }
                );


                $("#dvOuter_ddlCreditCustomers_pm").hide();
                $("ddlCreditCustomers_pm").hide();
                $("#ddlChosseCredit_pm").hide();
                $("#ddlChosseCredit_pm").val(0);

            }
            );


        </script>
         <script type="text/javascript">
             $(document).ready(function () {
                 $("#rdoOnline_pm").change(function () {
                     if ($("#rdoOnline_pm").prop('checked')) {
                         var FinalAmount = $("#txtFinalBillAmount_pm").val();
                         $("#txtpaymentAmount_pm").attr('readonly', 'readonly');
                         $("#txtCODAmount_pm").attr('readonly', 'readonly');
                         $("#txtBalanceReturn_pm").val(0)
                         $("#txtpaymentAmount_pm").val(FinalAmount);
                         $("#txtCODAmount_pm").val(0);
                         $("#rdoCOD_pm").prop('checked', false);
                     }
                 })
                 $("#rdoCOD_pm").change(function () {
                     if ($("#rdoCOD_pm").prop('checked')) {
                         var FinalAmount = $("#txtFinalBillAmount_pm").val();
                         $("#txtpaymentAmount_pm").attr('readonly', 'readonly');
                         $("#txtCODAmount_pm").attr('readonly', 'readonly');
                         $("#txtpaymentAmount_pm").val(0);
                         $("#txtBalanceReturn_pm").val(0)
                         $("#txtCODAmount_pm").val(FinalAmount);
                         $("#rdoOnline_pm").prop('checked', false);
                     }
                 })
             })
    </script>

 <div id="dvAddOn"></div>
<div class="right_col billing_screen_right_col" role="main">

        <div id="dvpaymode" class="change_paymode" style="z-index: 100;position:  relative;float: left;display:none">

                   

                    <div class="clearfix"></div>

                   
                    <div class="x_panel">

               <h2>Change Paymode</h2>
                        <div class="x_content">

                             <table width="100%">
             <tbody><tr><td align="center">
             <div style="" class="bill_number">
             
         
             <table width="100%" cellspacing="5" cellpadding="5">
             <tbody><tr>
             <td>Bill Number:</td><td>
             <input type="text" id="txtBillNumber_pm" style="width:200px;height:30px; text-transform:uppercase" class="form-control input-small" />
             </td>
            <td><div id="dvShow" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Show</div></td>
             </tr>
             </tbody></table>
                 </div>
             </td></tr>
             <tr>
             <td style="border-bottom:dotted 1px silver"></td>
             </tr>
             <tr>
             <td>
         <div class="cate paymode_cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px;">
                         <tr>
                                
                                <td colspan="100%">
                                    <table class="payment_cate_table">
                                        <tr>

                               
                               <%-- <td>
                                    Bill Type:
                                </td>--%>
                                <td>
                                    <select id="ddlbillttype_pm" style="height: 119px; width: 258px; padding-left: 0px;" class="form-control" multiple="multiple">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>

                                <td>
                                    <span id="ddlcustName_pm">Customer Name</span>
<%--                                  <select id="ddlCreditCustomers_pm" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 166px;
">
                                             <option value="0"></option>
                                             </select>--%>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit_pm" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>


                         

                                </td>
                               <td>
                                    
                                </td>
                                
                            </tr>
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;" id="txtFinalBillAmount_pm" disabled="disabled" multiple="multiple"/>
                                </td>
                                <td>
                                
                                </td>
                                <td>
                                    
                                </td>
                                <td>

                                

                                </td>
                                          
                                 <td> </td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading_pm" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;" value="0"
                                        id="txtCashReceived_pm" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer_pm" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName_pm" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td id="crcredit_pm">
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px;height: 30px;" value="0"
                                        id="Text13_pm" />
                                </td>
                                            <td id="Type_pm">Type:</td>
                                                  <td>
                                    <select id="ddlType_pm" style="height: 35px; width: 150px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>
                           
                                <td>
                                   
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                
                                </td>
                                <td colspan="100%">
                                  
                                </td>
                            </tr>
                            <tr>
                                <td id="Bank_pm">
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank_pm" style="height: 32px; width: 90px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15_pm"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>

                                              <tr><td id="OP_pm" style=" display: none;">Online payment</td>

                            <td> <select id="ddlOtherPayment_pm" class="form-control" clientidmode="Static" runat="server" style=" display: none;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                           
                                             </select></td>
                            
                                        
                                        
                                             </tr>
                            <tr>


                                                 
                                                  <td  style="text-align:left;width:50px">
                                                       <input type="checkbox" id="rdoCOD_pm"  style="width:15px;height:19px" />
                         <label class="headings" id="COD_pm" for="rdoCOD" style=" display: none;">COD</label>
                                </td>
                                                     <td colspan="3">
                                    <table>
                                        <tbody><tr>
                                                <td>
                                                   
                                   <input class="form-control input-small" style="width: 70px;height:25px;color:Black;display:none" id="txtCODAmount_pm" type="text" value="0"/>
                                </td>
                                <td>
                                      <input type="checkbox" id="rdoOnline_pm"  style="width:15px;height:19px" />
                                    <label class="headings" id="Online_pm" style=" display: none;" for="rdoOnline">Online</label>
                                </td>
                                <td>
                                  <input type="text" class="form-control input-small"style="width: 70px;height:25px;color:Black;display:none" value="0"
                                        id="txtpaymentAmount_pm" />
                                </td>

                               
                                        </tr>

                                    </tbody></table>


                                </td>
                                        </tr>
                                        <tr> 
                                           <td id="coupan_pm" style=" display: none;">Enter Coupan Qty</td>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 60px;display:none" value="0"
                                        id="Coupans_pm" />
                                        </td>
                                      <td style="width: 50px"><label id="No_pm"style="display: none; margin-left: -150px;" >CoupanNo</label>
                                      <div id="holder_pm"style="display: block; margin-left: -150px;"></div></td>
                                      <td style="width: 50px"><label id="Amt_pm" style=" display: none;">CoupanAmt</label><div id="holder2_pm"></div></td> 
                                            
                  
                                      </tr>
                                    </table>

                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16_pm" />
                                </td>
                            </tr>

                            <tr class="paymode_close">
                                <td style="">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn_pm"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk_pm" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Change Paymode</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose_pm" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                 </tbody>
                        </table>
                    </div>   
             
             
             
             </td></tr>
             
             
             </tbody></table>

                        </div>
                    </div>


     

                     
                </div>
<%-- paymodenn---%>
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row bill-screen-row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="page-title bill-screen-page-title">
                        <div class="title_left">
                            <h3><label>GST Billing        </label> 

                            <asp:Label ID="lbltotl" runat="server" Text="Total Sale = " Font-Size="Medium"></asp:Label> <asp:Label ID="LblTodayTotalSale" Font-Size ="Medium" runat="server"></asp:Label> 
                             <asp:Label ID="lblcash" runat="server" Text ="Cash Sale = " Font-Size="Medium"></asp:Label>   <asp:Label ID="LblTodayCash" Font-Size="Medium" runat="server"></asp:Label>
                             <asp:Label ID="lblcrcrd" runat="server" Text ="CrCard Sale =  " Font-Size="Medium"> </asp:Label><asp:Label ID="LblTodayCrCardSale" Font-Size="Medium" runat="server"></asp:Label>
                              <asp:Label ID="lblonline" runat="server" Text ="Online Sale =  " Font-Size="Medium"></asp:Label> <asp:Label ID="LblTodayOnlineSale" Font-Size="Medium" runat="server"></asp:Label>
                                <%--<label>Cash Sale = </label><asp:Label ID="Label4" runat="server"></asp:Label>
                                <label>Cash Sale = </label><asp:Label ID="Label5" runat="server"></asp:Label>
                                <label>Cash Sale = </label><asp:Label ID="Label6" runat="server"></asp:Label>--%>


                            </h3>
                            
                        </div>
                     </div>
                            <div class="x_panel">

                                <div class="x_title" style="display:none;">
                                    <h2>Bills List</h2>
                                 
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group" margin-bottom:10px">
                                 <div class="form-group betlist_form_table">
                                
                                <table>
                                <tr><td >Date From:</td><td>
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:120px;background-color:White;"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td >Date To:</td><td><input type="text" readonly="readonly"  class="form-control input-small" style="width:120px;background-color:White;font-size:medium;"   id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>

                <table id="jQGridDemoBill">

                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                </div>


                                   
                                        <div class="form-group bill_list_btns">
                                            <div class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table>
                                                    <tr><td><div class="btn btn-primary" style="display: block;" id="btnShowScreen">
                                           <i class="fa fa-external-link"></i> <u>N</u>ew</div></td>

                                           <td><div class="btn btn-success" style="display:none;" id="btnEdit">
                                           <i class="fa fa-edit m-right-xs"></i> Ed<u>i</u>t</div></td>

                                                        <td><div class="btn btn-success" " id="btnView">
                                           <i class="fa fa-edit m-right-xs"></i> View<u>Bi</u>ll</div></td>
                                           
                                           <td><div class="btn btn-danger" style="display: block;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> <u>R</u>eprint</div></td>

                                            <td >
                                        <div id="btnDelete" style="display:none;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i><u>C</u>ancel Bill</div>
                                    </td>

                                     <td> <div id="btnGSTBill" style="display:none;" class="btn btn-success">
                                           <i class="fa fa-trash m-right-xs"></i><u>G</u>ST Bill</div></td>

                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                    
                    <script type="text/javascript">

                        jQuery.fn.putCursorAtEnd = function () {

                            return this.each(function () {

                                // Cache references
                                var $el = $(this),el = this;

                                // Only focus if input isn't already
                                if (!$el.is(":focus")) {
                                    $el.focus();
                                }

                                // If this function exists... (IE 9+)
                                if (el.setSelectionRange) {

                                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                                    var len = $el.val().length * 2;

                                    // Timeout seems to be required for Blink
                                    setTimeout(function () {
                                        el.setSelectionRange(len, len);
                                    }, 1);

                                } else {

                                    // As a fallback, replace the contents with itself
                                    // Doesn't work in Chrome, but Chrome supports setSelectionRange
                                    $el.val($el.val());

                                }

                                // Scroll to the bottom, in case we're in a tall textarea
                                // (Necessary for Firefox and Chrome)
                                this.scrollTop = 999999;

                            });

                        };

                        (function () {

                            var searchInput = $("#search");

                            searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });

                        })();


                        $(document).ready(function () {



                            $("#No").hide();
                            $("#Amt").hide();
                            //$("#ddlOtherPayment").hide();
                            $("#Td1").hide();
                            $("#txtCODAmount").hide();
                            $("#rdoCOD").hide();
                            $("#rdoOnline").hide();
                            $("#COD").hide();
                            $("#Online").hide();

                           //$("#ddlBank").pr  //$("#txtpaymentAmount").hide();op("disabled", true);
                            $("#Coupans").hide();
                            //$("#OP").hide();
                            $("#coupan").hide();
                            $("#txtCoupan0").hide();

                            GetBranchState();

                            if ($("#ddlbillttype").val() == "Cash") {

                                //var customerId = 0ddlChosseCredit
                                CrdCustSelId = 0;
                                CrdCustSelName = "";
                                $("#hdnCreditCustomerId").val(0);
                                $("#lblCreditCustomerName").text("");
                                CrdCustSelName = "";
                                $("#ddlCreditCustomers").html("");
                                $("#txtddlCreditCustomers").val("");

                                $("#lblCashHeading").text("Cash Rec:");
                                $("#creditCustomer").css("display", "none");

                                $("#dvOuter_ddlCreditCustomers").hide();

                                $("#ddlChosseCredit").hide();
                              $("#btnAddCreditCustomer").css("display", "none");
                                $("#ddlChosseCredit").val(0);

                                $("#txtCashReceived").val("0").prop("readonly", false);
                                //$("#Text13").val("0").prop("readonly", true);
                                //$("#Text14").val("").prop("readonly", true);
                                $("#Text15").val("0").prop("readonly", true);
                                $("#Text16").val("").prop("readonly", true);
                                //$("#ddlType").prop("disabled", true);
                                //$("#ddlBank").prop("disabled", true);


                            }


                            $("#txtDateFrom,#txtDateTo,#txtBillDate").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();







                            $("#btnGo").click(function () { BindGrid(); });

                            $(".txt").click(function () {
                                $.ajax({
                                    type: "POST",
                                    url: "billscreen.aspx/Keyboard",
                                    contentType: "application/json",
                                    dataType: "json",

                                    error: function (xhr, ajaxOptions, thrownError) {

                                        var obj = jQuery.parseJSON(xhr.responseText);
                                        alert(obj.Message);
                                    },
                                    complete: function (msg) {


                                    }

                                });
                            });
							

                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

							$('#txtBillDate').daterangepicker({
								singleDatePicker: true,
								calender_style: "picker_1"
							}, function (start, end, label) {
								console.log(start.toISOString(), end.toISOString(), label);
							});
                        });
                    </script>

                    <script>
                        $(document).ready(function(){
                            $("#btnGSTBill").click(function () {
                                $(".ui-dialog.ui-widget.ui-widget-content").addClass("billscreen-gst-bill");
                            });
                        });
                        $(document).ready(function () {
                        	$("#btnShowScreen").click(function () {
                        		$(".ui-dialog.ui-widget.ui-widget-content").removeClass("billscreen-gst-bill");
                        	});
                        });
                        $(document).ready(function () {
                        	$("#btnEdit").click(function () {
                        		$(".ui-dialog.ui-widget.ui-widget-content").removeClass("billscreen-gst-bill");
                        	});
                        });
                        $(document).ready(function () {
                        	$("#btnReprint").click(function () {
                        		$(".ui-dialog.ui-widget.ui-widget-content").removeClass("billscreen-gst-bill");
                        	});
                        });
                        $(document).ready(function () {
                        	$("#btnDelete").click(function () {
                        		$(".ui-dialog.ui-widget.ui-widget-content").removeClass("billscreen-gst-bill");
                        	});
                        });

                    </script>

                    

 
                </div>
                <!-- /page content -->

               

            </div>


<div id="dvbillSave" style="float: left; display:none; position:absolute;z-index:1000;left:340px;width:300px;bottom:240px;background-color: #293C52;
                                                                color: white; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                                <table style="width:100%;">
                                                                    <tr style="text-align:center">
                                                                    <td style="text-align:center">BILL SAVED SUCCESSFULLY</td>
                                                                        
                                                                    </tr>
                                                                    <tr><td style="text-align:center;font-size:20px">AMOUNTED Rs.<asp:Label ID ="lblAmmmt" runat="server" ClientIDMode ="Static"></asp:Label></td></tr>
                                                                    <tr><td>
                                                                    <div class="button1" style="background-color:White;color:Black;width:60px;box-shadow: -2px -5px 5px transparent;margin-left:41%;height:40px;"  id="btnMsgClose">
                                    OK</div>
                                                                   </td></tr>
                                                                    
                                                                  
                                                                </table>
                                                            </div>


<div class="row" id="CustomerDialog" style="display:none">
<div class="form-horizontal form-label-left input_mask">
<%--<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />--%>
</div>


</div>



        
<div class="row" id="CreditCustomerDialog" style="display:none">
<div class="form-horizontal form-label-left input_mask">
<uc3:AddCashCustomer ID="AddCashCustomer" runat="server" />
</div>


</div>

<div id="GSTDialog" style="background-color:#26b99a;display:none">

 <table style="border-collapse: separate; border-spacing: 1;font-size:17px;background-color: lightblue!important; width:100%;" class="bill-screen-gst-dialog">

                        <tr style="display:none"><td><input type="radio" id="rdblocal" name="local" checked="checked" /> <label class="control-label" for="rbLocal" style="font-weight:normal"> Local</label></td>
                        <td><input type="radio" id="rdbout" name="local" /> <label class="control-label" for="rdbout" style="font-weight:normal"> Outstation</label></td></tr>
                        <tr><td><input type="checkbox" checked="checked" data-index="27" style="font-size:25px" id="chkGst" /></td><td>Mark as GST Bill</td></tr>
                        <tr> <td colspan="100%" >
               
                <table style="width:100%;" class="cstmer-table-bill-screen"><tr>      

                                        <td style="color:black"><b>CUSTOMER </b></td>
                                            <td style="padding-left:5px;">

                                            
                                                <select id="ddlMobSearchBox1">

                                                </select>
                                                

                                                <input type="text" placeholder="Mobile Number" style="display:none; border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox1" class="txt ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox" />

                                                 <div id="Div1" style="display:none;">
                                                    <img id="img1" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>
                                            <td>
                                              <img id="Img2"  style="cursor:pointer;width:25px;"  src="images/adduser-white.png"    />
                                            </td></tr></table></td>               
               
                       <td>
                                                <table width="100%" id="Table1" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2"; margin:0 auto !important;>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="Label1" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <td style="padding-left:50px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                                </td>
                                                   
                </tr>


                        <tr><td colspan="100%;"><div id="btnsa" style="display:block;" class="btn btn-primary">
                                           <i class="fa fa-trash m-right-xs"></i>Save</div></td></tr>
</table>



</div>


<div id="getcombodetail"  style="background-color: darkorange;float:  left;margin-top: -52%;z-index: 100;position:  relative;margin-left: 2%;width: 53%;display:none;padding-bottom: 9%;">


 <table style="width: 100%;font-size: 10px;color:  black;" id="tbComboInfo" class="table">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                                
                            </tr>
    
                        </table>
    <input type="button" id="SaveItemCode" class="btn-small btn-default" style="color:black" value="Save" />
    <input type="button" id="DialogClose" class="btn-small btn-default" style="color:black" value="Close" />

</div>



<div id="screenDialog" style="background:#333;display:none" title ="<%= Request.Cookies[Constants.BranchName].Value %>">

    <input type="hidden" id="hdnnetamt" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
    <div class="container" style="margin-top: -12px;">
        <%--<div class="row"  style="overflow-y:auto;width:100%;overflow-x:hidden; margin:0px;" id="MenuScroll">
            <div class="nav1 menu_tabs"> 
                <ul id="categories">
                </ul>
            </div>
        </div>--%>


         


                
    <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="bill_detail_screen">  
       
       <div class="col-xs-7" style="display: block; float:right;" id="colProducts">

           <div class="row"  style="overflow-y:auto;width:100%;overflow-x:hidden; margin:0px;" id="MenuScroll">
              <div class="nav1 menu_tabs"> 
                  <ul id="categories">
                  </ul>
				  <div class="btn-div">
					  <div class="arrow-div">
						  <a href="#" class="left-btn" type="button"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
						  <a href="#" class="right-btn" type="button"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
						</div>
				  </div>
              </div>
          </div>
          <div id="products">
          </div>
		     <div class="btn-left-panel">
					 <div class="left-btn-div">
                          
 <div class="button1" id="btnhold">
                                   
                                    (F12) Hold</div>
                                   
                            </div>

                            <div class="left-btn-div">
                         
<div class="button1" id="btnUnHold">
                                    (F11) UnHold</div>
                            </div>
				 <div class="left-btn-div">

                        <div class="button1" id="btnClear" style="vertical-align:middle">
                                    (F2) Refresh </div>

                            </div>
				   <div class="left-btn-div">
                                <div class="button1" id="btnCal">
                                    (F6) Calculator</div>
                            </div>
                             <div class="left-btn-div">
                          <div class="button3" id="btnreturn"  style="background:#f06671;">
                                    (F3) Return</div>

                            </div>            

				 </div>
		   <div class="pro-btn-div">
					  <div class="pro-arrow-div">
						  <a href="#" class="pro-left-btn" type="button"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
						  <a href="#" class="pro-right-btn" type="button"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div>
				  </div>
       </div>  
              
          <script>
           	var scrolled = 0;

           	$(document).ready(function () {


           		$(".right-btn").on("click", function () {
           			scrolled = scrolled + 300;

           			$("ul#categories").animate({
           				scrollTop: scrolled
           			});

           		});


           		$(".left-btn").on("click", function () {
           			scrolled = scrolled - 300;

           			$("ul#categories").animate({
           				scrollTop: scrolled
           			});

           		});


           		$(".clearValue").on("click", function () {
           			scrolled = 0;
           		});


           	});

           </script>
		     <script>
           	var scrolled = 0;

           	$(document).ready(function () {


           		$(".pro-right-btn").on("click", function () {
           			scrolled = scrolled + 300;

           			$("#products").animate({
           				scrollTop: scrolled
           			});

           		});


           		$(".pro-left-btn").on("click", function () {
           			scrolled = scrolled - 300;

           			$("#products").animate({
           				scrollTop: scrolled
           			});

           		});


           		$(".clearValue").on("click", function () {
           			scrolled = 0;
           		});


           	});

           </script>
 

         <div class="col-xs-5 col-div-product-lilst" >
			 <div class ="col-xs-12">
                <div class="Search search_section bill-screen-search" style="">
                    <table style="float:  left;">
                            <tr>

                                    <td>
                                    <asp:DropDownList ID="dd_customername"    class="form-control" runat="server"></asp:DropDownList>

                                </td>
                                             <td><b><asp:DropDownList ID="ddloption" runat="server">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                    <asp:ListItem Text="HomeDelivery" Value="HomeDelivery"></asp:ListItem>
                                                </asp:DropDownList></b>  </td> 
                                 
                                                 <td>
                                                 <b><select id="ddlEmployees" class="form-control" style="display:none"></select></b>
                                                 <%--<option value ="0">Choose</option></select></b>  --%>
                                                
                                                
                                               </td>
                                <td> <b><select id="ddlTable"class="form-control"></select></b></td>
                                <td >BillDate:</td><td>
                         
                                <input type="text" class="form-control input-small" style="width:120px;background-color:White;"  id="txtBillDate" aria-describedby="inputSuccess2Status" />
 
                                </td>
                                            
                                 <td><input type="button" id="btnclosekeyboard" style="height: 50px;background:  red; display:none;font-weight:  bold;" value="Close keyboard"/></td>
                                     <%--      <table width="100%" id="CashCustomer" style="display: none;color: wheat;border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing= "1" cellpadding="2"; margin:0 auto !important;>
                                               <tbody style="float:  left;">
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="lblCashCustomerName"style="font-weight: normal;margin-top: 1px;overflow-y: scroll;width: 336px;height: 59px;"></label>
                                                        </td>
                                                        <td style="padding-left:7px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                                        
                                                    </tr>
                                                   </tbody>
                                                      
                                                </table>--%>
                                </tr>
                                </table>

                                     <table class="code_search">
                                  <!--    <tr class="bill-screen-order-tab">


                                         </tr>-->
                                   <tr>
                                
                                          <td><input type="text" id="txtorderno" class="txt form-control" placeholder="Order No" aria-describedby="basic-addon2"  style="padding: 10px 10px;width: 100%;border: 0px;height: 50px;display:none"/></td>    
                                  <td>  <label  id="lblcustmobno"></label></td>
                               <td style="padding-left:5px" class="txt" id="autocomplete">

                                            
                                                <select id="ddlMobSearchBox"style="min-width:150px;" >

                                                </select>
                                                
                                                <div class="sdfg" id="dv_lblcustmobno"  style="display:none">
                                                <!--   <label  id="lblcustmobno"></label>-->
                                                <input type="text" placeholder="Mobile Number" style="display:none; border: solid 1px silver; padding: 5px"
                                                    id="txtMobSearchBox" class="txt ui-keyboard-input ui-widget-content ui-corner-all required valNumber"
                                                    aria-haspopup="true" role="textbox" />
                                                    </div>

                                                 <div id="btnCustomer" style="display:none;">
                                                    <img id="imgSrchCashCust" src="http://findicons.com/files/icons/1389/g5_system/32/toolbar_find.png" />
                                                </div>

                                            </td>

                                            <td>
                                              <img id="btnAddCustomer" src="images/adduser-white.png"/>
                                            </td>
                                <td>
                            
                                    <input type="text" class="txt form-control" placeholder="Enter Code" aria-describedby="basic-addon2"
                                        id="txtItemCode" style="padding: 10px 10px; width: 100%; border: 0px; height: 50px" />
                                </td>
                                <td>
                                    <span id="btnGetByItemCode" value="Search" >
                                        <img src="images/plusicon.png" alt=""/></span>
                                </td>
                                <td>
                                    <input type="text" class="txt form-control" placeholder="Search By Name" aria-describedby="basic-addon2"
                                        id="txtSearch" autocomplete="off"  style="padding: 10px 10px;width: 100%;border: 0px;height: 50px;" />
                                </td>
                                <td>
                                    <span   id="btnSearch" value="Search">
                                        <img src="images/search-button.png" alt=""/></span>
                                </td>
                               <%-- <td style="padding-left:50px">
                                 <input type="checkbox" data-index="27" style="font-size:25px" id="chkDelivery" /><label style="color:White;font-size:18px" id="lbldelcharges">Delivery Charges</label>
                                </td>--%>                            

                          </tr>
                            
                                        
                        </table>
                    </div>
                </div>
            <div class="" id="colDvProductList">
                <div id="dvProductList" class="product_items" style="display: block;">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                               <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                    Code
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>
                                  <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    Wt/Qty
                                </th>
                                   <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Tax
                                </th>
                                 <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    NetRate
                                </th>
                                 <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amount
                                </th>
                                
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" id="ScrollHgt" style="overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                                
                            </tr>

                         <%--      <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                  <table id="tbcomboInfo"></table>
                                </td>
                                
                            </tr>--%>
                       
                       
                          
                           
                        </table>
                      
                    </div>
                   
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    
                    <div class="cate cate-bill-screen" style="background-color:aquamarine!important">
               
                        <table style="border-collapse: separate; border-spacing: 1;font-size:15px; font-weight:700;" class="other-payment-tbl-billscreen">
                            <tr>
                                  <%--<td>
                                    Bill Type:
                                </td>--%>
                                <td></td>
                                <td>
                                    <select id="ddlbillttype" style="height: auto; width: 258px; padding-left: 0px;font-size:18px;" class="form-control" multiple="multiple">
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>
                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                 <td>
                                    <input type="text" class="form-control input-small" style="width: 90px" id="txtFinalBillAmount" />
                                </td>
                            </tr>

                               
                                             <tr>
                                <td>

                                <%--  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;float: left;font-size: 11px;min-width: 0;padding: 0;width: 222px;">
                                             <option value="0"></option>
                                             </select>--%>
                                     
                                    <span id="ddlcustName">Credit</span>
                                </td>
                                <td>
                                      <input type="text" class="form-control input-small cls_mmp" data="Credit" style="width: 90px;  height: 35px;display:none" id="txtCreditAmount" />
                       <%--         <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style="display: inline-block; font-size: 15px; font-weight: normal; height: 27px; padding-left: 0; 
  width: 165px; " class="form-control"> </asp:DropDownList>--%>
                                    <select id="ddlChosseCredit" style="display: none; font-size: 15px; font-weight: normal; height: 27px; padding-left: 0; 
  width: 165px; " class="form-control"></select>
							     </td>      
                                 <td> 
									 <img id="btnAddCreditCustomer" style="cursor: pointer;width: 28px;background-color: black;display:none"src="images/adduser-white.png">
                                 </td>
                                 <td >
                                              
                                  </td> 
                                              <%--    <td><input type="button" class="btn btn-primary btn-small" value="Search Customer" id="CreditSearchBtn" /></td>--%>
                                        </tr>
                                      
                              

                           
                              
                   
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: 700;">
                                        Cash Rec:</label>
                                </td>
                              
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="txt form-control input-small cls_mmp" data="Cash" style="width: 90px" value="0"
                                        id="txtCashReceived" />
                                            </td>
                                            <td>
                                         </td>
                                            
                                            <td>

                                                  <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span id="lblCardAmount"> Cr.Card Amount</span>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small cls_mmp" data="CreditCard" style="width: 90px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                   <span id="lblCreaditType">Type:</span> 
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 35px; width: 150px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>

                               
                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                 <span id="lblCCNO"> Cc No: </span> 
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 150px;height:35px;" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                  <span id="lblBank">Bank:</span>  
                                </td>
                                <td>
                                     <select id="ddlBank" style="height: 35px; width: 150px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            </div>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small cls_mmp" data="Cheque" style="width: 200px" id="Text16" />
                                </td>
                            </tr>

                            <tr style="display:none;"><td id="OP">Online</td>
              <td>
                               
                                </td>
                            <td> </td>
                            
                            
                                        
                                             </tr>

                                         <tr><td  style="text-align:left;width:50px"><input type="radio"   id="rdoCOD" name="abc" />
                         <input type="checkbox" id="codChk"  style="width:15px;height:19px" />  <label class="headings" id="COD" for="rdoCOD">COD</label></td>
                            <%--     <td id="Td1">Rs.</td>--%>

                             <td colspan="3">
                                    <table>
                                        <tbody><tr>
                                                <td>
                                                     
                                   <input class="form-control input-small cls_mmp" data="OnlinePayment" style="width: 90px;height:25px;color:Black;" id="txtCODAmount" type="text" value="0"/>&nbsp
                                </td>
                                <td class="online-tr-op">
                                    <input type="checkbox" id="OnlineChk"  style="width:15px;height:19px" />
                                    <label class="headings" id="Online" for="rdoOnline">Online</label>
                                </td>
                              <td>   <input type="text" class="form-control input-small cls_mmp" data="OnlinePayment"  style="width: 90px;height:25px;color:Black;" value="0"
                                        id="txtpaymentAmount" /></td>

                               <td><select id="ddlOtherPayment" class="form-control" clientidmode="Static" runat="server" style=" display: inline-block;
    float: left;
    font-size: 15px;
    min-width: 0;
    padding: 0;
    width: 145px;
	font-weight:400;
">
                                           
                                             </select></td>
                                        </tr>

                                    </tbody></table>


                                </td>
             
                 
 </tr>

                                             <tr>       <td id="coupan">Enter Coupan Qty</td>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 90px" value="0"
                                        id="Coupans" />
                                        </td>
                                                 <td><label id="lblOTP" style="margin: 0 0 0 -152px;">OTP</label></td>
                                                 <td><input type="text" class="form-control" id="OTPVal" style="margin: 0 0 0 -108px;width:84px;" /></td>
                                      <td style="width: 50px"><label id="No"style="display: inline-block; margin-left: -150px;" >CoupanNo</label><div id="holder"style="display: block; margin-left: -150px;"></div></td>
                                      <td style="width: 50px"><label id="Amt" >CoupanAmt</label>
                                          
                                          <div id="holder2"></div></td>
                                        
                                        </tr>
                           
                                <td style="width: 64px">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            

                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td style="width: 90px">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                       
                                                    </tr>

                                                    
                                                  
                                                </table>
                                            </td>
                                           
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                    
                    </div>
                </div>
         </div>
    
        </div>



    </div>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search  
                    </div>
                    <div class="cate">
                        <table width="100%" id="CreaditCustomerPart" style="background-color:bisque!important;display:none">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" checked="checked" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal;">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan = "100%">
                             <table>
                             <td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </table>
                                              
                                           
                                           
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Tablecate2
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <%--<div class="cate2" style="padding:0px;margin-top:0px;background: #F5FFFA;width:97%;padding-bottom: 40px;">--%>
                    
                    
                   <div class="col-md-6 col-sm-6 col-xs-12 no-of-itm">
                    <table class="table reciept_detail" id="tbliteminfo" style="float:left;font-size:12px; border: 1px solid #eeeeee;">
                                 <tr>         <%--  <td>No Of Items: </td>--%>
									 <td style="font-weight: bold;width:80px; text-align: left">
                                            Remarks:
                                        </td>
                                        <td style="width: 80px;border-right: 1px solid #eeeeee;"  >
                                            <input type="text" class="txt form-control" id="txtRemarks" />
                                            
                                        </td>
									 <td style="border-right: 1px solid #eeeeee;" colspan="2">
                            
                              <div style="text-align:center;width:60%;" class="item-no-td">
                                  <asp:Label ID ="lblNoItems"  runat="server" ClientIDMode ="Static"></asp:Label>
                                  </div></td><%--</tr>
                                  <tr>--%>
                                        
                                    </tr>
                                    <tr style="
    height: 20px;
"><td style="font-weight: bold;width:80px; text-align: left;font-weight:bold">
                                            Last Bill:
                                        </td><td style="border-right: 1px solid #eeeeee;"> <div id="lstBillNo" style="font-weight:bold;width: 120px;font-size:13px;color:Red;float:  left;">
                                            </div>
                         
                                             </td> <%-- </tr>
                        <tr style="
    height: 20px;
">--%>
                            <td >Amount:</td>
                            <td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt" style="font-weight:bold;font-size:13px;color:Red;"></div></td>

                        </tr>
                                        <%--    <tr style="border-bottom: 1px solid #eeeeee;border-right: 1px solid #eeeeee;"><td style="font-weight: bold;width:150px; text-align: left;font-weight:bold;border-bottom: 1px solid #eeeeee;"></td>--%>
                                               <%--<td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt" style="font-weight:bold;width: 146px;font-size:13px;color:Red;text-align:center"></div></td>--%>
                            
                    </table>

                       <p class="show_date"><%=DateTime.Now.DayOfWeek %>,<%=DateTime.Now %></p>
					   <div class="qty-btn-div">
						   <div class="button1" id="btn4">-250</div>
						   <div class="button1" id="btn2">+250</div>
						  <div class="button1" id="btn3">-500</div>
						   <div class="button1" id="btn1">+500</div>
					   </div>
                </div>

                <div style="float:right;" class="col-md-6 col-sm-6 col-xs-12 amt-details-tbl">
                    <table id="tbamountinfo" class="table reciept_detail" style="font-size: 12px;float: right;">
                        <tr>
                          
							<asp:Literal id="ltDateTime" runat="Server" Visible="false"/></tr>

                         
                           
                           

                                    <tr>

                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;border-right: 1px solid #eeeeee;" colspan="100%">
                                            <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Discount:
                                        </td>
                                        <td style="border-right: 1px solid #eeeeee;padding: 0px !important;">
                                            <table>
                                                <tr>

<td style="width: 30px; text-align: right;">
                                        <input type="text" class="form-control input-small" style="width:45px;height:19px;color:black;" 
                                                    id="dvdisper" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="3">
                                           <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;"  
                                                    id="dvdiscount" value ="0" />
                                        </td>
                                                </tr>


                                            </table>

                                        </td>


                                        
                                    </tr>
                                    <tr id ="trServicetax" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: right;"  colspan="3">
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="50%">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trKKC" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;    text-align: left">
                                            KKC:
                                        </td>
                                        <td style="width: 30px; text-align: right;"  colspan="3">
                                            <div id="dvKKCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="50%">
                                            <div id="dvKKCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trSBC" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;  text-align: left">
                                            SBC:
                                        </td>
                                        <td style="width: 30px; text-align: right;"  colspan="3">
                                            <div id="dvSBCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right"  colspan="50%">
                                            <div id="dvSBCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="vatIncOrExc" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   text-align: left">
                                            GST Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px; text-align: left;border-left: 1px solid #eeeeee;">
                                            DeliveryCharges:
                                        </td>
                                        <td style="width: 100px; text-align: right;border-right: 1px solid #eeeeee;padding-bottom: 0px;"  colspan="100%">
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;" id="dvDeliveryChrg" value ="0" />

                                                    </td>
                                                    </tr>
                                           
                                     
                                     <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;display:none">
                                        <td style="padding-right: 10px;height: 25px;  text-align: left;">
                                            Round Off:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: left;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                            Net Amt:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>
                            
                                           
                                </table>
                            </div>
                          <%--  </div>--%>

                            <div id="dvCreditCardWindow" style="display:none">
                                    <table>
                                     
                                    <tr><td>Bank:</td><td><select id="ddlCreditCardBank"></select></td>
                                    <td>
                                    
                                    <div  class="btn btn-primary" id="btnCreditCardBilling">
                                    Create Bill
                                     </div> 
                                    </td>
                                    
                                    </tr>
                                  

                                    </table>
                                    
                                    </div>
                    <table class="button_options">
                      
                        <tr id="OnEditHide">
                           
                            <td id="tdCash">
                            <div class="button1" id="btnCash">
                                   (F1) Cash</div>
                            
                          
                               
                            </td>
                            <td id="tdCreaditCard">

                  
                             
                           
   <div class="button1" id="btnCreditCard">
                                    (F8) Credit Card</div>

                                 <div class="button4" id="btnCredit" style="background:gray;display:none" disabled>
                                  Credit</div> 
                            </td>
                          <td id="OnlinePaymode" style="display:none">
                               <div class="button1" id="OnlinePaymodes">Online Payment</div>
                          </td>
                            <td id="tdsavebill">
                        <div class="button1" id="btnsavebill">
                                      (F4) Other Pay Mode</div>
                            </td>
                        
<%--                            </tr>
                        <tr>--%>
                       

                           

                      <td>
                          <div id="dvShow_pm" class="button3" style="background: #f06671;">
                                                                (F7) CHANGE PAYMODE</div>
                      </td>
                            
                           


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>
                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
    
                                         
            </div>

<div id="screenDialog1" style="background:#333;display:none" title ="<%= Request.Cookies[Constants.BranchName].Value %>">

    <input type="hidden" id="hdnnetamt1" value="0" />
    <input type="hidden" id="hdnCreditCustomerId1" value="0" />
    <div class="container" style="margin-top: -12px;">
        


         <div class ="col-xs-12">
                <div class="Search search_section bill-screen-search" style="">
                    <table style="float:  left;">
                            <tr>

                                    <td>
                                    <asp:DropDownList ID="dd_customername1"    class="form-control" runat="server"></asp:DropDownList>

                                </td>
                                             <td><b><asp:DropDownList ID="ddloption1" runat="server">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                    <asp:ListItem Text="HomeDelivery" Value="HomeDelivery"></asp:ListItem>
                                                </asp:DropDownList></b>  </td> 
                                 
                                               
                                <td> <b><select id="ddlTable1"class="form-control"></select></b></td>
                                
                                            
                              
                                </tr>
                                </table>

                                   
                    </div>
                </div>


                
    <div class="col-md-12 col-sm-12 col-xs-12">
       <div class="bill_detail_screen">  
       
       <div class="col-xs-7" style="display: block; float:right;" id="colProducts1">

           <div class="row"  style="overflow-y:auto;width:100%;overflow-x:hidden; margin:0px;" id="MenuScroll1">
              <div class="nav1 menu_tabs"> 
                  <ul id="categories1">
                  </ul>
              </div>
          </div>
          <div id="products1">
          </div>
       </div>  
              
           

         <div class="col-md-12 col-div-product-lilst" >
            <div class="" id="colDvProductList1">
                <div id="dvProductList1" class="product_items" style="display: block;">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                               <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                    Code
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>
                                  <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    Wt/Qty
                                </th>
                                   <th style="width: 25px; text-align: center; color: #FFFFFF;">
                                    
                                </th>
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                 <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    Tax
                                </th>
                                <th style="width: 53px; text-align: center; color: #FFFFFF;">
                                    NetRate
                                </th>
                                 <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amount
                                </th>
                                
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" id="ScrollHgt1" style="overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo1">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                                
                            </tr>

                         <%--      <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 12px">
                                  <table id="tbcomboInfo"></table>
                                </td>
                                
                            </tr>--%>
                       
                       
                          
                           
                        </table>
                      
                    </div>
                   
                </div>
               
              
                </div>
         </div>
    
        </div>



    </div>
               


                <%--<div class="cate2" style="padding:0px;margin-top:0px;background: #F5FFFA;width:97%;padding-bottom: 40px;">--%>
                    
                    
                   <div class="col-md-6 col-sm-6 col-xs-12 no-of-itm">
                    <table class="table reciept_detail" id="tbliteminfo1" style="float:left;font-size:12px; border: 1px solid #eeeeee;">
                                 <tr>           <td>No Of Items: </td><td style="border-right: 1px solid #eeeeee;">
                            
                              <div style="text-align:center;width:60%;">
                                  <asp:Label ID ="lblNoItems1"  runat="server" ClientIDMode ="Static"></asp:Label>
                                  </div></td></tr>
                                  <tr>
                                        <td style="font-weight: bold;width:80px; text-align: left">
                                            BillMode:
                                        </td>
                                        <td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtRemarks1" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td>
                                    </tr>
                                    <tr style="
    height: 20px;"> <td style="font-weight: bold;width:80px; text-align: left">
                                            CustomerName:
                                        </td>
                                        <td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtcustomer1" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td> 
                                        <td style="width: 100px;border-right: 1px solid #eeeeee;"  colspan="100%;">
                                            <input type="text" class="txt form-control" id="txtcst" style="width: 132px;" disabled="disabled"/>
                                            
                                        </td> 

                                    </tr>
                        <tr style="
    height: 20px;
">
                           <%-- <td > LastBillAmount:</td>
                            <td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt1" style="font-weight:bold;font-size:13px;color:Red;"></div></td>--%>

                        </tr>
                                        <%--    <tr style="border-bottom: 1px solid #eeeeee;border-right: 1px solid #eeeeee;"><td style="font-weight: bold;width:150px; text-align: left;font-weight:bold;border-bottom: 1px solid #eeeeee;"></td>--%>
                                               <%--<td style="border-right: 1px solid #eeeeee;"> <div id="lstBillAmt" style="font-weight:bold;width: 146px;font-size:13px;color:Red;text-align:center"></div></td>--%>
                            
                    </table>

                       <p class="show_date"><%=DateTime.Now.DayOfWeek %>,<%=DateTime.Now %></p>

                </div>

                <div style="float:right;" class="col-md-6 col-sm-6 col-xs-12 amt-details-tbl">
                    <table id="tbamountinfo1" class="table reciept_detail" style="font-size: 12px;float: right;">
                        <tr>
                          
							<asp:Literal id="Literal1" runat="Server" Visible="false"/></tr>

                         
                           
                           

                                    <tr>

                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;border-right: 1px solid #eeeeee;" colspan="100%">
                                            <div id="dvsbtotal1">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;text-align: left;border-left: 1px solid #eeeeee;">
                                            Discount:
                                        </td>
                                        <td style="border-right: 1px solid #eeeeee;padding: 0px !important;">
                                            <table>
                                                <tr>

<td style="width: 30px; text-align: right;">
                                        <input type="text" class="form-control input-small" style="width:45px;height:19px;color:black;" 
                                                    id="dvdisper1" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="3">
                                           <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;"  
                                                    id="dvdiscount1" value ="0" />
                                        </td>
                                                </tr>


                                            </table>

                                        </td>


                                        
                                    </tr>
                                   
                                    <tr id ="vatIncOrExc1" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   text-align: left">
                                            GST Amount:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvVat1">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges1" style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px; text-align: left;border-left: 1px solid #eeeeee;">
                                            DeliveryCharges:
                                        </td>
                                        <td style="width: 100px; text-align: right;border-right: 1px solid #eeeeee;padding-bottom: 0px;"  colspan="100%">
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:19px;color:Black;" id="dvDeliveryChrg1" value ="0" />

                                                    </td>
                                                    </tr>
                                           
                                     
                                     <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;display:none">
                                        <td style="padding-right: 10px;height: 25px;  text-align: left;">
                                            Round Off:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvRound1">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr style="border-right: 1px solid #eeeeee;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: left;border-left: 1px solid #eeeeee;border-bottom: 1px solid #eeeeee;">
                                            Net Amt:
                                        </td>
                                        <td style="width: 100px; text-align: right;"  colspan="100%">
                                            <div id="dvnetAmount1">
                                            </div>
                                        </td>
                                    </tr>
                            
                                           
                                </table>
                            </div>
                          <%--  </div>--%>

                         
                  
                </div>
    
                                         
            </div>
            
           
            
      <link href="css/Calculator/creative.css" rel="stylesheet" />
       <script src="js/Calculator/calculate.js"></script>
        <div id="getCalculator"  style="position:absolute;z-index:100;">
              <div class="calculator" align="center" >
                 <div class="x_panel">
      <div class="row displayBox">
        <p class="displayText" id="display">0</p>
      </div>
      <div class="row numberPad">
        <div class="col-md-9 col-sm-9 col-xs-9">
        <div class="num_key">
          <div class="row">
            <button class="btn clear hvr-back-pulse change-font" id="clear" style="background:#3AC9A9 !important">C</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="sqrt" style="background:#3AC9A9 !important">√</button>
            <button class="btn btn-calc hvr-radial-out hvr-radial-out change-font" id="square" style="background:#3AC9A9 !important">x<sup>2</sup></button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="seven" style="background:#3AC9A9 !important">7</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="eight" style="background:#3AC9A9 !important">8</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="nine" style="background:#3AC9A9 !important">9</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="four" style="background:#3AC9A9 !important">4</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="five" style="background:#3AC9A9 !important">5</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="six" style="background:#3AC9A9 !important">6</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="one" style="background:#3AC9A9 !important">1</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="two" style="background:#3AC9A9 !important">2</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="three" style="background:#3AC9A9 !important">3</button>
          </div>
          <div class="row">
            <button class="btn btn-calc hvr-radial-out change-font" id="plus_minus" style="background:#3AC9A9 !important">&#177;</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="zero" style="background:#3AC9A9 !important">0</button>
            <button class="btn btn-calc hvr-radial-out change-font" id="decimal" style="background:#3AC9A9 !important">.</button>
          </div>
          </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-3 operationSide">
          <button id="divide" class="btn btn-operation hvr-fade change-font" style="background:#3AC9A9 !important">÷</button>
          <button id="multiply" class="btn btn-operation hvr-fade change-font" style="background:#3AC9A9 !important">×</button>
          <button id="subtract" class="btn btn-operation hvr-fade change-font" style="background:#3AC9A9 !important">−</button>
          <button id="add" class="btn btn-operation hvr-fade change-font" style="background:#3AC9A9 !important">+</button>
          <button id="equals" class="btn btn-operation equals hvr-back-pulse change-font" style="background:#3AC9A9 !important">=</button>
        </div>
      </div>
                   <input  type="button" id="CloseCal" class="btn-small btn cal_close" value="Close" />
    </div>
</div>
          <div id="TenderAmounts" style="display:none;" title="Tender Amount">
            <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
              <label>Net Amount:</label>
              </div>
              <div class="col-md-6">
                <input type="text" id="TotalNetAmount" disabled="disabled" />
              </div>
              </div>
            </div>
             <div class="row">
            <div class="col-md-12" style="margin-top:10px;">
              <div class="col-md-6">
              <label>Paid Amount:</label>
              </div>
              <div class="col-md-6">
                <input type="text" id="TenderAmount"/>
              </div>
              </div>
            </div>
              <div class="row">
                  <div class="col-md-12" style="margin-top:10px;">
                <div class="col-md-6"><label>Tender Amount:</label></div>
                <div class="col-md-6">
                  <label id="TotalTenderAmount"></label>
                </div>

                  </div>
              </div>
          </div>
</div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#OnlineChk").change(function () {
                if ($("#OnlineChk").prop('checked')) {
                    var FinalAmount = $("#txtFinalBillAmount").val();
                    $("#txtpaymentAmount").attr('readonly', 'readonly');
                    $("#txtCODAmount").attr('readonly', 'readonly');
                    $("#txtBalanceReturn").val(0)
                    $("#txtpaymentAmount").val(FinalAmount);
                    $("#txtCODAmount").val(0);
                    $("#codChk").prop('checked', false);
                }
            })
            $("#codChk").change(function () {
                if ($("#codChk").prop('checked')) {
                    var FinalAmount = $("#txtFinalBillAmount").val();
                    $("#txtpaymentAmount").attr('readonly', 'readonly');
                    $("#txtCODAmount").attr('readonly', 'readonly');
                    $("#txtpaymentAmount").val(0);
                    $("#txtBalanceReturn").val(0)
                    $("#txtCODAmount").val(FinalAmount);
                    $("#OnlineChk").prop('checked', false);
                }
            })
        })
    </script>
  <script type="text/javascript">
      function AdjustMinues() {
          var liWidthCount = 0;
          var UlWidth = 1200;
          $("#categories").children().each(function (i, obj) {
              liWidthCount = parseInt(liWidthCount) + parseInt($(obj).width())
          })
          if (liWidthCount >= UlWidth) {
              $("#categories").remove('style');
              //$("#categories").attr('style', 'width:' + liWidthCount + 'px;table-layout:fixed"')
              $("#categories").attr('style', 'width:100%;table-layout:fixed;float: left;height:auto;margin-left: 0px;overflow-x: auto;display: flex;')
              $("#MenuScroll").removeAttr('style');
              $("#MenuScroll").attr('style', 'overflow-y:auto;width:100%;')
          }
      }
  </script>
  
   <script type="text/javascript">
       $(document).ready(function () {
           $("#getCalculator").hide();
       })
       $(document).on('click', '#btnCal', function () {
           $("#getCalculator").show();
       });
       $(document).on('click', '#CloseCal', function () {
           $("#getCalculator").hide();
       });
  </script>
  <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          jQuery("#jQGridDemoBill").GridUnload();

          var $grid = jQuery("#jQGridDemoBill").jqGrid({
			  url: 'handlers/ManageVatBills.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CstName', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer', 'CashCustAddr', 'CreditCustAddr', 'DeliveryCharges', 'KKCPer', 'KKCAmt', 'SBCPer', 'SBCAmt', 'User', 'Time', 'posid','cst_id'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 300, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                            { name: 'cst_name', index: 'cst_name', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCPer', index: 'KKCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCAmt', index: 'KKCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCPer', index: 'SBCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCAmt', index: 'SBCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'UserName', index: 'UserName', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                            { name: 'Time', index: 'Time', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                              { name: 'posid', index: 'posid', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                             { name: 'cst_id', index: 'cst_id', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },


              ],

              rowNum: 10,
              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'Bill_No',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'asc',
              ignoreCase: true,
              caption: "Bills List",
              rowList: [10, 20, 30, 100000000],
              loadComplete: function () {
                  $("option[value=100000000]").text('All');
              },
          })
          $('#t_' + $.jgrid.jqID($grid[0].id))
            .append($("<div><label for=\"globalSearchText\">Global search:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $(document).ready(function () {
              $("#gview_jQGridDemoBill").removeAttr('style');
              $("#gview_jQGridDemoBill").attr('style', 'width: 716px;');
              //$('#jQGridDemoBill').closest(".ui-jqgrid-bdiv").css({ "max-height: 364px;overflow-y": "scroll" });
              $(".ui-jqgrid-bdiv").remove('style');
              $(".ui-jqgrid-bdiv").attr('style', 'height: 100%;width: 716px;overflow-y": "scroll');

          });
          $(function () {
              $('#globalSearchText').keyboard();
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


              $("#btnEdit").css({ "display": "none" });
              $("#btnDelete").css({ "display": "none" });
              $("#btnShowScreen").css({ "display": "none" });


              for (var i = 0; i < arrRole.length; i++) {

                  if (arrRole[i] == 9) {

                      $("#btnShowScreen").css({ "display": "block" });
                  }
                  if (arrRole[i] == 2) {

                      $("#btnDelete").css({ "display": "block" });
                  }
                  if (arrRole[i] == 3) {
                      $("#btnEdit").css({ "display": "block" });
                      m_BillNowPrefix = 0;
                      m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

                  }
                  m_BillNowPrefix = 0;
                  m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');
              }







              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
              //  $("#txtddlMobSearchBox").val(CashCustName);

              //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

          }
      });





      $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
              {
                  refresh: false,
                  edit: false,
                  add: false,
                  del: false,
                  search: false,
                  searchtext: "Search",
                  addtext: "Add",
              },

              {//SEARCH
                  closeOnEscape: true

              }

                );


      var DataGrid = jQuery('#jQGridDemoBill');
      DataGrid.jqGrid('setGridWidth', '716');


  }

  $(".getprice").click(function () {

      //var prc = $(this).attr('class');
      alert("ok");
  });
  </script>
<%--     <link href="virtualKeyboard/jquery.ml-keyboard.css" rel="stylesheet" />

    <script src="virtualKeyboard/jquery.ml-keyboard.min.js"></script>--%>
    <script type="text/javascript">



    </script>
    <script type="text/javascript">
        $('#<%=dd_customername.ClientID %>').change(function () {
            PaymentModeID = $('#<%=dd_customername.ClientID %> option:selected').val();

            cst_id = $('#<%=dd_customername.ClientID %> option:selected').val();



            if (cst_id != 0) {

                $("#dvDeliveryChrg").val(0);
               // $("#dvDeliveryChrg").attr("disabled", "disabled");
                $("#chkDelivery").hide();
                $("#lbldelcharges").hide();
                //$("#DelCharges").hide(); 
                $("#dvnetAmount").text(0);
                $("#txtorderno").show();

            }
            else {
                $("#dvDeliveryChrg").val(DeliveryCharges);
               // $("#dvDeliveryChrg").attr("disabled", "disabled");
                $("#chkDelivery").hide();
                $("#lbldelcharges").hide();
                $("#DelCharges").show();
                $("#txtorderno").val("");
                $("#txtorderno").hide();

            }

        });

        //$('input.txt').mlKeyboard({
        //    layout: 'en_US'
        //}); 
        //$("#btnclosekeyboard").click(function () {

        //    $("#mlkeyboard").css('display', 'none');
        //    $("#btnclosekeyboard").css('display', 'none');


        //});




        //$('input.txt').click(function () {

        //    $("#btnclosekeyboard").css('display', 'block');

        //});


        //$('input[type="text"]').click(function () {
        //    $.ajax({
        //        type: "POST",
        //        url: "billscreen.aspx/Keyboard",
        //        contentType: "application/json",
        //        dataType: "json",

        //        error: function (xhr, ajaxOptions, thrownError) {

        //            var obj = jQuery.parseJSON(xhr.responseText);
        //            alert(obj.Message);
        //        },
        //        complete: function (msg) {
        //        }

        //    });
        //    $(this).focus();
        //});

        //$(".txt").click(function () {
        //    $.ajax({
        //        type: "POST",
        //        url: "billscreen.aspx/Keyboard",
        //        contentType: "application/json",
        //        dataType: "json",

        //        error: function (xhr, ajaxOptions, thrownError) {

        //            var obj = jQuery.parseJSON(xhr.responseText);
        //            alert(obj.Message);
        //        },
        //        complete: function (msg) {
        //        }

        //    });
        //    $(this).focus();
        //});


    </script>
  <script type="text/javascript">
      function CalculateComboQty(value) {
          var Qty = parseFloat($(value).eq(0).val());
          var Price = parseFloat($(value).parent().parent().find('td').eq(5).text());
          var Calculate = Qty * Price;
          $(value).parent().parent().find('td').eq(6).find('input').val('');
          $(value).parent().parent().find('td').eq(6).find('input').val(Calculate);
          subcm_Qtyamt = 0;
          $('.subQtycombo_amt').each(function (i, obj) {
              subcm_Qtyamt += parseFloat($(obj).find('input').val());
              //alert(subcm_amt);
          })
          var classlnt = $('.SumMainQtyCombo').nextAll().length;

          var ComboCalculation = subcm_Qtyamt;
          if (ComboCalculation == 'Infinity') {
              ComboCalculation = subcm_amt / 1;
          }
          if (ComboCalCount == 0) {
              if ($(".comboQtyamt").val() != "") {
                  $(".comboQtyamt").val(0);
              }
              $(".comboQtyamt").val(parseFloat(ComboCalculation).toFixed(2));
          }
          else {
              $(".comboQtyamt").val(parseFloat(ComboCalculation).toFixed(2));
          }

      }
  </script>
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
               <iframe id="reportkot" width="0" height="0" onload="processingComplete()"></iframe>
</asp:Content>

