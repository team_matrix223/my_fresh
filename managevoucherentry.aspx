﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="managevoucherentry.aspx.cs" Inherits="managevoucherentry" %>

<%@ Register Src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/Inventory.css" rel="stylesheet" />
    <link href="css/customcss/managevoucherentry.css" rel="stylesheet" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script src="js/jscommoncalculation.js" type="text/javascript"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js"></script>
    <script>

        function ApplyRoles(Roles) {

            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


      

    </script>
  
    <script>
        $(document).ready(function () {
            $("#btnSubmit").click(function () {
                $(".jq-cls").addClass("jq-zin");
            }); 
            $("#btnSave, #btnCloseMe").click(function () {
                $(".jq-cls").removeClass("jq-zin");
            });
                    
        });
    </script>
   
    <form id="form1" runat="server" autocomplete="off">
        <%--<asp:HiddenField ID="hdnDate" runat="server"/>--%>
        <asp:HiddenField ID="hdnRoles" runat="server" />
        <input type="hidden" id="hdnFirstTime" value="1" />
        <input type="hidden" id="hdnCounter" value="1" />
        <input type="hidden" id="hdnUpdateCounter" value="0" />
        <input type="hidden" id="status" value="I" />
        <input type="hidden" id="hdnGrnNo" value="0" />
         <input type="hidden" id="hdnVType" value="0" />
        <input type="hidden" id="hdntodaydate" runat="server" value="0" />
        <asp:DropDownList ID="hdnProducts" Style="display: none" runat="server">
        </asp:DropDownList>
        <div class="right_col" role="main">
            <div class="">

              
            </div>
            <div class="clearfix">
            </div>
            <div class="row purchase-row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Voucher Entry</h3>
                        </div>
                    </div>

                    <div class="x_panel inventory_xpanal">
                        <div class="x_title">
                            <h2>Add/Edit Voucher</h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    aria-expanded="false"><i class="fa fa-wrench"></i></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#">Settings 1</a> </li>
                                        <li><a href="#">Settings 2</a> </li>
                                    </ul>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                            </ul>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="x_content">
                            <br />
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <table style="width: 450px; margin-bottom: 10px">
                                            <tr>
                                                <td>Date From:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px; background-color: White"
                                                        id="txtDateFrom" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td></td>
                                                <td>Date To:
                                                </td>
                                                <td>
                                                    <input type="text" readonly="readonly" class="form-control input-small" style="width: 120px; background-color: White"
                                                        id="txtDateTo" aria-describedby="inputSuccess2Status" />
                                                </td>
                                                <td>
                                                    <div id="btnGo" class="btn btn-primary btn-small">
                                                        <i class="fa fa-search"></i>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                      

                        <div id="addDialog" class="inventry_adddialog" style="display: none;">

                            <div class="col-md-12 col-sm-12 col-xs-12  mng-pur-rturn-wdth">

                                <div>

                                    <table class="manage_table_top purchase-manage-table-top mng-pur-top-tbl">
                                        <tr>
                                            <td>
                                                <table>
                                                     <tr>
                                                        <td class="branch-td">Branch:</td>
                                                        <td>
                                                            <%--<select id="ddlSupplier"  style="width:93px;height:30px"> <option></option> </select>--%>
                                                            <asp:DropDownList ID="ddlBranch" ClientIDMode="Static" Style="width: 100%" runat="server"></asp:DropDownList>
                                                        </td>
                                                       
                                                    </tr>
                                                    <tr>
                                                        <td>Voucher No:</td>
                                                        <td>
                                                            <input type="text" id="txtVouchNo" class="form-control input-small" disabled="disabled" value="Automatic" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>TR Date:</td>
                                                        <td>
                                                            <input type="text" id="txtTrDate" class="form-control input-small" tabindex="-1" /></td>

                                                       
                                                    </tr>
                                                   
                                                </table>
                                            </td>
                                            <td>
                                                <table>
                                                    
                                                    <tr>
                                                        <td id="tdbnk" style="display:none">Bank:</td>
                                                        <td>
                                                            <%--<select id="ddlSupplier"  style="width:93px;height:30px"> <option></option> </select>--%>
                                                            <asp:DropDownList ID="ddlBank" ClientIDMode="Static" Style="width: 100%;display:none" runat="server"></asp:DropDownList>
                                                        </td>
                                                       
                                                    </tr>
                                                      <tr>
                                                        <td id="tdcsh">CashBal:</td>
                                                        <td>
                                                            <label id="lblcashbal">0</label>
                                                            
                                                        </td>
                                                       
                                                    </tr
                                                     <tr>
                                                        <td id="tdbnkbal" style="display:none" >Bal:</td>
                                                        <td>
                                                           
                                                           <label id="lblbankbal" style="display:none">0</label>
                                                        </td>
                                                       
                                                    </tr>
                                                </table>
                                            </td>
                                           
                                           
                                            <td>
                                                <table>
                                                    <tr>
                                                        <td class="cash-td">
                                                            <input type="radio" id="rdbcash" name="cash" checked="checked" />
                                                            <label for="rdbcash">Cash</label>
                                                        </td>
                                                        <td class="bank-td">
                                                            <input type="radio" id="rdbbank" name="cash" />
                                                            <label for="rdbbank">Bank</label>
                                                        </td>
                                                         <td class="journey-td">
                                                            <input type="radio" id="rdbJournal" name="cash" />
                                                            <label for="rdbJournal">Journal</label>
                                                        </td>
                                                    </tr>

                                                     <tr id="bnk" style="display:none">
                                                        <td>
                                                            <input type="radio" id="rdbSingle" name="Single" checked="checked" />
                                                            <label for="rdbSingle">Single</label>
                                                        </td>
                                                        <td>
                                                            <input type="radio" id="rdbMultiple" name="Single" />
                                                            <label for="rdbMultiple">Multiple</label>
                                                        </td>
                                                        
                                                    </tr>
                                         
                                                </table>
                                            </td>
                                        </tr>
                                    </table>

                                    <table style='width: 100%'>
                                        <tr>
                                            <td colspan="100%">
                                                <div class="pro-tbl">
                                                    <table class="manage_table_top" style="margin: 0px;" id="tbProducts">
                                                        <thead>
                                                            <tr>
                                                                <th>Code</th>
                                                                <th>Description</th>
                                                                <th>Balance</th>
                                                                <th class="pay">Payment</th>
                                                                <th class="rec">Receipt</th>
                                                                <th class="chq">Cheque No</th>
                                                                <th class="deb">Debit</th>
                                                                <th class="cre">Credit</th>
                                                                <th>Narr</th>
                                                                <th class="cc">CCODE</th>
                                                               
                                                                <th></th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>

                                        <table style="width: 100%;">
                                                    <tr>
                                                        
                                                                   
                                                                    <td colspan="100%">
                                                                        <table style="float: right; width: 350px;">
                                                                           <%-- <tr>
                                                                                <td>Difference:</td>
                                                                                <td>Rs.<label id="lblTotalAmount">0</label></td>
                                                                            </tr>
                                                                                   --%>                            
                                                                            
                                                                              
                                                                             
                                                                           
                                                     

                                                                            
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                           
                                                               
                                                            </table>
                                                       
                                                       
                                                    </tr>
                                        
                                                </table>
                                            </td>
                                          
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <div class="btn-sub-section-bottom">
                              <div class="btn btn-primary inv_sub_canc" data-toggle="modal" id="btnSubmit">
                                    <i class="fa fa-save"></i> Submit
                                </div>
                                <div id="btncncl" data-toggle="modal" class="btn   btn-danger inv_sub_canc">
                                    <i></i>Cancel
                                </div>
                            </div>
                        </div>
                        <div id="btnAdd" data-toggle="modal" class="btn btn-primary" style="margin-left: 6px">
                            <i class="fa fa-external-link"></i>New
                        </div>
                        <div id="btnEdit" data-toggle="modal" class="btn  btn-success">
                            <i class="fa fa-edit m-right-xs"></i>Edit
                        </div>
                    </div>
                </div>
            </div>
            <script>
          
                $(function () {
					$(".cc").hide();
					$(".chq").hide();
					$(".txtchq").hide();
					$(".deb").hide();
					$(".txtdeb").hide();
					$(".cre").hide();
					$(".txtcre").hide();
                    $("#txtTrDate").datepicker({
                        yearRange: '1900:2030',
               
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'mm-dd-yy'
                    });

                });

               
            </script>
            <script language="javascript" type="text/javascript">

                function processingComplete() {


                    $.uiUnlock();
                }


                function Printt(celValue) {

                    $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

                    var iframe = document.getElementById('reportout');

                    iframe = document.createElement("iframe");

                    iframe.setAttribute("id", "reportout");

                    iframe.style.width = 0 + "px";

                    iframe.style.height = 0 + "px";

                    document.body.appendChild(iframe);


                    document.getElementById('reportout').contentWindow.location = "Reports/rptPurchaseBill.aspx?Id=" + celValue;

                }


              



              


              

                function ServiceClick(counterId) {


                    var x = $("#ddlProducts" + counterId).val();
                    $("#ddlProducts" + counterId).html($("#<%=hdnProducts.ClientID%>").html());


         $("#ddlProducts" + counterId + " option").removeAttr("selected");
         $('#ddlProducts' + counterId + ' option[value=' + x + ']').prop('selected', 'selected');

         document.getElementById("ddlProducts" + counterId).onmouseover = null;
         document.getElementById("txtServiceId" + counterId).onmouseover = null;


     }
    



     function DeleteRow(counterId) {


         var len = $("input[name='txtServiceId']").length;
         if (len == 1) {
             alert("Row deletion failed. Package must contain atleast one service");
             return;
         }
         var tr = $("#btnRemove" + counterId).closest("tr");
         tr.remove();
        



     }

     var global_counterId = "";
    

     // register jQuery extension
     jQuery.extend(jQuery.expr[':'], {
         focusable: function (el, index, selector) {
             return $(el).is('a, button, :input, [tabindex]');
         }
     });
     var counter_cant = 0;
     $(document).on('keypress', 'input,select', function (e) {

         var focus_on;
         var counter;
         $(":focus").each(function () {
             focus_on = this.name;
             try {
                 counter = this.attributes.counter.value;
             } catch (e) {
                 counter = "";
             }
          
         });
       
        
		
             if (focus_on == "txtReceipt") {

                 if (e.which == 13) {

                     $("#txtNarr" + counter).focus();
                 }

             }
         
         

		 $('#rdbJournal').is(":checked")
         {
			 if (focus_on == "txtBalance") {

				 if (e.which == 13) {

                     $("#txtDebit" + counter).focus();
                     $("#txtDebit" + counter).select();
				 }

			 }
         }

		 $('#rdbbank').is(":checked")
		 {
			 if (focus_on == "txtReceipt") {

				 if (e.which == 13) {

                     $("#txtChqNo" + counter).focus();
                    
				 }

			 }

		 }
		 if (focus_on == "txtChqNo") {

			 if (e.which == 13) {

				 $("#txtNarr" + counter).focus();
			 }

         }

		 if (focus_on == "txtDebit") {

			 if (e.which == 13) {

                 $("#txtCredit" + counter).focus();
                 $("#txtCredit" + counter).select();
			 }

		 }

		 if (focus_on == "txtCredit") {

			 if (e.which == 13) {

				 $("#txtNarr" + counter).focus();
			 }

		 }

		
		 if (focus_on == "txtBalance") {

			 if (e.which == 13) {
                 $("#txtPayment" + counter).focus();
				 $("#txtPayment" + counter).select();
				 
			 }

         }
		 if (focus_on == "txtPayment") {

			 if (e.which == 13) {

                 $("#txtReceipt" + counter).focus();
                 $("#txtReceipt" + counter).select();

			 }

		 }
		 if (focus_on == "txtNarr") {

			 if (e.which == 13) {

                 addTR();

			 }

		 }

       
     });


    

     function CodeSearch(counterId, e) {


         if (e.keyCode == 13) {
             global_counterId = counterId;

             var serviceId = $("#txtServiceId" + counterId).val().toUpperCase();
             var cntr = 0;
             $("select[name='ddlProducts']").each(
                 function () {

                     if (serviceId == $(this).val()) {
                         cntr++;

                     }

                 });


             
             var ddlServiceVal = "";
            
             $('#ddlProducts' + counterId + ' option').each(
                 function () {

                     if ($(this).val() == serviceId) {
                         ddlServiceVal = $(this).val();
						 $.ajax({
							 type: "POST",
							 data: '{ "CCODE": "' + serviceId + '"}',
							 url: "managevoucherentry.aspx/GetBalance",
							 contentType: "application/json",
							 asyn: false,
							 dataType: "json",
							 success: function (msg) {

								 var obj = jQuery.parseJSON(msg.d);

                                 $("#txtBalance" + counterId).val(obj.counter);
                                

							 },
							 error: function (xhr, ajaxOptions, thrownError) {

								 var obj = jQuery.parseJSON(xhr.responseText);
								 alert(obj.Message);
							 },
							 complete: function () {




							 }




						 });



                     }
                 });

             if (ddlServiceVal == "") {



                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $("#txtBalance" + counterId).val("0");
                 $("#txtPayment" + counterId).val("0");
                 $("#txtReceipt" + counterId).val("0");
                 $("#txtChqNo" + counterId).val("");

                 $("#txtDebit" + counterId).val("0");
                 $("#txtCredit" + counterId).val("0");
                 $("#txtNarr" + counterId).val("");
				 $("#txtCCOde" + counterId).val("");
                 
                // alert("Product Not Found!!");

             }
             else {
                
                 $("#ddlProducts" + counterId + " option").removeAttr("selected");
                 $('#ddlProducts' + counterId + '  option[value=' + ddlServiceVal + ']').prop('selected', 'selected');

                // cost = $('#ddlProducts' + counterId + ' option[value=' + serviceId + ']').attr("cost");

				 $("#txtBalance" + counterId).val("0");
				 $("#txtPayment" + counterId).val("0");
				 $("#txtReceipt" + counterId).val("0");
				 $("#txtChqNo" + counterId).val("");

				 $("#txtDebit" + counterId).val("0");
				 $("#txtCredit" + counterId).val("0");
				 $("#txtNarr" + counterId).val("");
				 $("#txtCCOde" + counterId).val("");

				 
                 $("#txtServiceId" + counterId).val(ddlServiceVal);
				 $("#txtBalance" + counterId).focus();

             }

         }


     }


     function ServiceChange(counterId) {


         var serviceId = $("#ddlProducts" + counterId).val();
         global_counterId = counterId;
         var cntr = 0;
         $("select[name='ddlProducts']").each(
                 function () {


                     if (serviceId == $(this).val()) {
                         cntr++;
                     }

                 });


         var ddlServiceVal = "";

         ddlServiceVal = serviceId;
       
         $("#txtServiceId" + counterId).val(ddlServiceVal);


		 $.ajax({
			 type: "POST",
			 data: '{ "CCODE": "' + serviceId + '"}',
			 url: "managevoucherentry.aspx/GetBalance",
			 contentType: "application/json",
			 asyn: false,
			 dataType: "json",
			 success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 
                 $("#txtBalance" + counterId).val(obj.counter);
				 $("#txtBalance" + counterId).focus();

			 },
			 error: function (xhr, ajaxOptions, thrownError) {

				 var obj = jQuery.parseJSON(xhr.responseText);
				 alert(obj.Message);
			 },
			 complete: function () {




			 }




		 });


        

                }
                function GetBankForVoucherNo() {


					$.ajax({
						type: "POST",
						data: '{ "pid": "' + $("#hdnGrnNo").val() + '"}',
						url: "managevoucherentry.aspx/GetBank",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);

							var isexist = obj.rtnval;
							
							$("#ddlBank").val(obj.rtnval);
							
						
							
						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {


							
						
						}




					});




				}

     function addTRUpdate() {
         $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

         $("#tbProducts").append("<tr id='loading'><td colspan='100%' style='text-align:center'><img src='images/ajax-loader.gif' alt='loading please wait...'/></td></tr>");

         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
		
         $.ajax({
             type: "POST",
			 data: '{ "pid": "' + $("#hdnGrnNo").val() + '","VType": "' + $("#hdnVType").val() + '"}',
			 url: "managevoucherentry.aspx/BindVoucherDetail",
             contentType: "application/json",
             dataType: "json",
             success: function (msg) {

                 var obj = jQuery.parseJSON(msg.d);
                 var tr = "";
      
                 $("#tbProducts").append(obj.ServiceData);
                 $("#hdnCounter").val(obj.Counter);
               
                // for (var i = 0; i < 20 - Number(obj.Counter) ; i++) {
                     addTR();
                // }
             },
             error: function (xhr, ajaxOptions, thrownError) {

                 var obj = jQuery.parseJSON(xhr.responseText);
                 alert(obj.Message);
             },
             complete: function () {

                 GetBankForVoucherNo();
                 $('#tbProducts tr#loading').remove();
                 $.uiUnlock();
             }




         });





     }


     $(document).keydown(function (e) {
         if (e.keyCode == 27) return false;
     });
				$("#tbProducts").keydown(function (e) {

					var cellindex = $(this).parents('td').index();

					if (e.which == 40) {
						$(e.target).closest('tr').nextAll('tr').find('td').eq(cellindex).find(':text').focus();
					}
                    if (e.which == 38) {
                        
						$(e.target).closest('tr').prevAll('tr').first().find('td').eq(cellindex).find(':text').focus();
					}

				});		
				//$(document).keydown(function (e) {
    //                if (e.keyCode == 38) {
    //                    alert("Hello");
    //                   // $("#txtServiceId" + counterId-1).focus();
    //                }
				//});
     function addTR() {
         var counterId = Number($("#hdnCounter").val()) + 1;
         $("#hdnCounter").val(counterId);
         var tr = "";
         tr = "<tr><td><input type='text' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' /></td>" +
             "<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +

             "<td><input type='text' id='txtBalance" + counterId + "'  counter='" + counterId + "'  class='form-control input-small validate' name='txtBalance'  value = '0'/></td>" +
             "<td class ='txtpay'><input type='text' id='txtPayment" + counterId + "' counter='" + counterId + "'  class='form-control input-small  validate' name='txtPayment'  value = '0'/></td>" +
             "<td class='txtrec'><input type='text' id='txtReceipt" + counterId + "'   counter='" + counterId + "'  class='form-control input-small  validate' name='txtReceipt'   value = '0.0'/></td>" +

             "<td class='txtchq'><input type='text' id='txtChqNo" + counterId + "' counter='" + counterId + "'  class='form-control input-small' name='txtChqNo'  value = ' '/></td>" +
			 "<td class='txtdeb'><input type='text' id='txtDebit" + counterId + "'   counter='" + counterId + "' class='form-control input-small  validate float' name='txtDebit'  value = '0.0'/></td>" +

			 "<td class='txtcre'><input type='text' id='txtCredit" + counterId + "'   counter='" + counterId + "'  class='form-control input-small' name='txtCredit'  value='0.0' /></td>" +
             "<td><input type='text' id='txtNarr" + counterId + "' counter='" + counterId + "'  class='form-control input-small' name='txtNarr'  value = ' '/></td>" +
			 "<td style='display:none'><input type='text'  id='txtCCOde" + counterId + "'   counter='" + counterId + "' class='form-control input-small' name='txtCCOde'  value=' ' /></td>";

            


		 tr = tr + "<td style='display:none'><div id='btnAddRow" + counterId + "'  onclick='javascript:addTR();' style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-plus' style:'color:white'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove" + counterId + "' onclick='javascript:DeleteRow(" + counterId + ");'  style='cursor:pointer'  counter='" + counterId + "'><i class='fa fa-remove'></i></div> </td></tr>";

         $("#tbProducts").append(tr);
        
		 $("#txtServiceId" + counterId + "").focus();
		 if ($('#rdbbank').is(":checked")) {
			 $(".chq").show();
			 $(".txtchq").show();
			 $(".deb").hide();
			 $(".txtdeb").hide();
			 $(".cre").hide();
			 $(".txtcre").hide();
			 $(".pay").show();
			 $(".rec").show();
			 $(".txtpay").show();
			 $(".txtrec").show();
         }
         if ($('#rdbJournal').is(":checked")) {
             $(".chq").hide();
             $(".txtchq").hide();
             $(".deb").show();
             $(".txtdeb").show();
             $(".cre").show();
             $(".txtcre").show();
             $(".pay").hide();
             $(".rec").hide();
             $(".txtpay").hide();
             $(".txtrec").hide();
         }

         if ($('#rdbcash').is(":checked")) {
             $(".chq").hide();
             $(".txtchq").hide();
             $(".deb").hide();
             $(".txtdeb").hide();
             $(".cre").hide();
             $(".txtcre").hide();
             $(".pay").show();
             $(".rec").show();
             $(".txtpay").show();
             $(".txtrec").show();
         }


     }

     function SaveRecords() {



         
         var VouchNo = $("#hdnGrnNo").val();
         var TrDate = $("#txtTrDate").val();
        

         var BankId = 0;
		 if ($('#rdbbank').is(":checked")) {

			 BankId = $("#ddlBank").val();
			 if (BankId == "0") {
				 alert("Choose Bank");
				 $("#ddlBank").focus();
				 return;

			 }

         }
		 var Branchid = $("#ddlBranch").val();

         var VoucherType = "";
         if ($('#rdbbank').is(":checked")) {
             VoucherType = "BV";
         }
		 if ($('#rdbcash').is(":checked")) {
			 VoucherType = "CV";
         }
		 if ($('#rdbJournal').is(":checked")) {
			 VoucherType = "JV";
		 }
		
       
         Code = [];
         Name = [];
         Payment = [];
         Receipt = [];
         Cheque = [];
         Debiit = [];
         Credit = [];
         Narr = [];
         CCODE = [];
         
         $("input[name='txtServiceId']").each(
             function (x) {

                 var counterId = $(this).attr("counter");
                 if ($('#ddlProducts' + counterId).val() != "") {


                     Code[x] = $('#ddlProducts' + counterId).val();
                     Name[x] = $("#ddlProducts" + counterId + ' option:selected').text();					
                    Payment[x] = $("#txtPayment" + counterId).val();
                    Receipt[x] = $("#txtReceipt" + counterId).val();
                    Cheque[x] = $("#txtChqNo" + counterId).val();
                    Debiit[x] = $("#txtDebit" + counterId).val();
                    Credit[x] = $("#txtCredit" + counterId).val();
                    Narr[x] = $("#txtNarr" + counterId).val();
					CCODE[x] = $("#txtCCOde" + counterId).val();
                 }
				 
             });

         var multisingle = "";
         if ($('#rdbSingle').is(":checked")) {
             multisingle = "Single";
         }
		 if ($('#rdbMultiple').is(":checked")) {
			 multisingle = "Multi";
		 }
         $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
             $.ajax({
                 type: "POST",
				 data: '{"VouchNo": "' + VouchNo + '","TrDate": "' + TrDate + '","BankId": "' + BankId + '","VoucherType": "' + VoucherType + '","codeArr": "' + Code + '","NameArr": "' + Name + '","PaymentArr": "' + Payment + '","ReceiptArr": "' + Receipt + '","ChequeArr": "' + Cheque + '","DebitArr": "' + Debiit + '","CreditArr": "' + Credit + '","NarrArr": "' + Narr + '","CCODEArr": "' + CCODE + '","BranchId": "' + Branchid + '","multisingle": "' + multisingle + '"}',

                 url: "managevoucherentry.aspx/Insert",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);

                     if (obj.Status == -11) {
                         alert("You don't have permission to perform this action.");
                         return;
                     }
                     if (obj.Status == "0") {
                         alert("Insertion Failed. Please try again later.");
                         return;
                     }
                   

                     alert("Voucher added Successfully");
                     $("#addDialog").dialog("close");

                   

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {


                     $.uiUnlock();
                     window.location = "managevoucherentry.aspx";
                 }


             });


         
       



     }
				function BasicSettings() {


					$.ajax({
						type: "POST",
						data: '{ }',
						url: "managepurchase.aspx/FillBasicSettings",
						contentType: "application/json",
						dataType: "json",
						success: function (msg) {


							var obj = jQuery.parseJSON(msg.d);

							

                            var chkentry = obj.setttingData.BnkEntry;
                           

						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {


						}

					});

				}


     $(document).ready(
    function () {



        $('#txtDateFrom').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });


        $('#txtDateTo').daterangepicker({
            singleDatePicker: true,
            calender_style: "picker_1"
        }, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $("#txtTrDate").val($("#<%=hdntodaydate.ClientID %>").val());

                          
        $("#txtDateFrom").val($("#<%=hdntodaydate.ClientID %>").val());
        $("#txtDateTo").val($("#<%=hdntodaydate.ClientID %>").val());


        BasicSettings();
        BindGrid();

        $("#btnGo").click(
                   function () {

                       BindGrid();

                   }
                    );




        $("#btnSubmit").click(
            function () {
                if ($('#rdbJournal').is(":checked")) {
                    var debit = 0;
                    var credit = 0;

                    $("input[name='txtServiceId']").each(
                        function () {

                            var counterId = $(this).attr("counter");

                            if ($("#ddlProducts" + counterId).val() != "") {

                                debit = Number(debit) + Number($("#txtDebit" + counterId).val());
                                credit = Number(credit) + Number($("#txtCredit" + counterId).val());
                            }
                        });
                   var ttlamt = (Number(debit) - Number(credit));
                  
                   
                    if (ttlamt == 0) {
                        SaveRecords();
                    }
                    else {
                        alert("Debit and Credit Doesn't match");
                        return;
                    }
                   
                }
                else {

                    SaveRecords();
				}
           

        });


        $("#btnprint").click(
         function () {

             var myGrid = $('#jQGridDemo'),
             selRowId = myGrid.jqGrid('getGridParam', 'selrow'),
             celValue = myGrid.jqGrid('getCell', selRowId, 'GrnNo');

             Printt(celValue);
         }
         );

			
        $("#chkDis1InRs").change(
          function () {

              if ($("#txtNetCost").val() <= 0) {


                  return;
              }

              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                   
                }
            });
          }
             );
             $("#rbVAT").change(
                 function () {
                     if ($('#rbVAT').is(":checked")) {
                         $("#lblgsttxt").text("Add VAT");
                         $("#lblgsttr").text("VAT");
						 $("#lblgstper").text("VAT %");
                         $("#lblgstdeno").text("VAT Denomination");
                         $("#lblgstAmt").text("VATAmt");
						 $("#lblgst").text("VAT");
						 
						 
                     }
                    
					 
                 });

			 $("#rbGST").change(
				 function () {
					 if ($('#rbGST').is(":checked")) {
                         $("#lblgsttxt").text("Add GST/IGST");
                         $("#lblgsttr").text("GST");
                         $("#lblgstper").text("GST %");
                         $("#lblgstdeno").text("GST Denomination");
                         $("#lblgstAmt").text("GSTAmt");
						 $("#lblgst").text("GST");
						 
					 }
					 

				 });
       
        
      

        $("#rbLocal").change(
          function () {
              CommonCalculations();
              $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
                if ($("#ddlProducts" + counterId).val() != "") {


                   
                }
            });
              $("#trGST").show();
              $("#trIGST").hide();
             // $("#trSGST").hide();
          });
      
	

        $("#chkDis2InRs").change(
                function () {

                    if ($('#chkDis2InRs').is(":checked")) {
                        $("#thDis2").html("Dis2र");
                        CommonCalculations();
                        $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
              
            });
                    }
                    else {
                        $("#thDis2").html("Dis2%");
                        CommonCalculations();
                        $("input[name='txtServiceId']").each(
            function () {

                var counterId = $(this).attr("counter");
               
            });
                    }
                }
                );




        $("#txtDisAmount").keyup(function () {

            $("#txtDisPercent").val($("#txtDisAmount").val() * 100 / $("#lblTotalAmount").html());
            CommonCalculations();

        });


        $("#txtServiceId").change(function () {

            var sid = $("#txtServiceId").val();


            $.ajax({
                type: "POST",
                data: '{ "Id": "' + sid + '"}',

                url: "managepurchase.aspx/ValidateServiceID",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    if (msg.d == "n") {

                        $("#lblMsg").css({ "color": "red" });
                        $("#lblMsg").html("Sorry Product Id already Exists");
                        $("#hdnStatus").val("0");


                    }
                    else {
                        $("#lblMsg").css({ "color": "green" });

                        $("#lblMsg").html("Product Id Accepted!!");
                        $("#hdnStatus").val("1");

                    }
                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }


            });



        }
        );



        function BindContols() {



        }









        //----------------------btnADD------------------------

        $("#ddlstate").change(
        function () {

            BindCities($("#ddlstate").val());

        }

        );


        function BindCities(sid) {
            $("#ddlcities").html('');
            var stateId = sid;
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');
            $.ajax({
                type: "POST",
                data: '{ "stateId": "' + stateId + '"}',
                url: "ManageSupplier.aspx/BindCities",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);


                    $("#ddlcities").html(obj.CitiesOptions);

                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function (msg) {

                    $.uiUnlock();

                    $('#ddlcities option[value=' + $('#jQGridDemo').jqGrid('getCell', CurrentRowId, 'City') + ']').prop('selected', 'selected');

                }


            });
        }





        function InsertUpdateSupplier() {


            var suppliername = $("#txtSupplier").val();
            if (suppliername.trim() == "") {
                alert("Please enter suppliername");
                return;
            }

            var Address = $("#txtAddress").val();
            if (Address.trim() == "") {
                alert("Please enter Address");
                return;
            }

            var Address1 = $("#txtAddress1").val();
            if (Address1.trim() == "") {
                alert("Please enter Address1");
                return;
            }
            var City = $("#ddlcities").val();
            if (City == null) {
                alert("Please enter City");
                return;
            }

            var State = $("#ddlstate").val();
            if (State == null) {
                alert("Please enter State");
                return;
            }

            var ContactNumber = $("#txtContactNumber").val();
            if (ContactNumber.trim() == "") {
                alert("Please enter ContactNumber");
                return;
            }
            var ContactPerson = $("#txtContactPerson").val();
            if (ContactPerson.trim() == "") {
                alert("Please enter ContactPerson");
                return;
            }

            var CSTNo = $("#txtCSTNo").val();
            if (CSTNo == null) {
                alert("Please enter CSTNo");
                return;
            }
            var TINNo = $("#txtTINNo").val();
            if (TINNo == null) {
                alert("Please enter TINNo");
                return;
            }

            var IsActive = false;
            if ($('#chkIsActive').is(":checked")) {
                IsActive = true;
            }

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            $.ajax({
                type: "POST",
                data: '{"Supplier": "' + suppliername + '","Address": "' + Address + '","Address1": "' + Address1 + '","City": "' + City + '","State": "' + State + '","ContactNumber": "' + ContactNumber + '","Contactperson": "' + ContactPerson + '","CSTNo": "' + CSTNo + '","TINNo": "' + TINNo + '","isActive": "' + IsActive + '"}',
                url: "managepurchase.aspx/InsertSupplier",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.Status == 0) {

                        alert("Insertion Failed.Supplier with duplicate name already exists.");
                        return;

                    }

                    var option = "<option value='" + obj.Supplier.SupplierId + "'>" + obj.Supplier.Supplier + "</option>";
                    $("#ddlSupplier").append(option);

                    $("#ddlstate").html('');
                    $("#ddlcities").html('');
                    $("#txtSupplier").val("");
                    $("#txtAddress").val("");
                    $("#txtAddress1").val("");
                    $("#txtContactPerson").val("");
                    $("#txtContactNumber").val("");
                    $("#txtCSTNo").val("");
                    $("#txtTINNo").val("");



                    alert("Supplier added successfully.");

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }



            });


        }




        $("#dvSupplier").click(
        function () {

            $('#radhika').dialog(
        {
            autoOpen: false,

            width: 600,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateSupplier();
                    $(this).dialog("close");


                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }



        });

            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


            $.ajax({
                type: "POST",
                data: '{}',
                url: "ManageSupplier.aspx/BindStates",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    $("#ddlstate").html(obj.StateOptions);


                }, error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }


            });












            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#radhika');
            dialogDiv.dialog("option", "position", [100, 200]);
            dialogDiv.dialog('open');
            return false;


        }
        );



        $("#dvGodown").click(
        function () {

            $('#AddGodown').dialog(
        {
            autoOpen: false,

            width: 300,
            resizable: false,
            modal: true,
            buttons: {

                "Add": function () {
                    InsertUpdateGodown();
                    $(this).dialog("close");



                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            }
        });



            function InsertUpdateGodown() {


                var Title = $("#txtTitle").val();
                if (Title.trim() == "") {
                    alert("Please enter Title");
                    return;
                }

                $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');


                $.ajax({
                    type: "POST",
                    data: '{ "title": "' + Title + '"}',
                    url: "managepurchase.aspx/InsertGodown",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        if (obj.Status == 0) {

                            alert("Insertion Failed.Godown with duplicate name already exists.");
                            return;

                        }
                        jQuery("#jQGridDemo").jqGrid('addRowData', 0, obj.Godown, "last");

                        var option = "<option value='" + obj.Godown.GodownId + "'>" + obj.Godown.Title + "</option>";
                        $("#ddlGodown").append(option);

                        $("#txtTitle").val("");
                        alert("Godown added successfully.");

                    }
                     , error: function (xhr, ajaxOptions, thrownError) {

                         var obj = jQuery.parseJSON(xhr.responseText);
                         alert(obj.Message);
                     },
                    complete: function () {


                        $.uiUnlock();
                    }


                });

            }


            //change the title of the dialgo
            linkObj = $(this);
            var dialogDiv = $('#AddGodown');
            dialogDiv.dialog("option", "position", [400, 200]);
            dialogDiv.dialog('open');
            return false;


        }
        );





        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {

                if (arrRole[i] == "1") {

                    $("#btnAdd").click(function () {
                        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       
                        $("#txtGrnNo").focus();
                        $("#status").val("I");
                        $("#hdnGrnNo").val("0");

                        var IsFirstTime = $("#hdnFirstTime").val();

                        //if (IsFirstTime == "1") {
                            BindContols();
                          //  for (var i = 0; i < 15; i++) {
                                addTR();
                         
                             
                           // }
                      
                            //BindProducts();
                   

                            $("#hdnFirstTime").val("0");
                        //}
                        //else {

                        //    ResetControls();
                        //    Settings();
                        //}


                        $('#addDialog').dialog({
                            autoOpen: false,
                            closeOnEscape: false,
                            height: 600,
                            width: 1115,
                            resizable: false,
                            modal: false,
                        });
                        $("#addDialog").parent().addClass('add-pop');

                       
                        //change the title of the dialgo
                        linkObj = $(this);
                        var dialogDiv = $('#addDialog');
                        dialogDiv.dialog("option", "position", [70, 50]);
                        dialogDiv.dialog('open');
                        return false;

                    });


                }
                else if (arrRole[i] == "3") {

                    $("#btnEdit").click(
       function () {

           $("#status").val("U");
           addTRUpdate();
           $("#hdnFirstTime").val("0");

           var pid = $("#hdnGrnNo").val();

           if (pid == "0") {
               alert("No Row Selected");
               return;
           }

           //$("#innerSave").show();

           $('#addDialog').dialog(
       {
           autoOpen: false,
           height: 600,
           width: 1115,
           resizable: false,
           modal: false,

       });


           //change the title of the dialgo
           linkObj = $(this);
           var dialogDiv = $('#addDialog');
           dialogDiv.dialog("option", "position", [70, 50]);
           var viewUrl = "customeradd.aspx";
           dialogDiv.dialog('open');
           return false;


       }
       );

                }


            }

        }
			 $("#ddlBank").change(
                 function () {
                     var Bank = $("#ddlBank").val();
                    
					 $.ajax({
                         type: "POST",
                         data: '{ "CCODE": "' + Bank + '"}',
						 url: "managevoucherentry.aspx/GetBalance",
						 contentType: "application/json",
						 asyn: false,
						 dataType: "json",
						 success: function (msg) {

                             var obj = jQuery.parseJSON(msg.d);
                             
                             $("#lblbankbal").html(obj.counter);
							 
							
						 },
						 error: function (xhr, ajaxOptions, thrownError) {

							 var obj = jQuery.parseJSON(xhr.responseText);
							 alert(obj.Message);
						 },
						 complete: function () {
							



						 }




					 });




				 });

             $("#rdbcash").change(
                 function () {
					 if ($('#rdbcash').is(":checked")) {

                         $("#ddlBank").hide();
                         $("#tdbnk").hide();
                         $("#tdcsh").show();
                         $("#lblcashbal").show();
						 $("#tdbnkbal").hide();
                         $("#lblbankbal").hide();
                         $("#bnk").hide();
                         getcashbal();
                     }
                    

                 });

			 $("#rdbJournal").change(
				 function () {
					 if ($('#rdbJournal').is(":checked")) {

						 $("#ddlBank").hide();
                         $("#tdbnk").hide();
						 $("#tdcsh").hide();
                         $("#lblcashbal").hide();
						 $("#tdbnkbal").hide();
                         $("#lblbankbal").hide();
                         $("#lblbankbal").html('');
                         $("#ddlBank").val(0);
						 $("#bnk").hide();
					 }

				 });

             $("#rdbbank").change(
                 function () {
                     if ($('#rdbbank').is(":checked")) {

                         $("#ddlBank").show();
                         $("#tdbnk").show();
						
                         $("#tdcsh").hide();
                         $("#lblcashbal").hide();
                         $("#tdbnkbal").show();
                         $("#lblbankbal").show();
						 $("#lblbankbal").html('');
                         $("#ddlBank").val(0);
						 $("#bnk").show();
                     }
                    

                 });

        $("#btncncl").click(
        function () {


            //$("#addDialog").dialog("close");
            window.location = "managevoucherentry.aspx";
        });



         });


                function getcashbal() {
					$.ajax({
						type: "POST",
						data: '{ "CCODE": 0}',
						url: "managevoucherentry.aspx/GetBalance",
						contentType: "application/json",
						asyn: false,
						dataType: "json",
						success: function (msg) {

							var obj = jQuery.parseJSON(msg.d);

							$("#lblcashbal").html(obj.counter);


						},
						error: function (xhr, ajaxOptions, thrownError) {

							var obj = jQuery.parseJSON(xhr.responseText);
							alert(obj.Message);
						},
						complete: function () {




						}




					});

                }


                function chkbillno() {

                    if ($("#hdnGrnNo").val() == "0") {


                        $.ajax({
                            type: "POST",
                            data: '{ "billno": "' +  $("#txtBillNo").val() + '","supplier": "' + $("#ddlSupplier").val() + '"}',
                            url: "managepurchase.aspx/chkbillno",
                            contentType: "application/json",
                            asyn: false,
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);

                                if (obj == 1) {
                                    alert("Bill No already Exists!");

                                    $("#txtBillNo").val("");
                                    $("#txtBillNo").focus();

                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function () {
                                $("#closetast3").focus();



                            }




                        });
                    }
                }

                $("#txtBillNo").blur(function () {

                    chkbillno();
 
                });

                $("#ddlSupplier").change(function () {


                    chkbillno();



                });
    function BindProducts() {



        PageMethods.BindProducts(OnSuccess, OnFailure);

        //        $.ajax({
        //            type: "POST",
        //            data: '{}',
        //            url: "managepurchase.aspx/BindProducts",
        //            contentType: "application/json",
        //            dataType: "json",
        //            success: function (msg) {

        //                var obj = jQuery.parseJSON(msg.d);

        //                $("#hdnProducts").html("");

        //                $("#hdnProducts").append("<option></option>");
        //                $("#hdnProducts").append(obj.ProductOptions);





        //            },
        //            complete: function () {

        //           ResetControls();
        //            },
        //            error: function (xhr, ajaxOptions, thrownError) {

        //                var obj = jQuery.parseJSON(xhr.responseText);
        //                alert(obj.Message);
        //            }


        //        });
    }


    function ResetControls() {

        validateForm("detach");
        $('#tbProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
       
        $("#hdnCounter").val(1);

		tr = "<tr><td><input type='text' autocomplete='off' onkeyup='javascript:CodeSearch(" + counterId + ",event);' id='txtServiceId" + counterId + "'  counter='" + counterId + "' class='form-control input-small' name='txtServiceId' /></td>" +
			"<td><select id='ddlProducts" + counterId + "' onchange='javascript:ServiceChange(" + counterId + ");' name='ddlProducts' >" + $("#<%=hdnProducts.ClientID%>").html() + "</select></td>" +

			"<td><input type='text' id='txtBalance" + counterId + "'  counter='" + counterId + "'  class='form-control input-small validate' name='txtBalance'  value = '0'/></td>" +
			"<td class='txtpay'><input type='text' id='txtPayment" + counterId + "'  counter='" + counterId + "'  class='form-control input-small  validate' name='txtPayment'  value = '0'/></td>" +
			"<td class='txtrec'><input type='text' id='txtReceipt" + counterId + "'   counter='" + counterId + "'  class='form-control input-small  validate' name='txtReceipt'   value = '0.0'/></td>" +

			"<td class='txtchq'><input type='text' id='txtChqNo" + counterId + "' counter='" + counterId + "'  class='form-control input-small' name='txtChqNo'  value = ' '/></td>" +
			"<td class='txtdeb'><input type='text' id='txtDebit" + counterId + "'   onkeyup='javascript:QtyChange(" + counterId + ");' counter='" + counterId + "' class='form-control input-small  validate float' name='txtDebit'  value = '0.0'/></td>" +

			"<td class='txtcre'><input type='text' id='txtCredit" + counterId + "' onkeyup='javascript:QtyChange(" + counterId + ");'  counter='" + counterId + "'  class='form-control input-small' name='txtCredit'  value='0.0' /></td>" +
			"<td><input type='text' id='txtNarr" + counterId + "' counter='" + counterId + "'  class='form-control input-small' name='txtNarr'  value = ' '/></td>" +
			"<td><input type='text' style='display:none' id='txtCCOde" + counterId + "'   counter='" + counterId + "'   value=' ' /></td>";



		tr = tr + "<td style='display:none' ><div id='btnAddRow1'  onclick='javascript:addTR();' style='cursor:pointer'  counter='1'><i class='fa fa-plus'></i></div> </td>";

         tr = tr + "<td><div id='btnRemove1' onclick='javascript:DeleteRow(1);'  style='cursor:pointer'  counter='1'><i class='fa fa-remove'></i></div> </td></tr>";




         $("#tbProducts").append(tr);




        
         $("#txtTrDate").val($("#<%=hdntodaydate.ClientID %>").val());
        
        
        
        
         BindGrid();



     }


     function OnSuccess(data) {
         if (data) {


             // $("#hdnProducts").html("");
             // $("#hdnProducts").append("<option></option>");
             // $("#hdnProducts").append(data);
             ResetControls();
         }
     }
     function OnFailure(error) {
         alert(error);
     }
            </script>
        </div>
        <!-- /page content -->

        </div>
        <!-- footer content -->
        <footer>                 
                   <uc1:ucfooter ID="ucfooter1" runat="server" />                
                </footer>
        <!-- /footer content -->

        <script type="text/javascript">
            function BindGrid() {
                var DateFrom = $("#txtDateFrom").val();
                var DateTo = $("#txtDateTo").val();
                jQuery("#jQGridDemo").GridUnload();

                jQuery("#jQGridDemo").jqGrid({
                    url: 'handlers/ManageVoucher.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
                    ajaxGridOptions: { contentType: "application/json" },
                    datatype: "json",
                    colNames: ['VoucherNo', 'Date', 'CCODE', 'CNAME', 'Debit', 'Credit','VType'],
                    colModel: [

						{ name: 'VOUCH_NO', key: true, index: 'VOUCH_NO', width: 100, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: false },
        //   		           
						{ name: 'strTRDate', index: 'strTRDate', width: 100, stype: 'text', sortable: true, hidden: false },
						{ name: 'CCODE', key: true, index: 'CCODE', width: 100, searchoptions: { sopt: ['eq'] }, stype: 'text', sortable: true, hidden: false },
						{ name: 'CNAME', index: 'CNAME', width: 100, stype: 'text', sortable: true, hidden: false },
						
						{ name: 'Debit', index: 'Debit', width: 100, stype: 'text', sortable: true, hidden: false },
						{ name: 'Credit', index: 'Credit', width: 100, stype: 'text', sortable: true, hidden: false },
						{ name: 'VTYPE', index: 'VTYPE', width: 100, stype: 'text', sortable: true, hidden: false },

                    ],
                    rowNum: 10,
                    mtype: 'GET',
                    loadonce: true,
                    toppager: true,
                    rowList: [10, 20, 30],
                    pager: '#jQGridDemoPager',
                    sortname: 'Vouch_No',
                    viewrecords: true,
                    height: "100%",
                    width: "600px",
                    sortorder: 'desc',
                    caption: "Voucher List",
                    editurl: 'handlers/ManageVoucher.ashx',

                    toolbar: [true, "top"],
                    ignoreCase: true,
                });
                var $grid = $("#jQGridDemo");
                // fill top toolbar
                $('#t_' + $.jgrid.jqID($grid[0].id))
                    .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                $("#globalSearchText").keypress(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    if (key === $.ui.keyCode.ENTER) { // 13
                        $("#globalSearch").click();
                    }
                });
                $("#globalSearch").button({
                    icons: { primary: "ui-icon-search" },
                    text: false
                }).click(function () {
                    var postData = $grid.jqGrid("getGridParam", "postData"),
                        colModel = $grid.jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#globalSearchText").val(),
                        l = colModel.length,
                        i,
                        cm;
                    for (i = 0; i < l; i++) {
                        cm = colModel[i];
                        if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                            rules.push({
                                field: cm.name,
                                op: "cn",
                                data: searchText
                            });
                        }
                    }
                    postData.filters = JSON.stringify({
                        groupOp: "OR",
                        rules: rules
                    });
                    $grid.jqGrid("setGridParam", { search: true });
                    $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                    return false;
                });



                $("#jQGridDemo").jqGrid('setGridParam',
               {
                   onSelectRow: function (rowid, iRow, iCol, e) {

					   
					   $("#hdnGrnNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'VOUCH_NO'));

					   $("#txtVouchNo").val($('#jQGridDemo').jqGrid('getCell', rowid, 'VOUCH_NO'));
                       $("#txtTrDate").val($('#jQGridDemo').jqGrid('getCell', rowid, 'strTRDate'));
					   $("#hdnVType").val($('#jQGridDemo').jqGrid('getCell', rowid, 'VTYPE'));
					   if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VTYPE') == "CV") {

						   $('#rdbcash').prop('checked', true);
						   $("#ddlBank").hide();
						   $("#tdbnk").hide();
						   $("#tdcsh").show();
						   $("#lblcashbal").show();
						   $("#tdbnkbal").hide();
						   $("#lblbankbal").hide();
						   $("#bnk").hide();
                           getcashbal();
						   $(".chq").hide();
						   $(".txtchq").hide();
						   $(".deb").hide();
						   $(".txtdeb").hide();
						   $(".cre").hide();
						   $(".txtcre").hide();
						   $(".pay").show();
						   $(".rec").show();
						   $(".txtpay").show();
						   $(".txtrec").show();

					   }
					   if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VTYPE') == "BV") {
						  
                           $('#rdbbank').prop('checked', true);
						   $("#ddlBank").show();
						   $("#tdbnk").show();

						   $("#tdcsh").hide();
						   $("#lblcashbal").hide();
						   $("#tdbnkbal").show();
						   $("#lblbankbal").show();
						   $("#lblbankbal").html('');
						   $("#ddlBank").val(0);
                           $("#bnk").show();

						   $(".chq").show();
						   $(".txtchq").show();
						   $(".deb").hide();
						   $(".txtdeb").hide();
						   $(".cre").hide();
						   $(".txtcre").hide();
						   $(".pay").show();
						   $(".rec").show();
						   $(".txtpay").show();
						   $(".txtrec").show();


                       }
					   if ($('#jQGridDemo').jqGrid('getCell', rowid, 'VTYPE') == "JV") {
						   
                           $('#rdbJournal').prop('checked', true);
						   $("#ddlBank").hide();
						   $("#tdbnk").hide();
						   $("#tdcsh").hide();
						   $("#lblcashbal").hide();
						   $("#tdbnkbal").hide();
						   $("#lblbankbal").hide();
						   $("#lblbankbal").html('');
						   $("#ddlBank").val(0);
                           $("#bnk").hide();

						   $(".chq").hide();
						   $(".txtchq").hide();
						   $(".deb").show();
						   $(".txtdeb").show();
						   $(".cre").show();
						   $(".txtcre").show();
						   $(".pay").hide();
						   $(".rec").hide();
						   $(".txtpay").hide();
						   $(".txtrec").hide();


					   }
                   }
               });





                $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                           {
                               refresh: false,
                               edit: false,
                               add: false,
                               del: false,
                               search: true,
                               searchtext: "Search",
                               addtext: "Add",
                           },

                           {//SEARCH
                               closeOnEscape: true

                           });

                var DataGrid = jQuery('#jQGridDemo');
                DataGrid.jqGrid('setGridWidth', '700');

            }


        </script>
        <script>
			$(function () {
				
			});
			$(document).ready(function () {
				
                getcashbal();
				$("#rdbcash").click(function () {
					$(".chq").hide();
                    $(".txtchq").hide();
					$(".deb").hide();
                    $(".txtdeb").hide();
					$(".cre").hide();
                    $(".txtcre").hide();
					$(".pay").show();
					$(".rec").show();
					$(".txtpay").show();
					$(".txtrec").show();
				});
				$("#rdbbank").click(function () {
					$(".chq").show();
                    $(".txtchq").show();
					$(".deb").hide();
					$(".txtdeb").hide();
					$(".cre").hide();
                    $(".txtcre").hide();
                    $(".pay").show();
                    $(".rec").show();
                    $(".txtpay").show();
                    $(".txtrec").show();
				});
				$("#rdbJournal").click(function () {
					$(".chq").hide();
                    $(".txtchq").hide();
                    $(".deb").show();
                    $(".txtdeb").show();
                    $(".cre").show();
                    $(".txtcre").show();
                    $(".pay").hide();
                    $(".rec").hide();
                    $(".txtpay").hide();
                    $(".txtrec").hide();
				});




			});
        </script>
    </form>
</asp:Content>
