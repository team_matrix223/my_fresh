﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;

public partial class SearchPlugin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    [WebMethod]
    public static string KeywordSearch(string Keyword)
    {
        string Branch = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string Type = "";
        string Data = new ProductBLL().KeywordSearch(Keyword.Trim(), Type, Branch);

        return Data;
    }
}