﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;

public partial class StockAdjustment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hdntodaydate.Value = DateTime.Now.ToShortDateString();
        if (!IsPostBack)
        {

            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }

        }
    }


    [WebMethod]
    public static string ChkItemCode(string ItemCode)
    {

        string rtnval = "1";
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Select 1 from Packing_Belongs where Item_Name LIKE '%' + @SearchText + '%' order by Item_Name", con);
            cmd.CommandType = CommandType.Text;
            cmd.Parameters.AddWithValue("@SearchText", ItemCode);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);

            if (dt.Rows.Count >= 1)
            {
                rtnval = "1";
            }
            else
            {

                rtnval = "0";
            }

            con.Close();

        }

        var JsonData = new
        {
            rtnval = rtnval
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }





    [WebMethod]
    public static string GetByItemCode(string Keyword, string Type, string AccountType, int GroupId, string ItemType, int Godown)
    {
        Product objProduct = new Product() { Item_Code = Keyword };


        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);


        new ProductBLL().GetByItemCodeforStock(objProduct, Type, AccountType, GroupId, ItemType, Godown, Branch);

        var JsonData = new
        {
            productData = objProduct
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindDealers(string Type)
    {
       
        List<Godowns> Godown = new List<Godowns>();
        string groupstock = new PhysicalStockBLL().GetOptions(Type);
      
        Godown = new GodownsBLL().GetAll();

        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            
            GodownOptions = Godown,
            GroupStock = groupstock

        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string InsertUpdate(int RefNo, DateTime RefDate,int GodownId, StockAdj[] objStockMaster)
    {
        StockAdj objstock = new StockAdj()
        {

            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value),
          UserNo= Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value),
          Ref_No = RefNo,
          Ref_Date = RefDate,
          Godown_ID = GodownId,

        };

      
        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Code");
        dt.Columns.Add("IName");
        dt.Columns.Add("MRP");
        dt.Columns.Add("ActualStock");
        dt.Columns.Add("StockAdjust");
        dt.Columns.Add("StockAdjusted");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
       
        DataRow dr;



        foreach (var item in objStockMaster)
        {
            dr = dt.NewRow();
            dr["Code"] = item.Code;
            dr["IName"] = item.IName;
            dr["MRP"] = item.MRP;
            dr["ActualStock"] = item.ActualStock;
            dr["StockAdjust"] = item.StockAdjust;
            dr["StockAdjusted"] = item.StockAdjusted;
            dr["Rate"] = item.Rate;
            dr["Amount"] = item.Amount;
           
            dt.Rows.Add(dr);


        }

        int Status = new StockAdjustmentBLL().InsertUpdate(objstock, dt);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }
}