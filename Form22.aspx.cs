﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Form22 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            hdnDate.Value = DateTime.Now.ToShortDateString();
            BindBranches();
            ddcustomertype();
        }
    }



    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        ddlPos.DataSource = dt;
        ddlPos.DataTextField = "title";
        ddlPos.DataValueField = "posid";
        ddlPos.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose POS--";
        li1.Value = "0";
        ddlPos.Items.Insert(0, li1);
    }



    [WebMethod]
    public static string InsertUpdate(DeliveryDetail[] objDeliveryDetails, string ItemType,int Branch,int Pos)
    {
       
        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Scheme");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1P");
        dt.Columns.Add("Dis2P");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("Dis3P");
        dt.Columns.Add("Godown_ID");
        dt.Columns.Add("RowNum");
        dt.Columns.Add("Excise_Duty");
        dt.Columns.Add("Excise_Amt");
        DataRow dr;


        Int32 RowNumm = 1;
        foreach (var item in objDeliveryDetails)
        {
            dr = dt.NewRow();
            dr["Item_Code"] = item.Item_Code;
            dr["Scheme"] = 0;
            dr["Qty"] = 0;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = 0;
            dr["Dis1P"] = 0;
            dr["Dis2P"] = 0;
            dr["TaxP"] = 0;
            dr["Dis3P"] = 0;
            dr["Godown_ID"] = 0;
            dr["RowNum"] = RowNumm;
            dr["Excise_Duty"] = 0;
            dr["Excise_Amt"] = 0;
            dt.Rows.Add(dr);
            RowNumm = RowNumm + 1;


        }

        int Status = new SaleBLL().InsertnewDel(Branch, dt, ItemType,Pos);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertUpdateDel(DeliveryDetail[] objDeliveryDetails, string ItemType, int Branch, int Pos)
    {

        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();
        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Scheme");
        dt.Columns.Add("Qty");
        dt.Columns.Add("MRP");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Dis1P");
        dt.Columns.Add("Dis2P");
        dt.Columns.Add("TaxP");
        dt.Columns.Add("Dis3P");
        dt.Columns.Add("Godown_ID");
        dt.Columns.Add("RowNum");
        dt.Columns.Add("Excise_Duty");
        dt.Columns.Add("Excise_Amt");
        DataRow dr;


        Int32 RowNumm = 1;
        foreach (var item in objDeliveryDetails)
        {
            dr = dt.NewRow();
            dr["Item_Code"] = item.Item_Code;
            dr["Scheme"] = 0;
            dr["Qty"] = 0;
            dr["MRP"] = item.MRP;
            dr["Rate"] = item.Rate;
            dr["Amount"] = 0;
            dr["Dis1P"] = 0;
            dr["Dis2P"] = 0;
            dr["TaxP"] = 0;
            dr["Dis3P"] = 0;
            dr["Godown_ID"] = 0;
            dr["RowNum"] = RowNumm;
            dr["Excise_Duty"] = 0;
            dr["Excise_Amt"] = 0;
            dt.Rows.Add(dr);
            RowNumm = RowNumm + 1;


        }

        int Status = new SaleBLL().InsertItemToDelete(Branch, dt, ItemType, Pos);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetProductsByType(string ItemType,int Branch,int Pos)
    {
        
        List<Sale> lstProducts = new SaleBLL().GetProductsByTypeNewDel(ItemType, Branch,Pos);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetProductsToDeleteByType(string ItemType, int Branch, int Pos)
    {

        List<Sale> lstProducts = new SaleBLL().GetProductsToDeleteByTypeNewDel(ItemType, Branch, Pos);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string GetOrdersBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch,int Pos)
    {
       

        List<Bill> lstBills = new BillBLL().GetOrderBillsByDateNewform22(DateFrom,DateTo,ItemType, Branch,Pos);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetOrderItemBYDate(DateTime DateFrom, DateTime DateTo, string ItemType, int Branch,int Pos)
    {


        List<BillDetail> lstBills = new BillBLL().GetOrdersBillItemByDateNewDel(DateFrom, DateTo, ItemType, Branch,Pos);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            BillsList = lstBills

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static string GetOptions(string ProcessType)
    {


        List<Bill> lstOptions = new BillBLL().GetOptionsNewDel(ProcessType);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            lstOptions = lstOptions

        };
        return ser.Serialize(JsonData);

    }

    [WebMethod]
    public static void InsertNewForm22(string arrItemCode, string arrRRItemCode,string ItemType, string Saletype, int Branch, int Pos,DateTime DateFrom,DateTime DateTo)
    {
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            string[] ItemCodedata = arrItemCode.Split(',');
            string[] RRItemCodedata = arrRRItemCode.Split(',');
            

            DataTable dt = new DataTable();

            dt.Columns.Add("ItemCode");
            dt.Columns.Add("RRItemCode");
           



            for (int i = 0; i < ItemCodedata.Length; i++)
            {
                if (ItemCodedata[i] != "")
                {
                    DataRow dr = dt.NewRow();

                    dr["ItemCode"] = Convert.ToString(ItemCodedata[i]);
                    dr["RRItemCode"] = Convert.ToString(RRItemCodedata[i]);
                    

                    dt.Rows.Add(dr);
                }
            }
            SqlCommand cmd = new SqlCommand("master_sp_SaleDeletioncrCrdOnline", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@FromDate", DateFrom);
            cmd.Parameters.AddWithValue("@ToDate", DateTo);
            cmd.Parameters.AddWithValue("@ItemType", Saletype);
            cmd.Parameters.AddWithValue("@BranchId", Branch);
            
            
            cmd.Parameters.AddWithValue("@Pos", Pos);
            cmd.Parameters.AddWithValue("@BillType", dt);
            cmd.CommandTimeout = 20000;
            cmd.ExecuteNonQuery();
        }

        //JavaScriptSerializer ser = new JavaScriptSerializer();


        //int status = new SaleBLL().DeleteBillsnewDel(Branch, dt,ItemType,Saletype);
        //var JsonData = new
        //{

        //   Status = status
        //};
        // return ser.Serialize(JsonData);



    }



    [WebMethod]
    public static void Insert(string BillNowPrefix, string Amount, string BillNo, string ItemType, string Saletype,int Branch,int Pos)
    {
        Connection connn = new Connection();
        using (SqlConnection con = new SqlConnection(connn.sqlDataString))
        {
            con.Open();
            string[] BillNowPrefixData = BillNowPrefix.Split(',');
            string[] AmountData = Amount.Split(',');
            string[] BillNoData = BillNo.Split(',');

            DataTable dt = new DataTable();

            dt.Columns.Add("BillNowPrefix");
            dt.Columns.Add("Amount");
            dt.Columns.Add("BillNo");



            for (int i = 0; i < BillNowPrefixData.Length; i++)
            {
                if (BillNowPrefixData[i] != "")
                {
                    DataRow dr = dt.NewRow();

                    dr["BillNowPrefix"] = Convert.ToString(BillNowPrefixData[i]);
                    dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                    dr["BillNo"] = Convert.ToInt32(BillNoData[i]);

                    dt.Rows.Add(dr);
                }
            }
            SqlCommand cmd = new SqlCommand("master_sp_SaleDeletionNewDelcrCrdOnline", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchId", Branch);
            cmd.Parameters.AddWithValue("@BillType", dt);
            cmd.Parameters.AddWithValue("@Type", ItemType);
            cmd.Parameters.AddWithValue("@SaleType", Saletype);
            cmd.Parameters.AddWithValue("@Pos", Pos);
            cmd.CommandTimeout = 20000;
            cmd.ExecuteNonQuery();
        }

        //JavaScriptSerializer ser = new JavaScriptSerializer();


        //int status = new SaleBLL().DeleteBillsnewDel(Branch, dt,ItemType,Saletype);
        //var JsonData = new
        //{

        //   Status = status
        //};
       // return ser.Serialize(JsonData);



    }


    [WebMethod]
    public static string DeleteItemsBill(string ItemCode, string Amount,  string ItemType, string Saletype, int Branch,DateTime DateFrom,DateTime DateTo,int Pos)
    {

        string[] ItemCodeData = ItemCode.Split(',');
        string[] AmountData = Amount.Split(',');
       

        DataTable dt = new DataTable();

     
        dt.Columns.Add("Amount");
        dt.Columns.Add("ItemCode");



        for (int i = 0; i < ItemCodeData.Length; i++)
        {
            if (ItemCodeData[i] != "")
            {
                DataRow dr = dt.NewRow();

              
                dr["Amount"] = Convert.ToDecimal(AmountData[i]);
                dr["ItemCode"] = Convert.ToString(ItemCodeData[i]);

              

                dt.Rows.Add(dr);
            }
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();


        int status = new SaleBLL().DeleteItemBillsNewDel(Branch, dt, ItemType, Saletype,DateFrom,DateTo,Pos);
        var JsonData = new
        {

            Status = status
        };
        return ser.Serialize(JsonData);



    }

}


