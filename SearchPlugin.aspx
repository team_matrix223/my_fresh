﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="SearchPlugin.aspx.cs" Inherits="SearchPlugin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
   <script type="text/javascript" src="js/autocomplete/countries.js"></script>
        <script src="js/autocomplete/jquery.autocomplete.js"></script>

        <style type="text/css">
        #tbSearchResult{padding-left:0px;}
        #tbSearchResult li{padding:5px;border-bottom:1px solid #ddd;list-style-type:none;cursor:pointer;}
        #tbSearchResult li:hover{background:seashell;padding:5px;border-bottom:1px solid #ddd;list-style-type:none;cursor:pointer}
        
        
        </style>
        <script type="text/javascript">

            $(document).ready(
            function () {


                (function ($) {
                    $.fn.extend({
                        donetyping: function (callback, timeout) {
                            timeout = timeout || 1e3; // 1 second default timeout
                            var timeoutReference,
                                        doneTyping = function (el) {
                                            if (!timeoutReference) return;
                                            timeoutReference = null;
                                            callback.call(el);
                                        };
                            return this.each(function (i, el) {
                                var $el = $(el);
                                // Chrome Fix (Use keyup over keypress to detect backspace)
                                // thank you @palerdot
                                $el.is(':input') && $el.on('keyup keypress', function (e) {
                                    // This catches the backspace button in chrome, but also prevents
                                    // the event from triggering too premptively. Without this line,
                                    // using tab/shift+tab will make the focused element fire the callback.
                                    if (e.type == 'keyup' && e.keyCode != 8) return;

                                    // Check if timeout has been set. If it has, "reset" the clock and
                                    // start over again.
                                    if (timeoutReference) clearTimeout(timeoutReference);
                                    timeoutReference = setTimeout(function () {
                                        // if we made it here, our timeout has elapsed. Fire the
                                        // callback
                                        doneTyping(el);
                                    }, timeout);
                                }).on('blur', function () {
                                    // If we can, fire the event since we're leaving the field
                                    doneTyping(el);
                                });
                            });
                        }
                    });
                })(jQuery);




                $('#txtSearch').donetyping(function () {


                    var Keyword = $(this).val();

                    if (Keyword.length >= 3) {

                        $("#dvSearchResult").css("display", "block");
                        $("#tbSearchResult").html("<img src='images/searchloader.gif'>");

                        $.ajax({
                            type: "POST",
                            data: '{"Keyword":"' + Keyword + '"}',
                            url: "SearchPlugin.aspx/KeywordSearch",
                            contentType: "application/json",
                            dataType: "json",
                            success: function (msg) {
                                // var obj = jQuery.parseJSON(msg.d);


                                var Content = msg.d;
                                $("#tbSearchResult").html(Content);




                            }, error: function (xhr, ajaxOptions, thrownError) {

                                var obj = jQuery.parseJSON(xhr.responseText);
                                alert(obj.Message);
                            },
                            complete: function (msg) {


                            }

                        });

                    }
                    else {
                        $("#dvSearchResult").css("display", "none");

                    }



                });





            });
        </script>

        <div class="right_col" role="main">
                <div class="">

                
<input type="text" id="txtSearch" />
<div id="dvSearchResult" style="display:none;width:250px;max-height:400px;overflow-y:scroll;background:white">
 
<ul id="tbSearchResult">

</ul>

</div>

                </div>
                
                </div>





</asp:Content>

