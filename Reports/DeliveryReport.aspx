﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="DeliveryReport.aspx.cs" Inherits="Reports_DeliveryReport" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
 
  <style>
    tr{
          text-align: center;
    }
  </style>
  <div style="width:1050px;overflow:scroll;height:400px" id ="dvle">
     <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row">
      <table>
        <tr><td> <label style="color:white">Form Date</label></td><td><asp:TextBox ID="txtDateFrom" runat="server" 
            Width="100px"></asp:TextBox> <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtDateFrom">
        </cc1:CalendarExtender></td> <td style="padding: 10px;"> <label style="color:white"> To Date</label></td><td><asp:TextBox ID="txtToDate" runat="server" 
            Width="100px"></asp:TextBox>
          <cc1:CalendarExtender ID="txtToDate_CalendarExtender" runat="server" Enabled="True" TargetControlID="txtToDate">
          </cc1:CalendarExtender>
          </td><td style="padding: 10px;"><asp:Button runat="server" Text="Genrate Report" ID="btnSubmit" OnClick="btnSubmit_Click" /></td></tr>
      </table>
<b></b><br /> 
<asp:GridView ID="gvDeliveryReport" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" AutoGenerateColumns="false" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
     <HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
  <Columns>
    <asp:BoundField DataField="billnowprefix" HeaderText="BillNoWPrefix" />
   <asp:BoundField DataField="bill_date" HeaderText="Bill Date"  DataFormatString="{0:dd-M-yyyy}"  />
    <asp:BoundField DataField="remarks" HeaderText="Remarks" />
     <asp:BoundField DataField="cashcust_name" HeaderText="Customer Name" />
     <asp:BoundField DataField="net_amount" HeaderText="Net Amount" />
     <asp:BoundField DataField="billmode" HeaderText="Bill Mode" />
     <asp:BoundField DataField="custmobile" HeaderText="Mobile No" />
    <asp:BoundField DataField="empname" HeaderText="Employee Name" />
  </Columns>
</asp:GridView>
</div>
</asp:Content>


