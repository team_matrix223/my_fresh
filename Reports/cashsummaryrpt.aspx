﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="cashsummaryrpt.aspx.cs" Inherits="cashsummaryrpt" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/CashSummaryrpt.css" rel="stylesheet" />
	<link href="css/cashsummaryrptNew.css" rel="stylesheet" />

<script type ="text/javascript">
    $(document).ready(
function () {




}
);
</script>
    <div class="department_wise">
		<div style="text-align: center;"><h2 class="cash-sum-head">CASH SUMMARY REPORT</h2></div>
        <div class="cash-sum-top-panel">
			<div class="col-md-4 col-sm-4 col-xs-12 cash-sum-branch-col">
				<div class="cash-sum-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 cash-sum-dat-col">
				<div class="cash-sum-date-section">
					<div class="cash-sum-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
					<div class="cash-sum-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control">
                    </asp:TextBox><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 cash-sum-btn-col">
				<div class="cash-sum-btn-section">
					<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Generate Report" onclick="btnGetRecords_Click"/>
				</div>
			</div>
        </div>
		    <%--<table class="department_header">
                <tr><td colspan="100%"> CASH SUMMARY REPORT </td></tr>
            </table>--%>
		
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
     <%--   <table class="sale_report">

            <tr>
                <td>Choose Branch</td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Date From:</td>
                <td>
                </td>
                <td>Date To:</td>
                <td>
                </td>
                <td></td>
            </tr>
        </table>--%>
       </div>
		<div class="cash-sum-toolbar-rpt">
			 <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
		</div>
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>


</asp:Content>

