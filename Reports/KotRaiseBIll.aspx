﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="KotRaiseBIll.aspx.cs" Inherits="KotRaiseBIll" %>
  <%@ Register src="~/Templates/AddCashCustomer.ascx" tagname="AddCashCustomer" tagprefix="uc3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">




    <form id="form1" runat="server">

        <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDate" runat="server"/>
  
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <%--<link href="css/bootstrap.min.css" rel="stylesheet" />--%>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Super Store Billing</title>
<%--    <link rel="stylesheet" href="css/jquery-ui.css">
    <script src="js/jquery-1.10.2.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link rel="stylesheet" href="css/bootstrap-gl  yphicons.css" />
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />--%>
   
   
      <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
        <link href="semantic.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js/SearchPlugin.js"></script>
     <link href="css/css.css" rel="stylesheet" />
    <style type="text/css">
        .form-control {
            margin: 3px;
            border: solid 1px silver;
            padding: 3px;
        }

        .ui-widget-content a {
            color: White;
            text-decoration: none;
        }

        #tbProductInfo tr {
            border-bottom: dotted 1px silver;
        }

            #tbProductInfo tr td {
                padding: 3px;
            }


        #tboption tr {
            border-bottom: solid 1px black;
        }

            #tboption tr td {
                padding: 10px;
            }
    </style>
    <script language="javscript" type="text/javascript">
      var TableeId = 0;

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }

        var TakeAwayDine = "";
        var BillBasicType = "";
        var AllowServicetax = false;
        var AllowServicetaxontake = false;
        var EnableCashCustomer = false;
        var DefaultPaymode = "";
        var DefaultBank = "";
        var RoundBillAmount = false;
        var PrintShortName = false;
        var FocAffect = false;
        var NEgativeStock = false;
        var AllowDiscountOnBilling = false;
        var EnableDiscountAmount = false;
        var EnableCustomerDiscount = false;
        var DiscountOnBillValue = false;
        var BackEndDiscount = false;
        var BEndDiscountAmt = 0;
        var m_BillMode="";
        var RoleForEditRate = false;
        var HomeDelCharges = false;
        var minbillvalue = 0;
        var DeliveryCharges = 0;
        var AllowKKC = false;
        var AllowSBC = false;
        var KKC = 0;
        var SBC =0;



        function ResetCashCustmr() {

            $("#lblCashCustomerName").text("");
            CshCustSelId = 0;
            CshCustSelName = "";
            $("#CashCustomer").css("display", "none");
            $("#txtddlMobSearchBox").val("");
            $("#ddlMobSearchBox").html("");
            $("#txtddlMobSearchBox").val("CASH");
            $("#txtcustId").val("CASH");
          
        }


       

        function processingComplete() {
            $.uiUnlock();

        }


        var CshCustSelId = 0;
        var CshCustSelName = "";
        var CrdCustSelId = 0;
        var CrdCustSelName = "";
        var m_BillNowPrefix = "";
        var m_DiscountType = "";
        var Sertax = 0;
        var Takeaway = 0;
        var Takeawaydefault = 0;
        var mode = "";
        var count = 0;
        var modeRet = "";
        var billingmode = ""
        var OrderId = "";
        var Type = "";
        var TotalItems = 0;
        //.....................................


        function EditBill(BillNowPrefix) {


            BillNowPrefix = m_BillNowPrefix;
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                   
                    $("#dvDeliveryChrg").val(obj.productLists[0]["DeliveryCharges"]);
                    $("#dvDeliveryChrg").val(obj.productLists[0]["KKCPer"]);
                   
                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["Discount"], obj.productLists[i]["Qty"],obj.productLists[i]["Edit_SaleRate"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }




        function GenerateBill(OrderNo) {
            RestControls();
            OrderId = OrderNo;

            $.ajax({
                type: "POST",
                data: '{ "OrderNo": "' + OrderNo + '"}',
                url: "screen.aspx/GetByOrderNo",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["Item_Name"], obj.productLists[i]["Sale_Rate"], obj.productLists[i]["Tax_Code"], obj.productLists[i]["SurVal"], obj.productLists[i]["Item_Code"], obj.productLists[i]["Tax_ID"], obj.productLists[i]["Item_Remarks"], obj.productLists[i]["ItemDiscount"],1,obj.productLists[i]["Edit_SaleRate"]);


                    }

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#CustomerSearchWindow").hide();
                    $("#dvProductList").show();
                    $("#dvBillWindow").hide();
                    $("#dvCreditCustomerSearch").hide();
                    $("#dvHoldList").hide();
                    $("#dvOrderList").hide();
                    $.uiUnlock();
                }

            });


        }


    


        function DEVBalanceCalculation() {


            var txtCashReceived = $("#txtCashReceived");
            var txtCreditCard = $("#Text13");
            var txtCheque = $("#Text15");
            var txtFinalBillAmount = $("#txtFinalBillAmount");


            if (Number(txtCashReceived.val()) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(0);
                txtCheque.val(0);
            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val())) >= Number(txtFinalBillAmount.val())) {
                txtCreditCard.val(Number(txtFinalBillAmount.val()) - Number(txtCashReceived.val()));
                txtCheque.val(0);

            }
            else if ((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val())) >= Number(txtFinalBillAmount.val())) {
                txtCheque.val(Number(txtFinalBillAmount.val()) - (Number(txtCashReceived.val()) + Number(txtCreditCard.val())));

            }



            var balReturn = Number((Number(txtCashReceived.val()) + Number(txtCreditCard.val()) + Number(txtCheque.val()) - Number(txtFinalBillAmount.val())));


            $("#txtBalanceReturn").val(balReturn.toFixed(2));


        }




        //...................................

        var DiscountAmt = 0;

        var Total = 0;
        var DisPer = 0;
        var VatAmt = 0;
        var TaxAmt = 0;
        var KKCTAmt=0;
        var SBCTAmt =0;


       

        function RestControls() {
            $("#lblCreditCustomerName").text("");

            $("ddlCreditCustomers").hide();
            $("ddlChosseCredit").hide();
            $("ddlChosseCredit").val(0);
            BillNowPrefix1 = "";
            Type = "";
            $("#CashCustomer").css("display", "none");
            ProductCollection = [];

            $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();

            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:19px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
            $("#dvdisper").val(0);
            $("#dvdiscount").val(0);
            $("#dvnetAmount").html("0");
            $("#dvRound").html("0");
            $("#dvTax").html("0");
            $("#dvVat").html("0");
            $("#dvDeliveryChrg").val("0");
            $("#dvsertaxper").html("0");
            $("#dvKKCPer").html("0");
            $("#dvKKCAmt").html("0");
         
            $("#dvSBCPer").html("0");
            $("#dvSBCAmt").html("0");

            $("#dvsbtotal").html("0");
            m_ItemId = 0;
            m_ItemCode = "";
            m_ItemName = "";
            m_Qty = 0;
            m_Price = 0;
            m_Vat = 0;
            $("#hdnsteward").val("0");

            $("#txtFinalBillAmount").val("");
            $("#ddlbillttype").val("");
            $("#txtCashReceived").val("0");
            $("#Text13").val("0");
            $("#Text15").val("0");
            $("#Text14").val("");
            $("#ddlType").val("");
            $("#ddlBank").val("");
            $("#ddlTable option").removeAttr("selected");
            $("#Text16").val("");
            $("#txtBalanceReturn").val("0");
            $("#dvBillWindow").hide();
            $("#dvCreditCustomerSearch").hide();
            $("#dvProductList").show();
            $("#dvHoldList").hide();
            $("#dvOrderList").hide();
            $("#txtCashReceived").val("0").prop("readonly", false);
            $("#txtPax").val("");
            $("#txtKotTable").val("");
            $("#txtkotsteward").val("");
            $("#txtcustId").val("");
            $("#txtddlMobSearchBox").val("");
            $("#txtKotDate").val($("#<%=hdnDate.ClientID%>").val());
            CrdCustSelId = 0;
            CshCustSelId = 0;
            CshCustSelName = "";
            CrdCustSelName = "";
            TableeId = 0;
            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

             $("#btnEdit").css({ "display": "none" });
             $("#btnShowScreen").css({ "display": "none" });

             $("#btnDelete").css({ "display": "none" });

             for (var i = 0; i < arrRole.length; i++) {

                 if (arrRole[i] == "9") {

                     $("#btnShowScreen").css({ "display": "block" });
                 }

                 if (arrRole[i] == "3") {

                     $("#btnEdit").css({ "display": "block" });
                 }

                 if (arrRole[i] == "2") {

                     $("#btnDelete").css({ "display": "block" });
                 }
             }

           



        }



        function InsertUpdate() {

         if($("#txtcustId").val() == "")
         {
         alert("Please Choose Customer");
         return;
     }

     
         var CustomerId = "0";
         var CustomerName = "CASH";
         var billmode = "Cash";
         var NetAmt = $("#dvnetAmount").html();
         var CreditBank = "";
         var CashAmt = "0.00";
         var creditAmt = "0.00";
         var CreditCardAmt = "0.00";

        

      
         var cashcustcode = $("#txtcustId").val();


         var cashcustName = $("#txtddlMobSearchBox").val();
         if ($("#chktakeway").prop('checked') == true) {
             if (cashcustcode == "0") {
                 alert("Please Choose Customer");
                 return;
             }

         }


         var OnlinePayment = "0.00";



            var OrderNo = OrderId;
            if (OrderNo == "") {
                OrderNo = "0";
            }

            var BIllValue = $("#dvsbtotal").html();

            var DisPer = $("#dvdisper").val();

            var lessdisamt = $("#dvdiscount").val();

            var addtaxamt = $("#dvVat").html();
            var RoundAmt = 0;
            var hdnNetamt = $("#hdnnetamt").val();
            RoundAmt = Number(NetAmt) - Number(hdnNetamt);

            var DeliveryCharges = 0;
            var EmpCode = $("#hdnsteward").val();

      
           
            var remarks = option;


            var Tableno = TableeId;
            var setatx = $("#dvTax").html();
            var ServiceTax = $("#dvsertaxper").html();
            var KKCPer = $("#dvKKCPer").html();
            var KKCAmt = $("#dvKKCAmt").html();
            var SBCPer = $("#dvSBCPer").html();
            var SBCAmt = $("#dvSBCAmt").html();
           
            var ItemCode = [];
            var Price = [];
            var Qty = [];
            var Tax = [];
            var OrgSaleRate = [];
            var PAmt = [];
            var Ptax = [];
            var PSurChrg = [];
            var ItemRemarks = [];
            var SurPer = [];
            if (ProductCollection.length == 0) {
                alert("Please first Select ProductsFor Billing");

                return;
            }

            for (var i = 0; i < ProductCollection.length; i++) {
             
                ItemCode[i] = ProductCollection[i]["ItemCode"];
                Qty[i] = ProductCollection[i]["Qty"];
                Price[i] = ProductCollection[i]["Price"];
                Tax[i] = ProductCollection[i]["TaxCode"];
                OrgSaleRate[i] = ProductCollection[i]["Price"];
                PAmt[i] = ProductCollection[i]["ProductAmt"];
                Ptax[i] = ProductCollection[i]["Producttax"];
                PSurChrg[i] = ProductCollection[i]["ProductSurchrg"];
                ItemRemarks[i] = ProductCollection[i]["ItemRemarks"];
                SurPer[i] = ProductCollection[i]["SurVal"];

            }
           
            TaxDen = [];
            VatAmtDen = [];
            VatDen = [];
            SurDen = [];

            $("div[name='tax']").each(
           function (y) {
               TaxDen[y] = $(this).html();
           }
           );
            $("div[name='amt']").each(
           function (z) {
               VatAmtDen[z] = $(this).html();
           }
           );
            $("div[name='vat']").each(
           function (a) {
               VatDen[a] = $(this).html();
           }
           );
            $("div[name='sur']").each(
           function (a) {
               SurDen[a] = $(this).html();
           }
           );
            var BillNowPrefix = "";
            var BillRemarks = "";
            $.uiLock();
            var Result = "";

            $.ajax({
                type: "POST",
                data: '{ "CustomerId": "' + CustomerId + '","CustomerName": "' + CustomerName + '","BillValue": "' + BIllValue + '","DiscountPer": "' + DisPer + '","DiscountAmt": "' + lessdisamt + '","AddTaxAmt": "' + addtaxamt + '","NetAmt": "' + NetAmt + '","BillMode": "' + billmode + '","CreditBank": "' + CreditBank + '","CashAmt": "' + CashAmt + '","CreditAmt": "' + creditAmt + '","CrCardAmt": "' + CreditCardAmt + '","RoundAmt": "' + RoundAmt + '","CashCustCode": "' + cashcustcode + '","CashCustName": "' + cashcustName + '","TableNo": "' + Tableno + '","SerTax": "' + ServiceTax + '","Remarks": "' + remarks + '","OrderNo":"' + OrderNo + '","Type":"' + Type + '","EmpCode":"' + EmpCode + '","BillNowPrefix":"' + m_BillNowPrefix + '","itemcodeArr": "' + ItemCode + '","qtyArr": "' + Qty + '","priceArr": "' + Price + '","taxArr": "' + Tax + '","orgsalerateArr": "' + OrgSaleRate + '","SurPerArr": "' + SurPer + '","AmountArr": "' + PAmt + '","TaxAmountArr": "' + Ptax + '","SurValArr": "' + PSurChrg + '","ItemRemarksArr": "' + ItemRemarks + '","arrTaxden":"' + TaxDen + '","arrVatAmtden":"' + VatAmtDen + '","arrVatden":"' + VatDen + '","arrSurden":"' + SurDen + '","OnlinePayment": "' + OnlinePayment + '","DeliveryCharges": "' + DeliveryCharges + '","KKCPer": "' + KKCPer + '","KKCAmt": "' + KKCAmt + '","SBCPer": "' + SBCPer + '","SBCAmt": "' + SBCAmt + '","BillRemarks": "' + BillRemarks + '"}',
                url: "kotRaiseBill.aspx/InsertUpdate",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);
                    BillNowPrefix = obj.BNF;

                    if (obj.Status == -11) {
                        alert("You don't have permission to perform this action..Consult Admin Department.");
                        return;
                    }


                    if (obj.Status == -5) {
                        alert("Please Login Again and Try Again..");
                        return;
                    }

                    if (obj.Status == 0) {
                        alert("An Error Occured. Please try again Later");
                        return;

                    }

                    else {


                        Result = "Yes";
                        $("#lblAmmmt").html(NetAmt);
                        $("#dvbillSave").toggle();
                     
                        Printt(BillNowPrefix);
                        $.uiUnlock();

                        BindGrid();

                    }


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#btnBillWindowOk").removeAttr('disabled');

                    if (Result == "Yes") {

                        UpdateKotDeActive(TableeId);
                        RestControls();
                    }
                    GetTable();
                    $.uiUnlock();


                }

            });

        }


        var option = "";



        function UpdateKotDeActive(TableID)
        {


            $.ajax({
                type: "POST",
                data: '{"TableID": "' + TableID + '"}',
                url: "KotRaiseBIll.aspx/UpdateKotDeActive",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    GetTable();

                    $.uiUnlock();
                }

            });


        }



        function Printt(celValue) {
          
            $.uiLock('<img src="images/loader.gif" alt="Please wait while loading...."/>');

            var iframe = document.getElementById('reportout');
            iframe = document.createElement("iframe");

            iframe.setAttribute("id", "reportout");
            iframe.style.width = 0 + "px";

            iframe.style.height = 0 + "px";
            document.body.appendChild(iframe);
          
            document.getElementById('reportout').contentWindow.location = "Reports/Retailbillrpt.aspx?BillNowPrefix=" + celValue;



        }

        function GetByItemCode(div, KotNo) {
            RestControls();


            callKot(KotNo);


         TableeId = KotNo;
      
            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "KotRaiseBill.aspx/GetKotDet",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);







                    for (var i = 0; i < obj.productLists.length; i++) {
                        addToList(obj.productLists[i]["ItemID"], obj.productLists[i]["ItemName"], obj.productLists[i]["SaleRate"], obj.productLists[i]["TaxCode"], 0, obj.productLists[i]["ProductCode"], obj.productLists[i]["Tax_ID"], "", obj.productLists[i]["DisAmount"], obj.productLists[i]["Qty"], obj.productLists[i]["SaleRate"]);


                    }


                    




                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });


           


        }


        function callKot(KotNo) {

            $.ajax({
                type: "POST",
                data: '{ "KotNo": "' + KotNo + '"}',
                url: "KotRaiseBill.aspx/GetKot",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    CrdCustSelId = "0";
                    CshCustSelName = "CASH";
                    $("#txtkotsteward").val(obj.KOT.EmpName);
                    $("#txtKotTable").val(obj.KOT.TableID);
                    $("#txtPax").val(obj.KOT.PaxNo);
                    $("#hdnsteward").val(obj.KOT.EmpCode);


                    if (obj.KOT.TakeAway == true) {

                        option = "TakeAway";
                        $("#chktakeway").prop('checked', true);
                    }
                    else {
                        option = "Dine";
                        $("#chktakeway").prop('checked', false);
                    }


                    $("#txtPax").attr("disabled", "disabled");
                    $("#txtKotTable").attr("disabled", "disabled");
                    $("#txtkotsteward").attr("disabled", "disabled");
                    $("#txtddlMobSearchBox").val(CshCustSelName);
                    $("#txtcustId").val(CrdCustSelId);


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {


                    $.uiUnlock();
                }

            });
        }

    
        function GetPluginData(Type) {

            if (Type == "CashCustomer") {

        
                var Discount = $("#ddlMobSearchBox option:selected").attr("discount");
                $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));
                $("#txtcustId").val($("#ddlMobSearchBox option:selected").val());
                CshCustSelId = $("#ddlMobSearchBox option:selected").val();
                CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");
                 
                $("#CashCustomer").css("display", "block");

                if (EnableCustomerDiscount == 1) {
                    $("#dvdisper").val(Discount.toFixed(2));

                }

                $("#CustomerSearchWindow").hide();
                $("#dvProductList").show();
                $("#dvBillWindow").hide();
                $("#dvHoldList").hide();
                $("#dvOrderList").hide();
                CommonCalculation();





            }
            else if (Type == "Accounts") {
                var customerId = $("#ddlCreditCustomers").val();
                CrdCustSelId = customerId;
                $("#hdnCreditCustomerId").val(customerId);


                $("#lblCreditCustomerName").text($("#ddlCreditCustomers option:selected").text() + "  " + $("#ddlCreditCustomers option:selected").attr("CADD1") + "  " + $("#ddlCreditCustomers option:selected").attr("CONT_NO"));
                CrdCustSelName = $("#ddlCreditCustomers option:selected").text();



                $("#creditCustomer").css("display", "block");
            }
        }


        $(document).ready(
         function () {


             $("#chkhone").click(
             function () {
                 if ($("#chkhone").prop('checked') == true) {
                     $("#<%=ddlsteward.ClientID %>").removeAttr("disabled");
                     $("#DelCharges").css("display", "block");

                 }
                 else {
                     $("#<%=ddlsteward.ClientID %>").attr("disabled", "disabled");
                     $("#DelCharges").css("display", "none");
                 }

             });



             if ($("#hdndd").val() == "C") {
                 $("#btnCash").css("display", "none");
             }
             else {
                 $("#btnCash").css("display", "block");
             }


             GetTable();




             $(window).keydown(function (e) {


                 switch (e.keyCode) {
                     case 112:   // F1 key is left or up

                         CashSave();

                         return false;
                     case 113: //F2 key is left or down

                         ClearData();

                         return false; //"return false" will avoid further events
                     case 114: //F3 key is left or down
                         ReturnItem();

                         return false;
                     case 119: //F8 key is left or down

                         CreditCardSave();


                         return false;

                     case 122: //F11 key is left or down
                         BillUnhold();
                         return false;

                     case 123: //F12 key is left or down
                         InsertHoldBill();
                         return false;


                 }
                 return; //using "return" other attached events will execute
             });




             $("#chkDelivery").change(function () {

                 //                if($('#chkDelivery').prop('checked') == true)

                 //                {
                 //                $("#DelCharges").show();
                 //                }
                 //                else{

                 //                $("#DelCharges").hide();
                 //                }

             });



             $("#btnMsgClose").click(
         function () {
             $("#dvbillSave").css("display", "none");
         }
         )
         ;


             $("#btnReprint").click(
         function () {

             var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
             if ($.trim(SelectedRow) == "") {
                 alert("No Bill is selected to Reprint");
                 return;
             }

             m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillNowPrefix')

             $.uiLock('');
             var PrintType = "RetailBill";

             $.ajax({
                 type: "POST",
                 data: '{"PrintType": "' + PrintType + '","BillNowPrefix": "' + m_BillNowPrefix + '"}',
                 url: "BillScreen.aspx/Reprint",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                     Printt(m_BillNowPrefix);
                     $.uiUnlock();

                 }

             });





         }
         );



             $("#colDvProductList").mouseenter(
         function () {

             $("#colProducts").slideUp(200);
         }

         );

             $("#ddlCreditCustomers").val("").removeAttr("disabled");
             $("#ddlCreditCustomers").css({ "display": "none" });

             $("#ddlChosseCredit").css({ "display": "none" });
             $("#ddlChosseCredit").val(0);

             $.ajax({
                 type: "POST",
                 data: '{ }',
                 url: "BillScreen.aspx/GetAllBillSetting",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);
                     BillBasicType = obj.setttingData.retail_bill;
                     TakeAwayDine = obj.setttingData.TakeAwayDine;
                     AllowServicetax = obj.setttingData.ServiceTax;
                     if (AllowServicetax == 1) {
                         Sertax = obj.setttingData.SerTax_Per;

                         AllowKKC = obj.setttingData.AllowKKC;

                         AllowSBC = obj.setttingData.AllowSBC;
                         KKC = obj.setttingData.KKC;
                         SBC = obj.setttingData.SBC;
                         $("#trSBC").show();
                         $("#trKKC").show();
                         $("#trServicetax").show();
                     }
                     else {
                         $("#trServicetax").hide();
                         Sertax = 0;
                         AllowKKC = false;
                         AllowSBC = false;
                         KKC = 0;
                         SBC = 0;
                         $("#trSBC").hide();
                         $("#trKKC").hide();
                     }

                     Takeaway = obj.setttingData.AlloServicetax_TakeAway;
                     HomeDelCharges = obj.setttingData.homedel_charges;

                     minbillvalue = obj.setttingData.min_bill_value;
                     DeliveryCharges = obj.setttingData.del_charges;
                     EnableCashCustomer = obj.setttingData.CashCustomer;
                     DefaultBank = obj.setttingData.defaultBankID;
                     RoundBillAmount = obj.setttingData.roundamt;

                     PrintShortName = obj.setttingData.shortname;

                     FocAffect = obj.setttingData.focaffect;
                     NEgativeStock = obj.setttingData.NegtiveStock;

                     DefaultPaymode = obj.setttingData.defaultpaymodeID;
                     AllowDiscountOnBilling = obj.setttingData.Allow_Dis_on_Billing;

                     if (AllowDiscountOnBilling == 1) {
                         EnableCustomerDiscount = obj.setttingData.Enable_Cust_Dis;
                         EnableDiscountAmount = obj.setttingData.Enable_Dis_Amt;
                         DiscountOnBillValue = obj.setttingData.Dis_Bill_Value;
                         BackEndDiscount = obj.setttingData.Back_End_Discount;

                     }


                     if (HomeDelCharges == 1) {

                         if (minbillvalue == 0 || DeliveryCharges == 0) {
                             $("#DelCharges").show();
                             $("#dvDeliveryChrg").removeAttr("disabled");
                             $("#chkDelivery").show();
                             $("#lbldelcharges").show();

                         }
                         else {

                             $("#dvDeliveryChrg").val(DeliveryCharges);
                             $("#dvDeliveryChrg").attr("disabled", "disabled");
                             $("#chkDelivery").hide();
                             $("#lbldelcharges").hide();
                             $("#DelCharges").show();
                         }

                     }
                     else {
                         $("#DelCharges").hide();
                         $("#chkDelivery").hide();
                         $("#lbldelcharges").hide();
                         $("#dvDeliveryChrg").val(0);
                     }


                     if (EnableCashCustomer == 1) {

                         $("#btnCash").removeAttr('disabled')
                         //$("#txtMobSearchBox,#imgSrchCashCust").show();
                         $("#txtddlMobSearchBox").removeAttr('disabled');

                     }
                     else {

                         $("#btnCash").prop("disabled", true).css("background", "gray");
                         //$("#txtMobSearchBox,#imgSrchCashCust").hide();

                         $("#txtddlMobSearchBox").attr('disabled', 'disabled');
                     }


                     if (BillBasicType == "I") {

                         $("#vatIncOrExc").hide();
                     }
                     else {
                         $("#vatIncOrExc").show();
                     }



                     if (DiscountOnBillValue == 1) {

                         DiscountValues = obj.DiscountDetail;



                     }

                     if (EnableDiscountAmount == 1) {

                         $("#dvdisper").removeAttr('disabled');
                         $("#dvdiscount").removeAttr('disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }
                     else {

                         $("#dvdisper").attr('disabled', 'disabled');
                         $("#dvdiscount").attr('disabled', 'disabled');
                         $("#dvdisper").val("0");
                         $("#dvdiscount").val("0");
                     }

                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {



                 }

             });



             ValidateRoles();

             function ValidateRoles() {

                 var arrRole = [];
                 arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                 for (var i = 0; i < arrRole.length; i++) {

                     if (arrRole[i] == "18") {
                         RoleForEditRate = true;


                     }
                     else if (arrRole[i] == "9") {
                         $("#btnShowScreen").click(
                         function () {
                             RestControls();
                             m_BillNowPrefix = "";
                             m_BillMode = "";
                             $("#ddlbillttype").removeAttr("disabled");
                             $("#btnCreditCard").css("background", "#f06671");
                             $("#ddlCreditCustomers").html("");
                             $("#txtddlCreditCustomers").val("");

                             $("#btnCash").css("background", "#f06671");
                             $("#btnhold").css("background", "#f06671");

                             $("#screenDialog").dialog({
                                 autoOpen: true,

                                 width: 850,
                                 resizable: false,
                                 modal: false
                             });

                         }

                         );

                     }

                     else if (arrRole[i] == "2") {
                         $("#btnDelete").show();

                         $("#btnDelete").click(
        function () {

            var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');

            if ($.trim(SelectedRow) == "") {
                alert("No Bill is selected to Cancel");
                return;
            }

            var BillId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'BillNowPrefix')
            if (confirm("Are You sure to delete this record")) {
                $.uiLock('');


                $.ajax({
                    type: "POST",
                    data: '{"BillNowPrefix":"' + m_BillNowPrefix + '"}',
                    url: "BillScreen.aspx/Delete",
                    contentType: "application/json",
                    dataType: "json",
                    success: function (msg) {

                        var obj = jQuery.parseJSON(msg.d);

                        BindGrid();
                        alert("Bill is Canceled successfully.");

                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {
                        $.uiUnlock();
                    }
                });

            }


        }
        );

                     }




                     else if (arrRole[i] == "3") {
                         $("#btnEdit").show();
                         $("#btnEdit").click(
       function () {
           RestControls();

           var SelectedRow = jQuery('#jQGridDemoBill').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No Product is selected to add");
               return;
           }
           CshCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code');
           CshCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Name');
           CrdCustSelId = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_ID');
           CrdCustSelName = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'Customer_Name');

           m_BillMode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'BillMode');


           $("#btnhold").css("background", "gray");




           if (CshCustSelId != 0) {

               $("#ddlMobSearchBox").html("<option value='" + CshCustSelId + "'>" + CshCustSelName + "</option>");
               $("#txtddlMobSearchBox").val(CshCustSelName);
               $("#CashCustomer").show();
               $("#lblCashCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCustAddress'));
           }


           $("#ddlbillttype").prop("disabled", true);


           if (m_BillMode == "Credit") {

               $("#btnCash").css("background", "gray");
               $("#btnCreditCard").css("background", "gray");

               $("#lblCreditCustomerName").text($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CreditCustAddress'));

           }
           else if (m_BillMode == "CreditCard") {
               $("#btnCash").css("background", "gray");
               $("#btnCreditCard").css("background", "#f06671");

           }
           else if (m_BillMode == "Cash") {
               $("#btnCash").css("background", "#f06671");
               $("#btnCreditCard").css("background", "gray");
           }

           var TableNo = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno');



           if (TableNo == "0") {
               option = "TakeAway";
           }
           else {
               option = "Dine";
           }



           //option = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'remarks');




           //           $("#<%=ddloption.ClientID%>").val(option);
           //           if (option == "Dine") {
           //               $("#ddlTable").removeAttr("disabled");

           //               $("#ddlTable option").removeAttr("selected");

           //               $('#ddlTable option[value=' + TableNo + ']').prop('selected', 'selected');
           //           }

           //           else if (option == "HomeDelivery") {
           //               $("#ddlTable").hide();

           //               $("#ddlEmployees").show();
           //               $("#ddlTable option").removeAttr("selected");
           //               var empcode = $('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'EmpCode');

           //               $('#ddlEmployees option[value=' + empcode + ']').prop('selected', 'selected');

           //               $("#DelCharges").show();
           //               $("#dvDeliveryChrg").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DeliveryCharges'));
           //               $("#dvDeliveryChrg").attr("disabled", "disabled");

           //           }


           $("#dvdisper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'DiscountPer'));
           $("#txtKotTable").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'tableno'))
           $("#txtcustId").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'CashCust_Code'));
           $("#txtKotDate").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'strBD'));
           $("#txtkotsteward").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'EmpName'));


           EditBill(m_BillNowPrefix);
           $("#dvsertaxper").val($('#jQGridDemoBill').jqGrid('getCell', SelectedRow, 'servalue'));

           $("#screenDialog").dialog({
               autoOpen: true,

               width: 850,
               resizable: false,
               modal: false
           });

       }

        );

                     }
                     else if (arrRole[i] == "10") {
                         $("#btnhold").click(
function () {

    InsertHoldBill();
});

                     }



                 }

             }



             $("#btnGetByItemCode").click(
             function () {


                 var ItemCode = $("#txtItemCode");

                 if (ItemCode.val().trim() == "") {
                     ItemCode.focus();
                     return;
                 }


                 GetByItemCode(ItemCode.val());

             }

             );




             $("#btnClear").click(
              function () {

                  ClearControls();
                  $("#screenDialog").dialog("close");
              }

             );


             $("#ddlMobSearchBox").supersearch({
                 Type: "CashCustomer",
                 Caption: "CustName/Mobile ",
                 AccountType: "D",
                 Width: 50,
                 DefaultValue: 0,
                 Godown: 0
             });





             $("#dvdisper").keyup(
               function EnterEvent(e) {

                   if (e.keyCode == 13) {
                       var regex = /^[0-9\.]*$/;


                       var value = jQuery.trim($(this).val());
                       var count = value.split('.');


                       if (value.length >= 1) {
                           if (!regex.test(value) || value <= 0 || count.length > 2) {

                               $(this).val(0);


                           }
                       }

                       if (value > 100) {
                           $(this).val(100);

                       }




                       CommonCalculation();

                   }

               });


             //             $("#dvdisper").keyup(
             //     function () {

             //         var regex = /^[0-9\.]*$/;


             //         var value = jQuery.trim($(this).val());
             //         var count = value.split('.');


             //         if (value.length >= 1) {
             //             if (!regex.test(value) || value <= 0 || count.length > 2) {

             //                 $(this).val(0);


             //             }
             //         }

             //         if (value > 100) {
             //             $(this).val(100);

             //         }

             //       
             //      

             //         CommonCalculation();

             //     }
             //     );

             $("#dvdiscount").keyup(
           function () {
               var regex = /^[0-9]*$/;


               var value = jQuery.trim($(this).val());

               if (value.length >= 1) {
                   if (!regex.test(value) || value <= 0) {

                       $(this).val(0);


                   }
               }

               var ttlAmt = $("#dvsbtotal").html();

               if (value > Number(ttlAmt)) {
                   $(this).val(ttlAmt);

               }


               $("#dvdisper").val(($("#dvdiscount").val() * 100 / $("#dvsbtotal").html()).toFixed(2));
               CommonCalculation();

           }
           );




             $("#btnCash").click(
           function () {
               InsertUpdate();



           });





             function ClearData() {
                 m_BillNowPrefix = "";
                 m_BillMode = "";
                 $("#ddlbillttype").removeAttr("disabled");
                 $("#btnCreditCard").css("background", "#f06671");
                 $("#ddlCreditCustomers").html("");
                 $("#txtddlCreditCustomers").val("");
                 $("#ddlChosseCredit").val(0);
                 $("#btnCash").css("background", "#f06671");
                 $("#btnhold").css("background", "#f06671");

                 RestControls();
             }
















             $("#dvdisper").val("0");
             $("#dvdiscount").val("0");

             $("#btnSearch").click(
           function () {

               var Keyword = $("#txtSearch");
               if (Keyword.val().trim() != "") {
                   $("#colProducts").slideDown(200);

               }
               else {
                   Keyword.focus();
               }
               $("#txtSearch").val("");

           });



             $("#txtSearch").keyup(
             function (event) {

                 var keycode = (event.keyCode ? event.keyCode : event.which);

                 if (keycode == '13') {


                     var Keyword = $("#txtSearch");
                     if (Keyword.val().trim() != "") {
                         $("#colProducts").slideDown(200);

                         $("#txtSearch").val("");
                     }
                     else {
                         Keyword.focus();
                     }


                 }


             }

             );










             $.ajax({
                 type: "POST",
                 data: '{}',
                 url: "screen.aspx/BindCategories",
                 contentType: "application/json",
                 dataType: "json",
                 success: function (msg) {

                     var obj = jQuery.parseJSON(msg.d);


                     $("#categories").html(obj.categoryData);
                     //                     if (AllowServicetax == 1) {
                     //                         Sertax = obj.setttingData.SerTax;
                     //                     }
                     //                     else {
                     //                         Sertax = 0;
                     //                     }
                     //  Takeaway = obj.setttingData.TakeAway;

                     Takeawaydefault = obj.setttingData.TakeAwayDefault;

                     var CatId = obj.CategoryId;


                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     var obj = jQuery.parseJSON(xhr.responseText);
                     alert(obj.Message);
                 },
                 complete: function () {

                 }

             }
         );


             function ClearControls() {
                 $("#txtPax").val("");
                 $("#txtKotTable").val("");
                 $("#txtkotsteward").val("");
                 $("#txtKotDate").val("");
                 $("#txtcustId").val("");
                 $("#txtddlMobSearchBox").val("");
                 $("#dvdisper").val(0);
                 $("#dvdiscount").val(0);
                 $("#dvnetAmount").html("0");
                 $("#dvRound").html("0");
                 $("#dvTax").html("0");
                 $("#dvsertaxper").html("0");
                 $("#dvKKCPer").html("0");
                 $("#dvKKCAmt").html("0");
                 $("#dvSBCPer").html("0");
                 $("#dvSBCAmt").html("0");
                 $("#dvsbtotal").html("0");
                 ProductCollection = [];
                 $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
             }



             function ResetBillCntrols() {


                 $("#txtFinalBillAmount").val("");
                 $("#ddlbillttype").val("");
                 $("#txtCashReceived").val("0");
                 $("#Text13").val("0");
                 $("#Text15").val("0");
                 $("#Text14").val("");
                 $("#ddlType").val("");
                 $("#ddlBank").val("");

                 $("#Text16").val("");
                 $("#txtBalanceReturn").val("0");
                 modeRet = "";
                 $("#CustomerSearchWindow").hide();
                 $("#dvProductList").show();
                 $("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();



                 $("#hdnCreditCustomerId").val("0");
                 $("#ddlbillttype option[value='" + DefaultPaymode + "']").prop("selected", true);
                 $("#creditCustomer").css("display", "none");


                 if (m_BillNowPrefix != "") {

                     if (m_BillMode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (m_BillMode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (m_BillMode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }
                 }

                 else {


                     if (DefaultPaymode == "Credit") {


                         $("#dvOuter_ddlCreditCustomers").show();
                         $("#ddlChosseCredit").show();

                         $("#lblCashHeading").text("Receipt Amt:");
                         $("#ddlbillttype option[value='Credit']").prop("selected", true);
                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", true);
                         $("#Text14").val("").prop("readonly", true);
                         $("#Text15").val("0").prop("readonly", true);
                         $("#Text16").val("").prop("readonly", true);
                         $("#ddlType").prop("disabled", true);
                         $("#ddlBank").prop("disabled", true);

                     }
                     else if (DefaultPaymode == "CreditCard") {


                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");

                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();




                         $("#txtCashReceived").val("0").prop("readonly", true);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);

                     }
                     else if (DefaultPaymode == "Cash") {



                         CrdCustSelId = 0;
                         CrdCustSelName = "";
                         $("#hdnCreditCustomerId").val(0);
                         $("#lblCreditCustomerName").text("");
                         CrdCustSelName = "";
                         $("#ddlCreditCustomers").html("");
                         $("#txtddlCreditCustomers").val("");

                         $("#lblCashHeading").text("Cash Rec:");
                         $("#creditCustomer").css("display", "none");

                         $("#dvOuter_ddlCreditCustomers").hide();
                         $("#ddlChosseCredit").hide();



                         $("#txtCashReceived").val("0").prop("readonly", false);
                         $("#Text13").val("0").prop("readonly", false);
                         $("#Text14").val("").prop("readonly", false);
                         $("#Text15").val("0").prop("readonly", false);
                         $("#Text16").val("").prop("readonly", false);
                         $("#ddlType").prop("disabled", false);
                         $("#ddlBank").prop("disabled", false);


                     }



                 }








                 if (m_BillNowPrefix != "") {
                     $("#ddlbillttype option[value='" + m_BillMode + "']").prop("selected", true);
                 }
                 if (m_BillMode == "Credit") {

                     $("#ddlCreditCustomers").html("<option value='" + CrdCustSelId + "'>" + CrdCustSelName + "</option>");
                     $("#txtddlCreditCustomers").val(CrdCustSelName);
                     $("#creditCustomer").show();

                     if ($("#ddlChosseCredit").val() != "0") {
                         CrdCustSelId = $("#ddlChosseCredit").val();
                         CrdCustSelName = $("#ddlChosseCredit option:selected").text();
                     }


                 }


             }








             $("#btnCustomer").click(
         function () {

             if (ProductCollection.length == 0) {
                 alert("Please first Select ProductsFor Billing");

                 return;
             }
             else {
                 $("#CustomerSearchWindow").show();
                 $("#dvProductList").hide();
                 $("#dvBillWindow").hide();
                 $("#dvCreditCustomerSearch").hide();
                 $("#dvHoldList").hide();
                 $("#dvOrderList").hide();
                 bindGrid("M", $("#txtMobSearchBox").val());

             }



             //             $('#dvSearch').dialog(
             //            {
             //            autoOpen: false,

             //            width:720,
             //     
             //          
             //            resizable: false,
             //            modal: true,
             //                  
             //            });
             //            linkObj = $(this);
             //            var dialogDiv = $('#dvSearch');
             //            dialogDiv.dialog("option", "position", [238, 36]);
             //            dialogDiv.dialog('open');
             //            return false;



         });


             $(document).on("click", "#dvClose", function (event) {

                 var RowIndex = Number($(this).closest('tr').index());
                 var tr = $(this).closest("tr");
                 tr.remove();

                 ProductCollection.splice(RowIndex, 1);

                 if (ProductCollection.length == 0) {

                     //                    $("#tbProductInfo").append(" <tr><td colspan='100%' align='center'></td></tr>");


                     RestControls();



                 }
                 Bindtr();

             });





             $(document).on("click", "a[name='categories']", function (event) {

                 $("a[name='categories']").removeClass("ancSelected").addClass("ancBasic");
                 $(this).addClass("ancSelected").removeClass("ancBasic");



             });






             $("#btnSearch1").click(
         function () {

             bindGrid("N", "");

         }
         );

         });

    var m_ItemId = 0;
    var m_ItemCode = "";
    var m_ItemName = "";
    var m_Qty = 0;
    var m_Price = 0;
    var m_TaxRate = 0;
    var m_Surval = 0;
    var m_ItemDiscount = 0;
    var m_EditSaleRate = 0;

    var ProductCollection = [];
    function clsproduct() {
        this.ItemId = 0;
        this.ItemCode = "";
        this.ItemName = "";
        this.Qty = 0;
        this.Price = 0;
        this.TaxCode = 0;
        this.SurVal = 0;
        this.ProductAmt = 0;
        this.Producttax = 0;
        this.ProductSurchrg = 0;
        this.TaxId = 0;
        this.ItemRemarks = "";
        this.ItemDiscount = 0;
        this.EditSaleRate = false;
    }
    var m_Total =0;
    function Bindtr() {
        
        DiscountAmt = 0;
        VatAmt = 0;
        Total = 0;
        var fPrice = 0;
        TotalItems = 0;
        $("div[name='vat']").html("0.00");
        $("div[name='amt']").html("0.00");
        $("div[name='sur']").html("0.00");

        $('#tbProductInfo tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
        var counterId = 1;
        BEndDiscountAmt = 0;
        var Count = 0;
        for (var i = 0; i < ProductCollection.length; i++) {

        if(RoleForEditRate == true)
        {
         var tr = "<tr><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' name='txtBillQty' disabled='disabled' style='width:40px;text-align:center;font-size:15px;'  value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty"+counterId+"'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";

        }
        else
        {
         if(ProductCollection[i]["EditSaleRate"] == false)
           {

               var tr = "<tr><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' disabled='disabled'disabled='disabled'  name='txtBillQty' style='width:40px;text-align:center;font-size:15px;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' disabled='disabled' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
            }
            else
            {
                var tr = "<tr><td style='width:70px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemCode"] + "</td><td style='width:300px;text-align:center;font-size:15px'>" + ProductCollection[i]["ItemName"] + "</td><td style='width:50px;text-align:center'><input type='txtBillQty' disabled='disabled'  name='txtBillQty' style='width:40px;text-align:center;font-size:15px;' value='" + ProductCollection[i]["Qty"] + "' id='txtBillQty" + counterId + "'/></td><td style='width:100px;text-align:center;font-size:15px'><input type='txtBillPrice' disabled='disabled' style='width:100px;text-align:center' value='" + ProductCollection[i]["Price"] + ".00' id='txtBillPrice'/></td><td style='width:200px;text-align:center;display:none'><input type='text'    disabled=disabled style ='width:80px;padding:0px'  data-toggle='tooltip' title='" + ProductCollection[i]["ItemRemarks"] + "' id='txtRemarks" + counterId + "' onkeyup='javascript:AddRemarks(" + counterId + ");' value='" + ProductCollection[i]["ItemRemarks"] + "'/></td><td style='display:none'><img src='images/addon.png' style='cursor:pointer;height:23px' onclick='javascript:BindAddOn(" + counterId + ")'/></td></tr>";
            }

        }
           
            $("#tbProductInfo").append(tr);

                                   var searchInput = $("#txtBillQty"+counterId+"");

        searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });


            counterId = counterId + 1;
                   
            Count = Count + 1;

            fPrice = 0;
            fPrice = Number(ProductCollection[i]["Qty"]) * Number(ProductCollection[i]["Price"]);
            var itemDiscount = parseFloat(ProductCollection[i]["ItemDiscount"]);
            var Peritemdiscount = (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
            if (BillBasicType == "I") {
           

                if(ProductCollection[i]["SurVal"] == "0")
                {
                  var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"])));
                }
                else
                {
                   
                    var Survalll = ((Number(ProductCollection[i]["SurVal"]) * Number(ProductCollection[i]["TaxCode"])) / 100);
                    var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / (100 + Number(ProductCollection[i]["TaxCode"]) + Survalll));

                }
            }
            else {

                var TAx = (Number(fPrice) * Number(ProductCollection[i]["TaxCode"]) / 100);
               
               
            }

              
            var surchrg = (Number(TAx) * Number(ProductCollection[i]["SurVal"])) / 100;
            var tottax = Number(TAx) +  Number(surchrg);
           
          
           

            VatAmt = VatAmt + tottax;


            Total = Total + fPrice;
            ProductCollection[i]["ProductAmt"] = fPrice;
            ProductCollection[i]["Producttax"] = TAx;
            ProductCollection[i]["ProductSurchrg"] = surchrg;
          
          
            var amt = $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html()
            var vat = $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html()
            var sur = $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html()
            $("div[myid='amt_" + ProductCollection[i]["TaxId"] + "']").html(Number(amt) + Number(fPrice.toFixed(2)));
            $("div[myid='vat_" + ProductCollection[i]["TaxId"] + "']").html(Number(vat) + Number(TAx.toFixed(2)));
            $("div[myid='sur_" + ProductCollection[i]["TaxId"] + "']").html(Number(sur) + Number(surchrg.toFixed(2)));


            BEndDiscountAmt = BEndDiscountAmt + (parseFloat(fPrice) * parseFloat(itemDiscount) / 100);
            TotalItems = TotalItems + Number(ProductCollection[i]["Qty"]);
           
            
        }
       
      
        CommonCalculation();

        if (ProductCollection.length == 0) {
            var tr = "<tr style='border-bottom:0px'><td colspan='100%' style='text-align:center;font-weight:bold;font-size:12px'>ITEM(S) WILL BE DISPLAYED HERE </td></tr>";
            $("#tbProductInfo").append(tr);
        }
        
        $("#CustomerSearchWindow").hide();
        $("#dvProductList").show();
        $("#dvBillWindow").hide();
        $("#dvCreditCustomerSearch").hide();
        $("#dvHoldList").hide();
        $("#dvOrderList").hide();

        
    }


    function CommonCalculation() {

        m_Total = Total.toFixed(2) ;
        if(option == "HomeDelivery")
        {
        if(HomeDelCharges == 1)
        {

         if(minbillvalue != 0)
          {
            if(m_Total > minbillvalue)
            {
             $("#dvDeliveryChrg").val(0)

            }
          else{
          
           $("#dvDeliveryChrg").val(DeliveryCharges)


          }
          }
          else
          {
            DeliveryCharges =  $("#dvDeliveryChrg").val();
          }
        }

        }
       


          var BEndDiscountPer = 0;


        if (BackEndDiscount == 1) {



            BEndDiscountPer = BEndDiscountAmt * 100 / m_Total;

            $("#dvdisper").val(BEndDiscountPer.toFixed(2));
        }


        if (DiscountOnBillValue == 1) {

            for (var i = 0; i < DiscountValues.length; i++) {

                var StartValue = DiscountValues[i]["StartValue"];
                var EndValue = DiscountValues[i]["EndValue"];
                var DisPer = DiscountValues[i]["DisPer"];


                if (parseFloat(m_Total) >= parseFloat(StartValue) && parseFloat(m_Total) <= parseFloat(EndValue)) {

                    $("#dvdisper").val(DisPer.toFixed(2));
                    break;

                }
                else
                 {
                    $("#dvdisper").val(0);

                }



            }
        }



        DiscountAmt = (Number(Total) * Number($("#dvdisper").val())) / 100;

         
        $("#dvdiscount").val(DiscountAmt.toFixed(2));

          var  DisVatAmt = (Number(VatAmt) * Number($("#dvdisper").val()))/100 ;
          VatAmt = Number(VatAmt)-Number(DisVatAmt);

      
        $("div[id='dvsbtotal']").html(m_Total);


        m_Total = (Number(m_Total)-Number(DiscountAmt).toFixed(2));
       

        $("div[id='dvVat']").html(VatAmt.toFixed(2));
        TaxAmt = GetServiceTax();
        KKCTAmt = GetKKC();
        SBCTAmt = GetSBC();
    
        
        if(BillBasicType == "E") 
        {

            if (option == "Dine")
            {
               
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             
            }
            else if (option == "TakeAway")
            {
              if(Takeaway == 1)
             { 
              $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2)) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }
             else
             {
               $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+ Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }

              
            }
        else if (option == "HomeDelivery")
            {
               $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())+ Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
     




//        $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2)));
//        $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))))).toFixed(2));
        }
        else
        {

            if (option == "Dine")
            {
               
                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) ))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2)) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             
            }
            else if (option == "TakeAway")
            {
              if(Takeaway == 1)
             { 
              $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }
             else
             {
               $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
             }

              
            }
        else if (option == "HomeDelivery")
            {
               $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
     
        }
     
        

        if (RoundBillAmount == 1)
        {
            if (BillBasicType == "E") {

                if (option == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }
                else if (option == "TakeAway") {
                    
                    if(Takeaway ==1)
                    {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else{
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                }
            else if (option == "HomeDelivery")
                {
                 $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }




//                $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
               
            }
            else
            {

                if (option == "Dine") {

                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2)) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }
                else if (option == "TakeAway") {
                    
                    if(Takeaway ==1)
                    {
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                    else{
                    $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                    }
                }
            else if (option == "HomeDelivery")
                {
                 $("div[id='dvnetAmount']").html(Math.round((Number(Number(Number(Total.toFixed(2)) +Number($("#dvDeliveryChrg").val()) ) - Number(DiscountAmt.toFixed(2)))).toFixed(2)));
                }

            }
        }
        else
        {
            if (BillBasicType == "E") {

                if (option == "Dine") {
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))  + Number(VatAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                } else if (option == "TakeAway") {

                   if(Takeaway == 1)
                   {
                     $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                   }else{
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
               }
            else if (option == "HomeDelivery") {
                 $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) +Number($("#dvDeliveryChrg").val()) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
               }


             //   $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }
            else
            {
                if (option == "Dine") {
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                } else if (option == "TakeAway") {

                   if(Takeaway == 1)
                   {
                     $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                   }else{
                    $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                    }
               }
            else if (option == "HomeDelivery") {
                 $("div[id='dvnetAmount']").html((Number(Number(Number(Total.toFixed(2)) +Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
               }


            }


        }


        if (BillBasicType == "E") {


            if (option == "Dine")
           {
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
           }
            else if (option == "TakeAway") {
              
              if(Takeaway ==1)
              {
              $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(VatAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
              }else{
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+ Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
            }
        else if (option == "HomeDelivery")
            {

            $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))+Number($("#dvDeliveryChrg").val())+ Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }



          // $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2)) + Number(VatAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));

         }
        else {

            if (option == "Dine")
           {
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
           }
            else if (option == "TakeAway") {
              
              if(Takeaway ==1)
              {
              $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) + Number(TaxAmt.toFixed(2))+ Number(KKCTAmt.toFixed(2))+ Number(SBCTAmt.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
              }else{
                $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2))) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
                }
            }
        else if (option == "HomeDelivery")
            {

            $("#hdnnetamt").val((Number(Number(Number(Total.toFixed(2)) +Number($("#dvDeliveryChrg").val())) - Number(DiscountAmt.toFixed(2)))).toFixed(2));
            }

        }

        
        var netamount = $("div[id='dvnetAmount']").html();
        var hdnNetamt = $("#hdnnetamt").val();
        var Round = Number(netamount) - Number(hdnNetamt)

    

        $("div[id='dvRound']").html(Round.toFixed(2));
       

        $("#lblNoItems").html(TotalItems);
      
        //alert(Total);
        //alert(TaxAmt);
        //alert(VatAmt);
        //alert(DiscountAmt);

        //alert($("#hdnnetamt").val());
        //alert($("div[id='dvnetAmount']").html());


    }

    function addToList(ProductId, Name, Price, TaxCode, SurVal, Code, Tax_Id, Item_Remarks, Discount,B_Qty,EditSaleRate) {

    
        m_ItemId = ProductId;
        m_ItemCode = Code;
        m_Price = Price;
        m_ItemName = Name;
        m_EditSaleRate = EditSaleRate;

        if (B_Qty == 0) {
            if (modeRet == "Return") {
                m_Qty = -1;
            }
            else {
                m_Qty = 1;
            }
        }
        else {
            m_Qty = B_Qty;
        }
        m_TaxRate = TaxCode;
        m_Surval = SurVal;
        m_Tax_Id = Tax_Id;
        m_ItemDiscount = Discount;

        //         var item = $.grep(ProductCollection, function (item) {
        //             return item.ItemId == m_ItemId;
        //         });

        //         if (item.length) {

        //               for (var i = 0; i < ProductCollection.length; i++)
        //               {
        //                   if(ProductCollection[i]["ItemId"] == m_ItemId)
        //                   {
        //                       var qty = ProductCollection[i]["Qty"];
        //                       qty = qty+1;
        //                       ProductCollection[i]["Qty"] = qty ;
        //                       Bindtr();
        //                       return;
        //                   }
        //               }

        //         }


        TO = new clsproduct();

        TO.ItemId = m_ItemId;
        TO.ItemCode = m_ItemCode
        TO.ItemName = m_ItemName;
        TO.Qty = m_Qty;
        TO.Price = m_Price;
        TO.TaxCode = m_TaxRate;
        TO.SurVal = m_Surval;
        TO.TaxId = m_Tax_Id;
        TO.ItemRemarks = Item_Remarks;
        TO.ItemDiscount = m_ItemDiscount;
        TO.EditSaleRate = m_EditSaleRate;      
        ProductCollection.push(TO);
       
        Bindtr();
      


    }


    function GetServiceTax() {


        var TaxAmt = 0;

        
        $("#dvsertaxper").html(Sertax);
        var Dis = $("#dvdiscount").val();
     
     
        if (option == "TakeAway") { 
            if (Takeaway == "1") {
           
                TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
                $("#dvTax").html(TaxAmt.toFixed(2));


            }
            else {
                $("#dvsertaxper").html("0");
                TaxAmt == "0";
                $("#dvTax").html(TaxAmt.toFixed(2));
            }
        }
        else if (option == "Dine") {
       
            TaxAmt = (Number(m_Total) * Number(Sertax)) / 100;
            $("#dvTax").html(TaxAmt.toFixed(2));


        }


        return TaxAmt;

    }


     function GetKKC() {
         

         $("#dvKKCPer").html(KKC);
         
     
        if (option == "TakeAway") {
            if (Takeaway == "1") {

          

                KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
                $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


            }
            else {
             
                $("#dvKKCPer").html("0");
                KKCTAmt == "0";
                $("#dvKKCAmt").html(0.00);
            }
        }
        else if (option == "Dine") {
      
            KKCTAmt = (Number(m_Total) * Number(KKC)) / 100;
            $("#dvKKCAmt").html(KKCTAmt.toFixed(2));


        }

   

        return KKCTAmt;

    }


     function GetSBC() {


        $("#dvSBCPer").html(SBC);
    
        if (option == "TakeAway") {
            if (Takeaway == "1") {
           
                SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
                $("#dvSBCAmt").html(SBCTAmt.toFixed(2));
             
     

            }
            else {
                $("#dvSBCPer").html("0");
                SBCTAmt == "0.00";
                $("#dvSbCAmt").html(SBCTAmt.toFixed(2));
                
            }
        }
        else if (option == "Dine") {
            SBCTAmt = (Number(m_Total) * Number(SBC)) / 100;
            $("#dvSBCAmt").html(SBCTAmt.toFixed(2));
           


        }


        return SBCTAmt;

    }




     function GetTable() {



        $.ajax({
            type: "POST",
            data: '{}',
            url: "KotRaiseBill.aspx/GetTable",
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {

                var obj = jQuery.parseJSON(msg.d);

        
                $("#products").html(obj.productData);

              

            },
            error: function (xhr, ajaxOptions, thrownError) {

                var obj = jQuery.parseJSON(xhr.responseText);
                alert(obj.Message);
            },
            complete: function () {

            }

        }
       );


    }

    

    //........................................

	
	$(document).on("keyup", "input[name='txtBillQty']", function (event) {

       var keycode=(event.keyCode ? event.keyCode: event.which);
              
     if(keycode=='13')
    {
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
         

			
			var fQty=$(this).val();
		 if(isNaN(fQty))
		 {
		 fQty=1;
		 
		 }
        ProductCollection[RowIndex]["Qty"] =fQty;

        Bindtr();

          $("#txtItemCode").focus();
         }

    });


    $(document).on("change", "#txtBillPrice", function (event) {

        
        var RowIndex = Number($(this).closest('tr').index());
        var PId = ProductCollection[RowIndex]["ItemId"];

        var Mode = "Plus";

        var Qty = ProductCollection[RowIndex]["Qty"];
        var Price = ProductCollection[RowIndex]["Price"];
         

			
			var fPrice=$(this).val();
		
        ProductCollection[RowIndex]["Price"] =fPrice;

        Bindtr();

    });

	
	
   


   



    //............................................

    </script>

     <link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script>
        $(function () {
            //            $('#txtddlMobSearchBox,#txtItemCode').keyboard();



            //                $('#txtItemCode').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '1 2 3 4 5',
            //    '6 7 8 9 0',
            //    ' {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 10,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();




            //                $('#txtCashReceived').keyboard({
            //                    layout: 'custom',
            //                    customLayout: {
            //                        'default': [
            //    '9 8 7 6 5',
            //    '4 3 2 1 0',
            //    ' . {bksp}',
            //    '{a} {c}'
            //   ]
            //                    },
            //                    maxLength: 6,
            //                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            //                    useCombos: false // don't want A+E to become a ligature
            //                }).addTyping();


        });
	</script>









    
 <div id="dvAddOn"></div>
<div class="right_col" role="main">
                <div class="">

                
                    <div class="clearfix"></div>

                    <div class="row">
                    
                       <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>Bills List</small></h2>
                                    
                                    <div class="clearfix"></div>
                                </div>
                               <%-- <div class="x_content" style="background:seashell;margin-bottom:10px">

                                <div id="dvSave">SAVE
                                    </div>
                                
                                </div>--%>
                                 <div class="form-group"  margin-bottom:10px">

                                 <div class="form-group">
                                
                                <table>
                                <tr><td style="font-size:20px">Date From:</td><td style="font-size:20px">
                         
                                <input type="text" readonly="readonly"   class="form-control input-small" style="width:130px;background-color:White;font-size:20px;height:37px;"  id="txtDateFrom" aria-describedby="inputSuccess2Status" />
 
                                </td><td>
                                
                                </td>
                                <td style="font-size:20px">Date To:</td><td style="font-size:20px"><input type="text" readonly="readonly"  class="form-control input-small" style="width:130px;background-color:White;font-size:20px;height:37px;"    id="txtDateTo" aria-describedby="inputSuccess2Status" />
                              
                                </td>
                                <td>
                                    
                                    <div class="btn btn-primary btn-small" id="btnGo" style="font-size:20px">
                                 <i class="fa fa-search"></i>
                                </div>
                                  </td>
                                </tr>
                                </table>

                <table id="jQGridDemoBill">
                </table>
                <div id="jQGridDemoPagerBill">
                </div>

                </div>
                                </div>


                                   
                                        <div class="form-group">
                                            <div class="col-md-9 col-sm-9 col-xs-12   ">
                                                <table>
                                                    <tr><td><div class="btn btn-primary" style="display: block;height:50px;width:100px;font-size:20px;" id="btnShowScreen">
                                           <i class="fa fa-external-link"></i> New</div></td>
                                           <td><div class="btn btn-success" style="display:none;height:50px;width:100px;font-size:20px;" id="btnEdit">
                                           <i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                           
                                           <td><div class="btn btn-danger" style="display: block;height:50px;width:112px;font-size:20px;" id="btnReprint">
                                           <i class="fa fa-edit m-right-xs"></i> Reprint</div></td>
                                            <td >
                                        <div id="btnDelete" style="display:none;height:50px;width:130px;font-size:20px;" class="btn btn-danger">
                                           <i class="fa fa-trash m-right-xs"></i>Cancel Bill</div>
                                    </td>
                                           </tr>

                                                </table>

                                                
                                                
                                            

                                                
                                            
                                       
                                            </div>
                                        </div>
                                </div>
                                </div>

                    </div>

                     








                    
                    <script type="text/javascript">

                        jQuery.fn.putCursorAtEnd = function () {

                            return this.each(function () {

                                // Cache references
                                var $el = $(this),
        el = this;

                                // Only focus if input isn't already
                                if (!$el.is(":focus")) {
                                    $el.focus();
                                }

                                // If this function exists... (IE 9+)
                                if (el.setSelectionRange) {

                                    // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
                                    var len = $el.val().length * 2;

                                    // Timeout seems to be required for Blink
                                    setTimeout(function () {
                                        el.setSelectionRange(len, len);
                                    }, 1);

                                } else {

                                    // As a fallback, replace the contents with itself
                                    // Doesn't work in Chrome, but Chrome supports setSelectionRange
                                    $el.val($el.val());

                                }

                                // Scroll to the bottom, in case we're in a tall textarea
                                // (Necessary for Firefox and Chrome)
                                this.scrollTop = 999999;

                            });

                        };

                        (function () {

                            var searchInput = $("#search");

                            searchInput
    .putCursorAtEnd() // should be chainable
    .on("focus", function () { // could be on any event
        searchInput.putCursorAtEnd()
    });

                        })();


                        $(document).ready(function () {


                            if ($("#ddlbillttype").val() == "Cash") {
                                var customerId = 0;
                                CrdCustSelId = 0;
                                CrdCustSelName = "";
                                $("#hdnCreditCustomerId").val(0);
                                $("#lblCreditCustomerName").text("");
                                CrdCustSelName = "";
                                $("#ddlCreditCustomers").html("");
                                $("#txtddlCreditCustomers").val("");

                                $("#lblCashHeading").text("Cash Rec:");
                                $("#creditCustomer").css("display", "none");

                                $("#dvOuter_ddlCreditCustomers").hide();

                                $("#ddlChosseCredit").hide();
                                $("#ddlChosseCredit").val(0);

                                $("#txtCashReceived").val("0").prop("readonly", false);
                                $("#Text13").val("0").prop("readonly", false);
                                $("#Text14").val("").prop("readonly", false);
                                $("#Text15").val("0").prop("readonly", false);
                                $("#Text16").val("").prop("readonly", false);
                                $("#ddlType").prop("disabled", false);
                                $("#ddlBank").prop("disabled", false);


                            }


                            $("#txtDateFrom,#txtDateTo,#txtKotDate").val($("#<%=hdnDate.ClientID%>").val());
                            BindGrid();



                            $("#btnGo").click(
                     function () {

                         BindGrid();

                     }
                      );




                            $('#txtDateFrom').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });

                            $('#txtKotDate').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });


                            $('#txtDateTo').daterangepicker({
                                singleDatePicker: true,
                                calender_style: "picker_1"
                            }, function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                            });
                        });
                    </script>


                    

 
                </div>
                <!-- /page content -->

                <!-- footer content -->
                <footer>
                    <div class="">
                       <%-- <p class="pull-right">Gentelella Alela! a Bootstrap 3 template by <a>Kimlabs</a>. |
                            <span class="lead"> <i class="fa fa-paw"></i> Gentelella Alela!</span>
                        </p>--%>
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->

            </div>


<div id="dvbillSave" style="float: left; display:none; position:absolute;z-index:1000;left:340px;width:300px;bottom:240px;background-color: #293C52;
                                                                color: white; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                                <table style="width:100%;">
                                                                    <tr style="text-align:center">
                                                                    <td style="text-align:center">BILL SAVED SUCCESSFULLY</td>
                                                                        
                                                                    </tr>
                                                                    <tr><td style="text-align:center;font-size:20px">AMOUNTED Rs.<asp:Label ID ="lblAmmmt" runat="server" ClientIDMode ="Static"></asp:Label></td></tr>
                                                                    <tr><td>
                                                                    <div class="button1" style="background-color:White;color:Black;width:60px;box-shadow: -2px -5px 5px transparent;margin-left:41%;height:40px;"  id="btnMsgClose">
                                    OK</div>
                                                                   </td></tr>
                                                                    
                                                                  
                                                                </table>
                                                            </div>


<div class="row" id="CustomerDialog" style="display:none">
<div class="form-horizontal form-label-left input_mask">
<uc3:AddCashCustomer ID="ucAddCashCustomer" runat="server" />
</div>


</div>

<div id="screenDialog" style="background: url('images/nikbg2.jpg');display:none" title = <%=m_getdate %>>

    <input type="hidden" id="hdnnetamt" value="0" />
     <input type="hidden" id="hdnsteward" value="0" />
    <input type="hidden" id="hdnCreditCustomerId" value="0" />
	<asp:HiddenField ID = "hdndd" runat="server" ClientIDMode = "Static" />
    <div class="container">
        <div class="row">
            <div class="nav1" style="padding-top:0px;margin-top:-5px;display:none"> 
                <ul id="categories">
                </ul>
            </div>
        </div>
        <div class="row">

         <div class="col-xs-12">
         <div class ="col-xs-12">

                
                <div class="Search" style="padding-left:10px">
                    <div class="input-group">
                        <table width="100%">
                          <tr><td style="color:White;font-weight:bold;font-size:17px;">BillNo</td><td style="padding-left:5px;font-size:17px;"><input type="text" id ="txtKotBillNo" disabled="disabled" style="width:100px"  value="Auto" /></td><td style="color:White;font-weight:bold;padding-left:10px;font-size:17px;">BillDate</td><td style="padding-left:10px;font-size:17px;"><input type="text" disabled="disabled" id ="txtKotDate" style="width:100px;font-size:17px;" /></td><td style="color:White;font-weight:bold;padding-left:10px;font-size:px;">Cust Id:</td><td style ="padding-left:10px;font-size:22px;"><input type="text" disabled="disabled" id ="txtcustId" style="width:87px;font-size:17px;"  /></td><td style="color:White;font-weight:bold;padding-left:10px;font-size:17px;">Cust Name:</td><td style="padding-left:10px;font-size:17px;">  <select id="ddlMobSearchBox"> </select></td></tr>
                          <tr><td style="color:White;font-weight:bold;font-size:17px;">Pax No:</td><td style="padding-left:5px;font-size:17px;"><input type="text"  id ="txtPax" style="width:100px"  /></td><td style="color:White;font-weight:bold;padding-left:10px;font-size:17px">Steward</td><td style="padding-left:10px;font-size:17px;"><input type="text" id ="txtkotsteward" style="width:100px;font-size:17px;"  /></td>
                           <td>  </td> <td colspan="2" rowspan="2"><div style="width:100%;margin-top:5px; background-color:black; text-align:center;color:white;font-size:17px;font-weight: bold;">
                                                <table width="100%" id="CashCustomer" style="display: none; border: dashed 0px silver;
                                                    border-collapse: separate; border-spacing: 1" cellpadding="2"; margin:0 auto !important;>
                                                    <tr>
                                                       

                                                    </tr>
                                                    <tr>
                                                        <td>
                                                         
                                                            
                                                            
                                                            <label id="lblCashCustomerName" style="font-weight: normal;
                                                                    margin-top: 1px"></label>
                                                        </td>
                                                        <td style="padding-left:50px"> <img src="images/close-icon1.png" title="Remove Customer" style="cursor:pointer" onclick='javascript:ResetCashCustmr()' /></td>
                                                        <%--<td   ><label  id="lblCashCustomerAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                                    </tr>

                                                      
                                                </table>
                                                   </div>  </td>
                          
                          
                          </tr>
                          <tr><td style="color:White;font-weight:bold;font-size:17px;">TableNo</td><td style="padding-left:5px;padding-top:5px"><input type="text" id ="txtKotTable" style="width:100px;font-size:17px;"  /></td><td><label style="color:White;font-size:17px" id="lblchhk">TakeAway</label> </td><td style="color:White;font-weight:bold;padding-left:10px"> <input type="checkbox" data-index="27" style="font-size:17px;width:25px;height:25px;padding-top:5px" id="chktakeway" /></td>  <td colspan="4" style="padding-left:10px"></td></tr>
                        </table>
                    </div>
                </div>
                </div>

                </div>
                

                <div class="col-xs-12" style="display:none;margin-bottom:5px" id="colProducts">
              
                    </div>





             <div class="col-xs-12">
             <div class="col-xs-3">
               <div id="products" style="overflow-y: scroll; height: 353px; background: #2A1003">
                </div>
             </div>
            <div class="col-xs-6" style="padding-left: 0px" id="colDvProductList">
                <div id="dvProductList" style="display: block;margin-top:-10px">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 200px; text-align: center; color: #FFFFFF;">
                                    Name
                                </th>

                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Qty
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Amt
                                </th>
                                
                                  <th style="width: 200px;text-align:center;color: #FFFFFF;display:none">
                                    Add-On
                                </th>


                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                              
                               
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll;">
                        <table style="width: 100%; font-size: 10px" id="tbProductInfo">
                            <tr style="border-bottom: 0px">
                                <td colspan="100%" style="text-align: center; font-weight: bold; font-size: 19px">
                                    ITEM(S) WILL BE DISPLAYED HERE
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="CustomerSearchWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        CUSTOMER SEARCH
                    </div>
                    <div class="cate">
                        <div id="dvSearch">
                            <table>
                                <tr>
                                    <td>
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith" value="S" name="searchcriteria" />
                                                    <label for="rbStartingWith" style="font-weight: normal">
                                                        Start With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining" checked="checked" name="searchcriteria" />
                                                    <label for="rbContaining" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith" value="E" name="searchcriteria" />
                                                    <label for="rbEndingWith" style="font-weight: normal">
                                                        End With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact" name="searchcriteria" value="EX" />
                                                    <label for="rbExact" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="text" class="form-control input-small" placeholder="Enter Customer Name"
                                                        style="width: 196px; padding: 5px; margin-right: 5px; margin-bottom: 5px; margin-top: 5px"
                                                        id="txtSearch1" />
                                                </td>
                                                <td>
                                                    <div id="btnSearch1" class="btn btn-primary btn-small">
                                                        Search</div>
                                                </td>
                                                <td style="padding-left: 5px">
                                                    <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                        Close</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="jQGridDemo">
                                        </table>
                                        <div id="jQGridDemoPager">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="dvBillWindow" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Billing Window
                    </div>
                    <div class="cate">
                        <table style="border-collapse: separate; border-spacing: 1;font-size:17px">
                            <tr>
                                <td style="width:130px">
                                    Amount:
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>

                               <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" id="txtFinalBillAmount" />
                                </td>
                                <td>
                                    Bill Type:
                                </td>
                                <td>
                                    <select id="ddlbillttype" style="height: 27px; width: 120px; padding-left: 0px" class="form-control">
                                        
                                        
                                        <option value="Cash">Cash</option>
                                        <option value="Credit">Credit</option>

                                        <option value="CreditCard">Credit Card</option>
                                         <option value="OnlinePayment">Online Payment</option>

                                    </select>
                                </td>
                                <td>

                                  <select id="ddlCreditCustomers" class="form-control" style=" display: inline-block;
    float: left;
    font-size: 11px;
    min-width: 0;
    padding: 0;
    width: 222px;
">
                                             <option value="0"></option>
                                             </select>

                                </td>
                                          
                                 <td> <asp:DropDownList id="ddlChosseCredit" ClientIDMode="Static" runat="server"  style=" display: inline-block; font-size: 11px;
    font-weight: normal;
    height: 27px;
    padding-left: 0;
    width: 132px;
" class="form-control">
                                           
                                             </asp:DropDownList></td>
                                        </tr>
                                      
                                    </table>

                                </td>
                               
                                
                            </tr>
                             
                            <tr>
                                <td>
                                    <label id="lblCashHeading" style="font-weight: normal">
                                        Cash Rec:</label>
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                            <td>



                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="txtCashReceived" />
                                            </td>

                                            <td>

                                                  <table width="100%" id="creditCustomer" style="padding:0px 5px 0px 5px ;background:#2a3f54;color:white;display: none; border: dashed 1px silver;
                                        border-collapse: separate; border-spacing: 1" cellpadding="2">
                                        <tr>
                                            <td>
                                                <label id="lblCreditCustomerName" style="font-weight: normal; margin-top: 1px">
                                                </label>
                                            </td>
                                            
                                        </tr>
                                    </table>

                                            </td>




                                        </tr>

                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Cr.Card Amount
                                </td>
                                <td colspan="100%">
                                    <table>
                                        <tr>
                                                <td>
                                    <input type="text" class="form-control input-small" style="width: 70px" value="0"
                                        id="Text13" />
                                </td>
                                <td>
                                    Type:
                                </td>
                                <td>
                                    <select id="ddlType" style="height: 25px; width: 90px" class="form-control">
                                        <option></option>
                                        <option value="Visa">VISA</option>
                                        <option value="Maestro">MAESTRO</option>
                                        <option value="Master">MASTER</option>
                                    </select>
                                </td>


                                        </tr>

                                    </table>


                                </td>

                            
                            </tr>
                            <tr>
                                <td>
                                    Cc No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text14" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Bank:
                                </td>
                                <td>
                                     <select id="ddlBank" style="height: 25px; width: 90px" class="form-control">
                                    </select>

                                    <input type="text" class="form-control input-small" style="width: 70px;display:none" id="Text15"
                                        value="0" />
                                </td>
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td>
                                    Cheque No:
                                </td>
                                <td colspan="100%">
                                    <input type="text" class="form-control input-small" style="width: 200px" id="Text16" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 64px">
                                                Balance:
                                            </td>
                                <td colspan="100%">
                                    <table style="border-collapse: separate; border-spacing: 1;">
                                        <tr>
                                            
                                            <td style="width: 90px">
                                                <input type="text" class="form-control input-small" style="width: 90px" id="txtBalanceReturn"
                                                    readonly="readonly" />
                                            </td>
                                            <td colspan="100%">
                                                <table cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <div id="btnBillWindowOk" class="btn btn-primary btn-small" style="margin: 2px">
                                                                Generate Bill</div>
                                                        </td>
                                                        <td>
                                                            <div id="btnBillWindowClose" class="btn btn-primary btn-small"  style="margin: 2px">
                                                                Close</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvCreditCustomerSearch" style="display: none">
                    <div class="leftside" style="padding-left: 10px; font-weight: bold">
                        Credit Customers Search
                    </div>
                    <div class="cate">
                        <table width="100%">
                            <tr>
                                <td>
                                    <div id="dvLeft1" style="border: 1px solid silver">
                                        <table cellpadding="5" cellspacing="0" width="100%" style="background: silver;">
                                            <tr>
                                                <td style="padding: 5px">
                                                    <input type="radio" id="rbPhoneNo1" value="M" checked="checked" name="searchon1" />
                                                    <label for="rbPhoneNo1" style="font-weight: normal">
                                                        Phone No.</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbCustomerName1" value="N" name="searchon1" />
                                                    <label for="rbCustomerName1" style="font-weight: normal">
                                                        Customer Name</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="dvRight1" style="float: left; width: 100%; border: 1px solid silver; display: none">
                                        <table cellpadding="5" cellspacing="0" width="100%">
                                            <tr style="background-color: #E6E6E6">
                                                <td colspan="100%">
                                                    Search Criteria:
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <input type="radio" id="rbStartingWith1" value="S" name="searchcriteria1" />
                                                    <label for="rbStartingWith1" style="font-weight: normal">
                                                        Starting With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" value="C" id="rbContaining1" checked="checked" name="searchcriteria1" />
                                                    <label for="rbContaining1" style="font-weight: normal">
                                                        Containing</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbEndingWith1" value="E" name="searchcriteria1" />
                                                    <label for="rbEndingWith1" style="font-weight: normal">
                                                        Ending With</label>
                                                </td>
                                                <td>
                                                    <input type="radio" id="rbExact1" name="searchcriteria1" value="EX" />
                                                    <label for="rbExact1" style="font-weight: normal">
                                                        Exact</label>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" class="form-control input-small" style="width: 190px" placeholder="Enter Search Keyword"
                                                    id="Txtsrchcredit" />
                                            </td>
                                            <td>
                                                <div id="btncreditsrch" class="btn btn-primary btn-small">
                                                    Search</div>
                                            </td>
                                            <td style="padding-left: 5px">
                                                <div onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Close</div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table id="jQGridDemoCredit">
                                    </table>
                                    <div id="jQGridDemoPagerCredit">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                             <td colspan = "100%">
                             <table>
                             <td>  <div id="btnOk" class="btn btn-primary btn-small">
                                                    Ok</div></td>

                                                     <td style="padding-left: 5px">
                                                <div id="btnCancel" onclick="javascript:OpenProductWindow();" class="btn btn-primary btn-small">
                                                    Cancel</div>
                                            </td>
                             </table>
                                              
                                           
                                           
                            </tr>
                        </table>
                    </div>
                </div>
                <div id="dvHoldList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Hold No
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 120px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 70px; text-align: center; color: #FFFFFF;">
                                    Price
                                </th>
                                <th style="width: 30px; text-align: center; color: #FFFFFF;">
                                </th>
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 17px" id="tbHoldList">
                        </table>
                    </div>
                </div>

                 <div id="dvOrderList" style="display: none">
                    <div class="leftside">
                        <table style="width: 100%">
                            <tr>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Order No
                                </th>
                                <th style="width: 80px; text-align: center; color: #FFFFFF;">
                                    Date
                                </th>
                                <th style="width: 100px; text-align: center; color: #FFFFFF;">
                                    Customer
                                </th>
                                <th style="width: 50px; text-align: center; color: #FFFFFF;">
                                    Table
                                </th>
                                <th style="width:130px; text-align: center; color: #FFFFFF;">
                                </th>
                                 
                            </tr>
                        </table>
                    </div>
                    <div class="cate" style="min-height: 220px; max-height: 220px; overflow-y: scroll">
                        <table style="width: 100%; font-size: 10px" id="tbOrderList">
                        </table>
                    </div>
                </div>



                <div class="cate2" style="padding:0px;margin-top:0px; background: #F5FFFA">
                    <table id="tbamountinfo" style="width: 100%; text-align: right; background: #F5FFFA;padding:0px;font-size:17px;margin-left:178px">
                        <tr>
                            <td valign="top" style="padding-top:10px">
							<asp:Literal id="ltDateTime" runat="Server" Visible="false"/>

                                   
                                <div>
                                <table>
                                     <tr><td>No Of Items: </td><td colspan="100%">
                            
                              <div style="text-align:justify;width:60%;margin:0px 0px 0px;border-left:0px;border-top:0px;padding:5px">
                                  <asp:Label ID ="lblNoItems"  runat="server" ClientIDMode ="Static"></asp:Label>
                                  </div>
                            
                                           </td>
                        </tr>

                                </table>

                                </div>

                            </td>

                            
                            
                                           
                                </table>
                            </div>


                    <table>
                      
                 
                        <tr>
                           
                            <td>
                            <div class="button1" id="btnCash" style="width:70px;font-size:18px;">
                                   Save</div>
                          
                               
                            </td>
                            

                             <td>
                               <div class="button3" id="btnClear" style="width:75px;font-size:18px;">
                                    Exit </div>
                            </td>


                             <td rowspan = "2" style="display:none">
                               <div class="button1" id="btnOrdersList" style="height:105px">
                        Show Customers Bills
                        </div>
                            </td>
                           
                        </tr>
                        <tr>
                        <td colspan ="3">
                       
                        </td>
                        </tr>
                        <tr>
                            <td colspan="100%">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="dvTaxDenomination" style="float: left;display: none; background-color: #DBEAE8;
                                                color: #63a69a; bottom: 0px; right: 0px; border: solid 1px silver; border-radius: 10px;
                                                padding: 5px; -webkit-box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;
                                                box-shadow: inset 1px 1px 1px rgba(0, 0, 0, 0.075), -1px -1px 10px black;">
                                                <table>
                                                    <tr>
                                                        <td colspan="2">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    </td>
                                                                    <td>
                                                                        Tax Denomination:
                                                                    </td>
                                                                    <td>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Tax
                                                        </td>
                                                        <td>
                                                            VatAmt
                                                        </td>
                                                        <td style="width: 70px">
                                                            Vat
                                                        </td>
                                                        <td>
                                                            SurChg
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="gridTax" colspan="100%">
                                                            <asp:Repeater ID="gvTax" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td>
                                                                            <div name="tax">
                                                                                <%#Eval("Tax_Rate")%></div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='amt_<%#Eval("Tax_ID") %>' name="amt">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='vat_<%#Eval("Tax_ID") %>' name="vat">
                                                                                0.00</div>
                                                                        </td>
                                                                        <td>
                                                                            <div myid='sur_<%#Eval("Tax_ID") %>' name="sur">
                                                                                0.00</div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            <div class="col-xs-3" style="background-color:#F5FFFA">
            <td valign="top" align="right" style="padding-right:10px" >


    

                                                    <div style="border:solid 1px silver;display:none; background-color:#F5FFFA; box-shadow:2px 2px 3px silver;width:110%;margin:0px 0px 8px;border-left:0px;border-top:0px;padding:5px;">
                                                    <table>
                                                      <tr>
                            <td colspan="100%" align="right" style="padding-right:10px">
                                <table>
                                    <tbody>
                                        <tr>  <td><b>Choose Service</b></td>
                                              </tr>
                                        
                                        <tr>
                                            <td><b><asp:DropDownList ID="ddloption" runat="server" 
                                                    Style="width: 115px; height:25px;
                                                    border: solid 1px silver">
                                                    <asp:ListItem Text="Take Away" Selected Value="TakeAway"></asp:ListItem>
                                                    <asp:ListItem Text="Dine" Value="Dine"></asp:ListItem>
                                                    <asp:ListItem Text="HomeDelivery" Value="HomeDelivery"></asp:ListItem>
                                                </asp:DropDownList></b>
                                                 <b><select id="ddlEmployees" style="float:right;height: 25px; width: 86px;margin:0 0 0 5px; display:none" class="form-control"></select></b>  <b><select id="ddlTable" style="height: 25px; width: 115px" class="form-control"></select></b>
                                                
                                            </td>
                                            <%--<td ><label  id="lblCashCustmorName" style="font-weight:normal;margin-top:1px"   ></label></td><td ><label  id="lblCustmorAddress" style="font-weight:normal;margin-top:1px"   ></label></td>--%>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                                                    </table>
                                                    </div>
                                <div style="border:solid 1px silver; background-color:#F5FFFA; box-shadow:2px 2px 3px silver;width:110%;margin:0px 0px 8px;border-left:0px;border-top:0px;padding:5px;height:340px">

                                
                                <table  id="myCustomTable" style="font-size:17px">

                                <tr>
                                        <td style="padding-right: 10px;height: 25px;  text-align: left" text>
                                            HomeDel:
                                        </td>
                                        <td style="width: 100px; text-align: left;" colspan="100%">
                                            <input type="checkbox" id = "chkhone" style="height:20px;width:20px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;  text-align: left"   >
                                            Employee:
                                        </td>
                                        <td style="width: 100px; text-align: left;" colspan="100%">
                                          <asp:DropDownList ID ="ddlsteward" runat="server" Enabled = "false"></asp:DropDownList>
                                        </td>
                                    </tr>
                               


                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;  text-align: left" text>
                                            Amount:
                                        </td>
                                        <td style="width: 100px; text-align: left;" colspan="100%">
                                            <div id="dvsbtotal">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;   text-align: left">
                                            Discount:
                                        </td>
                                        <td colspan="100%">
                                            <table>
                                                <tr>

<td style="width: 30px; text-align: left;">
                                        <input type="text" class="form-control input-small" style="width:45px;height:29px" 
                                                    id="dvdisper" value ="0"/>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: left;"  colspan="3">
                                           <input type="text" class="form-control input-small" style="width: 45px;height:29px"  
                                                    id="dvdiscount" value ="0" />
                                        </td>
                                                </tr>

                                            </table>

                                        </td>


                                        
                                    </tr>
                                    <tr id ="trServicetax">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;    text-align: left">
                                            ServiceTax:
                                        </td>
                                        <td style="width: 30px; text-align: left;"  colspan="3">
                                            <div id="dvsertaxper">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="50%">
                                            <div id="dvTax">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trKKC">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;    text-align: left">
                                            KKC:
                                        </td>
                                        <td style="width: 30px; text-align: left;"  colspan="3">
                                            <div id="dvKKCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="50%">
                                            <div id="dvKKCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="trSBC">
                                        <td style="width: 200px;padding-right: 10px;height: 25px;  text-align: left">
                                            SBC:
                                        </td>
                                        <td style="width: 30px; text-align: left;"  colspan="3">
                                            <div id="dvSBCPer">
                                            </div>
                                        </td>
                                        <td>
                                            %
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="50%">
                                            <div id="dvSBCAmt">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr id ="vatIncOrExc">
                                        <td style="padding-right: 10px;height: 25px;   text-align: left">
                                            Vat Amount:
                                        </td>
                                        <td style="width: 100px; text-align: left;"  colspan="100%">
                                            <div id="dvVat">
                                            </div>
                                        </td>
                                    </tr>

                                     <tr id ="DelCharges" style="display:none">
                                        <td style="padding-right: 10px;height: 25px; text-align: left">
                                            Delv Chrgs:
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="100%">
                                           
                                             <input type="text" class="form-control input-small" style="width: 45px;height:25px"  
                                                    id="dvDeliveryChrg" value ="0" />

                                                    </td>
                                                    </tr>
                                           
                                     
                                     <tr>
                                        <td style="padding-right: 10px;height: 25px;  text-align: left">
                                            RoundOff:
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="100%">
                                            <div id="dvRound">
                                            </div>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td style="padding-right: 10px;height: 25px;   font-weight: bold; text-align: left">
                                            Net Amt:
                                        </td>
                                        <td style="width: 100px; text-align: left"  colspan="100%">
                                            <div id="dvnetAmount">
                                            </div>
                                        </td>
                                    </tr>
                                     



                                     </table>
                                            </div>
                                            </td>
            </div>


            
            </div>
            
        </div></div>
    </div>


            
    </form>

     
  <script language="javascript">



      function BindGrid() {
          var DateFrom = $("#txtDateFrom").val();
          var DateTo = $("#txtDateTo").val();
          jQuery("#jQGridDemoBill").GridUnload();

          jQuery("#jQGridDemoBill").jqGrid({
              url: 'handlers/ManageBills.ashx?dateFrom=' + DateFrom + '&dateTo=' + DateTo,
              ajaxGridOptions: { contentType: "application/json" },
              datatype: "json",

              colNames: ['BillNo', 'BillNowPrefix', 'BillDate', 'CustomerId', 'CustomerName', 'BillValue', 'DiscountAmt', 'Tax', 'NetAmount', 'BillMode', 'CreditBank', 'UserNo', 'BillType', 'CashAmt', 'CreditAmt', 'CreditCardAmt', 'CashCustCode', 'CashCustName', 'RoundAmt', 'Passing', 'Printed', 'TaxPer', 'GodownId', 'ModifiedDate', 'RAmt', 'TokenNo', 'TableNo', 'Remarks', 'Servalue', 'GRNNo', 'EmpCode', 'DisPer','CashCustAddr','CreditCustAddr','DeliveryCharges','KKCPer','KKCAmt','SBCPer','SBCAmt','EmpName'],
              colModel: [
                { name: 'Bill_No', index: 'Bill_No', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                           { name: 'BillNowPrefix', key: true, index: 'BillNowPrefix', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                           { name: 'strBD', index: 'strBD', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },

                          { name: 'Customer_ID', index: 'Customer_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Customer_Name', index: 'Customer_Name', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'Bill_Value', index: 'Bill_Value', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                         // { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true ,editrules: { required: true }},
                           { name: 'Less_Dis_Amount', index: 'Less_Dis_Amount', width: 150, stype: 'text', sortable: true, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Add_Tax_Amount', index: 'Add_Tax_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: false, editable: true, editrules: { required: true } },
                           { name: 'Net_Amount', index: 'Net_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'BillMode', index: 'BillMode', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                             { name: 'CreditBank', index: 'CreditBank', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'UserNO', index: 'UserNO', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'Bill_Type', index: 'Bill_Type', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },

                            { name: 'Cash_Amount', index: 'Cash_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Credit_Amount', index: 'Credit_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'CrCard_Amount', index: 'CrCard_Amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                          { name: 'CashCust_Code', index: 'CashCust_Code', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCust_Name', index: 'CashCust_Name', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Round_Amount', index: 'Round_Amount', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'Passing', index: 'Passing', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Bill_Printed', index: 'Bill_Printed', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'Tax_Per', index: 'Tax_Per', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'Godown_ID', index: 'Godown_ID', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'strMD', index: 'strMD', width: 50, stype: 'text', searchoptions: { sopt: ['eq'] }, sortable: true, hidden: true },
                             { name: 'R_amount', index: 'R_amount', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'tokenno', index: 'tokenno', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'tableno', index: 'tableno', width: 150, stype: 'text', sortable: true, hidden: false, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'remarks', index: 'remarks', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'servalue', index: 'servalue', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                             { name: 'ReceiviedGRNNo', index: 'ReceiviedGRNNo', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                          { name: 'EmpCode', index: 'EmpCode', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'DiscountPer', index: 'DiscountPer', width: 150, stype: 'text', sortable: true, editable: true, hidden: true, editrules: { required: true } },
                           { name: 'CashCustAddress', index: 'CashCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                           { name: 'CreditCustAddress', index: 'CreditCustAddress', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'DeliveryCharges', index: 'DeliveryCharges', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCPer', index: 'KKCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'KKCAmt', index: 'KKCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCPer', index: 'SBCPer', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'SBCAmt', index: 'SBCAmt', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },
                            { name: 'EmpName', index: 'EmpName', width: 150, stype: 'text', sortable: true, hidden: true, editable: true, editrules: { required: true } },

              ],

              rowNum: 10,

              mtype: 'GET',
              toolbar: [true, "top"],
              loadonce: true,
              rowList: [10, 20, 30],
              pager: '#jQGridDemoPagerBill',
              sortname: 'BillNowPrefix',
              viewrecords: true,
              height: "100%",
              width: "1100px",
              sortorder: 'desc',
              ignoreCase: true,
              caption: "Bills List",




          });

          var $grid = $("#jQGridDemoBill");
          // fill top toolbar
          $('#t_' + $.jgrid.jqID($grid[0].id))
              .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
          $("#globalSearchText").keypress(function (e) {
              var key = e.charCode || e.keyCode || 0;
              if (key === $.ui.keyCode.ENTER) { // 13
                  $("#globalSearch").click();
              }
          });
          $("#globalSearch").button({
              icons: { primary: "ui-icon-search" },
              text: false
          }).click(function () {
              var postData = $grid.jqGrid("getGridParam", "postData"),
                  colModel = $grid.jqGrid("getGridParam", "colModel"),
                  rules = [],
                  searchText = $("#globalSearchText").val(),
                  l = colModel.length,
                  i,
                  cm;
              for (i = 0; i < l; i++) {
                  cm = colModel[i];
                  if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                      rules.push({
                          field: cm.name,
                          op: "cn",
                          data: searchText
                      });
                  }
              }
              postData.filters = JSON.stringify({
                  groupOp: "OR",
                  rules: rules
              });
              $grid.jqGrid("setGridParam", { search: true });
              $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
              return false;
          });




          $("#jQGridDemoBill").jqGrid('setGridParam',
      {

          onSelectRow: function (rowid, iRow, iCol, e) {



              var arrRole = [];
              arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


              $("#btnEdit").css({ "display": "none" });
              $("#btnDelete").css({ "display": "none" });
                $("#btnShowScreen").css({ "display": "none" });


                for (var i = 0; i < arrRole.length; i++) {

                    if (arrRole[i] == 9) {

                        $("#btnShowScreen").css({ "display": "block" });
                    }
                    if (arrRole[i] == 2) {

                        $("#btnDelete").css({ "display": "block" });
                    }
                    if (arrRole[i] == 3) {
                        $("#btnEdit").css({ "display": "block" });
                        m_BillNowPrefix = 0;
                        m_BillNowPrefix = $('#jQGridDemoBill').jqGrid('getCell', rowid, 'BillNowPrefix');

                    }
                }




             


              var CashCustCode = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Code');
              var CashCustName = $('#jQGridDemo').jqGrid('getCell', rowid, 'CashCust_Name');
              //$("#ddlMobSearchBox").html("<option selected=selected value='" + CashCustCode + "'>" + CashCustName + "</option>");
            //  $("#txtddlMobSearchBox").val(CashCustName);

            //  $("#lblCashCustomerName").text($("#ddlMobSearchBox option:selected").attr("name") + " " + $("#ddlMobSearchBox option:selected").attr("address") + " " + $("#ddlMobSearchBox option:selected").attr("phone"));

              CshCustSelId = $("#ddlMobSearchBox option:selected").val();
              CshCustSelName = $("#ddlMobSearchBox option:selected").attr("name");

          }
      });





          $('#jQGridDemoBill').jqGrid('navGrid', '#jQGridDemoPagerBill',
                  {
                      refresh: false,
                      edit: false,
                      add: false,
                      del: false,
                      search: false,
                      searchtext: "Search",
                      addtext: "Add",
                  },

                  {//SEARCH
                      closeOnEscape: true

                  }

                    );


          var DataGrid = jQuery('#jQGridDemoBill');
          DataGrid.jqGrid('setGridWidth', '700');


      }
  </script>
               <iframe id="reportout" width="0" height="0" onload="processingComplete()"></iframe>
</asp:Content>

