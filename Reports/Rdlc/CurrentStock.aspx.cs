﻿
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Reports_CurrentStock : System.Web.UI.Page
{
    Connection conn = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

   
        if (!IsPostBack)
        {
            ddlItemType.DataSource = new RGodownsBLL().GetAllItemType();
            ddlItemType.DataTextField = "Item_Type";
            ddlItemType.DataValueField = "Item_TypeID";
            ddlItemType.DataBind();

            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();

            ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
            
            BindDepartents();
            BindBranches();
            BindGroups();
           
        }

        CheckRole();



  

       
    



    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

    void BindDepartents()
    {
        ltDepartments.Text = new RGodownsBLL().GetDepartmentHtml();
    }

    void BindGroups()
    {
        ltGroups.Text = new RGodownsBLL().GetGroupHtml();
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCURRENTSTOCK));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }
    public void Getdata()
    {
        string deptqry = "";
        int BranchId = 0;

        if (rdbAll.Checked == true)
        {
            deptqry = "All";
        }
        else
        {
            deptqry = hdnDept.Value;
        }

        string groupqry = "";

        if (rdbAllGroup.Checked == true)
        {
            groupqry = "All";
        }
        else
        {
            groupqry = hdnGroup.Value;
        }
        Int32 CompanyId = Convert.ToInt32(ddlcompany.SelectedValue);
        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);
        String ItemType = ddlItemType.SelectedValue;

        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("report_sp_CurrentStockReport", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@deptqry", deptqry.Trim());
            cmd.Parameters.AddWithValue("@groupqry", groupqry.Trim());
            cmd.Parameters.AddWithValue("@company", CompanyId);
            cmd.Parameters.AddWithValue("@godown", GodownId);
            cmd.Parameters.AddWithValue("@ItemType", ItemType.Trim());
            cmd.Parameters.AddWithValue("@BranchId", BranchId);
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\CurrentStockRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}