﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptDailySale.aspx.cs" Inherits="Reports_RptDailySale" %>

 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %><asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />

  <script type="text/javascript">
    $(document).ready(
function () {




}
);
  </script>
  
        <div style="text-align: center;"><h2>DAILY SALE REPORT</h2></div>
            
    <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   
        <table class="table">

          <tr>
            <td>Branch</td>
            <td><asp:DropDownList class="form-control" ID="ddlBranch" runat="server" placeholder="Choose Branch"> </asp:DropDownList></td>
            <td>POS</td>
            <td><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList></td>
              <td></td>
         </tr>
        <tr>

             <td>Date:</td>
                            <td><asp:TextBox ID="txtDateFrom" type="date" class="form-control"  runat="server" ></asp:TextBox>
                              
                            </td>
                       
                                 <td>To:</td>
                           <td><asp:TextBox ID="txtDateTo" type="date"  class="form-control"  name="All" runat="server" ></asp:TextBox></td>
      
             <td><asp:Button ID="Button1" runat="server"   CssClass="btn btn-success"  Text="Submit"  onclick="btnGetRecords_Click"/></td>
       <td><button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button></td>  
                         
        </tr>
    </table>
 

     <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ShowBackButton="true" ShowPrintButton="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" PageCountMode="Actual">

    </rsweb:ReportViewer>
</asp:Content>

