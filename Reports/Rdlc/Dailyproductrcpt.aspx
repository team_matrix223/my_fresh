﻿<%@ Page Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true"  CodeFile="Dailyproductrcpt.aspx.cs" Inherits="Dailyproductrcpt" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/DailyproductrcptNew.css" rel="stylesheet" />
    <script src="../js/jquery-1.9.0.min.js"></script>
            <script type="text/javascript">
				function myFunction1() {
					var input, filter, table, tr, td, i;
					input = document.getElementById("myInput1");
					filter = input.value.toUpperCase();
					table = document.getElementById("tblist");
					tr = table.getElementsByTagName("tr");
					for (i = 0; i < tr.length; i++) {
						td = tr[i].getElementsByTagName("td")[0];
						if (td) {
							if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
								tr[i].style.display = "";
							} else {
								tr[i].style.display = "none";
							}
						}
					}
				}
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
				    frameDoc.document.write('<html><head></head>');
				    frameDoc.document.write('<div  style = "text-align: center">Daily Product Receipt Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

   function RptHeadDetail() {
   var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
   return value;
         }
</script>
<script type="text/javascript">


    $(document).ready(
   function () {

       $("#td_1").hide();
       $("#myInput1").hide();
       $("#<%=rdbSelect.ClientID %>").click(function () {
           $("#myInput1").show();
           $("#td_1").show();
           $("#sel_table").show();
          
       });
       $("#<%=rdbAll.ClientID %>").click(function () {
           $("#myInput1").hide();
           $("#td_1").hide();
           $("#sel_table").hide();
         
       });

       $("input[name ='group']").click(function () {
           var CompaniesId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnsale.ClientID %>").val(CompaniesId);


       });







       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }


    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {


            $("#<%=rdbAll.ClientID %>").prop('checked', false);


        }


    });


   });




</script>
<script>
$(document).ready(function(){
	$(".daily-prod-selected-btn input").click(function () {
		$(".sale-report-table").addClass("sale-btm-bdr");
	});
	$(".daily-prod-all-btn input").click(function () {
		$(".sale-report-table").removeClass("sale-btm-bdr");
	});
});
</script>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />

    
           <div style="text-align: center;"><h2 class="daily-prod-rcpt-head">Daily Product Receipt Report</h2></div>
<div class="daily-prod-rcpt-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 daily-prod-rcpt-cst-col branch-col">
		<div class="daily-prod-rcpt-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-5 col-sm-5 col-xs-12 daily-prod-rcpt-cst-col date-col">
		<div class="daily-prod-rcpt-date-section">
			<div class="daily-prod-rcpt-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control" runat="server"></asp:TextBox>
			</div>
			<div class="daily-prod-rcpt-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" type="date" class="form-control" name="All" runat="server"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 daily-prod-rcpt-cst-col all-col">
		<div class="daily-prod-rcpt-radio-section">
			<asp:RadioButton ID="rdbAll" name="All" CssClass="daily-prod-all-btn" runat="server" Text="All" Checked="True" />
			<asp:RadioButton ID="rdbSelect" CssClass="daily-prod-selected-btn" runat="server" Text="Selected" />
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 daily-prod-rcpt-cst-col btn-col">
		<div class="daily-prod-rcpt-btn-section">
			<asp:Button ID="Button1" runat="server"   CssClass="btn btn-success"  Text="Submit"  onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>

<div class="sale_report">
         <table class="table sale-report-table">
           <%--   <tr>
                  <td>Branch:</td>
                  <td>
                      

                  </td>
                            <td>Date:</td>
                            <td>
                                
                              
                            </td>
                            <td>To:</td>
                            <td></td>
                    
                            <td></td> 
                            <td></td>  
                    <td></td>
       <td></td>
                        </tr>--%>
            
                 <tr>
                   <td colspan="2"  id="sel_table" style="display:none">
                    <table class="table">
                        <tbody>
                            <tr><td class="headings" colspan="2">Products:</td></tr>
                            <tr>
                                <td>
                                   <div style="height: 181px; overflow: auto;">
                                       <table id="tblist">
                                          <asp:Literal ID="ltProducts" runat="server"></asp:Literal>
                                       </table>
                                  </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
               </td>
                         

                       <td colspan="2" ><input type="text" id="myInput1"  class="form-control" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name" /></td>  
            
              
           </tr>

</table>
  
   <rsweb:ReportViewer ID="ReportViewer1" CssClass="daily-prod-rcpt-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ShowBackButton="true" ShowPrintButton="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="850px" PageCountMode="Actual">

    </rsweb:ReportViewer>


    </div>

   


        

</asp:Content>
