﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="creditsale.aspx.cs" Inherits="creditsale" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
  <link href="../css/CreditSaleNew.css" rel="stylesheet" />
   <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Credit Sale Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

                function RptHeadDetail() {
             var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>POS:' + $('#<%= dd_customername.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
             return value;
         }
</script>
    <div class="department_wise">

        <div style="text-align: center;" class="credit-sale-head"><h2>Credit Sale Report</h2></div>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
	<div class="credit-sale-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 credit-sale-cst-col branch-col">
		<div class="credit-sale-branch">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch">
               </asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 credit-sale-cst-col pos-col">
		<div class="credit-sale-pos">
			<span>POS:</span><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 credit-sale-cst-coltwo date-col">
		<div class="credit-sale-date-section credit-sale-cst-col date-inner-col">
			<div class="credit-sale-date">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control" runat="server"></asp:TextBox>
			</div>
			<div class="credit-sale-date credit-sale-date-to ">
				<span>To:</span><asp:TextBox ID="txtDateTo" type="date"  class="form-control"  runat="server"></asp:TextBox>
			</div>
		</div>
		<div class="credit-sale-button credit-sale-cst-col button-col">
			<asp:Button ID="btnGetRecords"  runat="server"  Text="Submit" CssClass="btn btn-success"  onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
	

	</div>



<div class="sale_report">
 
            <rsweb:ReportViewer ID="ReportViewer1" CssClass="credit-sale-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1100px">

    </rsweb:ReportViewer>
</div> 


</div>
</asp:Content>

