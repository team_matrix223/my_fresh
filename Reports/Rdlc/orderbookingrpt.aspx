﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="orderbookingrpt.aspx.cs" Inherits="orderbookingrpt" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/orderbookingrptnew.css" rel="stylesheet" />
       <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">
	   function Print() {
		   var report = document.getElementById("<%=ReportViewer1.ClientID %>");
		   var div = report.getElementsByTagName("DIV");
		   var reportContents;
		   for (var i = 0; i < div.length; i++) {
			   if (div[i].id.indexOf("VisibleReportContent") != -1) {
				   reportContents = div[i].innerHTML;
				   break;
			   }
		   }
		   var frame1 = document.createElement('iframe');
		   frame1.name = "frame1";
		   frame1.style.position = "absolute";
		   frame1.style.top = "-1000000px";
		   document.body.appendChild(frame1);
		   var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
		   frameDoc.document.open();
		   frameDoc.document.write('<html><head><title>RDLC Report</title>');
		   frameDoc.document.write('</head><body style = "font-family:arial;font-size:10pt;">');
		   frameDoc.document.write(reportContents);
		   frameDoc.document.write('</body></html>');
		   frameDoc.document.close();
		   setTimeout(function () {
			   window.frames["frame1"].focus();
			   window.frames["frame1"].print();
			   document.body.removeChild(frame1);
		   }, 500);
	   }
</script>
    <div class="department_wise">
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
         <div style="text-align: center;"><h2 class="oder-booking-head">Order Booking Report</h2></div>
		<div class="oder-booking-top-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 oder-booking-cst-col branch-col">
				<div class="order-booking-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 oder-booking-cst-col date-col">
				<div class="order-booking-date-section">
					<div class="order-booking-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control" runat="server" ></asp:TextBox>
					</div>
					<div class="order-booking-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo"  type="date" class="form-control" runat="server" ></asp:TextBox>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 oder-booking-cst-col pos-col">
				<div class="order-booking-pos-section">
					<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 oder-booking-cst-col btn-col">
				<div class="order-booking-btn-section">
					<asp:Button ID="btnGetRecords" runat="server"  CssClass="btn btn-success" Text="Submit" onclick="btnGetRecords_Click"/>
					 <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
				</div>
			</div>
		</div>
   <div class="sale_report">
       <%-- <table class="table">
          <tr>
              <td>Branch:</td>
              <td></td>
             <td>Date:</td>
             <td>
                
             </td>
             <td>To:</td>
             <td>
                
             </td>
			   <td>POS</td>
                <td></td>
               <td></td>
                 <td>  </td>
         </tr>
     </table>--%>
      <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="order-booking-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
    </div>


</div>
</asp:Content>

