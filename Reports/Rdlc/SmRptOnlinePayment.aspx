﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="SmRptOnlinePayment.aspx.cs" Inherits="Reports_SmRptOnlinePayment" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
 Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

       <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">
	   function Print() {
		   var report = document.getElementById("<%=ReportViewer1.ClientID %>");
		   var div = report.getElementsByTagName("DIV");
		   var reportContents;
		   for (var i = 0; i < div.length; i++) {
			   if (div[i].id.indexOf("VisibleReportContent") != -1) {
				   reportContents = div[i].innerHTML;
				   break;
			   }
		   }
		   var frame1 = document.createElement('iframe');
		   frame1.name = "frame1";
		   frame1.style.position = "absolute";
		   frame1.style.top = "-1000000px";
		   document.body.appendChild(frame1);
		   var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
		   frameDoc.document.open();
		   frameDoc.document.write('<html><head><title>RDLC Report</title>');
		   frameDoc.document.write('</head><body style = "font-family:arial;font-size:10pt;">');
		   frameDoc.document.write(reportContents);
		   frameDoc.document.write('</body></html>');
		   frameDoc.document.close();
		   setTimeout(function () {
			   window.frames["frame1"].focus();
			   window.frames["frame1"].print();
			   document.body.removeChild(frame1);
		   }, 500);
	   }
</script>
    
    <div style="text-align: center;"><h2>Online Payment Sale Report</h2></div>

       
           <table class="table">
                <tr>
                    <td>Payment Mode:</td>
                    <td><asp:DropDownList class="form-control" id="ddlOnlinePayment" runat="server" placeholder="OnlinePayment">
                        </asp:DropDownList></td>
                    <td>Branch:</td>
                    <td><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Branch">
                        </asp:DropDownList></td>
                    <td>POS:</td>
                    <td> <asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList></td>
                          <td>
                    <asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btnGetRecords_Click" /></td>
         <td>   <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button></td>
                  </tr>
             <tr>
                   <td>Date:</td>
                   <td><asp:TextBox ID="txtDateFrom" CssClass="form-control" type="date" runat="server"></asp:TextBox>
                       
                   </td>
                   <td>To:</td>
                   <td><asp:TextBox ID="txtDateTo" CssClass="form-control" type="date" runat="server"></asp:TextBox>
                      
                   </td>
         
                   <td>User:</td>
                   <td><asp:DropDownList class="form-control" id="dd_users" runat="server" ></asp:DropDownList></td>
            
               </tr>
           </table>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">
 
    </rsweb:ReportViewer>
        <div>

        </div>


</asp:Content>