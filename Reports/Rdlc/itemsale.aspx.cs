﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class itemsale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        if (!IsPostBack)
        {
			txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
			txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
			BindBranches();
           
            ddcustomertype();
            BinDDept();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }


    }


    public void BinDDept()
    {
        ddlCat2.DataSource = new DepartmentBLL().GetAll();
        ddlCat2.DataValueField = "Prop_ID";
        ddlCat2.DataTextField = "Prop_Name";
        ddlCat2.DataBind();
        ListItem li1 = new ListItem();

        li1.Text = "--Choose Department--";
        li1.Value = "0";

        ddlCat2.Items.Insert(0, li1);


        ddlGroup.DataSource = new GroupBLL().GetAll();
        ddlGroup.DataValueField = "Group_Id";
        ddlGroup.DataTextField = "Group_Name";
        ddlGroup.DataBind();
        ListItem li2 = new ListItem();

        li2.Text = "--Choose Group--";
        li2.Value = "0";

        ddlGroup.Items.Insert(0, li2);



        ddlsbgrp.DataSource = new SubGroupBLL().GetAll();
        ddlsbgrp.DataValueField = "SGroup_Id";
        ddlsbgrp.DataTextField = "SGroup_Name";
        ddlsbgrp.DataBind();
        ListItem li3 = new ListItem();

        li3.Text = "--Choose SubGroup--";
        li3.Value = "0";

        ddlsbgrp.Items.Insert(0, li3);


        ddlCompany.DataSource = new CompanyBLL().GetAll();
        ddlCompany.DataValueField = "Company_Id";
        ddlCompany.DataTextField = "Company_Name";
        ddlCompany.DataBind();
        ListItem li4 = new ListItem();

        li4.Text = "--Choose Company--";
        li4.Value = "0";

        ddlCompany.Items.Insert(0, li4);
    }
    public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
    {
        FieldInfo info;

        foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
        {
            if (extension.Name == strFormatName)
            {
                info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                info.SetValue(extension, false);
            }
        }
    }

    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();
        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose", "0");
        //dd_customername.Items.Insert(0, listItem1);

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPITEMSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }
    public void Getdata()
    {
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("report_sp_ItemSaleReport", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@qry", "All");
            cmd.Parameters.AddWithValue("@FromDate", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@ToDate", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@Pos_id", dd_customername.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@DeptId", ddlCat2.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@GroupId", ddlGroup.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@SubGroupId", ddlsbgrp.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@CompanyId", ddlCompany.SelectedItem.Value);
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////

            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }


    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

        ReportViewer1.Reset();
        DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\ItemSaleRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();


        DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
    protected void rdbsubgroup_CheckedChanged(object sender, EventArgs e)
    {

        if (rdbSubGroup.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbDepartment.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = true;
            rdbAll.Checked = false;
            ddlCompany.Visible = false;
            rdbCompany.Checked = false;

        }

    }

    protected void rdbgroup_changed(object sender, EventArgs e)
    {

        if (rdbGroup.Checked == true)
        {
            rdbSubGroup.Checked = false;
            rdbDepartment.Checked = false;
            ddlGroup.Visible = true;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = false;
            rdbAll.Checked = false;
            ddlCompany.Visible = false;
            rdbCompany.Checked = false;

        }

    }

    protected void rdbdepartment_changed(object sender, EventArgs e)
    {

        if (rdbDepartment.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = true;
            ddlsbgrp.Visible = false;
            rdbAll.Checked = false;
            ddlCompany.Visible = false;
            rdbCompany.Checked = false;

        }

    }

    protected void rdbAll_changed(object sender, EventArgs e)
    {

        if (rdbAll.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = false;
            rdbDepartment.Checked = false;
            ddlCompany.Visible = false;
            rdbCompany.Checked = false;

        }

    }

    protected void rdbCompany_CheckedChanged(object sender, EventArgs e)
    {

        if (rdbCompany.Checked == true)
        {
            rdbGroup.Checked = false;
            rdbSubGroup.Checked = false;
            ddlGroup.Visible = false;
            ddlCat2.Visible = false;
            ddlsbgrp.Visible = false;
            rdbDepartment.Checked = false;
            ddlCompany.Visible = true;
            rdbAll.Checked = false;

        }

    }


}