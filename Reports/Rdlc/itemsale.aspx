﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="itemsale.aspx.cs" Inherits="itemsale" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/itemsaleNew.css" rel="stylesheet" />
          <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Item Wise Sale Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

   function RptHeadDetail() {
   var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>POS:' + $('#<%= dd_customername.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
   return value;
         }
</script>


<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>

           <div style="text-align: center;"><h2 class="item-wise-sale-head">Item Wise Sale Report</h2></div>
<div class="item-wise-sale-top-panel">
    <div class="radio-div">    <asp:RadioButton ID="rdbAll" Checked="true" name ="all"   runat="server" AutoPostBack="true" Text="All" OnCheckedChanged="rdbAll_changed" />
     <asp:RadioButton ID="rdbCompany" name ="all"   runat="server" Text="Company" AutoPostBack="true" OnCheckedChanged="rdbCompany_CheckedChanged" />
    <asp:RadioButton ID="rdbDepartment" name ="all"   runat="server" AutoPostBack="true" Text="Department" OnCheckedChanged="rdbdepartment_changed" />
						<asp:RadioButton ID="rdbGroup" name ="all"   runat="server" Text="Group" AutoPostBack="true" OnCheckedChanged="rdbgroup_changed" />
					 <asp:RadioButton ID="rdbSubGroup" name ="all"   runat="server" Text="SubGroup" AutoPostBack="true" OnCheckedChanged="rdbsubgroup_CheckedChanged" />
				
</div>


    <div class=" item-wise-sale-col choose-col">     
						<div class="item-wise-sale-choose-section"> 
                         	<asp:DropDownList ID="ddlCat2" runat="server" Visible="false" CssClass="form-control compny-rate-selct" AutoPostBack="true" style="height: 30px;width: 20%;" ></asp:DropDownList>
                        </div>
					  <div class="item-wise-sale-choose-section"> 
							    <asp:DropDownList ID="ddlCompany" runat="server" Visible ="false"  CssClass="form-control compny-rate-selct" AutoPostBack="true" style="height: 30px;width: 20%;" ></asp:DropDownList>
                        </div>
					  <div class="item-wise-sale-choose-section"> 
						   <asp:DropDownList ID="ddlGroup" runat="server" Visible ="false"  CssClass="form-control compny-rate-selct" AutoPostBack="true" style="height: 30px;width: 20%;" ></asp:DropDownList>
                         </div>
						<div class="item-wise-sale-choose-section">     
					  <asp:DropDownList ID="ddlsbgrp" runat="server" Visible="false" CssClass="form-control compny-rate-selct" AutoPostBack="true" style="height: 30px;width: 20%;"></asp:DropDownList>
                       </div> 
						</div>

</div>
                           
                  
	<div class="col-md-3 col-sm-3 col-xs-12 item-wise-sale-col branch-col">
		<div class="item-wise-sale-branch-section"> 
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 item-wise-sale-col pos-col">
		<div class="item-wise-sale-pos-section"> 
			<span>POS:</span><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 item-wise-sale-col date-col">
		<div class="item-wise-sale-date-section"> 
			<div class="itme-wise-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" class="form-control" type="date"  runat="server"></asp:TextBox>
			</div>
			<div class="itme-wise-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" class="form-control" type="date"  name="All" runat="server"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 item-wise-sale-col btn-col">
		<div class="item-wise-sale-button-section"> 
			<asp:Button ID="Button1" runat="server"   CssClass="btn btn-success"  Text="Submit"  onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>

</div>
<div class="sale_report">
       
   <rsweb:ReportViewer ID="ReportViewer1" CssClass="item-wise-sale-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ShowBackButton="true" ShowPrintButton="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="850px" PageCountMode="Actual">

    </rsweb:ReportViewer>


</div>

        

</asp:Content>
