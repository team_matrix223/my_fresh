﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="newgroupreport.aspx.cs" Inherits="Reports_newgroupreport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReportsGroup.css" rel="stylesheet" />
	<link href="../css/newgroupreportsmallnew.css" rel="stylesheet" />
   <style type="text/css">
 #tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
 #tblist tr td{text-align:left;padding:2px}
  #tblist tr:nth-child(even){background:#F7F7F7}
 </style>
<script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
              <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head><title>RDLC Report</title>');
    frameDoc.document.write('</head><body style = "font-family:arial;font-size:10pt;">');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}
</script>
<script type="text/javascript">

    $(document).ready(
   function () {



       $("input[name ='group']").click(function () {
           var CompaniesId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnsale.ClientID %>").val(CompaniesId);


       });







       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }


    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {


            $("#<%=rdbAll.ClientID %>").prop('checked', false);


        }


    });



   });




</script>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />
<div class="department_wise">

         <div style="text-align: center;"><h2 class="small-grp-wise-head">Group Wise Sale Report</h2></div>
<div class="small-grp-wise-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 small-grp-wise-cst-col branch-col">
		<div class="small-grp-wise-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 small-grp-wise-cst-col date-col">
		<div class="small-grp-wise-date-section">
			<div class="small-grp-wise-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control" type="date"></asp:TextBox>
			</div>
			<div class="small-grp-wise-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" name="All" class="form-control" runat="server" type="date"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 small-grp-wise-cst-col pos-col">
		<div class="small-grp-wise-pos-section">
			<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 small-grp-wise-cst-col btn-col">
		<div class="small-grp-wise-btn-section">
			 <asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btnGetRecords_Click" />
           <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
<div class="small-grp-wise-second-panel">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="small-grp-wise-group-section">
			<span class="grp-section-span">Group:</span>
			<div class="small-grp-wise-radio-section">
				<asp:RadioButton ID="rdbAll" name="All" class="form-control" runat="server" Text="All" Checked="True" />
				<asp:RadioButton ID="rdbSelect" class="form-control"  runat="server" Text="Selected" />
				  <div style="height: 181px; width:265px; overflow: auto;">
						<table style="width:100%" id="tblist">
						<asp:Literal ID="ltGroups" runat="server"></asp:Literal>
                      
						</table>
				   </div>
			</div>
		</div>
	</div>
</div>

<div class="sale_report">
       <%--  <table class="table">
            <tr>
                <td>Branch:</td><td></td>
         
                
                   
                                <td>Date:</td>
                                <td>
                                    
                                  
                                </td>
                      
                                <td>To:</td>
                                <td></td>
                                <td></td>
                          
                                <td></td> 
                                <td></td> 
                            </tr>
                   
                      
                             <tr>
                                  <td class="headings" colspan="2">Groups:</td>
                              </tr>
                              <tr>
                                   <td colspan="2">
                                    
                                  </td>
                                                    <td>
                   </td>
                              </tr>
                  
             
                  


          
        </table>--%>
       <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="small-grp-wise-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
    </div>

 
</div>




        

</asp:Content>


