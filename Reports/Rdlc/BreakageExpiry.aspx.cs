﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class BreakageExpiry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {

            hdn_qry.Value = "All";
            hdn_depval.Value = "0";
            hdn_grpval.Value = "0";
            hdn_compval.Value = "0";
            BindBranches();
            ddlGodown.DataSource = new RGodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_id";
            ddlGodown.DataBind();

            ddlComapny.DataSource = new CompanyBLL().GetAll();
            ddlComapny.DataTextField = "Company_Name";
            ddlComapny.DataValueField = "Company_id";
            ddlComapny.DataBind();

            ddlGroup.DataSource = new GroupBLL().GetAll();
            ddlGroup.DataTextField = "Group_Name";
            ddlGroup.DataValueField = "Group_ID";
            ddlGroup.DataBind();

            ddlDepartment.DataSource = new DepartmentBLL().GetAll();
            ddlDepartment.DataTextField = "Prop_Name";
            ddlDepartment.DataValueField = "Prop_id";
            ddlDepartment.DataBind();

        }



        

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
   
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BREAKAGEEXPIRY));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }

   
   
    public void Getdata()
    {

        Int32 GodownId = Convert.ToInt32(ddlGodown.SelectedValue);
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("[report_sp_BreakageExpiryReport]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@qry", hdn_qry.Value);
            cmd.Parameters.AddWithValue("@CompanyId", hdn_compval.Value);
            cmd.Parameters.AddWithValue("@GroupId", hdn_grpval.Value);
            cmd.Parameters.AddWithValue("@DepartmentId", hdn_depval.Value);
            cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@GodownId", ddlGodown.SelectedItem.Value);
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////

            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\BreakageRpt2.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

    protected void ddlComapny_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdbCompany.Checked == true)
        {
            hdn_qry.Value = "Company";
            hdn_compval.Value  = ddlComapny.SelectedValue.ToString();
        }
        else
        {
            hdn_compval.Value  = "0";
        }
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdbDepartment.Checked == true)
        {
            hdn_qry.Value = "Department";
            hdn_depval.Value = ddlDepartment.SelectedValue.ToString();
        }
        else
        {
            hdn_depval.Value = "0";
        }
    }
    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rdbGroup.Checked == true)
        {
            hdn_qry.Value = "Group";
            hdn_grpval.Value = ddlGroup.SelectedValue.ToString();
        }
        else
        {
            hdn_grpval.Value = "0";
        }
    }
}