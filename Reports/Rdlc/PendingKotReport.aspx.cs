﻿
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Reports_PendingKotReport : System.Web.UI.Page
{
    Connection conn = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        int tableid = 0;
        if (!IsPostBack)
        {

			txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
			txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
			BindBranches();
            bindtables();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        if (dd_tables.SelectedValue == "0")
        {
            tableid = 0;
        }
        else
        {
            tableid = Convert.ToInt32(dd_tables.SelectedValue);
        }



    }
    void bindtables()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
       // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("pos_sp_TablesGetAll", con);
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);

        dd_tables.DataSource =dt;
        dd_tables.DataValueField = "tableid";
        dd_tables.DataTextField = "tablename";
        dd_tables.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Table no--";
        //li1.Value = "0";
        //dd_tables.Items.Insert(0, li1);

        con.Close();

    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPDELIVERYISSUE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }

    public void Getdata()
    {

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("report_sp_GetPendingKot", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@DateTo", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@Table_id", dd_tables.SelectedItem.Value);

            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////



            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {


        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\CancelledKotRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}