﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="DeptsaleSmallprinter.aspx.cs" Inherits="Reports_DeptsaleSmallprinter" %>

   
        <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
            <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
				<link href="../css/DeptsaleSmallprinterNew.css" rel="stylesheet" />
				<script src="../../js/jquery-1.9.0.min.js"  type="text/javascript"></script>
                
                  
                    <script type="text/javascript">

						function myFunction1() {
							var input, filter, table, tr, td, i;
							input = document.getElementById("myInput1");
							filter = input.value.toUpperCase();
							table = document.getElementById("tblist");
							tr = table.getElementsByTagName("tr");
							for (i = 0; i < tr.length; i++) {
								td = tr[i].getElementsByTagName("td")[0];
								if (td) {
									if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
										tr[i].style.display = "";
									} else {
										tr[i].style.display = "none";
									}
								}
							}
						}

                        $(document).ready(
                            function () {

                                $("#myInput1").hide();
                                $("#td_1").hide();
                                $("#<%=rdbSelect.ClientID %>").click(function () {
                                    $("#myInput1").show();
                                    $("#td_1").show();
                                    $(".cls_chk").removeAttr('checked', 'checked');
                                    $("#sel_table").show();
                                    $("#<%=hdnsale.ClientID %>").val("");
                                });
                                $("#<%=rdbAll.ClientID %>").click(function () {
                                    $("#myInput1").hide();
                                    $("#td_1").hide();
                                    $("#sel_table").hide();
                                    $(".cls_chk").attr('checked', 'checked');
                                    var CompaniesId = $('input[name="department"]:checkbox:checked').map(function () {
                                        return this.value;
                                    }).get();


                                    $("#<%=hdnsale.ClientID %>").val(CompaniesId);
                                });


                                $("input[name ='department']").click(function () {
                                    var CompaniesId = $('input[name="department"]:checkbox:checked').map(function () {
                                        return this.value;
                                    }).get();


                                    $("#<%=hdnsale.ClientID %>").val(CompaniesId);


                                });







                                $("#<%=rdbAll.ClientID %>").change(
                                    function () {


                                        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
                                            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

                                        }


                                    }
                                );
                                $("#<%=rdbSelect.ClientID %>").change(
                                    function () {

                                        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {


                                            $("#<%=rdbAll.ClientID %>").prop('checked', false);


                                        }


                                    });



                            });


						
                    </script>

                    <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />
                   <div style="text-align: center;"><h2 class="dep-wise-grp-wise-sale-head">DEPARTMENT WISE GROUP WISE SALE REPORT</h2></div>
                      <div class="dep-wise-grp-wise-sale-top-panel">
						  <div class="col-md-3 col-sm-3 col-xs-12 dep-wise-grp-wise-sale-cst-col branch-col">
							  <div class="dep-wise-grp-wise-sale-branch-section">
								  <span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch">
                                    </asp:DropDownList>
							  </div>
						  </div>
						  <div class="col-md-4 col-sm-4 col-xs-12 dep-wise-grp-wise-sale-cst-col date-col">
							  <div class="dep-wise-grp-wise-sale-date-section">
								  <div class="dep-wise-grp-wise-sale-date-part">
									  <span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control"  runat="server" ></asp:TextBox>
								  </div>
								  <div class="dep-wise-grp-wise-sale-date-part">
									  <span>To:</span><asp:TextBox ID="txtDateTo" type="date"  class="form-control"  name="All" runat="server" ></asp:TextBox>
								  </div>
							  </div>
						  </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 dep-wise-grp-wise-sale-cst-col pos-col">
							  <div class="dep-wise-grp-wise-sale-pos-section">
								  <span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
							  </div>
						  </div>
						  <div class="col-md-2 col-sm-2 col-xs-12 dep-wise-grp-wise-sale-cst-col btn-col">
							  <div class="dep-wise-grp-wise-sale-btn-section">
								  <asp:Button ID="Button1" runat="server"   CssClass="btn btn-success"  Text="Submit"  onclick="btnGetRecords_Click"/>
								  <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
							  </div>
						  </div>
                      </div>  
                      <div class="dep-wise-grp-wise-sale-bottom-panel">
						  <div class="col-md-12 col-sm-12 col-xs-12 ">
							  <div class="dep-wise-grp-wise-sale-dep-section">
								  <span>Department:</span>
								  <asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All"  Checked="True" />
								  <asp:RadioButton ID="rdbSelect" name="All"  runat="server" Text="Selected"/>
								  <div id="sel_table" style="display:none">
									  <input type="text" id="myInput1"  class="form-control" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name" />
									  <div style="height: 181px; overflow: auto;">
                                      <table id="tblist">
                                      <asp:Literal ID="ltDepartments" runat="server"></asp:Literal>
                      
                                      </table>
                                      </div>
								  </div>
							  </div>
						  </div>
					  </div>
                        
                      <%--   <table class="table">
                            <tr>
								<td>Branch:</td>
                              <td>  
                                </td>
                                 <td>Date:</td>
                            <td>
                              
                            </td>
                       
                                 <td>To:</td>
                           <td></td>
                            <td></td>
                           <td></td>
       <td></td>  
                         
                                
                            
                            </tr>
                             <tr>
                             <td></td> 
                            <td>
								</td>  

                                    
                      </tr>
                             <tr>
                           <td colspan="2"  >
                                              <table class="table">
                        <tbody>
                            <tr>
                                <td class="headings">Department:</td></tr>
                               <tr>
                                  <td>
                                      
                                   </td>
                                                    
                               </tr>
                        </tbody>
                    </table>
                          </td>
                            <td colspan="2" ></td>  
                          </tr>
                          

                        </table>--%>
                      
                                  
                 <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="dep-wise-grp-wise-sale-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ShowBackButton="true" ShowPrintButton="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" PageCountMode="Actual">

    </rsweb:ReportViewer>
                                      
                                      
            </asp:Content>