﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptOnlinePayment.aspx.cs" Inherits="Reports_RptOnlinePayment" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
 Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
	<link href="../css/RptOnlinePaymentNew.css" rel="stylesheet" />
       <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">
	   function Print() {
		   var report = document.getElementById("<%=ReportViewer1.ClientID %>");
		   var div = report.getElementsByTagName("DIV");
		   var reportContents;
		   for (var i = 0; i < div.length; i++) {
			   if (div[i].id.indexOf("VisibleReportContent") != -1) {
				   reportContents = div[i].innerHTML;
				   break;
			   }
		   }
		   var frame1 = document.createElement('iframe');
		   frame1.name = "frame1";
		   frame1.style.position = "absolute";
		   frame1.style.top = "-1000000px";
		   document.body.appendChild(frame1);
		   var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
		   frameDoc.document.open();
		   frameDoc.document.write('<html><head></head>');
		   frameDoc.document.write('<div  style = "text-align: center">Online Payment Sale Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
		   frameDoc.document.write(reportContents);
		   frameDoc.document.write('</body></html>');
		   frameDoc.document.close();
		   setTimeout(function () {
			   window.frames["frame1"].focus();
			   window.frames["frame1"].print();
			   document.body.removeChild(frame1);
		   }, 500);
	   }

       
   function RptHeadDetail() {
       var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>Payment Mode:' + $('#<%= ddlOnlinePayment.ClientID %> option:selected').text() + '<br>POS:' + $('#<%= dd_customername.ClientID %> option:selected').text() + '<br>User:' + $('#<%= dd_users.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
   return value;
         }
</script>
    
    <div style="text-align: center;"><h2 class="online-pymt-sale-rpt-head">Online Payment Sale Report</h2></div>
	<div class="online-pymt-sale-rpt-top-panel">



		 <div class="radio-section">
                   <span>Report Type:</span><asp:RadioButtonList ID="rdoselection"  runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="UserWise" Selected="True">&nbsp;UserWise&nbsp;</asp:ListItem>
                         <asp:ListItem Value="BillNoWise">&nbsp;BillNo Wise&nbsp;</asp:ListItem>
                       
                   </asp:RadioButtonList> 
                </div>

		<div class="col-md-4 col-sm-4 col-xs-12 online-pymt-sale-rpt-cst-col paymode-rw">
			<div class="online-pymt-sale-rpt-pymtmode-section">
				<span>Payment Mode:</span><asp:DropDownList class="form-control" id="ddlOnlinePayment" runat="server" placeholder="OnlinePayment">
                        </asp:DropDownList>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 online-pymt-sale-rpt-cst-col branch-rw">
			<div class="online-pymt-sale-rpt-branch-section">
				<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Branch">
                        </asp:DropDownList>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 online-pymt-sale-rpt-cst-col date-rw">
			<div class="online-pymt-sale-rpt-date-section">
				<div class="online-pymt-sale-rpt-date-part date-part">
					<span>Date:</span><asp:TextBox ID="txtDateFrom" CssClass="form-control" type="date" runat="server"></asp:TextBox>
				</div>
				<div class="online-pymt-sale-rpt-date-part to-part">
					<span>To:</span><asp:TextBox ID="txtDateTo" CssClass="form-control" type="date" runat="server"></asp:TextBox>
				</div>
			</div>
		</div>
<!--	</div>
	<div class="online-pymt-sale-rpt-bottom-panel">-->
		<div class="col-md-4 col-sm-4 col-xs-12 cst-mar-top-pymt online-pymt-sale-rpt-cst-col pos-rw">
			<div class="online-pymt-sale-rpt-pos-section">
				<span>POS:</span><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
			</div>
			
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 cst-mar-top-pymt online-pymt-sale-rpt-cst-col user-rw">
			<div class="online-pymt-sale-rpt-user-section">
				<span>User:</span><asp:DropDownList class="form-control" id="dd_users" runat="server" ></asp:DropDownList>
			</div>
		</div>
		<div class="col-md-4 col-sm-4 col-xs-12 cst-mar-top-pymt online-pymt-sale-rpt-cst-col btn-rw">
			<div class="online-pymt-sale-rpt-btn-section">
				 <asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btnGetRecords_Click" />
				 <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
			</div>
		</div>
	</div>
       
       
         <%--  <table class="table">
                <tr>
                    <td>Payment Mode:</td>
                    <td></td>
                    <td>Branch:</td>
                    <td></td>
                    <td>POS:</td>
                    <td> </td>
                          <td>
                   </td>
         <td>  </td>
                  </tr>
             <tr>
                   <td>Date:</td>
                   <td>
                       
                   </td>
                   <td>To:</td>
                   <td>
                      
                   </td>
         
                   <td>User:</td>
                   <td></td>
            
               </tr>
           </table>--%>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="online-pymt-sale-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">
 
    </rsweb:ReportViewer>
        <div>

        </div>


</asp:Content>