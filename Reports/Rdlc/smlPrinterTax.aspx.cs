﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Rdlc_smlPrinterTax : System.Web.UI.Page
{
    Connection conn = new Connection();

    protected void Page_Load(object sender, EventArgs e)
	{
        int BranchId = 0;
        if (IsPostBack == false)
        {
            tbfrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
            tbto.Text = DateTime.Now.ToString("yyyy-MM-dd");
           
            BindBranches();

            //  Getdata(rdoselection.SelectedValue);
            // BindOnlinePayment();

        }
        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        //if (ddlBranch.SelectedValue == "0")
        //{
        //    BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //}
        //else
        //{
        BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        //}
    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPTAXWISESALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    public void Getdata(string Type)
    {

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            if (Type == "TaxWise")
            {


                SqlCommand cmd = new SqlCommand("Report_Sp_TaxWiseSaleReportDated", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BillFrom", "");
                cmd.Parameters.AddWithValue("@BillTo", "");
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
                DataTable dt = new DataTable("dt_table");
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                ///////MainReport End/////////
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
            }
            else if (Type == "HSNWise")
            {
               
                SqlCommand cmd = new SqlCommand("Report_Sp_TaxWiseSaleReportDated", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BillFrom", "");
                cmd.Parameters.AddWithValue("@BillTo", "");
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
               
                DataTable dt = new DataTable("dt_table");
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                ///////MainReport End/////////

                ///////SubReport Start/////////
               

                ///////SubReport End/////////

                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.EnableHyperlinks = true;

                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
              

                Session["fromdate"] = tbfrom.Text.Trim();
                Session["todate"] = tbto.Text.Trim();

                Session["branchid"] = ddlBranch.SelectedItem.Value;
               
            }
           


        }



    }



    protected void btngetdata_Click(object sender, EventArgs e)
    {
        ReportViewer1.Reset();
        // DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
       
        ReportViewer1.AsyncRendering = false;

        if (rdoselection.SelectedValue == "TaxWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\smlPrintHsnWise.rdlc";
        }
        else if (rdoselection.SelectedValue == "HSNWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\smlPrintHsnWise.rdlc";
        }
        Getdata(rdoselection.SelectedValue);
        ReportViewer1.LocalReport.Refresh();
       
        // DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
       
    }

}