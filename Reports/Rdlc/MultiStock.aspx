﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="MultiStock.aspx.cs" Inherits="MultiStock" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/MultiStockNew.css" rel="stylesheet" />
      <script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Multi Stock Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}
         function RptHeadDetail() {
   var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() +'<br>GoDown:' + $('#<%= ddlGodown.ClientID %> option:selected').text()
   return value;
         }

		 $(document).ready(function () {

			 var counter = 0;
			 $(".cls_chk").change(function () {

			
			
				 if (counter == 3) {

					
					 //if (true) {
						// $(".cls_chk").RemoveAttr('disabled', 'disabled')
					 //}
					 counter = 0;

				 }
				 //else {
					// $(".cls_chk").RemoveAttr('disabled', 'disabled')
				 //}
				 counter = counter + 1;
			 })

		   <%-- $("#<%=ddlComapny.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbCompany.ClientID %>").attr('disabled', 'disabled');

			 $("#<%=ddlGroup.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbGroup.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=ddlSubGroup.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbSubgrp.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=ddlDepartment.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbDept.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=ddlLocation.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbloc.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=ddlColor.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbClr.ClientID %>").attr('disabled', 'disabled');

			 $("#<%=ddltax.ClientID %>").attr('disabled', 'disabled');
			 $("#<%=rdbtax.ClientID %>").attr('disabled', 'disabled');--%>


		 $("#<%=rdbAllCmpny.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllCmpny.ClientID %>").prop('checked') == true) {
				  $("#<%=rdbCompany.ClientID %>").prop('checked', false);
		

			  }


		  }
	  );

		 $("#<%=rdbCompany.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbCompany.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllCmpny.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		

     
		 $("#<%=rdbAllGrp.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllGrp.ClientID %>").prop('checked') == true) {
					  $("#<%=rdbGroup.ClientID %>").prop('checked', false);


				  }


			  }
		  );

		 $("#<%=rdbGroup.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbGroup.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllGrp.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		
    
		 $("#<%=rdbAllSbgrp.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllSbgrp.ClientID %>").prop('checked') == true) {
					  $("#<%=rdbSubgrp.ClientID %>").prop('checked', false);


				  }


			  }
		  );

		 $("#<%=rdbSubgrp.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbSubgrp.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllSbgrp.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		
		 $("#<%=rdbAllDept.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllDept.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbDept.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		 $("#<%=rdbDept.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbDept.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllDept.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		

		 $("#<%=rdbAllLoc.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllLoc.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbloc.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		 $("#<%=rdbloc.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbloc.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllLoc.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		



		 $("#<%=rdbAllClr.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAllClr.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbClr.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		 $("#<%=rdbClr.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbClr.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAllClr.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		



		 $("#<%=rdbAlltax.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbAlltax.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbtax.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		 $("#<%=rdbtax.ClientID %>").change(
			 function () {
				 if ($("#<%=rdbtax.ClientID %>").prop('checked') == true) {
					 $("#<%=rdbAlltax.ClientID %>").prop('checked', false);


				 }


			 }
		 );

		


         });

         </script>

    <div class="department_wise">


		<asp:HiddenField ID="hdn_depqry" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn_grpqry" ClientIDMode="Static" runat="server" />
     <asp:HiddenField ID="hdn_compqry" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_sbgrpqry" ClientIDMode="Static" runat="server" />  
		<asp:HiddenField ID="hdn_locqry" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn_colorqry" ClientIDMode="Static" runat="server" />
     <asp:HiddenField ID="hdn_taxqry" ClientIDMode="Static" runat="server" />
   

        
  <div style="text-align: center;"><h2 class="multi-stck-head">Multi Stock Report</h2></div>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<div class="multi-stck-top-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" AutoPostBack="true"  OnSelectedIndexChanged="ddlBranch_SelectedIndexChanged"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-godown-section">
			<span>Godown:</span><asp:DropDownList class="form-control" id="ddlGodown" runat="server" placeholder="Godown"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-cmpny-section">
			<asp:CheckBox ID="chkCompany" name="chkcomp" CssClass="cls_chk" runat="server" Text="Company" Checked="false" AutoPostBack="true" OnCheckedChanged="chkCompany_CheckedChanged" />
			<asp:RadioButton ID="rdbAllCmpny" name="All" runat="server" Text="All" Checked="True" />	
			<asp:RadioButton ID="rdbCompany" name="All"  runat="server" Text="Selected" />
			<asp:DropDownList class="form-control" id="ddlComapny" runat="server" placeholder="Company" AutoPostBack="true"  OnSelectedIndexChanged="ddlComapny_SelectedIndexChanged"></asp:DropDownList>
		</div>
	</div>
</div>
<div class="multi-stck-second-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-group-section">
			
				<asp:CheckBox ID="chkGroup" name="chkGrp" CssClass="cls_chk"  runat="server" Text="Group" Checked="false" AutoPostBack="true" OnCheckedChanged="chkGroup_CheckedChanged"/>
				<asp:RadioButton ID="rdbAllGrp" name="All" runat="server" Text="All" Checked="True" />
			
			
				<asp:RadioButton ID="rdbGroup" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddlGroup" runat="server" placeholder="Group" AutoPostBack="true"  OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged"></asp:DropDownList>
			
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-sub-group-section">
			
				<asp:CheckBox ID="chkSubGrop" name="chkSbGrp" CssClass="cls_chk" runat="server" Text="SubGroup" Checked="false" AutoPostBack="true" OnCheckedChanged="chkSubGrop_CheckedChanged" />
				<asp:RadioButton ID="rdbAllSbgrp" name="All" runat="server" Text="All" Checked="True" />
			
				<asp:RadioButton ID="rdbSubgrp" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddlSubGroup" runat="server" placeholder="SubGroup" AutoPostBack="true"  OnSelectedIndexChanged="ddlSubGroup_SelectedIndexChanged"></asp:DropDownList>
		
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-dep-section">
			
				<asp:CheckBox ID="chkDepartment" name="chkDept" CssClass="cls_chk"  runat="server" Text="Department" Checked="false" AutoPostBack="true" OnCheckedChanged="chkDepartment_CheckedChanged" />
				<asp:RadioButton ID="rdbAllDept" name="All" runat="server" Text="All" Checked="True" />
			
				<asp:RadioButton ID="rdbDept" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddlDepartment" runat="server" placeholder="Department" AutoPostBack="true"  OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
		
		</div>
	</div>
</div>
<div class="multi-stck-third-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-loc-section">
			
				<asp:CheckBox ID="chkLocation" name="chkLoc" CssClass="cls_chk" runat="server" Text="Location" Checked="false" AutoPostBack="true" OnCheckedChanged="chkLocation_CheckedChanged"  />
				<asp:RadioButton ID="rdbAllLoc" name="All" runat="server" Text="All" Checked="True" />
			
			
				<asp:RadioButton ID="rdbloc" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddlLocation" runat="server" placeholder="Location" AutoPostBack="true"  OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"></asp:DropDownList>
			
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-clr-section">
			
				<asp:CheckBox ID="chkColor" name="chkclr" CssClass="cls_chk" runat="server" Text="Color" Checked="false" AutoPostBack="true" OnCheckedChanged="chkColor_CheckedChanged" />
				<asp:RadioButton ID="rdbAllClr" name="All" runat="server" Text="All" Checked="True" />
			
				<asp:RadioButton ID="rdbClr" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddlColor" runat="server" placeholder="Color" AutoPostBack="true"  OnSelectedIndexChanged="ddlColor_SelectedIndexChanged"></asp:DropDownList>
		
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="multi-stck-tax-section">
		
				<asp:CheckBox ID="chktax" name="chkclr" CssClass="cls_chk" runat="server" Text="Tax" Checked="false" AutoPostBack="true" OnCheckedChanged="chktax_CheckedChanged" />
				<asp:RadioButton ID="rdbAlltax" name="All" runat="server" Text="All" Checked="True" />
			
				<asp:RadioButton ID="rdbtax" name="All"  runat="server" Text="Selected" />
				<asp:DropDownList class="form-control" id="ddltax" runat="server" placeholder="Color" AutoPostBack="true"  OnSelectedIndexChanged="ddltax_SelectedIndexChanged"></asp:DropDownList>
			
		</div>
	</div>
</div>
<div class="multi-stck-btn-sectoin">
	<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Submit" onclick="btnGetRecords_Click"/>
	<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
</div>
<div class="sale_report">
<%-- <table class="cashmemo_header">
     <tr>
         <td> Branch:</td>
         <td></td>
       
       

          <td> Godown:</td>
         <td colspan ="2"></td>
        
         <td></td><td colspan="2"></td>
         <td></td>
        <td>  </td>
       </tr>
     <tr>
         <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         </tr>


     <tr>
         <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         </tr>
    
           <tr>
          <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         
         </tr>

       <tr>
          <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         
         </tr>

      <tr>
          <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         
         </tr>

     <tr>
          <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         
         </tr>

      <tr>
          <td></td>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         
         </tr>
                     
      </table>--%>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="multi-stck-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1100px" PageCountMode="Actual">

    </rsweb:ReportViewer>
    </div>


</div>
</asp:Content>


