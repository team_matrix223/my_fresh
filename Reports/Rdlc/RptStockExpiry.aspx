﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptStockExpiry.aspx.cs" Inherits="RptStockExpiry" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/RptStockExpiryNew.css" rel="stylesheet" />
<script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
           <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    	frameDoc.document.write('<html><head></head>');
    	frameDoc.document.write('<div  style = "text-align: center">Stock Expiry Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

   function RptHeadDetail() {
   var value = 'Department:' + $('#<%= ddlDepartment.ClientID %> option:selected').text() + '<br>Company:' + $('#<%= ddlcompany.ClientID %> option:selected').text() + '<br>GoDown:' + $('#<%= ddlgodown.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
   return value;
         }
</script>
<script type ="text/javascript">
    $(document).ready(
function () {







    $("#<%=rdbFinished.ClientID %>").change(
    function () {

        if ($("#<%=rdbFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSemiFinished.ClientID %>").change(
    function () {
        if ($("#<%=rdbSemiFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbRaw.ClientID %>").change(
    function () {
        if ($("#<%=rdbRaw.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
        }


    }
    );





    $("#<%=rdbAllcom.ClientID %>").change(
    function () {

        if ($("#<%=rdbAllcom.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSelCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbSelDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSelCompany.ClientID %>").change(
    function () {
        if ($("#<%=rdbSelCompany.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllcom.ClientID %>").prop('checked', false);
            $("#<%=rdbSelDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSelDept.ClientID %>").change(
    function () {
        if ($("#<%=rdbSelDept.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllcom.ClientID %>").prop('checked', false);
            $("#<%=rdbSelCompany.ClientID %>").prop('checked', false);
        }


    }
    );




    $("#<%=rdbRefWise.ClientID %>").change(
    function () {

        if ($("#<%=rdbRefWise.ClientID %>").prop('checked') == true) {

            $("#<%=rdbConsolidate.ClientID %>").prop('checked', false);
           
        }


    }
    );
    $("#<%=rdbConsolidate.ClientID %>").change(
    function () {
        if ($("#<%=rdbConsolidate.ClientID %>").prop('checked') == true) {

            $("#<%=rdbRefWise.ClientID %>").prop('checked', false);
          
        }


    }
    );

}
);
</script>
   <div class="department_wise">

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
             <div style="text-align: center;"><h2 class="stck-expiry-head">Stock Expiry Report</h2></div>
<div class="stck-expiry-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 stck-expiry-cst-col">
		<div class="stck-expiry-radio-section">
				<asp:RadioButton ID="rdbFinished"  name ="all" runat="server" Text="Finished" Checked="True" AutoPostBack="True" />
               <asp:RadioButton ID="rdbSemiFinished"  name ="all" runat="server" Text="Semi-Finished" />
                <asp:RadioButton ID="rdbRaw"  name ="all" runat="server" Text="Raw"  />
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 stck-expiry-cst-col">
		<div class="stck-expiry-godown-section">
			<span>Godown:</span><asp:DropDownList ID ="ddlgodown" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 stck-expiry-cst-col">
		<div class="stck-expiry-cmpny-section">
			<span>Company:</span><asp:DropDownList ID ="ddlcompany" CssClass="form-control"  runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 stck-expiry-cst-col">
		<div class="stck-expiry-dep-section">
			<span>Department:</span><asp:DropDownList ID ="ddlDepartment" CssClass="form-control"  runat="server"></asp:DropDownList>
		</div>
	</div>
</div>
<div class="stck-expiry-middle-panel">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="stck-expiry-com-rdo-section">
			<asp:RadioButton ID="rdbAllcom"  name ="Company" runat="server" Text="All Company" Checked="True"  />
                <asp:RadioButton ID="rdbSelCompany"  name ="Company" runat="server" Text="Selected Company"  />
               <asp:RadioButton ID="rdbSelDept"  name ="Company" runat="server" Text="Selected Department" />
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="stck-expiry-report-section">
			<span>Report:</span><asp:RadioButton ID="rdbRefWise"  name ="Report" runat="server" Text="Ref No Wise Detailed" Checked="True"  />
			<asp:RadioButton ID="rdbConsolidate"  name ="Report" runat="server" Text="Consolidate"  />
		</div>
	</div>
	
</div>
<div class="stck-expiry-bottom-panel">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="stck-expiry-date-section">
			<div class="stck-expiry-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" CssClass="form-control" runat="server" type="date"></asp:TextBox>
			</div>
			<div class="stck-expiry-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" CssClass="form-control" runat="server" type="date"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="stck-expiry-btn-section">
			 <asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btngetdata_Click" />
           <button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
   <div class="sale_report">
        <%--<table class="table">

            <tr>
   
                <td>
               </td>
                   <td>Godown:</td><td></td>
              <td></td>
              <td></td>
            </tr>
  
            <tr>
   
                <td>
               </td>

                  <td>Report:</td>
                <td></td>
                <td></td>
            </tr>

           <tr>
               <td>Date:</td>
               <td>
        
               </td>
               <td> To:</td>
               <td>
                   
                  
               </td>
                            <td>
                   </td>
           </tr>
   </table>--%>
          <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="stck-expiry-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
       </div>
    


</div>
</asp:Content>





