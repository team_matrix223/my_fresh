﻿
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RptTaxWiseSale : System.Web.UI.Page
{
    Connection conn = new Connection();

    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        if (IsPostBack == false)
        {
            tbfrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
            tbto.Text = DateTime.Now.ToString("yyyy-MM-dd");
            ddcustomertype();
            BindOnlinePayment();
            BindBranches();
           
            //  Getdata(rdoselection.SelectedValue);
            // BindOnlinePayment();

        }
        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        //if (ddlBranch.SelectedValue == "0")
        //{
        //    BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //}
        //else
        //{
           BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        //}
    }




    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPTAXWISESALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }

   

    void BindOnlinePayment()
    {

        ddlOnlinePayment.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOnlinePayment.DataValueField = "OtherPayment_ID";
        ddlOnlinePayment.DataTextField = "OtherPayment_Name";
        ddlOnlinePayment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "All";
        li1.Value = "0";
        ddlOnlinePayment.Items.Insert(0, li1);

    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();

        ListItem li1 = new ListItem();
        li1.Text = "All";
        li1.Value = "0";
        dd_customername.Items.Insert(0, li1);
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
    {
        FieldInfo info;

        foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
        {
            if (extension.Name == strFormatName)
            {
                info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                info.SetValue(extension, false);
            }
        }
    }
    public void Getdata(string Type) {

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            if (Type == "HsnItemWise")
            {


                SqlCommand cmd = new SqlCommand("Report_Sp_HsnItemWiseSaleReportdated", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BillFrom", "");
                cmd.Parameters.AddWithValue("@BillTo", "");
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
                DataTable dt = new DataTable("dt_table");
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                ///////MainReport End/////////
                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
            }
            else if (Type == "HsnBillModeWise")
            {
                string Type12 = "HSNModeWise";
                SqlCommand cmd = new SqlCommand("Report_Sp_HSnWiseBillModeWise", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BillFrom", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@BillTo", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@Type", Type12);
                cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@BillMode", dd_billmode.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@cst_id", ddlOnlinePayment.SelectedItem.Value);
                DataTable dt = new DataTable("dt_table");
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                ///////MainReport End/////////

                ///////SubReport Start/////////
                SqlCommand sub_cmd = new SqlCommand("strp_TaxWiseCstSubReport", con);
                sub_cmd.CommandType = CommandType.StoredProcedure;
                sub_cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                sub_cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                sub_cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedItem.Value);
                DataTable sub_dt = new DataTable("dt_table2");
                SqlDataAdapter sub_adb = new SqlDataAdapter(sub_cmd);
                sub_adb.Fill(sub_dt);

                ///////SubReport End/////////

                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.EnableHyperlinks = true;

                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet2", sub_dt));

                Session["fromdate"] = tbfrom.Text.Trim();
                Session["todate"] = tbto.Text.Trim();

                Session["branchid"] = ddlBranch.SelectedItem.Value;
                Session["pos_id"] = dd_customername.SelectedItem.Value;
                Session["billmode"] = dd_billmode.SelectedItem.Value;
            }
            else
            {

                SqlCommand cmd = new SqlCommand("Report_Sp_TaxWiseSaleReportNEW", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BillFrom", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@BillTo", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@BillMode", dd_billmode.SelectedItem.Value);
                cmd.Parameters.AddWithValue("@cst_id", ddlOnlinePayment.SelectedItem.Value);
                cmd.CommandTimeout = 2000;
                DataTable dt = new DataTable("dt_table");
                SqlDataAdapter adb = new SqlDataAdapter(cmd);
                adb.Fill(dt);
                ///////MainReport End/////////

                ///////SubReport Start/////////
                SqlCommand sub_cmd = new SqlCommand("strp_TaxWiseCstSubReport", con);
                sub_cmd.CommandType = CommandType.StoredProcedure;
                sub_cmd.Parameters.AddWithValue("@FromDate", tbfrom.Text.Trim());
                sub_cmd.Parameters.AddWithValue("@ToDate", tbto.Text.Trim());
                sub_cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedItem.Value);
                DataTable sub_dt = new DataTable("dt_table2");
                SqlDataAdapter sub_adb = new SqlDataAdapter(sub_cmd);
                sub_adb.Fill(sub_dt);

                ///////SubReport End/////////

                ReportViewer1.LocalReport.Refresh();
                ReportViewer1.LocalReport.EnableHyperlinks = true;

                ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
                ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet2", sub_dt));

                Session["fromdate"] = tbfrom.Text.Trim();
                Session["todate"] = tbto.Text.Trim();

                Session["branchid"] = ddlBranch.SelectedItem.Value;
                Session["pos_id"] = dd_customername.SelectedItem.Value;
                Session["billmode"] = dd_billmode.SelectedItem.Value;
            }
           

       
        }

       

    }



    protected void btngetdata_Click(object sender, EventArgs e)
    {
        ReportViewer1.Reset();
       // DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
        DisableUnwantedExportFormat(ReportViewer1, "WORDOPENXML");
        ReportViewer1.AsyncRendering = false;

        if (rdoselection.SelectedValue== "TaxWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\TaxWiseSaleRpt.rdlc";
        }
       else if (rdoselection.SelectedValue == "HSNWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\HSNWiseSaleRpt.rdlc";
        }
        else if (rdoselection.SelectedValue == "Consolidate")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\TaxWiseConsRpt.rdlc";
        }
        else if (rdoselection.SelectedValue == "GroupWiseHSN")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\GroupWiseHsnRpt.rdlc";
        }
        else if (rdoselection.SelectedValue == "HsnItemWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\HsnItemWiseSaleRpt.rdlc";
        }
        else if (rdoselection.SelectedValue == "HsnBillModeWise")
        {
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\HSNBillMOdeWiseSaleRpt.rdlc";
        }
        Getdata(rdoselection.SelectedValue);
        ReportViewer1.LocalReport.Refresh();
        if (dd_billmode.SelectedItem.Value == "OnlinePayment")
        {
          
            lblonlinepayname.Text = ddlOnlinePayment.SelectedItem.Text;
        }
       // DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
        DisableUnwantedExportFormat(ReportViewer1, "WORDOPENXML");
    }



    protected void dd_billmode_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dd_billmode.SelectedItem.Value == "OnlinePayment")
        {
            div_onlinepay.Visible = true;
          
        }
        else
        {
            ddlOnlinePayment.SelectedValue = "0";
            div_onlinepay.Visible = false;
        }
    }
}