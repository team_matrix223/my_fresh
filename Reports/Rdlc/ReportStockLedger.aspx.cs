﻿
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class ReportStockLedger : System.Web.UI.Page
{
    Connection conn = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
			txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");

			ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
            //ListItem li = new ListItem();
            //li.Value = "0";
            //li.Text = "--Select--";
            //ddlgodown.Items.Insert(0, li);
            Bind();
            BindProducts();
          
        }

        CheckRole();

  


    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPSTOCKLEDGER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }
    public void Bind()
    {
        //ListItem li = new ListItem();
        //li.Value = "0";
        //li.Text = "--Select--";
        //string ItemType = "";
        //if (rdbFinished.Checked == true)
        //{
        //    ItemType = "3";
        //}
        //else if (rdbSemiFinished.Checked == true)
        //{
        //    ItemType = "5";
        //}
        //else if (rdbRaw.Checked == true)
        //{
        //    ItemType = "1";
        //}



        ddlProducts.DataSource = new RGodownsBLL().GetAllProducts();
        ddlProducts.DataTextField = "Item_Name";
        ddlProducts.DataValueField = "Item_Code";
        ddlProducts.DataBind();
        //ddlProducts.Items.Insert(0, li);

    }
    void BindProducts()
    {
        ltProducts.Text = new RGodownsBLL().GetProductHtmlwidradios();
    }
    public void Getdata()
    {
        string Products1 = "";
        //if (rdbName.Checked == true)
        //{
        //    Products1 = hdn_productval.Value;

        //}
        //else
        //{
            Products1 = txtCode.Text;
       // }
        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);

        //string Products1 = txtCode.Text;

        Boolean FOC = true;
        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("master_sp_LedgerStock", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@ItemCode", Products1.Trim());
            cmd.Parameters.AddWithValue("@dtStart", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@dtEnd", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@iGodownID", GodownId);
            cmd.Parameters.AddWithValue("@FocTrue", FOC);
            cmd.Parameters.AddWithValue("@BranchId", BranchId);

            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////



            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btngetdata_Click(object sender, EventArgs e)
    {



        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\StockLdgrRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

    //protected void rdbFinished_CheckedChanged(object sender, EventArgs e)
    //{
    //    Bind();
    //}
    //protected void rdbSemiFinished_CheckedChanged(object sender, EventArgs e)
    //{
    //    Bind();
    //}
    //protected void rdbRaw_CheckedChanged(object sender, EventArgs e)
    //{
    //    Bind();
    //}
}