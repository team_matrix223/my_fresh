﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="NegativeStock.aspx.cs" Inherits="NegativeStock" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/NegativeStockNew.css" rel="stylesheet" />
      <script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Negative Stock Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

function RptHeadDetail() {
   var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() +'<br>GoDown:' + $('#<%= ddlGodown.ClientID %> option:selected').text()
   return value;
         }
     $(document).ready(function () {


       $("#<%=rdbAll.ClientID %>").change(
    function () {

               if ($("#<%=rdbAll.ClientID %>").prop('checked') == true)
               {
            $("#<%=rdbCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbDepartment.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
 
             }


    });


      $("#<%=rdbGroup.ClientID %>").change(
    function () {

     if ($("#<%=rdbGroup.ClientID %>").prop('checked') == true) {
            $("#<%=rdbCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbDepartment.ClientID %>").prop('checked', false);
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            
        }


    }
    );

    
      $("#<%=rdbDepartment.ClientID %>").change(
    function () {
     if ($("#<%=rdbDepartment.ClientID %>").prop('checked') == true) {
            $("#<%=rdbCompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            
        }


    }
    );


       $("#<%=rdbCompany.ClientID %>").change(
    function () {
     if ($("#<%=rdbCompany.ClientID %>").prop('checked') == true) {
            $("#<%=rdbDepartment.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            
        }


    }
    );

         });

         </script>

    <div class="department_wise">



     <asp:HiddenField ID="hdn_depval" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn_grpval" ClientIDMode="Static" runat="server" />
     <asp:HiddenField ID="hdn_compval" ClientIDMode="Static" runat="server" />
    <asp:HiddenField ID="hdn_qry" ClientIDMode="Static" runat="server" />  
        

  <div style="text-align: center;"><h2 class="neg-stck-head">Negative Stock Report</h2></div>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>

<div class="neg-stck-second-panel">
	<%--<div class="col-md-1 col-sm-1 col-xs-12">
		
	</div>--%>
	<div class="col-md-12 col-sm-12 col-xs-12 all-col">
		<div class="neg-stck-all-section">
			<asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
		</div>
		<div class="neg-stck-company-section">
			<asp:RadioButton ID="rdbCompany" name="All"  runat="server" Text="Company" /><asp:DropDownList class="form-control" id="ddlComapny" runat="server" placeholder="Godown" AutoPostBack="true"  OnSelectedIndexChanged="ddlComapny_SelectedIndexChanged"></asp:DropDownList>
		</div>
		<div class="neg-stck-group-section">
			<asp:RadioButton ID="rdbGroup" name="All" runat="server" Text="Group" />
			<asp:DropDownList class="form-control" id="ddlGroup" runat="server" placeholder="Group" AutoPostBack ="true" OnSelectedIndexChanged="ddlGroup_SelectedIndexChanged"></asp:DropDownList>
		</div>
		<div class="neg-stck-dep-section">
			<asp:RadioButton ID="rdbDepartment" name="All"  runat="server" Text="Department" />
			<asp:DropDownList class="form-control" id="ddlDepartment" runat="server" placeholder="Department" AutoPostBack ="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged"></asp:DropDownList>
		</div>
		<div class="neg-stck-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
		<div class="neg-stck-godown-section">
			<span>Godown:</span><asp:DropDownList class="form-control" id="ddlGodown" runat="server" placeholder="Godown"></asp:DropDownList>
		</div>
	</div>
	<%--<div class="col-md-3 col-sm-3 col-xs-12">
		
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12">
		
	</div>--%>
	
</div>

<div class="neg-stck-top-panel">
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="neg-stck-btn-section">
			<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Submit" onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
<div class="sale_report">
<%-- <table class="cashmemo_header">
     <tr>
         <td> Branch:</td>
         <td></td>
       
       

          <td> Godown:</td>
         <td colspan ="2"></td>
        
         <td></td><td colspan="2"></td>
         <td></td>
        <td>  </td>
       </tr>
     <tr>
          <td></td> 
         <td></td>
         <td></td> 
         <td colspan ="2"></td> 
         
         </tr>
    
           <tr>
          <td></td> 
         <td ></td> 
         <td></td> 
         <td ></td> 
         
         </tr>
                     
      </table>--%>
        <rsweb:ReportViewer ID="ReportViewer1" CssClass="neg-stck-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1100px" PageCountMode="Actual">

    </rsweb:ReportViewer>
    </div>


</div>
</asp:Content>


