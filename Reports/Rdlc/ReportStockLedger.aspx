﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="ReportStockLedger.aspx.cs" Inherits="ReportStockLedger" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/ReportStockLedger.css" rel="stylesheet" />
	<link href="../css/ReportStockLedgerNew.css" rel="stylesheet" />


<script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
       <script type="text/javascript">

		   function myFunction1() {
			   var input, filter, table, tr, td, i;
			   input = document.getElementById("myInput1");
			   filter = input.value.toUpperCase();
			   table = document.getElementById("tblist");
			   tr = table.getElementsByTagName("tr");
			   for (i = 0; i < tr.length; i++) {
				   td = tr[i].getElementsByTagName("td")[0];
				   if (td) {
					   if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
						   tr[i].style.display = "";
					   } else {
						   tr[i].style.display = "none";
					   }
				   }
			   }
		   }
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
	frameDoc.document.write('<html><head></head>');
	frameDoc.document.write('<div  style = "text-align: center">Stock Ledger Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

   function RptHeadDetail() {
   var value = 'GoDown:' + $('#<%= ddlgodown.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
   return value;
         }
</script>
<script type ="text/javascript">
    $(document).ready(
function () {
			$("#td_1").hide();
			$("#myInput1").hide();

    $("#txtCode").change(
    function () {
        
        $("#<%=ddlProducts.ClientID %>").val($(this).val());

    }
    );



    $("#<%=ddlProducts.ClientID %>").change(
    function () {
       
        $("#txtCode").val($(this).val());

    }
    );


			$("#<%=rdbCode.ClientID %>").change(
				function () {

					if ($("#<%=rdbCode.ClientID %>").prop('checked') == true) {
						$("#<%=rdbName.ClientID %>").prop('checked', false);
                       
                        $("#<%=ddlProducts.ClientID %>").show();
                        $("#<%=txtCode.ClientID %>").show();
						$("#td_1").hide();
						$("#sel_table").hide();
						$("#myInput1").hide();
                       
					}


				}
			);



		$("#<%=rdbName.ClientID %>").change(
				function () {

					if ($("#<%=rdbName.ClientID %>").prop('checked') == true) {
						$("#<%=rdbCode.ClientID %>").prop('checked', false);
						$("#<%=ddlProducts.ClientID %>").hide();
                        $("#<%=txtCode.ClientID %>").hide();

						$("#td_1").show();
						$("#sel_table").show();
						$("#myInput1").show();
					}


				}
			);


			$("input[name ='group']").click(function () {
				var ProductId = $('input[name="group"]:radio:checked').map(function () {
					return this.value;
				}).get();


                $("#<%=hdn_productval.ClientID %>").val(ProductId);
                $("#<%=txtCode.ClientID %>").val(ProductId);
                
			 });


    <%--$("#<%=rdbFinished.ClientID %>").change(
    function () {

        if ($("#<%=rdbFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSemiFinished.ClientID %>").change(
    function () {
        if ($("#<%=rdbSemiFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbRaw.ClientID %>").change(
    function () {
        if ($("#<%=rdbRaw.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
        }


    }
    );--%>

}
);
</script>

     <asp:HiddenField ID="hdn_productval" ClientIDMode="Static" runat="server" />
    <div class="department_wise">

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
           <div style="text-align: center;"><h2 class="stck-ledger-head">Stock Ledger Report</h2></div>
<div class="stck-ledger-top-panel">
	<div class="col-md-4 col-sm-4 col-xs-12 stck-ledger-cst-col code-col">
		<div class="stck-ledger-code-section">
			<asp:RadioButton ID="rdbCode" name="All" runat="server" Text="By Code" Checked="True" />
			<asp:TextBox ID = "txtCode" runat="server" Text = "0"  ClientIDMode = "Static" CssClass="form-control stck-code-txt-code" ></asp:TextBox>
			<asp:DropDownList ID ="ddlProducts" runat="server"   CssClass="form-control stck-code-product-list"></asp:DropDownList>
		</div>
		<div class="stck-ledger-name-section">
			<asp:RadioButton ID="rdbName" name="All"  runat="server" Text="By Name" />
			<div id="sel_table" style="display:none">
				<input type="text" id="myInput1"  class="form-control" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name" />
				<table class="table">
                        <tbody>
                           
                            <tr>
                                <td>
                                   <div style="height: 181px; overflow: auto;">
                                       <table id="tblist">
                                          <asp:Literal ID="ltProducts" runat="server"></asp:Literal>
                                       </table>
                                  </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
				
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 stck-ledger-cst-col godown-col">
		<div class="stck-ledger-godown-section">
			<span>Godown:</span><asp:DropDownList ID ="ddlgodown" runat="server" CssClass="form-control"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 stck-ledger-cst-col date-col">
		<div class="stck-ledger-date-section">
			<div class="stck-ledger-date-part date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" CssClass="form-control" type="date"></asp:TextBox>
			</div>
			<div class="stck-ledger-date-part to-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" CssClass="form-control" type="date"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 stck-ledger-cst-col btn-col">
		<div class="stck-ledger-btn-section">
			<asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btngetdata_Click" />
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
   <div class="sale_report">
<%--        <table class="table">
            <tr>

                 <td></td> 
 
                <%--<td>Type:</td>
                <td><asp:RadioButton ID="rdbFinished"  name ="all" runat="server" Text="Finished" Checked="True" AutoPostBack="True" 
                     oncheckedchanged="rdbFinished_CheckedChanged" />
                <asp:RadioButton ID="rdbSemiFinished"  name ="all" runat="server" 
                    Text="Semi-Finished" AutoPostBack="True" 
                    oncheckedchanged="rdbSemiFinished_CheckedChanged" />
             <asp:RadioButton ID="rdbRaw"  name ="all" runat="server" Text="Raw" 
                    AutoPostBack="True" oncheckedchanged="rdbRaw_CheckedChanged" />
                </td>
  
               <td></td>
              <td></td>
                <td></td>
               <td>Godown:</td>
               <td></td>
           </tr>
            <tr>
                 <td></td> 
                  <td colspan="2" >
                    
               </td>
                         

                       <td colspan="2" ></td>  
            
              

            </tr>

           <tr>
               <td>Date:</td>
               <td>

               </td>
               <td>To:</td>
               <td>
                
               </td>
                           <td>
                    
           </td>
              </tr>
         </table>--%>
       </div>
    
      <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="stck-ledger-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>

</div>
</asp:Content>



