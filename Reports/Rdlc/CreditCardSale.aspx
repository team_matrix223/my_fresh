﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="CreditCardSale.aspx.cs" Inherits="CreditCardSale" %>


<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/CreditCardSalenew.css" rel="stylesheet" />
         <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Credit Card Sale Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

       
             function RptHeadDetail() {
             var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>POS:' + $('#<%= dd_customername.ClientID %> option:selected').text() + '<br>Bank:' + $('#<%= ddBank.ClientID %> option:selected').text() + '<br>User:' + $('#<%= dd_users.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
             return value;
         }
</script>

    <div>

<div style="text-align: center;" class="credit-card-sale-head"><h2>Credit Card Sale Report</h2></div>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>

<div class="credit-card-sale-report-first-panel">
	 <div class="radio-section">
                   <span>Report Type:</span><asp:RadioButtonList ID="rdoselection"  runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Value="UserWise" Selected="True">&nbsp;UserWise&nbsp;</asp:ListItem>
                         <asp:ListItem Value="BillNoWise">&nbsp;BillNo Wise&nbsp;</asp:ListItem>
                       
                   </asp:RadioButtonList> 
                </div>

	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-branch">
		<div class="credit-card-sale-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch">
               </asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-bank">
		<div class="credit-card-sale-bank-section">
			<span>Bank:</span><asp:DropDownList ID="ddBank"   class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-date">
		<div class="credit-card-sale-date-section">
			<div class="credit-card-sale-date-part date-part"> 
				<span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control" runat="server"></asp:TextBox>
			</div>
			<div class="credit-card-sale-date-part to-part"> 
				<span>To:</span><asp:TextBox ID="txtDateTo" type="date" runat="server" class="form-control"></asp:TextBox>
			</div>
		</div>
	</div>
</div>
<div class="credit-card-sale-report-second-panel">
	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-pos">
		<div class="credit-card-sale-pos-section">
			<span>POS:</span><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-user">
		<div class="credit-card-sale-user-section">
			<span>User:</span><asp:DropDownList class="form-control" id="dd_users" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 credit-sale-btn">
		<div class="credit-card-sale-button-section">
			<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Submit" onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
<div class="sale_report">
	
        <rsweb:ReportViewer ID="ReportViewer1" CssClass="credit-card-sale-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="1100px">

    </rsweb:ReportViewer>
</div>
    


</div>
</asp:Content>

