﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="DepartmentSaleRpt.aspx.cs" Inherits="Reports_DepartmentSaleRpt" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
	<link href="../css/DepartmentSaleRptnew.css" rel="stylesheet" />
        <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Department Wise Sale Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

             function RptHeadDetail() {
             var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>POS:' + $('#<%= dd_customername.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
             return value;
         }
</script>
	<script>
		$(document).ready(function () {
		    if ($(window).width() < 768) {


		        $("#hdn_screenres").val('m');

		    }
		    else {
		        $("#hdn_screenres").val('d');
		    }
			$("#mobile-rpt").click(function () {
				$(".rpt-bill-mob-secction").toggle();
			});
			$("#mobile-rpt").click(function () {
				$(".accordion").toggleClass("active");
			});
		});
	</script>


    <div style="text-align: center;border-bottom: 1px solid #fff; margin-bottom: 10px;"><h2 class="dept-head-h2">Department Wise Sale Report</h2></div>



        <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
	
	 <asp:HiddenField ID="hdn_screenres" runat="server" ClientIDMode="Static"/>
		<div class="departmetn-sale-top-panel">
			<a id="mobile-rpt" class="btn btn-success accordion">Option</a>
	  <div class="rpt-bill-mob-secction"> 	
			<div class="col-md-3 col-sm-3 col-xs-12 dpt-sale-cst-col branch-col">
				<div class="dep-branch-section">
					<span>Branch:</span> <asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch">
					 </asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 dpt-sale-cst-col pos-col">
				<div class="dep-pos-section dep-pos-mid-section">
					<span>POS:</span> <asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 dpt-sale-cst-col date-col">
				<div class="dep-pos-section">
					 <div class="date-part date-part"><span>Date:</span><asp:TextBox ID="txtDateFrom" type="date" class="form-control" runat="server" ></asp:TextBox></div>
                    <div class="date-part  to-part"><span>To:</span><asp:TextBox ID="txtDateTo" type="date" class="form-control" runat="server"></asp:TextBox></div>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 dpt-sale-cst-col btn-col">
				<div class="dep-button-section">
					<asp:Button ID="btnGetRecords" runat="server"  CssClass="btn btn-success" Text="Submit" onclick="btnGetRecords_Click"/>
					<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
				</div>
			</div>
		</div>
  </div>
	
        <rsweb:ReportViewer ID="ReportViewer1" CssClass="department-wise-salre-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>


</asp:Content>
