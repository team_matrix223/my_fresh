﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="CurrentStock.aspx.cs" Inherits="Reports_CurrentStock" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
<link href="../css/CurrentStockNew.css" rel="stylesheet" />


 <style type="text/css">
#tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblist tr td{text-align:left;padding:2px}
#tblist tr:nth-child(even){background:#F7F7F7}
 
#tblistItem tr{border-bottom:solid 1px silver;background:#EDEDED}
#tblistItem tr td{text-align:left;padding:2px}
#tblistItem tr:nth-child(even){background:#F7F7F7}
 </style>
    <script src="../js/jquery-1.9.0.min.js"></script>

<script type="text/javascript">
    function myFunction1() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput1");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblist");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    function myFunction() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblistItem");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }

    $(document).ready(
   function () {
       $("#myInput1").hide();
       $("#td_1").hide();
       $("#<%=rdbSelect.ClientID %>").click(function () {
           $("#myInput1").show();
           $("#td_1").show();
       });
       $("#<%=rdbAll.ClientID %>").click(function () {

           $("#myInput1").hide();
           $("#td_1").hide();
       });
       $("#td_2").hide();
       $("#myInput").hide();
       $("#<%=rdbSelectGroup.ClientID %>").click(function () {
           $("#myInput").show();
           $("#td_2").show();
       });
       $("input[name ='department']").click(function () {
           var CompaniesId = $('input[name="department"]:checkbox:checked').map(function () {
               return this.value;
           }).get();

           $("#<%=hdnDept.ClientID %>").val(CompaniesId);
       });

       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>

<script type="text/javascript">

  
   
    $(document).ready(
   function () {

       
     

      
        $("#<%=rdbAllGroup.ClientID %>").click(function () {

            $("#myInput").hide();
            $("#td_2").hide();
        });
       $("input[name ='group']").click(function () {
           var ItemsId = $('input[name="group"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnGroup.ClientID %>").val(ItemsId);
       });

       $("#<%=rdbAllGroup.ClientID %>").change(
    function () {


        if ($("#<%=rdbAllGroup.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelectGroup.ClientID %>").prop('checked', false);

        }

    }
    );
       $("#<%=rdbSelectGroup.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelectGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAllGroup.ClientID %>").prop('checked', false);

        }

    }
    );



   });

</script>
    <script type ="text/javascript">
    	   function Print() {
		   var report = document.getElementById("<%=ReportViewer1.ClientID %>");
		   var div = report.getElementsByTagName("DIV");
		   var reportContents;
		   for (var i = 0; i < div.length; i++) {
			   if (div[i].id.indexOf("VisibleReportContent") != -1) {
				   reportContents = div[i].innerHTML;
				   break;
			   }
		   }
		   var frame1 = document.createElement('iframe');
		   frame1.name = "frame1";
		   frame1.style.position = "absolute";
		   frame1.style.top = "-1000000px";
		   document.body.appendChild(frame1);
		   var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
		   frameDoc.document.open();
		 				    frameDoc.document.write('<html><head></head>');
		 				    frameDoc.document.write('<div  style = "text-align: center">Current Stock Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

   function RptHeadDetail() {
       var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>Item Type:' + $('#<%= ddlItemType.ClientID %> option:selected').text() + '<br>Godown:' + $('#<%= ddlgodown.ClientID %> option:selected').text()
   return value;
         }
</script>
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnDept" runat="server" />
<asp:HiddenField ID= "hdnGroup" runat="server" />

<div class="department_wise">
       <div style="text-align: center;"><h2 class="current-stock-head">Current Stock Report</h2></div>
<div class="current-stock-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 current-stock-cst-col branch-col">
		<div class="current-stock-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 current-stock-cst-col itm-col">
		<div class="current-stock-item-section">
			<span>Item Type:</span><asp:DropDownList ID ="ddlItemType" class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 current-stock-cst-col godown-col">
		<div class="current-stock-godown-section">
			<span>Godown:</span><asp:DropDownList ID ="ddlgodown" class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 current-stock-cst-col compny col">
		<div class="current-stock-company-section">
			<span>Company:</span><asp:DropDownList ID ="ddlcompany" class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
</div>

<div class="current-stock-second-panel">
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="current-stock-radio-section">
			<asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
			<asp:RadioButton ID="rdbSelect" runat="server" Text="Selected" />
			<div  id="td_1"><span>Search</span><input type="text" id="myInput1" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name" /></div>
		</div>
		<div class="current-stock-dep-section">
			  <span>Department:</span><div style="height: 181px; overflow: auto;">
                <table style="width:100%" id="tblist">
                <asp:Literal ID="ltDepartments" runat="server"></asp:Literal>
                      
                </table>
              </div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="current-stock-radio-grp-section">
			<asp:RadioButton ID="rdbAllGroup" name="All" runat="server" Text="All" Checked="True" />
			<asp:RadioButton ID="rdbSelectGroup"   runat="server" Text="Selected" />
			<div id="td_2"><span>Search</span><input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search for names.." title="Type in a name" /></div>
		</div>
		<div class="current-stock-grop-section">
			<span>Group:</span><div style="height: 181px; overflow: auto;">
							  <table style="width:100%" id="tblistItem">
							  <asp:Literal ID="ltGroups" runat="server"></asp:Literal>
                      
							  </table>
                          </div>
		</div>
	</div>
</div>

<div class="current-stock-btn-panel">
	<asp:Button ID="Button1" runat="server"  CssClass="btn btn-success" Text="Submit" onclick="btnGetRecords_Click"/>
	<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
</div>

    <div class="sale_report">
        <%-- <table class="table">
          <tr><td>Branch:</td>
               <td> </td>
      
                  
                            <td>Item Type:</td>
                            <td></td>
                    
                            <td>Godown:</td>
                            <td></td>
                     
                            <td>Company:</td>
                            <td></td>
                         </tr>
             <tr>
                             
                       
                        

                       
                              
                     
                
                 </tr>
                  
                        <tr>
                             <td></td> 
                              <td></td>  
                       
                              <td>Search</td><td> </td>
                              <td class="headings" colspan="2">Department:</td>
                        

                           <td colspan="2">
                        
                      
                          </td>
                         </tr>
                    

                 
                          <tr>

                              <td></td> 
                              <td></td>  
                      
                              <td >Search</td><td> </td>
                              <td class="headings" colspan="2">Group:</td>
                         

                          <td colspan="2">
                          
                      
                          </td>
                         </tr>
            
       

                      <tr>          
                              <td></td>
                 <td>   </td>
                      </tr>
</table>--%>
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="current-stock-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
</div>



</div>




</asp:Content>