﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptCancelledKot.aspx.cs" Inherits="Reports_RptCancelledKot" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="../css/RptCancelledKotNew.css" rel="stylesheet" />

<script type ="text/javascript">
    	   function Print() {
		   var report = document.getElementById("<%=ReportViewer1.ClientID %>");
		   var div = report.getElementsByTagName("DIV");
		   var reportContents;
		   for (var i = 0; i < div.length; i++) {
			   if (div[i].id.indexOf("VisibleReportContent") != -1) {
				   reportContents = div[i].innerHTML;
				   break;
			   }
		   }
		   var frame1 = document.createElement('iframe');
		   frame1.name = "frame1";
		   frame1.style.position = "absolute";
		   frame1.style.top = "-1000000px";
		   document.body.appendChild(frame1);
		   var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
		   frameDoc.document.open();
		   frameDoc.document.write('<html><head></head>');
		   frameDoc.document.write('<div  style = "text-align: center">Kot Cancel Report</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
		   frameDoc.document.write(reportContents);
		   frameDoc.document.write('</body></html>');
		   frameDoc.document.close();
		   setTimeout(function () {
		       window.frames["frame1"].focus();
		       window.frames["frame1"].print();
		       document.body.removeChild(frame1);
		   }, 500);
    	   }
     function RptHeadDetail() {
                    var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() + '<br>From:' + $('#<%= txtDateFrom.ClientID %>').val() + '&nbsp;To:' + $('#<%= txtDateTo.ClientID %>').val()
             return value;
         }
</script>
    <div class="department_wise">
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
                 <div style="text-align: center;"><h2 class="kot-cancel-head">Kot Cancel Report</h2></div>
<div class="kot-cancel-top-panel">
	<div class="col-md-4 col-sm-4 col-xs-12 kot-cancel-cst-col">
		<div class="kot-cancel-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12 kot-cancel-cst-col">
		<div class="kot-cancel-date-section">
			<div class="kot-cancel-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" class="form-control" type="date" runat="server"></asp:TextBox>
			</div>
			<div class="kot-cancel-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" class="form-control" type="date" runat="server"></asp:TextBox>
			</div>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 kot-cancel-cst-col-btn">
		<div class="kot-cancel-btn-section">
			<asp:Button ID="Button1" runat="server"  CssClass="btn btn-success" Text="Submit" onclick="btnGetRecords_Click"/>
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
	</div>
</div>
   <div class="sale_report">
       <%-- <table class="table">
            <tr>
                <td>Branch:</td>
                <td></td>
          
      
                <td>Date:</td>
                <td>
        
                </td>
                <td>To:</td>
                <td>
                   
                </td>

                 <td></td>
                 <td> </td>
            </tr>
        </table>--%>
          <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="kot-cancel-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
       </div>


</div>

</asp:Content>

