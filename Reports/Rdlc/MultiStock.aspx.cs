﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class MultiStock : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            hdn_compqry.Value = "All";
            hdn_grpqry.Value = "All";
            hdn_sbgrpqry.Value = "All";
            hdn_depqry.Value = "All";
            hdn_locqry.Value = "All";
            hdn_colorqry.Value = "All";
            hdn_taxqry.Value = "All";

            BindBranches();
            ddlGodown.DataSource = new RGodownsBLL().GetAll();
            ddlGodown.DataTextField = "Godown_Name";
            ddlGodown.DataValueField = "Godown_id";
            ddlGodown.DataBind();

            ddlComapny.DataSource = new CompanyBLL().GetAll();
            ddlComapny.DataTextField = "Company_Name";
            ddlComapny.DataValueField = "Company_id";
            ddlComapny.DataBind();

            ddlGroup.DataSource = new GroupBLL().GetAll();
            ddlGroup.DataTextField = "Group_Name";
            ddlGroup.DataValueField = "Group_ID";
            ddlGroup.DataBind();

            ddlDepartment.DataSource = new DepartmentBLL().GetAll();
            ddlDepartment.DataTextField = "Prop_Name";
            ddlDepartment.DataValueField = "Prop_id";
            ddlDepartment.DataBind();


            ddlSubGroup.DataSource = new SubGroupBLL().GetAll();
            ddlSubGroup.DataTextField = "SGroup_Name";
            ddlSubGroup.DataValueField = "SGroup_id";
            ddlSubGroup.DataBind();

            ddlLocation.DataSource = new LocationBLL().GetAll();
            ddlLocation.DataTextField = "Location_Name";
            ddlLocation.DataValueField = "Location_id";
            ddlLocation .DataBind();

            ddlColor.DataSource = new ColorBLL().GetAll();
            ddlColor.DataTextField = "Color_Name";
            ddlColor.DataValueField = "Color_id";
            ddlColor.DataBind();

           


        }



        

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
   
    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPBILLCANCEL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }

    string qry = "All";
    int company = 0;
    int group = 0;
    int Department = 0;
    public void Getdata()
    {

        Int32 GodownId = Convert.ToInt32(ddlGodown.SelectedValue);
        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("[report_sp_MultiStockReport]", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Compqry", hdn_compqry.Value);
            cmd.Parameters.AddWithValue("@Grpqry", hdn_grpqry.Value);
            cmd.Parameters.AddWithValue("@Sbgrpqry", hdn_sbgrpqry.Value);
            cmd.Parameters.AddWithValue("@Deptqry", hdn_depqry.Value);
            cmd.Parameters.AddWithValue("@Locqry", hdn_locqry.Value);
            cmd.Parameters.AddWithValue("@Colorqry", hdn_colorqry.Value);
            cmd.Parameters.AddWithValue("@Taxqry", hdn_taxqry.Value);
            cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@GodownId", ddlGodown.SelectedItem.Value);
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////

            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {
        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\MultiStockRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
    protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddltax.DataSource = new TaxStructureBLL().GetAll(Convert.ToInt32(ddlBranch.SelectedItem.Value));
        ddltax.DataTextField = "Tax_Rate";
        ddltax.DataValueField = "Tax_id";
        ddltax.DataBind();
    }
    protected void ddlComapny_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chkCompany.Checked == true)
        {
            if (rdbCompany.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Company");
                status = new ReportBLL().InsertTempReport("Company", Convert.ToInt32(ddlComapny.SelectedValue.ToString()));
            }
            else
            {
                hdn_compqry.Value = ddlComapny.SelectedValue.ToString();
            }
        }
        else
        {
            hdn_compqry.Value = "All";
        }
       
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chkDepartment.Checked == true)
        {
            if (rdbDept.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Department");
                status = new ReportBLL().InsertTempReport("Department", Convert.ToInt32(ddlDepartment.SelectedValue.ToString()));
                hdn_depqry.Value = ddlDepartment.SelectedValue.ToString();
            }
            else
            {
                hdn_depqry.Value = "All";
            }
        }
        else
        {
            hdn_depqry.Value = "All";
        }
       
    }
    protected void ddlGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        int status = 0;

        if (chkGroup.Checked == true)
        {
            if (rdbGroup.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Group");
                status = new ReportBLL().InsertTempReport("Group", Convert.ToInt32(ddlGroup.SelectedValue.ToString()));
                hdn_grpqry.Value = ddlGroup.SelectedValue.ToString();
            }
            else
            {
                hdn_grpqry.Value = "All";
            }
        }
        else
        {
            hdn_grpqry.Value = "All";
        }
       
    }

    protected void ddlSubGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chkSubGrop.Checked == true)
        {
            if (rdbSubgrp.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("SubGroup");
                status = new ReportBLL().InsertTempReport("SubGroup", Convert.ToInt32(ddlSubGroup.SelectedValue.ToString()));
                hdn_sbgrpqry.Value = ddlSubGroup.SelectedValue.ToString();
            }
            else
            {
                hdn_sbgrpqry.Value = "All";
            }
        }
        else
        {
            hdn_sbgrpqry.Value = "All";
        }


    }

    protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chkLocation.Checked == true)
        {
            if (rdbloc.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Location");
                status = new ReportBLL().InsertTempReport("Location", Convert.ToInt32(ddlLocation.SelectedValue.ToString()));
                hdn_locqry.Value = ddlLocation.SelectedValue.ToString();
            }
            else
            {
                hdn_locqry.Value = "All";
            }
        }
        else
        {
            hdn_locqry.Value = "All";
        }
    }

    protected void ddlColor_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chkColor.Checked == true)
        {
            if (rdbClr.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Color");
                status = new ReportBLL().InsertTempReport("Color", Convert.ToInt32(ddlColor.SelectedValue.ToString()));
                hdn_colorqry.Value = ddlColor.SelectedValue.ToString();
            }
            else
            {
                hdn_colorqry.Value = "All";
            }
        }
        else
        {
            hdn_colorqry.Value = "All";
        }
    }

    protected void ddltax_SelectedIndexChanged(object sender, EventArgs e)
    {
        int status = 0;

        if (chktax.Checked == true)
        {
            if (rdbtax.Checked == true)
            {
                status = new ReportBLL().DeleteTempReport("Tax");
                status = new ReportBLL().InsertTempReport("Tax", Convert.ToInt32(ddltax.SelectedValue.ToString()));
                hdn_taxqry.Value = ddltax.SelectedValue.ToString();
            }
            else
            {
                hdn_taxqry.Value = "All";

            }
        }
        else
        {
            hdn_taxqry.Value = "All";
        }
    }

    protected void chkCompany_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkCompany.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Company");
            hdn_compqry.Value = "All";
            rdbCompany.Enabled = false;
            ddlComapny.Enabled = false;
            rdbCompany.Checked = false;
            rdbAllCmpny.Checked = true;

        }
        else
        {
            rdbCompany.Enabled = true;
            ddlComapny.Enabled = true;
        }
    }
    protected void chkGroup_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkGroup.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Group");
            hdn_grpqry.Value = "All";
            rdbGroup.Enabled = false;
            ddlGroup.Enabled = false;
            rdbGroup.Checked = false;
            rdbAllGrp.Checked = true;
        }
        else
        {
            rdbCompany.Enabled = true;
            ddlComapny.Enabled = true;
        }
    }

    protected void chkDepartment_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkDepartment.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Department");
            hdn_depqry.Value = "All";

            
            rdbDept.Enabled = false;
            ddlDepartment.Enabled = false;
            rdbDept.Checked = false;
            rdbAllDept.Checked = true;
        }
        else
        {
            rdbDept.Enabled = true;
            ddlDepartment.Enabled = true;
        }
    }
    protected void chkLocation_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkLocation.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Location");
            hdn_locqry.Value = "All";
           
            rdbloc.Enabled = false;
            ddlLocation.Enabled = false;
            rdbloc.Checked = false;
            rdbAllLoc.Checked = true;
        }
        else
        {
            rdbloc.Enabled = true;
            ddlLocation.Enabled = true;
        }
    }
    protected void chkSubGrop_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkSubGrop.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("SubGroup");
            hdn_sbgrpqry.Value = "All";
           
            rdbSubgrp.Enabled = false;
            ddlSubGroup.Enabled = false;
            rdbSubgrp.Checked = false;
            rdbAllSbgrp.Checked = true;
        }
        else
        {
            rdbSubgrp.Enabled = true;
            ddlSubGroup.Enabled = true;
        }
    }
    protected void chktax_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chktax.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Tax");
            hdn_taxqry.Value = "All";
           
            rdbtax.Enabled = false;
            ddltax.Enabled = false;
            rdbtax.Checked = false;
            rdbAlltax.Checked = true;
        }
        else
        {
            rdbtax.Enabled = true;
            ddltax.Enabled = true;
        }
    }
    protected void chkColor_CheckedChanged(object sender, EventArgs e)
    {
        int status = 0;
        if (chkColor.Checked == false)
        {

            status = new ReportBLL().DeleteTempReport("Color");
            hdn_colorqry.Value = "All";
            
            rdbClr.Enabled = false;
            ddlColor.Enabled = false;
            rdbClr.Checked = false;
            rdbAllClr.Checked = true;
        }
        else
        {
            rdbClr.Enabled = true;
            rdbAllClr.Enabled = true;
        }
    }
   
}