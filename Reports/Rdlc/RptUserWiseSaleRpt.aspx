﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptUserWiseSaleRpt.aspx.cs" Inherits="Reports_RptUserWiseSaleRpt" %>


 <%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %><asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
   

<script type ="text/javascript">
    $(document).ready(
function () {


}
);
</script>
    <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />

            
                 <div style="text-align: center;"><h2>USER WISE SALE REPORT</h2></div>
               

   
        <table class="table">

            <tr>
                <td>Branch</td>
                <td><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList></td>
                <td>POS</td> 
                <td><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList></td>
                <td><asp:RadioButton ID="UserWise" runat="server" GroupName="Filter" /><label>User Wise</label> &nbsp<asp:RadioButton ID="PosWise" runat="server" Checked="true" GroupName="Filter" /><label>Pos Wise</label></td>
          </tr>
          <tr></tr>
          <tr>
              <td>Date:</td>
              <td><asp:TextBox ID="txtDateFrom" type="date" class="form-control"  runat="server" ></asp:TextBox>
                              
                            </td>
              <td>To:</td>
              <td><asp:TextBox ID="txtDateTo" type="date"  class="form-control"  name="All" runat="server" ></asp:TextBox></td>
              <td><asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click"/></td>
          </tr>
      </table>
   
    
        <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1000px" ShowBackButton="true" ShowPrintButton="true" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" PageCountMode="Actual">

    </rsweb:ReportViewer>  
  
   


</asp:Content>

