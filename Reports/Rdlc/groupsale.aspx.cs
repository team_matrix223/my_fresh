﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class groupsale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
			txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
			txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
			BindBranches();
            BindGroups();
            ddcustomertype();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

    }
    public void DisableUnwantedExportFormat(ReportViewer ReportViewerID, string strFormatName)
    {
        FieldInfo info;

        foreach (RenderingExtension extension in ReportViewerID.LocalReport.ListRenderingExtensions())
        {
            if (extension.Name == strFormatName)
            {
                info = extension.GetType().GetField("m_isVisible", BindingFlags.Instance | BindingFlags.NonPublic);
                info.SetValue(extension, false);
            }
        }
    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPGROUPSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }
    void BindGroups()
    {
        ltGroups.Text = new RGodownsBLL().GetGroupHtml();
    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();

        //ListItem li1 = new ListItem();
        //li1.Text = "-POS-";
        //li1.Value = "0";
        //dd_customername.Items.Insert(0, li1);
    }
    public void Getdata()
    {
        string query = "";

        if (rdbAll.Checked == true)
        {
            query = "All";
        }
        else
        {
            query = hdnsale.Value;
        }

        Connection conn = new Connection();
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("report_sp_GroupSaleReport", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@qry", query);
            cmd.Parameters.AddWithValue("@FromDate", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@ToDate", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@BranchId", ddlBranch.SelectedItem.Value);
            cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedItem.Value);
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////

            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



        ReportViewer1.Reset();
        DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");
        ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\GroupSaleRpt.rdlc";
        Getdata();
        ReportViewer1.LocalReport.Refresh();
        DisableUnwantedExportFormat(ReportViewer1, "EXCELOPENXML");


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

}