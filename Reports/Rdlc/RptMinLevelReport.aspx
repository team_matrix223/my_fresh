﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptMinLevelReport.aspx.cs" Inherits="RptMinLevelReport" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/RptMinLevelReport.css" rel="stylesheet" />
	<link href="../css/RptMinLevelReportNew.css" rel="stylesheet" />
<script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
           <script type="text/javascript">
function Print() {
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">Min Level Report</div><body style = "font-weight:bold; margin-bottom: 20px">');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}
</script>
<script type ="text/javascript">
    $(document).ready(
function () {







    $("#<%=rdbFinished.ClientID %>").change(
    function () {

        if ($("#<%=rdbFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSemiFinished.ClientID %>").change(
    function () {
        if ($("#<%=rdbSemiFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbRaw.ClientID %>").change(
    function () {
        if ($("#<%=rdbRaw.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
        }


    }
    );



    $("#<%=rdbAll.ClientID %>").change(
    function () {

        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {

            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbcompany.ClientID %>").change(
    function () {
        if ($("#<%=rdbcompany.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbGroup.ClientID %>").change(
    function () {
        if ($("#<%=rdbGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbSubGroup.ClientID %>").change(
    function () {
        if ($("#<%=rdbSubGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbDept.ClientID %>").change(
    function () {
        if ($("#<%=rdbDept.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
        }


    }
    );


}
);
</script>
    <div class="department_wise">
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
           <div style="text-align: center;"><h2 class="min-lvl-head">Min Level Report</h2></div>
<div class="min-lvl-top-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-godown-section">
			<span>Godown:</span><asp:DropDownList ID ="ddlGodown" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-8 col-sm-8 col-xs-12">
		<div class="min-lvl-finish-section">
			<asp:RadioButton ID="rdbFinished"  name ="Raw" CssClass="form-control" runat="server" Text="Finished" Checked="True" AutoPostBack="True" />
			</div>
		<div class="min-lvl-smi-finish-section">
			<asp:RadioButton ID="rdbSemiFinished"  name ="Raw" CssClass="form-control" runat="server" Text="Semi-Finished" />
			</div>
		<div class="min-lvl-raw-section">
			<asp:RadioButton ID="rdbRaw"  name ="Raw" CssClass="form-control" runat="server" Text="Raw"  />
		</div>
	</div>
</div>
<div class="min-lvl-middle-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-all-section">
			<asp:RadioButton ID="rdbAll"  name ="all" CssClass="form-control" runat="server" Text="All" Checked="True" AutoPostBack="True" />
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-compny-section">
			<asp:RadioButton ID="rdbcompany" CssClass="form-control" name ="all" runat="server" Text="Company"  AutoPostBack="True" />
			<asp:DropDownList ID ="ddlcompany" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-group-section">
			<asp:RadioButton ID="rdbGroup"  CssClass="form-control" name ="all" runat="server" Text="Group"  AutoPostBack="True" />
			<asp:DropDownList ID ="ddlGroup" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	
</div>
<div class="min-lvl-last-panel">
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-sub-group-section">
			<asp:RadioButton ID="rdbSubGroup" CssClass="form-control"  name ="all" runat="server" Text="Sub-Group"  AutoPostBack="True" />
			<asp:DropDownList ID ="ddlSubGroup" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-dep-section">
			<asp:RadioButton ID="rdbDept" CssClass="form-control"  name ="all" runat="server" Text="Department"  AutoPostBack="True" />
			<asp:DropDownList ID ="ddlDepartment" CssClass="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="min-lvl-btn-section">
			<asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btnGetRecords_Click" />
			<button id="PrintButton" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button>
		</div>
		
	</div>
</div>

   <div class="sale_report">
       <%-- <table class="table">

           <tr>
               <td>Godown:</td>
               <td></td>
               <td></td>
        
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
       
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td>
                    
           </td>
            </tr>
       </table>--%>
         <rsweb:ReportViewer ID="ReportViewer1" runat="server" CssClass="min-lvl-rpt-cls" Font-Names="Verdana" Font-Size="8pt" Height="1000px" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px">

        </rsweb:ReportViewer>
    </div>
    


</div>
</asp:Content>

