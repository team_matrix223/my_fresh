﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="smlPrinterTax.aspx.cs" Inherits="Reports_Rdlc_smlPrinterTax" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91"
 Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
     <link href="../css/taxwisesalerpt.css" rel="stylesheet" />

       <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.0.min.js"></script>
   <script type="text/javascript">

         function Print() {
        
    var report = document.getElementById("<%=ReportViewer1.ClientID %>");
    var div = report.getElementsByTagName("DIV");
    var reportContents;
    for (var i = 0; i < div.length; i++) {
        if (div[i].id.indexOf("VisibleReportContent") != -1) {
            reportContents = div[i].innerHTML;
            break;
        }
    }
    var frame1 = document.createElement('iframe');
    frame1.name = "frame1";
    frame1.style.position = "absolute";
    frame1.style.top = "-1000000px";
    frame1.style.height = "1000px";
    document.body.appendChild(frame1);
    var frameDoc = frame1.contentWindow ? frame1.contentWindow : frame1.contentDocument.document ? frame1.contentDocument.document : frame1.contentDocument;
    frameDoc.document.open();
    frameDoc.document.write('<html><head></head>');
    frameDoc.document.write('<div  style = "text-align: center">'+ $("#<%= rdoselection.ClientID %> input:checked").next().text()  + '</div><body style = "font-weight:bold; margin-bottom: 20px"><div><lable>' + RptHeadDetail() + '</label></div>');
    frameDoc.document.write(reportContents);
    frameDoc.document.write('</body></html>');
    frameDoc.document.close();
    setTimeout(function () {
        window.frames["frame1"].focus();
        window.frames["frame1"].print();
        document.body.removeChild(frame1);
    }, 500);
}

       function RptHeadDetail() {
           var FromDate = new Date(Date.parse($('#<%= tbfrom.ClientID %> ').val())).format("dd-MM-yyyy");
           var ToDate = new Date(Date.parse($('#<%= tbto.ClientID %> ').val())).format("dd-MM-yyyy");
           var value = 'Branch:' + $('#<%= ddlBranch.ClientID %> option:selected').text() 
             return value;
         }
     

</script>
   <div style="text-align: center;" class="tax-wise-sale-report-heading"><h2>Tax Wise Sale Report</h2></div>
	<div class="col-sm-12 col-md-12 col-xs-12" style="border-bottom:1px solid #fff;">
                <div class="radio-section">
                   <span>Report Type:</span><asp:RadioButtonList ID="rdoselection"  runat="server" RepeatDirection="Horizontal">
                           
                         <asp:ListItem Value="HSNWise">&nbsp;HSN Wise&nbsp;</asp:ListItem>
                        <asp:ListItem Value="TaxWise" >&nbsp;Tax Wise&nbsp;</asp:ListItem>
                       
                   </asp:RadioButtonList> 
                </div>
            </div>
    <div class="tax-sale-report-panel">
       <!-- <div class="container">
        <div class="row">-->
		<div class="col-md-4 col-sm-4 col-xs-12 tax-sale-report-cst-col">
				<div class="pos-section">
					<div class="pos-name">
                        <span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
                     </div>
				</div>
			</div>
		
		 <div class="col-sm-4 col-md-4 col-xs-12 tax-sale-report-cst-col">
						
                <div class="date-section">
                    <div class="date-part part-one-date"><span>Date:</span>
						<asp:TextBox ID="tbfrom" CssClass="form-control" type="date" runat="server"></asp:TextBox>
                    </div>
                    <div class="date-part  part-two-date"><span>To:</span><asp:TextBox ID="tbto" CssClass="form-control"   type="date" runat="server"></asp:TextBox></div>
                </div>
            </div>
		
		
            
           
           
			
            <div class="col-sm-4 col-md-4 col-xs-12"a>
                <div class="btn-section">   
                   <asp:Button ID="btngetdata" runat="server" CssClass="btn btn-success" Text="Submit" OnClick="btngetdata_Click" />  
                   <button id="PrintButton" style="display:none" type="button" onclick="Print()" class="btn btn-danger"><i class="fa fa-print" style="font-size: 20px;"></i></button> 
                 </div> 
            </div>
      <!--  </div>
        </div> -->
    </div>
   
  <div>
    <rsweb:ReportViewer ID="ReportViewer1"  CssClass="tax-wise-sale-rpt-cls" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="1100" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="900px" HyperlinkTarget="_blank">

    </rsweb:ReportViewer>
      </div>
</asp:Content>



