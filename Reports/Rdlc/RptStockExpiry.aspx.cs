﻿
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class RptStockExpiry : System.Web.UI.Page
{
    Connection conn = new Connection();

    protected void Page_Load(object sender, EventArgs e)
    {



        if (!IsPostBack)
        {

			txtDateFrom.Text = DateTime.Now.ToString("yyyy-MM-dd");
			txtDateTo.Text = DateTime.Now.ToString("yyyy-MM-dd");
			ListItem li = new ListItem();
            li.Value = "0";
            li.Text = "--Select--";
          



            ddlgodown.DataSource = new RGodownsBLL().GetAll();
            ddlgodown.DataTextField = "Godown_Name";
            ddlgodown.DataValueField = "Godown_id";
            ddlgodown.DataBind();
          

            ddlcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddlcompany.DataTextField = "Company_Name";
            ddlcompany.DataValueField = "Company_ID";
            ddlcompany.DataBind();
            //ddlcompany.Items.Insert(0, li);

            ddlDepartment.DataSource = new RGodownsBLL().GetAllDepartment();
            ddlDepartment.DataTextField = "Prop_Name";
            ddlDepartment.DataValueField = "Prop_ID";
            ddlDepartment.DataBind();
            //ddlDepartment.Items.Insert(0, li);

        }



        CheckRole();
       




    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPSTOCKEXPIRY));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }


    public void Getdata()
    {

        Int32 GodownId = Convert.ToInt32(ddlgodown.SelectedValue);

        Int32 CompanyId = Convert.ToInt32(ddlcompany.SelectedValue);
        Int32 Department = Convert.ToInt32(ddlDepartment.SelectedValue);

        string Type = "";
        if (rdbFinished.Checked == true)
        {
            Type = "3";
        }
        else if (rdbSemiFinished.Checked == true)
        {
            Type = "5";
        }
        else if (rdbRaw.Checked == true)
        {
            Type = "1";
        }

        string TypeWise = "";
        if (rdbAllcom.Checked == true)
        {
            TypeWise = "ALL";
        }
        else if (rdbSelCompany.Checked == true)
        {
            TypeWise = "Company";
        }
        else if (rdbSelDept.Checked == true)
        {
            TypeWise = "Department";
        }



        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

     

      


        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            ///////MainReport Start/////////
            con.Open();
            SqlCommand cmd = new SqlCommand("Report_sp_ExpiryReportRefWise", con);
            cmd.CommandType = CommandType.StoredProcedure;
  
            cmd.Parameters.AddWithValue("@DateFrom", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@DateTo", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@Godown_Id", GodownId);
            cmd.Parameters.AddWithValue("@Type", Type);
            cmd.Parameters.AddWithValue("@TypeWise", TypeWise);
            cmd.Parameters.AddWithValue("@Department_Id", Department);
            cmd.Parameters.AddWithValue("@Company_Id", CompanyId);
            cmd.Parameters.AddWithValue("@BranchId", BranchId);

            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ///////MainReport End/////////



            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));



        }



    }
    protected void btngetdata_Click(object sender, EventArgs e)
    {
        string ReportPath = "";
        if (rdbRefWise.Checked == true)
        {
            ReportPath = "Reports\\Rdlc\\RdlcDesign\\StockExpiryRpt.rdlc";


        }
        else
        {
            ReportPath = "Reports\\Rdlc\\RdlcDesign\\StockConsolidateRpt.rdlc";
        }

        ReportViewer1.Reset();
        ReportViewer1.LocalReport.ReportPath = ReportPath;
        Getdata();
        ReportViewer1.LocalReport.Refresh();


    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }

   
}