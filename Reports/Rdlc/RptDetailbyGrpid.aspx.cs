﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_Rdlc_RptDetailbyGrpid : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string Queryval = Request.QueryString["q"].ToString();

 

            ReportViewer1.Reset();
            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RdlcDesign\\RptDetailById.rdlc";
            Getdata(Queryval);
            ReportViewer1.LocalReport.Refresh();
        }

        CheckRole();
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPBILLDETAIL));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../../index.aspx");

        }


    }
    public void Getdata(string groupid)
    {
        Connection conn = new Connection();

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("Report_Sp_TaxWiseSaleReportNEW", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BillFrom", Session["fromdate"]);
            cmd.Parameters.AddWithValue("@BillTo", Session["todate"]);
            cmd.Parameters.AddWithValue("@Type", "Groupdetailbyid");
            cmd.Parameters.AddWithValue("@FromDate", Session["fromdate"]);
            cmd.Parameters.AddWithValue("@ToDate", Session["todate"]);
            cmd.Parameters.AddWithValue("@BranchId", Session["branchid"]);
            cmd.Parameters.AddWithValue("@pos_id", Session["pos_id"]);
            cmd.Parameters.AddWithValue("@BillMode", Session["billmode"]);
            cmd.Parameters.AddWithValue("@group_id", groupid.Trim());
            DataTable dt = new DataTable("dt_table");
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ReportViewer1.LocalReport.Refresh();
            ReportViewer1.LocalReport.EnableHyperlinks = true;
            ReportViewer1.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Local;
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(new Microsoft.Reporting.WebForms.ReportDataSource("DataSet1", dt));
  
        }
    }
        }