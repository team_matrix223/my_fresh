﻿<%@ Page Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptSaleGst.aspx.cs" Inherits="Reports_RptSaleGst" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="css/RptSalegstNew.css" rel="stylesheet" />
     
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type ="text/javascript">

    474

    $(document).ready(
function () {

    //    $("#<%=txtBillFrom.ClientID %>").prop('disabled', true);
    //    $("#<%=txtBillTo.ClientID %>").prop('disabled', true);
    $("#<%=rdbStateWise.ClientID %>").change(
    function () {


        if ($("#<%=rdbStateWise.ClientID %>").prop('checked') == true) {
            $("#<%=rdbPartyWise.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").prop('disabled', true);
            //            $("#<%=txtBillTo.ClientID %>").prop('disabled', true);
            $("#<%=txtDateFrom.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateTo.ClientID %>").removeAttr('disabled');

        }


    }
    );


    $("#<%=rdbGST.ClientID %>").change(
    function () {


        if ($("#<%=rdbGST.ClientID %>").prop('checked') == true) {
            $("#<%=rdbNGST.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").prop('disabled', true);
            //            $("#<%=txtBillTo.ClientID %>").prop('disabled', true);
            $("#<%=txtDateFrom.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateTo.ClientID %>").removeAttr('disabled');

        }


    }
    );
    $("#<%=rdbPartyWise.ClientID %>").change(
    function () {

        if ($("#<%=rdbStateWise.ClientID %>").prop('checked') == true) {


            $("#<%=rdbStateWise.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").removeAttr('disabled');
            //            $("#<%=txtBillTo.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateFrom.ClientID %>").prop('disabled');
            $("#<%=txtDateTo.ClientID %>").prop('disabled');

        }


    }
    );

    $("#<%=rdbNGST.ClientID %>").change(
    function () {

        if ($("#<%=rdbNGST.ClientID %>").prop('checked') == true) {


            $("#<%=rdbGST.ClientID %>").prop('checked', false);
            //            $("#<%=txtBillFrom.ClientID %>").removeAttr('disabled');
            //            $("#<%=txtBillTo.ClientID %>").removeAttr('disabled');
            $("#<%=txtDateFrom.ClientID %>").prop('disabled');
            $("#<%=txtDateTo.ClientID %>").prop('disabled');

        }


    }
    );



}
);
</script>
    <div class="department_wise">
		<div style="text-align: center;"><h2 class="state-wise-sale-head">State Wise Sale Report</h2></div>
		<div class="state-wise-sale-top-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 state-wise-sale-cst-col state-wise-sale-cst-col-branch">
				<div class="state-wise-sale-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12  state-wise-sale-cst-col state-wise-sale-cst-col-date">
				<div class="state-wise-sale-date-section">
					<div class="state-wise-sale-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
						<asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
					<div class="state-wise-sale-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
						<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 state-wise-sale-cst-col state-wise-sale-cst-col-state">
				<div class="state-wise-sale-sate-section">
					<asp:RadioButton ID="rdbStateWise" Checked="true" name ="all"   runat="server" Text="StateWise" />
					<asp:RadioButton ID="rdbPartyWise" name ="all"   runat="server" Text="PartyWise" />
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12  state-wise-sale-cst-col state-wise-sale-cst-col-gst">
				<div class="state-wise-sale-gst-section">
					<asp:RadioButton ID="rdbGST" Checked="true" name ="all"   runat="server" Text="GST" />
					<asp:RadioButton ID="rdbNGST" name ="all" runat="server" Text="NON_GST" />
				</div>
			</div>
			
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="state-wise-sale-btn-section">
					<asp:Button ID="btnGetRecords" CssClass="btn btn-success" runat="server" Text="Generate Report" onclick="btnGetRecords_Click"/>
				</div>
			</div>
		</div>
		<div>
			<asp:RadioButton ID="rdbDate" Checked="true" name ="all"   runat="server" Text="DateWise"  Visible="false"/>
			<asp:RadioButton ID="rdbBill" name ="all"   runat="server" Text="BillWise" Visible="false" />

			<asp:TextBox ID="txtBillFrom" Visible="false" runat="server"></asp:TextBox>
			<asp:TextBox ID="txtBillTo" runat="server" Visible="false"></asp:TextBox>
		</div>
          <%--  <table class="department_header">
                <tr><td colspan="100%">state WISE SALE REPORT </td></tr>
            </table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
  <div class="sale_report">
      <%--  <table class="sale_report">

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Date From:</td>
                <td>
                </td>
                <td>Date To:</td>
                <td>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
       </table>--%>
      </div>
     <div class="state-wise-sale-rpt-toolbar">
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
	</div>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>



