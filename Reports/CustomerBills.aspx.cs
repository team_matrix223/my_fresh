﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_CustomerBills : System.Web.UI.Page
{
    Connection conn = new Connection();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack == false)
        {
			txtDateFrom.Text = DateTime.Now.ToShortDateString();
			txtDateTo.Text = DateTime.Now.ToShortDateString();
			BindDDCustomer();
            BindBranches();
        }
    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

        Getdata();
    }
    public void Getdata()
    {

        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {

            con.Open();
            SqlCommand cmd = new SqlCommand("strp_getcustomerbills", con);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@fromdate", txtDateFrom.Text.Trim());
            cmd.Parameters.AddWithValue("@todate", txtDateTo.Text.Trim());
            cmd.Parameters.AddWithValue("@cashcustcode", ddlcustomer.SelectedValue);
            cmd.Parameters.AddWithValue("@BranchId", Request.Cookies[Constants.BranchId].Value);
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            gv_display.DataSource = dt;
            gv_display.DataBind();



        }

    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }
    public void BindDDCustomer()
    {
        using (SqlConnection con = new SqlConnection(conn.sqlDataString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("select customer_ID,Customer_Name from Customer_Master where isactive=1", con);
            cmd.CommandType = CommandType.Text;

            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            ddlcustomer.DataTextField = "customer_name";
            ddlcustomer.DataValueField = "customer_id";
            ddlcustomer.DataSource = dt;
            ddlcustomer.DataBind();
            ddlcustomer.Items.Insert(0, new ListItem("-All-", "0"));

        }


    }
    protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gv_display.PageIndex = e.NewPageIndex;
        this.Getdata();
    }
}