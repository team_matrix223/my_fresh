﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptMinLevelReport.aspx.cs" Inherits="RptMinLevelReport" %>
<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/RptMinLevelReport.css" rel="stylesheet" />

<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type ="text/javascript">
    $(document).ready(
function () {







    $("#<%=rdbFinished.ClientID %>").change(
    function () {

        if ($("#<%=rdbFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbSemiFinished.ClientID %>").change(
    function () {
        if ($("#<%=rdbSemiFinished.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbRaw.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbRaw.ClientID %>").change(
    function () {
        if ($("#<%=rdbRaw.ClientID %>").prop('checked') == true) {

            $("#<%=rdbFinished.ClientID %>").prop('checked', false);
            $("#<%=rdbSemiFinished.ClientID %>").prop('checked', false);
        }


    }
    );



    $("#<%=rdbAll.ClientID %>").change(
    function () {

        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {

            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbcompany.ClientID %>").change(
    function () {
        if ($("#<%=rdbcompany.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbGroup.ClientID %>").change(
    function () {
        if ($("#<%=rdbGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbSubGroup.ClientID %>").change(
    function () {
        if ($("#<%=rdbSubGroup.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbDept.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbDept.ClientID %>").change(
    function () {
        if ($("#<%=rdbDept.ClientID %>").prop('checked') == true) {

            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbcompany.ClientID %>").prop('checked', false);
            $("#<%=rdbGroup.ClientID %>").prop('checked', false);
            $("#<%=rdbSubGroup.ClientID %>").prop('checked', false);
        }


    }
    );




 





}
);
</script>
    <div class="department_wise">

            <table class="department_header">
                <tr><td colspan="100%">MIN LEVEL REPORT </td></tr>
            </table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
        <table class="sale_report">

           <tr>
               <td>Choose Godown:</td>
               <td><asp:DropDownList ID ="ddlGodown" runat="server"></asp:DropDownList></td>
               <td></td>
           </tr>


            <tr>
                <td><asp:RadioButton ID="rdbFinished"  name ="Raw" runat="server" Text="Finished" Checked="True" AutoPostBack="True" /></td>
                <td><asp:RadioButton ID="rdbSemiFinished"  name ="Raw" runat="server" Text="Semi-Finished" /></td>
                <td><asp:RadioButton ID="rdbRaw"  name ="Raw" runat="server" Text="Raw"  /></td>
            </tr>

            <tr>
                <td><asp:RadioButton ID="rdbAll"  name ="all" runat="server" Text="All" Checked="True" AutoPostBack="True" /></td>
                <td></td>
                <td></td>
            </tr>
       
            <tr>
                <td><asp:RadioButton ID="rdbcompany"  name ="all" runat="server" Text="Company"  AutoPostBack="True" /></td>
                <td><asp:DropDownList ID ="ddlcompany" runat="server"></asp:DropDownList></td>
                <td></td>
            </tr>

            <tr>
                <td><asp:RadioButton ID="rdbGroup"  name ="all" runat="server" Text="Group"  AutoPostBack="True" /></td>
                <td><asp:DropDownList ID ="ddlGroup" runat="server"></asp:DropDownList></td>
                <td></td>
            </tr>

            <tr>
                <td><asp:RadioButton ID="rdbSubGroup"  name ="all" runat="server" Text="Sub-Group"  AutoPostBack="True" /></td>
                <td><asp:DropDownList ID ="ddlSubGroup" runat="server"></asp:DropDownList></td>
                <td></td>
            </tr>

            <tr>
                <td><asp:RadioButton ID="rdbDept"  name ="all" runat="server" Text="Department"  AutoPostBack="True" /></td>
                <td><asp:DropDownList ID ="ddlDepartment" runat="server"></asp:DropDownList></td>
                <td><asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click"/></td>
            </tr>
       </table>
    </div>
    
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="850px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="100%">
    </dx:ReportViewer>

</div>
</asp:Content>

