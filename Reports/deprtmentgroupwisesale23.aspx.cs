﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

public partial class Reports_deprtmentgroupwisesale : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void ReceiptMasterGetByDate(DateTime StartDate, DateTime EndDate, int BranchId,string rdoval)
    {
        Connection con = new Connection();
        SqlParameter[] objParam = new SqlParameter[4];
        DataSet ds = new DataSet();
        objParam[0] = new SqlParameter("@StartDate", StartDate);
        objParam[1] = new SqlParameter("@EndDate", EndDate);
        objParam[2] = new SqlParameter("@BranchId", BranchId);
        objParam[3] = new SqlParameter("@qry", rdoval);

        try
        {
            ds = SqlHelper.ExecuteDataset( con.sqlDataString, CommandType.StoredProcedure,
            "report_sp_DepartmentWiseGroupSaleReport", objParam);
        }

        finally
        {
            objParam = null;
        }


    }
}