﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_CrCardSmallPrinter : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {

    int BranchId = 0;

    if (!IsPostBack)
    {

      txtDateFrom.Text = DateTime.Now.ToShortDateString();
      txtDateTo.Text = DateTime.Now.ToShortDateString();
      BindBranches();
      BindBankName();
      ddcustomertype();
            BindUsers();
    }

    CheckRole();
    if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
    {
      ddlBranch.Enabled = true;
    }
    else
    {
      ddlBranch.Enabled = false;
    }
    if (ddlBranch.SelectedValue == "0")
    {
      BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    }
    else
    {
      BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
    }
    if (txtDateFrom.Text != "")
    {
        if (rdbBillWise.Checked == true)
        {
            Rptcrcardsmall objBreakageExpiry = new Rptcrcardsmall(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, Convert.ToInt32(dd_customername.SelectedValue), Convert.ToInt32(ddBank.SelectedValue), Convert.ToInt32(dd_users.SelectedValue));
            ReportViewer1.Report = objBreakageExpiry;
        }
        else if (rdbUserWise.Checked == true)
        {
            Rptcrcardsmalluserwise objBreakageExpiry = new Rptcrcardsmalluserwise(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, Convert.ToInt32(dd_customername.SelectedValue), Convert.ToInt32(ddBank.SelectedValue), Convert.ToInt32(dd_users.SelectedValue));
            ReportViewer1.Report = objBreakageExpiry;

        }


    }

  }


  void BindBranches()
  {

    ddlBranch.DataSource = new BranchBLL().GetAll();
    ddlBranch.DataValueField = "BranchId";
    ddlBranch.DataTextField = "BranchName";
    ddlBranch.DataBind();
    //ListItem li1 = new ListItem();
    //li1.Text = "--Choose Branch--";
    //li1.Value = "0";
    //ddlBranch.Items.Insert(0, li1);

  }
  public void ddcustomertype()
  {
    pos objpos = new pos();
    objpos.req = "bindgrid";
    DataTable dt = objpos.bindgride();
    dd_customername.DataSource = dt;
    dd_customername.DataTextField = "title";
    dd_customername.DataValueField = "posid";
    dd_customername.DataBind();
    //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose", "0");
    //dd_customername.Items.Insert(0, listItem1);

  }

    public void BindUsers()
    {
        string SqlQuery = "select  Userno,user_id from  userinfo where Discontinued=0 order by user_id";
        Connection con = new Connection();
        DataTable dt = new DataTable();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        dd_users.DataSource = dt;
        dd_users.DataTextField = "user_id";
        dd_users.DataValueField = "Userno";
        dd_users.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select-", "0");
        dd_users.Items.Insert(0, listItem1);
    }
    public void BindBankName()
  {
    string SqlQuery = "select * from [dbo].[Prop_Bank]";
    Connection con = new Connection();
    DataTable dt = new DataTable();
    SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
    dad.Fill(dt);
    ddBank.DataSource = dt;
    ddBank.DataTextField = "Bank_Name";
    ddBank.DataValueField = "Bank_ID";
    ddBank.DataBind();
    System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("All", "0");
    ddBank.Items.Insert(0, listItem1);
  }
  public void CheckRole()
  {
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCREDITCARDSALE));

    string[] arrRoles = sesRoles.Split(',');

    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                select m;

    int len = roles.Count();
    if (len == 0)
    {
      Response.Redirect("../index.aspx");

    }


  }
  protected void btnGetRecords_Click(object sender, EventArgs e)
  {



  }
  protected void ReportToolbar1_Unload(object sender, EventArgs e)
  {

  }
}