﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="CustomerBills.aspx.cs" Inherits="Reports_CustomerBills" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">

    <script type="text/javascript">

        $(document).ready(
            function () {

                $("#btnPrint").click(
                    function () {
                        PrintGridData();
                    }
                );

                function PrintGridData() {

                    
                    var prtGrid = document.getElementById('dvle');
                    prtGrid.border = 0;
                    var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
                    prtwin.document.write(prtGrid.outerHTML);
                    prtwin.document.close();
                    prtwin.focus();
                    prtwin.print();
                    prtwin.close();
                }


            });
    </script>
	    <link href="css/CustomerBillsNew.css" rel="stylesheet" />
   <style>
       .modal-body {
           position: relative;
           padding: 15px;
           float: left;
           width: 100%;
       }
       .customer_bill_table {
    background-color: #ffffff96;
}
       .customer_bill_table th {
    color: #fff;
    background-color: #f06671;
    text-align: center;
    font-weight: 600;
}
       .customer_bill_table tr:nth-child(even) {
    background-color: #46c9ac;
    color: #fff;
}
       .customer_bill_table .cls_billnowprefix {
    color: #000;
}
       .customer_bill_table tr:nth-child(even) a {
    color: #fff;

}
       .customer_bill_table td {
    text-align: center;
    font-size: 14px;
}
       .customer_bill_table tr a {
    text-decoration: underline;
}


   </style>
        <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	

        <div class="department_wise">
			<div style="text-align: center;"><h2 class="customer-bills-head">Customer Bills</h2></div>
			<div class="customer-bills-top-panel">
				<div class="col-md-3 col-sm-3 col-xs-12 customer-bills-cst-col branch-col">
					<div class="customer-bills-branch-section">
						<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12 customer-bills-cst-col customer-col">
					<div class="customer-bills-customer-section">
						<span>Customer:</span><asp:DropDownList class="form-control" id="ddlcustomer" runat="server"></asp:DropDownList>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12 customer-bills-cst-col date-col">
					<div class="customer-bills-date-section">
						<div class="customer-bills-date-part">
							<span>Date:</span><asp:TextBox ID="txtDateFrom" autocomplete="off" runat="server" class="form-control"></asp:TextBox>
							<asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
						</div>
						<div class="customer-bills-date-part">
							<span>To:</span><asp:TextBox ID="txtDateTo" autocomplete="off" runat="server" class="form-control"></asp:TextBox>
							<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-sm-2 col-xs-12 customer-bills-cst-col btn-col">
					<div class="customer-bills-btn-section">
						<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success" Text="Generate Report" OnClick="btnGetRecords_Click"/>
					</div>
                    <div class="sale-detailed-print-btn-section">
					<input type="button" id="btnPrint" class="report_table_btns btn" value="Print" onclick="PrintGridData()" />
                      <%--  <asp:Button ID="btnexport"  class="report_table_btns btn btnexp" runat="server"  Text="Export To Excel" onclick="btnexport_Click"/>--%>
				</div>
				</div>
			</div>
          <%--  <table class="department_header">
                <tr><td colspan="100%"></td></tr>
            </table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
<%--        <table class="sale_report">
            <tr>
                <td>Choose Branch</td>
                <td></td>
            
                <td>Choose Customer</td>
               <td></td>
                <td></td>
            </tr>

             <tr>
                 <td>Date From:</td>
                 <td>
                 </td>
                 <td>Date To:</td>
                 <td>
                 </td>
                 <td></td>
             </tr>
        </table>--%>
       </div>

           
			<div class="customer-bills-report-section" id ="dvle">
            <asp:GridView ID="gv_display" AllowPaging="true" OnPageIndexChanging="OnPageIndexChanging" PageSize="100"  ClientIDMode="Static" CssClass= "table table-striped table-bordered table-condensed customer_bill_table"  runat="server" AutoGenerateColumns="false">
                <Columns>
            
              
                                  <asp:TemplateField HeaderText="Bill no">
                        <ItemTemplate>
                            <a style="cursor: pointer;" class="cls_billnowprefix"><%#Eval("billnowprefix")%></a>
                        
                        </ItemTemplate>
                    </asp:TemplateField>
                 
                              <asp:BoundField DataField="Bill_date" HeaderText="Bill Date"/>
                        <asp:BoundField DataField="cashcust_name" HeaderText="Customer Name"/>
                    <asp:BoundField DataField="Bill_Value" HeaderText="Amount"/>
                      <asp:BoundField  DataField="Less_dis_amount" HeaderText="Discount"/>
                     <asp:BoundField  DataField="Add_Tax_Amount" HeaderText="GST/IGST"/>
                       <asp:BoundField DataField="Net_amount" HeaderText="NetAmount"/>
                     <asp:BoundField  DataField="BillMode" HeaderText="Mode"/>
                    
                </Columns>

            </asp:GridView>

			</div>
    
  


</div>
    <button type="button" style="display:none" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="model_title"></h4>
        </div>
        <div class="modal-body">
          <p id="itemname"></p>
            <div>
                <table class= 'table table-striped table-bordered table-condensed' style="width: 40%;float: right;">
                             <tr><td>Total Amount</td><td id="total_amt"></td></tr>
                    <tr><td>Total Discount</td><td id="total_dis"></td></tr>
                     <tr><td>Net Amount</td><td id="netamt"></td></tr>
                </table>

            </div>
        
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <script>
        $(document).ready(function () {

            $(".cls_billnowprefix").click(function () {


                var billnowprefix = $(this).text();
                $("#model_title").text(billnowprefix);
                var TotalDiscount = $(this).closest('tr').find('td:eq(3)').text();
                var NetAmt = $(this).closest('tr').find('td:eq(4)').text();
                $("#total_dis").text(TotalDiscount);
                $("#netamt").text(NetAmt);
                
                GetBillDetail(billnowprefix);
                $(".btn-info").click();
            });

        });
     
        function GetBillDetail(BillNowPrefix) {


          
            $.ajax({
                type: "POST",
                data: '{ "BillNowPrefix": "' + BillNowPrefix + '"}',
                url: "/BillScreen.aspx/GetBillDetailByBillNowPrefix",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    var billdet = "<table class= 'table table-striped table-bordered table-condensed'><thead><th>Item Name</th><th>Rate</th><th>Qty</th><th>Amount</th></thead>";
                         for (var i = 0; i < obj.productLists.length; i++) {
                             billdet = billdet + "<tr><td>" + obj.productLists[i]["Item_Name"] + "</td><td>" + obj.productLists[i]["Sale_Rate"] + "</td><td>" + obj.productLists[i]["Qty"] + "</td><td class='cls_amount'>" + Number(obj.productLists[i]["Sale_Rate"] * obj.productLists[i]["Qty"]).toFixed(2) + "</td></tr>";
                         }
                         billdet = billdet + "</table>";
                 
             
                         $("#itemname").html(billdet);
                         var SumAmt = 0;
                         $('.cls_amount').each(function () {
                             SumAmt += parseFloat($(this).text());
                             
                         });
                         $("#total_amt").text(Number(SumAmt).toFixed(2));
                          

          

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);

                },
                complete: function () {
         
                }

            });


        }
    </script>
</asp:Content>

