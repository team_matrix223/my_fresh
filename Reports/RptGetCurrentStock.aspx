﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptGetCurrentStock.aspx.cs" Inherits="Reports_RptGetCurrentStock" %>
<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="css/RptGetCurrentStockSmallNew.css" rel="stylesheet" />

<script type ="text/javascript">
    $(document).ready(
function () {




}
);
</script>
    <div class="department_wise">
		<div style="text-align: center;"><h2 class="stck-small-head">STOCK REPORT</h2></div>
		<div class="stck-small-top-panel">
			<div class="col-md-4 col-sm-5 col-xs-12">
				<div class="stck-small-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-8 col-sm-7 col-xs-12">
				<div class="stck-small-btn-section">
					<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success" Text="Generate Report" onclick="btnGetRecords_Click"/>
				</div>
			</div>
		</div>
           <%-- <table class="department_header">
                <tr><td colspan="100%">STOCK REPORT </td></tr>
            </table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
       <%-- <table class="sale_report">

              <tr>
                  <td class="choos_branch"><span>Choose Branch</span>
                      </td>
              </tr>
              <tr>
                  <td class="sale_btn"></td>
              </tr>
       </table>--%>
    </div>
    <div class="stck-small-rpt-toolbar">
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    </div>
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>


