﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="smdisamt.aspx.cs" Inherits="Reports_smdisamt" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="css/smdisamtNew.css" rel="stylesheet" />



    <div class="department_wise">
		<div style="text-align: center;"><h2 class="dis-amt-head">Discount Amount</h2></div>
		<div class="dis-amt-top-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 dis-amt-cst-col dis-amt-cst-col-branch">
				<div class="dis-amt-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 dis-amt-cst-col dis-amt-cst-col-date">
				<div class="dis-amt-date-section">
					<div class="dis-amt-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                     <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
					<div class="dis-amt-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
                     <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 dis-amt-cst-col dis-amt-cst-col-pos">
				<div class="dis-amt-pos-section">
					<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 dis-amt-cst-col dis-amt-cst-col-user">
				<div class="dis-amt-user-section">
					<span>User:</span><asp:DropDownList class="form-control" id="dd_users" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dis-amt-btn-section">
					<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success" Text="Generate Report" onclick="btnGetRecords_Click"/>
				</div>
			</div>
		</div>
        <%--    <table class="department_header">
                <tr><td colspan="100%">Discount Amount  </td></tr>
            </table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
       <%-- <table class="sale_report">
             <tr><%--<td>Choose OnlinePayment</td><td>  <asp:DropDownList class="form-control" id="ddlOnlinePayment" runat="server" placeholder="Choose OnlinePayment" style="width:350px;margin-bottom:20px;height:32px" >
                                  
                                    </asp:DropDownList></td>
                                    
                  <td>Choose Branch</td>
                  <td></td>
                  <td>POS</td>
                  <td></td>
                  <td>User</td>
                  <td></td>
             </tr>

             <tr>
                 <td>Date From:</td>
                 <td>
                 </td>
                 <td>Date To:</td>
                 <td>
                 </td>
                 <td></td>
             </tr>
        </table>--%>
       </div>
	<div class="dis-amt-rpt-toolbar">	
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="880px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    </div>
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="880px">
    </dx:ReportViewer>

</div>
</asp:Content>

