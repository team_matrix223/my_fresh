﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Reports_smdisamt : System.Web.UI.Page
{
    int BranchId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {


        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            ddcustomertype();
            BindUsers();

        }

        //CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        if (txtDateFrom.Text != "")
        {
            SmDisAmt objBreakageExpiry = new SmDisAmt(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), Convert.ToInt32(dd_customername.SelectedValue), BranchId, Convert.ToInt32(dd_users.SelectedValue));
            ReportViewer1.Report = objBreakageExpiry;
        }
    }



    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        //ListItem li1 = new ListItem();
        //li1.Text = "--Choose Branch--";
        //li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }
    public void BindUsers()
    {
        string SqlQuery = "select  Userno,user_id from  userinfo where Discontinued=0 order by user_id";
        Connection con = new Connection();
        DataTable dt = new DataTable();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        dd_users.DataSource = dt;
        dd_users.DataTextField = "user_id";
        dd_users.DataValueField = "Userno";
        dd_users.DataBind();
        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select-", "0");
        //dd_users.Items.Insert(0, listItem1);
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RPTONLINEPAYMENT));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();
    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

        //getreport();

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}