﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="FOCItemSalerpt.aspx.cs" Inherits="Reports_FOCItemSalerpt" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<%@ Register TagPrefix="dx" Namespace="DevExpress.XtraReports.Web" Assembly="DevExpress.XtraReports.v11.2.Web" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/FOCItemSalerpt.css" rel="stylesheet" />
	<link href="css/FOCItemSalerptNew.css" rel="stylesheet" />

   <style type="text/css">
 #tblist tr{border-bottom:solid 1px silver;background:#EDEDED}
 #tblist tr td{text-align:left;padding:2px}
  #tblist tr:nth-child(even){background:#F7F7F7}
 </style>

<%-- <link href="../js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="../js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="../js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="../js/grid.locale-en.js" type="text/javascript"></script>
     <link href="../js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="../js/jquery-ui.js"></script>


    <script src="../js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/jquery.uilock.js"></script>--%>

 
     
     
 <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">
    function myFunction1() {
        var input, filter, table, tr, td, i;
        input = document.getElementById("myInput1");
        filter = input.value.toUpperCase();
        table = document.getElementById("tblist");
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }
    $(document).ready(
   function () {
       $("#myInput1").hide();
       $("#td_1").hide();
       
       $("#<%=rdbSelect.ClientID %>").click(function () {
           $("#myInput1").show();
           $("#td_1").show();
       });
       $("#<%=rdbAll.ClientID %>").click(function () {
           $("#myInput1").hide();
           $("#td_1").hide();
       });

       $("input[name ='item']").click(function () {
           var CompaniesId = $('input[name="item"]:checkbox:checked').map(function () {
               return this.value;
           }).get();


           $("#<%=hdnsale.ClientID %>").val(CompaniesId);


       });





       $("#<%=rdbAll.ClientID %>").change(
    function () {


        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbSelect.ClientID %>").prop('checked', false);

        }
    }
    );
       $("#<%=rdbSelect.ClientID %>").change(
    function () {

        if ($("#<%=rdbSelect.ClientID %>").prop('checked') == true) {


            $("#<%=rdbAll.ClientID %>").prop('checked', false);

        }


    }
    );

       //       $("#btnAdd").click(
       //       function () {

       //           $.uiLock('');


       //           var DateFrom = $("#txtDateFrom").val();
       //           var DateTo = $("#txtDateTo").val();
       //           var arrQry = "";
       //           if ($("#rdoAll").prop("checked") == true) {
       //               arrQry = 'All'
       //           }
       //           else {

       //               arrQry = $('input[name="company"]:checkbox:checked').map(function () {
       //                   return this.value;
       //               }).get();

       //           }
       //           

       //       });






   });




</script>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnsale" runat="server" />
<div class="department_wise">
	<div style="text-align: center;"><h2 class="foc-item-head">FOC ITEM WISE SALE REPORT</h2></div>
	<div class="foc-item-top-panel">
		<div class="col-md-3 col-sm-3 col-xs-12 foc-item-cst-col-branch">
			<div class="foc-item-branch-section">
				<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
			</div>
		</div>
		<div class="col-md-3 col-sm-3 col-xs-12 foc-item-cst-col-date">
			<div class="foc-item-date-section">
				<div class="foc-item-date-part">
					<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                                <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
				</div>
				<div class="foc-item-date-part">
					<span>To:</span><asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
                            <asp:TextBox ID="txtDateTo" name="All" runat="server" class="form-control"></asp:TextBox>
				</div>

			</div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12 foc-item-cst-col-item">
			<div class="foc-item-item-section">
				<span>Items:</span>
				<div class="foc-item-radio-section">
					<asp:RadioButton ID="rdbAll" name="All" runat="server" Text="All" Checked="True" />
					<asp:RadioButton ID="rdbSelect"   runat="server" Text="Selected" />
					<div class="search-section">
						<div id="td_1">Search</div><input type="text" id="myInput1" class="form-control" onkeyup="myFunction1()" placeholder="Search for names.." title="Type in a name" />
					</div>	
				</div>
					<div style="height: 182px; overflow: auto;" class="foc-item-tbl-section">
                        <table style="width:100%" id="tblist">
                        <asp:Literal ID="ltItems" runat="server"></asp:Literal>
                      
                        </table>
                    </div>

			</div>
		</div>
	</div>
	<div class="foc-item-second-panel">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="foc-item-btn-section">
				<asp:Button ID="btnGetRecords" CssClass="btn btn-success" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click"/>
                     <input type="button" class="btn btn-danger" id="btnPrint" value="Print" onclick="PrintGridData()" />
			 </div>
		</div>
	</div>
  <%--  <table class="department_header">
        <tr><td colspan="100%">FOC ITEM WISE SALE REPORT </td></tr>
    </table>--%>


<div class="sale_report">
        <%-- <table class="sale_report">
            <tr><td class="choos_branch"><span>Choose Branch</span>
                  </td>
            </tr>

            <tr>
                <td valign="top" width="100%">
                   <table class="report_left">
                        <tr><td><b>Select Date</b></td><td></td></tr>
                        <tr>
                           <td>From Date:</td>
                            <td>
                            </td>
                        </tr>

                        <tr>
                            <td>To Date:</td>
                        </tr>
                        <tr>
                            <td></td> 
                            <td></td>  
                        </tr>
                        <tr>
                            <td id="td_1">Search</td><td> </td>
                        </tr>
                </table>

                    <table class="report_right">
                        <tbody>
                             <tr><td class="headings" colspan="2">Items:</td></tr>
                              <tr>
                                <td colspan="2">
                                                        
                                </td>
                             </tr>
                        </tbody>
                    </table>
 
</td> 
</tr>


<%--<div style="width:1050px;overflow:scroll;height:400px">
<b></b><br /> 
<asp:GridView ID="gvUserInfo" runat="server" CellPadding="4" ForeColor="#333333" 
        Width="100%" GridLines="None" >
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
</asp:GridView>

</div>

              <tr>
                  <td class="sale_btn"></td>
              </tr>
    </table>--%>
    </div>
	<div class="foc-item-toolbar">
  <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
   </div> 
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="800px">
    </dx:ReportViewer>





    
</div>

        

</asp:Content>

