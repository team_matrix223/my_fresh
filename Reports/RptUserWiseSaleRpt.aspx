﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptUserWiseSaleRpt.aspx.cs" Inherits="Reports_RptUserWiseSaleRpt" %>


<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="css/RptUserWiseSaleRptNew.css" rel="stylesheet" />

<script>
	$(document).ready(function () {

		$("#user-sale-mob-rpt").click(function () {
			$(".rpt-user-wise-mob-section").toggle();
		});
		$("#user-sale-mob-rpt").click(function () {
			$(".accordion").toggleClass("active"); 
		});
	});
</script>
    <div class="department_wise">

           <!-- <table class="department_header">
                <tr><td colspan="100%"></td></tr>
            </table>-->
		
		 <div style="text-align: center;"><h2 class="rpt-user-wise-sale-head">USER WISE SALE REPORT</h2></div>

<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<div class="rpt-user-wise-sale-top-panel">
	<a id="user-sale-mob-rpt" class="btn btn-success accordion">Option</a>
<div class="rpt-user-wise-mob-section">
	<div class="col-md-3 col-sm-3 col-xs-12 rpt-user-wise-sale-cst-col">
		<div class="rpt-user-wise-sale-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 rpt-user-wise-sale-cst-col">
		<div class="rpt-user-wise-sale-date-section">
			<div class="rpt-user-wise-sale-date-part">
				<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                  <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
			<div class="rpt-user-wise-sale-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
                  <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
		</div>
	</div>

    <div class="col-md-3 col-sm-3 col-xs-12 rpt-user-wise-sale-cst-col radio-col">
		<div class="rpt-user-wise-sale-radio-section">
			<asp:RadioButton ID="UserWise" runat="server" AutoPostBack="true" OnCheckedChanged="rdbUser_changed"  GroupName="Filter" /><label>User Wise</label> 
			&nbsp;<asp:RadioButton ID="PosWise" runat="server" GroupName="Filter" Checked="true"  AutoPostBack="true" OnCheckedChanged="rdbpos_changed"  /><label>Pos Wise</label>
			
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 rpt-user-wise-sale-cst-col">
		<div class="rpt-user-wise-sale-pos-section">
			<asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
		    
            <asp:DropDownList ID="ddlUser" Visible="false"   class="form-control" runat="server"></asp:DropDownList>
        </div>
	</div>
    <div class="col-md-1 col-sm-1 col-xs-12 rpt-user-wise-sale-cst-col">
		<div class="rpt-user-wise-sale-pos-section">
     <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click" class="btn btn-success"/>
	</div>
        </div>
</div>
</div>
	
   <div class="sale_report">
       
   </div>
    <div class="rpt-user-wise-sale-rpt-tool-div">
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" class="rpt-user-wise-sale-toolbar">
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    </div>
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>

