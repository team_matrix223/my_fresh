﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="ItemSaleDetailedRPT.aspx.cs" Inherits="Reports_ItemSaleDetailedRPT" %>

    <%@ Register Assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
        <%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

            <asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
                <link href="css/SmallPrinterReportsGroup.css" rel="stylesheet" />
				<link href="css/ItemSaleDetailedRPTNew.css" rel="stylesheet" />

                <style type="text/css">
                    #tblist tr {
                        border-bottom: solid 1px silver;
                        background: #EDEDED;
                    }
                    
                    #tblist tr td {
                        text-align: left;
                        padding: 2px;
                    }
                    
                    #tblist tr:nth-child(even) {
                        background: #F7F7F7;
                    }
                    
                    #tblistItem tr {
                        border-bottom: solid 1px silver;
                        background: #EDEDED;
                    }
                    
                    #tblistItem tr td {
                        text-align: left;
                        padding: 2px;
                    }
                    
                    #tblistItem tr:nth-child(even) {
                        background: #F7F7F7;
                    }
                </style>
                <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
                <script type="text/javascript">
                    $(document).ready(
                        function() {

                            $("input[name ='item']").click(function() {
                                var ItemsId = $('input[name="item"]:checkbox:checked').map(function() {
                                    return this.value;
                                }).get();

                                //alert(ItemsId)
                                $("#<%=hdnItem.ClientID %>").val(ItemsId);
                            });

                            $("#<%=rdbAllItem.ClientID %>").change(
                                function() {

                                    if ($("#<%=rdbAllItem.ClientID %>").prop('checked') == true) {
                                        $("#<%=rdbSelectItem.ClientID %>").prop('checked', false);

                                    }

                                }
                            );
                            $("#<%=rdbSelectItem.ClientID %>").change(
                                function() {

                                    if ($("#<%=rdbSelectItem.ClientID %>").prop('checked') == true) {

                                        $("#<%=rdbAllItem.ClientID %>").prop('checked', false);

                                    }

                                }
                            );

                        });
                </script>

                <asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
                <asp:HiddenField ID="hdnItem" runat="server" />
                <div class="department_wise">
					<div style="text-align: center;"><h2 class="itm-sale-detl-head">Item Sale Detailed Report</h2></div>
					<div class="itm-sale-detl-top-panel">
						<div class="col-md-3 col-sm-3 col-xs-12 itm-sale-detl-cst-col branch-col">
							<div class="itm-sale-detl-branch-section">
								<span>Branch:</span><asp:DropDownList class="form-control" ID="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-12 itm-sale-detl-cst-col date-col">
							<div class="itm-sale-detl-date-section">
								<div class="itm-sale-detl-date-part">
									<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
									                  <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true" runat="server"></asp:CalendarExtender>
								</div>
								<div class="itm-sale-detl-date-part">
									<span>To:</span><asp:TextBox ID="txtDateTo" name="All" runat="server" class="form-control"></asp:TextBox>
											<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true" runat="server"></asp:CalendarExtender>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12 itm-sale-detl-cst-col pos-col">
							<div class="itm-sale-detl-pos-section">
								<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
							</div>
						</div>
						
						<div class="col-md-2 col-sm-2 col-xs-12 itm-sale-detl-cst-col btn-col">
							<div class="itm-sale-detl-btn-section">
								<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success" Text="Generate Report" OnClick="btnGetRecords_Click" />
							</div>
						</div>
					</div>
					<div class="itm-sale-detl-bottom-panel">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="itm-sale-detl-item-section">
								<span>Items:</span>
								<asp:RadioButton ID="rdbAllItem" name="All" runat="server" Text="All" Checked="True" />
                                <asp:RadioButton ID="rdbSelectItem" runat="server" Text="Selected" />
								<div style="height: 181px; overflow: auto;" class="itm-sale-detl-item-table-section">
                                    <table style="width: 100%" id="tblistItem">
                                        <asp:Literal ID="ltItems" runat="server"></asp:Literal>

                                    </table>
                                </div>

							</div>
						</div>
					</div>
                <%--    <table class="department_header">
                        <tr>
                            <td colspan="100%">Item Sale Detailed REPORT </td>
                        </tr>

                    </table>--%>

                    <div class="sale_report">
                  <%--     <table class="sale_report">
                        <tr>
                            <td class="choos_branch"><span>Choose Branch</span>
                                
                            </td>
                            <td class="choos_branch"><span>POS</span>
                                
                            </td>

                        </tr>

                        <tr>
                            <td valign="top" width="100%">

                                <table class="report_left">
                                    <tr>
                                        <td style="color:white"><b>Select Date</b></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>From Date:</td>
                                        <td>
                                            
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>To Date:</td>
                                        
                                        <td>
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            
                                        </td>
                                        <td>
											
                                        </td>
                                        <%--  <asp:Button ID="Button1" runat="server"  Text="Generate Report" 
            onclick="btnGetRecords_Click"/></td>
                                    </tr>
                                 </table>
                                <table class="report_right">
                                    <tr>
                                        <td class="headings" colspan="2">Items:</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                                                                    </td>
                                    </tr>
                                 </table>
                                </td>
                            </tr>
                                    <tr>
                                        <td class="sale_btn">
                                            
                                        </td>
                                    </tr>
                            
                          </table>--%>
                        </div>
						<div class="itm-sale-detl-item-rpt-toolbar">
                                <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" OnUnload="ReportToolbar1_Unload" Width="800px" ReportViewerID="ReportViewer1">
                                    <Items>
                                        <dx:ReportToolbarButton ItemKind="Search" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton ItemKind="PrintReport" />
                                        <dx:ReportToolbarButton ItemKind="PrintPage" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                                        <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                                        <dx:ReportToolbarLabel ItemKind="PageLabel" />
                                        <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                                        </dx:ReportToolbarComboBox>
                                        <dx:ReportToolbarLabel ItemKind="OfLabel" />
                                        <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                                        <dx:ReportToolbarButton ItemKind="NextPage" />
                                        <dx:ReportToolbarButton ItemKind="LastPage" />
                                        <dx:ReportToolbarSeparator />
                                        <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                                        <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                                        <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                                            <Elements>
                                                <dx:ListElement Value="pdf" />
                                                <dx:ListElement Value="xls" />
                                                <dx:ListElement Value="xlsx" />
                                                <dx:ListElement Value="rtf" />
                                                <dx:ListElement Value="mht" />
                                                <dx:ListElement Value="html" />
                                                <dx:ListElement Value="txt" />
                                                <dx:ListElement Value="csv" />
                                                <dx:ListElement Value="png" />
                                            </Elements>
                                        </dx:ReportToolbarComboBox>
                                    </Items>
                                    <Styles>
                                        <LabelStyle>
                                            <Margins MarginLeft="3px" MarginRight="3px" />
                                        </LabelStyle>
                                    </Styles>
                                </dx:ReportToolbar>
						</div>
                                <dx:ReportViewer ID="ReportViewer1" runat="server" Width="800px">
                                </dx:ReportViewer>

                            
                </div>
            </asp:Content>