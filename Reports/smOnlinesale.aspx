﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="smOnlinesale.aspx.cs" Inherits="smOnlinesale" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/SmallPrinterReports.css" rel="stylesheet" />
	<link href="css/smonlinesaleNew.css" rel="stylesheet" />

    <script type ="text/javascript">
        $(document).ready(
function () {




}
);
</script>
		<script>
		$(document).ready(function () {

			$("#mobile-rpt").click(function () {
				$(".rpt-bill-mob-secction").toggle();
			});
			$("#mobile-rpt").click(function () {
				$(".accordion").toggleClass("active");
});

$("#<%=rdbUserWise.ClientID %>").change(
    function () {


        if ($("#<%=rdbUserWise.ClientID %>").prop('checked') == true) {
            $("#<%=rdbBillWise.ClientID %>").prop('checked', false);


        }


    }
    );
$("#<%=rdbBillWise.ClientID %>").change(
    function () {

        if ($("#<%=rdbBillWise.ClientID %>").prop('checked') == true) {


            $("#<%=rdbUserWise.ClientID %>").prop('checked', false);


        }


    }
    );


		});
	</script>
    <div class="department_wise">

         <%--   <table class="department_header">
                <tr><td colspan="100%">ONLINE PAYMENT REPORT </td></tr>
            </table>--%>
		 <div style="text-align: center;"><h2 class="sm-online-sale-head">ONLINE PAYMENT REPORT</h2></div>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<div class="sm-online-sale-top-panel">
	<a id="mobile-rpt" class="btn btn-success accordion">Option</a>
	 <div class="rpt-bill-mob-secction rpt-bill-sec-mob-one"> 
	 <div class="tax-wise-sale-radio-section">
						
						<asp:RadioButton ID="rdbBillWise" Checked="true" name ="all"   runat="server" Text="BillWise" />
						<asp:RadioButton ID="rdbUserWise" name ="all"   runat="server" Text="UserWise" />
					
				</div>
    
    <div class="col-md-4 col-sm-4 col-xs-12 online-col">
    
		<div class="sm-online-sale-onlinepayment-section">
			<span>Online Payment:</span><asp:DropDownList class="form-control" id="ddlOnlinePayment" runat="server" placeholder="Choose OnlinePayment"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 branch-col">
		<div class="sm-online-sale-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 date-col">
		<div class="sm-online-sale-date-section">
			<div class="sm-online-sale-date-part date-part">	
				<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                     <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
			<div class="sm-online-sale-date-part to-part">	
				<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
                     <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
		</div>
	</div>
	</div>

</div>
<div class="sm-online-sale-second-panel">
	 <div class="rpt-bill-mob-secction rpt-bill-sec-mob-sec"> 
	<div class="col-md-4 col-sm-4 col-xs-12 pos-col">
		<div class="sm-online-sale-pos-section">
			<span>POS:</span><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 user-col">
		<div class="sm-online-sale-user-section">
			<span>User:</span><asp:DropDownList class="form-control" id="dd_users" runat="server" placeholder="Choose Branch"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 btn-col">
		<div class="sm-online-sale-btn-section">
			<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success"  Text="Generate Report" onclick="btnGetRecords_Click"/>
		</div>
	</div>
</div>
	</div>
		
   <div class="sale_report">
        
       </div>
    <div class="sm-online-sale-rpt-tool-div">
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="880px" 
        ReportViewerID="ReportViewer1" class="sm-online-sale-rpt-toolbar">
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
	</div>  
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="880px">
    </dx:ReportViewer>

</div>
</asp:Content>

