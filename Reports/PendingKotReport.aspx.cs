﻿
using System.Web.UI;
using System.Web.UI.WebControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class Reports_PendingKotReport : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
        int tableid = 0;
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            bindtables();
        }

        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        if (dd_tables.SelectedValue == "0")
        {
            tableid = 0;
        }
        else
        {
            tableid = Convert.ToInt32(dd_tables.SelectedValue);
        }

        if (txtDateFrom.Text != "")
        {
            rptPendingKot objBreakageExpiry = new rptPendingKot(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId,dd_tables.SelectedValue);
            ReportViewer1.Report = objBreakageExpiry;
        }

    }
    void bindtables()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
       // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("pos_sp_TablesGetAll", con);
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);

        dd_tables.DataSource =dt;
        dd_tables.DataValueField = "tableid";
        dd_tables.DataTextField = "tablename";
        dd_tables.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Table no--";
        li1.Value = "0";
        dd_tables.Items.Insert(0, li1);

        con.Close();

    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPDELIVERYISSUE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {



    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}