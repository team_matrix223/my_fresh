﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_RptUserWiseSaleRpt : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
        EncDyc ED = new EncDyc();
        string Screen = Convert.ToString(HttpContext.Current.Request.Cookies[Constants.dbname].Value);
        Screen = ED.Decrypt(Screen);
        Connection con = new Connection();
        con.sqlDataString = Screen;
        int BranchId = 0;
    if (!IsPostBack)
    {

      txtDateFrom.Text = DateTime.Now.ToShortDateString();
      txtDateTo.Text = DateTime.Now.ToShortDateString();
      BindBranches();
      ddcustomertype();
            bindusers();
    }
    string Filter = "";
    if (UserWise.Checked==true)
    {
      Filter = "UserWise";
    }
    else if(PosWise.Checked==true)
    {
      Filter = "PosWise";
    }
    CheckRole();
    if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
    {
      ddlBranch.Enabled = true;
    }
    else
    {
      ddlBranch.Enabled = false;
    }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
    if (txtDateFrom.Text != "")
    {
      UserDetailSaleRpt objBreakageExpiry = new UserDetailSaleRpt(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, Convert.ToInt32(ddlUser.SelectedValue), Convert.ToInt32(dd_customername.SelectedValue), Filter);
      ReportViewer1.Report = objBreakageExpiry;
    }
  }


  void BindBranches()
  {

    ddlBranch.DataSource = new BranchBLL().GetAll();
    ddlBranch.DataValueField = "BranchId";
    ddlBranch.DataTextField = "BranchName";
    ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    void bindusers()
    {
        int BranchId = 0;
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
        ddlUser.DataSource = new UserBLL().GetAll(BranchId);
        ddlUser.DataValueField = "UserNo";
        ddlUser.DataTextField = "User_Id";
        ddlUser.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose User--";
        li1.Value = "0";
        ddlUser.Items.Insert(0, li1);


    }
    public void ddcustomertype()
  {
    pos objpos = new pos();
    objpos.req = "bindgrid";
    DataTable dt = objpos.bindgride();
    dd_customername.DataSource = dt;
    dd_customername.DataTextField = "title";
    dd_customername.DataValueField = "posid";
    dd_customername.DataBind();
    ListItem li1 = new ListItem();
    li1.Text = "--All--";
    li1.Value = "0";
    dd_customername.Items.Insert(0, li1);

        //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose", "0");
        //dd_customername.Items.Insert(0, listItem1);

    }
    public void CheckRole()
  {
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPFOCSALE));

    string[] arrRoles = sesRoles.Split(',');

    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                select m;

    int len = roles.Count();
    if (len == 0)
    {
      Response.Redirect("../index.aspx");

    }


  }

    protected void rdbUser_changed(object sender, EventArgs e)
    {

        if (UserWise.Checked == true)
        {
            PosWise.Checked = false;
            
            dd_customername.Visible = false;
            ddlUser.Visible = true;
        }

    }


    protected void rdbpos_changed(object sender, EventArgs e)
    {

        if (PosWise.Checked == true)
        {
            UserWise.Checked = false;

            dd_customername.Visible = true;
            ddlUser.Visible = false;
        }

    }





    protected void btnGetRecords_Click(object sender, EventArgs e)
  {



  }
  protected void ReportToolbar1_Unload(object sender, EventArgs e)
  {

  }
}