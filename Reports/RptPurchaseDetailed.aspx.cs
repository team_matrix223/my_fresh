﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Services;
using DevExpress.XtraPrinting;


using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text;
using ListItem = System.Web.UI.WebControls.ListItem;
using Document = iTextSharp.text.Document;

public partial class RptPurchaseDetailed : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    protected void Page_Load(object sender, EventArgs e)
    {
        int BranchId = 0;
        if (!IsPostBack)
        {
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
            ddcustomertype();
            BindOptions();
        }
        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
       
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        string BillMode = "";
        string SaleType = "";
        if (rdbAll.Checked == true)
        {
            BillMode = "All";
        }
        else if (rdbCash.Checked == true)
        {
            BillMode = "Cash";
        }
        else if (rdbCredit.Checked == true)
        {
            BillMode = "Credit";
        }
      
       



        if (rdbDetailed.Checked == true)
        {
            List<GridOption> lst = new List<GridOption>();
           
                lst = new ReportBLL().GetPurchaseOptions();
          

           
            gvUserInfo.DataSource = null;
            gvUserInfo.Visible = true;
            gvUserInfo.DataSource = new ReportDAL().GetPurchaseReport( BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId);
            gvUserInfo.DataBind();
            foreach (var item in lst)
            {
                for (int i = 0; i < gvUserInfo.Columns.Count; i++)
                {

                    if (gvUserInfo.Columns[i].HeaderText == item.GridColumn)
                    {
                        if (item.HideShow == "checked")
                        {
                            gvUserInfo.Columns[i].Visible = true;
                        }

                        else
                        {
                            gvUserInfo.Columns[i].Visible = false;
                        }

                    }


                }
            }


        }
        else
        {
          //  List<GridOption> lst = new ReportBLL().GetAllcashmemoDated();
            gvUserInfo.Visible = false;
            gvUserInfo.DataSource = null;
            //gvUserInfo2.Visible = true;
            //gvUserInfo2.DataSource = null;

            //gvUserInfo2.DataSource = new ReportDAL().GetSaleDatedReport(BillType, BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId, SaleType);
            //gvUserInfo2.DataBind();

            //foreach (var item in lst)
            //{
            //    for (int i = 0; i < gvUserInfo2.Columns.Count; i++)
            //    {

            //        if (gvUserInfo2.Columns[i].HeaderText == item.GridColumn)
            //        {

            //            gvUserInfo2.Columns[i].Visible = true;
            //        }

            //    }
            //}


        }


  

    }




    [WebMethod]
    public static string Insert(string[] OptionList, string[] Width, string OptionType)
    {
        int status = 0;

        //string[] arroptions = OptionList.Split(',');
        DataTable dt = new DataTable();
        dt.Columns.Add("Id");
        dt.Columns.Add("width");

        DataRow dr;

        for (int i = 0; i < OptionList.Length; i++)
        {
            dr = dt.NewRow();
            dr["Id"] = OptionList[i];
            dr["width"] = Width[i];
            dt.Rows.Add(dr);
        }

        if (OptionType == "Dated")
        {
            //status = new ReportBLL().InsertCashMemoDated(dt);
        }
        else
        {
           
                status = new ReportBLL().InsertPurchase (dt);
           


        }

        var JsonData = new
        {

            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    void BindOptions()
    {
        if (rdbDetailed.Checked == true)
        {

            ltOptions.Text = new ReportBLL().GetPurchaseHtml();
        }
        else
        {
          //  ltOptions.Text = new ReportBLL().GetCashMemoDatedHtml();
        }

    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();
    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        //ddlBranch.Items.Insert(0, li1);

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPCASHMEMOSALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

    }
    protected void btnpdf_Click(object sender, EventArgs e)
    {
        int BranchId = 0;
      
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }

        string BillMode = "";
        string SaleType = "";
        if (rdbAll.Checked == true)
        {
            BillMode = "All";
        }
        else if (rdbCash.Checked == true)
        {
            BillMode = "Cash";
        }
        else if (rdbCredit.Checked == true)
        {
            BillMode = "Credit";
        }
       

      
        List<GridOption> lst = new ReportBLL().GetAllcashmemo();

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=UserDetails.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        gvUserInfo.AllowPaging = false;
        gvUserInfo.DataSource = null;
        gvUserInfo.DataSource = new ReportDAL().GetPurchaseReport(BillMode, txtDateFrom.Text, txtDateTo.Text, BranchId);
        gvUserInfo.DataBind();

        foreach (var item in lst)
        {
            for (int i = 0; i < gvUserInfo.Columns.Count; i++)
            {

                if (gvUserInfo.Columns[i].HeaderText == item.GridColumn)
                {

                    gvUserInfo.Columns[i].Visible = true;
                }

            }
        }

        gvUserInfo.RenderControl(hw);
        gvUserInfo.HeaderRow.Style.Add("width", "15%");
        gvUserInfo.HeaderRow.Style.Add("font-size", "10px");
        gvUserInfo.Style.Add("text-decoration", "none");
        gvUserInfo.Style.Add("font-family", "Arial, Helvetica, sans-serif;");
        gvUserInfo.Style.Add("font-size", "8px");
        StringReader sr = new StringReader(sw.ToString());
        Document pdfDoc = new Document(PageSize.A2, 7f, 7f, 7f, 0f);
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        pdfDoc.Open();
        htmlparser.Parse(sr);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();



    }

    protected void btnexport_Click(object sender, EventArgs e)
    {

        Response.Clear();
        Response.Buffer = true;
        Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
        Response.Charset = "";
        Response.ContentType = "application/vnd.ms-excel";
        using (StringWriter sw = new StringWriter())
        {
            HtmlTextWriter hw = new HtmlTextWriter(sw);

            //To Export all pages
            gvUserInfo.AllowPaging = false;



            foreach (TableCell cell in gvUserInfo.HeaderRow.Cells)
            {
                cell.BackColor = gvUserInfo.HeaderStyle.BackColor;
            }
            foreach (GridViewRow row in gvUserInfo.Rows)
            {

                foreach (TableCell cell in row.Cells)
                {
                    if (row.RowIndex % 2 == 0)
                    {
                        cell.BackColor = gvUserInfo.AlternatingRowStyle.BackColor;
                    }
                    else
                    {
                        cell.BackColor = gvUserInfo.RowStyle.BackColor;
                    }
                    cell.CssClass = "textmode";
                }
            }

            gvUserInfo.RenderControl(hw);

            //style to format numbers to string
            string style = @"<style> .textmode { } </style>";
            Response.Write(style);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void rdbDetailed_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbDetailed.Checked == true)
        {
            hdn_chkval.Value = "Detailed";
            ltOptions.Text = new ReportBLL().GetPurchaseHtml();
        }

    }

    protected void rdbDated_CheckedChanged(object sender, EventArgs e)
    {
        if (rdbDated.Checked == true)
        {
            hdn_chkval.Value = "Dated";
            ltOptions.Text = new ReportBLL().GetCashMemoDatedHtml();
        }

    }

   

}