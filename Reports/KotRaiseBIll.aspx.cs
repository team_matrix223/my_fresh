﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.IO;
using System.Data;

public partial class KotRaiseBIll : System.Web.UI.Page
{
    public string m_getdate { get; set; }
	 public string Status { get { return Request.QueryString["Status"] != null ? Convert.ToString(Request.QueryString["Status"]) : string.Empty; } }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
			 hdndd.Value = Status;
            hdnDate.Value = DateTime.Now.ToShortDateString();
            ltDateTime.Text = "<span style='font-weight:bold;'>Today - " + String.Format("{0:dddd, MMMM d, yyyy}", DateTime.Now) + "</span>";
            m_getdate = DateTime.Now.ToString("ddddd,MMMM-dd-yyyy");
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            gvTax.DataSource = new TaxStructureBLL().GetAll(Branch);
            gvTax.DataBind();

            string strDate = new DayOpenCloseDAL().IsDayOpen(Branch);

            if (strDate == "")
            {

                Response.Redirect("index.aspx?DayOpen=Close");
            }
            BindCreditCustomers();
        }

        CheckRole();
        BindSteward();
    }



    public void BindSteward()
    {
        ddlsteward.DataSource = new EmployeeBLL().GetAll();
        ddlsteward.DataTextField = "Name";
        ddlsteward.DataValueField = "Code";
        ddlsteward.DataBind();

        ListItem li1 = new ListItem();
        li1.Text = "Choose";
        li1.Value = "0";
        ddlsteward.Items.Insert(0, li1);
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.HOLD).ToString() | m == Convert.ToInt16(Enums.Roles.UNHOLD).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString() | m == Convert.ToInt16(Enums.Roles.RATEEDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string GetTable()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        string prodData = new kotBLL().GetTable(Branch);
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetKotDet(int KotNo)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<KotDetail> lstKot = new kotBLL().GetByKotNoDet(KotNo, Branch);

        var JsonData = new
        {
            productLists = lstKot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string UpdateKotDeActive(int TableID)
    {
        int status = 0;
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();

        status = new kotBLL().UpdateKOTDeActive(TableID,Branch);
        var JsonData = new
        {
           
            Status = status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetKot(int KotNo)
    {
        KOT objkot = new KOT();
        objkot.TableID = KotNo;
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new kotBLL().GetByKot(objkot, Branch);

        var JsonData = new
        {
            KOT = objkot
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string BindEmployees()
    {

        string EmployeesData = new EmployeeBLL().GetOptions();


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            EmployeeOptions = EmployeesData

        };
        return ser.Serialize(JsonData);
    }


    void BindCreditCustomers()
    {

        ddlChosseCredit.DataSource = new CustomerBLL().GetAllCreditCustomer();
        ddlChosseCredit.DataValueField = "CCODE";
        ddlChosseCredit.DataTextField = "CNAME";
        ddlChosseCredit.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Customer--";
        li1.Value = "0";
        ddlChosseCredit.Items.Insert(0, li1);

    }

    [WebMethod]
    public static string LoadUserControl(string counter)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/AddOn.ascx");
            (userControl.FindControl("hdnAOCounter") as Literal).Text = "<input type='hidden' id='hdnAddOnCounter' value='" + counter + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }


    [WebMethod]
    public static string GetBillDetailByBillNowPrefix(string BillNowPrefix)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        List<Product> lstProducts = new BillBLL().GetByBillNowPrefix(BillNowPrefix, Branch);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            productLists = lstProducts

        };
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static string Reprint(string PrintType, string BillNowPrefix)
    {

        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        new CommonMasterDAL().Reprint(UserNo, PrintType, Branch, BillNowPrefix);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {


        };
        return ser.Serialize(JsonData);

    }







    [WebMethod]
    public static string GetAllBillSetting()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        CommonSettings ObjSettings = new CommonSettings();
        ObjSettings.Type = "Retail";
        ObjSettings.BranchId = Branch;
        List<DiscountDetail> lst = new CommonSettingsBLL().GetAllBillSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,
            DiscountDetail = lst

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Delete(string BillNowPrefix)
    {


        int Status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');


        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            Status = -10;
        }
        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        Status = new BillBLL().DeleteBill(objBill);
        var JsonData = new
        {
            billmaster = objBill,
            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertUpdate(string CustomerId, string CustomerName, decimal BillValue, decimal DiscountPer, decimal DiscountAmt, decimal AddTaxAmt, decimal NetAmt, string BillMode, string CreditBank, decimal CashAmt, decimal CreditAmt, decimal CrCardAmt, decimal RoundAmt, int CashCustCode, string CashCustName, int TableNo, decimal SerTax, string Remarks, Int32 OrderNo, string Type, Int32 EmpCode, string BillNowPrefix, string itemcodeArr, string qtyArr, string priceArr, string taxArr, string orgsalerateArr, string SurPerArr, string AmountArr, string TaxAmountArr, string SurValArr, string ItemRemarksArr, string arrTaxden, string arrVatAmtden, string arrVatden, string arrSurden, decimal OnlinePayment, decimal DeliveryCharges, decimal KKCPer, decimal KKCAmt, decimal SBCPer, decimal SBCAmt, string BillRemarks)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RETAILBILLING));

        string[] arrRoles = sesRoles.Split(',');



        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString()
                    select m;


        if (roles.Count() == 0)
        {
            status = -11;
        }






        int Status = 0;
        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //User objuser = new User()
        //{
        //    UserNo = UserNo

        //};

        //Int32 status2 = new UserBLL().chkIsLogin(objuser);

        Bill objBill = new Bill()
        {
            BillNowPrefix = BillNowPrefix,
            Customer_ID = CustomerId,
            Customer_Name = CustomerName,
            Bill_Value = BillValue,
            DiscountPer = DiscountPer,
            Less_Dis_Amount = DiscountAmt,
            Add_Tax_Amount = AddTaxAmt,
            Net_Amount = NetAmt,
            BillMode = BillMode,
            CreditBank = CreditBank,
            UserNO = UserNo,
            Bill_Type = 1,
            Cash_Amount = CashAmt,
            Credit_Amount = CreditAmt,
            CrCard_Amount = CrCardAmt,
            Round_Amount = RoundAmt,
            Bill_Printed = false,
            Passing = false,
            CashCust_Code = CashCustCode,
            CashCust_Name = CashCustName,
            Tax_Per = 0,
            R_amount = RoundAmt,
            tableno = TableNo,
            remarks = Remarks,
            servalue = SerTax,
            ReceiviedGRNNo = 0,
            EmpCode = EmpCode,
            OrderNo = OrderNo,
            BranchId = Branch,
            OnlinePayment = OnlinePayment,
            DeliveryCharges = DeliveryCharges,
            KKCPer = KKCPer,
            KKCAmt = KKCAmt,
            SBCPer = SBCPer,
            SBCAmt = SBCAmt,
            BillRemarks = BillRemarks,


        };
        //if (status2 != 0)
        //{
        string[] ItemCode = itemcodeArr.Split(',');
        string[] Qty = qtyArr.Split(',');
        string[] Price = priceArr.Split(',');
        string[] Tax = taxArr.Split(',');
        string[] OrgSaleRate = orgsalerateArr.Split(',');
        string[] SurPer = SurPerArr.Split(',');
        string[] Amount = AmountArr.Split(',');
        string[] TaxAmount = TaxAmountArr.Split(',');
        string[] SurVal = SurValArr.Split(',');
        string[] remarksdata = ItemRemarksArr.Split(',');

        string[] taxdendata = arrTaxden.Split(',');
        string[] vatamtdendata = arrVatAmtden.Split(',');
        string[] vatdendata = arrVatden.Split(',');
        string[] surchrgdendata = arrSurden.Split(',');

        DataTable dt = new DataTable();
        dt.Columns.Add("ItemCode");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Tax");
        dt.Columns.Add("Tax_Amount");
        dt.Columns.Add("OrgSaleRate");
        dt.Columns.Add("SurPer");
        dt.Columns.Add("SurVal");
        dt.Columns.Add("Remarks");

        DataTable dt1 = new DataTable();
        dt1.Columns.Add("Tax");
        dt1.Columns.Add("VatAmt");
        dt1.Columns.Add("Vat");
        dt1.Columns.Add("SurCharge");



        for (int i = 0; i < ItemCode.Length; i++)
        {
            DataRow dr = dt.NewRow();
            dr["ItemCode"] = ItemCode[i];
            dr["Rate"] = Convert.ToDecimal(Price[i]);
            dr["Qty"] = Convert.ToDecimal(Qty[i]);
            dr["Amount"] = Convert.ToDecimal(Amount[i]);
            dr["Tax"] = Convert.ToDecimal(Tax[i]);
            dr["Tax_Amount"] = Convert.ToDecimal(TaxAmount[i]);
            dr["OrgSaleRate"] = Convert.ToDecimal(OrgSaleRate[i]);
            dr["SurPer"] = Convert.ToDecimal(SurPer[i]);
            dr["SurVal"] = Convert.ToDecimal(SurVal[i]);
            dr["Remarks"] = Convert.ToString(remarksdata[i]);
            dt.Rows.Add(dr);
        }


        for (int j = 0; j < taxdendata.Length; j++)
        {

            DataRow dr1 = dt1.NewRow();
            dr1["Tax"] = Convert.ToDecimal(taxdendata[j]);
            dr1["VatAmt"] = Convert.ToDecimal(vatamtdendata[j]);
            dr1["Vat"] = Convert.ToDecimal(vatdendata[j]);
            dr1["SurCharge"] = Convert.ToDecimal(surchrgdendata[j]);
            dt1.Rows.Add(dr1);

        }


        if (objBill.BillNowPrefix == "")
        {
            Status = new kotBLL().Insert(objBill, dt, dt1);
        }
        else
        {
            Status = new BillBLL().Update(objBill, dt, dt1);
        }
        //}
        //else
        //{
        //    Status = -5;


        //}
        JavaScriptSerializer ser = new JavaScriptSerializer();


        var JsonData = new
        {
            Bill = objBill,
            BNF = objBill.BillNowPrefix,
            Status = Status
        };
        return ser.Serialize(JsonData);
    }

}