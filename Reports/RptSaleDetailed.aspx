﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptSaleDetailed.aspx.cs" Inherits="RptSaleDetailed" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/rptSaleDetailed.css" rel="stylesheet" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
<script type="text/javascript">

    var OptionType = "";
	var BillVal= 0;
    function InsertUpdate() {

        OptionType = $("#hdn_chkval").val();
      

        BillVal = $('#<%=ddlbilltype.ClientID %> option:selected').val();
        //if ($("#hdn_billtypeval").val()!="") {
        //    BillVal = $("#hdn_billtypeval").val();
        //}
       
        if (OptionType == "") {
            OptionType = "Detailed";
        }
		

        var arr_optionlist = new Array();
		var arr_txtwidth = new Array();
        var OptionsId = "";
		var txtwidth = "";
     

		$('#tblist tr').each(function () {

            var currentRow = $(this).closest("tr");
            if (currentRow.find(".cls_chk").is(":checked"))
            {

                    OptionsId = currentRow.find(".cls_chk").val();
                    txtwidth = currentRow.find(".cls_width").val();
                    arr_optionlist.push(OptionsId);
			        arr_txtwidth.push(txtwidth);
				}
			
		});
		//var DTO = { 'OptionList': '' + arr_optionlist + '', 'Width': '' + arr_txtwidth + '' , 'OptionType': '' + OptionType + '' };

		
		$.ajax({
            type: "POST",
            
			contentType: "application/json; charset=utf-8",
			url: "RptSaleDetailed.aspx/Insert",
            //data: JSON.stringify(DTO),
            data: JSON.stringify({ OptionList: arr_optionlist, Width: arr_txtwidth, OptionType: OptionType, BillVal: BillVal }),
			dataType: "json",
			success: function (msg) {

				var obj = jQuery.parseJSON(msg.d);

			},
			error: function (xhr, ajaxOptions, thrownError) {

				var obj = jQuery.parseJSON(xhr.responseText);
				alert(obj.Message);
			},
			complete: function () {
			
			}
		});



    }



    $(document).ready(
        function () {


			$("#btngridoptions").click(
                function () {
                    $("#sel_table").show();
					$("#<%=hdnOption.ClientID %>").val("");
				}
			);
			
			$("#btnSaveOptions").click(
                function () {
                   
                    InsertUpdate();
				}
			);

    $("#btnPrint").click(
    function () {
        PrintGridData();
    }
    );

    function PrintGridData() {
        
        //var prtGrid = document.getElementById('<%=gvUserInfo.ClientID %>');
        var prtGrid = document.getElementById('dvle');
        prtGrid.border = 0;
        var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
        prtwin.document.write(prtGrid.outerHTML);
        prtwin.document.close();
        prtwin.focus();
        prtwin.print();
        prtwin.close();
    }

    $("#<%=rdbAll.ClientID %>").change(
    function () {

        if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCash.ClientID %>").change(
    function () {

        if ($("#<%=rdbCash.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCredit.ClientID %>").change(
    function () {
        if ($("#<%=rdbCredit.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbCard.ClientID %>").change(
    function () {
        if ($("#<%=rdbCard.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbOnline.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbOnline.ClientID %>").change(
    function () {
        if ($("#<%=rdbOnline.ClientID %>").prop('checked') == true) {
            $("#<%=rdbAll.ClientID %>").prop('checked', false);
            $("#<%=rdbCash.ClientID %>").prop('checked', false);
            $("#<%=rdbCredit.ClientID %>").prop('checked', false);
            $("#<%=rdbCard.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbDetailed.ClientID %>").change(
    function () {
            if ($("#<%=rdbDetailed.ClientID %>").prop('checked') == true) {
                OptionType = "Detailed";
				
            $("#<%=rdbDated.ClientID %>").prop('checked', false);
        }


    }
    );
    $("#<%=rdbDated.ClientID %>").change(
    function () {
            if ($("#<%=rdbDated.ClientID %>").prop('checked') == true) {
                OptionType = "Dated";
                
            $("#<%=rdbDetailed.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbOrderSale.ClientID %>").change(
    function () {
        if ($("#<%=rdbOrderSale.ClientID %>").prop('checked') == true) {
            $("#<%=rdbWidoutOrderSale.ClientID %>").prop('checked', false);
        }


    }
    );

    $("#<%=rdbWidoutOrderSale.ClientID %>").change(
    function () {
        if ($("#<%=rdbWidoutOrderSale.ClientID %>").prop('checked') == true) {
            $("#<%=rdbOrderSale.ClientID %>").prop('checked', false);
        }


    }
    );


}
);
</script>

    <div class="cashmemo_wise">

		<asp:HiddenField ID="hdn_chkval" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdn_billtypeval" ClientIDMode="Static" runat="server" />
		<div style="text-align: center;"><h2 class="sale-detailed-head">CASHMEMO WISE SALE REPORT</h2></div>
		<div class="sale-detailed-top-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 sale-detailed-cst-col">
				<div class="sale-detailed-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 sale-detailed-cst-col">
				<div class="sale-detailed-pos-section">
					<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-12 sale-detailed-cst-col">
				<div class="sale-detailed-biltype-section">
					<span>BillType:</span><asp:DropDownList class="form-control" id="ddlbilltype" runat="server" ClientIDMode="Static" AutoPostBack="true"   placeholder="Choose BillType" OnSelectedIndexChanged="ddlbilltype_SelectedIndexChanged">
             <asp:ListItem Value="1" Text="Retail Bill"></asp:ListItem> 
             <asp:ListItem Value="2" Text="GST Bill"></asp:ListItem>  
             </asp:DropDownList>	
				</div>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 sale-detailed-cst-col">
				<div class="sale-detailed-date-section">
					<div class="sale-detailed-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
						<asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server" ></asp:CalendarExtender>
					</div>
					<div class="sale-detailed-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox> 
             <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender> 
					</div>
				</div>
			</div>
		</div>
		
		<div class="sale-detailed-third-panel">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="sale-detailed-billmode-section">
					<span>Bill Mode:</span>
					<asp:RadioButton ID="rdbAll"  name ="all" runat="server" Text="All" Checked="True" />
					<asp:RadioButton ID="rdbCash" name ="all"  runat="server" Text="Cash" />
					<asp:RadioButton ID="rdbCredit" name ="all"  runat="server" Text="Credit" />
					<asp:RadioButton ID="rdbCard" name ="all"  runat="server" Text="Credit Card" />
					<asp:RadioButton ID="rdbOnline" name ="all"  runat="server" Text="Online Payment" />
				</div>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="sale-detailed-option-section">
					<span>Option:</span><asp:RadioButton ID="rdbDetailed"   runat="server" Text="Detailed" AutoPostBack="true" Checked="True" OnCheckedChanged="rdbDetailed_CheckedChanged"  />
					<asp:RadioButton ID="rdbDated"  runat="server" Text="Dated" AutoPostBack="true" OnCheckedChanged="rdbDated_CheckedChanged" />
					<asp:RadioButton ID="rdbOrderSale"   runat="server" Text="Order Sale" />
					<asp:RadioButton ID="rdbWidoutOrderSale"  runat="server" Text="Without Order Sale" />
				</div>
			</div>
		</div>
		<div class="sale-detailed-fourth-panel">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="sale-detailed-grid-section">
					<div id="btngridoptions" class="report_table_btns">Grid Options</div><div id="btnSaveOptions" class="report_table_btns">Save</div>
				</div>
				<div id="sel_table" style="display:none">
					<table class="table">
                        <tbody>
                            
                               <tr>
                                  <td>
                                      <div style="height: 181px; overflow: auto;">
                                      <table id="tblist">
                                      <asp:Literal ID="ltOptions" runat="server"></asp:Literal>
                      
                                      </table>
                                      </div>
                                   </td>
                                                    
                               </tr>
                        </tbody>
                    </table>
				</div>
			</div>
			<%--<div class="col-md-6 col-sm-6 col-xs-12">
				
				
			</div>--%>
		</div>
		<div class="sale-detailed-second-panel">
			<div class="sale-detailed-btn-section">
				<asp:Button ID="btnGetRecords" class="report_table_btns btn" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click"/>
			</div>
			<div class="sale-detailed-print-btn-section">
					<input type="button" id="btnPrint" class="report_table_btns btn" value="Print" onclick="PrintGridData()" /><asp:Button ID="btnexport"  class="report_table_btns btn btnexp" runat="server"  Text="Export To Excel" onclick="btnexport_Click"/><asp:Button ID="btnpdf" class="report_table_btns btn" runat="server"  Text="Export To Pdf" onclick="btnpdf_Click"/>
				</div>
		</div>
<%--<table class="cashmemo_header">
     <tr><td colspan="100%"> 
           </td>
     </tr>
</table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<asp:HiddenField ID= "hdnOption" runat="server" />
<div class="report_table_main">
   <%--<table class="report_table">
     <tr>
         <td>Choose Branch:</td>
         <td></td>
         <td>POS:</td> 
         <td></td>
         <td class="bill-type-td-cash-memo-rpt"></td>
         <!--<td class="bill-data-td-cash-memo-rpt">
         </td>-->
         <td></td>
     </tr>
    
   
     <tr>
         <td>Date From:</td>
         <td>
         </td>
         <td>Date To:</td>
         <td>
         </td>
		 
         <td colspan="3"></td>
         <!--<td></td>
         <td></td>
		 <td></td>-->
         </tr>

     <tr class="label-cash-memo-rpt"> 
       <td>Bill Mode:</td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
     </tr>
    <tr class="label-cash-memo-rpt"> 
       <td>Option:</td>
       <td></td>
       <td></td>
       
       
           <td></td>
           <td></td>
     </tr>
       <tr>
			<td></td>

          
                                    <td colspan="2"  >
                                              
                          </td>    
          <!-- <td></td>-->


       </tr>
      <tr class="tr-generate-cash-memo-rpt">
		  <td colspan="8"></td>
      </tr>
 </table>--%>
</div>
    

<div class="table_detail" id ="dvle">
<asp:GridView ID="gvUserInfo" AutoGenerateColumns="false" runat="server" CssClass="table table-striped table-bordered table-hover">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
   <Columns>
      
         <asp:BoundField DataField="BillNowPrefix" HeaderText="BillNowPrefix" Visible="false"/>
         <asp:BoundField DataField="Date" HeaderText="Date" Visible="false"/>
       <asp:BoundField DataField="CustomerName" HeaderText="CustomerName" Visible="false"/>
       <asp:BoundField DataField="GSTNo" HeaderText="GSTNo" Visible="false"/>
         <asp:BoundField DataField="0.00%" HeaderText="0.00%" Visible="false"/>
         <asp:BoundField DataField="5.00%" HeaderText="5.00%" Visible="false"/>
         <asp:BoundField DataField="12.00%" HeaderText="12.00%" Visible="false"/>
         <asp:BoundField DataField="18.00%" HeaderText="18.00%" Visible="false"/>
       <asp:BoundField DataField="28.00%" HeaderText="28.00%" Visible="false"/>
       <asp:BoundField DataField="40.00%" HeaderText="40.00%" Visible="false"/>
       
       <asp:BoundField DataField="RAmt" HeaderText="RAmt" Visible="false"/>
         <asp:BoundField DataField="DelCharg" HeaderText="DelCharg" Visible="false"/>
       <asp:BoundField DataField="NetAmt" HeaderText="NetAmt" Visible="false"/>
         <asp:BoundField DataField="CashAdv" HeaderText="CashAdv" Visible="false"/>
         <asp:BoundField DataField="CCAdv" HeaderText="CCAdv" Visible="false"/>
         <asp:BoundField DataField="CASH" HeaderText="CASH" Visible="false"/>
         <asp:BoundField DataField="CREDIT" HeaderText="CREDIT" Visible="false"/>
          <asp:BoundField DataField="ONLINEPAY" HeaderText="ONLINEPAY" Visible="false"/>
         <asp:BoundField DataField="SaleReturn" HeaderText="SaleReturn" Visible="false"/>
         <asp:BoundField DataField="VatAmt" HeaderText="VatAmt" Visible="false"/>
         <asp:BoundField DataField="CRCARD" HeaderText="CRCARD" Visible="false"/>
         
     
   </Columns>
</asp:GridView>


    <asp:GridView ID="gvUserInfo2" AutoGenerateColumns="false" runat="server" CssClass="table table-striped table-bordered table-hover">
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    <EditRowStyle BackColor="#999999" />
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White"/>
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <SortedAscendingCellStyle BackColor="#E9E7E2" />
    <SortedAscendingHeaderStyle BackColor="#506C8C" />
    <SortedDescendingCellStyle BackColor="#FFFDF8" />
    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
  

         <Columns>
      
         <asp:BoundField DataField="Date" HeaderText="Date" Visible="false"/>
         <asp:BoundField DataField="BillFrom" HeaderText="BillFrom" Visible="false"/>
         <asp:BoundField DataField="BillTo" HeaderText="BillTo" Visible="false"/>
         <asp:BoundField DataField="VATABLE@ 0.00%" HeaderText="VATABLE@ 0.00%" Visible="false"/>
         <asp:BoundField DataField="VAT@0.00%" HeaderText="VAT@0.00%" Visible="false"/>
         <asp:BoundField DataField="VATABLE@ 5.00%" HeaderText="VATABLE@ 5.00%" Visible="false"/>
       <asp:BoundField DataField="VAT@5.00%" HeaderText="VAT@5.00%" Visible="false"/>
       <asp:BoundField DataField="VATABLE@ 12.00%" HeaderText="VATABLE@ 12.00%" Visible="false"/>
         <asp:BoundField DataField="VAT@12.00%" HeaderText="VAT@12.00%" Visible="false"/>
        <asp:BoundField DataField="VATABLE@ 18.00%" HeaderText="VATABLE@ 18.00%" Visible="false"/>
       <asp:BoundField DataField="VAT@18.00%" HeaderText="VAT@18.00%" Visible="false"/>
       <asp:BoundField DataField="VATABLE@ 28.00%" HeaderText="VATABLE@ 28.00%" Visible="false"/>
         <asp:BoundField DataField="VAT@28.00%" HeaderText="VAT@28.00%" Visible="false"/>
       <asp:BoundField DataField="VATABLE@ 40.00%" HeaderText="VATABLE@ 40.00%" Visible="false"/>
         <asp:BoundField DataField="VAT@40.00%" HeaderText="VAT@40.00%" Visible="false"/>
             
             
             <asp:BoundField DataField="RAmt" HeaderText="RAmt" Visible="false"/>
         <asp:BoundField DataField="DelCharg" HeaderText="DelCharg" Visible="false"/>
       <asp:BoundField DataField="NetAmt" HeaderText="NetAmt" Visible="false"/>
         <asp:BoundField DataField="CashAdv" HeaderText="CashAdv" Visible="false"/>
         <asp:BoundField DataField="CCAdv" HeaderText="CCAdv" Visible="false"/>
         <asp:BoundField DataField="CASH" HeaderText="CASH" Visible="false"/>
         <asp:BoundField DataField="CREDIT" HeaderText="CREDIT" Visible="false"/>
          <asp:BoundField DataField="OPay" HeaderText="OPay" Visible="false"/>
        
         <asp:BoundField DataField="CRCARD" HeaderText="CRCARD" Visible="false"/>
         
     
   </Columns>
</asp:GridView>

    <table class="advance_cash">
        <tr>
            <td>ADVANCE CASH:</td>
            <td><asp:Label ID = "lblCash" runat="server"> </asp:Label></td>
        </tr>
        <tr>
            <td>ADVANCE CREDIT CARD:</td>
            <td><asp:Label ID = "lblCredit" runat="server"> </asp:Label></td>
        </tr>
    </table>



</div>


</div>
</asp:Content>
