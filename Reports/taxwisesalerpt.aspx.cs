﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class taxwisesalerpt : System.Web.UI.Page
{
    int BranchId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindBranches();
        }


        CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }
        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }
        string Type = "";
        if (txtDateFrom.Text != "")
        {
            if (rdbHsnWise.Checked == true)
            {
                Type = "HSNWise";
                RptTaxWiseSale objBreakageExpiry = new RptTaxWiseSale(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, txtBillFrom.Text, txtBillTo.Text, Type);
                XtraReport1 objsubreport = new XtraReport1(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text));
                ReportViewer1.Report = objBreakageExpiry;
            }
            else if (rdbTaxWise.Checked == true)
            {
                Type = "TaxWise";
                RptTaxWiseSale2 objBreakageExpiry = new RptTaxWiseSale2(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, txtBillFrom.Text, txtBillTo.Text, Type);
                XtraReport1 objsubreport = new XtraReport1(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text));
                ReportViewer1.Report = objBreakageExpiry;
            }

            else if (rdbConsolidate.Checked == true)
            {
                Type = "Consolidate";
                RptTaxWiseSale3 objBreakageExpiry = new RptTaxWiseSale3(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, txtBillFrom.Text, txtBillTo.Text, Type);
                //XtraReport1 objsubreport = new XtraReport1(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text));
                ReportViewer1.Report = objBreakageExpiry;
            }


        }
        else 
        {

            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            RptTaxWiseSale objBreakageExpiry = new RptTaxWiseSale(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, txtBillFrom.Text, txtBillTo.Text, Type);
            ReportViewer1.Report = objBreakageExpiry;
           
        }

    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPTAXWISESALE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }


    public void BindCstRate()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
       // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("select mc.Customer_Name,sum(bm.net_amount) as netamount from bill_master as bm inner join mst_customer as mc on mc.cst_id=bm.cst_id  where CAST(Bill_Date as date)between @FromDate and @ToDate and BranchId = @BranchId   group by mc.Customer_Name",con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.AddWithValue("@FromDate", txtDateFrom.Text.Trim());
        cmd.Parameters.AddWithValue("@ToDate",txtDateTo.Text.Trim());
        cmd.Parameters.AddWithValue("@BranchId",BranchId);
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adb.Fill(dt);
        gvcst_sale.DataSource = dt;
        gvcst_sale.DataBind();
        con.Close();

    }
    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

  

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }
}