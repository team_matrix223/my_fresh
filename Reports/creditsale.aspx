﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="creditsale.aspx.cs" Inherits="creditsale" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/craditsale.css" rel="stylesheet" />
<script type ="text/javascript">
    $(document).ready(
function () {




}
);
</script>
    <div class="department_wise">

<table  class="department_header">
 <tr><td colspan="100%"> CREDIT SALE REPORT </td></tr>
</table>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
<div class="sale_report">
 <table class="sale_report">
       <tr><td>Choose Branch</td>
           <td><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch">
               </asp:DropDownList>
           </td>
           <td style="color:white"> POS</td>
           <td><asp:DropDownList ID="dd_customername"   class="form-control" runat="server"></asp:DropDownList></td>
       </tr>
       <tr><td>Date From:</td>
           <td><asp:TextBox ID="txtDateFrom" runat="server" 
             Width="100px"></asp:TextBox>
            <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
          </td>
          <td>Date To:</td>
          <td><asp:TextBox ID="txtDateTo" runat="server"></asp:TextBox>
              <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender></td><td>
              <asp:Button ID="btnGetRecords" runat="server"  Text="Generate Report" onclick="btnGetRecords_Click"/>
          </td>
       </tr>
   </table>
</div> 
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
  
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>

