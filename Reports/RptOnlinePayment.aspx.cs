﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;

public partial class Reports_RptOnlinePayment : System.Web.UI.Page
{
    mst_customer_rate msr = new mst_customer_rate();
    protected void Page_Load(object sender, EventArgs e)
    {

        int BranchId = 0;
       int OtherPaymentid = 0;
        // manual_onlineotherpayment();
        if (!IsPostBack)
        {
        
            txtDateFrom.Text = DateTime.Now.ToShortDateString();
            txtDateTo.Text = DateTime.Now.ToShortDateString();
            BindOnlinePayment();
            BindBranches();
            ddcustomertype(); 
            BindUsers();
          


        }
        
        //CheckRole();
        if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
        {
            ddlBranch.Enabled = true;
        }
        else
        {
            ddlBranch.Enabled = false;
        }

        if (ddlBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
        }


        if (ddlOnlinePayment.SelectedValue == "0")
        {

          //  OtherPaymentid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.OtherPayment_ID].Value);
          //  Response.Write("<script LANGUAGE='JavaScript' >alert('Choose online Payment')</script>");
        }
        else
        {
            OtherPaymentid = Convert.ToInt32(ddlOnlinePayment.SelectedValue);
        }
        if (txtDateFrom.Text != "")
        {
            RptOnlinePayment objBreakageExpiry = new RptOnlinePayment(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, OtherPaymentid, 0, Convert.ToInt32(dd_customername.SelectedValue), Convert.ToInt32(dd_users.SelectedValue));
            ReportViewer1.Report = objBreakageExpiry;
        }


    }
    public void manual_onlineotherpayment()
    {
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
       // SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_manual_onlineotherpayment", con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@strt_date", txtDateFrom.Text.Trim());
        cmd.Parameters.AddWithValue("@end_date",txtDateTo.Text.Trim());
        cmd.ExecuteNonQuery();
        con.Close();

    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();
    }

    void BindOnlinePayment()
    {

        ddlOnlinePayment.DataSource = new OtherPaymentModeBLL().GetAll();
        ddlOnlinePayment.DataValueField = "OtherPayment_ID";
        ddlOnlinePayment.DataTextField = "OtherPayment_Name";
        ddlOnlinePayment.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose OnlinePayment--";
        li1.Value = "-1";
        ddlOnlinePayment.Items.Insert(0, li1);

    }
    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }



    public void BindUsers()
    {
        string SqlQuery = "select  Userno,user_id from  userinfo where Discontinued=0 order by user_id";
        Connection con = new Connection();
        DataTable dt = new DataTable();
        SqlDataAdapter dad = new SqlDataAdapter(SqlQuery, con.sqlDataString);
        dad.Fill(dt);
        dd_users.DataSource = dt;
        dd_users.DataTextField = "user_id";
        dd_users.DataValueField = "Userno";
        dd_users.DataBind();
        System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("-Select-", "0");
        dd_users.Items.Insert(0, listItem1);
    }
    
 


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.RPTONLINEPAYMENT));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }

    protected void btnGetRecords_Click(object sender, EventArgs e)
    {

     

    }
    protected void ReportToolbar1_Unload(object sender, EventArgs e)
    {

    }


    public int OtherPayment_ID { get; set; }
}