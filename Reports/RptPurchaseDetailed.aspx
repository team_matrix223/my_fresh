﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="RptPurchaseDetailed.aspx.cs" Inherits="RptPurchaseDetailed" %>

<%@ Register Assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" runat="Server">
	<link href="css/rptSaleDetailed.css" rel="stylesheet" />
	<link href="css/RptPurchaseDetailedNew.css" rel="stylesheet" />
	<script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
	<script type="text/javascript">

		var OptionType = "";

		function InsertUpdate() {

			OptionType = $("#hdn_chkval").val();

			if (OptionType == "") {
				OptionType = "Detailed";
			}


			var arr_optionlist = new Array();
			var arr_txtwidth = new Array();
			var OptionsId = "";
			var txtwidth = "";


			$('#tblist tr').each(function () {

				var currentRow = $(this).closest("tr");
				if (currentRow.find(".cls_chk").is(":checked")) {

					OptionsId = currentRow.find(".cls_chk").val();
					txtwidth = currentRow.find(".cls_width").val();
					arr_optionlist.push(OptionsId);
					arr_txtwidth.push(txtwidth);
				}

			});
			//var DTO = { 'OptionList': '' + arr_optionlist + '', 'Width': '' + arr_txtwidth + '' , 'OptionType': '' + OptionType + '' };


			$.ajax({
				type: "POST",

				contentType: "application/json; charset=utf-8",
				url: "RptPurchaseDetailed.aspx/Insert",
				//data: JSON.stringify(DTO),
				data: JSON.stringify({ OptionList: arr_optionlist, Width: arr_txtwidth, OptionType: OptionType }),
				dataType: "json",
				success: function (msg) {

					var obj = jQuery.parseJSON(msg.d);

				},
				error: function (xhr, ajaxOptions, thrownError) {

					var obj = jQuery.parseJSON(xhr.responseText);
					alert(obj.Message);
				},
				complete: function () {

				}
			});



		}



		$(document).ready(
			function () {


				$("#btngridoptions").click(
					function () {
						$("#sel_table").show();
						$("#<%=hdnOption.ClientID %>").val("");
				}
			);

			$("#btnSaveOptions").click(
				function () {
					InsertUpdate();
				}
			);

			$("#btnPrint").click(
				function () {
					PrintGridData();
				}
			);

			function PrintGridData() {

				//var prtGrid = document.getElementById('<%=gvUserInfo.ClientID %>');
				var prtGrid = document.getElementById('dvle');
				prtGrid.border = 0;
				var prtwin = window.open('', 'PrintGridViewData', 'left=100,top=100,width=1000,height=1000,tollbar=0,scrollbars=1,status=0,resizable=1');
				prtwin.document.write(prtGrid.outerHTML);
				prtwin.document.close();
				prtwin.focus();
				prtwin.print();
				prtwin.close();
			}

			$("#<%=rdbAll.ClientID %>").change(
				function () {

					if ($("#<%=rdbAll.ClientID %>").prop('checked') == true) {
						$("#<%=rdbCash.ClientID %>").prop('checked', false);
						$("#<%=rdbCredit.ClientID %>").prop('checked', false);


					}


				}
	);
			$("#<%=rdbCash.ClientID %>").change(
				function () {

					if ($("#<%=rdbCash.ClientID %>").prop('checked') == true) {
						$("#<%=rdbAll.ClientID %>").prop('checked', false);
						$("#<%=rdbCredit.ClientID %>").prop('checked', false);


					}


				}
	);
			$("#<%=rdbCredit.ClientID %>").change(
				function () {
					if ($("#<%=rdbCredit.ClientID %>").prop('checked') == true) {
						$("#<%=rdbAll.ClientID %>").prop('checked', false);
						$("#<%=rdbCash.ClientID %>").prop('checked', false);


					}


				}
	);


			$("#<%=rdbDetailed.ClientID %>").change(
				function () {
					if ($("#<%=rdbDetailed.ClientID %>").prop('checked') == true) {
						OptionType = "Detailed";

						$("#<%=rdbDated.ClientID %>").prop('checked', false);
			}


				}
	);
			$("#<%=rdbDated.ClientID %>").change(
				function () {
					if ($("#<%=rdbDated.ClientID %>").prop('checked') == true) {
						OptionType = "Dated";

						$("#<%=rdbDetailed.ClientID %>").prop('checked', false);
			}


				}
	);





		}
	);
	</script>

	<div class="cashmemo_wise">

		<asp:HiddenField ID="hdn_chkval" ClientIDMode="Static" runat="server" />

		<div style="text-align: center;">
			<h2 class="purchase-sale-head">PURCHASE SALE REPORT</h2>
		</div>
		<div class="purchase-sale-top-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 purchase-sale-cst-col branch-col">
				<div class="purchase-sale-branch-section">
					<span>Branch:</span><asp:DropDownList class="form-control" ID="ddlBranch" runat="server" placeholder="Choose Branch"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 purchase-sale-cst-col purchase-sale-cst-col-pos pos-col">
				<div class="purchase-sale-pos-section">
					<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 purchase-sale-cst-col purchase-sale-cst-col-date date-col">
				<div class="purchase-sale-date-section">
					<div class="purchase-sale-date-part">
						<span>Date:</span><asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
						<asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true" runat="server"></asp:CalendarExtender>
					</div>
					<div class="purchase-sale-date-part">
						<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
						<asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true" runat="server"></asp:CalendarExtender>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12 purchase-sale-cst-col purchase-sale-cst-col-billmode bill-col">
				<div class="purchase-sale-billmode-section">
					<span>Bill Mode:</span><asp:RadioButton ID="rdbAll" name="all" runat="server" Text="All" Checked="True" />
					<asp:RadioButton ID="rdbCash" name="all" runat="server" Text="Cash" />
					<asp:RadioButton ID="rdbCredit" name="all" runat="server" Text="Credit" />
				</div>
			</div>
		</div>
		<div class="purchase-sale-second-panel">
			<div class="col-md-3 col-sm-3 col-xs-12 purchase-sale-cst-col">
				<div class="purchase-sale-option-section">
					<span>Option:</span><asp:RadioButton ID="rdbDetailed" runat="server" Text="Detailed" AutoPostBack="true" Visible="false" Checked="True" OnCheckedChanged="rdbDetailed_CheckedChanged" />
					<asp:RadioButton ID="rdbDated" runat="server" Text="Dated" AutoPostBack="true" Visible="false" OnCheckedChanged="rdbDated_CheckedChanged" />
				</div>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12 purchase-sale-cst-col">
				<div class="purchase-sale-grid-section">
					<div id="btngridoptions" class="report_table_btns">Grid Options</div>
					<div id="btnSaveOptions" class="report_table_btns">Save</div>
				</div>
				<div id="sel_table" style="display: none">
					<table class="table">
						<tbody>

							<tr>
								<td>
									<div style=" overflow: auto;">
										<table id="tblist">
											<asp:Literal ID="ltOptions" runat="server"></asp:Literal>

										</table>
									</div>
								</td>

							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="purchase-sale-third-panel">
			<div class="purchase-sale-btn-section">
				<asp:Button ID="btnGetRecords" class="report_table_btns btn" runat="server" Text="Generate Report" OnClick="btnGetRecords_Click" />
			</div>
			<div class="purchase-sale-print-btn-section">
				<input type="button" id="btnPrint" class="report_table_btns btn" value="Print" onclick="PrintGridData()" />
				<asp:Button ID="btnexport" class="report_table_btns btn" runat="server" Text="Export To Excel" OnClick="btnexport_Click" />
				<asp:Button ID="btnpdf" class="report_table_btns btn" runat="server" Text="Export To Pdf" OnClick="btnpdf_Click" />
			</div>
		</div>
		<%--<table class="cashmemo_header">
     <tr><td colspan="100%"> 
          PURCHASE SALE REPORT </td>
     </tr>
</table>--%>
		<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
		<asp:HiddenField ID="hdnOption" runat="server" />
		<div class="report_table_main">
		<%--	<table class="report_table">
				<tr>
					<td>Choose Branch</td>
					<td></td>
					<td>POS</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>


				<tr>
					<td>Date From:</td>
					<td></td>
					<td>Date To:</td>
					<td></td>
					<td></td>
					<td>						</td>
					<td>						</td>
					<td></td>
				</tr>

				<tr>
					<td>Bill Mode</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Option</td>
					<td>
						</td>
					<td>
						</td>


					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>


					<td colspan="2"></td>
					<td></td>


				</tr>

			</table>--%>
		</div>


		<div class="table_detail" id="dvle">
			<asp:GridView ID="gvUserInfo" AutoGenerateColumns="false" runat="server" CssClass="table table-striped table-bordered table-hover">
				<AlternatingRowStyle BackColor="White" ForeColor="#284775" />
				<EditRowStyle BackColor="#999999" />
				<FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
				<HeaderStyle BackColor="#5D7B9D" Font-Bold="true" ForeColor="White" />
				<PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
				<RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
				<SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
				<SortedAscendingCellStyle BackColor="#E9E7E2" />
				<SortedAscendingHeaderStyle BackColor="#506C8C" />
				<SortedDescendingCellStyle BackColor="#FFFDF8" />
				<SortedDescendingHeaderStyle BackColor="#6F8DAE" />
				<Columns>

					<asp:BoundField DataField="BillNo" HeaderText="BillNo" Visible="false" />
					<asp:BoundField DataField="Date" HeaderText="Date" Visible="false" />
					<asp:BoundField DataField="SupplierName" HeaderText="SupplierName" Visible="false" />
					<asp:BoundField DataField="GSTNo" HeaderText="GSTNo" Visible="false" />
					<asp:BoundField DataField="0.00%" HeaderText="0.00%" Visible="false" />
					<asp:BoundField DataField="5.00%" HeaderText="5.00%" Visible="false" />
					<asp:BoundField DataField="12.00%" HeaderText="12.00%" Visible="false" />
					<asp:BoundField DataField="18.00%" HeaderText="18.00%" Visible="false" />
					<asp:BoundField DataField="28.00%" HeaderText="28.00%" Visible="false" />
					<asp:BoundField DataField="40.00%" HeaderText="40.00%" Visible="false" />

					<asp:BoundField DataField="DisplayAmount" HeaderText="DisplayAmount" Visible="false" />
					<asp:BoundField DataField="DIS/SCH" HeaderText="DIS/SCH" Visible="false" />
					<asp:BoundField DataField="RAmt" HeaderText="RAmt" Visible="false" />
					<asp:BoundField DataField="NetAmt" HeaderText="NetAmt" Visible="false" />



				</Columns>
			</asp:GridView>








		</div>


	</div>
</asp:Content>
