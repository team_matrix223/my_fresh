﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="OrderBookingDelivryDate.aspx.cs" Inherits="OrderBookingDelivryDate" %>

<%@ Register assembly="DevExpress.XtraReports.v11.2.Web, Version=11.2.10.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" namespace="DevExpress.XtraReports.Web" tagprefix="dx" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>
<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
    <link href="css/OrderBookingDelivryDate.css" rel="stylesheet" />
	<link href="css/OrderBookingDelivryDateNew.css" rel="stylesheet" /> 
<script type ="text/javascript">
    $(document).ready(
function () {




}
);
</script>
    <div class="department_wise">
<div style="text-align: center;"><h2 class="oder-booking-ddw-head">ORDER BOOKING REPORT(DeliveryDate Wise) </h2></div>
<div class="order-booking-ddw-top-panel">
	<div class="col-md-3 col-sm-3 col-xs-12 oder-booking-ddw-cst-col branch-col">
		<div class="order-booking-ddw-branch-section">
			<span>Branch:</span><asp:DropDownList class="form-control" id="ddlBranch" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12 oder-booking-ddw-cst-col date-col">
		<div class="order-booking-ddw-date-section">
			<div class="order-booking-ddw-date-part">
				<span>Date:</span> <asp:TextBox ID="txtDateFrom" runat="server" class="form-control"></asp:TextBox>
                <asp:CalendarExtender ID="cc1" TargetControlID="txtDateFrom" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
			<div class="order-booking-ddw-date-part">
				<span>To:</span><asp:TextBox ID="txtDateTo" runat="server" class="form-control"></asp:TextBox>
                    <asp:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDateTo" EnabledOnClient="true"  runat="server"></asp:CalendarExtender>
			</div>
		</div>
	</div>
	<div class="col-md-3 col-sm-3 col-xs-12 oder-booking-ddw-cst-col pos-col">
		<div class="order-booking-ddw-pos-section">
			<span>POS:</span><asp:DropDownList ID="dd_customername" class="form-control" runat="server"></asp:DropDownList>
		</div>
	</div>
	<div class="col-md-2 col-sm-2 col-xs-12 oder-booking-ddw-cst-col btn-col">
		<div class="order-booking-ddw-btn-section">
			<asp:Button ID="btnGetRecords" runat="server" CssClass="btn btn-success" Text="Generate Report" onclick="btnGetRecords_Click"/>
		</div>
	</div>
</div>
           <%-- <table class="department_header">
                <tr><td colspan="100%"></td></tr>
            </table>--%>
<asp:ScriptManager ID="scrip1" runat="server"></asp:ScriptManager>
   <div class="sale_report">
        <%--<table class="sale_report">
            <tr>
                <td>Choose Branch</td>
                <td></td>
                 <td>POS</td>
                <td></td>
                <td></td>
            </tr>

            <tr>
                <td>Date From:</td>
                <td>
                </td>
                <td>Date To:</td>
                <td>
                </td>
                <td></td>
            </tr>
        </table>--%>
    </div>
    <div class="order-booking-ddw-rpt-tool">
        <dx:ReportToolbar ID="ReportToolbar1" runat="server" ShowDefaultButtons="False" 
            onunload="ReportToolbar1_Unload" width="800px" 
        ReportViewerID="ReportViewer1" >
            <Items>
                <dx:ReportToolbarButton ItemKind="Search" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="PrintReport" />
                <dx:ReportToolbarButton ItemKind="PrintPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton Enabled="False" ItemKind="FirstPage" />
                <dx:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" />
                <dx:ReportToolbarLabel ItemKind="PageLabel" />
                <dx:ReportToolbarComboBox ItemKind="PageNumber" Width="65px">
                </dx:ReportToolbarComboBox>
                <dx:ReportToolbarLabel ItemKind="OfLabel" />
                <dx:ReportToolbarTextBox IsReadOnly="True" ItemKind="PageCount" />
                <dx:ReportToolbarButton ItemKind="NextPage" />
                <dx:ReportToolbarButton ItemKind="LastPage" />
                <dx:ReportToolbarSeparator />
                <dx:ReportToolbarButton ItemKind="SaveToDisk" />
                <dx:ReportToolbarButton ItemKind="SaveToWindow" />
                <dx:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                    <Elements>
                        <dx:ListElement Value="pdf" />
                        <dx:ListElement Value="xls" />
                        <dx:ListElement Value="xlsx" />
                        <dx:ListElement Value="rtf" />
                        <dx:ListElement Value="mht" />
                        <dx:ListElement Value="html" />
                        <dx:ListElement Value="txt" />
                        <dx:ListElement Value="csv" />
                        <dx:ListElement Value="png" />
                    </Elements>
                </dx:ReportToolbarComboBox>
            </Items>
            <Styles>
                <LabelStyle>
                <Margins MarginLeft="3px" MarginRight="3px" />
                </LabelStyle>
            </Styles>
        </dx:ReportToolbar>
    
   </div>
    <dx:ReportViewer ID="ReportViewer1" runat="server" Width="1000px">
    </dx:ReportViewer>

</div>
</asp:Content>


