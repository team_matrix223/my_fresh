﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backoffice_Reports_Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (HttpContext.Current.Request.Cookies[Constants.DesignationId].Value == "1")
        {
            dvfilter.Visible = true;
            didashtiles.Visible = true;

        }
        else
        {
            didashtiles.Visible = false;
        }
        if (IsPostBack==false)
        {

     
        //GetTodayData();
       // BindBranches();
        ddcustomertype();
            //CheckRole();
        }
    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPORTING));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        dd_customername.DataSource = dt;
        dd_customername.DataTextField = "title";
        dd_customername.DataValueField = "posid";
        dd_customername.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose POS--";
        li1.Value = "0";
        dd_customername.Items.Insert(0, li1);
    }
    //void BindBranches()
    //{

    //    ddlBranch.DataSource = new BranchBLL().GetAll();
    //    ddlBranch.DataValueField = "BranchId";
    //    ddlBranch.DataTextField = "BranchName";
    //    ddlBranch.DataBind();
    //    ListItem li1 = new ListItem();
    //    li1.Text = "--Choose Branch--";
    //    li1.Value = "0";
    //    ddlBranch.Items.Insert(0, li1);

    //}

    public void GetTodayData() {

        Connection con = new Connection();
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {

            conn.Open();
            Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
            SqlCommand cmd = new SqlCommand("strp_dashtiles", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchId", Branch);
            // cmd.Parameters.AddWithValue("@BranchId",ddlBranch.SelectedValue);
            cmd.Parameters.AddWithValue("@pos_id", dd_customername.SelectedValue);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                LblTodayCash.Text = rd["TodayCashSale"].ToString();
                LblTodayTotalSale.Text = rd["TodayTotalSale"].ToString();
                LblTodayOnlineSaleOrder.Text = rd["TodayOnlineOrder"].ToString();
                LblTodayOnlineSale.Text = rd["TodayOnlineSale"].ToString();
                LblTodayPendingSettelment.Text = rd["TodayPendingSettelment"].ToString();
                LblTodayOpenTables.Text = rd["TodayOpenTables"].ToString();
                LblTodayOpenKot.Text = rd["TodayOpenTables"].ToString();
                LblTodayCrCardSale.Text = rd["TodayCrCardSale"].ToString();
                LblTodayCredit.Text = rd["TodayCreditSale"].ToString();
                LblTodayGST.Text = rd["GSTSale"].ToString();
                lblswiggysale.Text = rd["Swiggy"].ToString();
                lblzomatosale.Text = rd["zomato"].ToString();

            }

         

        }
    }
    [WebMethod]
    public static string GetBarGraphData(string req,int branchid,int posid)
    {

        Connection con = new Connection();
        using (SqlConnection conn = new SqlConnection(con.sqlDataString))
        {

            JavaScriptSerializer ser = new JavaScriptSerializer();
            ser.MaxJsonLength = int.MaxValue;
            string Today = "";
            string Yesterday = "";
            conn.Open();
            SqlCommand cmd = new SqlCommand("strp_dash_bargraph", conn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@BranchId", branchid);
            cmd.Parameters.AddWithValue("@pos_id", posid);
            cmd.Parameters.AddWithValue("@req", req);
            SqlDataReader rd = cmd.ExecuteReader();
            if (rd.Read())
            {
                Today = rd["Today"].ToString();
                Yesterday = rd["Yesterday"].ToString();
         

            }
            var JsonData = new
            {
                Today,
                Yesterday

            };

            return ser.Serialize(JsonData);

        }
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        //if (HttpContext.Current.Request.Cookies[Constants.DesignationId].Value == "1")
        //{

            GetTodayData();
       // }
    }
}