﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Reports_UserWiseSaleReturnRPT : System.Web.UI.Page
{
  protected void Page_Load(object sender, EventArgs e)
  {
    int BranchId = 0;
    int UserId = 0;
    if (!IsPostBack)
    {

      txtDateFrom.Text = DateTime.Now.ToShortDateString();
      txtDateTo.Text = DateTime.Now.ToShortDateString();
      BindBranches();
      BindUsers();
      ddcustomertype();
    }

    CheckRole();
    if (Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 1 || Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value) == 13)
    {
      ddlBranch.Enabled = true;
    }
    else
    {
      ddlBranch.Enabled = false;
    }

    if (ddlBranch.SelectedValue == "0")
    {
      BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
    }
    else
    {
      BranchId = Convert.ToInt32(ddlBranch.SelectedValue);
    }


    if (ddlUser.SelectedValue == "0")
    {
      UserId = 0;
    }
    else
    {
      UserId = Convert.ToInt32(ddlUser.SelectedValue);
    }

    if (txtDateFrom.Text != "")
    {
      UserWiseSaleReport objUserWiseSaleReport = new UserWiseSaleReport(Convert.ToDateTime(txtDateFrom.Text), Convert.ToDateTime(txtDateTo.Text), BranchId, UserId,Convert.ToInt32(dd_customername.SelectedValue));
      ReportViewer1.Report = objUserWiseSaleReport;
    }
  }

  void BindBranches()
  {
    ddlBranch.DataSource = new BranchBLL().GetAll();
    ddlBranch.DataValueField = "BranchId";
    ddlBranch.DataTextField = "BranchName";
    ddlBranch.DataBind();
    //ListItem li1 = new ListItem();
    //li1.Text = "--Choose Branch--";
    //li1.Value = "0";
    //ddlBranch.Items.Insert(0, li1);
  }
  public void ddcustomertype()
  {
    pos objpos = new pos();
    objpos.req = "bindgrid";
    DataTable dt = objpos.bindgride();
    dd_customername.DataSource = dt;
    dd_customername.DataTextField = "title";
    dd_customername.DataValueField = "posid";
    dd_customername.DataBind();
    //System.Web.UI.WebControls.ListItem listItem1 = new System.Web.UI.WebControls.ListItem("Choose", "0");
    //dd_customername.Items.Insert(0, listItem1);

  }
  public void BindUsers()
  {
    ddlUser.DataSource = new UserBLL().GetAll(Convert.ToInt32(ddlBranch.SelectedValue));
    ddlUser.DataValueField = "UserNo";
    ddlUser.DataTextField = "User_Id";
    ddlUser.DataBind();
    //ListItem litem1 = new ListItem();
    //litem1.Text = "--Choose User--";
    //litem1.Value = "0";
    //ddlUser.Items.Insert(0, litem1);
  }

  public void CheckRole()
  {
    string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt16(Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.REPINTERNALDATE));

    string[] arrRoles = sesRoles.Split(',');

    var roles = from m in arrRoles
                where m == Convert.ToInt16(Enums.Roles.REPORTING).ToString()
                select m;

    int len = roles.Count();
    if (len == 0)
    {
      Response.Redirect("../index.aspx");

    }
  }

  protected void btnGetRecords_Click(object sender, EventArgs e)
  {

  }

  protected void ReportToolbar1_Unload(object sender, EventArgs e)
  {

  }
  protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
  {
    BindUsers();
  }
}