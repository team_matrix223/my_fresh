﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/Reports/ReportPage.master" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="backoffice_Reports_Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntMaster" Runat="Server">
      <script src="../dashboard_style/dashboard_js/jquery-1.10.2.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="../dashboard_style/dashboard_css/bootstrap.css" rel="stylesheet" />
    <link href="../dashboard_style/dashboard_css/cssCharts.css" rel="stylesheet" />
    <link href="../dashboard_style/dashboard_css/custom-styles.css" rel="stylesheet" />
    <%--<link href="../dashboard_style/dashboard_css/font-awesome.css" rel="stylesheet" />--%>
    <link href="../dashboard_style/dashboard_css/materialize.min.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../dashboard_style/dashboard_css/morris-0.4.3.min.css" rel="stylesheet" />
	<link href="css/default2new.css" rel="stylesheet" />

 <style>
    .welcome_to {
    width: 100%;
    float: left;
    text-align: center;
}
    .welcome_to h1 {
    margin: 0px;
    font-weight: bold;
    padding: 14% 0;
    color: #f06671;
    font-size: 60px;
    letter-spacing: 5px;
    word-spacing: 25px;
}
    .all_row {
    width: 100%;
    float: left;
}
header {
    padding-bottom: 0px;
}


     .progress {
         overflow: visible;
         margin-bottom: 50px;
         height: 30px;
     }
  .progress-bar {
         background-color: #f06671;
         position: relative;
         border-radius: 4px;
     }
    .progress_demo span {
      background-color: #f06671;
      position: absolute;
      bottom: -27px;
      font-size: 15px;
      line-height: 10px;
      padding: 6px 5px 6px 5px;
      right: -1.4em;
      border-radius: 2px;
    }
    .progress_demo span:after {
      bottom: 100%;
      left: 50%;
      border: solid transparent;
      content: " ";
      height: 0;
      width: 0;
      position: absolute;
      pointer-events: none;
      border-color: rgba(255, 255, 255, 0);
      border-bottom-color: #f06671;
      border-width: 5px;
      margin-left: -5px;
    }
 .progress_demo h1 {
    padding: 0 0 15px;
    font-size: 40px;
    font-weight: 600;
    border-bottom: 2px solid #9e9e9e;
    margin: 0px 0 25px;
}
    .progress_demo {
    width: 98%;
    margin: 25px auto;
    background-color: #ffffff75;
    padding: 30px;
    box-shadow: 0px 0px 14px #46c9ac;
}
    .progress_demo label {
    font-size: 15px;
    margin: 0 0 0;
}


    /*media*/


@media screen and (max-width:1300px) and (min-width:1150px)
{

}
@media screen and (max-width:1149px) and (min-width:1000px)
{

}
@media screen and (max-width:999px) and (min-width:768px)
{
    .welcome_to h1{font-size: 40px;}
}
@media screen and (max-width:767px) and (min-width:620px)
{
    .welcome_to h1{font-size: 40px;}
}
@media screen and (max-width:619px) and (min-width:500px)
{
    .welcome_to h1{font-size: 40px;}
}
@media screen and (max-width:499px) and (min-width:400px)
{
    .welcome_to h1{font-size: 40px;}
}
@media screen and (max-width:399px) and (min-width:320px)
{
   .welcome_to h1{font-size: 25px;}
}
</style>
    <script>


        $(document).ready(function () {
            $(".card").click(function () { 
                var req = $(this).find('.hdn_req').val();
               // var branchid = $("#ddlBranch").val();
                var posid = $("#dd_customername").val();
            $.ajax({
                type: "POST",
				data: '{"req": "' + req + '","posid": "' + posid + '"}',
              //  data: '{"req": "' + req + '","branchid": "' + branchid + '","posid": "' + posid + '"}',
                url: "Default2.aspx/GetBarGraphData",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {
                    var obj = jQuery.parseJSON(msg.d);
                    var TodaySale = parseFloat(obj.Today / 3000).toFixed(2);
                    var YesterdaySale = parseFloat(obj.Yesterday / 3000).toFixed(2);
                    $("#todaysale_spn").text(obj.Today);
                    $("#yeasterdaysale_spn").text(obj.Yesterday);
                    //alert('Today Sale: ' + obj.Today + ',Yesterday Sale: ' + obj.Yesterday);
              
          
                    $("#dv_todayprogressbar").css('width', TodaySale + 'em');
                    $("#dv_yeasterdayprogressbar").css('width', YesterdaySale + 'em');
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                }

            });


            });

        });


    </script>


<%-- <div class="welcome_to">
     <h1>WELCOME TO REPORTING SECTION</h1>
</div>--%>

    <div id="wrapper">
        <nav class="navbar navbar-default top-navbar" role="navigation" style="display: none;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle waves-effect waves-dark" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand waves-effect waves-dark" href="index.html"><i class="large material-icons">insert_chart</i> <strong>TRACK</strong></a>
				
		<div id="sideNav" href=""><i class="material-icons dp48">toc</i></div>
            </div>

            <ul class="nav navbar-top-links navbar-right"> 
				<li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown4"><i class="fa fa-envelope fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a></li>				
				<li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown3"><i class="fa fa-tasks fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a></li>
				<li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown2"><i class="fa fa-bell fa-fw"></i> <i class="material-icons right">arrow_drop_down</i></a></li>
		    	<li><a class="dropdown-button waves-effect waves-dark" href="#!" data-activates="dropdown1"><i class="fa fa-user fa-fw"></i> <b>John Doe</b> <i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
        </nav>
		<!-- Dropdown Structure -->
<ul id="dropdown1" class="dropdown-content">
<li><a href="#"><i class="fa fa-user fa-fw"></i> My Profile</a>
</li>
<li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
</li> 
<li><a href="#"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
</li>
</ul>
<ul id="dropdown2" class="dropdown-content w250">
  <li>

                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-tasks fa-fw"></i> New Task
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 min</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
</ul>
<ul id="dropdown3" class="dropdown-content dropdown-tasks w250">
<li>
		<a href="#">
			<div>
				<p>
					<strong>Task 1</strong>
					<span class="pull-right text-muted">60% Complete</span>
				</p>
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
						<span class="sr-only">60% Complete (success)</span>
					</div>
				</div>
			</div>
		</a>
	</li>
	<li class="divider"></li>
	<li>
		<a href="#">
			<div>
				<p>
					<strong>Task 2</strong>
					<span class="pull-right text-muted">28% Complete</span>
				</p>
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="28" aria-valuemin="0" aria-valuemax="100" style="width: 28%">
						<span class="sr-only">28% Complete</span>
					</div>
				</div>
			</div>
		</a>
	</li>
	<li class="divider"></li>
	<li>
		<a href="#">
			<div>
				<p>
					<strong>Task 3</strong>
					<span class="pull-right text-muted">60% Complete</span>
				</p>
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
						<span class="sr-only">60% Complete (warning)</span>
					</div>
				</div>
			</div>
		</a>
	</li>
	<li class="divider"></li>
	<li>
		<a href="#">
			<div>
				<p>
					<strong>Task 4</strong>
					<span class="pull-right text-muted">85% Complete</span>
				</p>
				<div class="progress progress-striped active">
					<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 85%">
						<span class="sr-only">85% Complete (danger)</span>
					</div>
				</div>
			</div>
		</a>
	</li>
	<li class="divider"></li>
	<li>
</ul>   
<ul id="dropdown4" class="dropdown-content dropdown-tasks w250">
  <li>
                            <a href="#">
                                <div>
                                    <strong>John Doe</strong>
                                    <span class="pull-right text-muted">
                                        <em>Today</em>
                                    </span>
                                </div>
                                <div>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem Ipsum has been the industry's standard dummy text ever since an kwilnw...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem Ipsum has been the industry's standard dummy text ever since the...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
</ul>  
	   <!--/. NAV TOP  -->
        <nav class="navbar-default navbar-side" role="navigation" style="display: none;">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                  
                    <li>
                        <a class="active-menu waves-effect waves-dark" href="index.html"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="ui-elements.html" class="waves-effect waves-dark"><i class="fa fa-desktop"></i> UI Elements</a>
                    </li>
					<li>
                        <a href="chart.html" class="waves-effect waves-dark"><i class="fa fa-bar-chart-o"></i> Charts</a>
                    </li>
                    <li>
                        <a href="tab-panel.html" class="waves-effect waves-dark"><i class="fa fa-qrcode"></i> Tabs & Panels</a>
                    </li>
                    
                    <li>
                        <a href="table.html" class="waves-effect waves-dark"><i class="fa fa-table"></i> Responsive Tables</a>
                    </li>
                    <li>
                        <a href="form.html" class="waves-effect waves-dark"><i class="fa fa-edit"></i> Forms </a>
                    </li>


                    <li>
                        <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i> Multi-Level Dropdown<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link</a>
                            </li>
                            <li>
                                <a href="#">Second Level Link<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>
                                    <li>
                                        <a href="#">Third Level Link</a>
                                    </li>

                                </ul>

                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="empty.html" class="waves-effect waves-dark"><i class="fa fa-fw fa-file"></i> Empty Page</a>
                    </li>
                </ul>

            </div>

        </nav>
        <!-- /. NAV SIDE  -->
        <div runat="server" Visible="false" id="dvfilter" class="rpt-dashboard-first-panel">
          <div class="col-md-4 col-sm-4 col-xs-12">
           <%--  <asp:DropDownList class="form-control" id="ddlBranch" ClientIDMode="Static" runat="server" placeholder="Choose Branch"></asp:DropDownList>--%>
           </div>
            <div class="col-md-4 col-sm-4 col-xs-12">
				<asp:DropDownList ID="dd_customername" class="form-control" ClientIDMode="Static" runat="server"></asp:DropDownList>
            </div>
        <div class="col-md-4 col-sm-4 col-xs-12 btn-section-rpt-dashboard">

            <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass="btn btn-success" OnClick="btnsubmit_Click"/>
        </div>
            </div>
		<div id="page-wrapper">
       
		  <div class="header"> 

                        <h1 class="page-header">
                            Dashboard
                        </h1>
						 <%--<ol class="breadcrumb">
						  <li><a href="#">Home</a></li>
						  <li><a href="#">Dashboard</a></li>
						  <li class="active">Data</li>
						</ol>  --%>
									
		</div>
            <div id="page-inner">
            

			
                <div class="all_row" runat="server" id ="didashtiles">
                        <div class="col-xs-12 col-sm-3 col-md-2">
					 
							<div class="card horizontal cardIcon waves-effect waves-dark">
                                <input type="hidden" value="total_sale" class="hdn_req"/>
						<div class="card-image blue">
						<i class="fa fa-pencil-square-o"></i>
						</div>
						<div class="card-stacked">
						<div class="card-content">
						<h3><asp:Label ID="LblTodayTotalSale" runat="server"></asp:Label></h3> 
						</div>
						<div class="card-action">
						<strong> Total Sale</strong>
						</div>
						</div>
						</div> 
						 
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                              <input type="hidden" value="cash_sale" class="hdn_req"/>
						    <div class="card-image red">
						     <i class="fa fa-money"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						            <h3><asp:Label ID="LblTodayCash" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						          <strong> Cash Sale</strong>
						        </div>
						    </div>
						</div>	 
                    </div>
                      <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                              <input type="hidden" value="cash_sale" class="hdn_req"/>
						    <div class="card-image red">
						     <i class="fa fa-money"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						            <h3><asp:Label ID="LblTodayCredit" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						            <strong> Credit Sale</strong>
						        </div>
						    </div>
						</div> 						 
                    </div>
                                   <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                                <input type="hidden" value="crcard_sale" class="hdn_req"/>
						    <div class="card-image">
						     <i class="fa fa-credit-card"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						            <h3><asp:Label ID="LblTodayCrCardSale" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						            <strong> Cr.Card Sale</strong>
						        </div>
						    </div>
						</div> 						 
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2">
					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                                <input type="hidden" value="online_sale" class="hdn_req"/>
						<div class="card-image orange">
						<i class="fa fa-shopping-cart fa-5x"></i>
						</div>
						<div class="card-stacked">
						<div class="card-content">
						<h3><asp:Label ID="LblTodayOnlineSale" runat="server"></asp:Label></h3> 
						</div>
						<div class="card-action">
						<strong> Online Sale</strong>
						</div>
						</div>
						</div> 
                    </div>

					  <div class="col-xs-12 col-sm-3 col-md-2">
					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                               
						<div class="card-image orange">
						<i class="fa fa-shopping-cart fa-5x"></i>
						</div>
						<div class="card-stacked">
						<div class="card-content">
						<h3><asp:Label ID="lblswiggysale" runat="server"></asp:Label></h3> 
						</div>
						<div class="card-action">
						<strong> SWIGGY</strong>
						</div>
						</div>
						</div> 
                    </div>

					 <div class="col-xs-12 col-sm-3 col-md-2">
					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                               
						<div class="card-image orange">
						<i class="fa fa-shopping-cart fa-5x"></i>
						</div>
						<div class="card-stacked">
						<div class="card-content">
						<h3><asp:Label ID="lblzomatosale" runat="server"></asp:Label></h3> 
						</div>
						<div class="card-action">
						<strong> ZOMATO</strong>
						</div>
						</div>
						</div> 
                    </div>
                              <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
						    <div class="card-image red">
						     <i class="fa fa-openid"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						            <h3><asp:Label ID="LblTodayOpenKot" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						         <strong> Open Kot</strong>
						        </div>
						    </div>
						</div>	 
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2">
					
							<div class="sel_tile card horizontal cardIcon waves-effect waves-dark">
                               
						<div class="card-image blue">
						<i class="fa fa-signal"></i>
						</div>
						<div class="card-stacked">
						<div class="card-content">
						<h3><asp:Label ID="LblTodayOnlineSaleOrder" runat="server"></asp:Label></h3> 
						</div>
						<div class="card-action">
						<strong> Online Sale Orders</strong>
						</div>
						</div>
						</div> 
                
                    </div>
             
						 
                    <div class="col-xs-12 col-sm-3 col-md-2">					
					    <div class="card horizontal cardIcon waves-effect waves-dark">
						    <div class="card-image">
						    <i class="fa fa-th"></i>
						    </div>
						    <div class="card-stacked">
						    <div class="card-content">
						    <h3><asp:Label ID="LblTodayOpenTables" runat="server"></asp:Label></h3> 
						    </div>
						    <div class="card-action">
						    <strong> Open Tables</strong>
						    </div>
						    </div>
					    </div> 						 
                    </div>

          

                    <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
						    <div class="card-image orange">
						     <i class="fa fa-exclamation-circle"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						        <h3><asp:Label ID="LblTodayPendingSettelment" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						        <strong> Pending Settelment</strong>
						        </div>
						    </div>
						</div> 
                    </div>


					 <div class="col-xs-12 col-sm-3 col-md-2">					
						<div class="card horizontal cardIcon waves-effect waves-dark">
                              <input type="hidden" value="cash_sale" class="hdn_req"/>
						    <div class="card-image red">
						     <i class="fa fa-money"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						        <h3><asp:Label ID="LblTodayGST" runat="server"></asp:Label></h3> 
						        </div>
						        <div class="card-action">
						        <strong> GST Sale</strong>
						        </div>
						    </div>
						</div> 
                    </div>

      

<%--     

                    <div class="col-xs-12 col-sm-3 col-md-2">					
					    <div class="card horizontal cardIcon waves-effect waves-dark">
						    <div class="card-image">
						        <i class="fa fa-users fa-5x"></i>
						    </div>
						    <div class="card-stacked">
						        <div class="card-content">
						         <h3>72,525</h3> 
						        </div>
						        <div class="card-action">
						         <strong> No. of Visits</strong>
						        </div>
						    </div>
						</div> 						 
                    </div>--%>

                </div>

                <div class="all_row">
                    
                      <div class="demo progress_demo">    
                          <h1>Sale Comparison</h1>  
                              <!-- example starts here -->
                              <label>Today Sale</label>
                              <div class="progress">
                                <div class="progress-bar" role="progressbar" id="dv_todayprogressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" >
                                  <span id="todaysale_spn"></span>
                                </div>
                              </div>
                              <label>Yesterday Sale</label>
                              <div class="progress">
                                <div class="progress-bar" id="dv_yeasterdayprogressbar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" >
                                  <span id="yeasterdaysale_spn"></span>
                                </div>
                              </div>
      
                              <!-- example ends here -->      
                      </div>
                </div>
			
                <!-- /. ROW  
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-3"> 
					<div class="card-panel text-center">
						<h4>Profit</h4>
						<div class="easypiechart" id="easypiechart-blue" data-percent="82" ><span class="percent">82%</span>
						</div> 
					</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3"> 
					<div class="card-panel text-center">
						<h4>No. of Visits</h4>
						<div class="easypiechart" id="easypiechart-red" data-percent="46" ><span class="percent">46%</span>
						</div>
					</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3"> 
					<div class="card-panel text-center">
						<h4>Customers</h4>
						<div class="easypiechart" id="easypiechart-teal" data-percent="84" ><span class="percent">84%</span>
						</div> 
					</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3"> 
					<div class="card-panel text-center">
						<h4>Sales</h4>
						<div class="easypiechart" id="easypiechart-orange" data-percent="55" ><span class="percent">55%</span>
						</div>
					</div>
			</div> 
		</div>/.row-->
			
		
				
				<div class="all_row" style="display:none;">

				    <div class="col-md-5"> 
						<div class="card">
						    <div class="card-image">
						     <div id="morris-line-chart"></div>
						    </div> 
						    <div class="card-action">
						      <b>Line Chart</b>
						    </div>
						</div>		  
					</div>		
					
					<div class="col-md-7"> 
					    <div class="card">
					        <div class="card-image">
					          <div id="morris-bar-chart"></div>
					        </div> 
					        <div class="card-action">
					          <b> Bar Chart Example</b>
					        </div>
					    </div>					
					</div>
					
				</div> 
			 
				
				
                <div class="all_row" style="display:none;">

                    <div class="col-md-9 col-sm-12 col-xs-12">
						<div class="card">
					        <div class="card-image">
					          <div id="morris-area-chart"></div>
					        </div> 
					        <div class="card-action">
					          <b>Area Chart</b>
					        </div>
					    </div>	 
                    </div>

                    <div class="col-md-3 col-sm-12 col-xs-12">
						<div class="card">
					        <div class="card-image">
					          <div id="morris-donut-chart"></div>
					        </div> 
					        <div class="card-action">
					          <b>Donut Chart Example</b>
					        </div>
					    </div>	 
                    </div>

                </div>

				<div class="all_row">
				<div class="col-md-12">
				
					</div>		
				</div> 	
                <!-- /. ROW  -->

	   
				
				
				
                <div class="all_row" style="display: none;">
                    <div class="col-md-4 col-sm-12 col-xs-12">
						<div class="card"><div class="card-action">
					  <b>Tasks Panel</b>
					</div>
					<div class="card-image">
					  <div class="collection">
						  <a href="#!" class="collection-item">Red<span class="new badge red" data-badge-caption="red">4</span></a>
						  <a href="#!" class="collection-item">Blue<span class="new badge blue" data-badge-caption="blue">4</span></a>
						  <a href="#!" class="collection-item"><span class="badge">1</span>Alan</a>
							<a href="#!" class="collection-item"><span class="new badge">4</span>Alan</a>
							<a href="#!" class="collection-item">Alan<span class="new badge blue" data-badge-caption="blue">4</span></a>
							<a href="#!" class="collection-item"><span class="badge">14</span>Alan</a>
							   <a href="#!" class="collection-item">Custom Badge Captions<span class="new badge" data-badge-caption="custom caption">4</span></a>
							<a href="#!" class="collection-item">Custom Badge Captions<span class="badge" data-badge-caption="custom caption">4</span></a>
						</div>
					</div> 
					
					</div>	  

                    </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
				<div class="card">
					<div class="card-action">
					  <b>User List</b>
					</div>
					<div class="card-image">
					  <ul class="collection">
					    <li class="collection-item avatar">
					      <i class="material-icons circle green">insert_chart</i>
					      <span class="title">Title</span>
					      <p>First Line <br>
					         Second Line
					      </p>
					      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
					    </li>
					    <li class="collection-item avatar">
					      <i class="material-icons circle">folder</i>
					      <span class="title">Title</span>
					      <p>First Line <br>
					         Second Line
					      </p>
					      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
					    </li>
					    <li class="collection-item avatar">
					      <i class="material-icons circle green">insert_chart</i>
					      <span class="title">Title</span>
					      <p>First Line <br>
					         Second Line
					      </p>
					      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
					    </li>
					    <li class="collection-item avatar">
					      <i class="material-icons circle red">play_arrow</i>
					      <span class="title">Title</span>
					      <p>First Line <br>
					         Second Line
					      </p>
					      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
					    </li>
					  </ul>
					 </div>  
				</div>			
                       
            </div>
                </div>
                <!-- /. ROW  -->
			   <div class="fixed-action-btn horizontal click-to-toggle" style="display: none;">
			    <a class="btn-floating btn-large red">
			      <i class="material-icons">menu</i>
			    </a>
			    <ul>
			      <li><a class="btn-floating red"><i class="material-icons">insert_chart</i></a></li>
			      <li><a class="btn-floating yellow darken-1"><i class="material-icons">format_quote</i></a></li>
			      <li><a class="btn-floating green"><i class="material-icons">publish</i></a></li>
			      <li><a class="btn-floating blue"><i class="material-icons">attach_file</i></a></li>
			    </ul>
			  </div>
		
				<%--<footer><p>All right reserved <a href="http://matrixrfid.in/">matrixrfid.in</a></p></footer>--%>

            </div>
            <!-- /. PAGE INNER  -->
        </div>
        <!-- /. PAGE WRAPPER  -->
    </div>
    <!-- /. WRAPPER  -->
      
 
    <script src="../dashboard_style/dashboard_js/custom-scripts.js"></script>
    <script src="../dashboard_style/dashboard_js/easypiechart-data.js"></script>
    <script src="../dashboard_style/dashboard_js/easypiechart.js"></script>

    <script src="../dashboard_style/dashboard_js/jquery.chart.js"></script>
    <script src="../dashboard_style/dashboard_js/jquery.metisMenu.js"></script>
    <script src="../dashboard_style/dashboard_js/materialize.min.js"></script>
    <script src="../dashboard_style/dashboard_js/morris.js"></script>
    <script src="../dashboard_style/dashboard_js/raphael-2.1.0.min.js"></script>
</asp:Content>



