﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using DevExpress.XtraPrinting;

public partial class ReportIngridient : System.Web.UI.Page
{

    public int Qty { get { return Request.QueryString["Qty"] != null ? Convert.ToInt32(Request.QueryString["Qty"]) : 0; } }
    public int ItemId { get { return Request.QueryString["ItemId"] != null ? Convert.ToInt32(Request.QueryString["ItemId"]) : 0; } }
    public DateTime Date { get { return Request.QueryString["Date"] != null ? Convert.ToDateTime(Request.QueryString["Date"]) : DateTime.Now; } }
    public bool BestExp { get { return Request.QueryString["BestExp"] != null ? Convert.ToBoolean(Request.QueryString["BestExp"]) : false; } }
    public string Day { get { return Request.QueryString["Day"] != null ? Convert.ToString(Request.QueryString["Day"]) : string.Empty; } }
    public int Branch { get { return Request.QueryString["Branch"] != null ? Convert.ToInt32(Request.QueryString["Branch"]) : 0; } }
    public string field1 { get { return Request.QueryString["field1"] != null ? Convert.ToString(Request.QueryString["field1"]) : string.Empty; } }
    public string field2 { get { return Request.QueryString["field2"] != null ? Convert.ToString(Request.QueryString["field2"]) : string.Empty; } }
    public string field3 { get { return Request.QueryString["field3"] != null ? Convert.ToString(Request.QueryString["field3"]) : string.Empty; } }
    public string ingrdient { get { return Request.QueryString["ingrdient"] != null ? Convert.ToString(Request.QueryString["ingrdient"]) : string.Empty; } }
    
   protected void Page_Load(object sender, EventArgs e)
    {
        using (MemoryStream ms = new MemoryStream())
        {
            rptBarcodeing_v r = new rptBarcodeing_v(ItemId, Qty, Date, BestExp, Day, Branch,field1,field2,field3,ingrdient);
            r.CreateDocument();
            PdfExportOptions opts = new PdfExportOptions();
            opts.ShowPrintDialogOnOpen = true;
            r.ExportToPdf(ms, opts);
            ms.Seek(0, SeekOrigin.Begin);
            byte[] report = ms.ToArray();
            Page.Response.ContentType = "application/pdf";
            Page.Response.Clear();
            Page.Response.OutputStream.Write(report, 0, report.Length);
            //
        }
    }
}