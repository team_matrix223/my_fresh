﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="ManageOtherPayment.aspx.cs" Inherits="Default3" %>

<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">

 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 <link href="css/customcss/setup.css" rel="stylesheet" />

     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

 
  <script language="javascript" type="text/javascript">

      function ApplyRoles(Roles) {


          $("#<%=hdnRoles.ClientID%>").val(Roles);
      }

      var m_OtherPaymentId = -1;

      function ResetControls() {

          $("input[name='supplier']").prop("checked", false);

          m_OtherPaymentId = -1;
          var txtTitle = $("#txtTitle");
          var btnAdd = $("#btnAdd");
          var btnUpdate = $("#btnUpdate");
          txtTitle.focus();
          $("#chkIsActive").prop("checked", "checked");
          txtTitle.val("");
          txtTitle.focus();

          var arrRole = [];
          arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');
          btnAdd.css({ "display": "none" });
          for (var i = 0; i < arrRole.length; i++) {
              if (arrRole[i] == "1") {

                  btnAdd.css({ "display": "block" });
              }

          }


          btnUpdate.css({ "display": "none" });


          $("#btnReset").css({ "display": "none" });
          $("#hdnId").val("0");
          validateForm("detach");
      }



      function TakeMeTop() {
          $("html, body").animate({ scrollTop: 0 }, 500);
      }

      function RefreshGrid() {
          $('#jQGridDemo').trigger('reloadGrid');

      }

      function InsertUpdate() {

          var PaymetModes = $('#<%=dd_customername.ClientID %>').val();
          var Id = m_OtherPaymentId;
          var Title = $("#txtTitle").val();
          if ($.trim(Title) == "") {
              $("#txtTitle").focus();
              return;
          }
          if (PaymetModes == 0)
          {
              return
          }
          var IsActive = false;

          if ($('#chkIsActive').is(":checked")) {
              IsActive = true;
          }
          $.ajax({
              type: "POST",
              data: '{"OtherPaymentId":"' + Id + '", "Title": "' + Title + '","IsActive": "' + IsActive + '","PaymetMode": "' + PaymetModes + '"}',
              url: "manageOtherPayment.aspx/Insert",
              contentType: "application/json",
              dataType: "json",
              success: function (msg) {

                  var obj = jQuery.parseJSON(msg.d);


                  if (obj.Status == -11) {
                      alert("You don't have permission to perform this action..Consult Admin Department.");
                      return;
                  }

                  if (obj.Status == -1) {

                      alert("Insertion Failed.OtherPayment with duplicate name already exists.");
                      return;
                  }
             
                  if (Id == "-1") {
                      ResetControls();

                     // jQuery("#jQGridDemo").jqGrid('addRowData', obj.OtherPayment.OtherPayment_ID, obj.OtherPayment, "last");

                      alert("OtherPayment is added successfully.");
                  }
                  else {
                      ResetControls();

                      var myGrid = $("#jQGridDemo");
                      var selRowId = myGrid.jqGrid('getGridParam', 'selrow');


                      myGrid.jqGrid('setRowData', selRowId, obj.OtherPayment);
                      alert("OtherPayment is Updated successfully.");
                  }


              },
              error: function (xhr, ajaxOptions, thrownError) {

                  var obj = jQuery.parseJSON(xhr.responseText);
                  alert(obj.Message);
              },
              complete: function () {
                  BindGrid();
                  $.uiUnlock();
              }
          });

      }

      $(document).ready(
    function () {
        BindGrid();


        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {
                    $("#btnAdd").show();
                    $("#btnAdd").click(
                    function () {


                        m_OtherPaymentId = -1;
                        InsertUpdate();
                    }
                    );

                }
                else if (arrRole[i] == "3") {

                    $("#btnUpdate").click(
       function () {

           var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
           if ($.trim(SelectedRow) == "") {
               alert("No OtherPayment is selected to Edit");
               return;
           }

           InsertUpdate();
           BindGrid();
       }
       );
                }

                else if (arrRole[i] == "2") {

                    $("#btnDelete").show();
                    $("#btnDelete").click(
      function () {

          var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
          if ($.trim(SelectedRow) == "") {
              alert("No OtherPayment is selected to Delete");
              return;
          }

          var OtherPaymentId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'OtherPayment_ID')
          if (confirm("Are You sure to delete this record")) {
              $.uiLock('');




              $.ajax({
                  type: "POST",
                  data: '{"OtherPaymentId":"' + OtherPaymentId + '"}',
                  url: "manageOtherPayment.aspx/Delete",
                  contentType: "application/json",
                  dataType: "json",
                  success: function (msg) {

                      var obj = jQuery.parseJSON(msg.d);
                      if (obj.status == -10) {
                          alert("You don't have permission to perform this action..Consult Admin Department.");
                          return;
                      }


                      if (obj.status == -1) {
                          alert("Deletion Failed. Company is in Use.");
                          return
                      }


                      BindGrid();
                      alert("Company is Deleted successfully.");




                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      var obj = jQuery.parseJSON(xhr.responseText);
                      alert(obj.Message);
                  },
                  complete: function () {
                      BindGrid();
                      $.uiUnlock();
                  }
              });









          }


      }
      );
                }
            }
        }








        $('#txtTitle').focus();
        $('#txtTitle').keypress(function (event) {


            var keycode = (event.keyCode ? event.keyCode : event.which);

            if (keycode == '13') {

                InsertUpdate();
            }


        });


        $("#btnReset").click(
        function () {

            ResetControls();
            InsertUpdate();
            BindGrid();

        }
        );


    }
    );

</script>
<style>
.setup_title
{
    background :#ffffff;
    color:#73879C;
}
select#cntAdmin_dd_customername
{
    border: 1px solid #DDE2E8;
    height: 25px !important;
    width:100%;
}
table#frmCity td {
    text-align: left;
}
div#btnAdd {
    position: relative;
    left: 135px;
}
.ui-jqgrid-bdiv {
    min-height: 104px;
    max-height: 104px;
    overflow: auto;
}
@media(max-width:330px)
{
    div#btnAdd {
	    position: relative;
	    left: 123px;
    }
}
@media(max-width:380px)
{
    table#frmCity input[type="text"], table#frmCity select {
        width: 70px !important;
    }
}
@media(min-width:381px) and (max-width:480px)
{
    table#frmCity input[type="text"], table#frmCity select
    {
        width:100% !important;
    }
}
@media(max-width:480px)
{
    
    #gbox_jQGridDemo {
	    width: 100% !important;
	    overflow: auto;
    }
}
@media(min-width:481px) and (max-width:567px)
{
    #gbox_jQGridDemo {
	    width: 100% !important;
	    overflow: auto;
    }
}
@media(min-width:992px) and (max-width:1200px)
{
    .ui-jqgrid-bdiv {
        min-height: 26px;
        max-height: 26px;
     }
}
</style>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Online Payment</h3>
                        </div>
                   <!--     <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                               <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div>-->
                    </div>
                    <div class="clearfix"></div>
                     

             

                     

                    <div class="x_title setup_title">
                            <h2>Add/Edit Online Payment</h2>
                             
                            <div class="clearfix"></div>
                        </div>
                    <div class="x_panel">
                        
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                     <tr><td class="headings">PaymentMode</td><td><asp:DropDownList ID="dd_customername"    class="" runat="server"  style=""></asp:DropDownList></td></tr>
                     <tr><td class="headings">OtherPayment Name:</td><td>  <input type="text"  name="txtTitle" maxlength="50" class="form-control validate required alphanumeric"   data-index="1" id="txtTitle" style="width: 213px"/></td></tr>
                    
                       <tr><td class="headings">IsActive:</td><td align="left" style="text-align:left">     <input type="checkbox" id="chkIsActive" checked="checked" data-index="2"  name="chkIsActive" /></td></tr> 
                      
                      
               
                                            <tr>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none;" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Add  </div></td>
                                            <td><div id="btnUpdate"  class="btn btn-success btn-small" style="display:none;" ><i class="fa fa-edit m-right-xs"></i> Update    </div></td>
                                            <td><div id="btnReset"  class="btn btn-danger btn-small" style="display:none;" ><i class="fa fa-mail-reply-all"></i> Cancel</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>

              <!--      <div class="x_title setup_title">
                            <h2>Manage OtherPayment</h2>
                             
                            <div class="clearfix"></div>
                        </div>-->
     <div class="x_panel">
                        
                        <div class="x_content">

                               <div class="youhave"  >
                    
      	          <table id="jQGridDemo">
    </table>

     <table cellspacing="0" cellpadding="0">
                                            <tr>
                                           
                                            
                                            <td>&nbsp;</td><td style="padding-top:5px"> <div id="btnDelete" style="display:none;"  class="btn btn-danger btn-small" ><i class="fa fa-trash m-right-xs"></i> Delete  </div></td>
                                            </tr>
                                            </table>
                                            <div id=" jQGridDemo"></div>
    <div id="jQGridDemoPager">
    </div>
      
                    </div>

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->


            </div>

                <!-- footer content -->
                <footer>
                      <uc1:ucfooter ID="ucfooter2" runat="server" />
                </footer>
                <!-- /footer content -->
 
</form>

            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageOtherPaymentMode.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['OtherPaymentId', 'PaymenttModeID', 'Title', 'IsActive'],
                        colModel: [
                                    { name: 'OtherPayment_ID', key: true, index: 'OtherPayment_ID', width: 100, stype: 'text', sorttype: 'int', hidden: true },
                                     { name: 'PaymetMode', index: 'PaymetMode', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                     { name: 'OtherPayment_Name', index: 'OtherPayment_Name', width: 200, stype: 'text', sortable: true, editable: true, editrules: { required: true } },
                                     { name: 'IsActive', index: 'IsActive', width: 150, editable: true, edittype: "checkbox", editoptions: { value: "true:false" }, formatter: "checkbox", formatoptions: { disabled: true } },
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30],
                        pager: '#jQGridDemoPager',
                        sortname: 'OtherPayment_ID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "OtherPaymentList",

                        editurl: 'handlers/ManageOtherPaymentMode.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {
                onSelectRow: function (rowid, iRow, iCol, e) {
               
                    m_OtherPaymentId = 0;
                    validateForm("detach");
                    var txtTitle = $("#txtTitle");
                    var ddlPaymentmode = $('#<%=dd_customername.ClientID %>');
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                    $("#btnUpdate").css({ "display": "none" });
                    $("#btnReset").css({ "display": "none" });
                    $("#btnAdd").css({ "display": "none" });
                  

                    for (var i = 0; i < arrRole.length; i++) {

                        if (arrRole[i] == 1) {

                            $("#btnAdd").css({ "display": "block" });
                        }

                      
                        if (arrRole[i] == 3) {



                            m_OtherPaymentId = $('#jQGridDemo').jqGrid('getCell', rowid, 'OtherPayment_ID');

                            

                           $("#txtTitle").val($('#jQGridDemo').jqGrid('getCell', rowid, 'OtherPayment_Name'));
                        
                           $('#<%=dd_customername.ClientID %>').val($('#jQGridDemo').jqGrid('getCell', rowid, 'PaymetMode'))
                            

                            if ($('#jQGridDemo').jqGrid('getCell', rowid, 'IsActive') == "true") {
                                $('#chkIsActive').prop('checked', true);
                            }
                            else {
                                $('#chkIsActive').prop('checked', false);

                            }
                            txtTitle.focus();
                           ddlPaymentmode.focus();
                            $("#btnAdd").css({ "display": "none" });
                            $("#btnUpdate").css({ "display": "block" });
                            $("#btnReset").css({ "display": "block" });
                        }
                    }
                    TakeMeTop();
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '500');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>


</asp:Content>