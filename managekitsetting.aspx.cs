﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ApplicationSettings_managekitsetting : System.Web.UI.Page
{
    int BranchId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBranches();
                                 
        }
        CheckRole();
        if (ddlBDBranch.SelectedValue == "0")
        {
            BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        }
        else
        {
            BranchId = Convert.ToInt32(ddlBDBranch.SelectedValue);
        }
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.KITSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");
        }
    }

    [WebMethod]
    public static string Update(string Column, string Value,int BranchId)
    {

        
        Column = Column.Substring(0, Column.Length - 1);
        Value = Value.Substring(0, Value.Length - 1);

        string[] arrColumn = Column.Split(',');
        string[] arrValue = Value.Split(',');

        string Query = "";
        for (int i = 0; i < arrColumn.Length; i++)
        {
            var val = arrValue[i] == "true" ? "1" : "0";

            Query += "update MasterSettting_PurchaseGridOption  set CEDIT='" + val + "' where ColumnName='" + arrColumn[i] + "' and Type='KT' and BranchId = "+ BranchId+"";

        }
        int status = new CommonSettingsBLL().UpdateOptions(Query);

        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);

    }
    void BindBranches()
    {
        ddlBDBranch.DataSource = new BranchBLL().GetAll();
        ddlBDBranch.DataValueField = "BranchId";
        ddlBDBranch.DataTextField = "BranchName";
        ddlBDBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBDBranch.Items.Insert(0, li1);

    }

    protected void ddlBDBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGridSettingsTab1();         
    }

    public void BindGridSettingsTab1()
    {
        ltKitSettings.Text = new CommonSettingsBLL().GetPurchaseGridOptionsByType(BranchId, "KT");
    }
}