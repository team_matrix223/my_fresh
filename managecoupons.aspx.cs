﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class managecoupons : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    [WebMethod]
    public static string Insert(string BillNo, Int32 NoOfCoupons,Int32 Amount)
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        Coupons objCoupon = new Coupons()
        {
            BillNo = BillNo,
            NoOfCoupons = NoOfCoupons,
            Amount = Amount,
            BranchId = Branch

        };
        int status = new CouponBLL().InsertUpdate(objCoupon);
        var JsonData = new
        {
            Coupon = objCoupon,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Delete(Int32 RefNo)
    {
        Coupons objCoupon = new Coupons()
        {
            RefNo = RefNo,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new CouponBLL().DeleteCoupon(objCoupon);
        var JsonData = new
        {
            coupon = objCoupon,
            status = Status
        };
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string CouponAmt(Int32 CouponNo)
    {
        Coupons objCoupon = new Coupons()
        {
            CouponNo = CouponNo,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new CouponBLL().CouponAmt(objCoupon);
        var JsonData = new
        {
            coupon = objCoupon,
            status = Status
        };
        return ser.Serialize(JsonData);
    }



}