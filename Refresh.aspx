﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Refresh.aspx.cs" Inherits="Refresh" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
    <link href="css/customcss/setup.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="print.css">
 <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <script src="print.js"></script>
 
     
   <script language="javascript" type="text/javascript">


       function ApplyRoles(Roles) {


           $("#<%=hdnRoles.ClientID%>").val(Roles);
       }

      

	  

    
    $(document).ready(
    function () {
       
           $("#btnPrint").click(
                function () {
					    var dbname = $("#<%=hdnDb.ClientID%>").val();
					    var link = $("#<%=hdnurl.ClientID%>").val();
                        var finallink = (link + 'PDFFile/' + dbname + '/CS-81589.pdf');
                        document.getElementById("frame").src = finallink;
                   
                        var myIframe = document.getElementById("frame").contentWindow;
                   
                   //     myIframe.focus();
                   //myIframe.print();
                   window.print();
						return false;
					
                });
        ValidateRoles();

            function ValidateRoles() {

                var arrRole = [];
                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

                for (var i = 0; i < arrRole.length; i++) {
                    if (arrRole[i] == "1") {

                        $("#btnAdd").show();
                        $("#btnAdd").click(
                            function () {
								$.ajax({
									type: "POST",
									data: '{}',
									url: "Refresh.aspx/RefreshSettings",
									contentType: "application/json",
									dataType: "json",
									success: function (msg) {

										var obj = jQuery.parseJSON(msg.d);

										




									},
									error: function (xhr, ajaxOptions, thrownError) {

										var obj = jQuery.parseJSON(xhr.responseText);
										alert(obj.Message);
									},
									complete: function () {
										$.uiUnlock();
									}
								});


                               
                            }
                        );


                    }


                }

            }
      

        

    }
    );

</script>
<style>.setup_title
{
    background :#ffffff;
    color:#73879C;
}
select#cntAdmin_ddlAccount {
    border: 1px solid #DDE2E8;
    height: 25px !important;
}
.youhave {
    padding: 15px 0;
}
.manage-bank-x-panel-sec
{
    max-height: 293px;
    overflow: auto;
}
table#frmCity td {
    text-align: left;
}
@media(max-width:480px)
{
    #frmCity input[type="text"], #frmCity select {
	    width: 100% !important;
    }
}
@media(max-width:567px)
{
    .x_content {
		    overflow: auto;
        }
}
@media(min-width:992px) and (max-width:1200px)
{
    .manage-bank-x-panel-sec {
	    max-height: 215px;
	    overflow: auto;
    }
}
</style>

<form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>
     <asp:HiddenField ID="hdnDb" runat="server"/>
      <asp:HiddenField ID="hdnurl" runat="server"/>
   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                       <!-- <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div> 
                            </div>
                        </div>-->
                    </div>
                    <div class="clearfix"></div>
                     
                    <div class="x_title setup_title">
                            
                             
                            
                        </div>
                    <div class="x_panel">
                        
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed" >
                     
                   
                    
                                            <tr>
                                             <td></td>
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td>&nbsp;</td><td> <div id="btnAdd" style="display:none" class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i> Refresh</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>

                         
                        <iframe id="frame" width="400" height="400"></iframe><br>
<button type="button" id="btnPrint">Print PDF</button>
                    </div>

                 
                      <div class="x_panel manage-bank-x-panel-sec">
                        <div class="x_content">

                               

                        </div>
                    </div>

                     
                </div>
                <!-- /page content -->

            </div>

                <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter2" runat="server" />
                </footer>
                <!-- /footer content -->
    
 
</form>

            <script type="text/javascript">
               





    </script>


</asp:Content>

