﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="newlogin.aspx.cs" Inherits="newlogin" EnableViewStateMac="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
   
	<link href="css/customcss/newlogin.css" rel="stylesheet" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>
  

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/animate.min.css" rel="stylesheet" />

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />


    <script src="js/jquery.min.js"></script>

 
</head>
    

<body>
    
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">
            <div id="login" class="animate form" style="background-image: url('<%= LogBG_IMG %>');">
                <section class="login_content">
                    <form id="form1" runat="server">
                       
                         <div class="login_inputs" runat="server" id="dvkeycode">
                              
                               <asp:TextBox ID="txtkey" ClientIDMode="Static" autocomplete="off" style="width:  !important;" AutoPostBack="true" OnTextChanged="Txtkey_TextChanged" placeholder="Master Key"   runat="server" ></asp:TextBox>
                                      
                             <%--  <asp:Label>Enter Pos</asp:Label>--%>
                                <asp:TextBox ID="txtNameUser" ClientIDMode="Static" autocomplete="off" style="width:  !important;" runat="server" ></asp:TextBox>
                               <asp:TextBox ID="txtpswrd" ClientIDMode="Static"  autocomplete="off" style="width:  !important;" runat="server" ></asp:TextBox>
                               
                               <asp:Button id="btkey" class="btn btn-default submit" Text="Login" style="" OnClick="btnLogin_Click" runat="server"/>
                                    <!-- width:65px!important;background: rgb(240, 102, 113) !important;color: black;font-weight: bold;-->
                          
                               </div>





                           <div class="login_inputs" runat="server" id="dvmasterkey" style ="display:none">
                              
                               <asp:TextBox ID="tbmasterkey" ClientIDMode="Static" autocomplete="off" TextMode="Password" style="width: 280px !important;" placeholder="Master Key"   runat="server" ></asp:TextBox>
                                      
                             <%--  <asp:Label>Enter Pos</asp:Label>--%>
                                <asp:TextBox ID="txtPos" ClientIDMode="Static" autocomplete="off" TextMode="Number"  style="width: 280px !important;" placeholder="Enter Pos Here"   runat="server" ></asp:TextBox>
                            
                               
                               <asp:Button id="Button1" class="btn btn-default submit" Text="Check" style="" OnClick="btnchkkey_Click" runat="server"/>
                                    <!-- width:65px!important;background: rgb(240, 102, 113) !important;color: black;font-weight: bold;-->
                          
                               </div>
                        <div runat="server" id="dvlocation" visible="false">
                         <div class="login_inputs">
                             <asp:DropDownList class="form-control" id="ddlLocation" runat ="server" AppendDataBoundItems ="true" DataTextField ="Client_Name" DataValueField ="Id" AutoPostBack="true" onselectedindexchanged="itemSelected" >
                             <asp:ListItem Value="0" Text="Choose Location"></asp:ListItem> 
                             
                             </asp:DropDownList>
                        <%-- <asp:DropDownList class="form-control" id="ddlLocation" runat="server" placeholder="Choose Location" AutoPostBack="true" onselectedindexchanged="itemSelected" >
                             <asp:ListItem Value="0" Text="Choose Location"></asp:ListItem> 
                             <asp:ListItem Value="1" Text="Connect Any Branch"></asp:ListItem>  
					  <asp:ListItem Value="2" Text="Connect 2020-21"></asp:ListItem>  
                         </asp:DropDownList>--%>
                        </div>

                      <div class="login_inputs"  runat="server" id="dvbranch"><asp:DropDownList class="form-control" id="ddlBranch" runat="server" placeholder="Choose Branch" ></asp:DropDownList></div>
                      <div class="login_inputs"><input type="text" id="txtUserName" class="form-control" runat="server" placeholder="Username"/></div>
                      <div class="login_inputs"><input type="password" id="txtPassword" class="form-control" runat="server" placeholder="Password" /></div>
                      <div class="login_content"><asp:Button id="btnLogin" class="btn btn-default submit" Text="Sign In" OnClick="btnLogin_Click" runat="server"/></div>
                        </div>
                      <div class="clearfix"></div>
                    </form>
                </section>
                <!-- content -->
            </div>
          
        </div>

</body>

</html>
