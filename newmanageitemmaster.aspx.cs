﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Text;

public partial class newmanageitemmaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (IsPostBack==false)
        {
            ddcustomertype();

            ddcompany.DataSource = new RGodownsBLL().GetAllCompany();
            ddcompany.DataTextField = "Company_Name";
            ddcompany.DataValueField = "Company_ID";
            ddcompany.DataBind();

            ListItem li1 = new ListItem();
            li1.Text = "-Company-";
            li1.Value = "0";
            ddcompany.Items.Insert(0, li1);
            deprtment();
            group();
            tax();
        }
        CheckRole();

    }
    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ITEMMASTER));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }
    public void tax()
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);

        string SqlQuery = "select Tax_ID,Tax_Rate from prop_taxstructure";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        ddtax.DataSource = dt;
        ddtax.DataTextField = "Tax_Rate";
        ddtax.DataValueField = "Tax_ID";
        ddtax.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "-Tax-";
        li1.Value = "0";
        ddtax.Items.Insert(0, li1);
    }
    public void group()
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);

        string SqlQuery = "select Group_ID,Group_Name  from prop_groups";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        ddgroup.DataSource = dt;
        ddgroup.DataTextField = "Group_Name";
        ddgroup.DataValueField = "Group_ID";
        ddgroup.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "-Group-";
        li1.Value = "0";
        ddgroup.Items.Insert(0, li1);
    }

    public void deprtment()
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);

        string SqlQuery = "select PROP_NAME,PROP_ID from departments";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        dddpt.DataSource = dt;
        dddpt.DataTextField = "PROP_NAME";
        dddpt.DataValueField = "PROP_ID";
        dddpt.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "-Department-";
        li1.Value = "0";
        dddpt.Items.Insert(0, li1);
    }
    public void ddcustomertype()
    {
        pos objpos = new pos();
        objpos.req = "bindgrid";
        DataTable dt = objpos.bindgride();
        ddpos.DataSource = dt;
        ddpos.DataTextField = "title";
        ddpos.DataValueField = "posid";
        ddpos.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "-POS-";
        li1.Value = "0";
        ddpos.Items.Insert(0, li1);
    }
    [WebMethod]
    public static string chkitemname(string Id)
    {

        ItemMaster objItemMaster = new ItemMaster()
        {
            ItemID =Convert.ToInt32(Id),

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int val = new ItemMasterBLL().ChkName(objItemMaster);

        return val.ToString();

    }

    [WebMethod]
    public static string LoadUserControl(string Id)
    {
        using (Page page = new Page())
        {
            UserControl userControl = (UserControl)page.LoadControl("Templates/newucItemMaster.ascx");
            (userControl.FindControl("ltContent") as Literal).Text = "<input type='hidden' id='hdnIMID' value='" + Id + "'/>";
            page.Controls.Add(userControl);
            using (StringWriter writer = new StringWriter())
            {
                page.Controls.Add(userControl);
                HttpContext.Current.Server.Execute(page, writer, false);
                return writer.ToString();
            }
        }
    }

    [WebMethod]
    public static string GetVariableMasters()
    {

        DataSet ds = new ItemMasterBLL().VariableMasterGetAll();
        List<Variable> lstDepartment = new List<Variable>();
        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        {
            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[0].Rows[i]["Prop_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[0].Rows[i]["Prop_ID"]);
           
            lstDepartment.Add(objVariable);
           
        }
        List<Variable> lstColor = new List<Variable>();
        for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[1].Rows[i]["Color_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[1].Rows[i]["Color_ID"]);
            lstColor.Add(objVariable);
        }

        List<Variable> lstItemType = new List<Variable>();
        for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[2].Rows[i]["ItemType_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[2].Rows[i]["ItemType_Id"]);
            lstItemType.Add(objVariable);
        }
        List<Variable> lstLocation = new List<Variable>();
        for (int i = 0; i < ds.Tables[3].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[3].Rows[i]["Location_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[3].Rows[i]["Location_ID"]);
            lstLocation.Add(objVariable);
        }

        List<Variable> lstSaleUnits = new List<Variable>();
        for (int i = 0; i < ds.Tables[4].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[4].Rows[i]["Unit_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[4].Rows[i]["Unit_Id"]);
            lstSaleUnits.Add(objVariable);
        }
        List<Variable> lstCompanies = new List<Variable>();
        for (int i = 0; i < ds.Tables[5].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[5].Rows[i]["Company_Name"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[5].Rows[i]["Company_ID"]);
            lstCompanies.Add(objVariable);
        }
        List<Variable> lstTaxRates = new List<Variable>();
        for (int i = 0; i < ds.Tables[6].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[6].Rows[i]["Tax_Rate"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[6].Rows[i]["Tax_ID"]);
            lstTaxRates.Add(objVariable);
        }

        List<Variable> lstExcise = new List<Variable>();
        for (int i = 0; i < ds.Tables[7].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[7].Rows[i]["ExciseDuty"].ToString() + '-' + ds.Tables[7].Rows[i]["Teriff_No"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[7].Rows[i]["Excise_ID"]);
            lstExcise.Add(objVariable);
        }
        List<Variable> lstHSN = new List<Variable>();
        for (int i = 0; i < ds.Tables[8].Rows.Count; i++)
        {

            Variable objVariable = new Variable();
            objVariable.Title = ds.Tables[8].Rows[i]["Title"].ToString();
            objVariable.Id = Convert.ToInt32(ds.Tables[8].Rows[i]["Id"]);
            lstHSN.Add(objVariable);
        }

        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        var JsonData = new
        {
            Departments = lstDepartment,
            Categories = lstColor,
            ItemTypes = lstItemType,
            Locations = lstLocation,
            SaleUnits = lstSaleUnits,
            Companies = lstCompanies,
            TaxRates = lstTaxRates,
            Excise = lstExcise,
            HSN = lstHSN,

        };
        return ser.Serialize(JsonData);


    }



    protected void btn_exp_excl_pl_Click(object sender, EventArgs e)
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);

        string SqlQuery = "select Prop_name as Department, item_code as ITEM_CODE,item_name as ITEM_NAME,sale_rate as UNIT_PRICE,Unit_name as UOM from packing_belongs inner join departments on packing_belongs.department=departments.prop_id  inner join Prop_SaleUnit on unit_id=Sales_In_Unit";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        string attachment = "attachment; filename=ProductList.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {
          
            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                Response.Write(tab + "\"=\"\"" + dr[i].ToString() + "\"\"\"");
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();


    }


    protected void btn_exp_excl_csv_Click(object sender, EventArgs e)
    {
        Connection conn = new Connection();
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        string SqlQuery = "select Master_code as PLU_CODE, item_code as ITEM_CODE,item_name as ITEM_NAME,sale_rate as UNIT_PRICE,Unit_name as UOM,day as USE_BY_DATE  from packing_belongs inner join Prop_SaleUnit on unit_id=Sales_In_Unit";
        con.Open();
        SqlCommand cmd = new SqlCommand(SqlQuery, con);
        cmd.CommandType = CommandType.Text;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);

        string attachment = "attachment; filename=Product_CSV.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/vnd.ms-excel";
        string tab = "";
        foreach (DataColumn dc in dt.Columns)
        {

            Response.Write(tab + dc.ColumnName);
            tab = "\t";
        }
        Response.Write("\n");
        int i;
        foreach (DataRow dr in dt.Rows)
        {
            tab = "";
            for (i = 0; i < dt.Columns.Count; i++)
            {
                Response.Write(tab + "\"=\"\"" + dr[i].ToString() + "\"\"\"");
                tab = "\t";
            }
            Response.Write("\n");
        }
        Response.End();


    }


    [WebMethod]
    public static string bind_itemlst(int pos,int deptt,int group,int tax,int company)
    {
        Connection conn = new Connection();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        StringBuilder str_builder = new StringBuilder();
        SqlConnection con = new SqlConnection(conn.sqlDataString);
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_getitemlist", con);
        cmd.Parameters.AddWithValue("@pos", pos);
        cmd.Parameters.AddWithValue("@group_id", group);
        cmd.Parameters.AddWithValue("@tax_code", tax);
        cmd.Parameters.AddWithValue("@department", deptt);
        cmd.Parameters.AddWithValue("@company_id", company);
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataReader rd = cmd.ExecuteReader();
        while (rd.Read())
        {
            str_builder.Append(string.Format("<tr><td>" + rd["Item_code"].ToString() + "</td><td class='cls_production_date'>" + rd["Item_name"].ToString() + "</td><td class='cls_production_id'>" + rd["Bar_code"].ToString() + "</td><td class='cls_production_id'>" + rd["purchase_rate"].ToString() + "</td><td class='cls_production_id'>" + rd["sale_rate"].ToString() + "</td><td class='cls_production_id'>" + rd["Tax_code"].ToString() + "</td><td class='cls_production_id'>" + rd["HSNcode"].ToString() + "</td><td class='cls_production_id'>" + rd["group_name"].ToString() + "</td><td class='cls_production_id'>" + rd["PROP_NAME"].ToString() + "</td></tr>"));
        }

        var JsonData = new
        {
            tableoption = str_builder.ToString(),

        };
        return ser.Serialize(JsonData);


    }
    [WebMethod]
    public static string BindSubGroups(int GroupId)
    {
        string SubGroup = new PropSGroupBLL().GetOptionsByGroupId(GroupId);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            SubGroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindSubGroups1()
    {

        string SubGroup = new PropSGroupBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            SubGroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindGroups()
    {

        string SubGroup = new PropGroupBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            GroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindPSMGroups()
    {

        string SubGroup = new PropGroupBLL().GetPSMOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            GroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }
    [WebMethod]
    public static string BindCompanies()
    {

        string SubGroup = new CompanyBLL().GetOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            GroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindPSMCompany()
    {

        string SubGroup = new CompanyBLL().GetPSmCompOptions();
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            GroupOptions = SubGroup,

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string BindIemDetail(int ItemId)
    {

        ItemMaster ObjItem = new ItemMaster();
        ObjItem.ItemID = ItemId;

        List<NutritionFacts> lst = new ItemMasterBLL().GetProductById(ObjItem);
       
        JavaScriptSerializer ser = new JavaScriptSerializer();
        var JsonData = new
        {
            ItemOptions = ObjItem,
            NutritionFact = lst
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string InsertUpdate(ItemMaster objItemMaster, NutritionFacts[] objNutritionFacts)
    {

        if (objItemMaster.Bar_Code.Trim() == "")
        {
            objItemMaster.Bar_Code = objItemMaster.Item_Code;
        }

        DataTable dt = new DataTable();
        dt.Columns.Add("Field1");
        dt.Columns.Add("Field2");
        dt.Columns.Add("Field3");
        dt.Columns.Add("Bold");
        dt.Columns.Add("Ingredients");
        DataRow dr;



        foreach (var item in objNutritionFacts)
        {
            dr = dt.NewRow();
            dr["Field1"] = item.Field1;
            dr["Field2"] = item.Field2;
            dr["Field3"] = item.Field3;
            dr["Bold"] = item.Bold;
            dr["Ingredients"] = objItemMaster.Ingredients;
            dt.Rows.Add(dr);


        }

        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        objItemMaster.UserId = Id;
        objItemMaster.BranchId = Branch;
        int status = new ItemMasterBLL().InsertUpdateforPsm(objItemMaster, dt);
        var JsonData = new
        {
            item = objItemMaster,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string GetByTax(decimal TaxRate)
    {
        TaxStructure ObjTax = new TaxStructure() { Tax_Rate = TaxRate };
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new TaxStructureDAL().GetByTaxStructure(ObjTax, Branch);
        var JsonData = new
        {

            TaxData = ObjTax,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetByItemCodepsm(string ItemCode)
    {
        Product ObjProduct = new Product() { Item_Code = ItemCode };
       
        new ProductBLL().GetByItemCodeforpsm(ObjProduct);
        var JsonData = new
        {

            Productdata = ObjProduct,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string getitemcodecheck(string ItemCode)
    {
       

        int status = new ProductBLL().GetItemCodeCheck(ItemCode);
        var JsonData = new
        {

           Status = status,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string Delete(Int32 ItemID)
    {
        ItemMaster objItemMaster = new ItemMaster()
        {
            ItemID = ItemID,

        };

        JavaScriptSerializer ser = new JavaScriptSerializer();

        int Status = new ItemMasterBLL().DeleteItem(objItemMaster);
        var JsonData = new
        {
            product = objItemMaster,
            status = Status
        };
        return ser.Serialize(JsonData);
    }


    [WebMethod]
    public static string FillSettings()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        ProductSetting ObjSettings = new ProductSetting();
        ObjSettings.BranchId = Branch;
        new ProductSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

}