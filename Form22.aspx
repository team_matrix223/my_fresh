﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="Form22.aspx.cs" Inherits="Form22" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">


        <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
    <link href="semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/form10.css" rel="stylesheet" />
    <link href="css/customcss/form23.css" rel="stylesheet" />
      <script type="text/javascript" src="js/SearchPlugin.js"></script>




   <script type="text/javascript">
	   var Stype = "";
	   var ProductCollectionDel = [];
       var ProductCollection = [];
       var BillCollection = [];
       var ItemBillCollection = [];
       var TotalItem = 0;
       var LeftItem = 0;
       var TotalItem1 = 0;
       var LeftItem1 = 0;
       var Counttr = 0;
       function clsProduct() {
         
           this.Item_Code = "";
           this.Item_Name = "";
           this.Rate = 0;
           this.Sale_Rate = 0;
           this.MRP = 0;
           this.QTY_TO_LESS = 0;
       

       }
       var ItemId = "";
       var mItemCode = "";
       var ItemName = "";
       var Rate = "";
       var Salerate = "";
       var QtyToLess = "";
	   var QtyInCase = "";
       var mrp = "";

	   function BindGridProducts(stext) {

		   var list = $("#jQGridProduct");
		   jQuery("#jQGridProduct").GridUnload();

		   jQuery("#jQGridProduct").jqGrid({
			   url: 'handlers/CProductsSearch.ashx?stext=' + stext,
			   ajaxGridOptions: { contentType: "application/json" },
			   datatype: "json",

			   colNames: ['Code', 'Name', 'Rate', 'MRP', 'TaxId', 'Tax', 'Hsn', 'Unit'
			   ],
			   colModel: [
				   { name: 'Item_Code', index: 'Item_Code', width: 100, stype: 'text', sortable: true, hidden: false },
				   { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, hidden: false },
				   { name: 'Sale_Rate', index: 'Sale_Rate', width: 50, stype: 'text', sortable: true, hidden: false },
				   { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 50, stype: 'text', sortable: true, hidden: false },
				   { name: 'Tax_ID', index: 'Tax_ID', width: 50, stype: 'text', sortable: true, hidden: true },
				   { name: 'Tax_Code', index: 'Tax_Code', width: 50, stype: 'text', sortable: true, hidden: false },
				   { name: 'hsncode', index: 'hsncode', width: 50, stype: 'text', sortable: true, hidden: true },
				   { name: 'Sales_In_Unit', index: 'Sales_In_Unit', width: 50, stype: 'text', sortable: true, hidden: true },

			   ],
			   //rowNum: 10,
			   mtype: 'GET',
			   loadonce: true,
			   //toppager: true,
			   //rowList: [10, 20, 30],
			   //pager: '#jQGridProductPager',
			   sortname: 'Item_Code',
			   viewrecords: true,
			   height: "100%",
			   width: "800px",

			   sortorder: 'desc',
			   caption: "",
			   editurl: 'handlers/CProductsSearch.ashx',

			   //toolbar: [true, "top"],
			   ignoreCase: true,



		   });




		   var $grid = $("#jQGridProduct");





		   $("#jQGridProduct").jqGrid('setGridParam',
			   {
				   ondblClickRow: function (rowid, iRow, iCol, e) {


                       var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
                       CodeSearch(serviceId);
					   $("#ItemGrid1").dialog("close");
					   event.stopPropagation();
					   isfirst = 1;
					   isupfirst = 1;
					   selrowf = true;

				   }
			   });







		   list.jqGrid('gridResize');
		   list.jqGrid('bindKeys');

		   var DataGrid = jQuery('#jQGridProduct');


		   DataGrid.jqGrid('setGridWidth', '400');
		   DataGrid.jqGrid('setSelection', 1, true);
		   //list.setSelection("selectRow", 0);



	   }





	   function CheckItemCode(ItemCode) {
		   $.ajax({
			   type: "POST",
			   data: '{"ItemCode": "' + ItemCode + '"}',
			   url: "BillScreenOption.aspx/ChkItemCode",
			   contentType: "application/json",
			   dataType: "json",
			   success: function (msg) {

				   var obj = jQuery.parseJSON(msg.d);

				   var isexist = obj.rtnval;

				   if (isexist == 1) {

					   BindGridProducts(ItemCode);
					   $("#ItemGrid1").dialog({
						   modal: true,
						   closeOnEscape: false,
					   });

					   $("#ItemGrid1").parent().addClass('Itemgrid-pop');


				   }
				   else {
					   alert("Item Code Does not Exists");
					   $("#txtServiceId").focus();
					   return;
				   }

			   },
			   error: function (xhr, ajaxOptions, thrownError) {

				   var obj = jQuery.parseJSON(xhr.responseText);
				   alert(obj.Message);
			   },
			   complete: function () {


				   $.uiUnlock();
			   }

		   });

	   }




       function CodeSearch(ItemCode) {

           var serviceId = ItemCode;
		   
			   if (serviceId != "") {
				   
				   $.ajax({
					   type: "POST",
					   data: '{ "ItemCode": "' + serviceId + '", "billtype": "' + 0 + '"}',
					   url: "BillScreenOption.aspx/GetByItemCode",
					   contentType: "application/json",
					   dataType: "json",
					   success: function (msg) {

						   var obj = jQuery.parseJSON(msg.d);

						   
						   if (obj.productData.Item_Name == "") {

							  
							 
							   CheckItemCode(serviceId);
							  

							   return;

						   }

                           else {
                               
                              
                               if (Stype == "Add") {
								   
								   $("#txtCode").val(serviceId);
								   $("#txtName").val(obj.productData.Item_Name);
								   $("#txtRate").val(obj.productData.Sale_Rate);
								   $("#txtMarketPrice").val(obj.productData.Max_Retail_Price);

								   ItemId = obj.productData.ItemID;
								   mItemCode = $("#txtCode").val();
								   ItemName = obj.productData.Item_Name;
								   QtyInCase = obj.productData.Qty_In_case;
								   Salerate = obj.productData.Sale_Rate;
								   QtyToLess = obj.productData.Qty_To_Less;


                               }
                               else {

								   $("#txtCodeDel").val(serviceId);
								   $("#txtNameDel").val(obj.productData.Item_Name);
								   $("#txtRateDel").val(obj.productData.Sale_Rate);
								   $("#txtMarketPriceDel").val(obj.productData.Max_Retail_Price);

								   ItemId = obj.productData.ItemID;
								   mItemCode = $("#txtCodeDel").val();
								   ItemName = obj.productData.Item_Name;
								   QtyInCase = obj.productData.Qty_In_case;
								   Salerate = obj.productData.Sale_Rate;
								   QtyToLess = obj.productData.Qty_To_Less;
                               }
                               

                              
							 
						   }


					   }, error: function (xhr, ajaxOptions, thrownError) {

						   var obj = jQuery.parseJSON(xhr.responseText);
						   alert(obj.Message);
					   },
					   complete: function (msg) {

						   $.uiUnlock();



					   }


				   });



			   }

		   


	   }




    
       function clsBill() {
          
           this.BillNo = "";
           this.Amount = 0;
           this.BillId = 0;


       }


       function clsItemBill() {

        
           this.Amount = 0;
         
           this.ItemCode = "";
           this.ItemName = "";


       }
       function GetPluginData(Type) {

               var m_ItemCode = $("#ddlProducts option:selected").attr("item_code");

               $("#txtCode").val(m_ItemCode);
               $("#txtName").val($("#ddlProducts option:selected").attr("item_name"));
             
               $("#txtRate").val($("#ddlProducts option:selected").attr("sale_rate"));
           $("#txtMarketPrice").val($("#ddlProducts option:selected").attr("mrp"));

		   var m_ItemCodeDel = $("#ddlProductsDel option:selected").attr("item_code");

		   $("#txtCodeDel").val(m_ItemCodeDel);
		   $("#txtNameDel").val($("#ddlProductsDel option:selected").attr("item_name"));

		   $("#txtRateDel").val($("#ddlProductsDel option:selected").attr("sale_rate"));
		   $("#txtMarketPriceDel").val($("#ddlProductsDel option:selected").attr("mrp"));
             
       }

	  

       function aa() {

           LeftItem = 0;
           $("input[name='chkbox1']").each(
            function (y) {


                if ($(this).prop("checked") == false) {

                    var Val = $(this).attr('val');
                    LeftItem = Number(LeftItem) + Number(Val);

                    $("#lblleft").html(LeftItem);
                }
                else {
                    $("#lblleft").html(LeftItem);
            
                }
         
            }

            );
       }




       $(document).on('change', '[name=chkbox1]', function () {



           aa();

           if ($(this).prop('checked') == true) {

               var Val = $(this).attr('val');
               TotalItem = Number(TotalItem) + Number(Val);
               $("#lblSalee").html(TotalItem);



           }
           else {
               var Val = $(this).attr('val');

               TotalItem = Number(TotalItem) - Number(Val);
               $("#lblSalee").html(TotalItem);

           }





       });


       function bb() {

           LeftItem1 = 0;
           $("input[name='chkbox']").each(
            function (y) {

          
                if ($(this).prop("checked") == false) {

                    var Val = $(this).attr('val');
                    LeftItem1 = Number(LeftItem1) + Number(Val);

                    $("#Label2").html(LeftItem1);

   
                   
                }
                else {
                    $("#Label2").html(LeftItem1);
               
                
                }
         
            }

            );
       }




       $(document).on('change', '[name=chkbox]', function () {



           bb();
          
           if ($(this).prop('checked') == true) {

               var Val = $(this).attr('val');
               TotalItem1 = Number(TotalItem1) + Number(Val);
               $("#Label1").html(TotalItem1);
               Counttr++;
               $("#btnBillCount").val(Counttr);

           }
           else {
               var Val = $(this).attr('val');

               TotalItem1 = Number(TotalItem1) - Number(Val);
               $("#Label1").html(TotalItem1);
               Counttr--;
               $("#btnBillCount").val(Counttr);

           }





       });

      
       function BindBills() {

           var html = "";

           for (var i = 0; i < BillCollection.length; i++) {




               html += "<tr id='tr_" + BillCollection[i]["BillId"] + "'>";
			   html += "<td>" + BillCollection[i]["BillNo"] + "</td>";
			   html += "<td>" + BillCollection[i]["Amount"] + "</td>";
               html += "<td><input type='checkbox' name= 'chkbox' val = " + BillCollection[i]["Amount"] + " id='chk_" + BillCollection[i]["BillId"] + "' /></td>";
              
               
              
             
               html += "</tr>";


           }
          
      
           $("#tbShowBills").html(html);



       }

       function BindItemBills() {


           var html = "";
          

           for (var i = 0; i < ItemBillCollection.length; i++) {




               html += "<tr>";
               html += "<td><input type='checkbox' name= 'chkbox1' val = " + ItemBillCollection[i]["Amount"] + "  id='chk_" + ItemBillCollection[i]["ItemCode"] + "' /></td>";
              
               html += "<td>" + ItemBillCollection[i]["ItemCode"] + "</td>";
            
               html += "<td>" + ItemBillCollection[i]["ItemName"] + "</td>";
               
               html += "<td>" + ItemBillCollection[i]["Amount"] + "</td>";

              
              


               html += "</tr>";

              

           }
    

           $("#tbShowItems").html(html);



       }

       function BindRows() {


           var TotalAmount = 0;
           var html = "";

           for (var i = 0; i < ProductCollection.length; i++) {



               html += "<tr>";
               html += "<td>" + ProductCollection[i]["Item_Code"] + "</td>";
               html += "<td>" + ProductCollection[i]["Item_Name"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["Qty_In_Case"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["Scheme"] + "</td>";
               html += "<td>" + ProductCollection[i]["Rate"] + "</td>";
               html += "<td>" + ProductCollection[i]["Sale_Rate"] + "</td>";
               html += "<td>" + ProductCollection[i]["MRP"] + "</td>";
               html += "<td style='display:none'>" + ProductCollection[i]["QTY_TO_LESS"] + "</td>";
               html += "<td><img id='btnDel' src='images/trashico.png'  style='cursor:pointer;width:20px'   /></td>";
               html += "</tr>";

               TotalAmount += parseFloat(ProductCollection[i]["Amount"])

           }

           $("#txtBillval").val(TotalAmount.toFixed(2));
           //var DisPer = $("#ddlDealer option:selected").attr("dis");
           var DisPer = "0";
           var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
           $("#txtDisPer").val(DisPer);
           $("#txtDisAmt").val(DisAmt);
           $("#txtAdj").val("0");
           $("#txtnetAmt").val(Number(TotalAmount) - Number(DisAmt));
           $("#tbKitProducts").html(html);



       }
	   function BindRowsDel() {


		   var TotalAmount = 0;
		   var html = "";

		   for (var i = 0; i < ProductCollectionDel.length; i++) {



			   html += "<tr>";
			   html += "<td>" + ProductCollectionDel[i]["Item_Code"] + "</td>";
			   html += "<td>" + ProductCollectionDel[i]["Item_Name"] + "</td>";
			   html += "<td style='display:none'>" + ProductCollectionDel[i]["Qty_In_Case"] + "</td>";
			   html += "<td style='display:none'>" + ProductCollectionDel[i]["Scheme"] + "</td>";
			   html += "<td>" + ProductCollectionDel[i]["Rate"] + "</td>";
			   html += "<td>" + ProductCollectionDel[i]["Sale_Rate"] + "</td>";
			   html += "<td>" + ProductCollectionDel[i]["MRP"] + "</td>";
			   html += "<td style='display:none'>" + ProductCollectionDel[i]["QTY_TO_LESS"] + "</td>";
			   html += "<td><img id='btnDelDel' src='images/trashico.png'  style='cursor:pointer;width:20px'   /></td>";
			   html += "</tr>";

			   TotalAmount += parseFloat(ProductCollectionDel[i]["Amount"])

		   }

		   $("#txtBillval").val(TotalAmount.toFixed(2));
		   //var DisPer = $("#ddlDealer option:selected").attr("dis");
		   var DisPer = "0";
		   var DisAmt = ((Number(TotalAmount) * Number(DisPer)) / 100);
		   $("#txtDisPer").val(DisPer);
		   $("#txtDisAmt").val(DisAmt);
		   $("#txtAdj").val("0");
		   $("#txtnetAmt").val(Number(TotalAmount) - Number(DisAmt));
		   $("#tbKitProductsDel").html(html);



	   }
       function ResetList() {


           $("#txtCode").val("");
           $("#txtName").val("");
           
           $("#txtRate").val("");
           $("#txtMarketPrice").val("");
      
        
           $("#ddlProducts").html("<option value='0'></option>");
           $("#txtddlProducts").val("").focus();
           TotalItem = 0;
           LeftItem = 0;
           TotalItem1 = 0;
           LeftItem1 = 0;
           $("#lblSalee").html("0");
        



       }
	   function ResetListDel() {


		   $("#txtCodeDel").val("");
		   $("#txtNameDel").val("");

		   $("#txtRateDel").val("");
		   $("#txtMarketPriceDel").val("");


		   $("#ddlProductsDel").html("<option value='0'></option>");
		   $("#txtddlProductsDel").val("").focus();
		   TotalItem = 0;
		   LeftItem = 0;
		   TotalItem1 = 0;
		   LeftItem1 = 0;
		   $("#lblSalee").html("0");




	   }

       $(document).ready(

           function () {
              

               $("#Button1").click(
               function () {

                   for (var i = 0; i < BillCollection.length; i++) {
                       Billprefix = BillCollection[i]["BillNo"];
                       if (Billprefix == $("#txtBarcode").val().toUpperCase()) {
                           billid = BillCollection[i]["BillId"];


                           $("#chk_" + billid).prop('checked', true);

                       
                           

                       }


                   }
                   $("#tbShowBills tr").removeAttr('style');
                   $("#tr_" + billid)[0].scrollIntoView();
             

                 

                   $('.main_container')[0].scrollIntoView(true);
                   $("#tr_" + billid).attr('style', 'background-color: chartreuse');

                  
                   //var rowpos = $("#tr_" + billid).position();
                   //rowpos.top = rowpos.top - 30;
                   //$('#tbShowBills').scrollTop(rowpos.top);
            

               }
               );

               function GenrateRowForaddDialog() {

                   var html = "";

                   for (var i = 0; i < BillCollection.length; i++) {
                       billid = BillCollection[i]["BillId"];
                       if ($("#chk_" + billid).prop('checked') == true) {
                      

                           //html += "<tr id='tr_" + BillCollection[i]["BillId"] + "'>";
                           html += "<div style='float: left;border: solid 1px;width: 100px;text-align: center'><label>" + BillCollection[i]["BillNo"] + "</label></div>";
                           //html += "<td>" + BillCollection[i]["Amount"] + "</td>";



                           //html += "</tr>";
                           $("#addDialog").html(html);
                       }
                   }

               }

               
               $("#txtBarcode").keyup(
            function (event) {
			event.preventDefault();
			
			var keycode = (event.keyCode ? event.keyCode : event.which);

			if (keycode == '13') {
		
			    for (var i = 0; i < BillCollection.length; i++) {
			  
                   Billprefix = BillCollection[i]["BillNo"];
                   if (Billprefix == $("#txtBarcode").val()) {
                       billid = BillCollection[i]["BillId"];

                       if ($("#chk_" + billid).prop('checked') != true)
                           {
                       $("#chk_" + billid).prop('checked', true);
                       $("#tr_" + billid)[0].scrollIntoView(false);
                       aa();
                       if ($("#chk_" + billid).prop('checked') == true) {
                       
                           var Val = $("#chk_" + billid).attr('val');
                         
                           TotalItem = Number(TotalItem) + Number(Val);
                           $("#lblSalee").html(TotalItem);
						   $("#Label1").html(TotalItem);
                           Counttr++;
                           $("#btnBillCount").val(Counttr);

						  
						   
                          

                       }
                       else {
                           var Val = $("#chk_" + billid).attr('val');

                           TotalItem = Number(TotalItem) - Number(Val);
                           $("#lblSalee").html(TotalItem);
						   $("#Label1").html(TotalItem);

                       }
                       }
                       bb();
                     
                   }


			    }
			

             
               $("#tbShowBills tr").removeAttr('style');
               $("#tr_" + billid)[0].scrollIntoView();
               $('.main_container')[0].scrollIntoView(true);
               $("#tr_" + billid).attr('style', 'background-color: chartreuse');

			   $("#txtBarcode").val("");

               }


			GenrateRowForaddDialog();

           });


               $("#txtStartDate,#txtEndDate").val($("#<%=hdnDate.ClientID%>").val());

              


               $("#txtServiceId").change(
                   function () {
                       Stype = "Del";
                       CodeSearch($("#txtServiceId").val());
                   });

			   $("#txtServiceIdAdd").change(
                   function () {
                       Stype = "Add";
					   CodeSearch($("#txtServiceIdAdd").val());
				   });


               $("#ddlProType").change(
         function () {

             var type = $("#ddlProType").val();
             var processtype = "";
             if (type == "0") {
                 alert("Choose Which Type Of Processing you want to do ...?");
                 return;
             }
             if (type == "1") {
                 processtype = "ItemWise";
             }
             else {
                 processtype = "BillWise";
             }

             //$.ajax({
             //    type: "POST",
             //    data: '{ "ProcessType": "' + processtype + '"}',
             //    url: "managesales.aspx/GetOptions",
             //    contentType: "application/json",
             //    dataType: "json",
             //    success: function (msg) {

             //        var obj = jQuery.parseJSON(msg.d);
             //        $("#ddlBillType").html(obj.lstOptions);

             //    },
             //    error: function (xhr, ajaxOptions, thrownError) {

             //        var obj = jQuery.parseJSON(xhr.responseText);
             //        alert(obj.Message);
             //    },
             //    complete: function () {

             //        $.uiUnlock();
             //    }

             // });

         });



               $("#dvShowBills").css("display", "none");
               $("#dvShowItems").css("display", "none");
               $("#btnShow").click(
                   function () {
                       $("#btnBillCount").css("display", "block");
                       Counttr = 0;
                       $("#btnBillCount").val(Counttr);
                       $.uiLock('');

                       TotalItem = 0;
                       $("#lblSalee").html("0");
                       LeftItem = 0;
                       $("#lblleft").html("0");

                       BillCollection = [];
                       ItemBillCollection = [];

                       $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       $('#tbShowItems tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       var ItemType = "";

                       var ItemType = $("#ddlBillType option:selected").text();

                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();
                       var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();
                       var ProType = $("#ddlProType").val();
                       if (ProType == "2") {

                           $("#dvShowBills").css("display", "block");
                           $("#dvaddItems").css("display", "none");
						   $("#dvDelItems").css("display", "none");
                           $("#dvShowItems").css("display", "none");

                           $.ajax({
                               type: "POST",
                               data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","ItemType": "' + ItemType + '","Branch": "' + Branch + '","Pos": "' + Pos + '"}',
                               url: "Form22.aspx/GetOrdersBYDate",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                   if (obj.BillsList.length > 0) {
                                       for (var i = 0; i < obj.BillsList.length; i++) {


                                           TO = new clsBill();

                                           TO.BillNo = obj.BillsList[i]["BillNowPrefix"];
                                           TO.Amount = obj.BillsList[i]["Net_Amount"];
                                           TO.BillId = obj.BillsList[i]["Bill_No"];
                                           BillCollection.push(TO);

                                       }
                                       $("#btnDelete").show();
                                       BindBills();
                                   }
                                   else {
                                       $("#btnDelete").hide();
                                       alert("No Bills Found.");
                                       return;

                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {

                                   $.uiUnlock();
                               }

                           });
                       }
                       else if (ProType == "1") {

                           $("#dvShowBills").css("display", "none");
                           $("#dvaddItems").css("display", "none");
						   $("#dvDelItems").css("display", "none");
                           $("#dvShowItems").css("display", "block");

                           $.ajax({
                               type: "POST",
                               data: '{ "DateFrom": "' + DateFrom + '","DateTo": "' + DateTo + '","ItemType": "' + ItemType + '","Branch": "' + Branch + '","Pos": "' + Pos + '"}',
                               url: "Form22.aspx/GetOrderItemBYDate",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {



                                   var obj = jQuery.parseJSON(msg.d);


                                   if (obj.BillsList.length > 0) {
                                       for (var i = 0; i < obj.BillsList.length; i++) {


                                           TO = new clsItemBill();

                                           TO.Amount = obj.BillsList[i]["Amount"];
                                           TO.ItemCode = obj.BillsList[i]["Item_Code"];
                                           TO.ItemName = obj.BillsList[i]["Item_Name"];
                                           ItemBillCollection.push(TO);

                                       }


                                       BindItemBills();
                                   }
                                   else {
                                       alert("No Bills Found.");
                                       return;

                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {


                                   $.uiUnlock();
                               }

                           });

                       }



                   });


               $("#btnBillCount").click(function () {
                   GenrateRowForaddDialog();

                   $('#addDialog').dialog(
     {
         autoOpen: false,
         height: 600,
         width: 1115,
         resizable: false,
         modal: false,
         title:'Selected Records'

     });


                   //change the title of the dialgo
                   linkObj = $(this);
                   var dialogDiv = $('#addDialog');
                   dialogDiv.dialog("option", "position", [70, 50]);
                   var viewUrl = "customeradd.aspx";
                   dialogDiv.dialog('open');

                   return false;

               });

               $("#btnDelete").click(
                   function () {

                       var IsChked = false;
                       var Billno = [];
                       var BillNowPrefix = [];
                       var Amount = [];
                       var Saletype = "";
                       var ItemType = "";
                       var ItemValue = $("#ddlBillType").val();
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }


                       Saletype = $("#ddlBillType option:selected").text();
                       var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();
                       for (var i = 0; i < BillCollection.length; i++) {
                           var billid = 0;
                           var Billprefix = "";
                           var netamount = 0;
                           billid = BillCollection[i]["BillId"];
                           Billprefix = BillCollection[i]["BillNo"];
                           netamount = BillCollection[i]["Amount"];


                           if ($("#chk_" + billid).prop('checked') == true) {


                               Billno[i] = billid;
                               BillNowPrefix[i] = Billprefix;
                               Amount[i] = netamount;
                               IsChked = true;
                           }

                       }

                       if (IsChked != true) {

                           alert("atleast one bill should be there to Save");
                           $.uiUnlock();
                           return;
                       }


                       if (confirm("Are You sure to save this record")) {
                           $.uiLock('');


                           $.ajax({
                               type: "POST",
                               data: '{"BillNowPrefix":"' + BillNowPrefix + '", "Amount": "' + Amount + '","BillNo": "' + Billno + '","ItemType": "' + ItemType + '","Saletype": "' + Saletype + '","Branch": "' + Branch + '","Pos": "' + Pos + '"}',
                               url: "Form22.aspx/Insert",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

								   var obj = jQuery.parseJSON(msg.d);
                                    

                                   alert("Sale Deleted Successfully");
								   BillCollection = [];

								   $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
								   $("#Label1").html(0);
								   $("#Label2").html(0);

                               },
							   error: function (xhr, ajaxOptions, thrownError) {

								   var obj = jQuery.parseJSON(xhr.responseText);
								   alert(obj.Message);
							   },
							   complete: function () {

								   $.uiUnlock();
							   }
                                   
                                   //if (obj.Status == "-4") {
                                   //    alert("Please Insert Items First");
                                   //    return;
                                   //}

                                   //if (obj.Status == "0") {
                                   //    alert("Sale Saved Successfully");

                                     //  BillCollection = [];

                                     //  $('#tbShowBills tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                   //}
                                   //else {
                                   //    alert("Operation Failed");
                                   //}

                               //},
                               //error: function (xhr, ajaxOptions, thrownError) {

                               //    var obj = jQuery.parseJSON(xhr.responseText);
                               //    alert(obj.Message);
                               //},
                               //complete: function () {
                               //    $.uiUnlock();

                               //}



                           });

                       }



                   });




               $("#btnDeleteItem").click(
                   function () {


                       var IsChked = false;

                       var ItemC = [];
                       var Amount = [];
                       var Saletype = "";
                       var ItemType = "";
                       var ItemValue = $("#ddlBillType").val();
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }

                       var DateFrom = $("#txtStartDate").val();
                       var DateTo = $("#txtEndDate").val();

                       Saletype = $("#ddlBillType option:selected").text();
                       var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();
                       for (var i = 0; i < ItemBillCollection.length; i++) {

                           var ItemCode = "";
                           var netamount = 0;
                           ItemCode = ItemBillCollection[i]["ItemCode"];

                           netamount = ItemBillCollection[i]["Amount"];


                           if ($("#chk_" + ItemCode).prop('checked') == true) {



                               ItemC[i] = ItemCode;
                               Amount[i] = netamount;
                               IsChked = true;
                           }

                       }


                       if (IsChked != true) {

                           alert("atleast one bill should be there to Save");
                           $.uiUnlock();
                           return;
                       }


                       if (confirm("Are You sure to Save this record")) {
                           $.uiLock('');



                           $.ajax({
                               type: "POST",
                               data: '{"ItemCode":"' + ItemC + '", "Amount": "' + Amount + '","ItemType": "' + ItemType + '","Saletype": "' + Saletype + '","Branch": "' + Branch + '","DateFrom":"' + DateFrom + '","DateTo":"' + DateTo + '","Pos":"' + Pos + '"}',
                               url: "Form22.aspx/DeleteItemsBill",
                               contentType: "application/json",
                               dataType: "json",
                               success: function (msg) {

                                   var obj = jQuery.parseJSON(msg.d);
                                   if (obj.Status == "-4") {
                                       alert("Please Insert Items First");
                                       return;
                                   }

                                   if (obj.Status == "0") {
                                       alert("Sale Saved Successfully");

                                       ItemBillCollection = [];

                                       $('#tbShowItems tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                                   }
                                   else {
                                       alert("Operation Failed");
                                   }

                               },
                               error: function (xhr, ajaxOptions, thrownError) {

                                   var obj = jQuery.parseJSON(xhr.responseText);
                                   alert(obj.Message);
                               },
                               complete: function () {
                                   $.uiUnlock();

                               }



                           });

                       }



                   });

			   var selrowf = true;

			   var isfirst = 0;
			   var isupfirst = 0;
			   var currow = 0;

			   $(document).on("keydown", "#jQGridProduct", function (e) {



				   if (e.keyCode == 40) {

					   currow = 0;

					   //arrow("next");

					   if (selrowf == true) {

						   $("#jQGridProduct").setSelection(1);
						   selrowf = false;

					   }
					   var list = $('#jQGridProduct'),

						   $td = $(e.target).closest("tr.jqgrow>td"),
						   p = list.jqGrid("getGridParam"),
						   //cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						   cm = "Item_Code";

					   var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					   var selectedRow = list.jqGrid('getGridParam', 'selrow');


					   if (isfirst == 0) {

						   selectedRow = selectedRow - 1;

					   }


					   if (selectedRow == null) return;

					   var ids = list.getDataIDs();
					   var index = list.getInd(selectedRow);

					   if (ids.length < 2) return;
					   index++;

					   list.setSelection(ids[index - 1], false, e);
					   currow = index;

					   var rows = document.querySelectorAll('#jQGridProduct tr');

					   var line = document.querySelector(1);



					   rows[line].scrollTop({
						   behavior: 'smooth',
						   block: 'nearest'
					   });

					   //                  var w = $(window);

					   //                  var row = $('#jQGridProduct').find('tr').eq(line);

					   //                  if (row.length) {

					   //                      w.scrollTop(row.offset().top - (w.height / 2));
					   //}
					   e.preventDefault();




				   }

				   if (e.keyCode == 38) {

					   currow = 0;
					   //arrow("prev");
					   var list = $('#jQGridProduct'),

						   $td = $(e.target).closest("tr.jqgrow>td"),
						   p = list.jqGrid("getGridParam"),
						   //cm = $td.length > 0 ? p.colModel[$td[0].cellIndex] : null;
						   cm = "Item_Code";

					   var cmName = cm !== 0 && cm.editable ? cm.name : 'Item_Code';


					   var selectedRow = list.jqGrid('getGridParam', 'selrow');


					   if (isupfirst == 0) {
						   selectedRow = Number(selectedRow) + Number(1);

					   }

					   if (selectedRow == null) return;
					   var ids = list.getDataIDs();

					   var index = list.getInd(selectedRow);

					   if (ids.length < 2) return;
					   index--;

					   list.setSelection(ids[index - 1], false, e);
					   currow = index;

					   var rows = document.querySelectorAll('#jQGridProduct tr');

					   var line = document.querySelector(1);



					   rows[line].scrollTop({
						   behavior: 'smooth',
						   block: 'nearest'
					   });

					   //var w = $(window);
					   //var row = $('#jQGridProduct').find('tr').eq(line);

					   //                  if (row.length) {
					   //                      list.scrollTop(row.offset().top + (12/ 2));
					   //}
					   e.preventDefault();


				   }

				   if (e.ctrlKey && e.keyCode == 13) {

					   if (currow == 0) {
						   currow = 1;
					   }
					   var rowid = currow;
                       var serviceId = $('#jQGridProduct').jqGrid('getCell', rowid, 'Item_Code');
                       if (Stype == "Add") {
						   $('#txtServiceIdAdd').val(serviceId);

                       }
                       else {
                           $('#txtServiceId').val(serviceId);
                       }
                       CodeSearch(serviceId);
					   $("#ItemGrid1").dialog("close");
					   event.stopPropagation();
					   isfirst = 1;
					   isupfirst = 1;
					   selrowf = true;
				   }
				   // }

			   });









               $("#btnItem").click(
                   function () {
           
                       Counttr = 0;
                     
                       $("#btnBillCount").val(Counttr);
                       $("#dvShowBills").css("display", "none");
                       $("#btnBillCount").css("display", "none");
                       ProductCollection = [];

                       $('#tbKitProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
                       var ItemType = "";
                       $("#dvaddItems").css("display", "block");
					   $("#dvDelItems").css("display", "none");
                       var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();
                       var ItemValue = $("#ddlItemType").val();
                       if (ItemValue == "0") {
                           alert("Choose Type");
                           return;
                       }
                       if (ItemValue == "1") {
                           ItemType = "Sale";
                       }
                       else {
                           ItemType = "Order";
                       }

                       $.uiLock();
                       $.ajax({
                           type: "POST",
                           data: '{ "ItemType": "' + ItemType + '", "Branch": "' + Branch + '","Pos": "' + Pos + '"}',
                           url: "Form22.aspx/GetProductsByType",
                           contentType: "application/json",
                           dataType: "json",
                           success: function (msg) {

                               var obj = jQuery.parseJSON(msg.d);

                               for (var i = 0; i < obj.productLists.length; i++) {


                                   TO = new clsProduct();


                                   TO.Item_Code = obj.productLists[i]["Item_Code"];
                                   TO.Item_Name = obj.productLists[i]["Item_Name"];
                                   TO.Rate = obj.productLists[i]["Rate"];
                                   TO.Sale_Rate = obj.productLists[i]["Rate"];
                                   TO.MRP = obj.productLists[i]["MRP"];
                                   TO.QTY_TO_LESS = obj.productLists[i]["qty_to_less"];

                                   ProductCollection.push(TO);
                                   BindRows();
                                  

                               }
                               if (obj.productLists.length > 0) {
                                   $("#btnDelete").show();

                               }
                               else {

                                   $("#btnDelete").hide();


                               }

                           },
                           error: function (xhr, ajaxOptions, thrownError) {

                               var obj = jQuery.parseJSON(xhr.responseText);
                               alert(obj.Message);
                           },
                           complete: function () {

                               $.uiUnlock();
                           }

                       });





                   }

               );

			   $("#btnItemDel").click(
				   function () {

					   Counttr = 0;

					   $("#btnBillCount").val(Counttr);
					   $("#dvShowBills").css("display", "none");
					   $("#btnBillCount").css("display", "none");
					   ProductCollectionDel = [];

					   $('#tbKitProductsDel tr').not(function () { if ($(this).has('th').length) { return true } }).remove();
					   var ItemType = "";
					   $("#dvaddItems").css("display", "none");
					   $("#dvDelItems").css("display", "block");
					   var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();
					   var ItemValue = $("#ddlItemType").val();
					   if (ItemValue == "0") {
						   alert("Choose Type");
						   return;
					   }
					   if (ItemValue == "1") {
						   ItemType = "Sale";
					   }
					   else {
						   ItemType = "Order";
					   }

					   $.uiLock();
					   $.ajax({
						   type: "POST",
						   data: '{ "ItemType": "' + ItemType + '", "Branch": "' + Branch + '","Pos": "' + Pos + '"}',
						   url: "Form22.aspx/GetProductsToDeleteByType",
						   contentType: "application/json",
						   dataType: "json",
						   success: function (msg) {

							   var obj = jQuery.parseJSON(msg.d);

							   for (var i = 0; i < obj.productLists.length; i++) {


								   TO = new clsProduct();


								   TO.Item_Code = obj.productLists[i]["Item_Code"];
								   TO.Item_Name = obj.productLists[i]["Item_Name"];
								   TO.Rate = obj.productLists[i]["Rate"];
								   TO.Sale_Rate = obj.productLists[i]["Rate"];
								   TO.MRP = obj.productLists[i]["MRP"];
								   TO.QTY_TO_LESS = obj.productLists[i]["qty_to_less"];

								   ProductCollectionDel.push(TO);
								   BindRowsDel();


							   }
							   if (obj.productLists.length > 0) {
								   $("#btnDelete").show();

							   }
							   else {

								   $("#btnDelete").hide();


							   }

						   },
						   error: function (xhr, ajaxOptions, thrownError) {

							   var obj = jQuery.parseJSON(xhr.responseText);
							   alert(obj.Message);
						   },
						   complete: function () {

							   $.uiUnlock();
						   }

					   });





				   }

			   );


               $(document).on("click", "#btnDel", function (event) {

                   var RowIndex = Number($(this).closest('tr').index());

                   ProductCollection.splice(RowIndex, 1);
                   BindRows();


               });


			   $(document).on("click", "#btnDelDel", function (event) {

				   var RowIndex = Number($(this).closest('tr').index());

				   ProductCollectionDel.splice(RowIndex, 1);
				   BindRowsDel();


			   });

               $("#btnSave").click(
               function () {



                   if (ProductCollection.length == 0) {

                       alert("Please choose Delivery Items");
                       $("#ddlProducts").focus();
                       return;
                   }
                   var ItemType = '';
                   if ($("#ddlItemType").val() == "1") {
                       ItemType = 'Sale';
                   }
                   else {
                       ItemType = 'Order';
                   }


                   var Branch = $("#ddlBranch").val();
				var Pos = $("#ddlPos").val();

                       var DTO = { 'objDeliveryDetails': ProductCollection, 'ItemType': ItemType, 'Branch': Branch, 'Pos': Pos };



                   $.ajax({
                       type: "POST",
                       contentType: "application/json; charset=utf-8",
                       url: "Form22.aspx/InsertUpdate",
                       data: JSON.stringify(DTO),
                       dataType: "json",
                       success: function (msg) {

                           var obj = jQuery.parseJSON(msg.d);


                           alert("Item Saved Successfully");

                           ProductCollection = [];

                           $('#tbKitProducts tr').not(function () { if ($(this).has('th').length) { return true } }).remove();



                       },
                       error: function (xhr, ajaxOptions, thrownError) {

                           var obj = jQuery.parseJSON(xhr.responseText);
                           alert(obj.Message);
                       },
                       complete: function () {

                       }
                   });









               });



			   $("#btnSaveDel").click(
				   function () {



					   if (ProductCollectionDel.length == 0) {

						   alert("Please choose Delivery Items");
						   $("#ddlProductsDel").focus();
						   return;
					   }
					   var ItemType = '';
					   if ($("#ddlItemType").val() == "1") {
						   ItemType = 'Sale';
					   }
					   else {
						   ItemType = 'Order';
					   }


					   var Branch = $("#ddlBranch").val();
					   var Pos = $("#ddlPos").val();

					   var DTO = { 'objDeliveryDetails': ProductCollectionDel, 'ItemType': ItemType, 'Branch': Branch, 'Pos': Pos };



					   $.ajax({
						   type: "POST",
						   contentType: "application/json; charset=utf-8",
						   url: "Form22.aspx/InsertUpdateDel",
						   data: JSON.stringify(DTO),
						   dataType: "json",
						   success: function (msg) {

							   var obj = jQuery.parseJSON(msg.d);


							   alert("Item Saved Successfully");

							   ProductCollectionDel = [];

							   $('#tbKitProductsDel tr').not(function () { if ($(this).has('th').length) { return true } }).remove();



						   },
						   error: function (xhr, ajaxOptions, thrownError) {

							   var obj = jQuery.parseJSON(xhr.responseText);
							   alert(obj.Message);
						   },
						   complete: function () {

						   }
					   });









				   });






               $("#btnAddKitItems").click(
                   function () {

                       var ExistItem = "";
                       for (var i = 0; i < ProductCollection.length; i++) {
                           if (mItemCode == ProductCollection[i].Item_Code && Salerate == ProductCollection[i].Sale_Rate) {
                               ExistItem = "Exist";
                           }
                       }
                       if (ExistItem == "") {

                           TO = new clsProduct();
                           TO.Item_ID = ItemId;
                           TO.Item_Code = mItemCode;
                           TO.Item_Name = ItemName;
                           TO.Qty_In_Case = QtyInCase
                           TO.Scheme = "false";
                           TO.Rate = $("#txtRate").val();
                           TO.Sale_Rate = Salerate;
                           TO.MRP = $("#txtMarketPrice").val();
                           TO.QTY_TO_LESS = QtyToLess;
                           TO.Bill_Date = '1/1/1';
                           ProductCollection.push(TO);

                           //TO = new clsProduct();
                           //TO.Item_ID = $("#ddlProducts option:selected").val();
                           //TO.Item_Code = $("#ddlProducts option:selected").attr("item_code")
                           //TO.Item_Name = $("#ddlProducts option:selected").attr("item_name");
                           //TO.Qty_In_Case = $("#ddlProducts option:selected").attr("Qty_In_Case");
                           //TO.Scheme = "false";
                           //TO.Rate = $("#txtRate").val();
                           //TO.Sale_Rate = $("#ddlProducts option:selected").attr("Sale_Rate");
                           //TO.MRP = $("#ddlProducts option:selected").attr("mrp");
                           //TO.QTY_TO_LESS = $("#ddlProducts option:selected").attr("Qty_To_Less");
                           //TO.Bill_Date = '1/1/1';
                           //ProductCollection.push(TO);

                           BindRows();
                           ResetList();
                           $("#txtServiceIdAdd").val("");
                           $("#txtServiceIdAdd").focus();
                           Stype = "";
                           ItemName = "";
                           mItemCode = "";
                           Rate = "";
                           Salerate = "";
                           QtyInCase = "";
                           QtyToLess = "";
                           mrp = "";
                       }
                       else {
                           alert("Item Already Exists");
                           ResetList();
                           $("#txtServiceIdAdd").val("");
                           $("#txtServiceIdAdd").focus();
                           Stype = "";
                           ItemName = "";
                           mItemCode = "";
                           Rate = "";
                           Salerate = "";
                           QtyInCase = "";
                           QtyToLess = "";
                           mrp = "";
                           return;
                       }
                   }

   );



			   $("#btnAddKitItemsDel").click(
                   function () {
                       var ExistItem = "";
                       for (var i = 0; i < ProductCollectionDel.length; i++) {
                           if (mItemCode == ProductCollectionDel[i].Item_Code && Salerate == ProductCollectionDel[i].Sale_Rate) {
                               ExistItem = "Exist";
                           }
                       }
                       if (ExistItem == "") {

                           TO = new clsProduct();
                           TO.Item_ID = ItemId;
                           TO.Item_Code = mItemCode;
                           TO.Item_Name = ItemName;
                           TO.Qty_In_Case = QtyInCase
                           TO.Scheme = "false";
                           TO.Rate = $("#txtRateDel").val();
                           TO.Sale_Rate = Salerate;
                           TO.MRP = $("#txtMarketPriceDel").val();
                           TO.QTY_TO_LESS = QtyToLess;
                           TO.Bill_Date = '1/1/1';
                           ProductCollectionDel.push(TO);
                           //TO = new clsProduct();
                           //TO.Item_ID = $("#ddlProductsDel option:selected").val();
                           //TO.Item_Code = $("#ddlProductsDel option:selected").attr("item_code")
                           //TO.Item_Name = $("#ddlProductsDel option:selected").attr("item_name");
                           //TO.Qty_In_Case = $("#ddlProductsDel option:selected").attr("Qty_In_Case");
                           //TO.Scheme = "false";
                           //TO.Rate = $("#txtRateDel").val();
                           //TO.Sale_Rate = $("#ddlProductsDel option:selected").attr("Sale_Rate");
                           //TO.MRP = $("#ddlProductsDel option:selected").attr("mrp");
                           //TO.QTY_TO_LESS = $("#ddlProductsDel option:selected").attr("Qty_To_Less");
                           //TO.Bill_Date = '1/1/1';
                           //ProductCollectionDel.push(TO);

                           BindRowsDel();
                           ResetListDel();
                           $("#txtServiceId").val("");
                           $("#txtServiceId").focus();
                           Stype = "";
                           ItemName = "";
                           mItemCode = "";
                           Rate = "";
                           Salerate = "";
                           QtyInCase = "";
                           QtyToLess = "";
                           mrp = "";
                       }
                       else {
                           alert("Item Already Exists");
                           ResetListDel();
                           $("#txtServiceId").val("");
                           $("#txtServiceId").focus();
                           Stype = "";
                           ItemName = "";
                           mItemCode = "";
                           Rate = "";
                           Salerate = "";
                           QtyInCase = "";
                           QtyToLess = "";
                           mrp = "";
                       }

                   }

			   );





               $('#txtStartDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });

               $('#txtEndDate').daterangepicker({
                   singleDatePicker: true,
                   calender_style: "picker_1"
               }, function (start, end, label) {
                   console.log(start.toISOString(), end.toISOString(), label);
               });


              // $("#DKID").html("<select style='width:154px' id='ddlProducts' class='form-control'></select>");

               var Godown = "17";





               $("#ddlProducts").supersearch({
                   Type: "Product",
                   Caption: "Please enter Item Name/Code ",
                   AccountType: "",
                   Godown: Godown,
                   Width: 214,
                   DefaultValue: 0
               });
			   $("#ddlProductsDel").supersearch({
				   Type: "Product",
				   Caption: "Please enter Item Name/Code ",
				   AccountType: "",
				   Godown: Godown,
				   Width: 214,
				   DefaultValue: 0
			   });


           }
           );
   </script>

    


<style type="text/css">

.table > tbody > tr > td
{
    padding:2px;
    }

.table > thead > tr > td
{
padding:2px;    
}
</style>
     <form id="formad" runat="server" onSubmit="return false;">
      <asp:HiddenField ID="hdnDate" runat="server"/>
    <div class="right_col" role="main">
                        <div class="col-md-12 col-sm-12 col-xs-12" id ="dvDialog">
                            <div class="x_panel" style="padding-top:5px">
                             
                              

                                <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    
                                    <div id="addDialog" style="display: none">



                                    </div>
                                <table style="width:100%"   >
                                <tr>
                              
                                <td style="vertical-align:top;padding:0px 2px"><div class="x_panel" style="padding-top:0px;padding-bottom:0px">
                                <div class="x_title">
                                    <h2>Manage Sales(CreditCard and Online)</h2>
                                    
                                    <div class="clearfix"></div>

                                </div>


                                 
                                
                            </div></td>
                                </tr>
                                </table>
                                
                                </div>
                         
                    </div>

                    <div class="x_panel choose-xpnl" style="padding:0px">

                     <div class="x_content chooose-xcnt">
                                    
                                       <table style="width:100%" class="choose-tbl">
 <tr><td  style="width: 150px">
                                     <label class="control-label">Choose Pos:</label>
                                    </td>

                                         <td>      
                                     <select id="ddlPos" style="width: 151px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>


                                    </td></tr>
                                  <tr><td  style="width: 150px">
                                     <label class="control-label">Choose Branch:</label>
                                    </td>

                                         <td>      
                                     <select id="ddlBranch" style="width: 151px;height: 30px;" class="form-control" clientidmode="Static" runat="server">
                                           
                                             </select>


                                    </td>
                                   

                                        
                                    <td  style="width: 100px">
                                     <label class="control-label">Choose Items:</label>
                                    </td>
                                        <td style="width: 200px">      
                                     <select id="ddlItemType" style="width:150px;height: 30px;" class="form-control">
                                            <option value="0">--SELECT--</option>
                                             <option value="1">Bill Items</option>
                                             <option value="2">Order Items</option>
                                             </select>


                                    </td>
                                           <td><button type="button" class="btn btn-success" id="btnItem" style="width:150px;margin-top:5px">Items</button></td>
                                                                                 <td></td>
                                        <td><input type="button" title="Selected Records" class="btn btn-link" id="btnBillCount" style="font-size: x-large;" value="0" /></td>
                                        </tr>
                                       <tr>
                                       <td style="width: 120px"> <label class="control-label">Processing Type:</label></td>
                                    <td style="width: 200px">      
                                     <select id="ddlProType" style="width:150px;height: 30px;" class="form-control">
                                            <option value="0">--SELECT--</option>
                                             <option value="1">Items Wise</option>
                                             <option value="2">Bill Wise</option>
                                             </select>


                                    </td> 
                                          
                                     <td style="width: 100px"><label class="control-label">Choose Type:</label></td>
                                     <td style="width: 200px">      
                                     <select id="ddlBillType" style="width:150px;height: 30px;" class="form-control">
                                             <option value="0">--SELECT--</option>
                                             <option value="1">Retail Bill</option>
                                             <option value="2">Order</option>
                                             </select>


                                    </td>
                                            <td><button type="button" class="btn btn-success" id="btnItemDel" style="width:150px;margin-top:5px">ItemsToDel</button></td>
                                    </tr>
                                    <tr><td style="width:80px"><label class="control-label">From:</label></td>  <td  style="width:130px"> <input type="text" class="form-control"  id="txtStartDate" style="width:150px;height:30px" /></td>
                                       <td style="width:80px"><label class="control-label">To:</label></td>  <td  style="width:150px"> <input type="text" class="form-control"  id="txtEndDate" style="width:150px;height:30px;"  /></td>
                                            <td><button type="button" class="btn btn-success" id="btnShow" style="width:150px;">Show</button></td>
                                        <td></td>
                                        <td>
                                       <div id="btnDelete"  class="btn btn-success" style="display:none"><i class="fa fa-save"></i> Save</div>
                                    
                                  
                                     </td>
                                     </tr>
                                   <tr><td ><label class="control-label">Scan Barcode:</label></td><td colspan="3"><input type="text"  id="txtBarcode" style="width: 450px; height:30px;"  class="form-control customTextBox"  /></td>
                            <%--       <td><button type="button" class="btn btn-success" id="Button1"  style="width:150px">Show</button></td>--%>

                                        <td>TotalSales:<asp:Label  style="font-weight:bold" ID ="Label1" Text="0" runat="server" ClientIDMode ="Static"></asp:Label>

                                            SaleLeft:<asp:Label  style="font-weight:bold" ID ="Label2" Text="0" runat="server" ClientIDMode ="Static"></asp:Label>
                                        </td>
                                   <td></td>
                                   </tr>
                                  

                                   
                                    </table>
                   
                                </div>
                                </div>
                          
<div id="ItemGrid1" title="Press CTRL+ENTER To Select Item" style="display:none;">
						
							<table id="jQGridProduct">
												</table>
												<div id="jQGridProductPager">
												</div>
						</div>

                    <div class="row" id="dvaddItems" style="display:none"> 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="background:seashell;padding:0px">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                 
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px">
<thead>
<tr><th>
 

Item/Code</th><th>Code</th><th>Name</th><th>Rate</th><th>MRP</th></tr>

</thead>
<tbody>
<tr>
<td id="DKID">
<input type='text' autocomplete='off' id="txtServiceIdAdd" class='form-control input-small name'  />
<%--<select style="width:154px" id="ddlProducts" class="form-control"></select>--%>
</td>
<td>
<input type="text"  id="txtCode"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtName" type="text"  class="form-control customTextBox"   style="width:320px"/></td>

<td><input type="text" id="txtRate"  class="form-control customTextBox"   /></td>
<td><input type="text" id="txtMarketPrice" class="form-control customTextBox" /></td>

<td>
<button type="button" class="btn btn-success" id="btnAddKitItems">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  
                                  
                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="max-height:300px;overflow-y:scroll;min-height:300px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th style="display:none">CaseQty</th><th style="display:none">Scheme</th><th>Rate</th><th>SaleRate</th><th>MRP</th><th style="display:none">QTy_To_Less</th></tr>
</thead>
<tbody id="tbKitProducts">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSave" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialog" style="margin-top:5px"  class="btn btn-danger" > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>

<div class="row" id="dvDelItems" style="display:none"> 
                  <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="background:seashell;padding:0px">
                               
                                <div class="x_content" style="padding-bottom:0px">
                                 
                               <table>
                               <tr><td>
                               <table style="border-collapse:separate;border-spacing:2px">
<thead>
<tr><th>
 

Item/Code</th><th>Code</th><th>Name</th><th>Rate</th><th>MRP</th></tr>

</thead>
<tbody>
<tr>
<td id="DKIDDel">
 
 
<input type='text' autocomplete='off' id="txtServiceId" class='form-control input-small name'  />

<%--<select style="width:154px" id="ddlProductsDel" class="form-control"></select>--%>
</td>
<td>
<input type="text"  id="txtCodeDel"  readonly="readonly" class="form-control customTextBox"  />
</td><td><input id="txtNameDel" type="text"  class="form-control customTextBox"   style="width:320px"/></td>

<td><input type="text" id="txtRateDel"  class="form-control customTextBox"   /></td>
<td><input type="text" id="txtMarketPriceDel" class="form-control customTextBox" /></td>

<td>
<button type="button" class="btn btn-success" id="btnAddKitItemsDel">Add</button></td></tr>
</tbody>

</table>
                               </td></tr>
                              
                               
                               </table>  
                                  
                                   
                                </div>
                            </div>

                      
                        </div>
                    

                    

                               <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="max-height:300px;overflow-y:scroll;min-height:300px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th>Code</th><th>Name</th><th style="display:none">CaseQty</th><th style="display:none">Scheme</th><th>Rate</th><th>SaleRate</th><th>MRP</th><th style="display:none">QTy_To_Less</th></tr>
</thead>
<tbody id="tbKitProductsDel">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnSaveDel" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>
                                       <button id="btnCancelDialogDel" style="margin-top:5px"  class="btn btn-danger" > <i class="fa fa-mail-reply-all"></i>Cancel</button>
    
                                     </td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>


                      <div class="row" id="dvShowBills" style="display:none"> 
               
                    

                               <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">

                                    <table class="table table-striped bilno-tbl"  style="font-size:12px;margin-top:-18px;">
                                         <thead>
<tr><th>BillNo</th><th>Amount</th><th>Select</th></tr>
</thead>
<tbody id="tbShowBills">
 
 


 
</tbody>
                                    </table>








                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                     
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                         <div class="row" id="dvShowItems" style="display:none"> 
               
                    

                               <div class="col-md-12">
                            <div class="x_panel" style="max-height:380px;overflow-y:scroll;min-height:380px">
                              <%--  <div class="x_title">
                                    <h2>Kit ITEMS</h2>
                                    
                                    <div class="clearfix"></div>
                                </div>--%>
                                <div class="x_content">




                                                                        <table    class="table table-striped" style="font-size:12px;margin-top:-18px">
                                         <thead>
<tr><th style="width:10px">Select</th><th style="width:50px">Code</th><th style="width:250px">Name</th><th>Amount</th></tr>
</thead>
<tbody id="tbShowItems">
 
 


 
</tbody>
                                    </table>





                                 
                                </div>
                            </div>


                            <div class="x_panel" style="padding:0px">

                     <div class="x_content">
                                    
                                       <table style="width:100%">
                                    <tr><td colspan="100%"><table>
                                    
                                    <tr>
                                    <td></td>
                                    <td>
                                    
                                     <table width="100%">
                                <tr>
                                    <td>
                                     <table >
                                     <tr><td>
                                       <div id="btnDeleteItem" style="margin-top:5px"  class="btn btn-success"><i class="fa fa-save"></i>Save</div>
    
                                     </td>
                                     <td>TotalSales:</td><td><asp:Label  style="font-weight:bold" ID ="lblSalee" runat="server" ClientIDMode ="Static"></asp:Label></td>
                                    <td>SaleLeft:</td><td><asp:Label  style="font-weight:bold" ID ="lblleft" runat="server" ClientIDMode ="Static"></asp:Label></td>
                                     </tr>
                                     </table>
                                     
                                     </td></tr>
                                     </table>
                                    </td>
                                    </tr>
                                    
                                    
                                    </table>
                                    </td>

                                    





                                    </tr>
                                   
                                  

                                   
                                    </table>

                                </div>
                                </div>


                               

                        </div>


                    </div>
                            </div>
                        </div>
  
    </div>
         </form>

</asp:Content>

