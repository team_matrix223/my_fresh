﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class manageaccounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
    }

    [WebMethod]
    public static string Insert(int AccountId, string AccountName, string SSCODE,string SCODE,string HCODE,decimal OpBal,string DRCR,bool IsFlag)
    {

        int status = 0;
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ACCOUNTGROUPS));

        string[] arrRoles = sesRoles.Split(',');


        if (AccountId == 0)
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.SAVE).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }
        else
        {
            var roles = from m in arrRoles
                        where m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                        select m;


            if (roles.Count() == 0)
            {
                status = -11;
            }
        }

        Int32 UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        AccLedger objAcc = new AccLedger()
        {
            AccountId = AccountId,
            CNAME = AccountName,
            SS_CODE = SSCODE,
            S_CODE = SCODE,
            H_CODE = HCODE,
            OP_BAL =OpBal,
            DR_CR= DRCR,
            UserId  =UserNo,
            BranchId = Branch,
            IsFlag = IsFlag,


        };
        status = new AccountsBLL().InsertUpdate(objAcc);
        var JsonData = new
        {
            Account = objAcc,
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.ACCOUNTGROUPS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.SAVE).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
        }

    }


    [WebMethod]
    public static string BindAccGroups(string BALINC)
    {

       var AccountGroups =  new AccGroupsBLL().GetAccGroupsByBALINC(BALINC);


        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;

        var JsonData = new
        {
            AccGroupOptions = AccountGroups

        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Getbyscode(string scode)
    {
        AccGroups objaccgrp = new AccGroups();
       
        objaccgrp.S_CODE = scode;
        new AccGroupsBLL().GetByScode(objaccgrp);
        var JsonData = new
        {

            acchead = objaccgrp,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }


}