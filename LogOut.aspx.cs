﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LogOut : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UserActivityLog.SetActivityLog("IN", "LogOut.aspx");
        Response.Cookies[Constants.BranchId].Value = null;
        Response.Redirect("Login.aspx");
    }
}