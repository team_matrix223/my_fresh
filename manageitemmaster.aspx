﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true"
    CodeFile="manageitemmaster.aspx.cs" Inherits="manageitemmaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" runat="Server">
    
    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
    <script src="js/grid.locale-en.js" type="text/javascript"></script>
    <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-ui.js"></script>
    <link href="css/tabcontent.css" rel="stylesheet" type="text/css" />
    <script src="js/customValidation.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.uilock.js"></script>
     <link href="semantic.css" rel="stylesheet" type="text/css" />
    <link href="css/customcss/manageitemmaster.css" rel="stylesheet" />
    <script type="text/javascript" src="js/SearchPlugin.js"></script>
     
      
    <script type="text/javascript" src="//cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
     <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" />
    <script language="javascript" type="text/javascript">

        function ApplyRoles(Roles) {


            $("#<%=hdnRoles.ClientID%>").val(Roles);
        }


                $(document).on("click", "#btnDel", function (event) {

                    var RowIndex = Number($(this).closest('tr').index());


                    NutritionFactCollection.splice(RowIndex, 1);
                    BindRows();


                });




                var m_ItemId = 0;

                $(document).keypress(function (event) {
                    var keycode = (event.keyCode ? event.keyCode : event.which);
                    if (keycode == '13') {
                        event.preventDefault();
                        var $this = $(event.target);
                        var index = parseFloat($this.attr('data-index'));
                        $('[data-index="' + (index + 1).toString() + '"]').focus();


                        $("#ddlIMDepartment").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMGroup").focus();
                            }
                        });


                        $("#ddlIMGroup").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMSubGroup").focus();
                            }
                        });

                        $("#ddlIMSubGroup").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMCompany").focus();
                            }
                        });

                        $("#ddlIMCompany").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMCategory").focus();
                            }
                        });

                        $("#ddlIMCategory").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMTax").focus();
                            }
                        });


                        $("#ddlIMTax").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMItemType").focus();
                            }
                        });

                        $("#ddlIMItemType").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMExicise").focus();
                            }
                        });

                        $("#ddlIMExicise").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMPurRate").focus();
                            }
                        });

                        $("#ddlIMRackShelf").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMDeiveryNoteRate").focus();
                            }
                        });

                        $("#ddlIMSaleUnit").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMWgtLtr").focus();
                            }
                        });

                        $("#ddlIMWgtLtr").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#ddlIMTransactionType").focus();
                            }
                        });

                        $("#ddlIMCurQtyRate").keypress(function (e) {

                            var k = e.which;
                            if (k == 13) {
                                $("#txtIMDealerMargin").focus();
                            }
                        });

                        $("#txtIMComments").keypress(function (e) {

                            var k = e.which;

                            if (k == 13) {
                                $("#btnIMAdd").click();
                            }
                        });
                        

                    }
                });



                $(document).ready(
    function () {



        $(window).keydown(function (e) {
            $.uiLock();

            switch (e.keyCode) {
                case 13:   // F1 key is left or up
                    $("#globalSearch").click();

            }
            $.uiUnlock();
            return; //using "return" other attached events will execute
        });

                        $(window).keydown(function (event) {
							if (event.altKey || event.metaKey) {
								switch (String.fromCharCode(event.which).toLowerCase()) {
                                    case 's':
                                        event.preventDefault();
										$("#btnIMAdd").click();
                                        break;

									case 'c':
										event.preventDefault();
                                        $("#btnIMCancel").click();
										break;
                                       
									
								}
							}
						});




        $('#formID').on('keydown', 'input', function (event) {

            if (event.which == 13) {

                event.preventDefault();

                var $this = $(event.target);

                var index = parseFloat($this.attr('data-index'));


                $('[data-index="' + (index + 1).toString() + '"]').focus();
            }
        });
        BindGrid();
        function BindItemLIst() {
            var company = $("#ddcompany").val();
            var group = $("#ddgroup").val();
            var deptt = $("#dddpt").val();
            var pos = $("#ddpos").val();
            var tax = $("#ddtax").val();
            $.ajax({
                type: 'POST',
                url: "manageitemmaster.aspx/bind_itemlst",
                data: '{"pos": "' + pos + '","deptt": "' + deptt + '","group": "' + group + '","tax": "' + tax + '","company": "' + company + '"}',
                contentType: "application/json",
                dataType: 'json',
                success: function (data) {
                    $("#tbltemlist").show();
                    var obj = jQuery.parseJSON(data.d);
                    var table = $("#tbltemlist").DataTable();


                    //clear datatable
                    table.clear().draw();

                    //destroy datatable
                    table.destroy();
                    $("#tbl_bodydtemlist").html(obj.tableoption);
                    //$("#tbltemlist").DataTable();
                    $('#tbltemlist').DataTable({
                        "paging": false,
                       
                    });


                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                }
          ,
                complete: function () {

                    $.unblockUI();


                }

            });
        }
            $("#btn_getitem").click(function(){
                BindItemLIst();
        
        
            });


        $("#btnDiscontinue").click(
      function () {

          BindGridDiscontinuedItems();
      });


        $("#btnRefresh").click(
     function () {

         BindGrid();
     });

        $("#Itemlist ").click(function () {
        $("#dvItemlst").dialog({
            autoOpen: true,

            width: 500,
            resizable: false,
            modal: false
        });
    });
        $("#btnclose ").click(function () {
        $("#dvItemlst").dialog('close');
        });

        ValidateRoles();

        function ValidateRoles() {

            var arrRole = [];
            arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');

            for (var i = 0; i < arrRole.length; i++) {
                if (arrRole[i] == "1") {

                    $("#btnAdd").show();
                    $("#btnAdd").click(
       function () {

           var Id = 0;
           $.ajax({
               type: "POST",
               data: '{"Id":"' + Id + '"}',
               url: "manageitemmaster.aspx/LoadUserControl",
               contentType: "application/json",
               dataType: "json",
               success: function (msg) {

                   //$("#dvIMDialog").remove();
                   //$("body").append("<div id='dvIMDialog'/>");
                   $("#dvIMDialog").html(msg.d).dialog({modal: true, width: 1115, height: 630, closeOnEscape:false });
               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {

               }
           });
       }

       );

                }
                else if (arrRole[i] == "3") {

                    $("#btnEdit").show();
                    $("#btnEdit").click(
        function () {

            $.uiLock('');

            var Id = m_ItemId;
            $.ajax({
                type: "POST",
                data: '{"Id":"' + Id + '"}',
                url: "manageitemmaster.aspx/LoadUserControl",
          
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    //$("#dvIMDialog").remove();
                    //$("body").append("<div id='dvIMDialog'/>");
                    $("#dvIMDialog").html(msg.d).dialog({ modal: true, width: 1115, height: 630, });
                    setTimeout(function () {
                        $.ajax({
                            type: "POST",
                            data: '{"Id":"' + Id + '"}',
                            url: "manageitemmaster.aspx/chkitemname",
                            contentType: "application/json",
                       
                            dataType: "json",
                            success: function (msg) {

                                var obj = jQuery.parseJSON(msg.d);
                                if (obj == -1) {
                                    $("#txtIMItemName").attr('disabled', 'disabled');
                                    
                                }
                
                                else {
                                  

                                }
                            }
                        },100)
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                   // BindGrid();
                    $.uiUnlock();
                }
            });

        });



                    $("#btnClone").show();
                    $("#btnClone").click(
        function () {

            $.uiLock('');

            var Id = m_ItemId;
            $.ajax({
                type: "POST",
                data: '{"Id":"' + Id + '"}',
                url: "manageitemmaster.aspx/LoadUserControl",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    //$("#dvIMDialog").remove();
                    //$("body").append("<div id='dvIMDialog'/>");
                    $("#dvIMDialog").html(msg.d).dialog({ modal: true, width: 1115, height: 630, });
           
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $("#hdnIMID").val(0);
                  
                
                    BindGrid();
                    setTimeout(function () {
                        $("#txtIMItemCode").val("");
                        $("#txtIMItemName").val("");
                        $("#txtIMShortName1").val("");
                        $("#txtIMShortName2").val("");
                        $("#txtIMBarCode").val("");

                    }, 100);
                    $.uiUnlock();
                }
            });


        });

                }
                else if (arrRole[i] == "2") {


                    $("#btnDelete").show();
                    $("#btnDelete").click(
    function () {

        var SelectedRow = jQuery('#jQGridDemo').jqGrid('getGridParam', 'selrow');
        if ($.trim(SelectedRow) == "") {
            alert("No Item is selected to Delete");
            return;
        }

        var ItemId = $('#jQGridDemo').jqGrid('getCell', SelectedRow, 'ItemID')
        if (confirm("Are You sure to delete this record")) {
            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{"ItemID":"' + ItemId + '"}',
                url: "manageitemmaster.aspx/Delete",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

                    if (obj.status == -1) {
                        alert("Deletion Failed. Item is in Use.");
                        return
                    }
                    BindGrid();
                    alert("Item is Deleted successfully.");
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {
                    $.uiUnlock();
                }
            });

        }


    });




                }
            }
        }
        $("#btnExport").click(function () {

            fnExcelReport();


        })

        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('tbltemlist'); // id of table

            for (j = 0 ; j < tab.rows.length ; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }

    });


    </script>
    <script>
        $(document).ready(function(){
            $("#Itemlist").click(function () {
                $(".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable").addClass("itm-list-cls");
            });
            $("#btnclose, #btnAdd, #btnEdit, #btnClone").click(function () {
                $(".ui-dialog.ui-widget.ui-widget-content.ui-corner-all.ui-front.ui-draggable").removeClass("itm-list-cls");
            });
        });
    </script>
    <style type="text/css">
        .table
        {
            margin: 5px;
        }
        .ui-jqgrid tr.ui-row-ltr td {
            text-align: center;
            height: 25px;
        }
		button.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-icon-only.ui-dialog-titlebar-close {
			display: none;
		}
    </style>
 
    
   <form   runat="server" id="formID" method="post">
       <iframe id="txtArea1" style="display:none"></iframe>
   <asp:HiddenField ID="hdnRoles" runat="server"/>
      <asp:HiddenField ID="hdnDate" runat="server"/>
   <div class="right_col manageitemmaste_right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3>Manage Items</h3>
                        </div>
                       
                    

                    <div class="x_panel">
                         <div class="form-group">
                                
                              
                <div class="youhave manageitemmaster_youhave" >
                     <table cellspacing="0" cellpadding="0" class="youhave-btn-table">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnDiscontinue"  class="btn btn-primary" ><i class="fa fa-external-link"></i> Disconitued Items</div></td>
                                        <td style="padding-top:5px"> <div id="btnRefresh"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Refresh</div></td>
                                               <td style="padding-top:5px"> 
                                        <asp:LinkButton ID="btn_exp_excl" OnClick="btn_exp_excl_pl_Click" class="btn btn-primary btn-small" runat="server">     Product List &nbsp; &nbsp;<i class="fa fa-download" style="font-size: large;"></i></asp:LinkButton>
                               </td>
                                                 <td style="padding-top:5px"> 
                                        <asp:LinkButton ID="btn_exp_excl_csv" OnClick="btn_exp_excl_csv_Click" class="btn btn-primary btn-small" runat="server">     Product List CSV &nbsp; &nbsp;<i class="fa fa-download" style="font-size: large;"></i></asp:LinkButton>
                               </td>
                                                  <td style="padding-top:5px"> 
                                                    <button id="Itemlist" type="button" class="btn btn-danger">ItemList</button>
                                                </td>
                                            </tr>
                                            </table>



      	          <table id="jQGridDemo">
    </table>
       <table cellspacing="0" cellpadding="0">
                                            <tr>
                                        <td style="padding-top:5px"> <div id="btnAdd" style="display:none;" class="btn btn-primary" ><i class="fa fa-external-link"></i> Add</div></td>
                                        <td style="padding-top:5px"> <div id="btnEdit" style="display:none;"  class="btn btn-success" ><i class="fa fa-edit m-right-xs"></i> Edit</div></td>
                                        <td style="padding-top:5px"> <div id="btnDelete" style="display:none;" class="btn btn-danger" ><i class="fa fa-trash m-right-xs"></i> Delete</div></td>
                                                   <td style="padding-top:5px"> <div id="btnClone" style="display:none;" class="btn btn-danger" ><i class="fa fa-clone m-right-xs"></i> ItemClone</div></td>
                                            </tr>
                                            </table>
    <div id="jQGridDemoPager">
    </div>
                </div>


                        
                    </div>


                    </div>

                     
                </div>
                <!-- /page content -->

               

            </div>

       </div>


 <div id='dvIMDialog' title="<%= Request.Cookies[Constants.BranchName].Value %>"> </div>

       <div id='dvItemlst' style="display:none">

           <div class="col-md-12 itm-list-first-sec">
               
              <div class="col-md-2 col-sm-2 col-xs-12 itm-list-company">
                  <label>Company:</label><asp:DropDownList ID="ddcompany"  runat="server" ClientIDMode="Static"><asp:ListItem Value="0">tests</asp:ListItem></asp:DropDownList>
              </div>
                <div class="col-md-2 col-sm-2 col-xs-12 itm-list-group">
                  <label>Group:</label><asp:DropDownList ID="ddgroup" runat="server" ClientIDMode="Static"><asp:ListItem Value="0">tests</asp:ListItem></asp:DropDownList>
              </div>
                <div class="col-md-2 col-sm-2  col-xs-12 itm-list-dep">
                  <label>Dept.:</label><asp:DropDownList ID="dddpt"  runat="server" ClientIDMode="Static"><asp:ListItem Value="0">tests</asp:ListItem></asp:DropDownList>
              </div>
                <div class="col-md-2 col-sm-2 col-xs-12 itm-list-pos">
                  <label>POS:</label><asp:DropDownList ID="ddpos"  runat="server" ClientIDMode="Static"></asp:DropDownList>
              </div>

               <div class="col-md-1 col-sm-1 col-xs-12 itm-list-tax">
                  <label>Tax:</label><asp:DropDownList ID="ddtax" runat="server" ClientIDMode="Static"><asp:ListItem Value="0">tests</asp:ListItem></asp:DropDownList>
              </div>

                <div class="col-md-3 col-sm-3 col-xs-12 itm-list-sub-btn">
                     <button type="button" id="btn_getitem" class="btn btn-primary">Submit</button>
                     <button id="btnExport" type="button" class="btn btn-success"> EXPORT </button>
                     <button id="btnclose" type="button" class="btn btn-danger"> Close </button>
                </div>

              
           <div>
               <table id="tbltemlist" style="display:none">
                    <thead>
                                      <th>Item Code</th>
                                      <th>Item Name</th>
                                        <th>BarCode</th>
                         <th>Purchase Rate</th>
                         <th>Sale Rate</th>
                           <th>Tax</th>
                                       <th>HSN</th>
                        <th>Group</th>
                        <th>Deptt.</th>
                            
                                  </thead>
                   <tbody id="tbl_bodydtemlist">
                       
                   </tbody>
               </table>
           </div>

       </div>
</form>


            <script type="text/javascript">
                function BindGrid() {
                    jQuery("#jQGridDemo").GridUnload();
                    jQuery("#jQGridDemo").jqGrid({
                        url: 'handlers/ManageProducts.ashx',
                        ajaxGridOptions: { contentType: "application/json" },
                        datatype: "json",

                        colNames: ['ItemId','MasterCode', 'ItemCode', 'ItemName', 'Barcode',   'SalesInUnit', 'PurchaseRate', 'SaleRate', 'MRP'],
                           
                        colModel: [
                                    { name: 'ItemID', key: true, index: 'ItemID', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                    { name: 'Master_Code', index: 'Master_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Code', index: 'Item_Code', width: 200, stype: 'text', sortable: true, editable: true,hidden:false, editrules: { required: true } },
                                    { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                    { name: 'Bar_Code', index: 'Bar_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    
                                    
                                    { name: 'Unit', index: 'Unit', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                   
                                     { name: 'Purchase_Rate', index: 'Purchase_Rate', width: 200, stype: 'text', formatter: 'number', formatoptions: { decimalPlaces: 2 }, sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                    { name: 'Sale_Rate', index: 'Sale_Rate', width: 200, stype: 'text', sortable: true, editable: true, formatter: 'number', formatoptions: { decimalPlaces: 2 }, hidden: false, editrules: { required: true } },
                                    { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 200, stype: 'text', sortable: true, formatter: 'number', formatoptions: { decimalPlaces: 2 }, editable: true, hidden: false, editrules: { required: true } },

                                
                        ],
                        rowNum: 10,

                        mtype: 'GET',
                        loadonce: true,
                        rowList: [10, 20, 30,'All'],
                        pager: '#jQGridDemoPager',
                        sortname: 'ItemID',
                        viewrecords: true,
                        height: "100%",
                        width: "400px",
                        sortorder: 'asc',
                        caption: "Items List",
                        rowList: [10, 20, 30, 100000000],
                        loadComplete: function () {
                            $("option[value=100000000]").text('All');
                        },
                        editurl: 'handlers/ManageProducts.ashx',
                         ignoreCase: true,
                         toolbar: [true, "top"],


                    });


  var   $grid = $("#jQGridDemo");
            // fill top toolbar
            $('#t_' + $.jgrid.jqID($grid[0].id))
                .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
            $("#globalSearchText").keypress(function (e) {
                var key = e.charCode || e.keyCode || 0;
                if (key === $.ui.keyCode.ENTER) { // 13
                    $("#globalSearch").click();
                }
            });
            $("#globalSearch").button({
                icons: { primary: "ui-icon-search" },
                text: false
            }).click(function () {
                var postData = $grid.jqGrid("getGridParam", "postData"),
                    colModel = $grid.jqGrid("getGridParam", "colModel"),
                    rules = [],
                    searchText = $("#globalSearchText").val(),
                    l = colModel.length,
                    i,
                    cm;
                for (i = 0; i < l; i++) {
                    cm = colModel[i];
                    if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                        rules.push({
                            field: cm.name,
                            op: "cn",
                            data: searchText
                        });
                    }
                }
                postData.filters = JSON.stringify({
                    groupOp: "OR",
                    rules: rules
                });
                $grid.jqGrid("setGridParam", { search: true });
                $grid.trigger("reloadGrid", [{ page: 1, current: true}]);
                return false;
            });








                    $("#jQGridDemo").jqGrid('setGridParam',
            {

                onSelectRow: function (rowid, iRow, iCol, e) {

                    m_ItemId = 0;
              
                    var arrRole = [];
                    arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                   $("#btnUpdate").css({ "display": "none" });
                   $("#btnReset").css({ "display": "none" });
                   $("#btnAdd").css({ "display": "none" });
                  

                   for (var i = 0; i < arrRole.length; i++) {

                       if (arrRole[i] == 1) {

                           $("#btnAdd").css({ "display": "block" });
                       }
                       if (arrRole[i] == 2) {

                           $("#btnDelete").css({ "display": "block" });
                       }
                       if (arrRole[i] == 3) {
                           $("#btnEdit").css({ "display": "block" });


                           m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');

                       }

                   }

                  
                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>



        <script type="text/javascript">
            function BindGridDiscontinuedItems() {
                jQuery("#jQGridDemo").GridUnload();
                jQuery("#jQGridDemo").jqGrid({
                    url: 'handlers/ManageDiscontinuedProducts.ashx',
                    ajaxGridOptions: { contentType: "application/json" },
                    datatype: "json",

                    colNames: ['ItemId', 'MasterCode', 'ItemCode', 'ItemName', 'Barcode', 'SalesInUnit', 'PurchaseRate', 'SaleRate', 'MRP'],

                    colModel: [
                                { name: 'ItemID', key: true, index: 'ItemID', width: 100, stype: 'text', sorttype: 'int', hidden: false },
                                { name: 'Master_Code', index: 'Master_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Item_Code', index: 'Item_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Item_Name', index: 'Item_Name', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                { name: 'Bar_Code', index: 'Bar_Code', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },


                                { name: 'Unit', index: 'Unit', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },

                                 { name: 'Purchase_Rate', index: 'Purchase_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Sale_Rate', index: 'Sale_Rate', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },
                                { name: 'Max_Retail_Price', index: 'Max_Retail_Price', width: 200, stype: 'text', sortable: true, editable: true, hidden: false, editrules: { required: true } },


                    ],
                    rowNum: 10,

                    mtype: 'GET',
                    loadonce: true,
                    rowList: [10, 20, 30],
                    pager: '#jQGridDemoPager',
                    sortname: 'ItemID',
                    viewrecords: true,
                    height: "100%",
                    width: "400px",
                    sortorder: 'asc',
                    caption: "Items List",

                    editurl: 'handlers/ManageProducts.ashx',
                    ignoreCase: true,
                    toolbar: [true, "top"],


                });


                var $grid = $("#jQGridDemo");
                // fill top toolbar
                $('#t_' + $.jgrid.jqID($grid[0].id))
                    .append($("<div><label for=\"globalSearchText\">Global search for:&nbsp;</label><input id=\"globalSearchText\" type=\"text\"></input>&nbsp;<button id=\"globalSearch\" type=\"button\">Search</button></div>"));
                $("#globalSearchText").keypress(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    if (key === $.ui.keyCode.ENTER) { // 13
                        $("#globalSearch").click();
                    }
                });
                $("#globalSearch").button({
                    icons: { primary: "ui-icon-search" },
                    text: false
                }).click(function () {
                    var postData = $grid.jqGrid("getGridParam", "postData"),
                        colModel = $grid.jqGrid("getGridParam", "colModel"),
                        rules = [],
                        searchText = $("#globalSearchText").val(),
                        l = colModel.length,
                        i,
                        cm;
                    for (i = 0; i < l; i++) {
                        cm = colModel[i];
                        if (cm.search !== false && (cm.stype === undefined || cm.stype === "text")) {
                            rules.push({
                                field: cm.name,
                                op: "cn",
                                data: searchText
                            });
                        }
                    }
                    postData.filters = JSON.stringify({
                        groupOp: "OR",
                        rules: rules
                    });
                    $grid.jqGrid("setGridParam", { search: true });
                    $grid.trigger("reloadGrid", [{ page: 1, current: true }]);
                    return false;
                });








                $("#jQGridDemo").jqGrid('setGridParam',
        {

            onSelectRow: function (rowid, iRow, iCol, e) {

                m_ItemId = 0;

                var arrRole = [];
                arrRole = $("#<%=hdnRoles.ClientID%>").val().split(',');


                    $("#btnUpdate").css({ "display": "none" });
                    $("#btnReset").css({ "display": "none" });
                    $("#btnAdd").css({ "display": "none" });


                    for (var i = 0; i < arrRole.length; i++) {

                        if (arrRole[i] == 1) {

                            $("#btnAdd").css({ "display": "block" });
                        }
                        if (arrRole[i] == 2) {

                            $("#btnDelete").css({ "display": "block" });
                        }
                        if (arrRole[i] == 3) {
                            $("#btnEdit").css({ "display": "block" });


                            m_ItemId = $('#jQGridDemo').jqGrid('getCell', rowid, 'ItemID');

                        }

                    }


                }
            });

            var DataGrid = jQuery('#jQGridDemo');
            DataGrid.jqGrid('setGridWidth', '700');

            $('#jQGridDemo').jqGrid('navGrid', '#jQGridDemoPager',
                             {
                                 refresh: false,
                                 edit: false,
                                 add: false,
                                 del: false,
                                 search: false,
                                 searchtext: "Search",
                                 addtext: "Add",
                             },

                             {//SEARCH
                                 closeOnEscape: true

                             }

                               );



        }





    </script>

 
</asp:Content>
