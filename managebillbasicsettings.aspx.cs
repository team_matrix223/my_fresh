﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;


public partial class ApplicationSettings_managebillbasicsettings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindBanks();
            BindBranches();
        }
        CheckRole();
    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.BILLSETTINGS));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.APPLYSETTINGS).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("../index.aspx");

        }


    }


    void BindBranches()
    {

        ddlBranch.DataSource = new BranchBLL().GetAll();
        ddlBranch.DataValueField = "BranchId";
        ddlBranch.DataTextField = "BranchName";
        ddlBranch.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Branch--";
        li1.Value = "0";
        ddlBranch.Items.Insert(0, li1);

    }


    void BindBanks()
    {

        ddlBank.DataSource = new BankBLL().GetAll();
        ddlBank.DataValueField = "Bank_ID";
        ddlBank.DataTextField = "Bank_Name";
        ddlBank.DataBind();
        ListItem li1 = new ListItem();
        li1.Text = "--Choose Account To Debit--";
        li1.Value = "0";
        ddlBank.Items.Insert(0, li1);

    }



    [WebMethod]
    public static string FillSettings(int Type)
    {
        BasicBillSettings ObjSettings = new BasicBillSettings();
     

        ObjSettings.BranchId = Type;
        new BasicBillSettingBLL().GetSettings(ObjSettings);
        var JsonData = new
        {

            setttingData = ObjSettings,

        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }



    [WebMethod]
    public static string Insert(BasicBillSettings objSettings)
    {
        var Id = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
      
        objSettings.UserId = Id;
       
        int status = new BasicBillSettingBLL().UpdateBasicSettings(objSettings);
        var JsonData = new
        {
            Status = status
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        return ser.Serialize(JsonData);
    }

   [WebMethod]
    public static string bindprintcopies()
    {

        string prodData = new BasicBillSettingBLL().getbillcopies();
        var JsonData = new
        {
            productData = prodData
        };
        JavaScriptSerializer ser = new JavaScriptSerializer();
        ser.MaxJsonLength = int.MaxValue;
        return ser.Serialize(JsonData);

    }


    [WebMethod]
    public static  void insertnoofcopies(string paymode,int nocopies)
    {
        Int32 Branchid = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        //Connection sqlcon = new Connection();
        Connection connn = new Connection();
        SqlConnection con = new SqlConnection(connn.sqlDataString);
      //  SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DB_CONN"].ConnectionString);
        con.Open();
        SqlCommand cmd = new SqlCommand("UpdateNoCopies", con);
        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        cmd.Parameters.AddWithValue("@paymode",paymode);
        cmd.Parameters.AddWithValue("@NoOfCopies", nocopies);
        cmd.Parameters.AddWithValue("@Branchid",Branchid);
        cmd.ExecuteNonQuery();
        con.Close();

    }

}