﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class manageInternalIssue : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CheckRole();
        if (!IsPostBack)
        {
            BindDepartment();
            hdntodaydate.Value = DateTime.Now.ToShortDateString();

            BindDropDownList();
        }

    }

    public void BindDropDownList()
    {
        Int32 Branch = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        new ProductBLL().GetTransferOptions(hdnProducts, Branch);
    }
    public void BindDepartment()
    {
        ddlDept.DataSource = new DepartmentBLL().GetAll();
        ddlDept.DataTextField = "PROP_NAME";
        ddlDept.DataValueField = "PROP_ID";
        ddlDept.DataBind();


    }


    public void CheckRole()
    {
        string sesRoles = new RolesBLL().GetRolesByDesignationAndPage(Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.DesignationId].Value), Convert.ToInt16(Enums.Pages.INTERNALISSUE));

        string[] arrRoles = sesRoles.Split(',');

        var roles = from m in arrRoles
                    where m == Convert.ToInt16(Enums.Roles.NEW).ToString() | m == Convert.ToInt16(Enums.Roles.EDIT).ToString() | m == Convert.ToInt16(Enums.Roles.DELETE).ToString()
                    select m;

        int len = roles.Count();
        if (len == 0)
        {
            Response.Redirect("index.aspx");

        }
        else
        {
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "role", "ApplyRoles('" + sesRoles + "');", true);
            hdnRoles.Value = sesRoles;
        }

    }

    [WebMethod]
    public static string InsertUpdate(InternalIssue[] objDailyTransfer, int RefNo, int GodownId, DateTime Date,int Department,String Remarks)
    {




        JavaScriptSerializer ser = new JavaScriptSerializer();

        DataTable dt = new DataTable();

        dt.Columns.Add("Item_Code");
        dt.Columns.Add("Item_Name");
        dt.Columns.Add("Qty");
        dt.Columns.Add("Rate");
        dt.Columns.Add("Amount");
        dt.Columns.Add("Stock");
        DataRow dr;



        foreach (var item in objDailyTransfer)
        {
            dr = dt.NewRow();

            dr["Item_Code"] = item.Item_Code;
            dr["Item_Name"] = item.ITEM_NAME;
            dr["Qty"] = item.QTYISSUED;
            dr["Rate"] = item.RATE;
            dr["Amount"] = item.AMOUNT;
            dr["Stock"] = 0;
            dt.Rows.Add(dr);


        }

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        int UserNo = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.AdminId].Value);
        int Status = 0;
        Status = new InternalIssueBLL().InsertUpdate(Date, GodownId, BranchId, RefNo, dt, UserNo,Department,Remarks);
        var JsonData = new
        {

            status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string Delete(Int32 RefNo)
    {

        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);
        JavaScriptSerializer ser = new JavaScriptSerializer();
        int Status = 0;
        Status = new InternalIssueBLL().Delete(RefNo, BranchId);
        var JsonData = new
        {

            Status = Status
        };
        return ser.Serialize(JsonData);
    }

    [WebMethod]
    public static string GetById(Int32 RefNo)
    {
        JavaScriptSerializer ser = new JavaScriptSerializer();


        int BranchId = Convert.ToInt32(HttpContext.Current.Request.Cookies[Constants.BranchId].Value);

        List<InternalIssue> TransferDetail = new InternalIssueBLL().GetByRefNo(RefNo, BranchId);
        var JsonData = new
        {

            TransferDetail = TransferDetail
        };
        return ser.Serialize(JsonData);


    }

}