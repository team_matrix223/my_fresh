﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="manageaccountsettings.aspx.cs" Inherits="ApplicationSettings_manageaccountsettings" %>
<%@ Register src="~/usercontrols/Footer.ascx" TagName="ucfooter" TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cntAdmin" Runat="Server">
	<link href="css/customcss/manageaccountsettings.css" rel="stylesheet" />
     <style type="text/css">
        .tableheadings
        {
            text-align:left;background-color:#1479B8;color:white;padding:4px;font-weight: bold
        }
        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td
        {
           padding:8px;
        }
        .bill_series td {text-align: left;}
        .bill_series input {width: 100%;}
        .bill_series select {width: 100%;}
        .bill_series [type=checkbox] {width: auto; margin: 0px; height: auto;}
        .bill_series [type=radio] {width: auto; margin: 0px;}
        .bill_series input#chkBSOpenClose {width: auto; margin: 0px;}
        .bill_series {border: 1px solid #ddd;}
        .bill_series tr td:last-child {border-right: 1px solid #ddd;}
        
        .page-title .title_left
        {
            background-color:#1479B8;color:white;width: 100% !important;
        }
        .page-title .title_left h3 {
            padding-left: 15px;
            width: 100% !important;
        }
        select#ddlBranch {
            border: 1px solid #DDE2E8;
            height: 25px;
        }
        table.table.bill_series input {
            border: 1px solid #ddd;
        }
        .right_col {
            min-height: unset;
        }
        /*body
        {
            background: lavender;
        }*/
        .manage-product-setting-xpanel {
            min-height: 532px;
            max-height: 532px;
            overflow: auto;
        }
        label.headings {
            font-weight: normal;
        }
        @media (min-width:992px) and (max-width:1200px)
        {
            .manage-product-setting-xpanel {
	            min-height: 452px;
	            max-height: 452px;
	            overflow: auto;
            }
        }
    </style>
    


    <form   runat="server" id="formID" method="post">
   <asp:HiddenField ID="hdnRoles" runat="server"/>

   <div class="right_col" role="main">
                <div class="">

                    <div class="page-title">
                        <div class="title_left">
                            <h3 >Accounts</h3>
                        </div>
                     <!--   <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                               <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                            <button class="btn btn-default" type="button">Go!</button>
                        </span>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="clearfix"></div>
     
                    <div class="x_panel manage-product-setting-xpanel">
                       
                        <div class="x_content">

                             <table cellpadding="0" cellspacing="0" border="0" id="frmCity" class="table-condensed"  >
                             <tr>
                                 <td>
                                     <table cellpadding="10" cellspacing="5" border="0"  class="table bill_series">

  <tr>
                                              <td class="tableheadings" align="right" style="text-align:left;width:20px" colspan="100%">Branch:</td>
                                              </tr>
                                         <tr>
                                              <td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlBranch" ClientIDMode="Static" runat="server" style="width:200px" >
                                  
                                    </asp:DropDownList></td></tr>


                                           <tr><td colspan ="100%"  class="tableheadings" >Account Linking</td></tr>
                         <tr>
							 <td class="headings" align="left" style="text-align:left;width:300px">CreditNote Account To Debit:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px" ><asp:DropDownList id="ddlcreditnote" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                        
							 <td class="headings" align="left" style="text-align:left;width:300px">DeliveryNote Debit Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px" ><asp:DropDownList id="ddldebitnote" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
                         </tr>  
                         <tr>
							 <td class="headings" align="left" style="text-align:left;width:300px">Order Sale Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlOrderSale" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                         
							 <td class="headings" align="left" style="text-align:left;width:300px">Order Advance Sale Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlOrderAdv" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						 </tr>                                         
                         <tr>
							 <td class="headings" align="left" style="text-align:left;width:300px">Cash In Hand Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlcashinhand" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                      
							 <td class="headings" align="left" style="text-align:left;width:300px">Display Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddldisplay" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
					     </tr>
                         <tr>
							 <td class="headings" align="left" style="text-align:left;width:300px">Adjustment Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddladjustment" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                                        
							 <td class="headings" align="left" style="text-align:left;width:300px">Discount Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddldiscount" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						 </tr>                                                                      
                         <tr><td class="headings" align="left" style="text-align:left;width:300px">TCS Account Debit To:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddltcsaccount" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                          
							 <td class="headings" align="left" style="text-align:left;width:300px">Bill Round Account:</td>
							 <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlbillround" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>                                                                        
                       <tr>
						   <td class="headings" align="left" style="text-align:left;width:300px">Service Tax:</td>
						   <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlservicetax" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
                           <td class="headings" align="left" style="text-align:left;width:300px">Service Charge:</td>
						   <td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlservicecharge" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Income Tax:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlincometax" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
							<td class="headings" align="left" style="text-align:left;width:300px">Labour Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddllabour" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>                                                                      
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">GST Deduction:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlgstded" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                                     
							<td class="headings" align="left" style="text-align:left;width:300px">Other Deduction:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlotherded" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>                                                                                
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Security:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlsecurity" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                                                
							<td class="headings" align="left" style="text-align:left;width:300px">Reserve:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlreserve" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>                                                                                
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Job Work:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddljobwrk" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
							<td class="headings" align="left" style="text-align:left;width:300px">RAR Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px" ><asp:DropDownList id="ddlrar" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr> 
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Sale Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlsale" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                                                   
							<td class="headings" align="left" style="text-align:left;width:300px">Purchase Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlpurchase" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
                        </tr>                                                                                               
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Journal Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddljournal" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
							<td class="headings" align="left" style="text-align:left;width:300px">Sale Account MSE:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlmse" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
                        </tr>                                                                                                          
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">Outstation Purchase Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddloutpurchase" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>                                                                                                                      
							<td class="headings" align="left" style="text-align:left;width:300px">Outstation Tax Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlouttax" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
						</tr>                                                                                                                      
                        <tr>
							<td class="headings" align="left" style="text-align:left;width:300px">CST Sale Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px"><asp:DropDownList id="ddlcstsale" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
							<td class="headings" align="left" style="text-align:left;width:300px">CST SaleTax Account:</td>
							<td class="headings" align="left" style="text-align:left;width:300px" colspan="100%"><asp:DropDownList id="ddlcstsaletax" ClientIDMode="Static" runat="server" style="width:200px" ></asp:DropDownList></td>
				         </tr>
                                 <tr>        <td class="headings">Banks:</td>
                      
                      <td>
                      <div class="no_splier_table">
                      <table style="width:100%" id="tblist">
                      <asp:Literal ID="ltBanks" runat="server"></asp:Literal>
                      
                      </table>
                      </div>
                      
                      </td></tr>
                                     
                                     </table>
                              </td>
                             </tr>
                   
                           
                                
                                            <tr>
                                             
                                            <td   >
                                            <table cellspacing="0" cellpadding="0">
                                            <tr>
                                            <td> <div id="btnAdd"  class="btn btn-primary btn-small" ><i class="fa fa-external-link"></i>
                                                Apply Settings</div></td>
                                            
                                           
                                            </tr>
                                            </table>
                                            </td>
                                            
                                            </tr>

                     </table>

                        </div>
                    </div>


  

                     
                </div>
                <!-- /page content -->

                

            </div>

        <!-- footer content -->
                <footer>
                     <uc1:ucfooter ID="ucfooter1" runat="server" />
                </footer>
            <!-- /footer content -->

 
</form>

    <link href="js/ui.jqgrid.css" rel="stylesheet" type="text/css" />
     <script src="js/jquery-1.9.0.min.js" type="text/javascript"></script>
     <script src="js/jquery.jqGrid.js" type="text/javascript"></script>
     <script src="js/grid.locale-en.js" type="text/javascript"></script>
     <link href="js/jquery-ui-1.9.2.custom.css" rel="stylesheet" type="text/css" />
 
     <script src="js/jquery-ui.js"></script>
    <script src="js/customValidation.js" type="text/javascript"></script>
   <script type="text/javascript" src="js/jquery.uilock.js"></script>

   <script language="javascript" type="text/javascript">


       function InsertUpdate() {
           var BranchId = 0;

           BranchId = $("#ddlBranch").val();
           if (BranchId == "0") {
               alert("Choose Branch");
               return;
           }
           var BankAccounts = "";
           var objAccounts = {};
           var CN_Acc2DebitCode = 0;
           var CN_Acc2DebitName = "";
           var OrderSaleAcnt = 0;
           var OrderSaleAcntName = "";
           var OrderAdvAcnt = 0;
           var OrderAdvAcntName = "";
           var DebitNoteAcnt = 0;
           var DebitNoteAcntName = "";
           var CompAcnt = 0;
           var CompAcntName = "";
           var DisplayAcnt = 0;
           var DisplayAcntName = "";
           var AdjAcnt = 0;
           var AdjAcntName = "";
           var DiscntAcnt = 0;
           var DiscntAcntName = "";
           var TcsAcnt = 0;
           var TcsAcntName = "";
           var BillRoundAcnt = 0;
           var BillRoundAcntName = "";
           var ServiceTaxAcnt = 0;
           var ServiceTaxAcntName = "";
           var ServiceChrgAcnt = 0;
           var ServiceChrgAcntName = "";
           var IncomeTaxAcnt = 0;
           var IncomeTaxAcntName = "";
           var LabourAcnt = 0;
           var LabourAcntName = "";
           var GstdedAcnt = 0;
           var GstdedAcntName = "";
           var OtherdedAcnt = 0;
           var OtherdedAcntName = "";
           var SecurityAcnt = 0;
           var SecurityAcntName = "";
           var ResrveAcnt = 0;
           var ResrveAcntName = "";
           var JobwrkAcnt = 0;
           var JobwrkAcntName = "";
           var RarAcnt = 0;
           var RarAcntName = "";
           var SaleAcnt = 0;
           var SaleAcntName = "";
           var PurchaseAcnt = 0;
           var PurchaseAcntName = "";
           var JournalAcnt = 0;
           var JournalAcntName = "";
           var MSEAcnt = 0;
           var MSEAcntName = "";
           var OutpurAcnt = 0;
           var OutpurAcntName = "";
           var OutTaxAcnt = 0;
           var OutTaxAcntName = "";
           var CstsaleAcnt = 0;
           var CstsaleAcntName = "";
           var CstsaleTaxAcnt = 0;
           var CstsaleTaxAcntName = "";
          


		   CN_Acc2DebitCode = $("#ddlcreditnote").val();
		   CN_Acc2DebitName = $("#ddlcreditnote option:selected").text();

		   OrderSaleAcnt = $("#ddlOrderSale").val();
		   OrderSaleAcntName = $("#ddlOrderSale option:selected").text();

		   OrderAdvAcnt = $("#ddlOrderAdv").val();
		   OrderAdvAcntName = $("#ddlOrderAdv option:selected").text();

		   DebitNoteAcnt = $("#ddldebitnote").val();
		   DebitNoteAcntName = $("#ddldebitnote option:selected").text();

           CompAcnt = $("#ddlcashinhand").val();
           CompAcntName = $("#ddlcashinhand option:selected").text();

		   DisplayAcnt = $("#ddldisplay").val();
		   DisplayAcntName = $("#ddldisplay option:selected").text();

		   AdjAcnt = $("#ddladjustment").val();
		   AdjAcntName = $("#ddladjustment option:selected").text();

		   DiscntAcnt = $("#ddldiscount").val();
		   DiscntAcntName = $("#ddldiscount option:selected").text();

		   TcsAcnt = $("#ddltcsaccount").val();
		   TcsAcntName = $("#ddltcsaccount option:selected").text();

		   BillRoundAcnt = $("#ddlbillround").val();
		   BillRoundAcntName = $("#ddlbillround option:selected").text();

		   ServiceTaxAcnt = $("#ddlservicetax").val();
		   ServiceTaxAcntName = $("#ddlservicetax option:selected").text();

		   ServiceChrgAcnt = $("#ddlservicecharge").val();
		   ServiceChrgAcntName = $("#ddlservicecharge option:selected").text();

		   IncomeTaxAcnt = $("#ddlincometax").val();
		   IncomeTaxAcntName = $("#ddlincometax option:selected").text();

		   LabourAcnt = $("#ddllabour").val();
		   LabourAcntName = $("#ddllabour option:selected").text();

		   GstdedAcnt = $("#ddlgstded").val();
		   GstdedAcntName = $("#ddlgstded option:selected").text();

		   OtherdedAcnt = $("#ddlotherded").val();
		   OtherdedAcntName = $("#ddlotherded option:selected").text();


		   SecurityAcnt = $("#ddlsecurity").val();
		   SecurityAcntName = $("#ddlsecurity option:selected").text();
		   ResrveAcnt = $("#ddlreserve").val();
		   ResrveAcntName = $("#ddlreserve option:selected").text();
		   JobwrkAcnt = $("#ddljobwrk").val();
		   JobwrkAcntName = $("#ddljobwrk option:selected").text();
		   RarAcnt = $("#ddlrar").val();
		   RarAcntName = $("#ddlrar option:selected").text();
		   SaleAcnt = $("#ddlsale").val();
		   SaleAcntName = $("#ddlsale option:selected").text();
		   PurchaseAcnt = $("#ddlpurchase").val();
		   PurchaseAcntName = $("#ddlpurchase option:selected").text();
		   JournalAcnt = $("#ddljournal").val();
		   JournalAcntName = $("#ddljournal option:selected").text();
		   MSEAcnt = $("#ddlmse").val();
           MSEAcntName = $("#ddlmse option:selected").text();
		   OutpurAcnt = $("#ddloutpurchase").val();
		   OutpurAcntName = $("#ddloutpurchase option:selected").text();
		   OutTaxAcnt = $("#ddlouttax").val();
		   OutTaxAcntName = $("#ddlouttax option:selected").text();
		   CstsaleAcnt = $("#ddlcstsale").val();
		   CstsaleAcntName = $("#ddlcstsale option:selected").text();
		   CstsaleTaxAcnt = $("#ddlcstsaletax").val();
           CstsaleTaxAcntName = $("#ddlcstsaletax option:selected").text();

           BankAccounts = $('input[name="accountbank"]:checkbox:checked').map(function () {
			   return this.value;
           }).get();
           
     //      objAccounts.CN_Acc2DebitCode = CN_Acc2DebitCode;
     //      objAccounts.CN_Acc2DebitName = CN_Acc2DebitName;
     //      objAccounts.OrderSaleAcnt = OrderSaleAcnt;
     //      objAccounts.OrderSaleAcntName = OrderSaleAcntName;
     //      objAccounts.OrderAdvAcnt = OrderAdvAcnt;
     //      objAccounts.OrderAdvAcntName = OrderAdvAcntName;
     //      objAccounts.DebitNoteAcnt = DebitNoteAcnt;
		   //objAccounts.DebitNoteAcntName = DebitNoteAcntName;
     //      objAccounts.CompAcnt = CompAcnt;
     //      objAccounts.CompAcntName = CompAcntName;
     //      objAccounts.DisplayAcnt = DisplayAcnt;
     //      objAccounts.DisplayAcntName = DisplayAcntName;
     //      objAccounts.AdjAcnt = AdjAcnt;
     //      objAccounts.AdjAcntName = AdjAcntName;
     //      objAccounts.DiscntAcnt = DiscntAcnt;
     //      objAccounts.DiscntAcntName = DiscntAcntName;
     //      objAccounts.TcsAcnt = TcsAcnt;
     //      objAccounts.TcsAcntName = TcsAcntName;
     //      objAccounts.BillRoundAcnt = BillRoundAcnt;
     //      objAccounts.BillRoundAcntName = BillRoundAcntName;
     //      objAccounts.ServiceChrgAcnt = ServiceChrgAcnt;
     //      objAccounts.ServiceChrgAcntName = ServiceChrgAcntName;
     //      objAccounts.ServiceTaxAcnt = ServiceTaxAcnt;
     //      objAccounts.ServiceTaxAcntName = ServiceTaxAcntName;
     //      objAccounts.IncomeTaxAcnt = IncomeTaxAcnt;
     //      objAccounts.IncomeTaxAcntName = IncomeTaxAcntName;
     //      objAccounts.LabourAcnt = LabourAcnt;
     //      objAccounts.LabourAcntName = LabourAcntName;
     //      objAccounts.GstdedAcnt = GstdedAcnt;
     //      objAccounts.GstdedAcntName = GstdedAcntName;
     //      objAccounts.OtherdedAcnt = OtherdedAcnt;
     //      objAccounts.OtherdedAcntName = OtherdedAcntName;
     //      objAccounts.SecurityAcnt = SecurityAcnt;
     //      objAccounts.SecurityAcntName = SecurityAcntName;
     //      objAccounts.ResrveAcnt = ResrveAcnt;
     //      objAccounts.ResrveAcntName = ResrveAcntName;
     //      objAccounts.JobwrkAcnt = JobwrkAcnt;
     //      objAccounts.JobwrkAcntName = JobwrkAcntName;
     //      objAccounts.RarAcnt = RarAcnt;
     //      objAccounts.RarAcntName = RarAcntName;
     //      objAccounts.SaleAcnt = SaleAcnt;
     //      objAccounts.SaleAcntName = SaleAcntName;

     //      objAccounts.PurchaseAcnt = PurchaseAcnt;
     //      objAccounts.PurchaseAcntName = PurchaseAcntName;

     //      objAccounts.JournalAcnt = JournalAcnt;
     //      objAccounts.JournalAcntName = JournalAcntName;

     //      objAccounts.MSEAcnt = MSEAcnt;
     //      objAccounts.MSEAcntName = MSEAcntName;

     //      objAccounts.OutpurAcnt = OutpurAcnt;
     //      objAccounts.OutpurAcntName = OutpurAcntName;
     //      objAccounts.OutTaxAcnt = OutTaxAcnt;
     //      objAccounts.OutTaxAcntName = OutTaxAcntName;

     //      objAccounts.CstsaleAcnt = CstsaleAcnt;
     //      objAccounts.CstsaleAcntName = CstsaleAcntName;
     //      objAccounts.CstsaleTaxAcnt = CstsaleTaxAcnt;
     //      objAccounts.CstsaleTaxAcntName = CstsaleTaxAcntName;
           
		  

         //  var DTO = { 'objSettings': objAccounts, 'objAccount': BankAccounts };

           $.ajax({

               type: "POST",
			   data: '{"CN_Acc2DebitCode":"' + CN_Acc2DebitCode + '","CN_Acc2DebitName":"' + CN_Acc2DebitName + '","OrderSaleAcnt":"' + OrderSaleAcnt + '","OrderSaleAcntName":"' + OrderSaleAcntName + '","OrderAdvAcnt":"' + OrderAdvAcnt + '","OrderAdvAcntName":"' + OrderAdvAcntName + '","DebitNoteAcnt":"' + DebitNoteAcnt + '","DebitNoteAcntName":"' + DebitNoteAcntName + '","CompAcnt":"' + CompAcnt + '","CompAcntName":"' + CompAcntName + '","DisplayAcnt":"' + DisplayAcnt + '","DisplayAcntName":"' + DisplayAcntName + '","AdjAcnt":"' + AdjAcnt + '","AdjAcntName":"' + AdjAcntName + '","DiscntAcnt":"' + DiscntAcnt + '","DiscntAcntName":"' + DiscntAcntName + '","TcsAcnt":"' + TcsAcnt + '","TcsAcntName":"' + TcsAcntName + '","BillRoundAcnt":"' + BillRoundAcnt + '","BillRoundAcntName":"' + BillRoundAcntName + '","ServiceChrgAcnt":"' + ServiceChrgAcnt + '","ServiceChrgAcntName":"' + ServiceChrgAcntName + '","ServiceTaxAcnt":"' + ServiceTaxAcnt + '","ServiceTaxAcntName":"' + ServiceTaxAcntName + '","IncomeTaxAcnt":"' + IncomeTaxAcnt + '","IncomeTaxAcntName":"' + IncomeTaxAcntName + '","LabourAcnt":"' + LabourAcnt + '","LabourAcntName":"' + LabourAcntName + '","GstdedAcnt":"' + GstdedAcnt + '","GstdedAcntName":"' + GstdedAcntName + '","OtherdedAcnt":"' + OtherdedAcnt + '","OtherdedAcntName":"' + OtherdedAcntName + '","SecurityAcnt":"' + SecurityAcnt + '","SecurityAcntName":"' + SecurityAcntName + '","ResrveAcnt":"' + ResrveAcnt + '","ResrveAcntName":"' + ResrveAcntName + '","JobwrkAcnt":"' + JobwrkAcnt + '","JobwrkAcntName":"' + JobwrkAcntName + '","RarAcnt":"' + RarAcnt + '","RarAcntName":"' + RarAcntName + '","SaleAcnt":"' + SaleAcnt + '","SaleAcntName":"' + SaleAcntName + '","PurchaseAcnt":"' + PurchaseAcnt + '","PurchaseAcntName":"' + PurchaseAcntName + '","JournalAcnt":"' + JournalAcnt + '","JournalAcntName":"' + JournalAcntName + '","MSEAcnt":"' + MSEAcnt + '","MSEAcntName":"' + MSEAcntName + '","OutpurAcnt":"' + OutpurAcnt + '","OutpurAcntName":"' + OutpurAcntName + '","OutTaxAcnt":"' + OutTaxAcnt + '","OutTaxAcntName":"' + OutTaxAcntName + '","CstsaleAcnt":"' + CstsaleAcnt + '","CstsaleAcntName":"' + CstsaleAcntName + '","CstsaleTaxAcnt":"' + CstsaleTaxAcnt + '","CstsaleTaxAcntName":"' + CstsaleTaxAcntName + '","BankAccounts":"' + BankAccounts + '"}',
			   url: "manageaccountsettings.aspx/Insert",
			   contentType: "application/json",
			   dataType: "json",
			   success: function (msg) {

               //type: "POST",
               //contentType: "application/json; charset=utf-8",
               //url: "manageaccountsettings.aspx/Insert",
               //data: JSON.stringify(DTO),
               //dataType: "json",
               //success: function (msg) {

                   var obj = jQuery.parseJSON(msg.d);

                   if (obj.Status == 1) {

                       alert("Accounts Saved Successfully.");
                       return;
                   }


               },
               error: function (xhr, ajaxOptions, thrownError) {

                   var obj = jQuery.parseJSON(xhr.responseText);
                   alert(obj.Message);
               },
               complete: function () {
                   $.uiUnlock();
               }
           });

       }




       $(document).ready(
           function () {
               $("#ddlcreditnote").val(0);
        $("#ddlBranch").change(function () {

            var Type = $("#ddlBranch").val();
            $.uiLock('');

            $.ajax({
                type: "POST",
                data: '{"Type":"' + Type + '" }',
                url: "manageaccountsettings.aspx/FillSettings",
                contentType: "application/json",
                dataType: "json",
                success: function (msg) {

                    var obj = jQuery.parseJSON(msg.d);

					var cnacc2debit = obj.setttingData.CN_Acc2DebitCode;
					$("#ddlcreditnote option[value='" + cnacc2debit + "']").prop("selected", true);


					var OrderSaleAcnt = obj.setttingData.OrderSaleAcnt;
					$("#ddlOrderSale option[value='" + OrderSaleAcnt + "']").prop("selected", true);

					var OrderAdvAcnt = obj.setttingData.OrderAdvAcnt;
					$("#ddlOrderAdv option[value='" + OrderAdvAcnt + "']").prop("selected", true);

					var DebitNoteAcnt = obj.setttingData.DebitNoteAcnt;
					$("#ddldebitnote option[value='" + DebitNoteAcnt + "']").prop("selected", true);

					var CompAcnt = obj.setttingData.CompAcnt;
					$("#ddlcashinhand option[value='" + CompAcnt + "']").prop("selected", true);

					var DisplayAcnt = obj.setttingData.DisplayAcnt;
					$("#ddldisplay option[value='" + DisplayAcnt + "']").prop("selected", true);

					var AdjAcnt = obj.setttingData.AdjAcnt;
					$("#ddladjustment option[value='" + AdjAcnt + "']").prop("selected", true);

					var DiscntAcnt = obj.setttingData.DiscntAcnt;
					$("#ddldiscount option[value='" + DiscntAcnt + "']").prop("selected", true);
					var TcsAcnt = obj.setttingData.TcsAcnt;
					$("#ddltcsaccount option[value='" + TcsAcnt + "']").prop("selected", true);

					var BillRoundAcnt = obj.setttingData.BillRoundAcnt;
					$("#ddlbillround option[value='" + BillRoundAcnt + "']").prop("selected", true);

					var ServiceChrgAcnt = obj.setttingData.ServiceChrgAcnt;
					$("#ddlservicecharge option[value='" + ServiceChrgAcnt + "']").prop("selected", true);

					var ServiceTaxAcnt = obj.setttingData.ServiceTaxAcnt;
					$("#ddlservicetax option[value='" + ServiceTaxAcnt + "']").prop("selected", true);

					var IncomeTaxAcnt = obj.setttingData.IncomeTaxAcnt;
					$("#ddlincometax option[value='" + IncomeTaxAcnt + "']").prop("selected", true);

					var LabourAcnt = obj.setttingData.LabourAcnt;
					$("#ddllabour option[value='" + LabourAcnt + "']").prop("selected", true);
					var GstdedAcnt = obj.setttingData.GstdedAcnt;
					$("#ddlgstded option[value='" + GstdedAcnt + "']").prop("selected", true);

					var OtherdedAcnt = obj.setttingData.OtherdedAcnt;
					$("#ddlotherded option[value='" + OtherdedAcnt + "']").prop("selected", true);

					var SecurityAcnt = obj.setttingData.SecurityAcnt;
					$("#ddlsecurity option[value='" + SecurityAcnt + "']").prop("selected", true);

					var ResrveAcnt = obj.setttingData.ResrveAcnt;
					$("#ddlreserve option[value='" + ResrveAcnt + "']").prop("selected", true);

					var JobwrkAcnt = obj.setttingData.JobwrkAcnt;
					$("#ddljobwrk option[value='" + JobwrkAcnt + "']").prop("selected", true);

					var RarAcnt = obj.setttingData.RarAcnt;
					$("#ddlrar option[value='" + RarAcnt + "']").prop("selected", true);
					var SaleAcnt = obj.setttingData.SaleAcnt;
					$("#ddlsale option[value='" + SaleAcnt + "']").prop("selected", true);

					var PurchaseAcnt = obj.setttingData.PurchaseAcnt;
					$("#ddlpurchase option[value='" + PurchaseAcnt + "']").prop("selected", true);

					var JournalAcnt = obj.setttingData.JournalAcnt;
					$("#ddljournal option[value='" + JournalAcnt + "']").prop("selected", true);

					var MSEAcnt = obj.setttingData.MSEAcnt;
					$("#ddlmse option[value='" + MSEAcnt + "']").prop("selected", true);

					var OutpurAcnt = obj.setttingData.OutpurAcnt;
					$("#ddloutpurchase option[value='" + OutpurAcnt + "']").prop("selected", true);

					var OutTaxAcnt = obj.setttingData.OutTaxAcnt;
					$("#ddlouttax option[value='" + OutTaxAcnt + "']").prop("selected", true);

					var CstSaleAcnt = obj.setttingData.CstSaleAcnt;
					$("#ddlcstsale option[value='" + CstSaleAcnt + "']").prop("selected", true);

					var CstsaleTaxAcnt = obj.setttingData.CstsaleTaxAcnt;
					$("#ddlcstsaletax option[value='" + CstsaleTaxAcnt + "']").prop("selected", true);

					var m_BankList = obj.setttingData.BankAccounts;

					$("input[name='accountbank']").prop("checked", false);


					var parts = [];
					parts = m_BankList.split(',');


					for (var j = 0; j < parts.length; j++) {

						$("#chkS_" + parts[j] + "").prop('checked', true);
					}



                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

                    $.uiUnlock();

                }

            });


        });


        $("#btnAdd").click(
        function () {

            InsertUpdate();
        }
        );

      
    });


   </script>
</asp:Content>

